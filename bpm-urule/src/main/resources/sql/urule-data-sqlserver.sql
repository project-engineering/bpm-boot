insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (1, 'DEFAULT.dbid', '0', '默认ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (2, 'URULE_PROPERTY.dbid', '1000', '属性ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (3, 'URULE_GROUP_ROLE.dbid', '0', '团队角色ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (4, 'URULE_GROUP_USER.dbid', '0', '团队用户ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (5, 'URULE_GROUP_USER_ROLE.dbid', '0', '团队角色用户ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (6, 'URULE_PROJECT.dbid', '0', '项目ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (7, 'URULE_PROJECT_ROLE.dbid', '0', '项目角色ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (8, 'URULE_PROJECT_USER.dbid', '0', '项目用户ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (9, 'URULE_PROJECT_USER_ROLE.dbid', '0', '项目角色用户ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (10, 'URULE_INVITE.dbid', '0', '团队邀请ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (11, 'URULE_AUTHORITY.dbid', '0', '权限ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (12, 'URULE_LOG_USERLOGIN.dbid', '0', '登录日志ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (13, 'URULE_LOG_OPERATION.dbid', '0', '操作日志ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (14, 'URULE_LOG_KNOWLEDGE.dbid', '0', '知识包执行日志ID', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (15, 'URULE_LOG_BATCH.dbid', '0', '批处理执行日志ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (16, 'URULE_LOG_BATCH_SKIP.dbid', '0', '批处理异常日志ID', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (41, 'URULE_DATASOURCE.dbid', '0', '数据源', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (42, 'URULE_BATCH.dbid', '0', '执行方案', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (43, 'URULE_BATCH_DATA_PROVIDER.dbid', '0', '加载数据方案', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (44, 'URULE_BATCH_PROVIDER_FIELD.dbid', '0', '加载数据方案字段表', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (45, 'URULE_BATCH_DATA_RESOLVER.dbid', '0', '存储数据方案', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (46, 'URULE_BATCH_RESOLVER_ITEM.dbid', '0', '存储数据方案存储项表', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (47, 'URULE_BATCH_RESOLVER_FIELD.dbid', '0', '存储数据方案字段表', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (51, 'URULE_FILE.dbid', '0', '规则文件ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (52, 'URULE_PACKET_PACKAGE.dbid', '0', '导出后上传的知识包ID', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (53, 'URULE_PACKET.dbid', '0', '知识包ID', 'system')

 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (54, 'URULE_PACKET_FILE.dbid', '0', '知识包文件ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (55, 'URULE_DEPLOYED_PACKET.dbid', '0', '已发布知识包ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (56, 'URULE_DEPLOYED_PACKET_FILE.dbid', '0', '已发布知识包文件ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (57, 'URULE_PACKET_APPLY.dbid', '0', '审批ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (58, 'URULE_PACKET_APPLY_DETAIL.dbid', '0', '审批明细ID', 'system')
 
insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (59, 'URULE_PACKET_SCENARIO.dbid', '0', '知识包测试ID', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (60, 'URULE_URL_CONFIG.dbid', '0', 'URL配置', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (61, 'URULE_DYNAMIC_JAR.dbid', '0', '动态JAR文件', 'system')
 

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (101, 'urule.application.title', 'URule Console Pro 4', 'Application Title', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (201, 'urule.security.useConservativeStrategy', 'false', '是否启用权限验证的保守策略', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (301, 'urule.mail.smtp.host', 'smtp.163.com', 'SMTP服务器', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (302, 'urule.mail.smtp.port', '25', 'SMTP端口', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (303, 'urule.mail.smtp.auth', 'true', '需要验证用户名密码', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (304, 'urule.mail.smtp.user', 'urule@163.com', '发件人', 'system')

insert into URULE_PROPERTY (ID_, KEY_, VALUE_, LABEL_, TYPE_) values (305, 'urule.mail.smtp.pass', 'urule', '授权码', 'system')