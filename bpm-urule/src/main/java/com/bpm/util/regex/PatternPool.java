package com.bpm.util.regex;

/**
 * @author liuc
 * @version V1.0
 * @description 正则表达式常量池
 * @date 2021/9/23 13:54
 * @since JDK1.8
 */
public class PatternPool {
    /**
     * 营业执照 统一社会信用代码由18位阿拉伯数字或英文大写字母表示（不包括I,O,Z,S,V以防止和阿拉伯字母混淆）
     */
    public static final String CREDIT_CODE_PATTERN = "^([159Y]{1})([1239]{1})([0-9ABCDEFGHJKLMNPQRTUWXY]{6})([0-9ABCDEFGHJKLMNPQRTUWXY]{9})([0-90-9ABCDEFGHJKLMNPQRTUWXY])$";

    /**
     * email正则表达式
     */
    public static final String EMAIL_PATTERN = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

    /**
     * 身份证正则表达式
     */
    public static final String ID_CARD_PATTERN = "[1-9]\\d{16}[a-zA-Z0-9]{1}";

    /**
     * 手机号码正则表达式
     */
    public static final String MOBILE_PATTERN = "(\\+\\d+)?1[3456789]\\d{9}$";

    /**
     * 固定电话正则表达式
     */
    public static final String PHONE_PATTERN = "(\\+\\d+)?(\\d{3,4}\\-?)?\\d{7,8}$";

    /**
     * 空白字符正则表达式
     */
    public static final String BLANK_SPACE_PATTERN = "\\s+";

    /**
     * QQ正则表达式
     */
    public static final String QQ_PATTERN = "^[1-9]*[1-9][0-9]*$";

    /**
     * 中国邮政编码正则表达式
     */
    public static final String POST_CODE_PATTERN = "[1-9]\\d{5}";

    /**
     * 纯数字正则表达式
     */
    public static final String NUMBER_PATTERN = "[0-9]{1,}";
    /**
     * 正数值正则表达式
     */
    public static final String POSITIVE_NUMBER_PATTERN = "[0-9]*\\.?[0-9]+";
    /**
     * 零和非零开头的数字
     */
    public static final String NONNEGATIVE_NUMBER_PATTERN = "^(0|[1-9][0-9]*)$";
    /**
     * 非零开头的最多带两位小数的数字
     */
    public static final String NONNEGATIVE_DECIMAL_NUMBER_PATTERN = "^([1-9][0-9]*)+(.[0-9]{1,2})?$";
}
