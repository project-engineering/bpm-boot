package com.bpm.util.regex;

import com.bpm.constant.Constant;
import com.bpm.util.UtilValidate;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author liuc
 * @version V1.0
 * @description 正则表达式验证工具类
 * @date 2021/9/23 13:40
 * @since JDK1.8
 */
public class RegexUtil {

    /**
     * @description
     * @author liuc
     * @date 2021/9/23 13:44
     * @since JDK1.8
     * @version V1.0
     */
    public static boolean isMatches(String regex, String content) {
        Matcher matcher = matcher(regex, content);
        boolean bool = matcher.matches();
        return bool;
    }

    /**
     * 编译一个正则表达式
     *
     * @param regex
     * @return
     */
    public static Pattern compile(String regex, boolean isInsensitive) {
        if (true == isInsensitive) {
            return Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        } else {
            return Pattern.compile(regex);
        }
    }

    /**
     * 返回一个mathcer
     *
     * @param regex
     * @param content
     * @return
     */
    public static Matcher matcher(String regex, String content) {
        return compile(regex, true).matcher(content);
    }

    /**
     * 验证Email
     * @param email email地址，格式：zhangsan@sina.com，zhangsan@xxx.com.cn，xxx代表邮件服务商
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkEmail(String email) {
        return isMatches(PatternPool.EMAIL_PATTERN,email);
    }

    /**
     * 验证身份证号码
     * @param idCard 居民身份证号码18位，第一位不能为0，最后一位可能是数字或字母，中间16位为数字 \d同[0-9]
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkIdCard(String idCard) {
        return isMatches(PatternPool.ID_CARD_PATTERN,idCard);
    }

    /**
     * 验证手机号码（支持国际格式，+86135xxxx...（中国内地），+00852137xxxx...（中国香港））
     * @param mobile 移动、联通、电信运营商的号码段
     *<p>移动的号段：134(0-8)、135、136、137、138、139、147（预计用于TD上网卡）
     *、150、151、152、157（TD专用）、158、159、187（未启用）、188（TD专用）</p>
     *<p>联通的号段：130、131、132、155、156（世界风专用）、185（未启用）、186（3g）</p>
     *<p>电信的号段：133、153、180（未启用）、189</p>
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkMobile(String mobile) {
        return isMatches(PatternPool.MOBILE_PATTERN,mobile);
    }

    /**
     * 验证固定电话号码
     * @param phone 电话号码，格式：国家（地区）电话代码 + 区号（城市代码） + 电话号码，如：+8602085588447
     * <p><b>国家（地区） 代码 ：</b>标识电话号码的国家（地区）的标准国家（地区）代码。它包含从 0 到 9 的一位或多位数字，
     *  数字之后是空格分隔的国家（地区）代码。</p>
     * <p><b>区号（城市代码）：</b>这可能包含一个或多个从 0 到 9 的数字，地区或城市代码放在圆括号——
     * 对不使用地区或城市代码的国家（地区），则省略该组件。</p>
     * <p><b>电话号码：</b>这包含从 0 到 9 的一个或多个数字 </p>
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkPhone(String phone) {
        return isMatches(PatternPool.PHONE_PATTERN,phone);
    }

    /**
     * 验证空白字符
     * @param blankSpace 空白字符，包括：空格、\t、\n、\r、\f、\x0B
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkBlankSpace(String blankSpace) {
        return isMatches(PatternPool.BLANK_SPACE_PATTERN,blankSpace);
    }

    /**
     * @description 验证QQ
     * @author liuc
     * @date 2021/9/23 19:47
     * @since JDK1.8
     * @version V1.0
     */
    public static boolean checkQq(String qq) {
        return isMatches(PatternPool.QQ_PATTERN,qq);
    }

    /**
     * 验证中国邮政编码
     * @param postcode 邮政编码
     * @return 验证成功返回true，验证失败返回false
     */
    public static boolean checkPostcode(String postcode) {
        return isMatches(PatternPool.POST_CODE_PATTERN,postcode);
    }

    /**
     * 判断字符串是否为纯数字
     * @param str
     * @return boolean
     * @author liuc
     * @date 2021/11/30 16:54
     * @throws
     */
    public static boolean isDigit(String str) {
        if (str == null) {
            return false;
        }
        return isMatches(PatternPool.NUMBER_PATTERN,str);
    }

    /**
     * 判断字符串是否是数字（包含正数、负数、小数）
     * @param str
     * @return boolean
     * @author liuc
     * @date 2021/12/8 14:08
     * @throws
     */
    public static boolean isNumeric(String str) {
        String bigStr;
        try {
            bigStr = new BigDecimal(str).toString();
        } catch (Exception e) {
            //异常 说明包含非数字。
            return false;
        }
        return true;
    }

    /**
     * 判断字符串是否是0或非0开头的数字
     * @param str
     * @return boolean
     * @author liuc
     * @date 2021/12/8 14:08
     * @throws
     */
    public static boolean isNonnegative(String str) {
        if (str == null) {
            return false;
        }
        return isMatches(PatternPool.NONNEGATIVE_NUMBER_PATTERN,str);
    }

    /**
     * 判断字符串是否是正数
     * 例子：
     * 返回fasle的有：1000a、a、特殊字符、汉字、-1000、+1000、00.5等
     * 返回true的有：1000.、1000.01111小数点后位数不限、正整数、.5等
     * @param str
     * @return boolean
     * @author liuc
     * @date 2021/12/8 14:08
     * @throws
     */
    public static boolean isPositive(String str) {
        boolean flag = false;
        if (str == null) {
            flag = false;
        }
        if (str.contains(Constant.PERIOD)) {
            String [] arr = str.split("\\.");
            if (UtilValidate.isEmpty(arr[0])) {
                flag = true;
            }
            //小数左边的部分为0或非0开头的数字，即0.1、10.1、2.2这种，如果为00.1这种就会返回false
            if (isNonnegative(arr[0])) {
                if (arr.length == 1 || isNumeric(arr[1])) {
                    flag = true;
                }
            }
        }else {
            flag = isMatches(PatternPool.POSITIVE_NUMBER_PATTERN,str);
        }
        return flag;
    }

    /**
     * 非零开头的最多带两位小数的数字
     * @param str
     * @return
     */
    public static boolean isNonnegativeDecimal(String str) {
        if (str == null) {
            return false;
        }
        return isMatches(PatternPool.NONNEGATIVE_DECIMAL_NUMBER_PATTERN,str);
    }
}
