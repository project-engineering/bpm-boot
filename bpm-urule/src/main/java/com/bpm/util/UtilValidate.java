package com.bpm.util;

import com.bpm.lang.IsEmpty;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @description 
 * @author liuc
 * @date 2021/8/24 11:05
 * @since JDK1.8
 * @version V1.0
 */
@Log4j2
public class UtilValidate {
    /***
     * @description 判断对象是否为空
     * @param o
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08
     * @throws
     */
    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof String) {
            return ((String) o).length() == 0;
        }
        if (o instanceof Collection) {
            return ((Collection<? extends Object>) o).size() == 0;
        }
        if (o instanceof Map) {
            return ((Map<? extends Object, ? extends Object>) o).size() == 0;
        }
        if (o instanceof CharSequence) {
            return ((CharSequence) o).length() == 0;
        }
        if (o instanceof IsEmpty) {
            return ((IsEmpty) o).isEmpty();
        }
        if (o instanceof Boolean) {
            return false;
        }
        if (o instanceof Number) {
            return false;
        }
        if (o instanceof Character) {
            return false;
        }
        if (o instanceof Date) {
            return false;
        }
        return false;
    }

    /***
     * @description 判断对象是否不为空
     * @param o
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08
     * @throws
     */
    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }

    /***
     * @description 比较两个对象是否相等
     * @param obj
     * @param obj2
     * @return boolean
     * @author liuc
     * @date 2021/8/24 11:08 
     * @throws
     */
    public static boolean areEqual(Object obj, Object obj2){
        if (obj == null){
            return obj2 == null;
        }else {
            return obj.equals(obj2);
        }
    }

    public static void notNull(@Nullable Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }
}
