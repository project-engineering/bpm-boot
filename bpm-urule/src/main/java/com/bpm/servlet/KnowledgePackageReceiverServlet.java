package com.bpm.servlet;


import com.bstek.urule.Utils;
import com.bstek.urule.runtime.DynamicSpringConfigLoader;
import com.bstek.urule.runtime.KnowledgePackageImpl;
import com.bstek.urule.runtime.RemoteDynamicJarsBuilder;
import com.bstek.urule.runtime.cache.CacheUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class KnowledgePackageReceiverServlet extends HttpServlet {
    private static final long a = -4342175088856372588L;
    public static final String URL = "/knowledgepackagereceiver";
    private DynamicSpringConfigLoader b;
    private RemoteDynamicJarsBuilder c;
    private Logger d = Logger.getGlobal();

    public KnowledgePackageReceiverServlet() {
    }

    public void init(ServletConfig var1) throws ServletException {
        super.init(var1);
        WebApplicationContext var2 = WebApplicationContextUtils.getRequiredWebApplicationContext(var1.getServletContext());
        this.b = (DynamicSpringConfigLoader)var2.getBean("urule.dynamicSpringConfigLoader");
        this.c = (RemoteDynamicJarsBuilder)var2.getBean("urule.remoteDynamicJarsBuilder");
    }

    public void doPost(HttpServletRequest var1, HttpServletResponse var2) throws ServletException, IOException {
        String var3 = var1.getParameter("_u");
        String var4 = var1.getParameter("_p");
        if (var3 != null && var4 != null) {
            var3 = URLDecoder.decode(var3, "utf-8");
            var4 = URLDecoder.decode(var4, "utf-8");
            if (var3.equals(this.c.getUser()) && var4.equals(this.c.getPwd())) {
                String var5 = var1.getParameter("dynamicjars");
                String var6 = var1.getParameter("packageId");
                String var7 = var1.getParameter("code");
                SimpleDateFormat var8 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String var9;
                String var10;
                if (StringUtils.isNotBlank(var6)) {
                    var9 = var1.getParameter("enable");
                    var10 = null;

                    try {
                        var6 = URLDecoder.decode(var6, "utf-8");
                        if (var6.startsWith("/")) {
                            var6 = var6.substring(1, var6.length());
                        }

                        if (StringUtils.isNotBlank(var9)) {
                            boolean var11 = Boolean.valueOf(var9);
                            CacheUtils.getKnowledgeCache().enable(var6, var11);
                            if (StringUtils.isNotBlank(var7)) {
                                CacheUtils.getKnowledgeCache().enable(var7, var11);
                            }
                        } else {
                            ServletInputStream var18 = var1.getInputStream();
                            byte[] var20 = IOUtils.toByteArray(var18);
                            var18.close();
                            String var13 = Utils.uncompress(var20);
                            if (var13 != null) {
                                KnowledgePackageImpl var14 = (KnowledgePackageImpl)Utils.stringToKnowledgePackage(var13);
                                var14.setPackageInfo(var6);
                                var10 = var14.getVersion();
                                CacheUtils.getKnowledgeCache().putKnowledge(var6, var14);
                                if (StringUtils.isNotBlank(var7)) {
                                    CacheUtils.getKnowledgeCache().putKnowledge(var7, var14);
                                }
                            }
                        }
                    } catch (Exception var16) {
                        var16.printStackTrace();
                        String var12 = this.a(var16);
                        this.a(var2, var12);
                        return;
                    }

                    if (StringUtils.isNotBlank(var9)) {
                        if (Boolean.valueOf(var9)) {
                            System.out.println("[" + var8.format(new Date()) + "] Successfully receive the server side to enable package:" + var6 + (StringUtils.isBlank(var7) ? "" : "(" + var7 + ")"));
                        } else {
                            System.out.println("[" + var8.format(new Date()) + "] Successfully receive the server side to disable package:" + var6 + (StringUtils.isBlank(var7) ? "" : "(" + var7 + ")"));
                        }
                    } else {
                        String var19 = "[" + var8.format(new Date()) + "] Successfully receive the server side to pushed package:" + var6 + (StringUtils.isBlank(var7) ? "" : "(" + var7 + ")");
                        if (StringUtils.isNotBlank(var10)) {
                            var19 = var19 + "(" + var10 + ")";
                        }

                        System.out.println(var19);
                    }
                } else if (var5 != null && var5.equals("true")) {
                    try {
                        var9 = this.b.buildDynamicJarsStoreDirectPath();
                        ServletInputStream var17 = var1.getInputStream();
                        this.c.unzipDynamicJars(var17, var9);
                        IOUtils.closeQuietly(var17);
                        System.out.println("[" + var8.format(new Date()) + "] Successfully receive the server side to pushed dynamic jars");
                        this.b.loadDynamicJars(var9);
                    } catch (Exception var15) {
                        var15.printStackTrace();
                        var10 = this.a(var15);
                        this.a(var2, var10);
                        return;
                    }
                }

                this.a(var2, "ok");
            } else {
                this.a(var2, "User or password is invalid.");
                this.d.warning("User or password is invalid.");
            }
        } else {
            this.a(var2, "User and password can not be null.");
            this.d.warning("User and password can not be null.");
        }
    }

    private void a(HttpServletResponse var1, String var2) throws ServletException, IOException {
        var1.setContentType("text/plain");
        PrintWriter var3 = var1.getWriter();
        var3.write(var2);
        var3.flush();
        var3.close();
    }

    private String a(Throwable var1) {
        ByteArrayOutputStream var2 = new ByteArrayOutputStream();
        PrintStream var3 = new PrintStream(var2);
        var1.printStackTrace(var3);
        String var4 = new String(var2.toByteArray());
        IOUtils.closeQuietly(var3);
        IOUtils.closeQuietly(var2);
        var4 = var4.replaceAll("\n", "<br>");
        return var4;
    }
}