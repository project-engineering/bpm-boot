package com.bpm.config;

import com.bpm.servlet.KnowledgePackageReceiverServlet;
import com.bpm.servlet.URuleServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UruleConfig {
    @Bean
    public ServletRegistrationBean registerUruleServlet() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(
                new URuleServlet(),true, "/urule/*");
        return servletRegistrationBean;
    }

    @Bean
    public ServletRegistrationBean registerKnowledgeServlet() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(
                new KnowledgePackageReceiverServlet(), "/knowledgepackagereceiver");
        return servletRegistrationBean;
    }
}
