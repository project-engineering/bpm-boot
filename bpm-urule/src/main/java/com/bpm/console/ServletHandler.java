package com.bpm.console;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface ServletHandler {
    void init();

    void execute(HttpServletRequest var1, HttpServletResponse var2) throws Exception;

    String url();
}