package com.bpm.console;


import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;


public class HttpServletResponseWrapper extends jakarta.servlet.http.HttpServletResponseWrapper {
    private ServletOutputStream a;

    public HttpServletResponseWrapper(HttpServletResponse var1) {
        super(var1);
    }

    public ServletOutputStream getOutputStream() throws IOException {
        if (this.a == null) {
            this.a = super.getOutputStream();
        }

        return this.a;
    }
}