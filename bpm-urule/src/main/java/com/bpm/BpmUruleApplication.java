package com.bpm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@SpringBootApplication(scanBasePackages = {"com.bpm"},exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class
})
@MapperScan({"com.bpm.*.mapper"})
@ImportResource("classpath:urule-console-context.xml")
public class BpmUruleApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BpmUruleApplication.class, args);
    }

}