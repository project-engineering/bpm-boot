package com.bpm.workflow.constant;

/**
 * @Author: zhuzj
 * @Date: 2021/10/28 21:46
 * @Description: 符号常量类
 */
public class SymbolConstants {

    /**
     * #{
     */
    public static final String SYMBOL_001 = "#{";
    /**
     * }
     */
    public static final String SYMBOL_002 = "}";
    /**
     * [
     */
    public static final String SYMBOL_003 = "[";
    /**
     * ]
     */
    public static final String SYMBOL_004 = "]";
    /**
     * \\.
     */
    public static final String SYMBOL_005 = "\\.";
    /**
     * []
     */
    public static final String SYMBOL_006 = "[]";
    /**
     * \\
     */
    public static final String SYMBOL_007 = "\\";
    /**
     * /
     */
    public static final String SYMBOL_008 = "/";
    /**
     * (
     */
    public static final String SYMBOL_009 = "(";
    /**
     * (
     */
    public static final char SYMBOL_009_CHAR = '(';
    /**
     * )
     */
    public static final String SYMBOL_010 = ")";
    /**
     * )
     */
    public static final char SYMBOL_010_CHAR = ')';
    /**
     * {
     */
    public static final String SYMBOL_011 = "{";
    /**
     * \"
     */
    public static final String SYMBOL_012 = "\"";
    /**
     * ^(
     */
    public static final String SYMBOL_013 = "^(";
    /**
     * ((\n)?.+(\n)?)
     */
    public static final String SYMBOL_014 = "((\n)?.+(\n)?)";
    /**
     * ((\n)?.+(\n)?)){2,}$
     */
    public static final String SYMBOL_015 = "((\n)?.+(\n)?)){2,}$";
    /**
     * /0/2/
     */
    public static final String SYMBOL_016 = "/0/2/";


    /**
     * ,
     */
    public static final String COMMA = ",";
    /**
     * .
     */
    public static final String POINT = ".";
    /**
     * :
     */
    public static final String COLON = ":";
    /**
     * ;
     */
    public static final String SEMICOLON = ";";

    /**
     * X-B3-TraceId
     */
    public static final String X_B3_TRACEID = "X-B3-TraceId";

    /**
     * X-B3-SpanId
     */
    public static final String X_B3_SPANID = "X-B3-SpanId";

    /**
     * X-B3-ParentSpanId
     */
    public static final String X_B3_PARENTSPANID = "X-B3-ParentSpanId";

    /**
     * X-Span-Export
     */
    public static final String X_SPAN_EXPORT = "X-Span-Export";

    /**
     * select distinct
     */
    public static final String SELECT_DISTINCT = "select distinct";

    /**
     * group by
     */
    public static final String GROUP_BY = "group by";

    /**
     * for update
     */
    public static final String FOR_UPDATE = "for update";

    /**
     * for update
     */
    public static final String QUERY = "query";
}
