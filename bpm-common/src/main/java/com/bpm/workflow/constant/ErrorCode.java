package com.bpm.workflow.constant;

public class ErrorCode {

    /**
     * 工作流程编码不能为空！
     */
    public static final String WF000001 = "WF000001";
    /**
     * 工作流程id不能为空！
     */
    public static final String WF000002 = "WF000002";
    /**
     * 工作流程编码和工作流程id不能同时为空！
     */
    public static final String WF000003 = "WF000003";
    /**
     * 当前时间不能为空！
     */
    public static final String WF000004 = "WF000004";
    /**
     * 定时执行参数不能全部为空！
     */
    public static final String WF000005 = "WF000005";
    /**
     * 上次执行时间不能晚于当前时间！
     */
    public static final String WF000006 = "WF000006";
    /**
     * 访问规则引擎平台失败,请检查规则引擎平台是否正常！
     */
    public static final String WF000007 = "WF000007";
    /**
     * 流程发起人不能为空，请确认！
     */
    public static final String WF000008 = "WF000008";
    /**
     * 输入的流程启动信息为空，无法确定需要启动的流程，请确认！
     */
    public static final String WF000009 = "WF000009";
    /**
     * 根据输入的相关信息，规则引擎无法确定需要启动的流程，请确认！
     */
    public static final String WF000010 = "WF000010";
    /**
     * 带有映射路径选择条件的工作节点必须指向一个网关节点，而且指向节点不能超过1个！
     */
    public static final String WF000011 = "WF000011";
    /**
     * 网关节点必须成对出现！
     */
    public static final String WF000012 = "WF000012";
    /**
     * 网关节点格式异常，无法判定是汇聚网关节点还是分支网关节点！
     */
    public static final String WF000013 = "WF000013";
    /**
     * 流程错误，非开始节点且没有前节点！
     */
    public static final String WF000014 = "WF000014";
    /**
     * 流程错误，非结束节点且没有后节点！
     */
    public static final String WF000015 = "WF000015";
    /**
     * 流程节点id不能为空！
     */
    public static final String WF000016 = "WF000016";
    /**
     * 流程节点id不能重复！
     */
    public static final String WF000017 = "WF000017";
    /**
     * 同一分支组的流程节点名称不能重复！
     */
    public static final String WF000018 = "WF000018";
    /**
     * 网关节点的下一个节点不允许是网关节点！
     */
    public static final String WF000019 = "WF000019";
    /**
     * 非网关发起的分支，目的节点中只允许出现一个网关节点！
     */
    public static final String WF000020 = "WF000020";
    /**
     * 选人失败！
     */
    public static final String WF000021 = "WF000021";
    /**
     * 不满足节点进入条件！
     */
    public static final String WF000022 = "WF000022";
    /**
     * 不满足节点离开条件！
     */
    public static final String WF000023 = "WF000023";
    /**
     * 任务处理人员不能为空！
     */
    public static final String WF000024 = "WF000024";
    /**
     * 任务标识不能为空！
     */
    public static final String WF000025 = "WF000025";
    /**
     * 指定的待办任务不存在，请确认！
     */
    public static final String WF000026 = "WF000026";
    /**
     * 不能部署没有开始节点的流程！
     */
    public static final String WF000027 = "WF000027";
    /**
     * activity版本号不能为空！
     */
    public static final String WF000028 = "WF000028";
    /**
     * 任务不存在或已执行，taskId:{0}!
     */
    public static final String WF000029 = "WF000029";
    /**
     * 上节处理人查找出错,请检查相关的处理人数据！
     */
    public static final String WF000030 = "WF000030";
    /**
     * 流程启动失败！
     */
    public static final String WF000031 = "WF000031";
    /**
     * 事项渠道类型不能为空！
     */
    public static final String WF000032 = "WF000032";
    /**
     * 回调服务配置url为空，请检查！
     */
    public static final String WF000035 = "WF000035";


    /**
     * 非业务异常 WF001001-WF002000
     */
    /**
     * 查询人员信息失败！
     */
    public static final String WF001001 = "WF001001";
    /**
     * 服务调用失败！
     */
    public static final String WF001002 = "WF001002";
    /**
     * {0}
     */
    public static final String WF001003 = "WF001003";
}
