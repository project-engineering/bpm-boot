package com.bpm.workflow.constant;

/**
 * @Author: zhuzj
 * @Date: 2021/10/27 20:53
 * @Description:
 */
public class ServiceNameConstants {

    /**
     * 根据流程编码查询流程定义
     */
    public static final String SELECT_ACTIVE_FLOW_BY_DEF_ID = "selectActiveFlowByDefId";

    /**
     * 根据流程编码和版本号查询流程定义
     */
    public static final String SELECT_BY_DEF_ID_ACT_VERSION = "selectByDefIdActVersion";

    /**
     * 根据流程编码查询流程定义列表
     */
    public static final String SELECT_BY_DEF_ID = "selectByDefId";

    /**
     * 根据属性名称查询扩展属性
     */
    public static final String SELECT_BY_OWN_ID_PROP_NAME = "selectByOwnIdPropName";

    /**
     * 根据ownId查询扩展属性
     */
    public static final String SELECT_MODEL_EXTENDS_PROP_BY_OWN_ID = "selectModelExtendsPropByOwnId";

    /**
     * 根据参数类型查询参数
     */
    public static final String SELECT_PARAM_BY_PARAM_TYPE = "selectParamByParamType";

    /**
     * 根据参数类型和编码查询参数
     */
    public static final String SELECT_PARAM_BY_PARAM_TYPE_PARAM_CODE = "selectParamByParamTypeParamCode";

    /**
     * 规则引擎服务
     */
    public static final String RULE_SERVICES = "RuleServices";

    /**
     * 系统利率区间维护审批回调服务
     */
    public static final String CALL_BACK_OF_UPDATE_BP_RATE = "callBackOfUpdateBpRate";

}
