package com.bpm.workflow.constant;

/**
 * @Author: zhuzj
 * @Date: 2021/10/27 21:17
 * @Description: 数字常量类
 */
public class NumberConstants {

    /**
     * -100
     */
    public static final int INT_NEGATIVE_100 = -100;
    /**
     * 1000
     */
    public static final int INT_1000 = 1000;
    /**
     * 20
     */
    public static final int INT_20 = 20;
    /**
     * -1
     */
    public static final long LONG_NEGATIVE_1 = -1L;
    /**
     * 10
     */
    public static final int INT_TEN = 10;
    /**
     * 0
     */
    public static final int INT_ZERO = 0;
    /**
     * 1
     */
    public static final String STRING_1 = "1";
    /**
     * 0
     */
    public static final String STRING_ZERO = "0";
    /**
     * 2
     */
    public static final int INT_TWO = 2;
    /**
     * 3
     */
    public static final int INT_THREE = 3;
    /**
     * 6
     */
    public static final int INT_SIX = 6;
    /**
     * 19
     */
    public static final int INT_19 = 19;
    /**
     * 30
     */
    public static final int INT_THIRTY = 30;
    /**
     * 100
     */
    public static final int INT_100 = 100;
    /**
     * 300
     */
    public static final int INT_300 = 300;
    /**
     * 10000
     */
    public static final int INT_10000 = 10000;
    /**
     * 20000
     */
    public static final int INT_20000 = 20000;
}
