package com.bpm.workflow.constant;

import java.util.Arrays;
import java.util.List;

/**
 * 常量类
 */
public class Constants {

	// ============================================节点类型分类/任务分类  ============================================//
	/**
	 * 流程节点类型-自动节点
	 */
	public static final String NODE_TYPE_ATTO = "auto";
	/**
	 * 流程节点类型-会签类型
	 */
	public static final String NODE_TYPE_SIGN = "sign";
	/**
	 * 流程节点类型-子流程类型
	 */
	public static final String NODETYPE_SUBPROC = "subproc";

	/**
	 * 流程节点类型-审批类型
	 */
	public static final String NODETYPE_APPROVE = "approve";

	/**
	 * 流程节点类型-task类型
	 */
	public static final String NODETYPE_TASK = "task";
	
	/**
	 * 流程节点类型-start类型
	 */
	public static final String NODETYPE_START = "start";
	
	/**
	 * 流程节点类型-end类型
	 */
	public static final String NODETYPE_END = "end";



	public static final String ADDITION_MAPKEY = "additionSign";

	/**
	 * 回退标记
	 */
	public static final String BACK_FROM_TASK = "backFromTask";

	// ============================================审批结果  分类============================================//

	/**
	 * 审批结果之--0-反对
	 */
	public static final String RESULT_OPPOSE = "1";

	/**
	 * 审批结果之--1-通过
	 */
	public static final String RESULT_PASS = "0";

	/**
	 * 审批结果之--2-再议
	 */
	public static final String RESULT_THINK = "2";
	
	/**
	 * 审批结果之--n-系统使用，自动关闭会签任务
	 */
	public static final String RESULT_SYSUSE = "n";

	// ============================================流程流转入参指定key 分类============================================//

	/**
	 * 前台进行执行人员选择 结果的报文key
	 */
	public static final String SPECIFYEXTOPERS = "specifyNextOpers";

	/**
	 * 前台进行人工路由选择 结果的报文key
	 */
	public static final String SPECIFYNEXTNODE = "specifyNextNode";

	/**
	 * 前台 流转确认提示 结果的报文key 1-已确认
	 */
	public static final String SPECIFYNEXTFLOW = "specifyNextFlow";

	/**
	 * 前台 定时时间确认 结果的报文key 1-已确认
	 */
	public static final String SPECIFYNEXTTIME = "specifyNextTime";
	
	/**
	 * 前台 人员选择所选人员任职结构 结果的报文key
	 */
	public static final String SPECIFYNEXTJOBBANK = "specifyNextJobBank";

	/**
	 * 前台 人员选择动态加签通知信息的报文key
	 */
	public static final String SPECIFYADDMESSAGE = "specityAddMessage";
	/**
	 * 前台 人员选择动态加签通知信息的报文key
	 */
	public static final String SPECIFYADDUSERIDS = "specityAddUserIds";
	/**
	 * 前台 人员选择动态加签通知信息的报文key
	 */
	public static final String SPECIFYNEXTBANKPOS = "specifyNextBankPost";
	
	/**
	 * 发起人信息
	 */
	public static final String STARTUSERDATA = "userIdMap";

	/**
	 * 客户经理
	 */
	public static final String CUSTOMERMUSER = "customerUserMap";

	/**
	 * 对方客户经理
	 */
	public static final String OPPOSIETUSER = "opposietUserMap";
	
	 /***
     * 用户ID 对应的KEY
     */
    public static final String USERID_KEY = "userId";

	 /***
    * 用户ID 对应的KEY
    */
   public static final String ASSIGN_USERID_KEY = "assignUserId";

	 /***
	  * 用户ID 对应的KEY
	  */
	 public static final String ASSIGN_TASKID_KEY = "assignTaskId";
	 
    /***
     * 用户名称 对应的KEY
     */
    public static final String USERNAME_KEY = "userName";
    
    /**
     * 任务ID  对应的KEY
     */
    public static final String TASKID_KEY = "taskId";

	/**
	 * 任务ID  对应的KEY
	 */
    public static final String TASK_ID_CODE = "taskIdCode";

    /**
     * 流程实例ID  对应的KEY
     */
    public static final String PROCESSID_KEY = "processId";
    
    /**
     * 流程编码  对应的KEY
     */
    public static final String PROCESSCODE_KEY = "processCode";
    
    /**
     * 流程实例简述 对应的KEY
     */
    public static final String PROCESSDESC_KEY = "processDesc";
    
    /***
     * 系统分类编码    对应的KEY
     */
    public static final String SYSTEMIDCODE_KEY = "systemIdCode";

	/**
	 * 前端上送的任务紧急程度 对应的KEY 0-正常 1-紧急
	 */
	public static final String TASK_URGENT_KEY = "taskUrgent";
	
    
    /**
     * 前端上送的  流程发起、任务处理、指定查询渠道 对应的KEY
     */
    public static final String INPUT_TRANCHANNEL_KEY = "tranChannel";

	// ============================================流程定义属性key
	// 分类============================================//

	/**
	 * 子流程节点的子流程编码属性名称
	 */
	public static final String SBUPROC_CODELIST = "subFlowSelectRule";

	/**
	 * 流程节点等待配置的属性名称
	 */
	public static final String WAITTYPE = "waitType";

	/**
	 * 流程定义中节点类型属性的名称
	 */
	public static final String NODETYPE = "nodeType";
	
	/**
	 * 流程定义中节点systemId属性的名称
	 */
	public static final String NODEPATH = "sysNodePath";
	/**
	 * 节点是否支持循环key
	 */
	public static final String FOR_END = "forEnd";

	/**
	 * 任务类型key
	 */
	public static final String NODEPROP_REFID = "sysNodeId";

	/**
	 * 第三方服务调用类型key
	 */
	public static final String CALL_TYPE = "callType";

	/**
	 * 第三方服务调用方式key
	 */
	public static final String CALL_MODE = "callMode";

	// ============================================流程定义属性选择值  分类============================================//

	/**
	 * 流程节点等待属性配置值--等待
	 */
	public static final String WAITTYPE_WAIT = "wait";

	/**
	 * 流程节点等待属性配置值--不等待
	 */
	public static final String WAITTYPE_NOWAIT = "nowait";

	/**
	 * 消息通知类型-短信通知
	 */
	public static final String MSGTYPE_SMS = "sms";

	/**
	 * 消息通知类型-所有方式都通知
	 */
	public static final String MSGTYPE_ALL = "all";

	/**
	 * 消息通知类型-邮件通知
	 */
	public static final String MSGTYPE_EMAIL = "email";
	/**
	 * 消息通知类型-无需通知
	 */
	public static final String MSGTYPE_NO = "no";
	
	/**
     * 流程状态 WORK_STATE 0 未完成,1 已结束,2 挂起
     */
	public static final String WORK_STATE_HUNG="2";
	

	 /**
     * 流程状态 WORK_STATE 0 未完成,1 已结束,2 挂起
     */
	public static final String WORK_STATE_END="1";
	
    /**
     * 流程状态 WORK_STATE 0 未完成,1 已结束,2 挂起
     */
	public static final String WORK_STATE_RUNNING="0";
	
	/**
	 * 任务状态 0 未完成,1 已结束,2 挂起, 3取消
	 */
	public static final String TASK_STATE_RUNING="0";
	
	/**
	 * 任务状态 0 未完成,1 已结束,2 挂起, 3取消
	 */
	public static final String TASK_STATE_END="1";
	
	/**
	 * 任务状态 0 未完成,1 已结束,2 挂起, 3取消
	 */
	public static final String TASK_STATE_HUNG="2";
	
	/**
	 * 任务状态 0 未完成,1 已结束,2 挂起, 3取消
	 */
	public static final String TASK_STATE_CANCEL="3";
	
	/**
	 * 操作的对象 0 流程实例  1-任务实例
	 */
	public static final String OPERATION_TYPE_PROCESS="0";
	
	/**
	 * 操作的对象 0 流程实例  1-任务实例
	 */
    public static final String OPERATION_TYPE_TASK="1";
    
    
    /**
     * 流程活动
     */
    public static final String PROCESS_STATE_ACTIVITY="0";
    /**
     * 流程挂起
     */
    public static final String PROCESS_STATE_HUNG="1";
    
 // ============================================扩展属性配置类型  分类============================================//
	/**
	 * FILED_TYPE_NOMEAL,基本格式
	 */
	public  static final String FILED_TYPE_NOMEAL="1";

	/**
	 * FILED_TYPE_URULE,规则类型
	 */
	public  static final String FILED_TYPE_URULE="2";
	
	/**
	 * FILED_TYPE_JS,js脚本
	 */
	public  static final String FILED_TYPE_JS="3";
	// ============================================其他  分类============================================//

    /**
     * 流程流转变量存储的key	
     */
    public static final String ROUTE_CONDITION_KEY="ROUTE_CONDITION_KEY";
    
    /**
     * 系统设置的流程流转变量存储的key	
     */
    public static final String ROUTE_CONDITION_KEY_SYS="ROUTE_CONDITION_KEY_SYS";
    
    /**
     * 流程结束方式存储的key
     */
    public static final String COMPLETE_TYPE_KEY="complete_type_key";
  
    /**
     * 流程结束方式 --正常运行
     */
    public static final String COMPLETE_TYPE_ACTIVITY="0";
    /**
     * 流程结束方式 --挂起
     */
    public static final String COMPLETE_TYPE_HUNG="1";
    /**
     * 流程结束方式 --正常结束
     */
    public static final String COMPLETE_TYPE_END="2";
    /**
     * 流程结束方式 --异常结束
     */
    public static final String COMPLETE_TYPE_ERROR="3";
    
    /**
     * 流程结束方式 --超时终止结束
     */
    public static final String COMPLETE_TYPE_TIMEEND="4";
  
    /**
     * 流程结束方式 --取消
     */
    public static final String COMPLETE_TYPE_CANCLE="5";
    
   
    
    /**
	 * 流程流转类型：发起人取消流程实例
	 */
	public static final String TRANTYPE_ASKDEL = "A0";

    /**
	 * 流程流转类型：发起人强制取消流程实例
	 */
	public static final String TRANTYPE_DEL = "M8";

	/**
	 * 流程流转类型：撤回
	 */
	public static final String TRANTYPE_UNDO = "A1";
	
	/**
	 * 流程流转类型：最后意见修改
	 */
	public static final String TRANTYPE_CHANGE = "A2";
	/**
	 * 流程流转类型：附件补充提交
	 */
	public static final String TRANTYPE_FILEADD = "A3";
	/**
	 * 流程流转类型：附件申请撤销
	 */
	public static final String TRANTYPE_FILEDEL = "A4";
	/**
	 * 任务拒绝，拒绝转办的任务
	 */
	public static final String TRANTYPE_REJECT = "B1";
	/**
	 * 流程流转类型：任务转办（将本人的任务转给其他人办理）
	 */
	public static final String TRANTYPE_TRANSER = "B2";
	/**
	 * 流程流转类型：任务驳回（打回）到指定节点
	 */
	public static final String TRANTYPE_REFUSE = "B3";
	/**
	 * 流程流转类型：驳回（打回）至上一节点
	 */
	public static final String TRANTYPE_REJECT_ONESTEP = "B0";
	
	/**
	 * 流程流转类型：驳回（打回）至首节点
	 */
	public static final String TRANTYPE_REJECT_BEGIN = "B4";
	
	/**
	 * 流程流转类型：M0-取消指定人员的节点任务，自动进行下一步处理
	 */
	public static final String TRANTYPE_CANCEL = "M0";
	/**
	 * 流程流转类型：挂起流程实例
	 */
	public static final String TRANTYPE_HANGUP = "M1";
	/**
	 * 流程流转类型：终止流程实例
	 */
	public static final String TRANTYPE_DELETE = "M2";
	
	/**
	 * 流程流转类型：自由流转
	 */
	public static final String TRANTYPE_FREE = "M3";
	/**
	 * 流程流转类型：重启流程实例(挂起实例重新启动)
	 */
	public static final String TRANTYPE_HANGDOWN = "M4";
	/**
	 * 流程流转类型：更改处理人员
	 */
	public static final String TRANTYPE_USERCHG = "M5";
	/**
	 * 流程流转类型：任务催办，催办不限定超时时间
	 */
	public static final String TRANTYPE_PROMPT = "M6";
	/**
	 * 流程流转类型：更改超时截止时间
	 */
	public static final String TRANTYPE_TIMECHG = "M7";
	
	/**
	 * 时间格式
	 */
	public static final String DATATIME_FORMMATE = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 日期格式
	 */
	public static final String DATA_FORMMATE = "yyyy-MM-dd";

	/**
	 * 会签任务的审批结果标识
	 */
	public static final String SINGLASTTASKRESULT = "signTaskResult";

	/**
	 * 流程定义的扩展属性的命名命名空间
	 */
	public static final String XMLNAMESPACE = "http://activiti.org/bpmn";

	/**
	 * 前一任务执行人
	 */
	public static final String BEFOR_USERID = "beforUserId";

	/**
	 * 前一任务Id
	 */
	public static final String BEFOR_TASKID = "beforTaskId";
	
	/**
	 * 根流程Id
	 */
	public static final String ROOT_PROCESS_ID = "ROOT_PROCESS_ID";

	/**
	 * 根流程code
	 */
	public static final String ROOT_PROCESS_CODE = "ROOT_PROCESS_CODE";
	
	/**
	 * 根流程name
	 */
	public static final String ROOT_PROCESS_NAME = "ROOT_PROCESS_NAME";
	
	/**
	 * 上一执行人信息
	 */
	public static final String BEFORUSERDATA = "beforUserData";

	/**
	 * 选人为空时，是否跳过当前节点
	 */
	public static final String IS_NO_PERSION_SKIP = "isblock";

	/**
	 * 待保存的关键业务参数
	 */
	public static final String BUSINESS_SAVE = "bussinessSave";

	/**
	 * 按照设定的时间，定时生成任务
	 */
	public static final String NEXT_EXEC_TIME = "nextExecTime";

	public static String START_TIME="startTime";

	/**
	 * 编码格式
	 */
	public static final String UTF_8 = "UTF-8";

	/**
	 * urule请求地址
	 */
	public static final String URULE_SERVER = "urule.service";

	public static final String SERVICE = "Service";

	/**
	 * 第三方系统请求地址
	 */
	public static final String BUSINESS_SERVER = "business.service";
	
	/**
	 * 配置的行方的请求报文头
	 */
	public static final String QUERY_TERLLER_BYRULE_HEADER = "urule.query.header";

	/**
	 * 请求超时设置
	 */
	public static final int HTTP_TIME_OUT = 30000;

	public static String BACK_INVOKE_SERVICE_TIMEOUT = "backInvokeTimeOut";
	public static String INVOKE_SERVICE_RSPSUCC = "S";
	public static String INVOKE_SERVICE_RSPFAIL = "E";

	public static String SEVER_IP_ADDR = "severIpAddr";

	public static String ENDWORKFLWOW_USERID = "endworkflow_userid";
	public static String BACK_INVOKE_PROTOCOL_HTTP = "http";

	/**
	 * 流程变量中保存taskID的key值
	 */
	public static final String NODE_TASKID = "wf_initTaskId";

	/**
	 * 获取报文类型
	 */
	public static final String MSG_BANK_TYPE = "bank.msg.type";

	public static final String URULE_DOUSER_PROJECT_NAME = "urule.douser.projectname";
	public static final String URULE_DOUSER_ORGLEVEL = "urule.douser.orglevel";

	public static final String BACKINVOKE_SERVICECODE = "backInvokeServiceCode";

	public static String MAIL_SMTP_HOST = "mail.smtp.host";
	public static String MAIL_USER = "mail.userName";
	public static String MAIL_PASSWORD = "mail.password";
	public static String MAIL_AUTH = "mail.smtp.auth";
	public static String PROCESS_CTRL="processCtrl";
	public static String TASk_AUTO_ACTOR="auto.doactor";

	/**
	 * 上一审批状态
	 */
	public static String DO_RESAULT="doResult";

	/**
	 * 上一审批阶段
	 */
	public static String LAST_TASK_NAME="lastTaskName";

	/**
	 * 当前审批阶段
	 */
	public static String TASK_NAME="taskName";

	/**
	 * 当前任务成员
	 */
	public static String TASK_USERS="taskUsers";

	public static Object MESSAGE_FLAG="messageFlag";

	/**
	 * 催办状态
	 */
	public static final String TASK_STATE_NOTYFY="1";

	
	/**
	 * 选人为空时，规则引擎启动一个分配人员的流程，这是流程的编码key
	 */
	public static final String QUERY_RELATION_TELLER_FAIL_FLOW_CODE = "flowcode";

	public static final String ASSIGN_TASK_ID = "assignTaskId";
	
	
    /**
     * EVENT_P1,流程启动触发事件
     */
    public  static final String EVENT_P1="EVENT_P1";
    /**
     * EVENT_P2,流程正常结束触发事件
     */
    public  static final String EVENT_P2="EVENT_P2";
    /**
     * EVENT_P3,流程异常结束触发事件
     */
    public  static final String EVENT_P3="EVENT_P3";
    /**
     * EVENT_P4,挂起触发调研
     */
    public  static final String EVENT_P4="EVENT_P4";
    /**
     * EVENT_P5,挂起恢复触发调用
     */
    public  static final String EVENT_P5="EVENT_P5";
    /**
     * EVENT_P6,终止取消触发调用
     */
    public  static final String EVENT_P6="EVENT_P6";
    /**
     * EVENT_T0,自动服务调用
     */
    public  static final String EVENT_T0="EVENT_T0";
    /**
     * EVENT_T1,任务生成事件
     */
    public  static final String EVENT_T1="EVENT_T1";
    /**
     * EVENT_T2,任务完成事件
     */
    public  static final String EVENT_T2="EVENT_T2";
    /**
     * EVENT_T3,任务回退事件
     */
    public  static final String EVENT_T3="EVENT_T3";
    /**
     * EVENT_T4,任务撤回事件
     */
    public  static final String EVENT_T4="EVENT_T4";
    /**
     * EVENT_P7,超时事件
     */
    public  static final String EVENT_T5="EVENT_T5";
    /***
     * 参数分类类型:业务数据比较参数类型
     */
    public static final String PARAM_TYPE_C = "C0";
    /***
     * 参数分类类型:流程编码归属事项参数类型
     */
    public static final String PARAM_TYPE_P = "P0";
    
    /***
     * 参数分类类型:流程编码查找类型
     */
    public static final String PARAM_TYPE_PROCESSCODE = "F0";
    
    
    /***
     * 参数分类类型:大类
     */
    public static final String PARAM_TYPE_B = "10";
    /***
     * 参数分类类型:小类
     */
    public static final String PARAM_TYPE_L = "01";
    
    /***
     * 参数缺省分类类型,00-其他
     */
    public static final String PARAM_TYPE_DEFAULT = "00";
    
    /***
     * 参数缺省分类类型,00-其他
     */
    public static final String PARAM_TYPE_CACHE_REFRESH = "CA";
    
    /***
     * 参数缺省分类名称,00-其他
     */
    public static final String PARAM_TYPE_DEFNAME = "其他";
    
    
    /***
     * 系统分类编码    内部使用的system_code编码,因systemIdCode必需原样返回
     */
    public static final String SYSTEM_CODE_KEY = "system_Code";
    
    /***
     * 系统分类编码   大类编码
     */
    public static final String SYSTEM_CODE_ITEM_TYPE = "itemType";
    
    /***
     * 系统分类编码   小类编码
     */
    public static final String SYSTEM_CODE_ITEM_NODE = "itemNode";
    /***
     * 系统分类编码   小类编码名称
     */
    public static final String SYSTEM_CODE_ITEM_NAME = "itemName";
    
    public static final String QUERY_INDEX_1 = "query_Indexa";
    public static final String QUERY_INDEX_2 = "query_Indexb";
    public static final String QUERY_INDEX_3 = "query_Indexc";
    public static final String QUERY_INDEX_4 = "query_Indexd";
    public static final String QUERY_INDEX_5 = "query_Indexe";
    public static final String QUERY_INDEX_6 = "query_Indexf";
    public static final String QUERY_INDEX_7 = "query_Indexg";
    public static final String QUERY_INDEX_8 = "query_Indexh";

    public static final String BUSSINESS_KEY_COMPARE="_comPare_Info";
    
    public static final String QUERY_TASK_LIST_ITEM_COMPARE = "compareInfo";
    
    /**
     * 流程分支规则查询出的下一个节点的信息
     */
	public static final String ROUTE_SELECT_KEY = "flowParam";

	/**
     * 通用参数配置信息
     */
	public static final String PARAM_LARGE = "10"; // 事项大类
	public static final String PARAM_SMALL = "01"; // 事项小类
	public static final String PARAM_BUSINESS = "C0"; // 业务数据比较参数
	public static final String PARAM_PROCESS_CODE = "F0";// 事项小类的流程编码
	public static final String PARAM_CHANNEL_MAP = "H0";// 参数类型值 渠道映射类型
	public static final String PARAM_TIMEOUT = "G0";// 超时处理相关参数 :超时通知策略
	public static final String PARAM_MODIFY_USER = "M0";// 更换处理处理人流程配置
	public static final String PARAM_GLOBALEVENT="GE";//全局默认事件ID配置(转授权和流程流转通知（发起人）)

	public static final String PARAM_UPID = "0";
	// udpate by xuesw 20190524 通用参数类型整理和优化
	
	public static final String PARAM_TIMEOUT_TYPE= "timeoutType";//PARAM_CODE  超时通知类型
	public static final String PARAM_TIMEOUT_FUNC = "timeoutFunc";//PARAM_CODE  超时通知机制
	public static final String PARAM_TIMEOUT_TEMPLATE = "timeoutTemplate";//PARAM_CODE  超时通知模板
	public static final String PARAM_TIMEOUT_WARN_PRE_HOUR = "timeoutWarnPreHour";//PARAM_CODE  超时前多少小时进行预警通知
	
	public static final String PARAM_TIMEOUT_TYPE_AFTER= "timeoutAfter";//PARAM_OBJ_CODE  超时通知
	public static final String PARAM_TIMEOUT_TYPE_BEFORE= "timeoutBefore";//PARAM_OBJ_CODE  超时预警通知
	public static final String PARAM_TIMEOUT_TYPE_BEFORE_AFTER= "timeoutBeforeAfter";//PARAM_OBJ_CODE  超时通知+预警通知
	public static final String PARAM_TIMEOUT_TYPE_NONE= "none";//PARAM_OBJ_CODE  不通知
	
	public static final String PARAM_TIMEOUT_LASTEXEC_PARATYPE="GZ";//超时处理最后一次执行时间
	public static final String PARAM_TIMEOUT_LASTEXEC_ID="GZ0001";//超时处理最后一次执行时间 参数id

	public static final String TIME_OUT_STATE = "1";
	public static final String NOT_TIME_OUT_STATE = "0";
	
	public static final String TRAN_TYPE_KEY = "tranType";
	public static final String TEMPLATE_ID = "templateId";
	public static final String SMS_ADDRESS = "smsAddress";
	
	/**
	 * 同步调用
	 */
	public static final String CALL_MODE_SYNC = "1";
	/**
	 * 异步调用
	 */
	public static final String CALL_MODE_ASYNC = "0";
	/**
	 * 异步等待
	 */
	public static final String CALL_MODE_ASYNC_WAIT = "2";
	
	/**
	 * http调用
	 */
	public static final String CALL_TYPE_HTTP = "0";
	/**
	 *dubbo调用
	 */
	public static final String CALL_TYPE_DUBBO = "1";
	/**
	 * esb调用
	 */
	public static final String CALL_TYPE_ESB = "2";
	/**
	 * sofa调用
	 */
	public static final String CALL_TYPE_SOFA = "3";

	public static final String PROCESS_TASK_ID = "PROCESS_TASK_ID";

	public static final String AUTO_USER_ID = "autoNodeUserId";

	/**新增发起子流程的主流程中的上一处理处理人标识**/
	public static final String CHILD_START_USER_ID = "_childUserId";
	/**子流程编码**/
	public static final String CHILD_PROCESS_CODE = "childProcessCode";
	
	/**
	 * 流程实例状态
	 */
	public static final String NODE_STATE_VALUE="AAAAA";

	public static final String TASK_CATEGORY_ID = "taskCategoryId";
	
	public static final String TASK_CATEGORY_ID_PARENT = "taskCategoryIdParent";

	public static final int BUSS_MAN_FLOW_LEN = 23;

	/**
	 * 流程创建时间
	 */
	public static final String PROCESS_CREATE_TIME = "processCreateTime";

	public static final String TASK_RESULTS = "task.result";
	
	public static final String CALL_URULE_TYPE = "urule.interface.way";
	
	public static final String ROUTE_MESSAGE_KEY = "message";
	
	public static final String RESULT_DATA_MAP_KEY = "result_map";
	
	/**
	 * 报文头存入业务参数
	 */
	public static final String SYS_COMM_HEADER_IN_BIZ = "SYS_COMM_HEADER_IN_BIZ";
	
	/**
	 * 手工结束
	 */
	public static final String MANUAL_END_PROCESS = "manualEndProcess";
	
	
	public static final String START_PERSON =  "startPerson";

	/**
	 * 流程流转类型：任务检出
	 */
	public static final String TRAN_TYPE_CHECKOUT = "3";
	/**
	 * 流程流转类型：任务检入
	 */
	public static final String TRAN_TYPE_CHECKIN = "0";

	/**
	 * 只允许检出不允许检入
	 */
	public static final String TRAN_TYPE_CHECKOUT_NOTIN = "2";

	/**
	 * 代检出
	 */
	public static final String TRAN_TYPE_UNDO_CHECKOUT = "1";

	public static final String CALL_MODE_SYNC_WAIT = "3";

    /**
     * 
     */
	public static final String IS_IGNORE_PROCESS_VERSION = "isIgnoreProcessVersion";
	
	public static final String SIGN_USER_LIST = "signUserlist";

	public static int syncFlag_check_ready = 0;
	public static int syncFlag_check_timeout = 1;

	public static String[] use_db_process_def_properties = new String[] { "waitType", "nodeType", "sysNodePath",
			"isblock", "checkPer", "confirInfo", "personPro", "timeOut", "timeFlowFlag", "sysNodeId",
			"subFlowSelectRule", "startPerson", "jointlySelect", "jointlyPercent", "jointlyManage", "routeRule" };

	/**
	 * 指定的目标任务ID,待领用的未处理任务列表信息
	 */
	public static final String TASK_ID_LIST = "taskIdList";

    /**
     * 
     */
	public static final String SHOW_PROCESS_URL = "showProcessUrl";

	/**
	 * 节点循环支持存储对象 taskLoopCount{taskDefId:count}
	 */
	public static final String TASK_LOOP_RESULT = "taskLoopResult";
	public static final String TASK_LOOP_COUNT = "taskLoopCount";

	public static final String TASK_DEF_ID = "taskDefId";

	public static final String CALL_TYPE_SPRINGCLOUD = "1";
	/**
	 * 若specifyNextOpers非空,
	 * specifyNextOpersList为空时，此时系统会根据此上送的信息，调用公共选人服务接口去获取相关的信息；
	 * 若specifyNextOpers为空,
	 * specifyNextOpersList非空时，此时系统将不再调用公共选人服务接口获取对应的信息，而是直接取此上送的内容作为对应的可处理人员信息；
	 * 注意：若上送 specifyNextOpersList时，若对应的任务节点需要发送通知信息，则相应的用户电话等信息必须要上送，否则将无法发送通知
	 */
	public static final String SPECIFY_NEXT_OPERS_LIST = "specifyNextOpersList";
	public static final String LOCK_HOST = "127.0.0.1";
	/**
	 * 任务类型-转办
	 */
	public static final Long WORK_TYPE_TRANSFER_OUT = 2L; 
	/**
	 * 
	 * 任务类型-动态加签
	 */
	public static final Long WORK_TYPE_ADDITION_TASK = 3L;


	/**
	 * 任务类型-打回
	 */
	public static final Long WORK_TYPE_REFULSE_TASK = 4L;

	/**
	 * 任务类型-撤回
	 */
	public static final Long WORK_TYPE_RECOVER_TASK = 5L;
	/**
	 * 任务类型-动态加签通知
	 */
	public static final Long WORK_TYPE_ADDITION_TASKNOTIF = 6L;
	/**
	 * 任务类型-分配任务
	 */
	public static final Long WORK_TYPE_ALLOT_TASK = 7L;

	/**
	 * 任务类型-超时任务回收
	 */
	public static final Long WORK_TYPE_RECOVERY = 8L;
	/**
	 * 任务类型-领用任务
	 */
	public static final Long WORK_TYPE_CHECK_OUT = 9L;
	
	/**
	 * 任务类型-自动分配
	 */
	public static final Long WORK_TYPE_AUTO =10L ;

	
	/**
	 * 代办任务查看状态-1已经查看
	 */
	public static final String IS_VIEW_TASK = "1";
	/**
	 * 代办任务查看状态-0未查看
	 */
	public static final String IS_NO_VIEW_TASK = "0";

	/**
	 * 任务池 0-非任务池任务
	 */
	public static final String IS_NO_TASK_POOL = "0";
	/**
	 * 任务池 1-任务池任务
	 */
	public static final String IS_TASK_POOL = "1";
	/**
	 * 任务池 2-任务池任务历史
	 */
	public static final String IS_HIST_TASK_POOL = "2";
	/**
	 * 最大检出数
	 */
	public static final String MAX_CHECK_NUM = "maxCheckOutNum";

	/**
	 * 是否可以领用
	 */
	public static final String CHECK_IN_FLAG = "checkInFlag";

	/**
	 * 强制分配
	 */
	public static final String BATCH_FORCE_ALLOT = "2";

	/**
	 * 批量领用
	 */
	public static final String BATCH_CHECK_OUT = "0";

	/**
	 * 超时提示-不霸屏
	 */
	public static final String TIMEOUT_TIP_NO_MALL = "1";

	/**
	 * 超时提示-霸屏
	 */
	public static final String TIMEOUT_TIP_FULLSCREEN = "2";

	/**
	 * 超时配置 时间
	 */
	public static final String TIMEOUT_DATA = "timeOut";

	/**
	 * 超时配置 时间间隔
	 */
	public static final String TIMEOUT_DATA_INTERVAL = "value";

	/**
	 * 超时配置时间类型：0 小时，1天
	 */
	public static final String TIMEOUT_DATA_TYPE = "type";

	/**
	 * 超时时间类型-天
	 */
	public static final String TIMEOUT_DATA_TYPE_DAY = "1";
	/**
	 * 超时时间类型-小时
	 */
	public static final String TIMEOUT_DATA_TYPE_HOURS = "0";

	/**
	 * 超时配置-规则
	 */
	public static final String TIMEOUT_URULE = "urule";
	

	/**
	 * 转办类型-转办
	 */
	public static final Integer TRANSFER_TYPE_TRANSF = 1; 
	/**
	 * 
	 * 转办类型-拒绝转办
	 */
	public static final Integer TRANSFER_TYPE_REFUSE_TRANSF = 2;

	/**
	 * 
	 * 转办类型-分配
	 */
	public static final Integer TRANSFER_TYPE_ALLOT = 3;

	/**
	 * 
	 * 转办类型-回收分配
	 */
	public static final Integer TRANSFER_TYPE_RECOVER = 4;

	public static final String TIMEOUT_TIP_NO = "0";

    /**
     * 分配
     */
    public static final String BACTH_NOMAL_ALLOT = "1";

	public static final String TRANTYPE_REJECT_CHECK = "A1";

	public static final String AUTO_ALLOT = "autoAllot";

	/**
	 * 自动-转办
	 */
	public static final String ACCEDIT_TYPE_AUTO_ALLOT = "2";

	/**
	 * 授权-追加
	 */
	public static final String ACCEDIT_TYPE_AUTHORIZE_ADD = "0"; 

	/**
	 * 授权-替换
	 */
	public static final String ACCEDIT_TYPE_AUTHORIZE_REPLACE = "1";

	/**
	 * 流程，节点属性类型--流程
	 */
	public static final String EXTENDS_OWNTYPE_DEF = "1";
	
	/**
	 * 流程，节点属性类型--节点
	 */
	public static final String EXTENDS_OWNTYPE_NODE = "0";
	
	public static final String PROCESS_ID_LIST = "processIdList";

	public static final String TIMEOUT_DATA_TYPE_MILL_P = "1";

	public static final String TIMEOUT_DATA_TYPE_MILL_T = "2";

	/**
	 * String-true
	 */
	public static final String STRING_TRUE = "true";

	/**
	 * String-TRUE
	 */
	public static final String STRING_TRUE_UPPER = "TRUE";

	/**
	 * 逗号
	 */
	public static final String COMMA = ",";
	/**
	 * 百分号
	 */
	public static final String PERCENTAGE = "%";

	/**
	 * null-字符串
	 */
	public static final String STR_NULL = "null";
	/**
	 * NULL-字符串
	 */
	public static final String STR_NULL_UPPER = "NULL";

	/**
	 * oracle
	 */
	public static final String ORACLE = "oracle";

	/**
	 * mysql
	 */
	public static final String MYSQL = "mysql";

	/**
	 * key
	 */
	public static final String KEY = "key";
	/**
	 * value
	 */
	public static final String VALUE = "value";

	/**
	 * 成功响应编码
	 */
	public static String RET_SUCC="000000";
	/**
	 * 失败响应编码
	 */
	public static String RET_FAIL="999999";
	/**
	 * 异常响应编码
	 */
	public static String RET_EXCEPT="444444";

	public static String HTTP_CONNECT_RSPCODE  ="rspCode";
	public static String HTTP_CONNECT_RSPHEAD  ="rspHead";
	public static String HTTP_CONNECT_RSPDATA  ="rspData";
	public static String HTTP_CONNECT_RSPSUCC  ="000000";
	public static String HTTP_CONNECT_RSPFAIL  ="999999";
	public static String HTTP_CONNECT_RspTimeOut  ="444444";

	/**
	 * 退回流程流程
	 */
	public static List<String> RETURN_CODE_LIST = Arrays.asList("4","7");

	/**
	 * 撤销
	 */
	public static String DO_RESULT_THREE = "3";
}
