package com.bpm.workflow.constant;

/**
 * @Description: 日期常量类
 */
public class DateConstants {

    /**
     * 秒
     */
    public static final String SECOND = "秒";
    /**
     * 分
     */
    public static final String MINUTE = "分";
    /**
     * 时
     */
    public static final String HOUR = "时";
    /**
     * 天
     */
    public static final String DAY = "天";
}
