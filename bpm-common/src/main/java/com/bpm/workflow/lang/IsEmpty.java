package com.bpm.workflow.lang;

/**
 * @description 
 * @author liuc
 * @date 2021/8/24 11:07
 * @since JDK1.8
 * @version V1.0
 */
public interface IsEmpty {
    boolean isEmpty();
}
