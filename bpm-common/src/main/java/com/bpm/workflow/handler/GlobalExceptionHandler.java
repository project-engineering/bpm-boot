package com.bpm.workflow.handler;

import com.bpm.workflow.code.Code;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.apache.xmlbeans.impl.piccolo.util.DuplicateKeyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import java.net.BindException;

/**
 * 全局异常处理类
 * @RestControllerAdvice(@ControllerAdvice)，拦截异常并统一处理
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理自定义的业务异常
     * @param e	异常对象
     * @param
     * @return	错误结果
     */
    @ResponseBody
    @ExceptionHandler(ServiceException.class)
    public Result ServiceExceptionHandler(ServiceException e) {
        log.error("发生业务异常！原因是: {}", e.getMessage());
        return Result.fail(e.getCode(), e.getMessage());
    }

    /**
     * 拦截抛出的异常，@ResponseStatus：用来改变响应状态码
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public Result handlerThrowable(Throwable e) {
        log.error("发生未知异常！原因是: ", e);
        Result error = Result.fail(Code.SYSTEM_ERROR.getCode(), e.getMessage());
        return error;
    }

    /**
     * 处理绑定异常
     */
    @ExceptionHandler(BindException.class)
    public Result handleBindExcpetion(BindException e) {
        log.error("发生参数校验异常！原因是：",e);
        Result error = Result.fail(Code.CLIENT_PATH_VARIABLE_ERROR.getCode(), e.getMessage());
        return error;
    }

    /**
     * 处理参数校验异常
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("发生参数校验异常！原因是：",e);
        Result error = Result.fail(Code.CLIENT_REQUEST_BODY_CHECK_ERROR.getCode(),e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        return error;
    }

    /**
     * 处理未知异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Result exceptionHandler(Exception e) {
        log.error("发生未知异常！原因是：",e);
        Result error = Result.fail(Code.UNKUOW_ERROR.getCode(),e.getMessage());
        return error;
    }

    /**
     * 处理请求方法不支持异常
     * @param e
     * @return
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result handleMethodArgumentNotValidException(HttpRequestMethodNotSupportedException e) {
        log.error("请求方法不支持,原因是：{}",e.getMessage());
        Result error = Result.fail(Code.CLIENT_HTTP_METHOD_ERROR.getCode(),e.getMessage());
        return error;
    }

    /**
     * 请求的路径与任何注册的处理程序都不匹配
     * @param e
     * @return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public Result handlerNoFoundException(Exception e) {
        log.error(e.getMessage(), e);
        return Result.fail(Code.UNABLE_FIND_RESOURCES.getCode(), "路径不存在，请检查路径是否正确");
    }

    /**
     * 处理重复键异常
     * @param e
     * @return
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public Result handleDuplicateKeyException(DuplicateKeyException e){
        log.error(e.getMessage(), e);
        return Result.fail("主键冲突，请检查数据是否重复");
    }

    /**
     * 请求路径中缺少必需的路径变量
     */
    @ExceptionHandler(MissingPathVariableException.class)
    public Result handleMissingPathVariableException(MissingPathVariableException e, HttpServletRequest request) {
        log.error("请求路径中缺少必需的路径变量：path={}，error={}", request.getRequestURI(), e.getMessage());
        return Result.fail(String.format("请求路径中缺少必需的路径变量[%s]", e.getVariableName()));
    }

    /**
     * 客户端@RequestBody请求体JSON格式错误或字段类型错误
     * @param e
     * @return
     */
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Result handle(HttpMessageNotReadableException e) {
        log.error("[客户端请求体JSON格式错误或字段类型不匹配]", e);
        return Result.fail(Code.CLIENT_REQUEST_BODY_FORMAT_ERROR.getCode(), "客户端请求体JSON格式错误或字段类型不匹配");
    }

    /**
     * 客户端@RequestParam参数校验不通过
     * 主要是未能通过Hibernate Validator校验的异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result handle(ConstraintViolationException e) {
        log.error("[客户端请求参数校验不通过]", e);
        return Result.fail(Code.CLIENT_REQUEST_PARAM_PARSE_ERROR.getCode(), e.getMessage());
    }

    /**
     * 客户端请求缺少必填的参数
     * @param e
     * @return
     */
    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public Result handle(MissingServletRequestParameterException e) {
        log.error("[客户端请求缺少必填的参数]", e);
        String errorMsg = null;
        String parameterName = e.getParameterName();
        if (!"".equals(parameterName)) {
            errorMsg = parameterName + "不能为空";
        }
        if (errorMsg != null) {
            return Result.fail(Code.CLIENT_REQUEST_PARAM_REQUIRED_ERROR.getCode(), errorMsg);
        }
        return Result.fail(Code.CLIENT_REQUEST_PARAM_REQUIRED_ERROR.getCode(), "请求参数不能为空");
    }

    /**
     * 通用的业务方法入参检查错误
     * @param e
     * @return
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    public Result handle(IllegalArgumentException e) {
        log.error("[客户端请求参数格式错误]", e);
        return Result.fail(Code.SERVER_ILLEGAL_ARGUMENT_ERROR.getCode(), e.getMessage());
    }

}
