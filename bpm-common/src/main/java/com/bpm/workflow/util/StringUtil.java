package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;

/**
 * @Description: 字符串工具类
 */
public class StringUtil {

    public static String substringBefore(String str, String separator) {
        if (StrUtil.isNotEmpty(str) && separator != null) {
            if (separator.isEmpty()) {
                return "";
            } else {
                int pos = str.indexOf(separator);
                return pos == -1 ? str : str.substring(0, pos);
            }
        } else {
            return str;
        }
    }

    public static int indexOf(String str, String index) {
        return str != null && index != null ? str.indexOf(index) : -1;
    }
}
