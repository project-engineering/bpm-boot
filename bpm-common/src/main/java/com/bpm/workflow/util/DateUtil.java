package com.bpm.workflow.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.DateConstants;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.util.regex.RegexUtil;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.*;
import java.util.*;

/**
 * 日期工具类
 * @author liuc
 * @date 2023/12/16 14:48
 */
public class DateUtil {
    private static final Logger log = LoggerFactory.getLogger(DateUtil.class);

    private static Map<String, ThreadLocal<SimpleDateFormat>> SDF_MAP = new HashMap<>(16);
    public static Object LOCK_OBJ = new Object();
    /**
     * 考虑港股和美股 采用GMT-1时区来确定报表日 即T日的报表包含北京时间T日9时至T+1日9时的数据
     */
    public static ZoneId TIMEZONE_GMT_1 = ZoneId.of("GMT-1");
    public static ZoneId TIMEZONE_EST = ZoneId.of("US/Eastern");
    public static ZoneId TIMEZONE_GMT8 = ZoneId.of("GMT+8");

    /**
     * 常用时间转换格式
     */
    public static String DATE_FORMATTER_1 = "yyyy";
    public static String DATE_FORMATTER_2 = "yyyy-MM";
    public static String DATE_FORMATTER_3 = "yyyy-MM-dd";
    public static String DATE_FORMATTER_4 = "yyyy-MM-dd HH:mm";
    public static String DATE_FORMATTER_5 = "yyyy-MM-dd HH:mm:ss";
    public static String DATE_FORMATTER_6 = "yyyy-MM-dd HH:mm:ss.S";
    public static String DATE_FORMATTER_7 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static String DATE_FORMATTER_8 = "yyyy-MM-dd HH:mm:ss.SSSSSSS";
    public static String DATE_FORMATTER_9 = "yyyyMM";
    public static String DATE_FORMATTER_10 = "yyyyMMdd";
    public static String DATE_FORMATTER_11 = "yyyyMMddHHmmss";
    public static String DATE_FORMATTER_12 = "yyyyMMdd HHmmss";
    public static String DATE_FORMATTER_13 = "yyyyMMdd HHmmssSSS";
    public static String DATE_FORMATTER_14 = "yyyyMMddHHmmssSSS";
    public static String DATE_FORMATTER_15 = "yyyy-MM-dd'T'HH:mm:ss.SSSz";
    public static String DATE_FORMATTER_16 = "yyyy.MM";
    public static String DATE_FORMATTER_17 = "yyyy.MM.dd";
    public static String DATE_FORMATTER_18 = "yyyy.MM.dd HH:mm";
    public static String DATE_FORMATTER_19 = "yyyy.MM.dd HH:mm:ss";
    public static String DATE_FORMATTER_20 = "yyyy.MM.dd HH:mm:ss.S";
    public static String DATE_FORMATTER_21 = "yyyy.MM.dd HH:mm:ss.SSS";
    public static String DATE_FORMATTER_22 = "yyyy.MM.dd HH:mm:ss.SSSSSSS";
    public static String DATE_FORMATTER_23 = "yyyy/MM";
    public static String DATE_FORMATTER_24 = "yyyy/MM/dd";
    public static String DATE_FORMATTER_25 = "yyyy/MM/dd HH:mm";
    public static String DATE_FORMATTER_26 = "yyyy/MM/dd HH:mm:ss";
    public static String DATE_FORMATTER_27 = "yyyy/MM/dd HH:mm:ss.S";
    public static String DATE_FORMATTER_28 = "yyyy/MM/dd HH:mm:ss.SSS";
    public static String DATE_FORMATTER_29 = "yyyy/MM/dd HH:mm:ss.SSSSSSS";
    public static String DATE_FORMATTER_30 = "yyyy年";
    public static String DATE_FORMATTER_31 = "yyyy年MM月";
    public static String DATE_FORMATTER_32 = "yyyy年MM月dd日";
    public static String DATE_FORMATTER_33 = "yyyy年MM月dd日 HH时mm分";
    public static String DATE_FORMATTER_34 = "yyyy年MM月dd日 HH时mm分ss秒";
    public static String DATE_FORMATTER_35 = "HH:mm:ss";
    public static String DATE_FORMATTER_36 = "HHmmss";
    public static String DATE_FORMATTER_37 = "HHmmssSSS";

    @SuppressWarnings("serial")
    protected static Map<String, DateTimeFormatter> DATE_TIME_FORMAT_MAP = new Hashtable<String, DateTimeFormatter>() {
        {
            put(DATE_FORMATTER_1, DateTimeFormatter.ofPattern(DATE_FORMATTER_1));
            put(DATE_FORMATTER_2, DateTimeFormatter.ofPattern(DATE_FORMATTER_2));
            put(DATE_FORMATTER_3, DateTimeFormatter.ofPattern(DATE_FORMATTER_3));
            put(DATE_FORMATTER_4, DateTimeFormatter.ofPattern(DATE_FORMATTER_4));
            put(DATE_FORMATTER_5, DateTimeFormatter.ofPattern(DATE_FORMATTER_5));
            put(DATE_FORMATTER_6, DateTimeFormatter.ofPattern(DATE_FORMATTER_6));
            put(DATE_FORMATTER_7, DateTimeFormatter.ofPattern(DATE_FORMATTER_7));
            put(DATE_FORMATTER_8, DateTimeFormatter.ofPattern(DATE_FORMATTER_8));
            put(DATE_FORMATTER_9, DateTimeFormatter.ofPattern(DATE_FORMATTER_9));
            put(DATE_FORMATTER_10, DateTimeFormatter.ofPattern(DATE_FORMATTER_10));
            put(DATE_FORMATTER_11, DateTimeFormatter.ofPattern(DATE_FORMATTER_11));
            put(DATE_FORMATTER_12, DateTimeFormatter.ofPattern(DATE_FORMATTER_12));
            put(DATE_FORMATTER_13, DateTimeFormatter.ofPattern(DATE_FORMATTER_13));
            put(DATE_FORMATTER_14, DateTimeFormatter.ofPattern(DATE_FORMATTER_14));
            put(DATE_FORMATTER_15, DateTimeFormatter.ofPattern(DATE_FORMATTER_15));
            put(DATE_FORMATTER_16, DateTimeFormatter.ofPattern(DATE_FORMATTER_16));
            put(DATE_FORMATTER_17, DateTimeFormatter.ofPattern(DATE_FORMATTER_17));
            put(DATE_FORMATTER_18, DateTimeFormatter.ofPattern(DATE_FORMATTER_18));
            put(DATE_FORMATTER_19, DateTimeFormatter.ofPattern(DATE_FORMATTER_19));
            put(DATE_FORMATTER_20, DateTimeFormatter.ofPattern(DATE_FORMATTER_20));
            put(DATE_FORMATTER_21, DateTimeFormatter.ofPattern(DATE_FORMATTER_21));
            put(DATE_FORMATTER_22, DateTimeFormatter.ofPattern(DATE_FORMATTER_22));
            put(DATE_FORMATTER_23, DateTimeFormatter.ofPattern(DATE_FORMATTER_23));
            put(DATE_FORMATTER_24, DateTimeFormatter.ofPattern(DATE_FORMATTER_24));
            put(DATE_FORMATTER_25, DateTimeFormatter.ofPattern(DATE_FORMATTER_25));
            put(DATE_FORMATTER_26, DateTimeFormatter.ofPattern(DATE_FORMATTER_26));
            put(DATE_FORMATTER_27, DateTimeFormatter.ofPattern(DATE_FORMATTER_27));
            put(DATE_FORMATTER_28, DateTimeFormatter.ofPattern(DATE_FORMATTER_28));
            put(DATE_FORMATTER_29, DateTimeFormatter.ofPattern(DATE_FORMATTER_29));
            put(DATE_FORMATTER_30, DateTimeFormatter.ofPattern(DATE_FORMATTER_30));
            put(DATE_FORMATTER_31, DateTimeFormatter.ofPattern(DATE_FORMATTER_31));
            put(DATE_FORMATTER_32, DateTimeFormatter.ofPattern(DATE_FORMATTER_32));
            put(DATE_FORMATTER_33, DateTimeFormatter.ofPattern(DATE_FORMATTER_33));
            put(DATE_FORMATTER_34, DateTimeFormatter.ofPattern(DATE_FORMATTER_34));
            put(DATE_FORMATTER_35, DateTimeFormatter.ofPattern(DATE_FORMATTER_35));
            put(DATE_FORMATTER_36, DateTimeFormatter.ofPattern(DATE_FORMATTER_36));
            put(DATE_FORMATTER_37, DateTimeFormatter.ofPattern(DATE_FORMATTER_37));
        }
    };

    /**
     * 获取日期格式
     * @param date 字符串
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/16 19:54
     * @throws
     */
    public static String getDateStr(String date) {
        String dateStr = null;
        int len = date.length();
        if (UtilValidate.areEqual(len, DATE_FORMATTER_2.length())) {
            dateStr = DATE_FORMATTER_2;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_10.length())) {
            dateStr = DATE_FORMATTER_10;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_3.length())) {
            if (date.contains("-")) {
                dateStr = DATE_FORMATTER_3;
            }
            if (date.contains("/")) {
                dateStr = DATE_FORMATTER_24;
            }
            if (date.contains(".")) {
                dateStr = DATE_FORMATTER_17;
            }
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_6.length())) {
            dateStr = DATE_FORMATTER_6;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_11.length())) {
            dateStr = DATE_FORMATTER_11;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_14.length())) {
            dateStr = DATE_FORMATTER_14;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_7.length())) {
            dateStr = DATE_FORMATTER_7;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_8.length())) {
            //yyyy-MM-dd HH:mm:ss:SSSSSSS格式的日期按yyyy-MM-dd HH:mm:ss:SSS格式来处理
            dateStr = DATE_FORMATTER_7;
        }else if (UtilValidate.areEqual(len, DATE_FORMATTER_5.length())) {
            dateStr = DATE_FORMATTER_5;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_12.length())) {
            dateStr = DATE_FORMATTER_12;
        } else if (UtilValidate.areEqual(len, DATE_FORMATTER_4.length())) {
            dateStr = DATE_FORMATTER_4;
        }
        return dateStr;
    }

    private static SimpleDateFormat getSdf(String pattern) {
        ThreadLocal<SimpleDateFormat> tl = SDF_MAP.get(pattern);
        if (UtilValidate.isEmpty(tl)) {
            synchronized (LOCK_OBJ) {
                tl = SDF_MAP.get(pattern);
                if (UtilValidate.isEmpty(tl)) {
                    tl = new ThreadLocal<SimpleDateFormat>() {
                        @Override
                        protected SimpleDateFormat initialValue() {
                            return new SimpleDateFormat(pattern);
                        }
                    };
                    SDF_MAP.put(pattern, tl);
                }
            }
        }
        return tl.get();
    }

    public static String format(Date date, String pattern) {
        return getSdf(pattern).format(date);
    }

    /**
     * 根据format的格式获取相应的DateTimeFormatter对象
     *
     * @param format 时间转换格式字符串
     * @return java.time.format.DateTimeFormatter
     */
    public static DateTimeFormatter getDateTimeFormatter(String format) {
        if (DATE_TIME_FORMAT_MAP.containsKey(format)) {
            return DATE_TIME_FORMAT_MAP.get(format);
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            DATE_TIME_FORMAT_MAP.put(format, formatter);
            return formatter;
        }
    }

    /**
     * 获取本月的第一天，格式为自定义格式，默认格式为yyyy-MM-dd HH:mm:ss
     *
     * @return java.lang.String
     */
    public static String getFirstDayOfThisMonth(String pattern) {
        DateTimeFormatter formatter = null;
        if (UtilValidate.isEmpty(pattern)) {
            formatter = getDateTimeFormatter(DATE_FORMATTER_5);
        } else {
            formatter = getDateTimeFormatter(pattern);
        }
        LocalDateTime firstDayOfThisYear = getCurrLocalDateTime().with(TemporalAdjusters.firstDayOfMonth());
        return formatter.format(firstDayOfThisYear);
    }

    /**
     * 获取本月第N天，格式为自定义格式，默认格式为yyyy-MM-dd HH:mm:ss
     * @param pattern
     * @return java.lang.String
     */
    public static String getNdayOfThisMonth (int n ,String pattern) {
        DateTimeFormatter formatter = null;
        if (UtilValidate.isEmpty(n)) {
            throw new DateTimeException("Please enter the day of the month you want to get!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            formatter = getDateTimeFormatter(DATE_FORMATTER_5);
        } else {
            formatter = getDateTimeFormatter(pattern);
        }
        LocalDate secondDayOfThisMonth = LocalDate.now().withDayOfMonth(n);
        return formatter.format(secondDayOfThisMonth);
    }

    /**
     * 获取本月的最末天，格式为自定义格式，默认格式为yyyy-MM-dd HH:mm:ss
     *
     * @return java.lang.String
     */
    public static String getLastDayOfThisMonth(String pattern) {
        DateTimeFormatter formatter = null;
        if (UtilValidate.isEmpty(pattern)) {
            formatter = getDateTimeFormatter(DATE_FORMATTER_5);
        } else {
            formatter = getDateTimeFormatter(pattern);
        }
        LocalDateTime firstDayOfThisYear = getCurrLocalDateTime().with(TemporalAdjusters.lastDayOfMonth());
        return formatter.format(firstDayOfThisYear);
    }

    /**
     * 当前日期向后推多少天
     *
     * @param days
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime getLocalPlusDays(int days) {
        return getCurrLocalDateTime().plusDays(days);
    }

    /**
     * 当前日期向后推多少天,默认格式为yyyy-MM-dd
     *
     * @param days
     * @return java.lang.String
     */
    public static String getStringPlusDays (int days) {
        if (UtilValidate.isEmpty(days)) {
            throw new DateTimeException("Please enter the number of days you want to move back from the current date!");
        }
        return getStringPlusDays(days,DATE_FORMATTER_3);
    }

    /**
     * 当前日期向后推多少天，格式为自定义格式，默认格式为yyyy-MM-dd HH:mm:ss
     *
     * @param days
     * @param pattern
     * @return java.lang.String
     */
    public static String getStringPlusDays (int days ,String pattern) {
        DateTimeFormatter formatter;
        if (UtilValidate.isEmpty(days)) {
            throw new DateTimeException("Please enter the number of days you want to move back from the current date!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            formatter = getDateTimeFormatter(DATE_FORMATTER_5);
        } else {
            formatter = getDateTimeFormatter(pattern);
        }
        LocalDateTime localDateTime = getCurrLocalDateTime().plusDays(days);
        return formatter.format(localDateTime);
    }

    /**
     * 获取今天的00:00:00
     *
     * @return java.lang.String
     */
    public static String getDayStart() {
        return getDayStart(LocalDateTime.now());
    }

    /**
     * 获取今天的23:59:59
     *
     * @return java.lang.String
     */
    public static String getDayEnd() {
        return getDayEnd(LocalDateTime.now());
    }

    /**
     * 获取某天的00:00:00
     *
     * @param dateTime
     * @return java.lang.String
     */
    public static String getDayStart(LocalDateTime dateTime) {
        if (UtilValidate.isEmpty(dateTime)) {
            throw new DateTimeException("Please enter the localDateTime to be converted!");
        }
        return covertObjToString(dateTime.with(LocalTime.MIN));
    }

    /**
     * 获取某天的23:59:59
     *
     * @param dateTime
     * @return java.lang.String
     */
    public static String getDayEnd(LocalDateTime dateTime) {
        if (UtilValidate.isEmpty(dateTime)) {
            throw new DateTimeException("Please enter the localDateTime to be converted!");
        }
        return covertObjToString(dateTime.with(LocalTime.MAX));
    }

    /**
     * 获取本月第一天的00:00:00
     *
     * @return java.lang.String
     */
    public static String getFirstDayOfMonth() {
        return getFirstDayOfMonth(LocalDateTime.now());
    }

    /**
     * 获取本月最后一天的23:59:59
     *
     * @return java.lang.String
     */
    public static String getLastDayOfMonth() {
        return getLastDayOfMonth(LocalDateTime.now());
    }

    /**
     * 获取某月第一天的00:00:00
     *
     * @param dateTime
     *            LocalDateTime对象
     * @return java.lang.String
     */
    public static String getFirstDayOfMonth(LocalDateTime dateTime) {
        return covertObjToString(dateTime.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN));
    }

    /**
     * 获取某月最后一天的23:59:59
     *
     * @param dateTime
     *            LocalDateTime对象
     * @return java.lang.String
     */
    public static String getLastDayOfMonth(LocalDateTime dateTime) {
        return covertObjToString(dateTime.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX));
    }

    /**
     * 获取几个月前的第一天
     * 比如现在是2022-09-20，获取3个月前的第一天，那就是2022-06-01
     * @return
     */
    public static String getLastMonthStartDay(int num,String pattern){
        if (UtilValidate.isEmpty(num)) {
            throw new DateTimeException("Please enter the num parameters!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        LocalDate start =  LocalDate.now().minusMonths(num);
        LocalDate firstday = LocalDate.of(start.getYear(), start.getMonthValue(), 1);
        return covertObjToString(firstday,pattern);
    }

    /**
     * 获取上个月的最后一天
     * @return
     */
    public static String getLastMonthEndDay(String pattern){
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        // 月份
        LocalDate end = LocalDate.now().minusMonths(1);
        //上个月的最后一天
        LocalDate lastMonthDay = end.with(TemporalAdjusters.lastDayOfMonth());
        return covertObjToString(lastMonthDay,pattern);
    }

    /**
     * 当前时间减N个月
     * @return
     */
    public static String getCurrDateTimeSubNMonth(int n){
        return getCurrDateTimeSubNMonth(n,DateUtil.DATE_FORMATTER_5);
    }

    /**
     * 当前时间减N个月
     * @return
     */
    public static String getCurrDateTimeSubNMonth(int n,String pattern){
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_5;
        }
        return covertObjToString(getCurrLocalDateTime().minusMonths(n),pattern);
    }

    /**
     * 当前时间加N个月
     * @return
     */
    public static String getCurrDateTimePlusNMonth(int n){
        return getCurrDateTimePlusNMonth(n,DateUtil.DATE_FORMATTER_5);
    }

    /**
     * 当前时间加N个月
     * @return
     */
    public static String getCurrDateTimePlusNMonth(int n,String pattern){
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_5;
        }
        return covertObjToString(getCurrLocalDateTime().plusMonths(n),pattern);
    }

    /**
     * 获取几个月前的最后一天
     * 比如现在是2022-09-20，获取3个月前的最后一天，那就是2022-06-30
     * @return
     */
    public static String getLastMonthsEndDay(int num,String pattern){
        if (UtilValidate.isEmpty(num)) {
            throw new DateTimeException("Please enter the num parameters!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        // 月份
        LocalDate end = LocalDate.now().minusMonths(num);
        //上个月的最后一天
        LocalDate lastMonthDay = end.with(TemporalAdjusters.lastDayOfMonth());
        return covertObjToString(lastMonthDay,pattern);
    }

    /**
     * 获取几个月后的第一天
     * 比如现在是2022-09-20，获取1个月后的第一天，那就是2022-10-01
     * @return
     */
    public static String getNextMonthsStartDay(int num,String pattern){
        if (UtilValidate.isEmpty(num)) {
            throw new DateTimeException("Please enter the num parameters!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        // 起始时间
        LocalDate start =  LocalDate.now().plusMonths(num);
        LocalDate firstday = LocalDate.of(start.getYear(), start.getMonthValue(), 1);
        return covertObjToString(firstday,pattern);
    }

    /**
     * 获取下个月的最后一天
     * @return
     */
    public static String getNextMonthEndDay(String pattern){
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        //下个月的最后一天
        LocalDate nextMonthEndDay = LocalDate.now().plusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
        return covertObjToString(nextMonthEndDay,pattern);
    }

    /**
     * 获取下几月的最后一天
     * @return
     */
    public static String getNextMonthsEndDay(int num,String pattern){
        if (UtilValidate.isEmpty(num)) {
            throw new DateTimeException("Please enter the num parameters!");
        }
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DateUtil.DATE_FORMATTER_3;
        }
        //下几个月的最后一天
        LocalDate nextMonthEndDay = LocalDate.now().plusMonths(num).with(TemporalAdjusters.lastDayOfMonth());
        return covertObjToString(nextMonthEndDay,pattern);
    }

    /**
     * 获取系统当前日期时间字符串，格式为yyyy-MM-dd HH:mm:ss
     *
     * @return java.lang.String
     */
    public static String getCurrDateTime(){
        return covertObjToString(getCurrLocalDateTime(),DateUtil.DATE_FORMATTER_5);
    }

    /**
     * 获取系统当前日期时间字符串
     *
     * @return java.lang.String
     */
    public static String getCurrDateTime(String pattern){
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DATE_FORMATTER_5;
        }
        return covertObjToString(getCurrLocalDateTime(),pattern);
    }

    /**
     * 获取系统当前日期时间字符串，格式为yyyy-MM-dd
     *
     * @return java.sql.Date
     */
    public static java.sql.Date getCurrSqlDate(){
        return convertObjToSqlDate(getCurrLocalDateTime());
    }

    /**
     * 获取系统当前日期时间字符串，格式为yyyy-MM-dd HH:mm:ss:SSS
     *
     * @return java.sql.Timestamp
     */
    @SneakyThrows
    public static Timestamp getCurrTimestamp(){
        return convertObjToTimestamp(LocalDateTime.now());
    }

    /**
     * 获取系统当前日期时间
     *
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime getCurrLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 获取系统当前日期时间字符串，格式为自定义格式，默认格式为yyyy-MM-dd HH:mm:ss
     *
     * @return java.lang.String
     */
    public static String getCurrentLocalDate(String pattern) {
        if (UtilValidate.isEmpty(pattern)) {
            pattern = DATE_FORMATTER_5;
        }
        return covertObjToString(getCurrLocalDateTime(), pattern);
    }

    /**
     * 返回当前的日期
     *
     * @return java.time.LocalDate
     */
    public static LocalDate getCurrentLocalDate() {
        return LocalDate.now();
    }

    /**
     * 返回当前时间
     *
     * @return java.time.LocalTime
     */
    public static LocalTime getCurrentLocalTime() {
        return LocalTime.now();
    }


    /**
     * 获取两个日期的差  field参数为ChronoUnit.*
     * @param startTime
     * @param endTime
     * @param field  单位(年月日时分秒)
     * @return long
     */
    public static long betweenTwoTime(LocalDateTime startTime, LocalDateTime endTime, ChronoUnit field) {
        if (UtilValidate.isEmpty(startTime)) {
            throw new DateTimeException("Please enter a start time!");
        }
        if (UtilValidate.isEmpty(endTime)) {
            throw new DateTimeException("Please enter a end time!");
        }
        if (UtilValidate.isEmpty(field)) {
            throw new DateTimeException("Please enter a time unit!");
        }
        Period period = Period.between(LocalDate.from(startTime), LocalDate.from(endTime));
        if (field == ChronoUnit.YEARS) {
            return period.getYears();
        }
        if (field == ChronoUnit.MONTHS) {
            return period.getYears() * 12 + period.getMonths();
        }
        return field.between(startTime, endTime);
    }

    /**
     * 获取两个时间相差天数
     * @param startDateInclusive
     * @param endDateExclusive
     * @return long
     */
    public static long periodDays(LocalDate startDateInclusive, LocalDate endDateExclusive) {
        if (UtilValidate.isEmpty(startDateInclusive)) {
            throw new DateTimeException("Please enter a start time!");
        }
        if (UtilValidate.isEmpty(endDateExclusive)) {
            throw new DateTimeException("Please enter a end time!");
        }
        return endDateExclusive.toEpochDay() - startDateInclusive.toEpochDay();
    }

    /**
     * 日期相隔小时
     * @param startInclusive
     * @param endExclusive
     * @return long
     */
    public static long durationHours(Temporal startInclusive, Temporal endExclusive) {
        if (UtilValidate.isEmpty(startInclusive)) {
            throw new DateTimeException("Please enter a start time!");
        }
        if (UtilValidate.isEmpty(endExclusive)) {
            throw new DateTimeException("Please enter a end time!");
        }
        return Duration.between(startInclusive, endExclusive).toHours();
    }

    /**
     * 日期相隔分钟
     * @param startInclusive
     * @param endExclusive
     * @return long
     */
    public static long durationMinutes(Temporal startInclusive, Temporal endExclusive) {
        if (UtilValidate.isEmpty(startInclusive)) {
            throw new DateTimeException("Please enter a start time!");
        }
        if (UtilValidate.isEmpty(endExclusive)) {
            throw new DateTimeException("Please enter a end time!");
        }
        return Duration.between(startInclusive, endExclusive).toMinutes();
    }

    /**
     * 日期相隔毫秒数
     * @param startInclusive
     * @param endExclusive
     * @return long
     */
    public static long durationMillis(Temporal startInclusive, Temporal endExclusive) {
        if (UtilValidate.isEmpty(startInclusive)) {
            throw new DateTimeException("Please enter a start time!");
        }
        if (UtilValidate.isEmpty(endExclusive)) {
            throw new DateTimeException("Please enter a end time!");
        }
        return Duration.between(startInclusive, endExclusive).toMillis();
    }

    /**
     * 是否当天
     * @param date
     * @return boolean
     */
    public static boolean isToday(LocalDate date) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please enter a date!");
        }
        return getCurrentLocalDate().equals(date);
    }

    /**
     * 获取此日期时间与默认时区<Asia/Shanghai>组合的时间毫秒数
     * @param dateTime
     * @return
     */
    public static Long toEpochMilli(LocalDateTime dateTime) {
        if (UtilValidate.isEmpty(dateTime)) {
            throw new DateTimeException("Please enter a localDateTime!");
        }
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 获取此日期时间与指定时区组合的时间毫秒数
     * @param dateTime
     * @return
     */
    public static Long toSelectEpochMilli(LocalDateTime dateTime, ZoneId zoneId) {
        if (UtilValidate.isEmpty(dateTime)) {
            throw new DateTimeException("Please enter a localDateTime!");
        }
        if (UtilValidate.isEmpty(zoneId)) {
            throw new DateTimeException("Please enter a zoneId!");
        }
        return dateTime.atZone(zoneId).toInstant().toEpochMilli();
    }

    /**
     * 判断是否为闰年
     * @param date
     * @return boolean
     */
    public static boolean isLeapYear (LocalDate date) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please Enter a LocalDate!");
        }
        return date.isLeapYear();
    }

    /**
     * 获取当前的ZoneDateTime
     *
     * @param zoneId 时区偏移量
     * @return
     */
    public static ZonedDateTime now(ZoneId zoneId) {
        if (UtilValidate.isEmpty(zoneId)) {
            throw new DateTimeException("Please enter a zoneId!");
        }
        return ZonedDateTime.now(zoneId);
    }

    /**
     * 获取当前日期的开始时间ZonedDateTime
     *
     * @param date   日期
     * @param zoneId 时区偏移量
     * @return
     */
    public static ZonedDateTime ldToZoneDateTime(LocalDate date, ZoneId zoneId) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please enter a LocalDate!");
        }
        if (UtilValidate.isEmpty(zoneId)) {
            throw new DateTimeException("Please enter a zoneId!");
        }
        return date.atStartOfDay(zoneId);
    }

    /**
     * 获取当前日期的开始时间
     *
     * @param dateTime
     * @return
     */
    public static LocalDateTime startOfDay(ZonedDateTime dateTime) {
        if (UtilValidate.isEmpty(dateTime)) {
            throw new DateTimeException("Please enter a ZonedDateTime!");
        }
        return dateTime.truncatedTo(ChronoUnit.DAYS).toLocalDateTime();
    }

    /**
     * 获取今天后的指定天数的开始时间
     *
     * @param plusDays 当前多少天后
     * @param zoneId   时区偏移量
     * @return
     */
    public static LocalDateTime startOfDay(int plusDays, ZoneId zoneId) {
        if (UtilValidate.isEmpty(plusDays)) {
            throw new DateTimeException("Please enter the specified number of days after today!");
        }
        if (UtilValidate.isEmpty(zoneId)) {
            throw new DateTimeException("Please enter a zoneId!");
        }
        return startOfDay(now(zoneId).plusDays(plusDays));
    }

    /**
     * 获取指定日期的后几个工作日的时间LocalDate
     *
     * @param date 指定日期
     * @param days 工作日数
     * @return
     */
    public static LocalDate plusWeekdays(LocalDate date, int days) {
        if (days == 0) {
            return date;
        }
        if (Math.abs(days) > 50) {
            throw new IllegalArgumentException("days must be less than 50");
        }
        int i = 0;
        int delta = days > 0 ? 1 : -1;
        while (i < Math.abs(days)) {
            date = date.plusDays(delta);
            DayOfWeek dayOfWeek = date.getDayOfWeek();
            if (dayOfWeek != DayOfWeek.SATURDAY && dayOfWeek != DayOfWeek.SUNDAY) {
                i += 1;
            }
        }
        return date;
    }

    /**
     * 获取指定时间的上一个工作日
     *
     * @param time           指定时间
     * @param formattPattern 格式化参数
     * @return
     */
    public static String getPreWorkDay(String time, String formattPattern) {
        DateTimeFormatter dateTimeFormatter = generateDefualtPattern(formattPattern);
        LocalDateTime compareTime1 = LocalDateTime.parse(time, dateTimeFormatter);
        compareTime1 = compareTime1.with(temporal -> {
            // 当前日期
            DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
            // 正常情况下，每次减去一天
            int dayToMinu = 1;
            // 如果是周日，减去2天
            if (dayOfWeek == DayOfWeek.SUNDAY) {
                dayToMinu = 2;
            }
            // 如果是周六，减去一天
            if (dayOfWeek == DayOfWeek.SATURDAY) {
                dayToMinu = 1;
            }
            return temporal.minus(dayToMinu, ChronoUnit.DAYS);
        });
        return compareTime1.format(dateTimeFormatter);
    }


    /**
     * 获取指定时间的下一个工作日
     *
     * @param time           指定时间
     * @param formattPattern 格式参数
     * @return
     */
    public static String getNextWorkDay(String time, String formattPattern) {
        DateTimeFormatter dateTimeFormatter = generateDefualtPattern(formattPattern);
        LocalDateTime compareTime1 = LocalDateTime.parse(time, dateTimeFormatter);
        compareTime1 = compareTime1.with(temporal -> {
            // 当前日期
            DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
            // 正常情况下，每次增加一天
            int dayToAdd = 1;
            // 如果是星期五，增加三天
            if (dayOfWeek == DayOfWeek.FRIDAY) {
                dayToAdd = 3;
            }
            // 如果是星期六，增加两天
            if (dayOfWeek == DayOfWeek.SATURDAY) {
                dayToAdd = 2;
            }
            return temporal.plus(dayToAdd, ChronoUnit.DAYS);
        });
        return compareTime1.format(dateTimeFormatter);
    }

    /**
     * 生成默认的格式器
     *
     * @param timeFormat 指定格式
     * @return 默认时间格式器
     */
    private static DateTimeFormatter generateDefualtPattern(String timeFormat) {
        return new DateTimeFormatterBuilder().appendPattern(timeFormat)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                .toFormatter(Locale.CHINA);
    }

    /**
     * 获取指定日期的后几个工作日的时间ZoneDateTime
     *
     * @param date
     * @param days
     * @return
     */
    public static ZonedDateTime plusWeekdays(ZonedDateTime date, int days) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please enter a ZonedDateTime!");
        }
        if (UtilValidate.isEmpty(days)) {
            throw new DateTimeException("Please enter the working day after the specified date!");
        }
        return plusWeekdays(date.toLocalDate(), days).atStartOfDay(date.getZone());
    }

    /**
     * 获取当前月份的第一天的时间ZoneDateTime
     *
     * @param zoneId
     * @return
     */
    public static ZonedDateTime firstDayOfMonth(ZoneId zoneId) {
        if (UtilValidate.isEmpty(zoneId)) {
            throw new DateTimeException("Please enter a ZoneId!");
        }
        return now(zoneId).withDayOfMonth(1);
    }

    /**
     * 两个时区的zoneDateTime相互转换
     *
     * @param zonedDateTime 需要转换的如期
     * @param zoneId        转换成的ZoneDateTime的时区偏移量
     * @return
     */
    public static ZonedDateTime zdtToZdt(ZonedDateTime zonedDateTime, ZoneId zoneId) {
        if (UtilValidate.isEmpty(zonedDateTime)) {
            throw new DateTimeException("Please enter a ZonedDateTime object to be converted!");
        }
        ZonedDateTime zdt = null;
        try {
            zdt =  ZonedDateTime.ofInstant(zonedDateTime.toInstant(), zoneId);
        } catch (DateTimeException e){
            throw new DateTimeException("Date conversion exception!");
        }
        return zdt;
    }

    /**
     * 将ZonedDateTime转成时间戳long
     *
     * @return
     * @parm zonedDateTime
     */
    public static long zoneDateTimeToLong(ZonedDateTime zonedDateTime) {
        if (UtilValidate.isEmpty(zonedDateTime)) {
            throw new DateTimeException("Please enter the ZonedDateTime Object to be converted!");
        }
        long timeStamp = 0L;
        try {
            timeStamp = zonedDateTime.toInstant().toEpochMilli();
        } catch (DateTimeException e){
            throw new DateTimeException("Date conversion exception!");
        }
        return timeStamp;
    }

    /**
     * 将LocalDateTime转成时间戳long
     *
     * @param localDateTime
     * @param zoneId
     * @return
     */
    public static long toLong(LocalDateTime localDateTime, ZoneId zoneId) {
        if (UtilValidate.isEmpty(localDateTime)) {
            throw new DateTimeException("Please enter the LocalDateTime Object to be converted!");
        }
        long timeStamp = 0L;
        try {
            timeStamp = zoneDateTimeToLong(localDateTime.atZone(zoneId));
        } catch (DateTimeException e){
            throw new DateTimeException("Date conversion exception!");
        }
        return timeStamp;
    }

    /**
     * 获取周第一天
     *
     * @param date
     * @return
     */
    public static Date getStartDayOfWeek(String date) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please enter the date string to be converted!");
        }
        Date dt = null;
        try {
            LocalDate now = LocalDate.parse(date);
            dt =  getStartDayOfWeek(now);
        } catch (DateTimeException e){
            throw new DateTimeException("Date conversion exception!");
        }
        return dt;
    }

    public static Date getStartDayOfWeek(TemporalAccessor date) {
        TemporalField fieldISO = WeekFields.of(Locale.CHINA).dayOfWeek();
        LocalDate localDate = LocalDate.from(date);
        localDate = localDate.with(fieldISO, 1);
        return convertObjToUtilDate(localDate);
    }

    /**
     * 获取周最后一天
     *
     * @param date
     * @return
     */
    public static Date getEndDayOfWeek(String date) {
        if (UtilValidate.isEmpty(date)) {
            throw new DateTimeException("Please enter the date string to be converted!");
        }
        Date dt = null;
        try {
            LocalDate now = LocalDate.parse(date);
            dt =  getEndDayOfWeek(now);
        } catch (DateTimeException e){
            throw new DateTimeException("Date conversion exception!");
        }
        return dt;
    }

    public static Date getEndDayOfWeek(TemporalAccessor date) {
        TemporalField fieldISO = WeekFields.of(Locale.CHINA).dayOfWeek();
        LocalDate localDate = LocalDate.from(date);
        localDate = localDate.with(fieldISO, 7);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).plusDays(1L).minusNanos(1L).toInstant());
    }

    /**
     * String转成java.sql.Timestamp
     * @param date
     * @return java.sql.Timestamp
     * @author liuc
     * @date 2021/11/30 21:43
     * @throws
     */
    public static Timestamp stringToTimestamp(String date) throws ServiceException {
        String dateStr = null;
        if (UtilValidate.isEmpty(date)) {
            return null;
        }
        if (RegexUtil.isDigit(date)){
            if (UtilValidate.isNotEmpty(getDateStr(date))) {
                dateStr = getDateStr(date);
            } else {
                if (date.length() == 10 || date.length() == 13) {
                    dateStr = getDateStr(getSdf(DateUtil.DATE_FORMATTER_14).format(Long.parseLong(date)));
                }
            }
        } else {
            dateStr = getDateStr(date);
        }
        if (UtilValidate.isEmpty(dateStr)) {
            /**
             * 时间转换异常
             */
            log.error("时间转换异常:" + date);
            throw new ServiceException("时间["+date+"]格式转换异常");
        }
        try {
            if (RegexUtil.isDigit(date)) {
                if (UtilValidate.isNotEmpty(getDateStr(date))) {
                    return new Timestamp(parse(date, dateStr).getTime());
                } else {
                    date = getSdf(DateUtil.DATE_FORMATTER_14).format(Long.parseLong(date));
                    return new Timestamp(parse(date, dateStr).getTime());
                }
            } else {
                if (UtilValidate.areEqual(date.length(), DATE_FORMATTER_8.length())) {
                    date = date.substring(0,date.length()-4);
                }
                return new Timestamp(parse(date, dateStr).getTime());
            }
        } catch (ParseException e) {
            /**
             * 时间转换异常
             */
            log.error("时间转换异常:" + e.getMessage());
            throw new ServiceException("时间["+date+"]格式转换异常");
        }
    }

    public static Date parse(String dateStr, String pattern)
            throws ParseException {
        return getSdf(pattern).parse(dateStr);
    }

    /**
     * Object转成Timestamp<br>
     * 能转换成Timestamp的有yyyy-MM-dd、yyyy/MM/dd、yyyy.MM.dd、yyyy-MM-dd HH:mm、yyyy-MM-dd HH:mm:ss、yyyy-MM-dd HH:mm:ss:SSS、
     * yyyyMMdd HHmmss、yyMMddHHmmss、yyyyMMdd HHmmssSSS、yyyyMMddHHmmssSSS格式的时间字符串，以及java.util.Date、java.sql.Date、
     * LocalDate、LocalDateTime
     * @param obj
     * @return java.sql.Timestamp
     * @author liuc
     * @date 2021/11/16 19:02
     * @throws
     */
    @SneakyThrows
    public static Timestamp  convertObjToTimestamp(Object obj) throws ParseException {
        Timestamp timestamp = null;
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        if (obj instanceof Timestamp) {
            timestamp = (Timestamp) obj;
        } else if (obj instanceof String) {
            timestamp = stringToTimestamp((String)obj);
        } else if (obj instanceof Date) {
            timestamp = new Timestamp(((Date)obj).getTime());
        } else if (obj instanceof java.sql.Date) {
            Long l = ((java.sql.Date)obj).getTime();
            timestamp = new Timestamp(l);
        } else if (obj instanceof LocalDate) {
            //毫秒时间戳
            Long l = ((LocalDate)obj).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
            timestamp = new Timestamp(l);
        } else if (obj instanceof LocalDateTime) {
            //毫秒时间戳
            Long l = ((LocalDateTime)obj).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            timestamp = new Timestamp(l);
        } else if (obj instanceof ZonedDateTime) {
            timestamp = Timestamp.valueOf(((ZonedDateTime)obj).toLocalDateTime());
        } else if (obj instanceof Instant) {
            timestamp = Timestamp.from((Instant)obj);
        } else if (obj instanceof Long) {
            timestamp = new Timestamp((Long)obj);
        } else {
            throw new ClassCastException("Not possible to coerce [" + obj + "] from class " + obj.getClass()
                    + " into a Timestamp.");
        }
        return timestamp;
    }

    /**
     * Object转成java.sql.Date<br>
     * @param obj
     * @return java.sql.Date
     * @author liuc
     * @date 2021/11/17 21:56
     * @throws
     */
    @SneakyThrows
    public static java.sql.Date convertObjToSqlDate(Object obj){
        Timestamp timestamp =  convertObjToTimestamp(obj);
        java.sql.Date date = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            date = new java.sql.Date(timestamp.getTime());
        }
        return date;
    }

    /**
     * Object转成java.util.Date
     * @param obj
     * @return java.util.Date
     * @author liuc
     * @date 2021/11/17 22:05
     * @throws
     */
    @SneakyThrows
    public static Date convertObjToUtilDate(Object obj){
        Date date =  new Date(convertObjToTimestamp(obj).getTime());
        return date;
    }

    /**
     * Object转成DateTime
     * @param obj
     * @return DateTime
     * @author liuc
     * @date 2022/09/09 10:20
     * @throws
     */
    @SneakyThrows
    public static DateTime covertObjToDateTime(Object obj){
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        Date date1 = convertObjToUtilDate(obj);
        DateTime dateTime = new DateTime(date1.getTime());
        return dateTime;
    }

    /**
     * Object转成java.time.LocalDateTime
     * @param obj
     * @return java.time.LocalDateTime
     * @author liuc
     * @date 2021/11/17 22:17
     * @throws
     */
    @SneakyThrows
    public static LocalDateTime convertObjToLdt(Object obj){
        Timestamp timestamp =  convertObjToTimestamp(obj);
        LocalDateTime localDateTime = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            localDateTime = timestamp.toLocalDateTime();
        }
        return localDateTime;
    }

    /**
     * Object转成java.time.LocalDate
     * @param obj
     * @return java.time.LocalDate
     * @author liuc
     * @date 2021/11/17 22:17
     * @throws
     */
    @SneakyThrows
    public static LocalDate convertObjToLd(Object obj){
        LocalDateTime ldt =  convertObjToLdt(obj);
        LocalDate ld = null;
        if (UtilValidate.isNotEmpty(ldt)) {
            ld = ldt.toLocalDate();
        }
        return ld;
    }

    /**
     * Object转String类型的日期，格式默认为yyyy-MM-dd HH:mm:ss
     * @param obj
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/17 22:25
     * @throws
     */
    @SneakyThrows
    public static String covertObjToString(Object obj){
        DateFormat sdf = new SimpleDateFormat(DATE_FORMATTER_5);
        Timestamp timestamp =  convertObjToTimestamp(obj);
        String time = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            time = sdf.format(timestamp);
        }
        return time;
    }

    /**
     * Object按指定日期格式转String类型的日期
     * @param obj
     * @param pattern
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/17 22:26
     * @throws
     */
    @SneakyThrows
    public static String covertObjToString(Object obj,String pattern){
        DateFormat sdf = new SimpleDateFormat(pattern);
        Timestamp timestamp =  convertObjToTimestamp(obj);
        String time = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            time = sdf.format(timestamp);
        }
        return time;
    }

    /**
     * Object转成java.time.ZonedDateTime
     * @param obj
     * @return java.lang.String
     * @author liuc
     * @date 2021/11/30 21:03
     * @throws
     */
    @SneakyThrows
    public static ZonedDateTime covertObjToZdt(Object obj){
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        Timestamp timestamp =  convertObjToTimestamp(obj);
        return ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneId.systemDefault());
    }

    /**
     * Object转成java.time.Instant
     * @param obj
     * @return java.time.Instant
     * @author liuc
     * @date 2021/11/30 21:20
     * @throws
     */
    @SneakyThrows
    public static Instant covertObjToInstant(Object obj){
        if (UtilValidate.isEmpty(obj)) {
            return null;
        }
        Timestamp timestamp =  convertObjToTimestamp(obj);
        Date date = new Date(timestamp.getTime());
        return date.toInstant();
    }

    /**
     * Object转LocalDateTime类型的日期
     * @param obj
     * @return LocalDateTime
     * @author liuc
     * @date 2021/11/17 22:25
     * @throws
     */
    @SneakyThrows
    public static LocalDateTime covertObjToLdt(Object obj){
        Timestamp timestamp =  convertObjToTimestamp(obj);
        LocalDateTime ldt = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            ldt = timestamp.toLocalDateTime();
        }
        return ldt;
    }

    /**
     * Object转LocalDate类型的日期
     * @param obj
     * @return LocalDate
     * @author liuc
     * @date 2021/11/17 22:25
     * @throws
     */
    @SneakyThrows
    public static LocalDate covertObjToLd(Object obj){
        Timestamp timestamp =  convertObjToTimestamp(obj);
        LocalDate ld = null;
        if (UtilValidate.isNotEmpty(timestamp)) {
            ld = timestamp.toLocalDateTime().toLocalDate();
        }
        return ld;
    }

    /**
     * 查询上个月的今天，如果当前时间是5月31号，上个月没有31号，那么只能显示4月30号
     * @return
     */
    public static String getDayOfLastMonth() {
        LocalDate date = LocalDate.now();
        // 当前月份减1
        LocalDate lastMonth = date.minusMonths(1);
        return covertObjToString(lastMonth);
    }

    /**
     * 指定时间减一个月
     * @param dateTime
     * @return
     */
    public static String getDayOfLastMonth(String dateTime) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateTime, fmt);
        // 当前月份减1
        LocalDate lastMonth = date.minusMonths(1);
        return covertObjToString(lastMonth);
    }

    /**
     * 比较日期大小
     * <p>obj1或者obj2为空，返回-1</p>
     * <p>obj1早于obj2，返回0</p>
     * <p>obj1晚于obj2，返回1</p>
     * <p>obj1等于obj2，返回2</p>
     * @author liuc
     * @param obj1
     * @param obj2
     * @return
     */
    public static int compareDate(Object obj1, Object obj2){
        if (UtilValidate.isEmpty(obj1)||UtilValidate.isEmpty(obj2)) {
            return -1;
        }
        LocalDateTime ld1 = convertObjToLdt(obj1);
        LocalDateTime ld2 = convertObjToLdt(obj2);
        //比较日期
        if (ld1.isBefore(ld2)) {
            //obj1早于obj2，返回0
            return 0;
        }if (ld1.isAfter(ld2)) {
            return 1;
        }else {
            //obj1等于obj2，返回1
            return 2;
        }
    }

    public static Date addTimeoutDateWithMillisecond(Date ori, long milli) {
        if (ori == null) {
            return null;
        }
        if (milli == -1 || milli == 0) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(ori);
        cal.setTimeInMillis(cal.getTimeInMillis() + milli);
        return cal.getTime();
    }

    /**
	 * 给当前时间加上几分钟、几个小时、几天
	 *
	 * @param cDate
	 *            当前时间 格式：yyyy-MM-dd HH:mm:ss
	 * @param val
	 *            需要加的时间
	 * @return
	 */
	public static String addDateByUnit(String cDate, String val) {
		// 引号里面个格式也可以是 HH:mm:ss或者HH:mm等等，很随意的，不过在主函数调用时，要和输入的变量day格式一致
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 24小时制
		Date date = null;
		try {
			date = format.parse(cDate);
		} catch (ParseException ex) {
			log.error("日期格式化异常，原因：{}",ex);
		}
		if (date == null) {
			return "";
		}
		if (StrUtil.isBlank(val) || StrUtil.isBlank(val)) {
			return "";
		}
		log.info("| - DateUtil>>>>>front:{}" , format.format(date)); // 显示输入的日期
		Calendar cal = Calendar.getInstance();
		try {
			if (StringUtil.indexOf(val, DateConstants.MINUTE) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.MINUTE));
				int minut = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.MINUTE));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.MINUTE, minut);
				date = cal.getTime();
			}
			if (StringUtil.indexOf(val, DateConstants.HOUR) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.HOUR));
				int hour = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.HOUR));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.HOUR_OF_DAY, hour);
				date = cal.getTime();
			}
			if (StringUtil.indexOf(val, DateConstants.DAY) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.DAY));
				int day = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.DAY));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.DAY_OF_MONTH, day);
				date = cal.getTime();
			}
		} catch (NumberFormatException ex) {
			log.error("计算时间异常，原因：{}",ex);
		}
		cal = null;
		log.info("| - DateUtil>>>>>after:{}" , format.format(date));
		return format.format(date);
	}

}
