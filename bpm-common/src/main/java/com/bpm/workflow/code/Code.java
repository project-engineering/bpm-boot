package com.bpm.workflow.code;

public enum Code implements ICode {
    /* 成功状态码 */
    SUCCESS("000000", "操作成功"),
    FAILURE("999999", "处理失败"),
    /* 系统错误 100000-199999*/
    SYSTEM_ERROR("100000", "系统异常，请稍后重试!"),
    LIMIT_ERROR("100001","当前排队人数较多，请稍后再试!"),
    PREVENT_SUBMIT_ERROR("100002","请勿重复提交!"),
    /* 用户错误 200000-299999*/
    USER_NOTLOGGED_IN("200000", "用户未登录"),
    USER_LOGIN_ERROR("200001", "账号不存在或密码错误"),
    /**
     * 客户端request body参数错误
     * 主要是未能通过Hibernate Validator校验的异常处理
     * <p>
     * org.springframework.web.bind.MethodArgumentNotValidException
     */
    CLIENT_REQUEST_BODY_CHECK_ERROR("300000", "客户端请求体参数校验不通过"),
    PARAM_IS_BLANK("300001", "参数为空"),
    PARAM_TYPE_BIND_ERROR("300002", "参数类型错误"),
    PARAM_NOT_COMPLETE("300003", "参数缺失"),
    UNABLE_FIND_RESOURCES("300004","无法找到资源"),
    /**
     * 客户端HTTP请求方法错误
     * org.springframework.web.HttpRequestMethodNotSupportedException
     */
    CLIENT_HTTP_METHOD_ERROR("300005","客户端HTTP请求方法错误"),
    /**
     * 客户端@PathVariable参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.validation.BindException
     */
    CLIENT_PATH_VARIABLE_ERROR("300006","客户端URL中的参数类型错误"),
    /**
     * 客户端@RequestBody参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.bind.MethodArgumentConversionNotSupportedException
     */
    CLIENT_REQUEST_BODY_ERROR("300007","客户端请求体参数类型错误"),
    /**
     * 客户端@RequestParam参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
     */
    CLIENT_REQUEST_PARAM_ERROR("300008","客户端请求参数类型错误"),
    /**
     * 客户端@CookieValue参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.CookieValueMethodArgumentTypeMismatchException
     */
    CLIENT_COOKIE_VALUE_ERROR("300009","客户端Cookie参数类型错误"),
    /**
     * 客户端@RequestHeader参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
     */
    CLIENT_REQUEST_HEADER_ERROR("300010","客户端请求头参数类型错误"),
    /**
     * 客户端@ModelAttribute参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.method.annotation.ModelAttributeMethodProcessor.resolveArgument
     */
    CLIENT_MODEL_ATTRIBUTE_ERROR("300011","客户端@ModelAttribute参数类型错误"),
    /**
     * 客户端@SessionAttribute参数错误
     * 一般是类型不匹配，比如本来是Long类型，客户端却给了一个无法转换成Long字符串
     * org.springframework.web.bind.MissingSessionAttributeException
     */
    CLIENT_SESSION_ATTRIBUTE_ERROR("300012","客户端@SessionAttribute参数类型错误"),
    /**
     * 客户端@RequestBody参数解析错误
     * 一般是JSON格式错误，无法正确解析
     * org.springframework.http.converter.HttpMessageNotReadableException
     */
    CLIENT_REQUEST_BODY_PARSE_ERROR("300013","客户端请求体参数解析错误"),
    /**
     * 客户端@RequestBody参数序列化错误
     * 一般是JSON格式错误，无法正确序列化
     * org.springframework.http.converter.HttpMessageNotWritableException
     */
    CLIENT_REQUEST_BODY_SERIALIZE_ERROR("300014","客户端请求体参数序列化错误"),
    /**
     * 客户端@PathVariable参数解析错误
     * 一般是路径参数错误，无法正确解析
     * org.springframework.web.servlet.mvc.method.annotation.PathVariableMethodArgumentResolver.resolveArgument
     */
    CLIENT_PATH_VARIABLE_PARSE_ERROR("300015","客户端URL中的参数解析错误"),
    /**
     * 客户端@RequestParam参数解析错误
     * 一般是请求参数错误，无法正确解析
     * org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.resolveArgument
     */
    CLIENT_REQUEST_PARAM_PARSE_ERROR("300016","客户端请求参数解析错误"),
    /**
     * 客户端@CookieValue参数解析错误
     * 一般是Cookie参数错误，无法正确解析
     * org.springframework.web.method.annotation.CookieValueMethodArgumentResolver.resolveArgument
     */
    CLIENT_COOKIE_VALUE_PARSE_ERROR("300017","客户端Cookie参数解析错误"),
    /**
     * 客户端@RequestHeader参数解析错误
     * 一般是请求头参数错误，无法正确解析
     * org.springframework.web.method.annotation.RequestHeaderMethodArgumentResolver.resolveArgument
     */
    CLIENT_REQUEST_HEADER_PARSE_ERROR("300018","客户端请求头参数解析错误"),
    /**
     * 客户端@ModelAttribute参数解析错误
     * 一般是@ModelAttribute参数错误，无法正确解析
     * org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor.resolveArgument
     */
    CLIENT_MODEL_ATTRIBUTE_PARSE_ERROR("300019","客户端@ModelAttribute参数解析错误"),
    /**
     * 客户端@SessionAttribute参数解析错误
     * 一般是@SessionAttribute参数错误，无法正确解析
     * org.springframework.web.bind.support.SessionAttributeMethodArgumentResolver.resolveArgument
     */
    CLIENT_SESSION_ATTRIBUTE_PARSE_ERROR("300020","客户端@SessionAttribute参数解析错误"),
    /**
     * 客户端@RequestBody请求体JSON格式错误或字段类型错误
     * org.springframework.http.converter.HttpMessageNotReadableException
     * <p>
     * eg:
     * 1、参数类型不对:{"test":"abc"}，本身类型是Long
     * 2、{"test":}  test属性没有给值
     */
    CLIENT_REQUEST_BODY_FORMAT_ERROR("300021","客户端请求体JSON格式错误或字段类型不匹配"),
    /**
     * 客户端@RequestParam请求参数缺失
     * org.springframework.web.method.annotation.RequestParamMethodArgumentResolver.handleMissingValue
     */
    CLIENT_REQUEST_PARAM_REQUIRED_ERROR("300022","客户端请求参数缺失"),
    /**
     * 通用的业务方法入参检查错误
     * java.lang.IllegalArgumentException
     */
    SERVER_ILLEGAL_ARGUMENT_ERROR("300023", "业务方法参数检查不通过"),
    UNKUOW_ERROR("999999","系统未知错误");

    private String code;
    private String message;

    private Code(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}