package com.bpm.workflow.code;

public interface ICode {
    String getCode();

    String getMessage();
}