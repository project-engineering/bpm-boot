package com.bpm.workflow.exception;

public interface ResourceMessage {
    String getCode();

    Object[] getParams();
}
