package com.bpm.workflow.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

public class CommonRuntimeException extends RuntimeException implements ResourceMessage {
    private static final long serialVersionUID = -6634377945853150471L;
    private I18NException i18nException;

    public String getCode() {
        return this.i18nException.getCode();
    }

    public Object[] getParams() {
        return this.i18nException.getParams();
    }

    public Throwable getException() {
        return this.i18nException.getException();
    }

    public String getMessage(Locale locale) {
        return this.i18nException.getLocalizedMessage(locale);
    }

    public String getMessage() {
        return this.i18nException.getLocalizedMessage();
    }

    public String toStackTraceString() {
        StringWriter sw = new StringWriter();
        this.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public CommonRuntimeException(String code) {
        this.i18nException = new I18NException(code);
    }

    public CommonRuntimeException(String code, Object[] params) {
        this.i18nException = new I18NException(code, params);
    }

    public CommonRuntimeException(String code, String message) {
        super(message);
        this.i18nException = new I18NException(code, message);
    }

    public CommonRuntimeException(String code, Throwable cause) {
        super(cause);
        this.i18nException = new I18NException(code, cause);
    }

    public CommonRuntimeException(String code, Object[] params, String message) {
        super(String.format(message, params));
        this.i18nException = new I18NException(code, params, message);
    }

    public CommonRuntimeException(String code, String message, Throwable cause) {
        super(message, cause);
        this.i18nException = new I18NException(code, message, cause);
    }

    public CommonRuntimeException(String code, Object[] params, Throwable cause) {
        super(cause);
        this.i18nException = new I18NException(code, params, cause);
    }

    public CommonRuntimeException(String code, Object[] params, String message, Throwable cause) {
        super(String.format(message, params), cause);
        this.i18nException = new I18NException(code, params, message, cause);
    }

    public String toString() {
        return this.i18nException.toString();
    }
}
