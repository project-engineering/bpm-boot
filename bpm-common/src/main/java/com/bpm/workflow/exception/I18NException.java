package com.bpm.workflow.exception;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.springframework.util.Assert;

public class I18NException implements Serializable {
    private static final long serialVersionUID = -1460720398885925388L;
    private static final Log log = LogFactory.get(I18NException.class);
    private String code;
    private String message;
    private Object[] params;
    private Throwable exception;
    private static final String SEPARATOR = "-";

    public I18NException(String code) {
        this(code, (Object[])null, (String)null, (Throwable)null);
    }

    public I18NException(String code, String message) {
        this(code, (Object[])null, message, (Throwable)null);
    }

    public I18NException(String code, Object[] params) {
        this(code, params, (String)null, (Throwable)null);
    }

    public I18NException(String code, Throwable exception) {
        this(code, (Object[])null, (String)null, exception);
    }

    public I18NException(String code, Object[] params, String message) {
        this(code, params, message, (Throwable)null);
    }

    public I18NException(String code, String message, Throwable exception) {
        this(code, (Object[])null, message, exception);
    }

    public I18NException(String code, Object[] params, Throwable exception) {
        this(code, params, (String)null, exception);
    }

    public I18NException(String code, Object[] params, String message, Throwable exception) {
        Assert.notNull(code, "exception code cannot be null!");
        this.code = code;
        this.message = message;
        this.params = params;
        this.exception = exception;
    }

    public String toString() {
        return this.getLocalizedMessage();
    }

    public String getLocalizedMessage() {
        return this.getLocalizedMessage((Locale)null);
    }

    public String getLocalizedMessage(Locale locale) {
        StringBuilder localizedMessage = new StringBuilder();
        if (this.message != null) {
            return this.message;
        } else {
            ExceptionMessageHelper messageHelper = ExceptionMessageHelper.getInstance();
            String codeMessage = messageHelper.getMessage(this.code, this.params, locale);
            if (codeMessage != null) {
                localizedMessage.append(codeMessage);
            } else if (this.params != null) {
                try {
                    localizedMessage.append(MessageFormat.format(this.message, this.params));
                } catch (Exception var6) {
                    log.error("format exception message {} error", new Object[]{this.message, var6});
                }
            } else {
                localizedMessage.append(this.message);
            }

            return localizedMessage.toString();
        }
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Throwable getException() {
        return this.exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object[] getParams() {
        return this.params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
