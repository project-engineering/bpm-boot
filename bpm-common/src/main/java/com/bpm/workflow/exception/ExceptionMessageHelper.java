package com.bpm.workflow.exception;

import java.util.Locale;

public class ExceptionMessageHelper {
    private static final ExceptionMessageHelper INSTANCE = new ExceptionMessageHelper();
    private ExceptionMessageSource messageSource;

    private ExceptionMessageHelper() {
    }

    public static ExceptionMessageHelper getInstance() {
        return INSTANCE;
    }

    public String getMessage(String code, Object[] params, Locale locale) {
        return this.messageSource == null ? null : this.messageSource.getMessage(code, params, locale);
    }

    public ExceptionMessageSource getMessageSource() {
        return this.messageSource;
    }

    public void setMessageSource(ExceptionMessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
