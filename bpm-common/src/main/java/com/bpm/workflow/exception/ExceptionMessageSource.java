package com.bpm.workflow.exception;


import java.util.Locale;

public interface ExceptionMessageSource {
    String getMessage(String var1, Object[] var2, String var3, Locale var4);

    String getMessage(String var1, Object[] var2, Locale var3);
}
