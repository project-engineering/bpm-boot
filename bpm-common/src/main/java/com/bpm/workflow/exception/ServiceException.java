package com.bpm.workflow.exception;

public class ServiceException extends CommonRuntimeException {
    public ServiceException(String code) {
        super(code);
    }

    public ServiceException(String code, Throwable cause) {
        super(code, cause);
    }

    public ServiceException(String code, String... params) {
        super(code, params);
    }

    public ServiceException(String code, Throwable cause, String... params) {
        super(code, params, cause);
    }
}