package com.bpm.workflow.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface WebLog {
    /**
     * 操作模块
     * @return
     */
    String module() default "";

    /**
     * 操作类型
     * @return
     */
    String type() default "";
    /**
     * 日志描述信息
     * @return
     **/
    String description() default "";
}
