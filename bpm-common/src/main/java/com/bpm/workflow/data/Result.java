package com.bpm.workflow.data;

import com.bpm.workflow.code.Code;
import com.bpm.workflow.code.ICode;
import com.bpm.workflow.exception.ServiceException;
import java.io.Serializable;
import java.util.List;

public class Result<T> implements Serializable {
    private String code;
    private String message;
    private long total;
    private T data;
    private long timestamp;
    private String traceId;

    public Result() {
    }

    public Result(String code, String message) {
        this.code = code;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
    }

    private Result(long total, T data, String code, String message) {
        this.code = code;
        this.message = message;
        this.timestamp = System.currentTimeMillis();
        this.total = total;
        this.data = data;
    }

    public static <T> Result<T> success() {
        return new Result(Code.SUCCESS.getCode(), Code.SUCCESS.getMessage());
    }

    public static <T> Result<T> success(T data) {
        Result<T> result = new Result(Code.SUCCESS.getCode(), Code.SUCCESS.getMessage());
        result.data = data;
        return result;
    }
    public static <T> Result<T> success(T data, String message) {
        Result<T> result = new Result(Code.SUCCESS.getCode(), message);
        result.data = data;
        return result;
    }

    public static <T> Result<T> success(long total, List<T> data) {
        return new Result(total, data, Code.SUCCESS.getCode(), Code.SUCCESS.getMessage());
    }

    public static <T> Result<T> success(long total, List<T> data, String message) {
        return new Result(total, data, Code.SUCCESS.getCode(), message);
    }

    public static <T> Result<T> fail() {
        return new Result(Code.FAILURE.getCode(), Code.FAILURE.getMessage());
    }

    public static <T> Result<T> fail(String message) {
        return new Result(Code.FAILURE.getCode(), message);
    }

    public static <T> Result<T> fail(String code, String message) {
        return new Result(code, message);
    }

    public static <T> Result<T> fail(ServiceException exception) {
        return new Result(exception.getCode(), exception.getMessage());
    }

    public static <T> Result<T> result(ICode code) {
        return new Result(code.getCode(), code.getMessage());
    }

    public static <T> Result<T> result(T data, ICode code) {
        Result<T> result = new Result(code.getCode(), code.getMessage());
        result.data = data;
        return result;
    }

    public static <T> Result<T> result(T data, String code, String message) {
        Result<T> result = new Result(code, message);
        result.data = data;
        return result;
    }

    public boolean isSuccess() {
        return Code.SUCCESS.getCode().equals(this.code);
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public T getData() {
        return this.data;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getTraceId() {
        return this.traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}