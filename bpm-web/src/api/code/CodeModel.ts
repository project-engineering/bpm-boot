//列表查询参数类型
export type ListParam = {
    tableName:string,
    pageNo:number,
    pageSize:number,
    total:number
}