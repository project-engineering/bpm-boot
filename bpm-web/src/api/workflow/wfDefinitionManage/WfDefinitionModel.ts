//角色数据类型
export type AddWfDefinitionModel = {
  // 主要用于区分是新增还是编辑：0-新增；1-编辑
  type:string,
  defId:string,
  defName:string,
  defDesc:string,
}

//列表查询参数类型
export type ListParam = {
  defId:string,
  defName:string,
  parentId:string,
  start:number,
  limit:number,
  total:number
}
