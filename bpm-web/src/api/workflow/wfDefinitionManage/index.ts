import http from "../../../http";
import {type AddWfDefinitionModel, type ListParam} from "./WfDefinitionModel";
//新增角色
export const addApi =(param:AddWfDefinitionModel)=>{
  return http.post('/api/role',param)
}

//列表
export const getListApi =(param:ListParam)=>{
  return http.post('/wfDefinitionManage/listFlowDefinition',param)
}

//编辑
export const editApi =(param:AddWfDefinitionModel)=>{
  return http.put(`/api/role`,param)
}

//删除
export const deleteApi =(defId:string)=>{
  return http.delete(`/wfDefinitionManage/deleteFlowDefinition/${defId}`)
}