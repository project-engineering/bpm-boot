import http from '@/http';
import {type AddUserModel, type ListParam} from "./UserModel";
//列表
export const getSelectApi = () =>{
    return http.get("/api/role/getRoleSelectList");
}

//新增角色
export const addApi =(param:AddUserModel)=>{
    return http.post('/api/user',param)
}

//列表
export const getListApi =(param:ListParam)=>{
    return http.get('/api/user/list',param)
}

//编辑
export const editApi =(param:AddUserModel)=>{
    return http.put(`/api/user`,param)
}

//删除
export const deleteApi =(userId:string)=>{
    return http.delete(`/api/user/${userId}`)
}

//重置密码
export const resetPwdApi =(userId:string)=>{
    return http.post(`/api/user/resetPwd/${userId}`)
}

//根据用户id查询角色信息
export const getRoleByUserIdApi =(userId:string)=>{
    return http.get(`/api/userRole/getRoleByUserId/${userId}`)
}