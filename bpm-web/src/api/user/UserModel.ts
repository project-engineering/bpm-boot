//定义角色下拉选择数据类型
export type RoleSelectType = {
    roleId: string,
    roleName: string
}

//下拉数据列表
export type selectTypeList = {
    list: RoleSelectType[]
}

//角色数据类型
export type AddUserModel = {
    // 主要用于区分是新增还是编辑：0-新增；1-编辑
    type:string,
    userId:string,
    userName:string,
    password:string,
    phone:string,
    email:string,
    sex:string,
    status:string,
    nickName:string
}

//列表查询参数类型
export type ListParam = {
    userId:string,
    userName:string,
    nickName:string,
    phone:string,
    start:number,
    limit:number,
    total:number
}
