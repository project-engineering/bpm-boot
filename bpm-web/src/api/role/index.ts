import http from "@/http";
import {type AddRoleModel, type ListParam} from "./RoleModel";
//新增角色
export const addApi =(param:AddRoleModel)=>{
    return http.post('/api/role',param)
}

//列表
export const getListApi =(param:ListParam)=>{
    return http.get('/api/role/list',param)
}

//编辑
export const editApi =(param:AddRoleModel)=>{
    return http.put(`/api/role`,param)
}

//删除
export const deleteApi =(roleId:string)=>{
    return http.delete(`/api/role/${roleId}`)
}