//角色数据类型
export type AddRoleModel = {
    type:string, // 角色类型
    roleId:string,
    roleName:string,
    remark:string
}

//列表查询参数类型
export type ListParam = {
    roleName:string,
    start:number,
    limit:number,
    total:number
}
