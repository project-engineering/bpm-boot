import http from '@/http';
import {type ListParam} from "./LogModel";

//列表
export const getListApi =(param:ListParam)=>{
    return http.get('/api/log/list',param)
}
