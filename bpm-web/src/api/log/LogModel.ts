
//列表查询参数类型
export type ListParam = {
    userId:string,
    userName:string,
    method:string,
    start:number,
    limit:number,
    total:number
}
