import { ElMessageBox } from "element-plus";
export default function customConfirm(text:string){
    return new Promise((resolve, reject)=>{
        ElMessageBox.confirm(text, '系统提示',
            {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }
        )
            .then(() => {
                // 用户点击确定按钮后的逻辑处理
                resolve(true)
            })
            .catch(() => {
                // 用户点击取消按钮后的逻辑处理
               reject(false)
            })
    }).catch(()=>{
        return false;
    })
}