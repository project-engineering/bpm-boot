import { createRouter, createWebHistory } from 'vue-router'

//导入组件
import Layout from "@/views/layout/Layout.vue";

const routes = [
  {
    path: "/",
    component: Layout,
    name: "dashboard",
    redirect: 'dashboard',
    meta:{
      title: "首页",
      icon: "HomeFilled",
    },
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: ()=> import("@/views/layout/dashboard/Dashboard.vue"),
        meta: {
          title: '首页',
          icon: 'HomeFilled'
        }
      }
    ]
  },
  {
    path: "/system",
    component: Layout,
    name: "system",
    meta:{
      title: "系统管理",
      icon: "el-icon-menu",
      roles: ["sys:manage"]
    },
    children: [
      {
        path: "/userList",
        component: ()=>import("@/views/system/user/UserList.vue"),
        name: "userList",
        meta: {
          title: "用户管理",
          icon: "el-icon-custom",
          roles: ["sys:user"]
        }
      },
      {
        path: "/roleList",
        component: ()=>import("@/views/system/role/RoleList.vue"),
        name: "roleList",
        meta: {
          title: "角色管理",
          icon: "Wallet",
          roles: ["sys:role"]
        }
      },
      {
        path: "/menuList",
        component: ()=>import("@/views/system/menu/MenuList.vue"),
        name: "menuList",
        meta: {
          title: "菜单管理",
          icon: "Menu",
          roles: ["sys:menu"]
        }
      },
      {
        path: "/logList",
        component: ()=>import("@/views/system/log/LogList.vue"),
        name: "logList",
        meta: {
          title: "日志管理",
          icon: "Notebook",
          roles: ["sys:log"]
        }
      },
      {
        path: "/code",
        component: ()=>import("@/views/system/code/CodeList.vue"),
        name: "code",
        meta: {
          title: "代码生成",
          icon: "Tools",
          roles: ["sys:code"]
        }
      }
    ]
  },
  {
    path: "/workflow",
    component: Layout,
    name: "workflow",
    meta:{
      title: "工作流",
      icon: "Setting",
      roles: ["sys:workflow"]
    },
    children: [
      {
        path: "/wfDefinitionList",
        component: ()=>import("@/views/workflow/wfDefinitionManage/WfDefinitionList.vue"),
        name: "wfDefinitionList",
        meta: {
          title: "流程定义",
          icon: "UserFilled",
        }
      },
      {
        path: "/workflow",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "节点定义",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/workflow",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "通用参数",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/workflow",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "事件服务查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/workflow",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "流程实例查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/workflow",
        component: "/processQuery/queryManagerProcInstList",
        name: "goodsList",
        meta: {
          title: "历史实例查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/goodsList",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "已办任务查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/goodsList",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "代办任务查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/goodsList",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "消息发送查询",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      },
      {
        path: "/goodsList",
        component: "/goods/goodsList",
        name: "goodsList",
        meta: {
          title: "规则引擎管理",
          icon: "Wallet",
          roles: ["sys:goodsList"]
        }
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login.vue')
  }
]

//创建路由器
const router = createRouter({
  //不要使用createWebHashHistory，不然会自动在所有访问路径后面加上#/
  history: createWebHistory(),
  routes
})

//导出路由器
export default router
