import {ref} from "vue";
import {type AddRoleModel} from "../../api/role/RoleModel";
import {EditType} from "../../type/BaseEnums";
import {deleteApi} from "../../api/role";
import {ElMessage} from "element-plus";
import {type FuncList} from "../../type/BaseType";
import useInstance from "../../hooks/useInstance";

export default function useRole (getList:FuncList) {
    const {global} = useInstance()
    //新增弹框的ref属性
    //type:0-新增 1-修改 ；row:要编辑的数据
    const addRef = ref<{show:(type:string,row?:AddRoleModel)=>void}>()
    //新增
    const addBtn = () => {
        //父组件调用子组件的方法
        addRef.value?.show(EditType.ADD)
    }
    //修改
    const editBtn = (row:AddRoleModel) => {
        console.log(row)
        //显示弹框：父组件调用子组件
        addRef.value?.show(EditType.EDIT,row)

    }
    //删除
    const delBtn = async (row:AddRoleModel) => {
        //信息确定
        let confirm = await global.$confirm('是否删除该角色？');
        if(confirm) {
            let res = await deleteApi(row.roleId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
                //刷新列表
                getList();
            }
        }
    }

    return {
        addBtn,
        editBtn,
        delBtn,
        addRef
    }
}