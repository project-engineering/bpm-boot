import {ref} from "vue";
import {EditType} from "../../type/BaseEnums";
import {deleteApi, resetPwdApi} from "../../api/user";
import {ElMessage} from "element-plus";
import {type FuncList} from "../../type/BaseType";
import useInstance from "../../hooks/useInstance";
import {type AddUserModel} from "../../api/user/UserModel";
import {type AddRoleModel} from "../../api/role/RoleModel";

export default function useUser (getList:FuncList) {
    const {global} = useInstance()
    //新增弹框的ref属性
    //type:0-新增 1-修改 ；row:要编辑的数据
    const addRef = ref<{show:(type:string,row?:AddUserModel)=>void}>()
    //新增
    const addBtn = () => {
        addRef.value?.show(EditType.ADD)
    }
    //修改
    const editBtn = (row:AddUserModel) => {
        console.log(row)
        //显示弹框：父组件调用子组件
        addRef.value?.show(EditType.EDIT,row)

    }
    //删除
    const delBtn = async (row:AddUserModel) => {
        //信息确定
        let confirm = await global.$confirm('是否删除该用户？');
        if(confirm) {
            let res = await deleteApi(row.userId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
                //刷新列表
                getList();
            }
        }
    }

    //重置密码
    const resetPwdBtn = async (row:AddUserModel) => {
        let confirm = await global.$confirm('是否重置该用户密码？');
        if(confirm) {
            let res = await resetPwdApi(row.userId);
            if(res && res.code == 200) {
                //信息提示
                ElMessage.success(res.message);
            }
        }
    }

    return {
        addBtn,
        editBtn,
        delBtn,
        addRef,
        resetPwdBtn
    }
}