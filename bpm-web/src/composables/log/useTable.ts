import {nextTick, onMounted, reactive, ref} from "vue";
import {type ListParam } from "@/api/log/LogModel";
import {getListApi} from "@/api/log";

export default function useTable(){
    //表格高度
    const tableHeight = ref(0)
    //列表参数
    const listParam = reactive<ListParam>({
        userId: '',
        userName: '',
        method: '',
        start: 1,
        limit: 10,
        total: 0
    })
    //表格数据
    const tableList = reactive({
        list:[]
    })
    //分页
    //列表查询
    const getList = async () => {
        let res = await getListApi(listParam);
        if (res && res.code == "000000"){
            //设置表格数据
            tableList.list = res.data.records;
            //设置分页总条数
            listParam.total = res.data.total;
        }
    }
    //搜索
    const searchBtn = () => {
        getList();
    }
    //重置
    const resetBtn = () => {
        listParam.userId = '';
        listParam.userName = '';
        listParam.method = '';
        getList();
    }
    //页容量改变时触发
    const sizeChange = (val:number)=>{
        listParam.limit = val;
        getList();
    }
    //页码改变时触发
    const currentChange = (val:number)=>{
        listParam.start = val;
        getList();
    }

    //刷新列表
    const refresh = () => {
        getList();
    }

    onMounted(()=>{
        //计算表格高度
        nextTick(()=>{
                tableHeight.value = window.innerHeight - 320;
            }
        )
        //查询列表
        getList();
    })
    return {
        listParam,
        getList,
        searchBtn,
        resetBtn,
        tableList,
        sizeChange,
        currentChange,
        tableHeight,
        refresh
    }
}
