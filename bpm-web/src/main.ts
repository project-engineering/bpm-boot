import { createApp } from 'vue'
//引入element plus
import ElementPlus from 'element-plus'
import locale from 'element-plus/dist/locale/zh-cn.js'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//确定弹框封装
import customConfirm from "@/utils/customConfirm.ts";
import objCopy from "@/utils/objCopy.ts";
//引入路由的js文件
import router from "@/router/index.ts";
import App from "@/App.vue";
//引入Pinia构造函数
import {createPinia} from "pinia";
//实例化Pinia
const pinia = createPinia()

const app = createApp(App)

app.use(ElementPlus,{locale})
app.use(router)
app.use(pinia)
//进行挂载，如果不挂载，页面无法访问
app.mount('#app')

//全局注册图标组件
for(const[key,componet] of Object.entries(ElementPlusIconsVue)){
    app.component(key,componet)
}
//全局挂载弹框
app.config.globalProperties.$confirm = customConfirm
app.config.globalProperties.$objCopy = objCopy