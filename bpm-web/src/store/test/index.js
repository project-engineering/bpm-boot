import {defineStore} from "pinia";

//定义store
export const testStore = defineStore('testStore',{
    //定义state
    state: () => {
        return {
            count: 0,
        }
    },
    //获取数据
    getters: {
        getCount: (state) => this.state.count
    },
    //操作state中的数据
    actions: {
        setCount(count){
            console.log(count)
            this.count = count;
        }
    }
})