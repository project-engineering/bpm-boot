import {defineStore} from "pinia";
//定义一个选项卡数据类型
export type Tab = {
    title: string,
    path: string
}

//定义state类型
export type TabState = {
    tabList: Tab[]
}

//定义一个pinia的store
export const tabStore = defineStore('tabStore',{
    state: ():{ tabList: { path: string; title: string }[]; activeIndex: string } =>{
        return {
            //所有打开的路由
            tabList: [{title: "首页", path: "/dashboard" }],
            // 激活状态，侧边导航和标签页的激活状态，初始为首页
            activeIndex: "/dashboard",
        }
    },
    getters: {
        //获取选项卡数据
        getTabs(state){
            return state.tabList
        }
    },
    actions: {
        //添加选项卡
        addTab(tab:Tab){
            //判断数组里面是否已经存在
            if(this.tabList.some(item =>item.path === tab.path)) return
            //否则添加
            this.tabList.push(tab);
        },
    }
})
