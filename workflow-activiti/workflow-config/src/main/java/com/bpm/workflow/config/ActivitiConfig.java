package com.bpm.workflow.config;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.annotation.Resource;
import org.activiti.engine.*;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * Activiti配置类
 */
@Configuration
public class ActivitiConfig {

    @Resource
    HikariDataSource dataSource;
    @Resource
    DataSourceTransactionManager transactionManager;

    /**
     * 初始化配置，将创建25张表
     * @return
     */
    @Bean
    public SpringProcessEngineConfiguration processEngineConfiguration(){
        SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
        configuration.setDataSource(dataSource);
        configuration.setTransactionManager(transactionManager);
        configuration.setDatabaseSchemaUpdate("true");
        return configuration;
    }

    /**
     * 流程实例类，启动流程时创建
     * @return
     */
    @Bean
    public ProcessEngine processEngine(){
        ProcessEngineConfiguration pro=ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration();
        pro.setDataSource(dataSource);
        pro.setDatabaseSchemaUpdate("true");
        pro.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
        return pro.buildProcessEngine();
    }


    /**
     * 仓库服务类，用于管理bpmn文件与流程图片
     * @return
     */
    @Bean
    public RepositoryService repositoryService(){
        return processEngine().getRepositoryService();
    }

    /**
     * 流程运行服务类，用于获取流程执行相关信息
     * @return
     */
    @Bean
    public RuntimeService runtimeService(){
        return processEngine().getRuntimeService();
    }

    /**
     * 任务服务类，用户获取任务信息
     * @return
     */
    @Bean
    public TaskService taskService(){
        return processEngine().getTaskService();
    }


    /**
     * 获取正在运行或已经完成的流程实例历史信息
     * @return
     */
    @Bean
    public HistoryService historyService(){
        return processEngine().getHistoryService();
    }

    /**
     * 流程引擎的管理与维护
     * @return
     */
    @Bean
    public ManagementService managementService(){
        return processEngine().getManagementService();
    }

}
