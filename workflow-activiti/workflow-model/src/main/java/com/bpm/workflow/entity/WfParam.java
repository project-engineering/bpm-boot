package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_param")
@Tag(name = "WfParam对象", description = "")
public class WfParam {

    @Schema(description = "参数ID")
    @TableId("id")
    private String id;

    @Schema(description = "父级ID")
    @TableField("upid")
    private String upid;

    @Schema(description = "10-事项大类 01-事项小类 CP-业务参数值比较")
    @TableField("param_type")
    private String paramType;

    @Schema(description = "参数编码")
    @TableField("param_code")
    private String paramCode;

    @Schema(description = "参数编码名称")
    @TableField("param_name")
    private String paramName;

    @Schema(description = "参数对象类型")
    @TableField("param_obj_type")
    private String paramObjType;

    @Schema(description = "参数对象编码")
    @TableField("param_obj_code")
    private String paramObjCode;

    @Schema(description = "参数对象名称")
    @TableField("param_obj_name")
    private String paramObjName;

    @Schema(description = "排序顺序")
    @TableField("order_flag")
    private Integer orderFlag;

    @Schema(description = "优先级1-紧急0-正常")
    @TableField("param_urgent")
    private String paramUrgent;
    /**
     * 流程编码
     */
    @TableField(exist = false)
    private String processCode;
    /**
     * 流程名称
     */
    @TableField(exist = false)
    private String processName;
    /**
     * 排序顺序，正整数
     */
    @TableField(exist = false)
    private BigInteger paramOrder;
    /**
     * 排序方式  asc(默认) 或desc
     */
    @TableField(exist = false)
    private String orderType;
    /**
     * 事项渠道类型 00-审批平台 01-区块链 02-APP端 03-PC端
     */
    @TableField(exist = false)
    private String itemChannelType;
    /**
     * 事项渠道类型 00-审批平台 01-区块链 02-APP端 03-PC端
     */
    @TableField(exist = false)
    private List<String> itemChannelTypeList;
}
