package com.bpm.workflow.dto.transfer;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程流转参数实体
 * 
 * @author xuesw
 *
 */
public class WorkFlowTaskTransferData implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5302362940982972297L;

	// 任务标识
	private String taskId;

	// 流程代理处理用户
	private String agentUserId;

	// 流程实例ID
	private String processInstId;

	// 任务流转发起人
	private String userId;

	// 流程定时启动时间
	private Date startTime;

	// 任务截止处理时间
	private Date endTime;
	
	//任务我实例的超时时间
	private Date estEndDate;

	// 系统分类编码
	private String systemIdCode;

	// 流程流转类型：undo 撤回（撤销）、reject驳回(打回)、free自由流转
	private String tranType;

	// 流程撤回、撤销、自由跳转的目标节点ID
	private String targetActivitiId;

	// 流程发起的业务变量
	private Map<String, Object> bussinessMap = new HashMap<>();
	
	//2019-05-05 amt modify start 查询人员信息中根据规则配置进行人员查询 增加uruleMap
	private Map<String, Object> uruleMap = new HashMap<>();
	//2019-05-05 amt modify end 查询人员信息中根据规则配置进行人员查询 增加uruleMap
	private Map<String, Object> userIdMap = new HashMap<>();

	//需要保存的业务数据
	private Map<String, Object>  bussinessSave;

	// 审批结果 0-反对 1-通过 2-再议
	private String result;
	
	/**
	 * 查询的URL地址
	 */
	private String urlIpAddr;
	
	/**
	 * 流程编码
	 */
	private String processCode;
	
	/**
	 * processCodeList,流程编码列表
	 */
	private List<String> processCodeList;
	
	/**
	 * processIdList,流程实例ID列表
	 */
	private List<String> processIdList;

	/**
	 * 附件
	 */
	private String accessories;
	/**
	 * nodeIdCodeList,节点标识编码列表
	 */
	private List<String> nodeIdCodeList;
	
	/**
	 * bussinessList,业务参数列表{"queryIndexa":"123","queryIndexb":"aaa":"queryIndexc":"ccc","queryIndexd":"1"}
	 */
	private List<Map<String,Object>> bussinessList;
	
	/**
	 * cancelType 结束类型：2:正常结束、3:异常结束、4：超时结束
	 */
	private String cancelType;
	private String passingRouteFlag;

	// 审批意见或备注
	private String resultRemark;
	
	
	private boolean doFinishSign=false; 

	private List<String> taskIdList;
	
	private SysCommHead sysCommHead;

	/** 2022/9/26 16:55 流程终止增加终止原因 start by lingengming */
	// 处理意见
	private String doRemark;
	/** 2022/9/26 16:55 流程终止增加终止原因 end by lingengming */
	
	public String getPassingRouteFlag() {
		return passingRouteFlag;
	}

	public void setPassingRouteFlag(String passingRouteFlag) {
		this.passingRouteFlag = passingRouteFlag;
	}

	public Map<String, Object> getBussinessSave() {
		return bussinessSave;
	}

	public void setBussinessSave(Map<String, Object> bussinessSave) {
		this.bussinessSave = bussinessSave;
	}

	
	
	public SysCommHead getSysCommHead() {
		return sysCommHead;
	}

	public void setSysCommHead(SysCommHead sysCommHead) {
		this.sysCommHead = sysCommHead;
	}
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getAgentUserId() {
		return agentUserId;
	}

	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}

	public String getProcessInstId() {
		return processInstId;
	}

	public void setProcessInstId(String processInstId) {
		this.processInstId = processInstId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public String getCancelType() {
		return cancelType;
	}

	public void setCancelType(String cancelType) {
		this.cancelType = cancelType;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSystemIdCode() {
		return systemIdCode;
	}

	public void setSystemIdCode(String systemIdCode) {
		this.systemIdCode = systemIdCode;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getTargetActivitiId() {
		return targetActivitiId;
	}

	public void setTargetActivitiId(String targetActivitiId) {
		this.targetActivitiId = targetActivitiId;
	}

	public Map<String, Object> getBussinessMap() {
		return bussinessMap;
	}

	public void setBussinessMap(Map<String, Object> bussinessMap) {
		this.bussinessMap = bussinessMap;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResultRemark() {
		return resultRemark;
	}

	public void setResultRemark(String resultRemark) {
		this.resultRemark = resultRemark;
	}


	
	
	public Date getEstEndDate() {
		return estEndDate;
	}

	public void setEstEndDate(Date estEndDate) {
		this.estEndDate = estEndDate;
	}

	public boolean isDoFinishSign() {
		return doFinishSign;
	}

	public void setDoFinishSign(boolean doFinishSign) {
		this.doFinishSign = doFinishSign;
	}
 
	public List<String> getTaskIdList() {
		return taskIdList;
	}

	public void setTaskIdList(List<String> taskIdList) {
		this.taskIdList = taskIdList;
	}

	@Override
	public String toString() {
		return "WorkFlowTaskTransferData [taskId=" + taskId + ", agentUserId=" + agentUserId + ", processInstId="
				+ processInstId + ", userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", systemIdCode=" + systemIdCode + ", tranType=" + tranType + ", targetActivitiId=" + targetActivitiId
				+ ", bussinessMap=" + bussinessMap + ", bussinessSave=" + bussinessSave + ", result=" + result
				+ ", resultRemark=" + resultRemark + "]";
	}

	public String getUrlIpAddr() {
		return urlIpAddr;
	}

	public void setUrlIpAddr(String urlIpAddr) {
		this.urlIpAddr = urlIpAddr;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public Map<String, Object> getUruleMap() {
		return uruleMap;
	}

	public void setUruleMap(Map<String, Object> uruleMap) {
		this.uruleMap = uruleMap;
	}

	public List<String> getProcessCodeList() {
		return processCodeList;
	}

	public void setProcessCodeList(List<String> processCodeList) {
		this.processCodeList = processCodeList;
	}

	public List<String> getProcessIdList() {
		return processIdList;
	}

	public void setProcessIdList(List<String> processIdList) {
		this.processIdList = processIdList;
	}

	public List<String> getNodeIdCodeList() {
		return nodeIdCodeList;
	}

	public void setNodeIdCodeList(List<String> nodeIdCodeList) {
		this.nodeIdCodeList = nodeIdCodeList;
	}

	public List<Map<String, Object>> getBussinessList() {
		return bussinessList;
	}

	public void setBussinessList(List<Map<String, Object>> bussinessList) {
		this.bussinessList = bussinessList;
	}

	public String getAccessories() {
		return accessories;
	}

	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}

	public Map<String, Object> getUserIdMap() {
		return userIdMap;
	}

	public void setUserIdMap(Map<String, Object> userIdMap) {
		this.userIdMap = userIdMap;
	}

	/** 2022/9/26 16:55 流程终止增加终止原因 start by lingengming */
	public String getDoRemark() {
		return doRemark;
	}

	public void setDoRemark(String doRemark) {
		this.doRemark = doRemark;
	}
	/** 2022/9/26 16:55 流程终止增加终止原因 end by lingengming */

}
