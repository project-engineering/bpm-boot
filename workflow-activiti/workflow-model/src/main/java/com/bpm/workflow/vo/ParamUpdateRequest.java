package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Data
public class ParamUpdateRequest {

    @NotBlank(message = "id不能为空！")
    @Schema(description = "id", required = true)
    private String id;

    @Schema(description = "父级ID")
    private String upid;

    @NotBlank(message = "参数类型不能为空！")
    @Schema(description = "参数类型：10-事项大类 01-事项小类 C0-业务数据比较参数F0-流程编码实现", required = true)
    private String paramType;

    @NotBlank(message = "参数编码不能为空！")
    @Schema(description = "参数编码", required = true)
    private String paramCode;

    @NotBlank(message = "参数名称不能为空！")
    @Schema(description = "参数名称", required = true)
    private String paramName;

    @Schema(description = "参数对象类型")
    private String paramObjType;

    @Schema(description = "参数对象编码")
    private String paramObjCode;

    @Schema(description = "参数对象名称")
    private String paramObjName;

    @Schema(description = "流程编码")
    private String processCode;

    @Schema(description = "流程名称")
    private String processName;

    @Schema(description = "紧急程度")
    private String paramUrgent;

    @Schema(description = "事项渠道类型")
    private List<String> itemChannelTypeList;

}
