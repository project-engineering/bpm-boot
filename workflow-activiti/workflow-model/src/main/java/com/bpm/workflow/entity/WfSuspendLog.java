package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_suspend_log")
@Tag(name = "WfSuspendLog对象", description = "")
public class WfSuspendLog {

    @Schema(description = "自增标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程实例Id")
    @TableField("process_id")
    private String processId;

    @Schema(description = "流程编码")
    @TableField("process_code")
    private String processCode;

    @Schema(description = "任务Id")
    @TableField("task_id")
    private String taskId;

    @Schema(description = "操作类型0流程操作1任务操作")
    @TableField("opition_type")
    private String opitionType;

    @Schema(description = "操作状态1挂起0取消")
    @TableField("opition_state")
    private String opitionState;

    @Schema(description = "操作人员")
    @TableField("opition_actor")
    private String opitionActor;

    @Schema(description = "操作时间")
    @TableField("opition_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime opitionTime;

    @Schema(description = "操作备注")
    @TableField("opition_remark")
    private String opitionRemark;
}
