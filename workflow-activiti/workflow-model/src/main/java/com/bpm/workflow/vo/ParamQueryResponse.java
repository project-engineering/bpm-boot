package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import java.util.List;

@Data
public class ParamQueryResponse {
	 @Schema(description = "记录Id")
	private String id;

	 @Schema(description = "父级ID")
	private String upid;

	 @Schema(description = "参数类型：10-事项大类 01-事项小类 C0-业务数据比较参数F0-流程编码实现")
	private String paramType;

	 @Schema(description = "参数编码")
	private String paramCode;

	 @Schema(description = "参数编码名称")
	private String paramName;

	 @Schema(description = "参数对象类型")
	private String paramObjType;

	 @Schema(description = "参数对象编码")
	private String paramObjCode;

	 @Schema(description = "参数对象名称")
	private String paramObjName;

	 @Schema(description = "流程编码")
	private String processCode;

	 @Schema(description = "流程名称")
	private String processName;

	 @Schema(description = "优先级 0-正常 1-紧急")
	private String paramUrgent = "0";

	 @Schema(description = "排序顺序，正整数")
	private Integer paramOrder;

	 @Schema(description = "事项渠道类型 00-审批平台 01-区块链 02-APP端 03-PC端")
	private String itemChannelType;

	 @Schema(description = "事项渠道类型 00-审批平台 01-区块链 02-APP端 03-PC端")
	private List<String> itemChannelTypeList;

}
