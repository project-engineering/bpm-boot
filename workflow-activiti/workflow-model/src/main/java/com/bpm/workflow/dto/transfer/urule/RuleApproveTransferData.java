package com.bpm.workflow.dto.transfer.urule;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 
 * 数据传输规则文件组装类
 */
public class RuleApproveTransferData implements Serializable, Cloneable {

    private String id;

    private String projectName;

    private String version;

    private String ruleNo;

    private String filePath;

    private Date createTime;

    private Date updateTime;

    private String state;

    private String xml;

	private String knowledgeId;
	/**
	 * 操作 0新增,1修改,2删除
	 */
	private int operate;
	private static final long serialVersionUID = 1L;
	
	List<URuleRepoTreeTransferData> repoTreeDataList;
	
	List<URuleRepoTreeTransferData> repoTreeDataAllList;
	
	List<URuleFileStoreTransferData> fileStoreDataList;
	
	List<URuleFileStoreTransferData> fileStoreDataAllList;
	
	List<URuleRulesetInterfaceTransferData> rulesetInterfaceDataList;
	
	public List<URuleRepoTreeTransferData> getRepoTreeDataList() {
		return repoTreeDataList;
	}

	public void setRepoTreeDataList(List<URuleRepoTreeTransferData> repoTreeDataList) {
		this.repoTreeDataList = repoTreeDataList;
	}

	public List<URuleFileStoreTransferData> getFileStoreDataList() {
		return fileStoreDataList;
	}

	public void setFileStoreDataList(List<URuleFileStoreTransferData> fileStoreDataList) {
		this.fileStoreDataList = fileStoreDataList;
	}

	public List<URuleRulesetInterfaceTransferData> getRulesetInterfaceDataList() {
		return rulesetInterfaceDataList;
	}

	public void setRulesetInterfaceDataList(List<URuleRulesetInterfaceTransferData> rulesetInterfaceDataList) {
		this.rulesetInterfaceDataList = rulesetInterfaceDataList;
	}

	public List<URuleRepoTreeTransferData> getRepoTreeDataAllList() {
		return repoTreeDataAllList;
	}

	public void setRepoTreeDataAllList(List<URuleRepoTreeTransferData> repoTreeDataAllList) {
		this.repoTreeDataAllList = repoTreeDataAllList;
	}

	public List<URuleFileStoreTransferData> getFileStoreDataAllList() {
		return fileStoreDataAllList;
	}

	public void setFileStoreDataAllList(List<URuleFileStoreTransferData> fileStoreDataAllList) {
		this.fileStoreDataAllList = fileStoreDataAllList;
	}

	public String getKnowledgeId()
	{
		return knowledgeId;
	}

	public void setKnowledgeId(String knowledgeId)
	{
		this.knowledgeId = knowledgeId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getRuleNo()
	{
		return ruleNo;
	}

	public void setRuleNo(String ruleNo)
	{
		this.ruleNo = ruleNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getCreateTime() {
		if (createTime == null)
		{
			return null;
		}
		return (Date)createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		if (createTime == null)
		{
			this.createTime = null;
		} else {
			this.createTime = (Date)createTime.clone();
		}
	}

	public Date getUpdateTime() {
		if (updateTime == null)
		{
			return null;
		}
		return (Date)updateTime.clone();
	}

	public void setUpdateTime(Date updateTime) {
		if (updateTime == null)
		{
			this.updateTime = null;
		} else {
			this.updateTime = (Date)updateTime.clone();
		}
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOperate() {
		return operate;
	}

	public void setOperate(int operate) {
		this.operate = operate;
	}

}
