package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_accedit_log")
@Tag(name = "WfAcceditLog对象", description = "")
public class WfAcceditLog {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程定义")
    @TableField("PROC_DEF_ID")
    private String procDefId;

    @Schema(description = "节点定义")
    @TableField("TASK_DEF_KEY")
    private String taskDefKey;

    @Schema(description = "流程实例id")
    @TableField("PROC_INST_ID")
    private String procInstId;

    @Schema(description = "执行对象id")
    @TableField("EXECUTION_ID")
    private String executionId;

    @Schema(description = "节点名称")
    @TableField("`NAME`")
    private String name;

    @Schema(description = "转出人ID")
    @TableField("send_id")
    private String sendId;

    @Schema(description = "转入人ID")
    @TableField("recive_id")
    private String reciveId;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "执行人")
    @TableField("operator")
    private String operator;

    @Schema(description = "业务数据")
    @TableField("trans_extend")
    private String transExtend;

    @TableField("version")
    @Version
    private Integer version;
}
