package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_evt_service_log")
@Tag(name = "WfEvtServiceLog对象", description = "")
public class WfEvtServiceLog {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "节点id")
    @TableField("task_id")
    private String taskId;

    @Schema(description = "服务事件相关id")
    @TableField("service_id")
    private String serviceId;

    @Schema(description = "流程定义id")
    @TableField("proc_def_id")
    private String procDefId;

    @Schema(description = "流程code")
    @TableField("proc_ext_id")
    private String procExtId;

    @Schema(description = "流程变量")
    @TableField("varinst_ext")
    private String varinstExt;

    @Schema(description = "入参")
    @TableField("input_param")
    private String inputParam;

    @Schema(description = "出参")
    @TableField("output_param")
    private String outputParam;

    @Schema(description = "执行结果")
    @TableField("invock_result")
    private String invockResult;

    @Schema(description = "状态")
    @TableField("`status`")
    private Integer status;

    @Schema(description = "说明")
    @TableField("note")
    private String note;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;
}
