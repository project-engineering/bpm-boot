package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
@Data
public class ParamQueryRequest extends PageDataRequest {
	 @Schema(description = "参数类型：10-事项大类 01-事项小类 C0-业务数据比较参数F0-流程编码实现")
	@NotBlank(message = "参数类型不能为空！")
	private String paramType;

	 @Schema(description = "参数编码")
	private String paramCode;

	 @Schema(description = "参数编码名称")
	private String paramName;

	 @Schema(description = "参数对象类型")
	private String paramObjType;

	 @Schema(description = "参数对象编码")
	private String paramObjCode;

	 @Schema(description = "参数对象名称")
	private String paramObjName;

	 @Schema(description = "排序方式")
	private String orderType;

}
