package com.bpm.workflow.entity;

import lombok.Data;

@Data
public class ParamShowConfig {

    /**
     * 事项小类编码
     */
    private String systemIdCode;

    /**
     * 配置类型
     * 	0-存储字段
     * 	1-查询条件
     * 	2-展示字段
     */
    private String type;

    /**
     * 配置内容
     */
    private String content;
}
