package com.bpm.workflow.dto.transfer;

import java.io.Serializable;

/**
 * 
 * @author xuesw
 * @version V1.0
 * @Description: TODO
 * @date 2018年7月26日
 */
public class WorkFlowDefNode implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2409152500363751332L;
	private String nodeId;
	
	private String nodeName;
	
	private String nodeType;
	
	private String userId;
	
	private String userName;

	private String nodeMsg;
	private String userPostName;
	private String userPost;
	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNodeMsg() {
		return nodeMsg;
	}

	public void setNodeMsg(String nodeMsg) {
		this.nodeMsg = nodeMsg;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPostName() {
		return userPostName;
	}

	public void setUserPostName(String userPostName) {
		this.userPostName = userPostName;
	}

	public String getUserPost() {
		return userPost;
	}

	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}

}
