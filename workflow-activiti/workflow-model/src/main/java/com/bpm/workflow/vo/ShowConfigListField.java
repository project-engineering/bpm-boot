package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.util.List;
import java.util.Map;

@Data
public class ShowConfigListField {

     @Schema(description = "字段key")
    private String fieldKey;
     @Schema(description = "字段描述")
    private String fieldDesc;
     @Schema(description = "码值映射")
    private List<Map<String ,Object>> code;
}
