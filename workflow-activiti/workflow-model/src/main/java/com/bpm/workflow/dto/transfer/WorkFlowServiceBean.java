package com.bpm.workflow.dto.transfer;


import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


public class WorkFlowServiceBean implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(WorkFlowServiceBean.class);
    private static final long serialVersionUID = -2081149137155612960L;

    SysCommHead tranHead;
    SysCommBody tranBody;

    public WorkFlowServiceBean() {
        this.tranHead = new SysCommHead();
        this.tranBody = new SysCommBody();
    }

    public SysCommHead getTranHead() {
        return this.tranHead;
    }

    public void setTranHead(SysCommHead tranHead) {
        this.tranHead = tranHead;
    }

    public SysCommBody getTranBody() {
        return this.tranBody;
    }

    public void setTranBody(SysCommBody tranBody) {
        this.tranBody = tranBody;
    }


    public void resetErrorRetCode(String errMsg) {
        this.tranHead.setRetCode(Constants.RET_FAIL);
        if (!errMsg.isEmpty()) {
            log.error(errMsg);
            this.tranHead.setRetMsg(errMsg);
        }

    }

    public void resetExceptionRetCode(String errMsg) {
        this.tranHead.setRetCode(Constants.RET_EXCEPT);
        if (!errMsg.isEmpty()) {
            log.error(errMsg);
            this.tranHead.setRetMsg(errMsg);
        }

    }

    public String getReturnCode() {
        return this.tranHead.getRetCode();
    }

    public String getReturnMessage() {
        return this.tranHead.getRetMsg();
    }

    public void setReturnMessage(String retMessage) {
        this.tranHead.setRetMsg(retMessage);
    }

    public boolean chkJSONObjectValid() {
        JSONObject jsonObject = this.tranBody.getJsonReqObj();
        if (jsonObject == null) {
            log.error("交易报文体非JSON格式，请确认!!!");
            this.tranHead.setRetCode(Constants.RET_FAIL);
            this.tranHead.setRetMsg("交易报文体非JSON格式，请确认!!!");
            return false;
        }
        return true;
    }

}
