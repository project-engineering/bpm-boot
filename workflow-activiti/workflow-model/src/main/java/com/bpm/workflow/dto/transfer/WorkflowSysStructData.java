package com.bpm.workflow.dto.transfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author xuesw
 * @version V1.0
 * @Description: TODO
 * @date 2018年5月31日
 */
public class WorkflowSysStructData implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8185199184542121861L;

	// 主键ID
	private int id;

	// 节点ID,注意这个是同父节点下同级唯一
	private String sysId;

	// 节点名称
	private String sysName;

	// 父节点主键ID
	private int parentId;
	
	//节点的路径标识
	private String path;

	// 系统自节点（树形结构）
	private List<WorkflowSysStructData> children = new ArrayList<WorkflowSysStructData>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public List<WorkflowSysStructData> getChildren() {
		return children;
	}

	public void setChildren(List<WorkflowSysStructData> children) {
		this.children = children;
	}

	public void addChildren(WorkflowSysStructData wfSysStructData) {
		this.children.add(wfSysStructData);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
