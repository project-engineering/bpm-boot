package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name ="查询节点定义列表入参实体类")
public class NodeDefinitionQueryRequest extends PageRequest {

	@Schema(description = "节点标识")
	private String nodeId;

	@Schema(description = "节点名称")
	private String nodeName;

	@Schema(description = "所属节点Id")
	private String parentId;

	@Schema(description = "节点类型")
	private String type;
	
}
