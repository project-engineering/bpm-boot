package com.bpm.workflow.dto.transfer;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;

import java.io.Serializable;

/**
 * 流程策略选人，请求rest服务输出参数
 * 
 * @author yxz
 *
 */
public class UserOutputData implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5531674621104198527L;
	private String userId;
	private String userName;
	private String userPost;
	private String userPostName;
	private String userRole;
	private String userRoleName;
	private String userBank;
	private String userBankName;
	private String userJobBank;
	private String userJobBankName;
	private String userPhone;
	private String userEmail;
	private String userWeChat;
	private String userState;
	// private List<WaitSelJobRule> waitSelJobRuleList;

	public UserOutputData() {

	}

	public UserOutputData(String userInfoStr) {
		JSONObject json = JSONObject.parseObject(userInfoStr);
		this.initField(json);
	}

	public UserOutputData(JSONObject userInfoJSON) {
		this.initField(userInfoJSON);
	}

	private void initField(JSONObject json) {
		this.setUserId(json.getString("userId"));
		this.setUserName(json.getString("userName"));
		this.setUserBank(json.getString("userBank"));
		this.setUserBankName(json.getString("userBankName"));
		this.setUserEmail(json.getString("userEmail"));
		this.setUserJobBank(json.getString("userJobBank"));
		this.setUserJobBankName(json.getString("userJobBankName"));
		this.setUserPhone(json.getString("userPhone"));
		this.setUserPost(json.getString("userPost"));
		this.setUserPostName(json.getString("userPostName"));
		this.setUserRole(json.getString("userRole"));
		this.setUserRoleName(json.getString("userRoleName"));
		this.setUserState(json.getString("userState"));
		this.setUserWeChat(json.getString("userWeChat"));
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userPost
	 */
	public String getUserPost() {
		return userPost;
	}

	/**
	 * @param userPost
	 *            the userPost to set
	 */
	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}

	/**
	 * @return the userPostName
	 */
	public String getUserPostName() {
		return userPostName;
	}

	/**
	 * @param userPostName
	 *            the userPostName to set
	 */
	public void setUserPostName(String userPostName) {
		this.userPostName = userPostName;
	}

	/**
	 * @return the userRole
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole
	 *            the userRole to set
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the userRoleName
	 */
	public String getUserRoleName() {
		return userRoleName;
	}

	/**
	 * @param userRoleName
	 *            the userRoleName to set
	 */
	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}

	/**
	 * @return the userBank
	 */
	public String getUserBank() {
		return userBank;
	}

	/**
	 * @param userBank
	 *            the userBank to set
	 */
	public void setUserBank(String userBank) {
		this.userBank = userBank;
	}

	/**
	 * @return the userBankName
	 */
	public String getUserBankName() {
		return userBankName;
	}

	/**
	 * @param userBankName
	 *            the userBankName to set
	 */
	public void setUserBankName(String userBankName) {
		this.userBankName = userBankName;
	}

	/**
	 * @return the userJobBank
	 */
	public String getUserJobBank() {
		return userJobBank;
	}

	/**
	 * @param userJobBank
	 *            the userJobBank to set
	 */
	public void setUserJobBank(String userJobBank) {
		this.userJobBank = userJobBank;
	}

	/**
	 * @return the userJobBankName
	 */
	public String getUserJobBankName() {
		return userJobBankName;
	}

	/**
	 * @param userJobBankName
	 *            the userJobBankName to set
	 */
	public void setUserJobBankName(String userJobBankName) {
		this.userJobBankName = userJobBankName;
	}

	/**
	 * @return the userPhone
	 */
	public String getUserPhone() {
		return userPhone;
	}

	/**
	 * @param userPhone
	 *            the userPhone to set
	 */
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	/**
	 * @return the userWeChat
	 */
	public String getUserWeChat() {
		return userWeChat;
	}

	/**
	 * @param userWeChat
	 *            the userWeChat to set
	 */
	public void setUserWeChat(String userWeChat) {
		this.userWeChat = userWeChat;
	}

	/**
	 * @return the userState
	 */
	public String getUserState() {
		return userState;
	}

	/**
	 * @param userState
	 *            the userState to set
	 */
	public void setUserState(String userState) {
		this.userState = userState;
	}


	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}

}
