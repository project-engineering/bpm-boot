package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class ModelPropertyQueryResponse implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String ownId;
	private String ownType;
	private String propName;
	private String propDesc;
	private String propValue;
	private String type;
	private Date createTime;
	private String operator;

}
