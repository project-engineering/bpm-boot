package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Tag(name ="删除节点定义入参实体类")
public class NodeDefinitionDeleteRequest {

	@Schema(description = "节点标识")
	@NotBlank(message = "节点标识不能为空！")
	private String nodeId;

}
