package com.bpm.workflow.dto.transfer;

import org.activiti.engine.runtime.ProcessInstance;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class ProcessInstanceEntityData implements Serializable,Cloneable{
/**
	 * 
	 */
	private static final long serialVersionUID = -738201694810536764L;
	//	  ID_, REV_, PROC_INST_ID_, BUSINESS_KEY_, PARENT_ID_, PROC_DEF_ID_, SUPER_EXEC_, 
	//ROOT_PROC_INST_ID_, ACT_ID_, IS_ACTIVE_, IS_CONCURRENT_, IS_SCOPE_, IS_EVENT_SCOPE_, IS_MI_ROOT_, SUSPENSION_STATE_, CACHED_ENT_STATE_, TENANT_ID_, NAME_, START_TIME_, START_USER_ID_, LOCK_TIME_, IS_COUNT_ENABLED_, EVT_SUBSCR_COUNT_, TASK_COUNT_, JOB_COUNT_, TIMER_JOB_COUNT_, SUSP_JOB_COUNT_, DEADLETTER_JOB_COUNT_, VAR_COUNT_, ID_LINK_COUNT_, ProcessDefinitionKey, ProcessDefinitionId, ProcessDefinitionName, ProcessDefinitionVersion, DeploymentId
	private String activityId;
	private String businessKey;
	private String deploymentId;
	private String description;
	private String id;
	private String localizedDescription;
	private String localizedName;
	private String name;
	private String parentId;
	private String processDefinitionId;
	private String processDefinitionKey;
	private String processDefinitionName;
	private Integer processDefinitionVersion;
	private String processInstanceId;
	private Map<String,Object> processVariables;
	private String pootProcessInstanceId;
 	private Date startTime;
	private String startUserId;
	private String superExecutionId;
	private String tenantId;
	public ProcessInstanceEntityData(ProcessInstance processInstance) {
		// TODO Auto-generated constructor stub
		this.activityId					 =processInstance.getActivityId();
		this.businessKey                  =processInstance.getBusinessKey();
		this.deploymentId                 =processInstance.getDeploymentId();
		this.description                  =processInstance.getDescription();
		this.id                           =processInstance.getId();
		this.localizedDescription         =processInstance.getLocalizedDescription();
		this.localizedName                =processInstance.getLocalizedName();
		this.name                         =processInstance.getName();
		this.parentId                     =processInstance.getParentId();
		this.processDefinitionId          =processInstance.getProcessDefinitionId();
		this.processDefinitionKey         =processInstance.getProcessDefinitionKey();
		this.processDefinitionName        =processInstance.getProcessDefinitionName();
		this.processDefinitionVersion     =processInstance.getProcessDefinitionVersion();
		this.processInstanceId            =processInstance.getProcessInstanceId();
		this.processVariables             =processInstance.getProcessVariables();
		this.pootProcessInstanceId        =processInstance.getRootProcessInstanceId();
		this.startTime                    =processInstance.getStartTime();
		this.startUserId                  =processInstance.getStartUserId();
		this.superExecutionId             =processInstance.getSuperExecutionId() ;
		this.tenantId                     =processInstance.getTenantId();
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getBusinessKey() {
		return businessKey;
	}
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	public String getDeploymentId() {
		return deploymentId;
	}
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocalizedDescription() {
		return localizedDescription;
	}
	public void setLocalizedDescription(String localizedDescription) {
		this.localizedDescription = localizedDescription;
	}
	public String getLocalizedName() {
		return localizedName;
	}
	public void setLocalizedName(String localizedName) {
		this.localizedName = localizedName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}
	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}
	public String getProcessDefinitionName() {
		return processDefinitionName;
	}
	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}
	public Integer getProcessDefinitionVersion() {
		return processDefinitionVersion;
	}
	public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
		this.processDefinitionVersion = processDefinitionVersion;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}
	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}
	public String getPootProcessInstanceId() {
		return pootProcessInstanceId;
	}
	public void setPootProcessInstanceId(String pootProcessInstanceId) {
		this.pootProcessInstanceId = pootProcessInstanceId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getStartUserId() {
		return startUserId;
	}
	public void setStartUserId(String startUserId) {
		this.startUserId = startUserId;
	}
	public String getSuperExecutionId() {
		return superExecutionId;
	}
	public void setSuperExecutionId(String superExecutionId) {
		this.superExecutionId = superExecutionId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
 
	
}
