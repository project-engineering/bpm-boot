package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_upload_definition_detail")
@Tag(name = "WfUploadDefinitionDetail对象", description = "")
public class WfUploadDefinitionDetail {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "主表id")
    @TableField("main_id")
    private Integer mainId;

    @Schema(description = "上级路径")
    @TableField("pid")
    private Integer pid;

    @Schema(description = "类型")
    @TableField("data_type")
    private String dataType;

    @Schema(description = "关键字")
    @TableField("key_word")
    private String keyWord;

    @Schema(description = "导入内容")
    @TableField("detail")
    private String detail;

    @Schema(description = "原来的内容")
    @TableField("old_detail")
    private String oldDetail;

    @Schema(description = "差异")
    @TableField("diff")
    private String diff;

    @Schema(description = "是否规则")
    @TableField("is_rule")
    private String isRule;

    @Schema(description = "是否生效")
    @TableField("is_valid")
    private String isValid;
}
