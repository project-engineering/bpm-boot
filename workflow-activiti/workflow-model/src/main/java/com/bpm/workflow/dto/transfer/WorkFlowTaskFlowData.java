package com.bpm.workflow.dto.transfer;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * TaskFlow Bean
 * 
 * @author xuesw
 * @version V1.0
 * @Description: TODO
 * @date 2018年6月4日
 */
public class WorkFlowTaskFlowData {

	private String processCode;
	
	private String processDesc;

	private String systemCode;

	private String processInstId;

	private String userId;

	private String taskId;

	private String startUserId;

	private Date startDate;

	private Date endStartDate;

	private Date createDate;

	private Date endDate;

	private Date estEndDate;
	private String messageFlag;
	private String workState;

	private boolean isEndProcess;

	private boolean isAllNode;

	private boolean isEndProcessSet;

	private boolean startUserFlag;
	
	private boolean noStartFlag;

	private Integer fromIdx;

	private Integer pageSize;
	private String timeOutFlag;

    // 2019-04-17 amt start modify 增加支持任务池的明细查询、支持优先级、渠道号、霸屏、提醒等条件查询满足 
	private String taskWarn;
	//渠道号
	private String tranChannel;
	//任务池
    private String taskPoolFlag;
	//是否已经查看
	private String viewFlag;

	//是否已经查看
	private String operatorId;
    // 2019-04-17 amt end modify 增加支持任务池的明细查询、支持优先级、渠道号、霸屏、提醒等条件查询满足 

	// 2019-04-17 xuesw start modify 增加支持查询字段排序次序 
	//查询字段排序次序
	private String queryOrder;
	
	//优先级
	private String taskUrgent;
	
	//检查渠道（查询发起渠道）
	private String checkChannel;
	
	private boolean  isChannelCheck;
    // 2019-04-17 xuesw start modify 增加支持查询字段排序次序
	
	//管理人员标志:0-非管理人员 1-管理人员
	//当为1-管理人员时，则忽略任务处理用户ID（userId)字段条件;主要用于任务池管理人员查询所有的任务池任务或任务池历史任务
	private String userManagerFlag;
	// update by xuesw 20180520  增加管理人员标志
	private String itemCtrl;
	
	//update by xuesw 20191114  start  区分对公对私系统的数据功能
	private String sysFlag;

	private List<String> itemSecondCodeList;

	public List<String> getItemSecondCodeList() {
		return itemSecondCodeList;
	}

	public void setItemSecondCodeList(List<String> itemSecondCodeList) {
		this.itemSecondCodeList = itemSecondCodeList;
	}

	public String getSysFlag() {
		return sysFlag;
	}

	public void setSysFlag(String sysFlag) {
		this.sysFlag = sysFlag;
	}
	// update by xuesw 20191114  end  区分对公对私系统的数据功能 

	public Integer getFromIdx() {
		return fromIdx;
	}

	public void setFromIdx(Integer fromIdx) {
		this.fromIdx = fromIdx;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isEndProcessSet() {
		return isEndProcessSet;
	}
	
	private String processState;
	
	/**
	 * ,需由前端自行根据此信息来组织查询条件,如：queryIdex1 > 10 And queryIndex2 like '%AABB%'
	 */
	private String queryCondition;

	private String queryIndexa;
	private String queryIndexb;

	private String timeoutWarn;
	private String isTimeout;
	private BigInteger duedata;

	private String tableName;

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableName() {
		return tableName;
	}

	/**
	 * 流程创建时间-查询历史表使用
	 */
	private String dataCreateDate;

	public void setDataCreateDate(String dataCreateDate) {
		this.dataCreateDate = dataCreateDate;
	}

	public String getDataCreateDate() {
		return dataCreateDate;
	}

	public String getQueryIndexa() {
		return queryIndexa;
	}

	public void setQueryIndexa(String queryIndexa) {
		this.queryIndexa = queryIndexa;
	}

	public String getQueryIndexb() {
		return queryIndexb;
	}

	public void setQueryIndexb(String queryIndexb) {
		this.queryIndexb = queryIndexb;
	}

	public String getQueryIndexc() {
		return queryIndexc;
	}

	public void setQueryIndexc(String queryIndexc) {
		this.queryIndexc = queryIndexc;
	}

	public String getQueryIndexd() {
		return queryIndexd;
	}

	public void setQueryIndexd(String queryIndexd) {
		this.queryIndexd = queryIndexd;
	}

	public String getMessageFlag() {
		return messageFlag;
	}

	public void setMessageFlag(String messageFlag) {
		this.messageFlag = messageFlag;
	}

	private String queryIndexc;
	private String queryIndexd;

	// 流程流转类型：undo 撤回（撤销）、reject驳回(打回)、free自由流转
	private String tranType;

	public boolean isEndProcess() {
		return isEndProcess;
	}

	public void setEndProcessSet(boolean isEndProcessSet) {
		this.isEndProcessSet = isEndProcessSet;
	}

	public String getStartUserId() {
		return startUserId;
	}

	public void setStartUserId(String startUserId) {
		this.startUserId = startUserId;
	}

	public void setEndProcess(boolean isEndProcess) {
		this.isEndProcess = isEndProcess;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getProcessDesc() {
		return processDesc;
	}

	public void setProcessDesc(String processDesc) {
		this.processDesc = processDesc;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getWorkState() {
		return workState;
	}

	public void setWorkState(String workState) {
		this.workState = workState;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isAllNode() {
		return isAllNode;
	}

	public void setAllNode(boolean isAllNode) {
		this.isAllNode = isAllNode;
	}

	public String getProcessInstId() {
		return processInstId;
	}

	public void setProcessInstId(String processInstId) {
		this.processInstId = processInstId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getEstEndDate() {
		return estEndDate;
	}

	public void setEstEndDate(Date estEndDate) {
		this.estEndDate = estEndDate;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getQueryCondition() {
		return queryCondition;
	}

	public void setQueryCondition(String queryCondition) {
		this.queryCondition = queryCondition;
	}



	public boolean isStartUserFlag() {
		return startUserFlag;
	}

	public void setStartUserFlag(boolean startUserFlag) {
		this.startUserFlag = startUserFlag;
	}

	public String getTimeoutWarn() {
		return timeoutWarn;
	}

	public void setTimeoutWarn(String timeoutWarn) {
		this.timeoutWarn = timeoutWarn;
	}

	public String getIsTimeout() {
		return isTimeout;
	}

	public void setIsTimeout(String isTimeout) {
		this.isTimeout = isTimeout;
	}

	public BigInteger getDuedata() {
		return duedata;
	}

	public void setDuedata(BigInteger duedata) {
		this.duedata = duedata;
	}

	public String getProcessState() {
		return processState;
	}

	public void setProcessState(String processState) {
		this.processState = processState;
	}
	
	//时间范围查询 辅助参数
	private Date createDateBegin;
	private Date createDateEnd;

	private Date startDateBegin;
	private Date startDateEnd;
	
	private Date endDateBegin;
	private Date endDateEnd;
	
	private Date doDateBegin;
	private Date doDateEnd;

	public Date getCreateDateBegin() {
		return createDateBegin;
	}

	public void setCreateDateBegin(Date createDateBegin) {
		this.createDateBegin = createDateBegin;
	}

	public Date getStartDateBegin() {
		return startDateBegin;
	}

	public void setStartDateBegin(Date startDateBegin) {
		this.startDateBegin = startDateBegin;
	}

	public Date getStartDateEnd() {
		return startDateEnd;
	}

	public void setStartDateEnd(Date startDateEnd) {
		this.startDateEnd = startDateEnd;
	}

	public Date getCreateDateEnd() {
		return createDateEnd;
	}

	public void setCreateDateEnd(Date createDateEnd) {
		this.createDateEnd = createDateEnd;
	}

	public Date getEndDateBegin() {
		return endDateBegin;
	}

	public void setEndDateBegin(Date endDateBegin) {
		this.endDateBegin = endDateBegin;
	}

	public Date getEndDateEnd() {
		return endDateEnd;
	}

	public void setEndDateEnd(Date endDateEnd) {
		this.endDateEnd = endDateEnd;
	}

	public Date getDoDateBegin() {
		return doDateBegin;
	}

	public void setDoDateBegin(Date doDateBegin) {
		this.doDateBegin = doDateBegin;
	}

	public Date getDoDateEnd() {
		return doDateEnd;
	}

	public void setDoDateEnd(Date doDateEnd) {
		this.doDateEnd = doDateEnd;
	}
	
	private String doResult;
	private String doTaskActor;
	private String processName;
	private String startUser;
	private String taskType;
	private String taskName;
	private String taskCode;
	private Integer workType;
	
	public String getDoResult() {
		return doResult;
	}

	public void setDoResult(String doResult) {
		this.doResult = doResult;
	}

	public String getDoTaskActor() {
		return doTaskActor;
	}

	public void setDoTaskActor(String doTaskActor) {
		this.doTaskActor = doTaskActor;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getStartUser() {
		return startUser;
	}

	public void setStartUser(String startUser) {
		this.startUser = startUser;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getWorkType() {
		return workType;
	}

	public void setWorkType(Integer workType) {
		this.workType = workType;
	}
	
	//子流程字段
	private String parentTaskId;

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	
	private List<String> workStates;

	public List<String> getWorkStates() {
		return workStates;
	}

	public void setWorkStates(List<String> workStates) {
		this.workStates = workStates;
	}

	public boolean isNoStartFlag() {
		return noStartFlag;
	}

	public void setNoStartFlag(boolean noStartFlag) {
		this.noStartFlag = noStartFlag;
	}
	
	private List<String> processIds;

	public List<String> getProcessIds() {
		return processIds;
	}

	public void setProcessIds(List<String> processIds) {
		this.processIds = processIds;
	}

	public String getTaskCode() {
		return taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	
	/***
     * 系统分类编码   大类编码
     */
    private String itemType;
    
    /***
     * 系统分类编码   小类编码
     */
    private String itemNode;
    /***
     * 系统分类编码   小类编码名称
     */
    private String itemName;

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemNode() {
		return itemNode;
	}

	public void setItemNode(String itemNode) {
		this.itemNode = itemNode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	private String startUserBank;
	
	private Date startCreateDate;
	
	private boolean autoNode;
	
	/**
	 * 排除子流程节点任务
	 */
	private boolean excludeSubproc;
	
	public String getStartUserBank() {
		return startUserBank;
	}

	public void setStartUserBank(String startUserBank) {
		this.startUserBank = startUserBank;
	}

	public Date getStartCreateDate() {
		return startCreateDate;
	}

	public void setStartCreateDate(Date startCreateDate) {
		this.startCreateDate = startCreateDate;
	}

	public boolean isAutoNode() {
		return autoNode;
	}

	public void setAutoNode(boolean autoNode) {
		this.autoNode = autoNode;
	}

	public boolean isExcludeSubproc() {
		return excludeSubproc;
	}

	public void setExcludeSubproc(boolean excludeSubproc) {
		this.excludeSubproc = excludeSubproc;
	}

	public String getTaskWarn() {
		return taskWarn;
	}

	public void setTaskWarn(String taskWarn) {
		this.taskWarn = taskWarn;
	}

	public String getTranChannel() {
		return tranChannel;
	}

	public void setTranChannel(String tranChannel) {
		this.tranChannel = tranChannel;
	}

	public String getTaskPoolFlag() {
		return taskPoolFlag;
	}

	public void setTaskPoolFlag(String taskPoolFlag) {
		this.taskPoolFlag = taskPoolFlag;
	}

	public String getViewFlag() {
		return viewFlag;
	}

	public void setViewFlag(String viewFlag) {
		this.viewFlag = viewFlag;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getQueryOrder() {
		return queryOrder;
	}

	public void setQueryOrder(String queryOrder) {
		this.queryOrder = queryOrder;
	}

	public String getTaskUrgent() {
		return taskUrgent;
	}

	public void setTaskUrgent(String taskUrgent) {
		this.taskUrgent = taskUrgent;
	}

	public String getCheckChannel() {
		return checkChannel;
	}

	public void setCheckChannel(String checkChannel) {
		this.checkChannel = checkChannel;
	}
	
	public boolean isChannelCheck() {
		return isChannelCheck;
	}

	public void setChannelCheck(boolean isChannelCheck) {
		this.isChannelCheck = isChannelCheck;
	}

	public String getTimeOutFlag() {
		return timeOutFlag;
	}

	public void setTimeOutFlag(String timeOutFlag) {
		this.timeOutFlag = timeOutFlag;
	}
	
	// update by xuesw 20180520 增加管理人员标志
	public String getUserManagerFlag() {
		return userManagerFlag;
	}

	public void setUserManagerFlag(String userManagerFlag) {
		this.userManagerFlag = userManagerFlag;
	}
	// update by xuesw 20180520 增加管理人员标志

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("----------------WorkFlowTaskFlowData------------------").append(System.lineSeparator());
		sb.append("processCode:").append(getProcessCode());
		sb.append("processInstId:").append(getProcessInstId());
		sb.append("processCode:").append(getProcessCode());
		sb.append("processIds:").append(getProcessIds());
		sb.append("systemCode:").append(getSystemCode());
		sb.append("workState:").append(getWorkState());
		sb.append("startUserId:").append(getStartUserId());
		sb.append("endDate:").append(getEndDate());
		sb.append("endDateBegin:").append(getEndDateBegin());
		sb.append("endDateEnd:").append(getEndDateEnd());
		sb.append("queryCondition:").append(getQueryCondition());
		sb.append("userId:").append(getUserId());
		sb.append("taskType:").append(getTaskType());
		return sb.toString();
	}

	public String getItemCtrl() {
		return itemCtrl;
	}

	public void setItemCtrl(String itemCtrl) {
		this.itemCtrl = itemCtrl;
	}

	public Date getEndStartDate() {
		return endStartDate;
	}

	public void setEndStartDate(Date endStartDate) {
		this.endStartDate = endStartDate;
	}
}
