package com.bpm.workflow.dto.transfer;

import java.io.Serializable;

/**
 * 待办任务分类查询返回对象
 */
public class WorkFlowUnDoTaskData implements Serializable, Cloneable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7063797223623856737L;

	private String systemCode;
	private String littleClassNo;
	private String littleClassName;

	private int classCount;
	// 待办任务分类统计返回超时任务数和紧急任务数
	private int timeOutCount;
	private int urgentCount;

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getLittleClassNo() {
		return littleClassNo;
	}

	public void setLittleClassNo(String littleClassNo) {
		this.littleClassNo = littleClassNo;
	}

	public String getLittleClassName() {
		return littleClassName;
	}

	public void setLittleClassName(String littleClassName) {
		this.littleClassName = littleClassName;
	}

	public int getSysCount() {
		return classCount;
	}

	public void setSysCount(int sysCount) {
		this.classCount = sysCount;
	}

	public int getTimeOutCount() {
		return timeOutCount;
	}

	public void setTimeOutCount(int timeOutCount) {
		this.timeOutCount = timeOutCount;
	}

	public int getUrgentCount() {
		return urgentCount;
	}

	public void setUrgentCount(int urgentCount) {
		this.urgentCount = urgentCount;
	}
}
