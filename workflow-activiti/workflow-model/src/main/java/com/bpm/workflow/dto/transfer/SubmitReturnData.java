package com.bpm.workflow.dto.transfer;


import com.alibaba.fastjson2.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author xuesw
 * @version V1.0
 * @Description: 提交时需要处理的信息
 * @date 2018年5月29日
 */
public class SubmitReturnData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6622496109769201822L;

	/**
	 * 流程实例id
	 */
	private String processInstId;

	private String processState;

	/**
	 * 流转提示信息
	 */
	private String reminder;

	/**
	 * 相关执行人员信息
	 */
	private List<UserOutputData> selectUserList;

	/**
	 * 节点类型
	 */
	private String nodeType;

	/**
	 * 节点名称
	 */
	private String nodeName;
	
	/**
	 * 节点唯一标识
	 */
	private String nodeId;
	
    /**
     * 建模节点ID
     */
	private String nodeRefId;
	
	/**
	 * 建模节点名称
	 */
	private String nodeRefName;

	/**
	 * 元素类型（线、节点）
	 */
	private String type;
	/**
	 * 节点进入线的ID
	 */
	private String outFlowId;

	/**
	 * 是否选择后续流转路径
	 */
	private boolean isSelectOutFlow;

	/**
	 * 是否进行选人操作
	 */
	private boolean isSelectPerson;

	/**
	 * 是否提示确认信息
	 */
	private boolean isPrompt;
	/**
	 * 流程是否结束
	 */
	private boolean isEnd;
	/**
	 * 相关执行人员信息
	 */
	private JSONObject sourceNodeUser;

	/**
	 * 规则调用返回业务参数
	 */
	private String dataReturnMap;


	public String getDataReturnMap() {
		return dataReturnMap;
	}

	public void setDataReturnMap(String returnMap) {
		this.dataReturnMap = returnMap;
	}
	
	public void setDataReturnMap(Object returnMap) {
		if (returnMap != null) {
			this.dataReturnMap = returnMap.toString();
		} else {
			this.dataReturnMap = "";
		}
	}

	/**
	 * 流出路线名称和对应下一节点数据的对应关系
	 */
	private List<SubmitReturnData> nextOutData = new ArrayList<SubmitReturnData>();

	public String getReminder() {
		return reminder;
	}

	public void setReminder(String reminder) {
		this.reminder = reminder;
	}

	public List<UserOutputData> getSelectUserList() {
		return selectUserList;
	}

	public void setSelectUserList(List<UserOutputData> selectUserList) {
		this.selectUserList = selectUserList;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public boolean isEnd() {
		return isEnd;
	}

	public void setEnd(boolean isEnd) {
		this.isEnd = isEnd;
	}

	public List<SubmitReturnData> getNextOutData() {
		return nextOutData;
	}

	public void setNextOutData(List<SubmitReturnData> nextData) {
		this.nextOutData = nextData;
	}

	public void addOutData(SubmitReturnData data) {
		nextOutData.add(data);
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public boolean isSelectOutFlow() {
		return isSelectOutFlow;
	}

	public void setSelectOutFlow(boolean isSelectOutFlow) {
		this.isSelectOutFlow = isSelectOutFlow;
	}

	public boolean isSelectPerson() {
		return isSelectPerson;
	}

	public void setSelectPerson(boolean isSelectPerson) {
		this.isSelectPerson = isSelectPerson;
	}

	public boolean isPrompt() {
		return isPrompt;
	}

	public void setPrompt(boolean isPrompt) {
		this.isPrompt = isPrompt;
	}

	public String getProcessInstId() {
		return processInstId;
	}

	public void setProcessInstId(String processInstId) {
		this.processInstId = processInstId;
	}

	public String getProcessState() {
		return processState;
	}

	public void setProcessState(String processState) {
		this.processState = processState;
	}

	public String getOutFlowId() {
		return outFlowId;
	}

	public void setOutFlowId(String outFlowId) {
		this.outFlowId = outFlowId;
	}

	public JSONObject getSourceNodeUser() {
		return sourceNodeUser;
	}

	public void setSourceNodeUser(JSONObject sourceNodeUser) {
		this.sourceNodeUser = sourceNodeUser;
	}


	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String sourceNodeId) {
		this.nodeId = sourceNodeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNodeRefId() {
		return nodeRefId;
	}

	public void setNodeRefId(String nodeRefId) {
		this.nodeRefId = nodeRefId;
	}

	public String getNodeRefName() {
		return nodeRefName;
	}

	public void setNodeRefName(String nodeRefName) {
		this.nodeRefName = nodeRefName;
	}
 
 
}
