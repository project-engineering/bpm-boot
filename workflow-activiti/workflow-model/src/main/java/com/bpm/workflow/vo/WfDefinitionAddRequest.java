package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class WfDefinitionAddRequest {
    @Schema(description = "流程编码")
    @NotBlank(message="流程编码不能为空！")
    private String defId;

    @Schema(description = "流程名称")
    @NotBlank(message="流程名称不能为空！")
    private String defName;

    @Schema(description = "流程描述")
    private String defDesc;

    @Schema(description = "所属模块id")
    @NotBlank(message="所属模块id不能为空！")
    private String parentId;

    @Schema(description = "操作人")
    private String operator;
}
