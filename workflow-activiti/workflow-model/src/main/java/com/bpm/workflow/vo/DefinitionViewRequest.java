package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DefinitionViewRequest {

	@Schema(description = "记录ID")
	@NotBlank(message = "记录ID不能为空！")
	private String id;
}
