package com.bpm.workflow.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "工作流选人查询userList返回参数")
public class UserAskQueryResponse {
    @Schema(description = "用户编号")
    private String userId;
    @Schema(description = "用户姓名")
    private String userName;
    @Schema(description = "用户所属机构")
    private String userBank;
    @Schema(description = "用户所属机构名称")
    private String userBankName;
    @Schema(description = "手机号码")
    private String phone;

    public UserAskQueryResponse() {
    }

    public String getUserId() {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getUserBank() {
        return this.userBank;
    }

    public String getUserBankName() {
        return this.userBankName;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserBank(String userBank) {
        this.userBank = userBank;
    }

    public void setUserBankName(String userBankName) {
        this.userBankName = userBankName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserAskQueryResponse)) {
            return false;
        } else {
            UserAskQueryResponse other = (UserAskQueryResponse)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$userId = this.getUserId();
                    Object other$userId = other.getUserId();
                    if (this$userId == null) {
                        if (other$userId == null) {
                            break label71;
                        }
                    } else if (this$userId.equals(other$userId)) {
                        break label71;
                    }

                    return false;
                }

                Object this$userName = this.getUserName();
                Object other$userName = other.getUserName();
                if (this$userName == null) {
                    if (other$userName != null) {
                        return false;
                    }
                } else if (!this$userName.equals(other$userName)) {
                    return false;
                }

                label57: {
                    Object this$userBank = this.getUserBank();
                    Object other$userBank = other.getUserBank();
                    if (this$userBank == null) {
                        if (other$userBank == null) {
                            break label57;
                        }
                    } else if (this$userBank.equals(other$userBank)) {
                        break label57;
                    }

                    return false;
                }

                Object this$userBankName = this.getUserBankName();
                Object other$userBankName = other.getUserBankName();
                if (this$userBankName == null) {
                    if (other$userBankName != null) {
                        return false;
                    }
                } else if (!this$userBankName.equals(other$userBankName)) {
                    return false;
                }

                Object this$phone = this.getPhone();
                Object other$phone = other.getPhone();
                if (this$phone == null) {
                    if (other$phone == null) {
                        return true;
                    }
                } else if (this$phone.equals(other$phone)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof UserAskQueryResponse;
    }

    public int hashCode() {
        int result = 1;
        Object $userId = this.getUserId();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $userName = this.getUserName();
        result = result * 59 + ($userName == null ? 43 : $userName.hashCode());
        Object $userBank = this.getUserBank();
        result = result * 59 + ($userBank == null ? 43 : $userBank.hashCode());
        Object $userBankName = this.getUserBankName();
        result = result * 59 + ($userBankName == null ? 43 : $userBankName.hashCode());
        Object $phone = this.getPhone();
        result = result * 59 + ($phone == null ? 43 : $phone.hashCode());
        return result;
    }

    public String toString() {
        return "UserAskQueryResponse(userId=" + this.getUserId() + ", userName=" + this.getUserName() + ", userBank=" + this.getUserBank() + ", userBankName=" + this.getUserBankName() + ", phone=" + this.getPhone() + ")";
    }
}