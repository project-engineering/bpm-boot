package com.bpm.workflow.vo;

import java.io.Serializable;
import java.util.Date;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name ="查询节点定义列表出参实体类")
public class NodeDefinitionQueryResponse  implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1369383763571867798L;
	@Schema(description = "主键")
	private Integer id;

	@Schema(description = "节点标识")
    private String nodeId;

	@Schema(description = "节点名称")
    private String nodeName;

	@Schema(description = "节点类型")
    private String type;

	@Schema(description = "所属节点Id")
    private Integer parentId;

	@Schema(description = "创建时间")
    private Date createTime;

	@Schema(description = "更新时间")
    private Date updateTime;

	@Schema(description = "操作人")
    private String operator;
}
