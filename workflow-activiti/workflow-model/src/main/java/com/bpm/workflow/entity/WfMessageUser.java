package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_message_user")
@Tag(name = "WfMessageUser对象", description = "")
public class WfMessageUser {

    @Schema(description = "提醒任务唯一标识(必须以MMM开头)")
    @TableId("id")
    private String id;

    @Schema(description = "用户标识")
    @TableField("user_id")
    private String userId;

    @Schema(description = "用户名称")
    @TableField("user_name")
    private String userName;

    @Schema(description = "用户机构号")
    @TableField("user_bankId")
    private String userBankid;

    @Schema(description = "用户机构名称")
    @TableField("user_bankName")
    private String userBankname;

    @Schema(description = "消息唯一标识")
    @TableField("msg_id")
    private String msgId;

    @Schema(description = "消息状态(0初始1提醒2已查看)")
    @TableField("msg_state")
    private String msgState;

    @Schema(description = "状态变更时间")
    @TableField("update_date")
    private LocalDateTime updateDate;

    @Schema(description = "扩展字段1")
    @TableField("extend01")
    private String extend01;

    @Schema(description = "扩展字段2")
    @TableField("extend02")
    private String extend02;

    @Schema(description = "扩展字段3")
    @TableField("extend03")
    private String extend03;
}
