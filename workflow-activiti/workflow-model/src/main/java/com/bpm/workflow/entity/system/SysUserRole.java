package com.bpm.workflow.entity.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Builder;
import lombok.Data;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Data
@Builder
@TableName("sys_user_role")
@Tag(name = "SysUserRole对象", description = "用户角色表")
public class SysUserRole {

    @Schema(description = "用户角色主键ID")
    @TableId(value = "user_role_id")
    private String userRoleId;

    @Schema(description = "用户ID")
    @TableField("user_id")
    private String userId;

    @Schema(description = "角色ID")
    @TableField("role_id")
    private String roleId;
}
