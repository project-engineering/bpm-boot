package com.bpm.workflow.dto.transfer.urule;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 数据传输规则接口组装类
 */
public class URuleRulesetInterfaceTransferData implements Serializable, Cloneable {

	private String id;

	private static final long serialVersionUID = 3868030584678015165L;

	private String order;

	private String rulesetId;

	private String serviceUrl;

	private String version;

	private Date createTime;

	private Date updateTime;
	private String paramName;
	/**
	 * 操作 0新增,1修改,2删除
	 */
	private int operate;
	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getRulesetId() {
		return rulesetId;
	}

	public void setRulesetId(String rulesetId) {
		this.rulesetId = rulesetId;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getCreateTime() {
		if (createTime == null) {
			return null;
		}
		return (Date) createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		if (createTime == null) {
			this.createTime = null;
		} else {
			this.createTime = (Date) createTime.clone();
		}
	}

	public Date getUpdateTime() {
		if (updateTime == null) {
			return null;
		}
		return (Date) updateTime.clone();
	}

	public void setUpdateTime(Date updateTime) {
		if (updateTime == null) {
			this.updateTime = null;
		} else {
			this.updateTime = (Date) updateTime.clone();
		}
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOperate() {
		return operate;
	}

	public void setOperate(int operate) {
		this.operate = operate;
	}

}
