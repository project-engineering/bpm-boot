package com.bpm.workflow.dto.transfer.urule;

import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 数据传输参数详情组装类
 */
public class URuleFileElementTransferData implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String elementId;

	private String elementName;

	private String elementLabel;

	private String elementType;

	private String elementResourcetype;

	private String elementConfiginfo;

	private String elementAct;

	private String elementTag;

	private String parentId;

	private String projectName;

	private String fileId;

	private String filePath;

	private Date createTime;

	private Date updateTime;
	/**
	 * 操作 0新增,1修改,2删除
	 */
	private int operate;
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getElementId() {
		return elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId == null ? null : elementId.trim();
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName == null ? null : elementName.trim();
	}

	public String getElementLabel() {
		return elementLabel;
	}

	public void setElementLabel(String elementLabel) {
		this.elementLabel = elementLabel == null ? null : elementLabel.trim();
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType == null ? null : elementType.trim();
	}

	public String getElementResourcetype() {
		return elementResourcetype;
	}

	public void setElementResourcetype(String elementResourcetype) {
		this.elementResourcetype = elementResourcetype == null ? null : elementResourcetype.trim();
	}

	public String getElementConfiginfo() {
		return elementConfiginfo;
	}

	public void setElementConfiginfo(String elementConfiginfo) {
		this.elementConfiginfo = elementConfiginfo == null ? null : elementConfiginfo.trim();
	}

	public String getElementAct() {
		return elementAct;
	}

	public void setElementAct(String elementAct) {
		this.elementAct = elementAct == null ? null : elementAct.trim();
	}

	public String getElementTag() {
		return elementTag;
	}

	public void setElementTag(String elementTag) {
		this.elementTag = elementTag == null ? null : elementTag.trim();
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId == null ? null : fileId.trim();
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath == null ? null : filePath.trim();
	}

	public Date getCreateTime() {
		if (createTime == null) {
			return null;
		}
		return (Date) createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		if (createTime == null) {
			this.createTime = null;
		} else {
			this.createTime = (Date) createTime.clone();
		}
	}

	public Date getUpdateTime() {
		if (updateTime == null) {
			return null;
		}
		return (Date) updateTime.clone();
	}

	public void setUpdateTime(Date updateTime) {
		if (updateTime == null) {
			this.updateTime = null;
		} else {
			this.updateTime = (Date) updateTime.clone();
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOperate() {
		return operate;
	}

	public void setOperate(int operate) {
		this.operate = operate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elementAct == null) ? 0 : elementAct.hashCode());
		result = prime * result + ((elementConfiginfo == null) ? 0 : elementConfiginfo.hashCode());
		result = prime * result + ((elementLabel == null) ? 0 : elementLabel.hashCode());
		result = prime * result + ((elementName == null) ? 0 : elementName.hashCode());
		result = prime * result + ((elementResourcetype == null) ? 0 : elementResourcetype.hashCode());
		result = prime * result + ((elementTag == null) ? 0 : elementTag.hashCode());
		result = prime * result + ((elementType == null) ? 0 : elementType.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result + operate;
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		URuleFileElementTransferData other = (URuleFileElementTransferData) obj;
		if (StrUtil.isEmpty(elementAct)) {
			if (!StrUtil.isEmpty(other.elementAct)) {
				return false;
			}
		} else if (!elementAct.equals(other.elementAct)) {
			return false;
		}
		if (StrUtil.isEmpty(elementConfiginfo)) {
			if (!StrUtil.isEmpty(other.elementConfiginfo)) {
				return false;
			}
		} else if (!elementConfiginfo.equals(other.elementConfiginfo)) {
			return false;
		}
		if (StrUtil.isEmpty(elementLabel)) {
			if (!StrUtil.isEmpty(other.elementLabel)) {
				return false;
			}
		} else if (!elementLabel.equals(other.elementLabel)) {
			return false;
		}
		if (StrUtil.isEmpty(elementName) ) {
			if (!StrUtil.isEmpty(other.elementName)) {
				return false;
			}
		} else if (!elementName.equals(other.elementName)) {
			return false;
		}
		if (StrUtil.isEmpty(elementResourcetype)) {
			if (!StrUtil.isEmpty(other.elementResourcetype)) {
				return false;
			}
		} else if (!elementResourcetype.equals(other.elementResourcetype)) {
			return false;
		}
		if (StrUtil.isEmpty(elementTag)) {
			if (!StrUtil.isEmpty(other.elementTag)) {
				return false;
			}
		} else if (!elementTag.equals(other.elementTag)) {
			return false;
		}
		if (StrUtil.isEmpty(elementType)) {
			if (!StrUtil.isEmpty(other.elementType)) {
				return false;
			}
		} else if (!elementType.equals(other.elementType)) {
			return false;
		}
		if (StrUtil.isEmpty(fileId)) {
			if (!StrUtil.isEmpty(other.fileId)) {
				return false;
			}
		} else if (!fileId.equals(other.fileId)) {
			return false;
		}
		if (StrUtil.isEmpty(filePath)) {
			if (!StrUtil.isEmpty(other.filePath)) {
				return false;
			}
		} else if (!filePath.equals(other.filePath)) {
			return false;
		}
		if (StrUtil.isEmpty(projectName)) {
			return StrUtil.isEmpty(other.projectName);
		} else {
			return projectName.equals(other.projectName);
		}
	}
	
	
}
