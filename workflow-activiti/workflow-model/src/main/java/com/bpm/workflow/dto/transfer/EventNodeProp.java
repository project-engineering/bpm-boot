package com.bpm.workflow.dto.transfer;

/**
 * 节点触发事件配置
 * @author amt
 *
 */
public class EventNodeProp {

	/**
	 * 执行顺序
	 */
	private int orderNo;
	
	
	/**
	 * js表达式
	 */
	private String scriptEval;

	/**
	 * 调用方式：1异步，0同步
	 */
	private String callType;
	
	/**
	 * 配置的事件服务id
	 */
	private String evtServiceId;

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public String getScriptEval() {
		return scriptEval;
	}

	public void setScriptEval(String scriptEval) {
		this.scriptEval = scriptEval;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getEvtServiceId() {
		return evtServiceId;
	}

	public void setEvtId(String evtServiceId) {
		this.evtServiceId = evtServiceId;
	}

	@Override
	public String toString() {
		return "EventNodeProp [orderNo=" + orderNo + ", scriptEval=" + scriptEval + ", callType=" + callType
				+ ", evtServiceId=" + evtServiceId + "]";
	}

	public EventNodeProp(int orderNo, String scriptEval, String callType, String evtServiceId) {
		super();
		this.orderNo = orderNo;
		this.scriptEval = scriptEval;
		this.callType = callType;
		this.evtServiceId = evtServiceId;
	}
	
	
}
