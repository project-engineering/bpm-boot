package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_task_date_conf")
@Tag(name = "WfTaskDateConf对象", description = "")
public class WfTaskDateConf {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "节点id")
    @TableField("node_id")
    private String nodeId;

    @Schema(description = "配置id")
    @TableField("conf_id")
    private String confId;

    @Schema(description = "执行配置时间")
    @TableField("conf_date")
    private String confDate;

    @Schema(description = "上次执行时间")
    @TableField("last_exec_date")
    private String lastExecDate;

    @Schema(description = "配置类型 specific-指定时间运行，schedule-每到指定时间运行")
    @TableField("conf_type")
    private String confType;

    @Schema(description = "执行次数")
    @TableField("runtimes")
    private Integer runtimes;

    @Schema(description = "执行配置时间")
    @TableField("run_month")
    private Integer runMonth;

    @Schema(description = "执行配置时间")
    @TableField("run_day_of_month")
    private Integer runDayOfMonth;

    @Schema(description = "执行配置时间")
    @TableField("run_day_of_week")
    private Integer runDayOfWeek;

    @Schema(description = "执行配置时间")
    @TableField("run_hour_of_day")
    private Integer runHourOfDay;

    @Schema(description = "执行配置时间")
    @TableField("run_minute_of_hour")
    private Integer runMinuteOfHour;

    @Schema(description = "执行配置时间")
    @TableField("run_second_of_minute")
    private Integer runSecondOfMinute;

    @Schema(description = "执行任务类序列化值")
    @TableField("serialized_task")
    private String serializedTask;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @TableField("operator")
    private String operator;

    @TableField("version")
    @Version
    private Integer version;
}
