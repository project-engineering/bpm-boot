package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_view_task_log")
@Tag(name = "WfViewTaskLog对象", description = "")
public class WfViewTaskLog {

    @Schema(description = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "待办任务ID")
    @TableField("task_id")
    private String taskId;

    @Schema(description = "用户id")
    @TableField("user_Id")
    private String userId;

    @Schema(description = "姓名")
    @TableField("user_name")
    private String userName;

    @Schema(description = "查看人信息")
    @TableField("user_info")
    private String userInfo;

    @Schema(description = "查看次数")
    @TableField("view_count")
    private Integer viewCount;

    @Schema(description = "第一次查看时间")
    @TableField("view_first_Time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime viewFirstTime;

    @Schema(description = "最后一次查看时间")
    @TableField("view_cur_Time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime viewCurTime;
}
