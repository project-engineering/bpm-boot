package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ModelPropertyDeleteRequest {
	@Schema(description = "属性所属标识")
	@NotBlank(message = "属性所属标识不能为空！")
	private String ownId;

	@Schema(description = "属性所属类型")
	@NotBlank(message = "属性所属类型不能为空！")
	private String ownType;

	@Schema(description = "属性名称")
	@NotBlank(message = "属性名称不能为空！")
	private String propName;

}
