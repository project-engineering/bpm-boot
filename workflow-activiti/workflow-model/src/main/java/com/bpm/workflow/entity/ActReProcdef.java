package com.bpm.workflow.entity;

import java.io.Serializable;
import java.math.BigInteger;

public class ActReProcdef implements Serializable,Cloneable	{

	private static final long serialVersionUID = 1L;

    private String id;
    private BigInteger rev;
    private String category;
    private String name;
    private String key;
    private BigInteger version;
    private String deploymentId;
    private String resourceName;
    private String dgrmResourceName;
    private String description;
    private BigInteger hasStartFromKey;
    private BigInteger hasGraphicalNotaion;
    private BigInteger suspensionState;
    private String engineVersion;

    
        
    
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public BigInteger getRev() {
		return rev;
	}



	public void setRev(BigInteger rev) {
		this.rev = rev;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getKey() {
		return key;
	}



	public void setKey(String key) {
		this.key = key;
	}



	public BigInteger getVersion() {
		return version;
	}



	public void setVersion(BigInteger version) {
		this.version = version;
	}



	public String getDeploymentId() {
		return deploymentId;
	}



	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}



	public String getResourceName() {
		return resourceName;
	}



	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}



	public String getDgrmResourceName() {
		return dgrmResourceName;
	}



	public void setDgrmResourceName(String dgrmResourceName) {
		this.dgrmResourceName = dgrmResourceName;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public BigInteger getHasStartFromKey() {
		return hasStartFromKey;
	}



	public void setHasStartFromKey(BigInteger hasStartFromKey) {
		this.hasStartFromKey = hasStartFromKey;
	}



	public BigInteger getHasGraphicalNotaion() {
		return hasGraphicalNotaion;
	}



	public void setHasGraphicalNotaion(BigInteger hasGraphicalNotaion) {
		this.hasGraphicalNotaion = hasGraphicalNotaion;
	}



	public BigInteger getSuspensionState() {
		return suspensionState;
	}



	public void setSuspensionState(BigInteger suspensionState) {
		this.suspensionState = suspensionState;
	}



	public String getEngineVersion() {
		return engineVersion;
	}



	public void setEngineVersion(String engineVersion) {
		this.engineVersion = engineVersion;
	}



	@Override
    public String toString()
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ActReProcdef[");
    	sb.append("\n id = ").append(id);
    	sb.append("\n processId = ").append(rev);
    	sb.append("\n processCode = ").append(category);
    	sb.append("\n proccessName = ").append(name);
    	sb.append("\n proccessName = ").append(key);
    	sb.append("\n proccessName = ").append(version);
    	sb.append("\n proccessName = ").append(deploymentId);
    	sb.append("\n proccessName = ").append(resourceName);
    	sb.append("\n proccessName = ").append(dgrmResourceName);
    	sb.append("\n proccessName = ").append(description);
    	sb.append("\n proccessName = ").append(hasStartFromKey);
    	sb.append("\n proccessName = ").append(hasGraphicalNotaion);
    	sb.append("\n proccessName = ").append(suspensionState);
    	sb.append("\n proccessName = ").append(engineVersion);
    	sb.append("\n]");
    	return sb.toString();
    }
}