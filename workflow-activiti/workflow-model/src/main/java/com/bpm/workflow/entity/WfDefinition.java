package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 流程定义表实体类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_definition")
@Tag(name = "WfDefinition对象", description = "流程定义表实体类")
public class WfDefinition {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程定义唯一编码")
    @TableField("def_id")
    private String defId;

    @Schema(description = "流程定义名称")
    @TableField("def_name")
    private String defName;

    @Schema(description = "流程描述")
    @TableField("def_desc")
    private String defDesc;

    @Schema(description = "结构ID")
    @TableField("parent_id")
    private Integer parentId;

    @Schema(description = "Xml定义内容")
    @TableField(value = "xml_source",exist = false)
    private String xmlSource;

    @Schema(description = "json定义内容")
    @TableField(value = "json_source",exist = false)
    private String jsonSource;

    @Schema(description = "流程定义版本")
    @TableField("def_version")
    private Integer defVersion;

    @Schema(description = "发布状态")
    @TableField("public_flag")
    private String publicFlag;

    @Schema(description = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(description = "操作人")
    @TableField("operator")
    private String operator;

    @Schema(description = "乐观锁")
    @TableField("version")
    @Version
    private Integer version;
}
