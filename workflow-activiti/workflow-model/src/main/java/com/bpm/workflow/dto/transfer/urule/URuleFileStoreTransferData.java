package com.bpm.workflow.dto.transfer.urule;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * 数据传输参数文件组装类
 */
public class URuleFileStoreTransferData implements Serializable, Cloneable{

	private static final long serialVersionUID = 1L;
	private String id;
	private String fileId;

	private String fileName;

	private String fileType;

	private String filePath;

	private String fileVersion;

	private String fileContent;

	private Date createTime;

	private Date updateTime;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId == null ? null : fileId.trim();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName == null ? null : fileName.trim();
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType == null ? null : fileType.trim();
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath == null ? null : filePath.trim();
	}

	public String getFileVersion() {
		return fileVersion;
	}

	public void setFileVersion(String fileVersion) {
		this.fileVersion = fileVersion == null ? null : fileVersion.trim();
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent == null ? null : fileContent.trim();
	}

	public Date getCreateTime() {
		if (createTime == null) {
			return null;
		}
		return (Date) createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		if (createTime == null) {
			this.createTime = null;
		} else {
			this.createTime = (Date) createTime.clone();
		}
	}

	public Date getUpdateTime() {
		if (updateTime == null) {
			return null;
		}
		return (Date) updateTime.clone();
	}

	public void setUpdateTime(Date updateTime) {
		if (updateTime == null) {
			this.updateTime = null;
		} else {
			this.updateTime = (Date) updateTime.clone();
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	List<URuleFileElementTransferData> fileElementDataList;
    List<URuleRepoTreeTransferData> repoTreeDataList;
    /**
	 * 操作 0新增,1修改,2删除
	 */
	private int operate;
	public List<URuleFileElementTransferData> getFileElementDataList() {
		return fileElementDataList;
	}

	public void setFileElementDataList(List<URuleFileElementTransferData> fileElementDataList) {
		this.fileElementDataList = fileElementDataList;
	}

	public List<URuleRepoTreeTransferData> getRepoTreeDataList() {
		return repoTreeDataList;
	}

	public void setRepoTreeDataList(List<URuleRepoTreeTransferData> repoTreeDataList) {
		this.repoTreeDataList = repoTreeDataList;
	}

	public int getOperate() {
		return operate;
	}

	public void setOperate(int operate) {
		this.operate = operate;
	}

}
