package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Tag(name ="修改节点定义入参实体类")
public class NodeDefinitionUpdateRequest {

	@Schema(description = "节点记录Id")
	@NotBlank(message = "节点记录Id不能为空！")
	private String id;

	@Schema(description = "节点标识")
	@NotBlank(message = "节点标识不能为空！")
	private String nodeId;

	@Schema(description = "节点名称")
	@NotBlank(message = "节点名称不能为空！")
	private String nodeName;

	@Schema(description = "节点类型")
	@NotBlank(message = "节点类型不能为空！")
	private String type;

}
