package com.bpm.workflow.dto.transfer;

import org.activiti.engine.impl.persistence.entity.IdentityLinkEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;

import java.io.Serializable;
import java.util.List;

public class ProcessDefinitionData implements Serializable,Cloneable{
/**
	 * 
	 */
	private static final long serialVersionUID = 2686686499104569341L;
	//	 ID_, PROC_DEF_ID_, PROC_INST_ID_, EXECUTION_ID_, ACT_ID_, TASK_ID_,
	//CALL_PROC_INST_ID_, ACT_NAME_, ACT_TYPE_, ASSIGNEE_,
	//START_TIME_, END_TIME_, DURATION_, DELETE_REASON_, TENANT_ID_
	private String category;            
	private String deploymentId;        
	private String description;         
	private String diagramResourceName; 
	private String engineVersion;       
	private boolean hasStartFormKey;     
	private Integer historyLevel;        
	private String id;          				
	private List<IdentityLinkEntity> identityLinks;       
	private String key;           			
	private String name;            		
	private Object persistentState;     
	private String resourceName;        
	private int revision;            
	private int revisionNext;        
	private int suspensionState;     
	private String tenantId;            
	private int version;             

	
	public ProcessDefinitionData(ProcessDefinitionEntity processDefinition) {
		category = processDefinition.getCategory();             
		deploymentId = processDefinition.getDeploymentId();         
		description = processDefinition.getDescription();          
		diagramResourceName = processDefinition.getDiagramResourceName();  
		engineVersion = processDefinition.getEngineVersion();        
		hasStartFormKey = processDefinition.getHasStartFormKey();      
		historyLevel = processDefinition.getHistoryLevel();         
		id = processDefinition.getId();                   
		identityLinks = processDefinition.getIdentityLinks();        
		key = processDefinition.getKey();                  
		name = processDefinition.getName();                 
		persistentState = processDefinition.getPersistentState();      
		resourceName = processDefinition.getResourceName();         
		revision = processDefinition.getRevision();             
		revisionNext = processDefinition.getRevisionNext();         
		suspensionState = processDefinition.getSuspensionState();      
		tenantId = processDefinition.getTenantId();             
		version = processDefinition.getVersion();     
 	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getDeploymentId() {
		return deploymentId;
	}


	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDiagramResourceName() {
		return diagramResourceName;
	}


	public void setDiagramResourceName(String diagramResourceName) {
		this.diagramResourceName = diagramResourceName;
	}


	public String getEngineVersion() {
		return engineVersion;
	}


	public void setEngineVersion(String engineVersion) {
		this.engineVersion = engineVersion;
	}


	public boolean isHasStartFormKey() {
		return hasStartFormKey;
	}


	public void setHasStartFormKey(boolean hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}


	public Integer getHistoryLevel() {
		return historyLevel;
	}


	public void setHistoryLevel(Integer historyLevel) {
		this.historyLevel = historyLevel;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public List<IdentityLinkEntity> getIdentityLinks() {
		return identityLinks;
	}


	public void setIdentityLinks(List<IdentityLinkEntity> identityLinks) {
		this.identityLinks = identityLinks;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Object getPersistentState() {
		return persistentState;
	}


	public void setPersistentState(Object persistentState) {
		this.persistentState = persistentState;
	}


	public String getResourceName() {
		return resourceName;
	}


	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}


	public int getRevision() {
		return revision;
	}


	public void setRevision(int revision) {
		this.revision = revision;
	}


	public int getRevisionNext() {
		return revisionNext;
	}


	public void setRevisionNext(int revisionNext) {
		this.revisionNext = revisionNext;
	}


	public int getSuspensionState() {
		return suspensionState;
	}


	public void setSuspensionState(int suspensionState) {
		this.suspensionState = suspensionState;
	}


	public String getTenantId() {
		return tenantId;
	}


	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	} 
	
	
}
