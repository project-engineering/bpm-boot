package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class DefinitionDeployRequest {

	@Schema(description = "流程编码")
	@NotBlank(message="流程编码不能为空！")
	private String defId;

	@Schema(description = "流程版本号")
	@NotBlank(message="流程版本号不能为空！")
	private int defVersion;
}
