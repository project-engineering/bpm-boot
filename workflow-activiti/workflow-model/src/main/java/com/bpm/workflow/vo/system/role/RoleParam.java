package com.bpm.workflow.vo.system.role;

import com.bpm.workflow.vo.PageDataRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleParam extends PageDataRequest {
    @Schema(description = "角色名称")
    private String roleName;
}
