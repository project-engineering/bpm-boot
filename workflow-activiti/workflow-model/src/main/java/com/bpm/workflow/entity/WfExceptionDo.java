package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_exception_do")
@Tag(name = "WfExceptionDo对象", description = "")
public class WfExceptionDo {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程编码")
    @TableField("process_code")
    private String processCode;

    @Schema(description = "异常类型")
    @TableField("exp_type")
    private String expType;

    @Schema(description = "异常模式")
    @TableField("exp_Mode")
    private String expMode;

    @Schema(description = "异常级别")
    @TableField("exp_Rate")
    private Integer expRate;

    @Schema(description = "异常处理模式")
    @TableField("exp_do_type")
    private String expDoType;

    @Schema(description = "自动处理次数")
    @TableField("exp_auto_num")
    private Integer expAutoNum;

    @Schema(description = "自动处理间隔")
    @TableField("exp_do_Interval")
    private Integer expDoInterval;

    @Schema(description = "自动处理方式")
    @TableField("exp_do_way")
    private Integer expDoWay;

    @Schema(description = "自动处理服务")
    @TableField("exp_do_Service")
    private String expDoService;

    @Schema(description = "超时标准时间")
    @TableField("exp_time_out")
    private Integer expTimeOut;

    @Schema(description = "处理人员类型")
    @TableField("exp_do_man_flag")
    private Integer expDoManFlag;

    @Schema(description = "指定处理人员")
    @TableField("exp_do_man_id")
    private String expDoManId;

    @Schema(description = "通知方式")
    @TableField("exp_tell_mode")
    private String expTellMode;

    @Schema(description = "处理人手机")
    @TableField("exp_do_man_tell")
    private String expDoManTell;

    @Schema(description = "处理人邮箱")
    @TableField("exp_do_man_mail")
    private String expDoManMail;

    @TableField("version")
    @Version
    private String version;
}
