package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import java.util.Map;

@Data
public class BusinessDataUpdateRequest {

     @Schema(description = "任务编号", required = true)
    @NotEmpty(message = "任务编号不能为空！")
    private String taskId;

     @Schema(description = "业务数据", required = true)
    @NotEmpty(message = "业务数据不能为空！")
    private Map<String, Object> dataMap;

     @Schema(description = "用户编号", required = true)
    @NotEmpty(message = "用户编号不能为空！")
    private String userId;
}
