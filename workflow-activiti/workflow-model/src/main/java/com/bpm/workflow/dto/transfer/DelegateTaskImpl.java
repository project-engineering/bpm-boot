package com.bpm.workflow.dto.transfer;

import org.activiti.bpmn.model.ActivitiListener;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.impl.persistence.entity.VariableInstance;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.IdentityLink;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class DelegateTaskImpl implements DelegateTask{

	@Override
	public Map<String, Object> getVariables() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getVariables(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getVariables(Collection<String> variableNames, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstances(Collection<String> variableNames,
			boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getVariablesLocal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getVariablesLocal(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getVariablesLocal(Collection<String> variableNames, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, VariableInstance> getVariableInstancesLocal(Collection<String> variableNames,
			boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getVariable(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariableInstance getVariableInstance(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getVariable(String variableName, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariableInstance getVariableInstance(String variableName, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariableInstance getVariableInstanceLocal(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getVariableLocal(String variableName, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariableInstance getVariableInstanceLocal(String variableName, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T getVariable(String variableName, Class<T> variableClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T getVariableLocal(String variableName, Class<T> variableClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getVariableNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getVariableNamesLocal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVariable(String variableName, Object value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setVariable(String variableName, Object value, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object setVariableLocal(String variableName, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object setVariableLocal(String variableName, Object value, boolean fetchAllVariables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVariables(Map<String, ? extends Object> variables) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setVariablesLocal(Map<String, ? extends Object> variables) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasVariables() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasVariablesLocal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasVariable(String variableName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeVariable(String variableName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeVariables(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeVariablesLocal(Collection<String> variableNames) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeVariables() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeVariablesLocal() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTransientVariable(String variableName, Object variableValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTransientVariableLocal(String variableName, Object variableValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTransientVariables(Map<String, Object> transientVariables) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getTransientVariable(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getTransientVariables() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTransientVariablesLocal(Map<String, Object> transientVariables) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getTransientVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getTransientVariablesLocal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeTransientVariableLocal(String variableName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTransientVariable(String variableName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTransientVariables() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTransientVariablesLocal() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDescription(String description) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPriority() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPriority(int priority) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getProcessInstanceId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExecutionId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProcessDefinitionId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getCreateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTaskDefinitionKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSuspended() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getTenantId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFormKey(String formKey) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DelegateExecution getExecution() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEventName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActivitiListener getCurrentActivitiListener() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DelegationState getDelegationState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addCandidateUser(String userId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCandidateUsers(Collection<String> candidateUsers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCandidateGroup(String groupId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCandidateGroups(Collection<String> candidateGroups) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getOwner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setOwner(String owner) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAssignee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAssignee(String assignee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Date getDueDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDueDate(Date dueDate) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCategory(String category) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addUserIdentityLink(String userId, String identityLinkType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addGroupIdentityLink(String groupId, String identityLinkType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteCandidateUser(String userId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteCandidateGroup(String groupId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteUserIdentityLink(String userId, String identityLinkType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteGroupIdentityLink(String groupId, String identityLinkType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<IdentityLink> getCandidates() {
		// TODO Auto-generated method stub
		return null;
	}

}
