package com.bpm.workflow.dto.transfer;

import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.SymbolConstants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 定义工作流平台的公共报文体属性
 */
public class SysCommBody implements Serializable {
    private static final long serialVersionUID = -2243668191603596573L;

    private String uniqueParam = null;
    private String jsonReqData;
    private String jsonRspData;
    private String xmlReqData;
    private String xmlRspData;
    private JSONObject jsonReqObj;
    private JSONObject jsonRspObj;

    private Map<String, Object> mapReqData = null;
    private Map<String, Object> mapRspData = null;


    public String getUniqueParam() {
        return this.uniqueParam;
    }

    public void setUniqueParam(String uniqueParam) {
        this.uniqueParam = uniqueParam;
    }

    public String getJsonReqData() {
        return this.jsonReqData;
    }

    public void setJsonReqData(String jsonReqData) {
        this.jsonReqData = jsonReqData;
    }

    public String getJsonRspData() {
        return this.jsonRspData;
    }

    public void setJsonRspData(String jsonRspData) {
        this.jsonRspData = jsonRspData;
    }

    public String getXmlReqData() {
        return this.xmlReqData;
    }

    public void setXmlReqData(String xmlReqData) {
        this.xmlReqData = xmlReqData;
    }

    public String getXmlRspData() {
        return this.xmlRspData;
    }

    public void setXmlRspData(String xmlRspData) {
        this.xmlRspData = xmlRspData;
    }

    public Map<String, Object> getMapReqData() {
        if (this.mapReqData == null) {
            this.mapReqData = new HashMap<>();
        }
        return this.mapReqData;
    }

    public void setMapReqData(Map<String, Object> mapReqData) {
        this.mapReqData = mapReqData;
    }

    public Map<String, Object> getMapRspData() {
        if (this.mapRspData == null) {
            this.mapRspData = new HashMap<>();
        }
        return this.mapRspData;
    }

    public void setMapRspData(Map<String, Object> mapRspData) {
        this.mapRspData = mapRspData;
    }

    public JSONObject getJsonReqObj() {
        if (this.jsonReqObj == null || this.jsonReqObj.isEmpty()) {
            if (this.jsonReqData == null || this.jsonReqData.isEmpty()) {
                this.jsonReqObj = new JSONObject();
            } else {
                this.jsonReqObj = JSONObject.parseObject(jsonReqData);
            }

        }

        return jsonReqObj;

    }

    public void setJsonReqObj(JSONObject jsonReqObj) {
        this.jsonReqObj = jsonReqObj;
    }

    public JSONObject getJsonRspObj() {
        if (this.jsonRspObj == null || this.jsonRspObj.isEmpty()) {
            if (this.jsonRspData == null || this.jsonRspData.isEmpty()) {
                this.jsonRspObj = new JSONObject();
            } else {
                if (this.jsonRspData.startsWith(SymbolConstants.SYMBOL_003)) {
                    return null;

                } else {
                    this.jsonRspObj = JSONObject.parseObject(jsonRspData);
                }
            }

        }

        return jsonRspObj;
    }

    public void setJsonRspObj(JSONObject jsonRspObj) {
        this.jsonRspObj = jsonRspObj;
        if (!jsonRspObj.isEmpty()) {
            this.jsonRspData = this.jsonRspObj.toString();
        }

    }


}
