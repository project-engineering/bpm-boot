package com.bpm.workflow.dto.transfer;

import com.bpm.workflow.entity.WfHistTaskflow;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfViewTaskLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkFlowTaskData extends WfHistTaskflow {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2397838941153890457L;

	private static final int INT_0 = 0;
	private static final int INT_19 = 19;

	private Map<String,Object> varMap = new HashMap<>();

	private String bigClassNo;
	private String bigClassName;
	private String littleClassNo;
	private String littleClassName;
	private String doResultInfo;
	private String doTaskActorPost;
	private WfInstance subProInstance ;
	private String compareInfo;
	private String errorMessage;
	private String errorStatus;
	//节点相关服务是否异常 0-正常,1-异常
	private String isExcepiton;
	private String timeOutFlag;

	private String viewFlag;
	private String tranChannel;
	private String nodeType;
	private int sysCount;
	private int type;
	//2019-04-18 amt add 轨迹查询 
    private List<WfViewTaskLog> viewUserList;
	//2019-04-18 amt end 轨迹查询

	public String getCompareInfo() {
		return compareInfo;
	}

	public void setCompareInfo(String compareInfo) {
		this.compareInfo = compareInfo;
	}

	public Map<String, Object> getVarMap() {
		return varMap;
	}

	public void setVarMap(Map<String, Object> varMap) {
		this.varMap = varMap;
	}
	
	public void putVarMap(String key,Object value){
		varMap.put(key, value);
	}
	
	public void removeVarMap(String key){
		varMap.remove(key);
	}
	
	public void setBigClassNo(String bigClassNo) {
		this.bigClassNo=bigClassNo;
	}
	public void setBigClassName( String bigClassName) {
		this.bigClassName=bigClassName;
	}

	public void setLittleClassNo(String littleClassNo) {
		this.littleClassNo=littleClassNo;
	}
	public void setLittleClassName(String littleClassName) {
		this.littleClassName=littleClassName;
	}
	public String getBigClassNo() {
		return bigClassNo;
	}
	public String getBigClassName() {
		return bigClassName;
	}

	public String getLittleClassNo() {
		return littleClassNo;
	}
	public String getLittleClassName() {
		return littleClassName;
	}

	public String getDoTaskActorPost() {
		return doTaskActorPost;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getDoResultInfo() {
		return doResultInfo;
	}

	public void setDoResultInfo(String doResultInfo) {
		this.doResultInfo = doResultInfo;
	}

	public void setDoTaskActorPost(String doTaskActorPost) {
		this.doTaskActorPost = doTaskActorPost;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public WfInstance getSubProInstance() {
		return subProInstance;
	}

	public void setSubProInstance(WfInstance subProInstance) {
		this.subProInstance = subProInstance;
	}

	public String getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(String errorStatus) {
		this.errorStatus = errorStatus;
	}

	public String getIsExcepiton() {
		return isExcepiton;
	}

	public void setIsExcepiton(String isExcepiton) {
		this.isExcepiton = isExcepiton;
	}

	public String getTimeOutFlag() {
		return timeOutFlag;
	}

	public List<WfViewTaskLog> getViewUserList() {
		return viewUserList;
	}

	public void setViewUserList(List<WfViewTaskLog> viewUserList) {
		this.viewUserList = viewUserList;
	}

	public String getViewFlag() {
		return viewFlag;
	}

	public void setViewFlag(String viewFlag) {
		this.viewFlag = viewFlag;
	}

	/**
	 * 获取 tranChannel.
	 *
	 * @return 返回 tranChannel
	 */
	@Override
	public String getTranChannel() {
		return tranChannel;
	}

	/**
	 * 设置 tranChannel 的值
	 *
	 * @param tranChannel tranChannel
	 */
	@Override
	public WorkFlowTaskData setTranChannel(String tranChannel) {
		this.tranChannel = tranChannel;
		return this;
	}

	public void setTimeOutFlag(String timeOutFlag) {
		this.timeOutFlag = timeOutFlag;
	}

	public int getSysCount() {
		return sysCount;
	}

	public void setSysCount(int sysCount) {
		this.sysCount = sysCount;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
