package com.bpm.workflow.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 */
public class WfDefinePropExtends implements Serializable,Cloneable	{

	private static final long serialVersionUID = 1L;

    private BigInteger id;
    private String processCode;
    private String taskCode;
    private String propName;
    private String propValue;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
    private BigInteger version;

    public BigInteger getId()
    {
        return id;
    }

    public void setId(BigInteger id)
    {
        this.id = id;
    }

    public String getProcessCode()
    {
        return processCode;
    }

    public void setProcessCode(String processCode)
    {
        this.processCode = processCode;
    }

    public String getTaskCode()
    {
        return taskCode;
    }

    public void setTaskCode(String taskCode)
    {
        this.taskCode = taskCode;
    }

    public String getPropName()
    {
        return propName;
    }

    public void setPropName(String propName)
    {
        this.propName = propName;
    }

    public String getPropValue()
    {
        return propValue;
    }

    public void setPropValue(String propValue)
    {
        this.propValue = propValue;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    public BigInteger getVersion()
    {
        return version;
    }

    public void setVersion(BigInteger version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("WfDefinePropExtends[");

    	sb.append("\n id = ").append(id);
    	sb.append("\n processCode = ").append(processCode);
    	sb.append("\n taskCode = ").append(taskCode);
    	sb.append("\n propName = ").append(propName);
    	sb.append("\n propValue = ").append(propValue);
    	sb.append("\n createTime = ").append(createTime);
    	sb.append("\n updateTime = ").append(updateTime);
    	sb.append("\n version = ").append(version);
    	sb.append("\n]");

    	return sb.toString();
    }
}