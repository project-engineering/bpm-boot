package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ModelPropertyConfigQueryResponse {

	private String propKey;

	private String propName;

	private String type;

	private String groupKey;

	private String propDesc;

	private String orderFlag;

	private List<ModelPropertyConfigQueryResponse> childList = new ArrayList<ModelPropertyConfigQueryResponse>();

}
