package com.bpm.workflow.entity;

import java.io.Serializable;
import java.math.BigInteger;

public class TaskCodeCount implements Serializable,Cloneable	{

	private static final long serialVersionUID = 1L;
	/**
	 * 节点编码
	 */
	private String taskCode;
	/**
	 * 节点名称
	 */
	private String taskName;
	/**
	 * 用户id
	 */
	private String userId;
	
	/**
	 * 代办任务数量
	 */
	private BigInteger num;

	/**
	 * 代办任务数量上限
	 */
	private BigInteger maxCount;
	/**
	 * 待进入的数量
	 */
	private BigInteger undoCount;
	
	public String getTaskCode() {
		return taskCode;
	}
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public BigInteger getNum() {
		return num;
	}
	public void setNum(BigInteger num) {
		this.num = num;
	}
	public BigInteger getMaxCount() {
		return maxCount;
	}
	public void setMaxCount(BigInteger maxCount) {
		this.maxCount = maxCount;
	}
	public BigInteger getUndoCount() {
		return undoCount;
	}
	public void setUndoCount(BigInteger undoCount) {
		this.undoCount = undoCount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
