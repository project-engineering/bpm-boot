package com.bpm.workflow.vo.system.role;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleSelectType {
    private String roleId;
    private String roleName;
}
