package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Log4j2
@TableName("wf_business_data")
@Tag(name = "WfBusinessData对象", description = "")
public class WfBusinessData {

    @Schema(description = "自增主键")
    @TableId("id")
    private String id;

    @Schema(description = "当前待办任务ID")
    @TableField("task_id")
    private String taskId;

    @TableField("process_id")
    private String processId;

    @Schema(description = "流程备注")
    @TableField("wf_desc")
    private String wfDesc;

    @Schema(description = "前一任务成员")
    @TableField("prvious_actor")
    private String prviousActor;

    @TableField("timeout_warn")
    private String timeoutWarn;

    @Schema(description = "当前任务成员信息")
    @TableField("task_actors_data")
    private String taskActorsData;

    @Schema(description = "更改备注")
    @TableField("update_remark")
    private String updateRemark;

    @Schema(description = "当前任务催办信息")
    @TableField("task_do_info")
    private String taskDoInfo;

    @Schema(description = "任务处理URL地址/服务")
    @TableField("work_usr_service")
    private String workUsrService;

    @Schema(description = "任务处理URL标志")
    @TableField("work_url_flag")
    private String workUrlFlag;

    @Schema(description = "任务服务方法入口参数")
    @TableField("work_in_param")
    private String workInParam;

    @Schema(description = "处理附件信息")
    @TableField("do_accessories")
    private String doAccessories;

    @Schema(description = "处理水印签名信息")
    @TableField("do_water_mark")
    private String doWaterMark;

    @Schema(description = "保留的业务参数")
    @TableField("bussiness_map")
    private String bussinessMap;

    public WfBusinessData(WfTaskflow taskflow) {
        this.id=taskflow.getId().toString();
        this.taskId=taskflow.getTaskId();
        this.processId=taskflow.getProcessId();
        this.wfDesc=taskflow.getWfDesc();
        this.prviousActor=taskflow.getPrviousActor();
        this.timeoutWarn=taskflow.getTimeoutWarn();
        this.taskActorsData=taskflow.getTaskActorsData();
        this.updateRemark=taskflow.getUpdateRemark();
        this.taskDoInfo=taskflow.getTaskDoInfo();
        this.workUsrService=taskflow.getWorkUsrService();
        this.workUrlFlag=taskflow.getWorkUrlFlag();
        this.workInParam=taskflow.getWorkInParam();
        this.doAccessories=taskflow.getDoAccessories();
        this.doWaterMark=taskflow.getDoWaterMark();
        this.bussinessMap=taskflow.getBussinessMap();
    }

    private Date formatDate(String theDate) {
        if (Objects.isNull(theDate)) {
            return null;
        }
        try {
            return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(theDate);
        } catch (ParseException e) {
            log.error("日期转换失败:"+theDate,e);
            return null;
        }
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("WfBusinessData[");

        sb.append("\n id = ").append(id);
        sb.append("\n taskId = ").append(taskId);
        sb.append("\n processId = ").append(processId);
        sb.append("\n wfDesc = ").append(wfDesc);
        sb.append("\n prviousActor = ").append(prviousActor);
        sb.append("\n timeoutWarn = ").append(timeoutWarn);
        sb.append("\n taskActorsData = ").append(taskActorsData);
        sb.append("\n updateRemark = ").append(updateRemark);
        sb.append("\n taskDoInfo = ").append(taskDoInfo);
        sb.append("\n workUsrService = ").append(workUsrService);
        sb.append("\n workUrlFlag = ").append(workUrlFlag);
        sb.append("\n workInParam = ").append(workInParam);
        sb.append("\n doAccessories = ").append(doAccessories);
        sb.append("\n doWaterMark = ").append(doWaterMark);
        sb.append("\n bussinessMap = ").append(bussinessMap);
        sb.append("\n]");

        return sb.toString();
    }
}
