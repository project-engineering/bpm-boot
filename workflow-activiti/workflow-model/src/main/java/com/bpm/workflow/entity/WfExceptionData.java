package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.*;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("wf_exception_data")
@Tag(name = "WfExceptionData对象", description = "")
public class WfExceptionData {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "异常类型")
    @TableField("exp_type")
    private String expType;

    @Schema(description = "异常级别")
    @TableField("exp_Rate")
    private Integer expRate;

    @Schema(description = "异常模式")
    @TableField("exp_Mode")
    private String expMode;

    @Schema(description = "补充登记具体的异常信息")
    @TableField("exp_message")
    private String expMessage;

    @Schema(description = "节点id")
    @TableField("exp_task_id")
    private String expTaskId;

    @Schema(description = "服务事件相关id")
    @TableField("evt_service_id")
    private String evtServiceId;

    @Schema(description = "流程定义id")
    @TableField("exp_proc_id")
    private String expProcId;

    @Schema(description = "流程编码")
    @TableField("exp_proc_code")
    private String expProcCode;

    @Schema(description = "异常节点类型")
    @TableField("exp_node_type")
    private String expNodeType;

    @Schema(description = "异常节点名称")
    @TableField("exp_node_name")
    private String expNodeName;

    @Schema(description = "异常业务数据")
    @TableField("exp_trans_data")
    private String expTransData;

    @Schema(description = "异常处理状态")
    @TableField("exp_do_flag")
    private Integer expDoFlag;

    @Schema(description = "异常处理备注")
    @TableField("exp_do_msg")
    private String expDoMsg;

    @Schema(description = "指定处理人员")
    @TableField("exp_user_id")
    private String expUserId;

    @Schema(description = "原定处理人员")
    @TableField("exp_old_user_id")
    private String expOldUserId;

    @Schema(description = "已处理次数")
    @TableField("exp_nums")
    private Integer expNums;

    @Schema(description = "异常业务数据")
    @TableField("version")
    @Version
    private String version;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    public WfExceptionData(String expType, String expMode, String expMessage, String expTaskId, String evtServiceId,
                           String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData) {
        super();
        this.expType = expType;
        this.expMode = expMode;
        this.expMessage = expMessage;
        this.expTaskId = expTaskId;
        this.evtServiceId = evtServiceId;
        this.expProcId = expProcId;
        this.expProcCode = expProcCode;
        this.expNodeType = expNodeType;
        this.expNodeName = expNodeName;
        this.expTransData = expTransData;
        this.expRate= 1;
        this.setExpRate(1);
        this.setCreateTime(LocalDateTime.now());
        this.expNums=0;
        this.expDoFlag= 0;
    }
}
