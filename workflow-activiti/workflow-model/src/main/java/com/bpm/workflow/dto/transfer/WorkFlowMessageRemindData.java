package com.bpm.workflow.dto.transfer;

import com.bpm.workflow.entity.WfMessageContent;
import com.bpm.workflow.entity.WfMessageUser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorkFlowMessageRemindData extends WfMessageContent implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7514311288252044611L;

	private List<WfMessageUser> msgUserList;

	public List<WfMessageUser> getMsgUserList() {
		return msgUserList;
	}

	public void setMsgUserList(List<WfMessageUser> msgUserList) {
		this.msgUserList = msgUserList;
	}

	public void addMsgUser(WfMessageUser wfMsgUser) {
		if (msgUserList == null) {
			msgUserList = new ArrayList<>();
		}
		msgUserList.add(wfMsgUser);
	}

}
