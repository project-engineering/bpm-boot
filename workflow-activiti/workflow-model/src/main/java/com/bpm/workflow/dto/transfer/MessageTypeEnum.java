package com.bpm.workflow.dto.transfer;

public enum MessageTypeEnum {
	EMAIL("email","2"),WECHAT("wechat","3"),SMS("sms","1");
 
	private String name;
	private String type;

	/**
	 * 邮件
	 */
	private static final String MESSAGE_TYPE_EMAIL = "2";
	/**
	 * 微信
	 */
	private static final String MESSAGE_TYPE_WECHAT = "3";
	/**
	 * 短信
	 */
	private static final String MESSAGE_TYPE_SMS = "1";

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	MessageTypeEnum(String name, String type) {
		this.name=name;
		this.type=type;
	}
	
	public static MessageTypeEnum getEnumByValue(String type)
	{
		if(MESSAGE_TYPE_EMAIL.equals(type))
		{
			return MessageTypeEnum.EMAIL;
		}
		if(MESSAGE_TYPE_WECHAT.equals(type))
		{
			return MessageTypeEnum.WECHAT;
		}
		if(MESSAGE_TYPE_SMS.equals(type))
		{
			return MessageTypeEnum.SMS;
		}
		return null;
	}
}
