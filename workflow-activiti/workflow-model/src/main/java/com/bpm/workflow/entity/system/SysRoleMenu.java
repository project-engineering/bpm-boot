package com.bpm.workflow.entity.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_role_menu")
@Tag(name = "SysRoleMenu对象", description = "角色菜单表")
public class SysRoleMenu {

    @Schema(description = "角色菜单主键ID")
    @TableId(value = "role_menu_id")
    private String roleMenuId;

    @Schema(description = "角色ID")
    @TableField("role_id")
    private String roleId;

    @Schema(description = "菜单ID")
    @TableField("menu_id")
    private String menuId;
}
