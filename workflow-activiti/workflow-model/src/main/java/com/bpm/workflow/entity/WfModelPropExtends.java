package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_model_prop_extends")
@Tag(name = "WfModelPropExtends对象", description = "")
public class WfModelPropExtends {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("own_id")
    private String ownId;

    @TableField("own_type")
    private String ownType;

    @TableField("prop_name")
    private String propName;

    @TableField("prop_desc")
    private String propDesc;

    @TableField("prop_value")
    private String propValue;

    @TableField("`type`")
    private String type;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @TableField("operator")
    private String operator;

    @TableField("version")
    @Version
    private Integer version;
}
