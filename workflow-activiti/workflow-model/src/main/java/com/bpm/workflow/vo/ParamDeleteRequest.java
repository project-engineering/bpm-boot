package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ParamDeleteRequest {
     @Schema(description = "记录Id")
    @NotBlank(message = "记录Id不能为空！")
    private String id;

}
