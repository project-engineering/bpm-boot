package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_node_state")
@Tag(name = "WfNodeState对象", description = "")
public class WfNodeState {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "事件类型编码")
    @TableField("event_type")
    private String eventType;

    @Schema(description = "状态类型")
    @TableField("state_type")
    private String stateType;

    @Schema(description = "流程编码")
    @TableField("process_code")
    private String processCode;

    @Schema(description = "流程名称")
    @TableField("process_name")
    private String processName;

    @Schema(description = "节点标识")
    @TableField("node_id")
    private String nodeId;

    @Schema(description = "节点名称")
    @TableField("node_name")
    private String nodeName;

    @Schema(description = "业务状态")
    @TableField("node_state")
    private String nodeState;

    @Schema(description = "业务状态名称")
    @TableField("node_state_name")
    private String nodeStateName;
}
