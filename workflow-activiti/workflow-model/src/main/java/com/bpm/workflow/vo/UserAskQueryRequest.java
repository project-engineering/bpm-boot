package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Pattern;

@Tag(name = "工作流选人查询userAskList请求参数")
public class UserAskQueryRequest {
    @Schema(description = "选中是否退出标志<br>0-适配人员非空时退出，后继策略不处理<br>1-不退出，继续下一策略选人<br>默认 1（不退出）")
    private @Pattern(
            regexp = "[0-1]",
            message = "选中是否退出标志参数异常！"
    ) String exitFlag;
    @Schema(description = "适配人员工号")
    private String orderUserNo;
    @Schema(description = "干系人工号")
    private String userId;
    @Schema(description = "适配目标人员机构级别<br>101-总行<br>102-省行<br>103-市行<br>104-县行")
    private @Pattern(
            regexp = "^10[1-4]$",
            message = "适配目标人员机构级别参数异常！"
    ) String orderBankRate;
    @Schema(description = "适配目标人员机构层级<br>1-上级机构<br>2-本级机构")
    private @Pattern(
            regexp = "[1-2]",
            message = "适配目标人员机构层级参数异常！"
    ) String orderBankGrade;
    @Schema(description = "适配目标人员机构编码")
    private String orderBankNo;
    @Schema(description = "适配目标人员岗位编码")
    private String orderUserPost;
    @Schema(description = "渠道类型 SESL-智贷审批平台 APP-APP端 PC-PC端 APP_PC-APP/PC")
    private String channelType;
    @Schema(description = "上级机构是否相等")
    private @Pattern(
            regexp = "[1-2]",
            message = "适配目标人员上级机构参数异常！"
    ) String parentOrgEquals;

    public UserAskQueryRequest() {
    }

    public String getExitFlag() {
        return this.exitFlag;
    }

    public String getOrderUserNo() {
        return this.orderUserNo;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getOrderBankRate() {
        return this.orderBankRate;
    }

    public String getOrderBankGrade() {
        return this.orderBankGrade;
    }

    public String getOrderBankNo() {
        return this.orderBankNo;
    }

    public String getOrderUserPost() {
        return this.orderUserPost;
    }

    public String getChannelType() {
        return this.channelType;
    }

    public String getParentOrgEquals() {
        return this.parentOrgEquals;
    }

    public void setExitFlag(String exitFlag) {
        this.exitFlag = exitFlag;
    }

    public void setOrderUserNo(String orderUserNo) {
        this.orderUserNo = orderUserNo;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setOrderBankRate(String orderBankRate) {
        this.orderBankRate = orderBankRate;
    }

    public void setOrderBankGrade(String orderBankGrade) {
        this.orderBankGrade = orderBankGrade;
    }

    public void setOrderBankNo(String orderBankNo) {
        this.orderBankNo = orderBankNo;
    }

    public void setOrderUserPost(String orderUserPost) {
        this.orderUserPost = orderUserPost;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public void setParentOrgEquals(String parentOrgEquals) {
        this.parentOrgEquals = parentOrgEquals;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserAskQueryRequest)) {
            return false;
        } else {
            UserAskQueryRequest other = (UserAskQueryRequest)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$exitFlag = this.getExitFlag();
                    Object other$exitFlag = other.getExitFlag();
                    if (this$exitFlag == null) {
                        if (other$exitFlag == null) {
                            break label119;
                        }
                    } else if (this$exitFlag.equals(other$exitFlag)) {
                        break label119;
                    }

                    return false;
                }

                Object this$orderUserNo = this.getOrderUserNo();
                Object other$orderUserNo = other.getOrderUserNo();
                if (this$orderUserNo == null) {
                    if (other$orderUserNo != null) {
                        return false;
                    }
                } else if (!this$orderUserNo.equals(other$orderUserNo)) {
                    return false;
                }

                label105: {
                    Object this$userId = this.getUserId();
                    Object other$userId = other.getUserId();
                    if (this$userId == null) {
                        if (other$userId == null) {
                            break label105;
                        }
                    } else if (this$userId.equals(other$userId)) {
                        break label105;
                    }

                    return false;
                }

                Object this$orderBankRate = this.getOrderBankRate();
                Object other$orderBankRate = other.getOrderBankRate();
                if (this$orderBankRate == null) {
                    if (other$orderBankRate != null) {
                        return false;
                    }
                } else if (!this$orderBankRate.equals(other$orderBankRate)) {
                    return false;
                }

                label91: {
                    Object this$orderBankGrade = this.getOrderBankGrade();
                    Object other$orderBankGrade = other.getOrderBankGrade();
                    if (this$orderBankGrade == null) {
                        if (other$orderBankGrade == null) {
                            break label91;
                        }
                    } else if (this$orderBankGrade.equals(other$orderBankGrade)) {
                        break label91;
                    }

                    return false;
                }

                Object this$orderBankNo = this.getOrderBankNo();
                Object other$orderBankNo = other.getOrderBankNo();
                if (this$orderBankNo == null) {
                    if (other$orderBankNo != null) {
                        return false;
                    }
                } else if (!this$orderBankNo.equals(other$orderBankNo)) {
                    return false;
                }

                label77: {
                    Object this$orderUserPost = this.getOrderUserPost();
                    Object other$orderUserPost = other.getOrderUserPost();
                    if (this$orderUserPost == null) {
                        if (other$orderUserPost == null) {
                            break label77;
                        }
                    } else if (this$orderUserPost.equals(other$orderUserPost)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$channelType = this.getChannelType();
                    Object other$channelType = other.getChannelType();
                    if (this$channelType == null) {
                        if (other$channelType == null) {
                            break label70;
                        }
                    } else if (this$channelType.equals(other$channelType)) {
                        break label70;
                    }

                    return false;
                }

                Object this$parentOrgEquals = this.getParentOrgEquals();
                Object other$parentOrgEquals = other.getParentOrgEquals();
                if (this$parentOrgEquals == null) {
                    if (other$parentOrgEquals != null) {
                        return false;
                    }
                } else if (!this$parentOrgEquals.equals(other$parentOrgEquals)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof UserAskQueryRequest;
    }

    public int hashCode() {
        int result = 1;
        Object $exitFlag = this.getExitFlag();
        result = result * 59 + ($exitFlag == null ? 43 : $exitFlag.hashCode());
        Object $orderUserNo = this.getOrderUserNo();
        result = result * 59 + ($orderUserNo == null ? 43 : $orderUserNo.hashCode());
        Object $userId = this.getUserId();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $orderBankRate = this.getOrderBankRate();
        result = result * 59 + ($orderBankRate == null ? 43 : $orderBankRate.hashCode());
        Object $orderBankGrade = this.getOrderBankGrade();
        result = result * 59 + ($orderBankGrade == null ? 43 : $orderBankGrade.hashCode());
        Object $orderBankNo = this.getOrderBankNo();
        result = result * 59 + ($orderBankNo == null ? 43 : $orderBankNo.hashCode());
        Object $orderUserPost = this.getOrderUserPost();
        result = result * 59 + ($orderUserPost == null ? 43 : $orderUserPost.hashCode());
        Object $channelType = this.getChannelType();
        result = result * 59 + ($channelType == null ? 43 : $channelType.hashCode());
        Object $parentOrgEquals = this.getParentOrgEquals();
        result = result * 59 + ($parentOrgEquals == null ? 43 : $parentOrgEquals.hashCode());
        return result;
    }

    public String toString() {
        return "UserAskQueryRequest(exitFlag=" + this.getExitFlag() + ", orderUserNo=" + this.getOrderUserNo() + ", userId=" + this.getUserId() + ", orderBankRate=" + this.getOrderBankRate() + ", orderBankGrade=" + this.getOrderBankGrade() + ", orderBankNo=" + this.getOrderBankNo() + ", orderUserPost=" + this.getOrderUserPost() + ", channelType=" + this.getChannelType() + ", parentOrgEquals=" + this.getParentOrgEquals() + ")";
    }
}