package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

@Data
@Tag(name = "分页对象")
public class PageDataRequest {
	@Schema(description = "页码",defaultValue = "1")
	private int start;

	@Schema(description = "每页条数",defaultValue = "10")
	private int limit;
}
