package com.bpm.workflow.vo;


import java.util.Map;

public class WorkflowCallBackRequest {
    private String processId;
    private String approvalResult;
    private String lastApprovalUser;
    private String systemIdCode;
    private String taskId;
    private Map<String, Object> dataMap;

    public WorkflowCallBackRequest() {
    }

    public String getProcessId() {
        return this.processId;
    }

    public String getApprovalResult() {
        return this.approvalResult;
    }

    public String getLastApprovalUser() {
        return this.lastApprovalUser;
    }

    public String getSystemIdCode() {
        return this.systemIdCode;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public Map<String, Object> getDataMap() {
        return this.dataMap;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public void setApprovalResult(String approvalResult) {
        this.approvalResult = approvalResult;
    }

    public void setLastApprovalUser(String lastApprovalUser) {
        this.lastApprovalUser = lastApprovalUser;
    }

    public void setSystemIdCode(String systemIdCode) {
        this.systemIdCode = systemIdCode;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public void setDataMap(Map<String, Object> dataMap) {
        this.dataMap = dataMap;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof WorkflowCallBackRequest)) {
            return false;
        } else {
            WorkflowCallBackRequest other = (WorkflowCallBackRequest)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$processId = this.getProcessId();
                Object other$processId = other.getProcessId();
                if (this$processId == null) {
                    if (other$processId != null) {
                        return false;
                    }
                } else if (!this$processId.equals(other$processId)) {
                    return false;
                }

                Object this$approvalResult = this.getApprovalResult();
                Object other$approvalResult = other.getApprovalResult();
                if (this$approvalResult == null) {
                    if (other$approvalResult != null) {
                        return false;
                    }
                } else if (!this$approvalResult.equals(other$approvalResult)) {
                    return false;
                }

                Object this$lastApprovalUser = this.getLastApprovalUser();
                Object other$lastApprovalUser = other.getLastApprovalUser();
                if (this$lastApprovalUser == null) {
                    if (other$lastApprovalUser != null) {
                        return false;
                    }
                } else if (!this$lastApprovalUser.equals(other$lastApprovalUser)) {
                    return false;
                }

                label62: {
                    Object this$systemIdCode = this.getSystemIdCode();
                    Object other$systemIdCode = other.getSystemIdCode();
                    if (this$systemIdCode == null) {
                        if (other$systemIdCode == null) {
                            break label62;
                        }
                    } else if (this$systemIdCode.equals(other$systemIdCode)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$taskId = this.getTaskId();
                    Object other$taskId = other.getTaskId();
                    if (this$taskId == null) {
                        if (other$taskId == null) {
                            break label55;
                        }
                    } else if (this$taskId.equals(other$taskId)) {
                        break label55;
                    }

                    return false;
                }

                Object this$dataMap = this.getDataMap();
                Object other$dataMap = other.getDataMap();
                if (this$dataMap == null) {
                    if (other$dataMap != null) {
                        return false;
                    }
                } else if (!this$dataMap.equals(other$dataMap)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof WorkflowCallBackRequest;
    }

    public int hashCode() {
        int result = 1;
        Object $processId = this.getProcessId();
        result = result * 59 + ($processId == null ? 43 : $processId.hashCode());
        Object $approvalResult = this.getApprovalResult();
        result = result * 59 + ($approvalResult == null ? 43 : $approvalResult.hashCode());
        Object $lastApprovalUser = this.getLastApprovalUser();
        result = result * 59 + ($lastApprovalUser == null ? 43 : $lastApprovalUser.hashCode());
        Object $systemIdCode = this.getSystemIdCode();
        result = result * 59 + ($systemIdCode == null ? 43 : $systemIdCode.hashCode());
        Object $taskId = this.getTaskId();
        result = result * 59 + ($taskId == null ? 43 : $taskId.hashCode());
        Object $dataMap = this.getDataMap();
        result = result * 59 + ($dataMap == null ? 43 : $dataMap.hashCode());
        return result;
    }

    public String toString() {
        return "WorkflowCallBackRequest(processId=" + this.getProcessId() + ", approvalResult=" + this.getApprovalResult() + ", lastApprovalUser=" + this.getLastApprovalUser() + ", systemIdCode=" + this.getSystemIdCode() + ", taskId=" + this.getTaskId() + ", dataMap=" + this.getDataMap() + ")";
    }
}