package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.util.List;
import java.util.Map;

@Data
public class ShowConfigQueryCondition {

     @Schema(description = "展示描述")
    private String showDesc;
     @Schema(description = "查询key")
    private String queryKey;
     @Schema(description = "查询条件")
    private String queryCondition;
     @Schema(description = "码值映射")
    private List<Map<String ,Object>> code;
     @Schema(description = "类型")
    private String type;
     @Schema(description = "最大长度")
    private Integer maxLength;
}
