package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ModelPropertyConfigQueryRequest {

	@Schema(description = "模型类型")
	@NotBlank(message = "模型类型不能为空！")
	private String type;

}
