package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_prop_config")
@Tag(name = "WfPropConfig对象", description = "")
public class WfPropConfig {

    @Schema(description = "属性键值")
    @TableId("prop_key")
    private String propKey;

    @Schema(description = "属性键值")
    @TableField("prop_name")
    private String propName;

    @Schema(description = "节点类型")
    @TableField("`type`")
    private String type;

    @Schema(description = "所属组的key标识")
    @TableField("group_key")
    private String groupKey;

    @Schema(description = "属性开关")
    @TableField("open_flag")
    private String openFlag;

    @Schema(description = "属性说明")
    @TableField("prop_desc")
    private String propDesc;

    @Schema(description = "用户排序的字段")
    @TableField("order_flag")
    private String orderFlag;
}
