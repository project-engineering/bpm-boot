package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.Map;

@Data
public class ModelPropertyJavaScriptTestRequest {
    @Schema(description = "JavaScript脚本")
    @NotBlank(message = "测试的JavaScript脚本不存在！")
    private String js;

    private Map<String, Object> businessMap;

}
