package com.bpm.workflow.dto.transfer;

import java.io.Serializable;

/**
 * @author: zgf
 * @date: 2018/5/16 18:11
 * @description:
 */
public class ProcessInstanceData implements Serializable,Cloneable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 961380445499852006L;
	private String processInstanceId;
    private String processDefinitionId;

    public String getProcessInstanceId()
    {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId)
    {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessDefinitionId()
    {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId)
    {
        this.processDefinitionId = processDefinitionId;
    }
}
