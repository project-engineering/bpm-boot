package com.bpm.workflow.vo.system.log;

import com.bpm.workflow.vo.PageDataRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogParam extends PageDataRequest {
    @Schema(description = "用户编码")
    private String userId;

    @Schema(description = "用户名")
    private String userName;

    @Schema(description = "操作方法")
    private String method;

}