package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_ru_identitylink")
@Tag(name = "WfRuIdentitylink对象", description = "")
public class WfRuIdentitylink {

    @TableId("ID_")
    private String id;

    @TableField("REV_")
    private Integer rev;

    @TableField("GROUP_ID_")
    private String groupId;

    @TableField("TYPE_")
    private String type;

    @TableField("USER_ID_")
    private String userId;

    @TableField("TASK_ID_")
    private String taskId;

    @TableField("PROC_INST_ID_")
    private String procInstId;

    @TableField("PROC_DEF_ID_")
    private String procDefId;
}
