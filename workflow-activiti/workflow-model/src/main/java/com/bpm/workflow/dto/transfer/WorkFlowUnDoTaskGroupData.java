package com.bpm.workflow.dto.transfer;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.SymbolConstants;

import java.io.Serializable;

/**
 * 待办任务分类查询返回对象
 */
public class WorkFlowUnDoTaskGroupData implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7063797223623856737L;

	private String systemCode;
	private String bigClassNo;
	private String bigClassName;
	private String littleClassNo;
	private String littleClassName;

	private int classCount;
	// update by xuesw 20180425 待办任务分类统计返回超时任务数和紧急任务数
	private int timeOutCount;
	private int urgentCount;
	// update by xuesw 20180425 待办任务分类统计返回超时任务数和紧急任务数

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
		if( this.systemCode==null)
		{
			bigClassNo= Constants.PARAM_TYPE_DEFAULT;
			bigClassName=Constants.PARAM_TYPE_DEFNAME;
			littleClassNo=Constants.PARAM_TYPE_DEFAULT;
			littleClassName = Constants.PARAM_TYPE_DEFNAME;
			return;
		}
		String[] classArray=this.systemCode.split(SymbolConstants.SEMICOLON);
		if( classArray.length>0 && classArray[0].length()>0 )
		{
			if(!classArray[0].contains(SymbolConstants.COLON))
			{
				bigClassNo=Constants.PARAM_TYPE_DEFAULT;
				bigClassName = classArray[0];
			}
			else
			{
				String[] codeArray = classArray[0].split(SymbolConstants.COLON);
				if(codeArray.length > 0){
					bigClassNo=codeArray[0];
					bigClassName = codeArray[1];
				}
			}
		}
		if( classArray.length>1 && classArray[1].length()>0 )
		{
			if( classArray[1].indexOf(SymbolConstants.COLON)<0)
			{
				littleClassNo=Constants.PARAM_TYPE_DEFAULT;
				littleClassName = classArray[1];
			}
			else
			{
				String[] codeArray = classArray[1].split(SymbolConstants.COLON);
				if(codeArray != null && codeArray.length > 0){
					littleClassNo=codeArray[0];
					littleClassName = codeArray[1];
				}
			}
		}
	}

	public int getSysCount() {
		return classCount;
	}

	public void setSysCount(int sysCount) {
		this.classCount = sysCount;
	}
	

	public String getBigClassNo() {
		return bigClassNo;
	}
	public String getBigClassName() {
		return bigClassName;
	}

	public String getLittleClassNo() {
		return littleClassNo;
	}
	public String getLittleClassName() {
		return littleClassName;
	}

	/**
	 * @return the timeOutCount
	 */
	public int getTimeOutCount() {
		return timeOutCount;
	}

	/**
	 * @param timeOutCount the timeOutCount to set
	 */
	public void setTimeOutCount(int timeOutCount) {
		this.timeOutCount = timeOutCount;
	}

	public int getUrgentCount() {
		return urgentCount;
	}

	public void setUrgentCount(int urgentCount) {
		this.urgentCount = urgentCount;
	}
	
	

}
