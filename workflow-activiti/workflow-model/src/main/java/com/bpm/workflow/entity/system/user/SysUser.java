package com.bpm.workflow.entity.system.user;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.*;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user")
@Tag(name = "SysUser对象", description = "用户表")
public class SysUser {

    @Schema(description = "用户id")
    @TableId(value = "user_id")
    private String userId;

    @Schema(description = "角色id")
    @TableField(value = "role_id",exist = false)
    private String roleId;

    @Schema(description = "用户名")
    @TableField("user_name")
    private String userName;

    @Schema(description = "密码")
    @TableField("`password`")
    private String password;

    @Schema(description = "手机号")
    @TableField("phone")
    private String phone;

    @Schema(description = "用户邮箱")
    @TableField("email")
    private String email;

    @Schema(description = "性别(0-男,1-女)")
    @TableField("sex")
    private String sex;

    @Schema(description = "是否为超级管理员(0-否,1-是)")
    @TableField("is_admin")
    private String isAdmin;

    @Schema(description = "密码是否过期(0-已过期,1-未过期)")
    @TableField("is_credentials_non_expired")
    private String isCredentialsNonExpired;

    @Schema(description = "用户头像")
    @TableField("avatar")
    private String avatar;

    @Schema(description = "昵称")
    @TableField("nick_name")
    private String nickName;

    @Schema(description = "最后登录时间")
    @TableField("login_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime loginDate;

    @Schema(description = "帐号状态（0正常 1停用）")
    @TableField("`status`")
    private String status;

    @Schema(description = "创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(description = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;
}
