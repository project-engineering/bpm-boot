package com.bpm.workflow.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class WfPropertiesConfig implements Serializable ,Cloneable	{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8135866281820172880L;

	
	private String propKey;

	
	private String propName;

	
	private String type;
	
	
	private String groupKey;

	
	private String propDesc;

	
	private String openFlag;

	
	private String orderFlag;

	private List<WfPropertiesConfig> childList = new ArrayList<WfPropertiesConfig>();


	public String getPropKey() {
		return propKey;
	}

	public void setPropKey(String propKey) {
		this.propKey = propKey == null ? null : propKey.trim();
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName == null ? null : propName.trim();
	}

	public String getType() {
		return type;
	}

	public void setPropType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public String getOpenFlag() {
		return openFlag;
	}

	public void setOpenFlag(String openFlag) {
		this.openFlag = openFlag == null ? null : openFlag.trim();
	}

	public String getPropDesc() {
		return propDesc;
	}

	public void setPropDesc(String propDesc) {
		this.propDesc = propDesc == null ? null : propDesc.trim();
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getOrderFlag() {
		return orderFlag;
	}

	public void setOrderFlag(String orderFlag) {
		this.orderFlag = orderFlag;
	}


	public void setType(String type) {
		this.type = type;
	}
	
	public List<WfPropertiesConfig> getChildList() {
		return childList;
	}

	public void setChildList(List<WfPropertiesConfig> childList) {
		this.childList = childList;
	}
	
	
}