package com.bpm.workflow.dto.transfer;

import java.io.Serializable;

public class TaskSetAssigneeData implements Serializable,Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 865185220497270281L;
	private String taskId;
	private String userCode;
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
}
