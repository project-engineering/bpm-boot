package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_accessory")
@Tag(name = "WfAccessory对象", description = "")
public class WfAccessory {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("proc_def_code")
    private String procDefCode;

    @TableField("proc_exe_id")
    private Integer procExeId;

    @TableField("`name`")
    private String name;

    @TableField("`type`")
    private String type;

    @TableField("`path`")
    private String path;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;

    @TableField("operator")
    private String operator;

    @TableField("version")
    @Version
    private Integer version;
}
