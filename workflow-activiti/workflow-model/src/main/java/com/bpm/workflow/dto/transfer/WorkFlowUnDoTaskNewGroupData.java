package com.bpm.workflow.dto.transfer;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author xuesw
 * @version V1.0
 * @Description: 待办任务分类查询返回对象
 * @date 2018年7月27日
 */
public class WorkFlowUnDoTaskNewGroupData implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7063797223623856737L;

	private String bigClassNo;
	private String bigClassName;

	private List<WorkFlowUnDoTaskData> children;

	public String getBigClassNo() {
		return bigClassNo;
	}

	public void setBigClassNo(String bigClassNo) {
		this.bigClassNo = bigClassNo;
	}

	public String getBigClassName() {
		return bigClassName;
	}

	public void setBigClassName(String bigClassName) {
		this.bigClassName = bigClassName;
	}

	public List<WorkFlowUnDoTaskData> getChildren() {
		return children;
	}

	public void setChildren(List<WorkFlowUnDoTaskData> children) {
		this.children = children;
	}
}
