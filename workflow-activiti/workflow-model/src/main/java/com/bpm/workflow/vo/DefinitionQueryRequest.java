package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DefinitionQueryRequest extends PageDataRequest {
	@Schema(description = "结构ID")
	@NotBlank(message="结构ID不能为空！")
	private String parentId;

	@Schema(description = "流程编码")
	private String defId;
	@Schema(description = "流程名称")
	private String defName;

	@Schema(description = "流程版本号")
	private int defVersion;

}