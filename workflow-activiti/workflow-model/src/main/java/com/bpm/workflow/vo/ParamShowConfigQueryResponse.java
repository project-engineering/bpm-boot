package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.util.List;

/**
 * 查询展示配置响应报文
 */
@Data
public class ParamShowConfigQueryResponse {

    @Schema(description = "查询条件")
    private List<ShowConfigQueryCondition> showConfigQueryConditionList;

    @Schema(description = "展示字段列表")
    private List<ShowConfigListField> showConfigListFieldList;

    @Schema(description = "是否展示机构字段，0-否 1-是")
    private String isShowOrgField;
}
