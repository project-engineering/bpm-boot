package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description ="分页对象")
public class PageRequest {

    @Schema(description = "起始页", required = true)
    private Integer start;

    @Schema(description = "页大小", required = true)
    private Integer limit;
}
