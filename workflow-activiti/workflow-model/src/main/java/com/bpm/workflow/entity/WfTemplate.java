package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_template")
@Tag(name = "WfTemplate对象", description = "")
public class WfTemplate {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("template_id")
    private String templateId;

    @TableField("`name`")
    private String name;

    @TableField("temp_desc")
    private String tempDesc;

    @TableField("`type`")
    private String type;

    @TableField("content")
    private String content;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @TableField("operator")
    private String operator;

    @TableField("version")
    @Version
    private Integer version;
}
