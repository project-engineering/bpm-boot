package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class NodeDefinitionAddRequest {

	@Schema(description = "节点标识")
	@NotBlank(message = "节点标识不能为空！")
	private String nodeId;

	@Schema(description = "节点名称")
	@NotBlank(message = "节点名称不能为空！")
	private String nodeName;

	@Schema(description = "所属节点Id")
	@NotBlank(message = "所属节点Id不能为空！")
	private String parentId;

	@Schema(description = "节点类型")
	@NotBlank(message = "节点类型不能为空！")
	private String type;
	
}
