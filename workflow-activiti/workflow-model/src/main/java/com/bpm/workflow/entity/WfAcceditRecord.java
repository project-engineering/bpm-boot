package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_accedit_record")
@Tag(name = "WfAcceditRecord对象", description = "")
public class WfAcceditRecord {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "转出人ID")
    @TableField("send_id")
    private String sendId;

    @Schema(description = "转出人名称")
    @TableField("send_name")
    private String sendName;

    @Schema(description = "转入人ID")
    @TableField("recive_id")
    private String reciveId;

    @Schema(description = "转入人名称")
    @TableField("recive_name")
    private String reciveName;

    @Schema(description = "流程编码")
    @TableField("flow_code")
    private String flowCode;

    @Schema(description = "流程名称列表")
    @TableField("flow_name")
    private String flowName;

    @Schema(description = "节点定义id")
    @TableField("TASK_DEF_KEY")
    private String taskDefKey;

    @Schema(description = "授权方式")
    @TableField("`type`")
    private String type;

    @Schema(description = "授权生效时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @Schema(description = "授权失效时间")
    @TableField("end_time")
    private LocalDateTime endTime;

    @Schema(description = "生效标志")
    @TableField("valid_flag")
    private String validFlag;

    @Schema(description = "授权备注")
    @TableField("remark")
    private String remark;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;

    @TableField("operator")
    private String operator;

    @Schema(description = "授权操作人名称")
    @TableField("operator_name")
    private String operatorName;

    @TableField("version")
    @Version
    private Integer version;
}
