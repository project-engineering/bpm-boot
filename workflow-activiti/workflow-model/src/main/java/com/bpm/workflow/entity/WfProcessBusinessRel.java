package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_process_business_rel")
@Tag(name = "WfProcessBusinessRel对象", description = "")
public class WfProcessBusinessRel {

    @Schema(description = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程实例id")
    @TableField("process_id")
    private String processId;

    @Schema(description = "业务id")
    @TableField("business_id")
    private String businessId;

    @Schema(description = "工作流事项小类编码")
    @TableField("param_code")
    private String paramCode;
}
