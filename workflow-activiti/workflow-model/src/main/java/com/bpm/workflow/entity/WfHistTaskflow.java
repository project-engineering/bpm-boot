package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_hist_taskflow")
@Tag(name = "WfHistTaskflow对象", description = "")
public class WfHistTaskflow {

    @Schema(description = "自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "当前待办任务ID")
    @TableField("task_id")
    private String taskId;

    @Schema(description = "任务节点编码")
    @TableField("task_code")
    private String taskCode;

    @Schema(description = "当前任务节点定义名称")
    @TableField("task_name")
    private String taskName;

    @Schema(description = "当前任务节点定义ID")
    @TableField("task_def_id")
    private String taskDefId;

    @Schema(description = "当前任务节点类型")
    @TableField("task_type")
    private String taskType;

    @Schema(description = "任务类型")
    @TableField("work_type")
    private Integer workType;

    @Schema(description = "当前任务级别")
    @TableField("task_rate")
    private Integer taskRate;

    @Schema(description = "当前任务紧急程度")
    @TableField("task_urgent")
    private Integer taskUrgent;

    @Schema(description = "当前任务业务名称")
    @TableField("task_buss_name")
    private String taskBussName;

    @Schema(description = "流程编码")
    @TableField("process_code")
    private String processCode;

    @Schema(description = "流程名称")
    @TableField("process_name")
    private String processName;

    @Schema(description = "当前流程实例ID")
    @TableField("process_id")
    private String processId;

    @Schema(description = "流程备注")
    @TableField("wf_desc")
    private String wfDesc;

    @Schema(description = "流程发起创始人")
    @TableField("work_creater")
    private String workCreater;

    @Schema(description = "系统分类编码")
    @TableField("system_code")
    private String systemCode;

    @Schema(description = "任务创建日期")
    @TableField("create_date")
    private LocalDateTime createDate;

    @Schema(description = "开始日期时间")
    @TableField("start_date")
    private LocalDateTime startDate;

    @Schema(description = "更改日期时间")
    @TableField("update_date")
    private LocalDateTime updateDate;

    @Schema(description = "截止日期时间")
    @TableField("end_date")
    private LocalDateTime endDate;

    @Schema(description = "预期任务结束时间")
    @TableField("est_end_date")
    private LocalDateTime estEndDate;

    @Schema(description = "前一任务成员")
    @TableField("prvious_actor")
    private String prviousActor;

    @Schema(description = "上一节点任务Id")
    @TableField("super_task_id")
    private String superTaskId;

    @Schema(description = "父任务Id")
    @TableField("parent_task_id")
    private String parentTaskId;

    @Schema(description = "当前任务成员")
    @TableField("task_actors")
    private String taskActors;

    @Schema(description = "当前任务成员信息")
    @TableField("task_actors_data")
    private String taskActorsData;

    @Schema(description = "当前任务处理状态")
    @TableField("work_state")
    private String workState;

    @TableField("do_task_actor")
    private String doTaskActor;

    @Schema(description = "处理结果")
    @TableField("do_result")
    private String doResult;

    @TableField("do_remark")
    private String doRemark;

    @Schema(description = "更改备注")
    @TableField("update_remark")
    private String updateRemark;

    @Schema(description = "任务分配人员")
    @TableField("work_allot")
    private String workAllot;

    @Schema(description = "步骤计数器")
    @TableField("step_count")
    private Integer stepCount;

    @Schema(description = "定时触发")
    @TableField("strike_timing")
    private String strikeTiming;

    @Schema(description = "当前任务催办状态")
    @TableField("task_do_state")
    private String taskDoState;

    @Schema(description = "当前任务催办信息")
    @TableField("task_do_info")
    private String taskDoInfo;

    @Schema(description = "是否可以检出")
    @TableField("check_in_flag")
    private String checkInFlag;

    @Schema(description = "任务处理类型")
    @TableField("work_do_flag")
    private String workDoFlag;

    @Schema(description = "任务处理URL地址/服务")
    @TableField("work_usr_service")
    private String workUsrService;

    @Schema(description = "任务处理URL标志")
    @TableField("work_url_flag")
    private String workUrlFlag;

    @Schema(description = "任务服务方法入口参数")
    @TableField("work_in_param")
    private String workInParam;

    @Schema(description = "处理附件信息")
    @TableField("do_accessories")
    private String doAccessories;

    @Schema(description = "处理水印签名信息")
    @TableField("do_water_mark")
    private String doWaterMark;

    @Schema(description = "保留的业务参数")
    @TableField("bussiness_map")
    private String bussinessMap;

    @Schema(description = "父任务Id")
    @TableField("query_indexa")
    private String queryIndexa;

    @Schema(description = "父任务Id")
    @TableField("query_indexb")
    private String queryIndexb;

    @Schema(description = "父任务Id")
    @TableField("query_indexc")
    private String queryIndexc;

    @Schema(description = "父任务Id1")
    @TableField("query_indexd")
    private String queryIndexd;

    @Schema(description = "检出时间")
    @TableField("check_out_time")
    private LocalDateTime checkOutTime;

    @Schema(description = "超时提示信息")
    @TableField("timeout_warn")
    private String timeoutWarn;

    @Schema(description = "是否超时(0:未超时;1:超时)")
    @TableField("is_timeout")
    private String isTimeout;

    @Schema(description = "渠道号")
    @TableField("tran_channel")
    private String tranChannel;

    @Schema(description = "1表示任务池")
    @TableField("task_pool_flag")
    private String taskPoolFlag;

    @Schema(description = "整个任务耗时")
    @TableField("duedata")
    private Long duedata;

    @Schema(description = "发起人姓名")
    @TableField("work_creater_name")
    private String workCreaterName;

    @Schema(description = "处理结果通知确认标志")
    @TableField("do_result_check")
    private String doResultCheck;

    @Schema(description = "主流水号")
    @TableField("buss_man_flow")
    private String bussManFlow;

    @Schema(description = "子流水号")
    @TableField("buss_sub_flow")
    private String bussSubFlow;

    @Schema(description = " 处理人姓名")
    @TableField("do_task_actor_name")
    private String doTaskActorName;

    @Schema(description = "当前子流程祖先实例ID")
    @TableField("root_process_id")
    private String rootProcessId;

    @TableField("item_first_code")
    private String itemFirstCode;

    @TableField("item_second_code")
    private String itemSecondCode;

    @TableField("item_second_name")
    private String itemSecondName;

    @TableField("work_creater_agency_id")
    private String workCreaterAgencyId;

    @TableField("work_creater_agency_name")
    private String workCreaterAgencyName;

    @TableField("do_task_actor_agency_id")
    private String doTaskActorAgencyId;

    @TableField("do_task_actor_agency_name")
    private String doTaskActorAgencyName;

    @Schema(description = "当前子流程祖先流程编码")
    @TableField("root_process_code")
    private String rootProcessCode;

    @Schema(description = "当前子流程祖先流程名称")
    @TableField("root_process_name")
    private String rootProcessName;

    @Schema(description = "流程实例启动时间")
    @TableField("start_create_date")
    private LocalDateTime startCreateDate;

    @Schema(description = "业务参数")
    @TableField("query_indexe")
    private String queryIndexe;

    @Schema(description = "业务参数")
    @TableField("query_indexf")
    private String queryIndexf;

    @Schema(description = "业务参数")
    @TableField("query_indexg")
    private String queryIndexg;

    @Schema(description = "业务参数")
    @TableField("query_indexh")
    private String queryIndexh;
}
