package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_evt_service_def")
@Tag(name = "WfEvtServiceDef对象", description = "")
public class WfEvtServiceDef {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "事件服务标识")
    @TableField("service_id")
    private String serviceId;

    @Schema(description = "服务名称")
    @TableField("`name`")
    private String name;

    @Schema(description = "调用类型")
    @TableField("call_Type")
    private String callType;

    @Schema(description = "调用模式")
    @TableField("call_Mode")
    private String callMode;

    @Schema(description = "调用服务")
    @TableField("call_Service")
    private String callService;

    @Schema(description = "调用方法")
    @TableField("call_Method")
    private String callMethod;

    @Schema(description = "参数获取方式")
    @TableField("call_ParamFlag")
    private String callParamflag;

    @Schema(description = "入口参数")
    @TableField("call_Input")
    private String callInput;

    @Schema(description = "出口参数")
    @TableField("call_Output")
    private String callOutput;

    @Schema(description = "服务方法调用返回结果")
    @TableField("call_Result")
    private String callResult;

    @Schema(description = "目标系统ID")
    @TableField("target_Id")
    private String targetId;

    @Schema(description = "渠道")
    @TableField("`channel`")
    private String channel;

    @Schema(description = "终端号")
    @TableField("terminal_Code")
    private String terminalCode;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("version")
    @Version
    private Integer version;

    @TableField("operator")
    private String operator;
}
