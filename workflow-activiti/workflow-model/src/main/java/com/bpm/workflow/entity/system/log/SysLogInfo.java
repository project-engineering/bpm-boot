package com.bpm.workflow.entity.system.log;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

/**
 * @description：操作日志信息
 * @author：liuc
 * @dateTime：2024/02/17 11:33
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_log_info")
public class SysLogInfo {

    // 主键id
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    // 功能模块
    @TableField("module")
    private String module;

    // 操作类型
    @TableField("type")
    private String type;

    // 操作描述
    @TableField("message")
    private String message;

    // 请求参数
    @TableField("req_param")
    private String reqParam;

    // 响应参数
    @TableField("res_param")
    private String resParam;

    // 耗时
    @TableField("take_up_time")
    private String takeUpTime;

    // 操作用户id
    @TableField("user_id")
    private String userId;

    // 操作用户名称
    @TableField("user_name")
    private String userName;

    // 操作方法
    @TableField("method")
    private String method;

    // 请求url
    @TableField("uri")
    private String uri;

    // 请求IP
    @TableField("ip")
    private String ip;

    // 版本号
    @TableField("version")
    private String version;

    // 创建时间
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

}
