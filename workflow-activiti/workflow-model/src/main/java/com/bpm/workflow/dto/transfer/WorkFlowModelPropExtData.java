package com.bpm.workflow.dto.transfer;

import com.bpm.workflow.entity.WfModelPropExtends;

import java.util.ArrayList;
import java.util.List;

public class WorkFlowModelPropExtData extends WfModelPropExtends {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3435056223113715363L;

	@SuppressWarnings("rawtypes")
	private PageData pageData = new PageData();

	private List<WfModelPropExtends> pageList = new ArrayList<WfModelPropExtends>();

	@SuppressWarnings("rawtypes")
	public PageData getPageData() {
		return pageData;
	}

	@SuppressWarnings("rawtypes")
	public void setPageData(PageData pageData) {
		this.pageData = pageData;
	}

	public List<WfModelPropExtends> getPageList() {
		return pageList;
	}

	public void setPageList(List<WfModelPropExtends> pageList) {
		this.pageList = pageList;
	}

}
