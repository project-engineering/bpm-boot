package com.bpm.workflow.dto.transfer;

import com.bpm.workflow.constant.Constants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SysCommHead implements Serializable {
    private static final long serialVersionUID = -4499101342644170236L;
    private static final String CODE_SUCCESS = "000000";


    protected Map<String, Object> values;

    public Object getValue(String key) {
        if (values == null) {
            values = new HashMap<String, Object>();
            values.put(FieldId.retCode.getName(), Constants.RET_SUCC);
        }
        if (values == null || !values.containsKey(key)) {
            return null;
        }
        return values.get(key);
    }

    public Map<String, Object> getValues() {
        return values;
    }

    public synchronized void setValue(String key, Object value) {
        if (values == null) {
            values = new HashMap<String, Object>();
        }
        values.put(key, value);
        if (FieldId.retCode.name.equals(key) && CODE_SUCCESS.equals(value)) {
            values.put(FieldId.retStatus.name, "S");
        }
    }

    public enum FieldId {
        /**
         * 交易日期
         */
        trandate("trandate", ""),
        /**
         * 交易时间
         */
        trantime("trantime", ""),
        /**
         * 交易渠道
         */
        tranChannel("tranChannel", ""),
        /**
         * 柜员编号
         */
        tellerId("tellerId", ""),
        /**
         * 机构编号
         */
        branchId("branchId", ""),
        /**
         * 渠道
         */
        channel("channel", ""),
        /**
         * 用户编号
         */
        userId("userId", ""),
        /**
         * 机构编号
         */
        orgId("orgId", ""),
        /**
         * 扩展参数
         */
        extParam("extParam", ""),
        /**
         * 用户角色
         */
        userRoles("userRoles", ""),
        /**
         * 消费者编号
         */
        consumerId("consumerId", ""),
        /**
         * 终端代码
         */
        terminalCode("terminalCode", ""),
        /**
         * 目标系统编号
         */
        targetId("targetId", ""),
        /**
         * 全局流水号
         */
        globalSeqNo("globalSeqNo", ""),
        /**
         * 扩展内容
         */
        extendContent("extendContent", ""),
        /**
         * Map数据
         */
        mapData("mapData", ""),
        /**
         * 服务名称
         */
        serviceName("serviceName", ""),
        /**
         * 最终时区
         */
        lastTimeZone("lastTimeZone", ""),
        /**
         * 域名
         */
        domain("domain", ""),
        /**
         * 参数列表
         */
        parameterList("parameterList", ""),
        /**
         * 外部系统
         */
        externalSystem("externalSystem", ""),
        /**
         * 外部代码
         */
        externalCode("externalCode", ""),
        /**
         * 外部消息
         */
        externalMessage("externalMessage", ""),
        /**
         * 源系统编号
         */
        sourceSysId("sourceSysId", ""),
        /**
         * 任务编号
         */
        scheduleId("scheduleId", ""),
        /**
         * 服务码
         */
        serviceCode("serviceCode", ""),
        /**
         * 响应状态
         */
        retStatus("retStatus", ""),
        /**
         * 响应码值
         */
        retCode("retCode", ""),
        /**
         * 响应消息
         */
        retMsg("retMsg", ""),
        /**
         * 交易流水
         */
        tranSeqNo("tranSeqNo", ""),
        /**
         * 权限柜员编号
         */
        authTellerId("authTellerId", ""),
        /**
         * 权限密码
         */
        authrPwd("authrPwd", ""),
        /**
         * 权限标识
         */
        authFlag("authFlag", ""),
        /**
         * 交易Key参数
         */
        tranKeyParam("tranKeyParam", ""),
        /**
         * 本地
         */
        localLang("localLang", ""),
        /**
         * 合法响应代码
         */
        legalRepCode("legalRepCode", ""),
        /**
         * mac
         */
        mac("mac", ""),
        /**
         * key
         */
        keyId("keyId", ""),
        /**
         * 交易代码
         */
        tranCode("tranCode", "");
        private String name;
        private String desc;
        private static String[] names;

        private FieldId(String name, String desc) {
            this.name = name;
            this.desc = desc;
        }

        public String getName() {
            return name;
        }

        public static FieldId getFieldIdByName(String name) {
            for (FieldId e : FieldId.values()) {
                if (e.name.equals(name)) {
                    return e;
                }
            }
            return null;
        }

        public synchronized static String[] getFieldNames() {
            if (names != null) {
                return names;
            }
            names = new String[FieldId.values().length];
            for (int i = 0; i < FieldId.values().length; i++) {
                names[i] = FieldId.values()[i].getName();
            }
            return names;
        }
    }

    public enum ConstantType {
        Success("000000", "S", "S"),
        Fail("Fail", "F", "F"),
        ;
        private String typeName;
        private String reqTypeValue;
        private String respTypeValue;


        private ConstantType(String typeName, String reqTypeValue, String respTypeValue) {
            this.typeName = typeName;
            this.reqTypeValue = reqTypeValue;
            this.respTypeValue = respTypeValue;
        }

        public String getTypeName() {
            return typeName;
        }

        public String getReqTypeValue() {
            return reqTypeValue;
        }

        public String getRespTypeValue() {
            return respTypeValue;
        }

        public static ConstantType getFieldIdByName(String typeName) {
            for (ConstantType e : ConstantType.values()) {
                if (e.typeName.equals(typeName)) {
                    return e;
                }
            }
            return null;
        }

    }


    public String getSourceSysId() {
        Object obj = getValue(FieldId.sourceSysId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setSourceSysId(String sourceSysId) {
        setValue(FieldId.sourceSysId.getName(), sourceSysId);
    }

    public String getBranchId() {
        Object obj = getValue(FieldId.branchId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setBranchId(String branchId) {
        setValue(FieldId.branchId.getName(), branchId);
    }

    public String getTrantime() {
        Object obj = getValue(FieldId.trantime.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTrantime(String trantime) {
        setValue(FieldId.trantime.getName(), trantime);
    }

    public String getServiceCode() {
        Object obj = getValue(FieldId.serviceCode.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setServiceCode(String serviceCode) {
        setValue(FieldId.serviceCode.getName(), serviceCode);
    }

    public String getRetStatus() {
        Object obj = getValue(FieldId.retStatus.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setRetStatus(String retStatus) {
        setValue(FieldId.retStatus.getName(), retStatus);
    }

    public String getConsumerId() {
        Object obj = getValue(FieldId.consumerId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setConsumerId(String consumerId) {
        setValue(FieldId.consumerId.getName(), consumerId);
    }

    public String getTrandate() {
        Object obj = getValue(FieldId.trandate.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTrandate(String trandate) {
        setValue(FieldId.trandate.getName(), trandate);
    }

    public String getServiceName() {
        Object obj = getValue(FieldId.serviceName.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setServiceName(String serviceName) {
        setValue(FieldId.serviceName.getName(), serviceName);
    }

    public String getRetCode() {
        Object obj = getValue(FieldId.retCode.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setRetCode(String retCode) {
        setValue(FieldId.retCode.getName(), retCode);
    }

    public String getRetMsg() {
        Object obj = getValue(FieldId.retMsg.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setRetMsg(String retMsg) {
        setValue(FieldId.retMsg.getName(), retMsg);
    }

    public String getTranSeqNo() {
        Object obj = getValue(FieldId.tranSeqNo.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTranSeqNo(String tranSeqNo) {
        setValue(FieldId.tranSeqNo.getName(), tranSeqNo);
    }

    public String getAuthTellerId() {
        Object obj = getValue(FieldId.authTellerId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setAuthTellerId(String authTellerId) {
        setValue(FieldId.authTellerId.getName(), authTellerId);
    }

    public String getTranChannel() {
        Object obj = getValue(FieldId.tranChannel.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTranChannel(String tranChannel) {
        setValue(FieldId.tranChannel.getName(), tranChannel);
    }

    public void setMapData(String mapKey, String mapValue) {
        if (getValue(FieldId.mapData.getName()) == null) {
            setValue(FieldId.mapData.getName(), new HashMap<String, String>());
        }
        ((Map<String, String>) getValue(FieldId.mapData.getName())).put(mapKey, mapValue);
    }

    public String getMapData(String mapKey) {
        Object obj = getValue(FieldId.mapData.getName());
        if (obj == null) {
            return null;
        }
        Map<String, String> mapData = (Map<String, String>) obj;
        if (mapData != null) {
            return mapData.get(mapKey);
        }
        return null;
    }

    public String getTellerId() {
        Object obj = getValue(FieldId.tellerId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTellerId(String tellerId) {
        setValue(FieldId.tellerId.getName(), tellerId);
    }

    public String getGlobalSeqNo() {
        Object obj = getValue(FieldId.globalSeqNo.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setGlobalSeqNo(String globalSeqNo) {
        setValue(FieldId.globalSeqNo.getName(), globalSeqNo);
    }

    public String getTranKeyParam() {
        Object obj = getValue(FieldId.tranKeyParam.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTranKeyParam(String tranKeyParam) {
        setValue(FieldId.tranKeyParam.getName(), tranKeyParam);
    }

    public String getTranCode() {
        Object obj = getValue(FieldId.tranCode.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTranCode(String tranCode) {
        setValue(FieldId.tranCode.getName(), tranCode);
    }


    public String getChannel() {
        Object obj = getValue(FieldId.channel.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setChannel(String channel) {
        setValue(FieldId.channel.getName(), channel);
    }


    public String getUserId() {
        Object obj = getValue(FieldId.userId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setUserId(String userId) {
        setValue(FieldId.userId.getName(), userId);
    }

    public String getOrgId() {
        Object obj = getValue(FieldId.orgId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setOrgId(String orgId) {
        setValue(FieldId.orgId.getName(), orgId);
    }

    public void setExtParam(Map<String, String> map) {
        if (getValue(FieldId.extParam.getName()) == null) {
            setValue(FieldId.extParam.getName(), map);
        }
    }

    public Map<String, String> getExtParam() {
//		Object obj = getValue(FieldId.mapData.getName());
//		if(obj ==null) {return null;}
//		Map<String,String>mapData=(Map<String,String>)obj;
//		if( mapData!=null ){
//			return mapData;
//		}
//		return null;
        Object obj = getValue(FieldId.extParam.getName());
        if (obj == null) {
            return null;
        } else {
            return (Map<String, String>) obj;
        }
    }

    public String getTargetId() {
        Object obj = getValue(FieldId.targetId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setTargetId(String targetId) {
        setValue(FieldId.targetId.getName(), targetId);
    }

    public void setDefaultValue(Map<String, Object> defaultValueByBosentId) {
        if (defaultValueByBosentId == null || defaultValueByBosentId.size() == 0) {
            return;
        }
        if (values == null) {
            values = new HashMap<String, Object>();
        }
        for (FieldId field : FieldId.values()) {
            if (values.get(field.getName()) == null && defaultValueByBosentId.keySet().contains(field.getName())) {
                values.put(field.getName(), defaultValueByBosentId.get(field.getName()));
            }
        }
    }

    public String getUserRoles() {

        Object obj = getValue(FieldId.userRoles.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public String getScheduleId() {
        Object obj = getValue(FieldId.scheduleId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public String getLocalLang() {

        Object obj = getValue(FieldId.localLang.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setLocalLang(String localLang) {
        setValue(FieldId.localLang.getName(), localLang);
    }

    public String getLegalRepCode() {
        Object obj = getValue(FieldId.legalRepCode.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setLegalRepCode(String legalRepCode) {
        setValue(FieldId.legalRepCode.getName(), legalRepCode);
    }

    public String getMac() {
        Object obj = getValue(FieldId.mac.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setMac(String mac) {
        setValue(FieldId.mac.getName(), mac);
    }

    public String getKeyId() {
        Object obj = getValue(FieldId.keyId.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setKeyId(String keyId) {
        setValue(FieldId.keyId.getName(), keyId);
    }

    public String getAuthrPwd() {
        Object obj = getValue(FieldId.authrPwd.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setAuthrPwd(String authrPwd) {
        setValue(FieldId.authrPwd.getName(), authrPwd);
    }

    public String getAuthFlag() {
        Object obj = getValue(FieldId.authFlag.getName());
        if (obj == null) {
            return null;
        } else {
            return (String) obj;
        }
    }

    public void setAuthFlag(String authFlag) {
        setValue(FieldId.authFlag.getName(), authFlag);
    }

}
