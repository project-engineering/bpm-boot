package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_instance")
@Tag(name = "WfInstance对象", description = "")
public class WfInstance {

    @Schema(description = "自增涨主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "流程实例ID")
    @TableField("process_id")
    private String processId;

    @TableField("parent_Task_Id")
    private String parentTaskId;

    @Schema(description = "流程定义编码")
    @TableField("process_code")
    private String processCode;

    @Schema(description = "流程定义名称")
    @TableField("proccess_name")
    private String proccessName;

    @Schema(description = "流程实例简述")
    @TableField("process_inst_desc")
    private String processInstDesc;

    @Schema(description = "流程定义备注")
    @TableField("process_desc")
    private String processDesc;

    @Schema(description = "流程发起人Id")
    @TableField("start_user_Id")
    private String startUserId;

    @Schema(description = "流程审批人员列表")
    @TableField("join_user_ids")
    private String joinUserIds;

    @TableField("system_code")
    private String systemCode;

    @Schema(description = "流程结束类型")
    @TableField("end_type")
    private String endType;

    @Schema(description = "流程运行状态")
    @TableField("work_state")
    private String workState;

    @Schema(description = "流程实例创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @Schema(description = "流程发起时间")
    @TableField("start_time")
    private LocalDateTime startTime;

    @Schema(description = "记录更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @Schema(description = "流程结束时间")
    @TableField("end_time")
    private LocalDateTime endTime;

    @Schema(description = "预期任务结束时间")
    @TableField("est_end_date")
    private LocalDateTime estEndDate;

    @Schema(description = "超时提示信息")
    @TableField("timeout_warn")
    private String timeoutWarn;

    @Schema(description = "是否超时(0:未超时;1:超时)")
    @TableField("is_timeout")
    private String isTimeout;

    @Schema(description = "整个流程耗时")
    @TableField("duedata")
    private Long duedata;

    @Schema(description = "流程发起人map")
    @TableField("start_user_map")
    private String startUserMap;

    @Schema(description = " 机构ID")
    @TableField("org_id")
    private String orgId;

    @Schema(description = " 机构名称")
    @TableField("org_name")
    private String orgName;

    @TableField("start_user_name")
    private String startUserName;
}
