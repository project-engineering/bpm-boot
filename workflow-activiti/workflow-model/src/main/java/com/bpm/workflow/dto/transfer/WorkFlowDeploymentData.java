package com.bpm.workflow.dto.transfer;

import java.io.Serializable;

/**
 * 流程部署对象的数据类
 * 
 * @author xuesw
 *
 */
public class WorkFlowDeploymentData implements Serializable,Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5332316785537266205L;

	// 流程编码
	private String workFlowCode;

	// 流程定义的bpmn、xml等文件路径
	private String xmlFilePath;

	// bpmn等对应的图片文件路径
	private String pngFilePath;

	// 流程定义的内容（xml结构）
	private String deployXmlContent;

	private String deployId;

 	public String getWorkFlowCode() {
		return workFlowCode;
	}

	public void setWorkFlowCode(String workFlowCode) {
		this.workFlowCode = workFlowCode;
	}

	public String getXmlFilePath() {
		return xmlFilePath;
	}

	public void setXmlFilePath(String xmlFilePath) {
		this.xmlFilePath = xmlFilePath;
	}

	public String getPngFilePath() {
		return pngFilePath;
	}

	public void setPngFilePath(String pngFilePath) {
		this.pngFilePath = pngFilePath;
	}

	public String getDeployXmlContent() {
		return deployXmlContent;
	}

	public void setDeployXmlContent(String deployXmlContent) {
		this.deployXmlContent = deployXmlContent;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

}
