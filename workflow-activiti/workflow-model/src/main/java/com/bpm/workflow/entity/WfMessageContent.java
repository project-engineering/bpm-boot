package com.bpm.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("wf_message_content")
@Tag(name = "WfMessageContent对象", description = "")
public class WfMessageContent {

    @Schema(description = "消息唯一标识")
    @TableId("id")
    private String id;

    @Schema(description = "消息大类")
    @TableField("first_type")
    private String firstType;

    @Schema(description = "消息小类")
    @TableField("second_type")
    private String secondType;

    @Schema(description = "系统标识")
    @TableField("system_id")
    private String systemId;

    @Schema(description = "流水号(防止重复插入)")
    @TableField("msg_seq")
    private String msgSeq;

    @Schema(description = "数据渠道")
    @TableField("`channel`")
    private String channel;

    @Schema(description = "消息简述")
    @TableField("msg_desc")
    private String msgDesc;

    @Schema(description = "消息详述")
    @TableField("msg_content")
    private String msgContent;

    @Schema(description = "消息创建时间")
    @TableField("create_date")
    private LocalDateTime createDate;

    @Schema(description = "扩展字段1")
    @TableField("extend01")
    private String extend01;

    @Schema(description = "扩展字段2")
    @TableField("extend02")
    private String extend02;

    @Schema(description = "扩展字段3")
    @TableField("extend03")
    private String extend03;

    @Schema(description = "提醒日期")
    @TableField("remind_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime remindDate;

    @Schema(description = "提醒结束日期")
    @TableField("remind_end_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime remindEndDate;

    @Schema(description = "事件发生日期")
    @TableField("event_create_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime eventCreateDate;

    @Schema(description = "客户ID")
    @TableField("customer_id")
    private String customerId;

    @Schema(description = "客户姓名")
    @TableField("customer_name")
    private String customerName;
}
