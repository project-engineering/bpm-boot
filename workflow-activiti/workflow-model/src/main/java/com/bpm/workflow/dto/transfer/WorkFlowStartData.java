package com.bpm.workflow.dto.transfer;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程启动参数实体
 * 
 * @author xuesw
 *
 */
public class WorkFlowStartData implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1139831046862145805L;

	//子流程启动标志
	private boolean childFlag = false;
	// 流程编码
	private String processCode;
	//流程实例简述  EG:同一个请假流程：张三请假、李四请假5天
	private String processInstDesc;

	// 流程发起人
	private String userId;
	
	//流程代理触发用户
	private String agentUserId;
	
	// 流程发起人信息的map
	private Map<String, Object> userIdMap;

	// 流程发起的业务变量
	private Map<String, Object> bussinessMap  = new HashMap<String, Object>();;
	
	//流程定时启动时间
	private Date startTime;
	
	//任务截止处理时间
	private Date endTime;
	
	//系统分类编码
	private String systemIdCode;
	
	private SysCommHead sysCommHead;

	/**
	 * 发起人处理意见
	 */
	private String doRemark;

	public String getDoRemark() {
		return doRemark;
	}

	public void setDoRemark(String doRemark) {
		this.doRemark = doRemark;
	}


	public SysCommHead getSysCommHead() {
		return sysCommHead;
	}

	public void setSysCommHead(SysCommHead sysCommHead) {
		this.sysCommHead = sysCommHead;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getProcessInstDesc() {
		return processInstDesc;
	}

	public void setProcessInstDesc(String processInstDesc) {
		this.processInstDesc = processInstDesc;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAgentUserId() {
		return agentUserId;
	}

	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}

	public Map<String, Object> getUserIdMap() {
		return userIdMap;
	}

	public void setUserIdMap(Map<String, Object> userIdMap) {
		this.userIdMap = userIdMap;
	}

	public Map<String, Object> getBussinessMap() {
		return bussinessMap;
	}

	public void setBussinessMap(Map<String, Object> bussinessMap) {
		this.bussinessMap = bussinessMap;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSystemIdCode() {
		return systemIdCode;
	}

	public void setSystemIdCode(String systemIdCode) {
		this.systemIdCode = systemIdCode;
	}
	
	public boolean getChildFlag() {
		return childFlag;
	}

	public void setChildFlag(boolean childFlag) {
		this.childFlag = childFlag;
	}
}
