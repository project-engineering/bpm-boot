package com.bpm.workflow.dto.transfer.urule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 数据传输文件目录组装类
 */
public class URuleRepoTreeTransferData implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private String label;
	private String name;

	private String fullPath;

	private String type;

	private String foderType;

	private String lock;

	private String lockInfo;

	private String libType;
	private String fileTitle;

	private String parentId;
	private String ruleNo;
	/**
	 * 操作 0新增,1修改,2删除
	 */
	private int operate;
	public String getRuleNo() {
		return ruleNo;
	}

	public void setRuleNo(String ruleNo) {
		this.ruleNo = ruleNo;
	}

	private String ruleType;

	private List<URuleRepoTreeTransferData> children;

	public URuleRepoTreeTransferData() {
		super();
//		setName(label);
	}

	public URuleRepoTreeTransferData(String id, String label, String fullPath, String type, String foderType, String parentId) {
		super();
		this.id = id;
		this.label = label;
		this.fullPath = fullPath;
		this.type = type;
		this.foderType = foderType;
		this.parentId = parentId;
	}

	public URuleRepoTreeTransferData(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath == null ? null : fullPath.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public String getFoderType() {
		return foderType;
	}

	public void setFoderType(String foderType) {
		this.foderType = foderType == null ? null : foderType.trim();
	}

	public String getLock() {
		return lock;
	}

	public void setLock(String lock) {
		this.lock = lock == null ? null : lock.trim();
	}

	public String getLockInfo() {
		return lockInfo;
	}

	public void setLockInfo(String lockInfo) {
		this.lockInfo = lockInfo == null ? null : lockInfo.trim();
	}

	public String getLibType() {
		return libType;
	}

	public void setLibType(String libType) {
		this.libType = libType == null ? null : libType.trim();
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId == null ? null : parentId.trim();
	}

	/**
	 * 根据目录文件列表递归插入节点，生成机构树
	 * 
	 * @Title: insertParntURuleRepoTree
	 * @param tempOrgTreeNode
	 *            待插入节点
	 * @time 2018-07-11
	 * @auther mgx
	 */
	public void insertParentURuleRepoTree(String parentId, URuleRepoTreeTransferData tempOrgTreeNode) {
		if (this.getId().equals((String) parentId)) {
			// 如果无子节点,创建子节点加入节点
			if (this.getChildren() == null) {
				List<URuleRepoTreeTransferData> childrens = new ArrayList<URuleRepoTreeTransferData>();
				childrens.add(tempOrgTreeNode);
				this.setChildrens(childrens);

			} else {
				this.getChildren().add(tempOrgTreeNode);
			}
			return;
		}
		// 如果无子节点
		if (this.getChildren() != null) {
			// 获取子节点列表
			List<URuleRepoTreeTransferData> list = this.getChildren();
			// 遍历列表
			for (URuleRepoTreeTransferData m : list) {
				m.insertParentURuleRepoTree(parentId, tempOrgTreeNode);
			}
		}

	}


	public List<URuleRepoTreeTransferData> getChildren() {
		return children;
	}

	public void setChildrens(List<URuleRepoTreeTransferData> childrens) {
		this.children = childrens;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public int getOperate() {
		return operate;
	}

	public void setOperate(int operate) {
		this.operate = operate;
	}

	public void setChildren(List<URuleRepoTreeTransferData> children) {
		this.children = children;
	}

}
