package com.bpm.workflow.dto.transfer;

import org.apache.ibatis.session.RowBounds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class PageData<T> extends RowBounds implements Serializable,Cloneable {

	private static final int INT_200 = 200;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6409678515297104758L;

	// 分页查询：每页行数
	int pageRows = 1000;

	// 分页查询：当前页数
	int curPage = 1;

	// 开始记录数
	private int startRows=0;

	// 总记录数
	private int totalRows;

	/**
	 * 当前页的数据
	 */
	private List<T> result = new ArrayList<T>();
	
	
	public int getStartRows() {
		return startRows;
	}

	public void setStartRows(int startRows) {
		this.startRows = startRows;
	}

	public int getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	public int getPageRows() {
		return pageRows;
	}

	public void setPageRows(int pageRows) {
		if(pageRows > INT_200) {
			this.pageRows=200;
		}else {
			this.pageRows = pageRows;
		}
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		if(result == null) {
			return "null";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("=========PageData=============").append(System.lineSeparator());
		sb.append("[").append(System.lineSeparator());
		for(T obj:result) {
			if(obj == null) {
				sb.append("null");
			}else {
				sb.append(obj);
			}			
		}
		sb.append("]").append(System.lineSeparator());
		return sb.toString();
	}
}
