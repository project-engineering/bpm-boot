package com.bpm.workflow.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotEmpty;
import java.util.List;

@Tag(name = "工作流选人查询请求参数")
public class UserAskListQueryRequest {
    @Schema(description = "用户选人适配规则列表", required = true)
    private @NotEmpty List<UserAskQueryRequest> userAskList;
    @Schema(description = "事项小类编码")
    private String systemIdCode;
    @Schema(description = "是否发起人")
    private boolean startUserFlag;

    public UserAskListQueryRequest() {
    }

    public List<UserAskQueryRequest> getUserAskList() {
        return this.userAskList;
    }

    public String getSystemIdCode() {
        return this.systemIdCode;
    }

    public boolean isStartUserFlag() {
        return this.startUserFlag;
    }

    public void setUserAskList(List<UserAskQueryRequest> userAskList) {
        this.userAskList = userAskList;
    }

    public void setSystemIdCode(String systemIdCode) {
        this.systemIdCode = systemIdCode;
    }

    public void setStartUserFlag(boolean startUserFlag) {
        this.startUserFlag = startUserFlag;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof UserAskListQueryRequest)) {
            return false;
        } else {
            UserAskListQueryRequest other = (UserAskListQueryRequest)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.isStartUserFlag() != other.isStartUserFlag()) {
                return false;
            } else {
                Object this$userAskList = this.getUserAskList();
                Object other$userAskList = other.getUserAskList();
                if (this$userAskList == null) {
                    if (other$userAskList != null) {
                        return false;
                    }
                } else if (!this$userAskList.equals(other$userAskList)) {
                    return false;
                }

                Object this$systemIdCode = this.getSystemIdCode();
                Object other$systemIdCode = other.getSystemIdCode();
                if (this$systemIdCode == null) {
                    if (other$systemIdCode != null) {
                        return false;
                    }
                } else if (!this$systemIdCode.equals(other$systemIdCode)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof UserAskListQueryRequest;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + (this.isStartUserFlag() ? 79 : 97);
        Object $userAskList = this.getUserAskList();
        result = result * 59 + ($userAskList == null ? 43 : $userAskList.hashCode());
        Object $systemIdCode = this.getSystemIdCode();
        result = result * 59 + ($systemIdCode == null ? 43 : $systemIdCode.hashCode());
        return result;
    }

    public String toString() {
        return "UserAskListQueryRequest(userAskList=" + this.getUserAskList() + ", systemIdCode=" + this.getSystemIdCode() + ", startUserFlag=" + this.isStartUserFlag() + ")";
    }
}