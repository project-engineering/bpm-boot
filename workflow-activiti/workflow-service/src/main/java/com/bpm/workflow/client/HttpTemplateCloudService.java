package com.bpm.workflow.client;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.util.constant.BpmConstants;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service("workflowHttpTemplateCloudService")
public class HttpTemplateCloudService {
	
	@Resource
	private RestTemplate getRestTemplate;
	
	public  String postForObject(String url, String params) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<>(params,  headers);
        String result = getRestTemplate.postForObject(url, requestEntity, String.class);
        return result;
    }

    public  Map<String, Object> postForEntity(String url, String params) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<String>(params,  headers);
        try
        {
            ResponseEntity<String> responseEntity  = getRestTemplate.postForEntity(url, requestEntity, String.class);
//            removeMdc();
            HttpStatus status= (HttpStatus) responseEntity.getStatusCode();
            log.info("| - HttpTemplateCloudService>>>>>HttpStatus is:{}",String.valueOf(status.value()));
            if(status.is2xxSuccessful())
            {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPSUCC);

            }else
            {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPFAIL);
            }
            resultMap.put(Constants.HTTP_CONNECT_RSPHEAD, responseEntity.getHeaders());
            resultMap.put(Constants.HTTP_CONNECT_RSPDATA, responseEntity.getBody());
        } catch (Exception e)
        {
            log.error("Post Error!" ,e);
            resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RspTimeOut);
        }
        return resultMap;
    }

    public  Map<String, Object> getForEntity(String url) {
        Map<String, Object> resultMap = new HashMap<>();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        try
        {
            ResponseEntity<String> responseEntity  =getRestTemplate
                    .getForEntity(url,String.class,requestEntity);
//            removeMdc();
            HttpStatus status = (HttpStatus) responseEntity.getStatusCode();
            log.info("| - HttpTemplateCloudService>>>>>HttpStatus is:{}",String.valueOf(status.value()));
            if(status.is2xxSuccessful())
            {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPSUCC);

            }else
            {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPFAIL);
            }
            resultMap.put(Constants.HTTP_CONNECT_RSPHEAD, responseEntity.getHeaders());
            resultMap.put(Constants.HTTP_CONNECT_RSPDATA, responseEntity.getBody());
        } catch (Exception e)
        {
            log.error("Post Error!",e);
            resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RspTimeOut);
        }
        return resultMap;
    }
    
    public static void removeMdc () {
    	Map<String,String> mdc = MDC.getCopyOfContextMap();
		if(mdc.containsKey(SymbolConstants.X_B3_TRACEID)) {
			MDC.remove(SymbolConstants.X_B3_TRACEID);
		}
		if(mdc.containsKey(SymbolConstants.X_B3_SPANID)) {
			MDC.remove(SymbolConstants.X_B3_SPANID);
		}
		if(mdc.containsKey(SymbolConstants.X_B3_PARENTSPANID)) {
			MDC.remove(SymbolConstants.X_B3_PARENTSPANID);
		}
		if(mdc.containsKey(SymbolConstants.X_SPAN_EXPORT)) {
			MDC.remove(SymbolConstants.X_SPAN_EXPORT);
		}
    }
}
