package com.bpm.workflow.service.system.log;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import com.bpm.workflow.vo.system.log.LogParam;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-17
 */
public interface SysLogInfoService extends IService<SysLogInfo> {
    IPage<SysLogInfo> getList(LogParam param);
}
