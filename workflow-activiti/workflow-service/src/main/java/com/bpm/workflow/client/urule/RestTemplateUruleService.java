package com.bpm.workflow.client.urule;

import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.HttpTemplateCloudService;
import com.bpm.workflow.dto.transfer.WorkFlowServiceBean;
import com.bpm.workflow.exception.ServiceException;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import java.util.Map;

@Log4j2
@Service(value = "restTemplateUruleService")
public class RestTemplateUruleService  extends RestUruleService implements IUruleService{
	@Resource
	private HttpTemplateCloudService httpTemplateCloudService;
	
	private String uruleUrl;
	
	@Override
	public String execByRuleName(String ruleName, Map<String, Object> params) {
		WorkFlowServiceBean requestBean = new WorkFlowServiceBean();
		requestBean.getTranBody().getMapReqData().put("knowledgeId", ruleName);
		String jsonData = prepareReqMapDataByParamsMap(params, requestBean);
		String result = "";
		try {
			 result = httpTemplateCloudService.postForObject(uruleUrl, jsonData);
			if (log.isInfoEnabled()) {
				log.info("| - RestTemplateUruleService>>>>> execByRuleName:{} resp:{}",ruleName,result);
				log.info(result);
			}
		} catch (Exception e) {
			log.error("规则服务访问失败：" + e.getMessage(), e);
			throw new ServiceException("请检查规则服务是否启动，或者相关配置是否正确。");
		}
		return getResultByRuleName(result);
	}

	@Override
	public String execUruleReusltList(Map<String, Object> param, JSONObject uruleMap) {
 		if (configUtil.isNoUrule()) {
			return noUruleReturn(param, uruleMap);
		}
		WorkFlowServiceBean requestBean = getRequetParams(param, uruleMap);
		String jsonData = prepareReqMapDataByParamsMap(param, requestBean);
		String result ="";
		try {
			 result = httpTemplateCloudService.postForObject(uruleUrl, jsonData);
		} catch (Exception e) {
			log.error("规则服务访问失败：{}",e.getMessage(),e);
			throw new ServiceException("请检查规则服务是否启动，或者相关配置是否正确。");
		}
 		return getReturnResult(result, param, uruleMap);
 	}
}
