package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfViewTaskLog;
import com.bpm.workflow.mapper.WfViewTaskLogMapper;
import com.bpm.workflow.service.WfViewTaskLogService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfViewTaskLogServiceImpl extends ServiceImpl<WfViewTaskLogMapper, WfViewTaskLog> implements WfViewTaskLogService {
    @Resource
    private WfViewTaskLogMapper wfViewTaskLogMapper;
    @Override
    public WfViewTaskLog selectByTaskIdUserId(WfViewTaskLog wfViewTaskLog) {
        LambdaQueryWrapper<WfViewTaskLog> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfViewTaskLog::getTaskId,wfViewTaskLog.getTaskId());
        queryWrapper.eq(WfViewTaskLog::getUserId,wfViewTaskLog.getUserId());
        return wfViewTaskLogMapper.selectOne(queryWrapper);
    }

    @Override
    public void insertWfViewTaskLog(WfViewTaskLog wfViewTaskLog) {
        wfViewTaskLogMapper.insert(wfViewTaskLog);
    }

    @Override
    public void updateByTaskIdUserId(WfViewTaskLog wfViewTaskLog) {
        LambdaUpdateWrapper<WfViewTaskLog> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(WfViewTaskLog::getTaskId,wfViewTaskLog.getTaskId());
        updateWrapper.eq(WfViewTaskLog::getUserId,wfViewTaskLog.getUserId());
        wfViewTaskLogMapper.update(wfViewTaskLog,updateWrapper);
    }
}
