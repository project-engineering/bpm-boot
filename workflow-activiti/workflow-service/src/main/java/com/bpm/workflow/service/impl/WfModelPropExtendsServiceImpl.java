package com.bpm.workflow.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowModelPropExtData;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfPropertiesConfig;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfModelPropConfigMapper;
import com.bpm.workflow.mapper.WfModelPropExtendsMapper;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.CacheUtil;
import com.bpm.workflow.util.ModelProperties;
import com.bpm.workflow.util.ObjectUtil;
import com.bpm.workflow.util.UtilValidate;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.script.*;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Log4j2
@Service
public class WfModelPropExtendsServiceImpl extends ServiceImpl<WfModelPropExtendsMapper, WfModelPropExtends> implements WfModelPropExtendsService {
    @Resource
    private WfModelPropExtendsMapper wfModelPropExtendsMapper;
    @Resource
    private WfModelPropConfigMapper modelPropConfigMapper;
    @Resource
    private CacheUtil cacheUtil;

    @Override
    public WfModelPropExtends selectByOwnIdPropName(WfModelPropExtends wfNodepropExtends) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfModelPropExtends::getOwnId, wfNodepropExtends.getOwnId());
        queryWrapper.eq(WfModelPropExtends::getPropName, wfNodepropExtends.getPropName());
        return wfModelPropExtendsMapper.selectOne(queryWrapper);
    }

    @Override
    public void insertWfModelPropExtends(WfModelPropExtends wfNodepropExtends) {
        wfModelPropExtendsMapper.insert(wfNodepropExtends);
    }

    @Override
    public void deleteByOwnIdPropName(WfModelPropExtends wfNodepropExtends) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfModelPropExtends::getOwnId, wfNodepropExtends.getOwnId());
        queryWrapper.eq(WfModelPropExtends::getPropName, wfNodepropExtends.getPropName());
        wfModelPropExtendsMapper.delete(queryWrapper);
    }

    @Override
    public List<WfModelPropExtends> selectModelExtendsPropByOwnId(String ownId) {
        return cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).getCacheListValueByKey("selectModelExtendsPropByOwnId", ownId, null);
    }

    @Override
    public void deleteModelExtendPropByOwnId(String ownId) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfModelPropExtends::getOwnId, ownId);
        wfModelPropExtendsMapper.delete(queryWrapper);
    }

    @Override
    public WfModelPropExtends selectById(Integer id) {
        return wfModelPropExtendsMapper.selectById(id);
    }

    @Override
    public void updateByOwnIdPropName(WfModelPropExtends nodepropExtends) {
        LambdaUpdateWrapper<WfModelPropExtends> updateWrapper = new LambdaUpdateWrapper<>();
        if (UtilValidate.isNotEmpty(nodepropExtends.getId())) {
            updateWrapper.eq(WfModelPropExtends::getId, nodepropExtends.getId());
        }
        if (UtilValidate.isNotEmpty(nodepropExtends.getOwnId())) {
            updateWrapper.set(WfModelPropExtends::getOwnId, nodepropExtends.getOwnId());
        }
        if (UtilValidate.isNotEmpty(nodepropExtends.getPropName())) {
            updateWrapper.set(WfModelPropExtends::getPropName, nodepropExtends.getPropName());
        }
        wfModelPropExtendsMapper.update(nodepropExtends, updateWrapper);
    }

    @Override
    public List<WfModelPropExtends> selectModelExtendsPropByOwnIdAndOwnType(WfModelPropExtends wfModelPropExtends) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfModelPropExtends::getOwnId, wfModelPropExtends.getOwnId());
        queryWrapper.eq(WfModelPropExtends::getOwnType, wfModelPropExtends.getOwnType());
        return wfModelPropExtendsMapper.selectList(queryWrapper);
    }

    /**
     * 属性值模糊查询所有扩展属性
     * @param propValue
     * @return
     */
    @Override
    public List<WfModelPropExtends> selectModelExtendsPropByPropVal(String propValue) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(WfModelPropExtends::getPropValue, propValue);
        return wfModelPropExtendsMapper.selectList(queryWrapper);
    }

    /**
     * 获取自动分配的属性列表
     * @return
     */
    @Override
    public List<WfModelPropExtends> selectAutoAllotList() {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        List<String> propNames = new ArrayList<>();
        propNames.add("flowAutoAllot");
        propNames.add("nodeAutoAllot");
        queryWrapper.in(WfModelPropExtends::getPropName, propNames);
        return wfModelPropExtendsMapper.selectList(queryWrapper);
    }

    @Override
    public List<WfModelPropExtends> selectList(WfModelPropExtends selectCondition, PageData<Object> objectPageData) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(selectCondition.getOwnId())) {
            queryWrapper.eq(WfModelPropExtends::getOwnId, selectCondition.getOwnId());
        }
        if (UtilValidate.isNotEmpty(selectCondition.getOwnType())) {
            queryWrapper.eq(WfModelPropExtends::getOwnType, selectCondition.getOwnType());
        }
        if (UtilValidate.isNotEmpty(selectCondition.getPropName())) {
            queryWrapper.eq(WfModelPropExtends::getPropName, selectCondition.getPropName());
        }
        if (UtilValidate.isNotEmpty(selectCondition.getPropValue())) {
            queryWrapper.like(WfModelPropExtends::getPropValue, selectCondition.getPropValue());
        }
        if (UtilValidate.isNotEmpty(selectCondition.getPropDesc())) {
            queryWrapper.like(WfModelPropExtends::getPropDesc, selectCondition.getPropDesc());
        }
        if (UtilValidate.isNotEmpty(selectCondition.getType())) {
            queryWrapper.like(WfModelPropExtends::getType, selectCondition.getType());
        }
        Page page = new Page(objectPageData.getCurPage(), objectPageData.getLimit());
        List<WfModelPropExtends> list = wfModelPropExtendsMapper.selectPage(page, queryWrapper).getRecords();
        return list;
    }

    @Override
    public List<WfModelPropExtends> selectList(WfModelPropExtends wfModelPropExtends) {
        LambdaQueryWrapper<WfModelPropExtends> queryWrapper = new LambdaQueryWrapper<>();

        if (UtilValidate.isNotEmpty(wfModelPropExtends.getPropName())) {
            queryWrapper.eq(WfModelPropExtends::getPropName, wfModelPropExtends.getPropName());
        }
        if (UtilValidate.isNotEmpty(wfModelPropExtends.getPropValue())) {
            queryWrapper.like(WfModelPropExtends::getPropValue, wfModelPropExtends.getPropValue());
        }
        return wfModelPropExtendsMapper.selectList(queryWrapper);
    }

    /**
     * 增加一条扩展属性
     *
     * @param modelPropExtData
     * @
     */
    @Transactional
    public void insertModelPropExtend(WorkFlowModelPropExtData modelPropExtData) {

        checkInputParam(modelPropExtData);
        String ownType = modelPropExtData.getOwnType();
        if (StrUtil.isBlank(ownType)) {
            throw new ServiceException("属性所属模型类型不能为空！");
        }
        log.info(" | - WfModelpropExtendsService>>>>>insertModelPropExtend:  modelPropExtData.getPropName():{} ,modelPropExtData.getOwnType():{}", modelPropExtData.getPropName(), modelPropExtData.getOwnType());
        boolean isInList = false;
        for (ModelProperties itemProp : ModelProperties.values()) {
            if (modelPropExtData.getPropName().equals(itemProp.getName())
                    && modelPropExtData.getOwnType().equals(itemProp.getOwnType())) {
                modelPropExtData.setType(itemProp.getType());
                if (StrUtil.isBlank(modelPropExtData.getPropDesc())) {
                    modelPropExtData.setPropDesc(itemProp.getDesc());
                }
                isInList = true;
                break;
            }
        }
        if (!isInList) {
            throw new ServiceException("扩展属性不是预设属性，请查看！");
        }

        WfModelPropExtends wfNodepropExtends = ObjectUtil.copyBeanProp(modelPropExtData, WfModelPropExtends.class);

        WfModelPropExtends hasexist = selectByOwnIdPropName(wfNodepropExtends);
        if (hasexist != null) {
            throw new ServiceException(hasexist.getPropDesc() + "已经存在，不能重复提交！");
        }

        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{wfNodepropExtends.getOwnId(), wfNodepropExtends.getPropName()}, true);

        insertWfModelPropExtends(wfNodepropExtends);
    }

    /**
     * 批量增加扩展属性
     *
     * @param list
     * @
     */
    public void insertModelPopForBatch(List<WfModelPropExtends> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        for (WfModelPropExtends toBeInsert : list) {
            cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{toBeInsert.getOwnId(), toBeInsert.getPropName()}, true);

        }
        saveBatch(list);
    }

    /**
     * 删除一条扩展属性
     *
     * @param modelPropExtData
     * @
     */
    public void deleteModelPropExtend(WorkFlowModelPropExtData modelPropExtData) {
        checkInputParam(modelPropExtData);
        WfModelPropExtends wfNodepropExtends = ObjectUtil.copyBeanProp(modelPropExtData, WfModelPropExtends.class);

        WfModelPropExtends toBeUpdate = null;
        if (StrUtil.isEmpty(modelPropExtData.getOwnId()) || StrUtil.isEmpty(modelPropExtData.getPropName())) {
            throw new RuntimeException("ownerId or propName can't be null");
        }
        toBeUpdate = selectByOwnIdPropName(modelPropExtData);
        if (toBeUpdate == null || StrUtil.isEmpty(toBeUpdate.getOwnId())) {
            return;
        }
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{toBeUpdate.getOwnId(), toBeUpdate.getPropName()}, true);
        deleteByOwnIdPropName(wfNodepropExtends);
    }

    /**
     * 删除一个节点的所有扩展属性
     *
     * @
     */
    public void deleteModelPropExtendByNodeId(String nodeId) {
        List<WfModelPropExtends> toBeDeletes = selectModelExtendsPropByOwnId(nodeId);
        if (toBeDeletes == null || toBeDeletes.size() == 0) {
            return;
        }
        for (WfModelPropExtends toBeDelete : toBeDeletes) {
            cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{toBeDelete.getOwnId(), toBeDelete.getPropName()}, true);

        }
        deleteModelExtendPropByOwnId(nodeId);
    }

    /**
     * 修改节点扩展属性
     *
     * @param nodepropExtends
     * @
     */
    public void updateModelPropExtend(WfModelPropExtends nodepropExtends) {
        if (nodepropExtends == null) {
            throw new RuntimeException("param can't be null");
        }
        WfModelPropExtends toBeUpdate = null;
        if (nodepropExtends.getId() != null) {
            toBeUpdate = selectById(nodepropExtends.getId());
        } else {
            if (StrUtil.isEmpty(nodepropExtends.getOwnId()) || StrUtil.isEmpty(nodepropExtends.getPropName())) {
                throw new RuntimeException("ownerId or propName can't be null");
            }
            toBeUpdate = selectByOwnIdPropName(nodepropExtends);
        }
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{toBeUpdate.getOwnId(), toBeUpdate.getPropName()}, true);

        updateByOwnIdPropName(nodepropExtends);
    }

    /**
     * 查询一个节点的所有扩展属性
     * @param workFlowModelPropExtData
     * @return
     */
    @Override
    public List<WfModelPropExtends> selectModelPropExtendList(WorkFlowModelPropExtData workFlowModelPropExtData) {
        if (StrUtil.isBlank(workFlowModelPropExtData.getOwnId()) || StrUtil.isBlank(workFlowModelPropExtData.getOwnType())) {
            throw new ServiceException("属性所属不能为空！");
        }
        return wfModelPropExtendsMapper.selectModelExtendsPropByOwnIdAndOwnType(workFlowModelPropExtData);
    }


    /**
     * 查看节点扩展属性
     *
     * @param nodeId
     * @
     */
    public WfModelPropExtends viewModelPropExtend(String nodeId, String propName) {
        List<WfModelPropExtends> propList = this.selectModelPropExtendList(nodeId);
        for (WfModelPropExtends index : propList) {
            if (index.getPropName().equals(propName)) {
                return index;
            }
        }
        return null;
    }

    /**
     * 根据节点定义ID查询节点的所有的扩展属性
     *
     * @param nodeId
     * @return
     * @
     */
    public List<WfModelPropExtends> selectModelPropExtendList(String nodeId) {
        if (StrUtil.isBlank(nodeId)) {
            throw new ServiceException("属性所属不能为空！");
        }
        return selectModelExtendsPropByOwnId(nodeId);
    }

    /**
     * 获取扩展节点指定属性的扩展值
     *
     * @param nodeId
     * @param propName
     * @return
     */
    public String getModelPropExtendsValue(String nodeId, String propName) {
        WfModelPropExtends modelProp = viewModelPropExtend(nodeId, propName);
        if (modelProp != null) {
            return modelProp.getPropValue();
        }
        return "";
    }

    private void checkInputParam(WorkFlowModelPropExtData modelPropExtData) {
        if (StrUtil.isBlank(modelPropExtData.getOwnId())) {
            throw new ServiceException("属性所属节点标识不能为空！");
        }
        if (StrUtil.isBlank(modelPropExtData.getPropName())) {
            throw new ServiceException("扩展属性名称不能为空！");
        }
    }

    public List<WfModelPropExtends> selectModelPropExtendList(WfModelPropExtends model) {
        if (StrUtil.isBlank(model.getOwnId()) || StrUtil.isBlank(model.getOwnType())) {
            throw new ServiceException("属性所属不能为空！");
        }
        return selectModelExtendsPropByOwnIdAndOwnType(model);
    }

    public WfModelPropExtends selectByOwnIdPropName(String ownId, String propName) {
        return (WfModelPropExtends) cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).getCacheValueByKey("selectByOwnIdPropName", ownId, propName);
    }

    @SuppressWarnings("unchecked")
    public void addUseProcessLatesVersion(String processId, Integer version) {
        List<WfDefinition> theWfDefinitions = cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getCacheListValueByKey("selectByDefId", processId, null);
        if (theWfDefinitions == null) {
            return;
        }
        if (theWfDefinitions.size() == 1 && theWfDefinitions.get(0).getVersion().intValue() == 0) {
            WorkFlowModelPropExtData modelPropExtData = new WorkFlowModelPropExtData();
            modelPropExtData.setOwnId(processId);
            modelPropExtData.setPropName(ModelProperties.ignoreProcessVersion.getName());
            deleteModelPropExtend(modelPropExtData);
        }


        WfModelPropExtends wfModelpropExtends = selectByOwnIdPropName(processId, ModelProperties.ignoreProcessVersion.getName());
        if (existVersionInIgnoreProcessVersion(version, wfModelpropExtends)) {
            return;
        }

        if (wfModelpropExtends == null) {
            wfModelpropExtends = new WfModelPropExtends();
            wfModelpropExtends.setOwnId(processId);
            wfModelpropExtends.setOwnType(ModelProperties.ignoreProcessVersion.getOwnType());
            wfModelpropExtends.setPropName(ModelProperties.ignoreProcessVersion.getName());
            wfModelpropExtends.setPropValue("" + version);
            wfModelpropExtends.setPropDesc(ModelProperties.ignoreProcessVersion.getDesc());
            wfModelpropExtends.setType(ModelProperties.ignoreProcessVersion.getType());
            wfModelpropExtends.setVersion(0);
            cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{wfModelpropExtends.getOwnId(), wfModelpropExtends.getPropName()}, true);

            insertWfModelPropExtends(wfModelpropExtends);
        } else {
            cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfModelPropExtends).refreshByKey(CacheUtil.SvcType.wfModelPropExtends, null, new String[]{wfModelpropExtends.getOwnId(), wfModelpropExtends.getPropName()}, true);
            wfModelpropExtends.setPropValue(wfModelpropExtends.getPropValue() + "," + version);
            updateByOwnIdPropName(wfModelpropExtends);
        }
    }

    public boolean existVersionInIgnoreProcessVersion(String processId, Integer version) {
        WfModelPropExtends wfModelpropExtends = selectByOwnIdPropName(processId, ModelProperties.ignoreProcessVersion.getName());
        return existVersionInIgnoreProcessVersion(version, wfModelpropExtends);
    }

    private boolean existVersionInIgnoreProcessVersion(Integer version, WfModelPropExtends wfModelpropExtends) {
        if (wfModelpropExtends == null || StrUtil.isEmpty(wfModelpropExtends.getPropValue())) {
            return false;
        }
        if (wfModelpropExtends.getPropValue().trim().equals("" + version)) {
            return true;
        } else if (wfModelpropExtends.getPropValue().endsWith(SymbolConstants.COMMA + version)) {
            return true;
        } else if (wfModelpropExtends.getPropValue().startsWith(version + SymbolConstants.COMMA)) {
            return true;
        } else {
            return wfModelpropExtends.getPropValue().indexOf(SymbolConstants.COMMA + version + SymbolConstants.COMMA) > 0;
        }
    }

    /**
     * @return
     */
    public Map<String, WfModelPropExtends> getAutoAllotExtends() {
        Map<String, WfModelPropExtends> map = new HashMap<>();
        List<WfModelPropExtends> list = selectAutoAllotList();
        for (WfModelPropExtends model : list) {
            map.put(model.getOwnId(), model);
        }
        return map;
    }


    /**
     * 获取渠道不可查询待办的节点列表
     *
     * @param propName
     * @param propValuekey
     * @param checkChannel
     * @param isOkNode
     * @return
     */
    public Set<String> getEnableSelectNodeByCheckChannel(String propName, String propValuekey, String checkChannel, boolean isOkNode) {
        // 查询所有的节点扩展属性
        WfModelPropExtends selectCondition = new WfModelPropExtends();
        selectCondition.setOwnType("0");
        // 任务转授权控制到流程层面
        List<WfModelPropExtends> list = selectList(selectCondition, new PageData<>());
        // 任务转授权控制到流程层面

        //
        Set<String> allSet = new HashSet<String>();
        Set<String> notOkSet = new HashSet<String>();
        for (WfModelPropExtends mProp : list) {

            //统计全部节点的Id
            allSet.add(mProp.getOwnId());

            //统计出所有配置了查询渠道控制属性但不允许查询
            if (mProp.getPropName().equals(propName)) {
                String propValue = mProp.getPropValue();
                if (StrUtil.isNotBlank(propValue)) {

                    String targetValue = "";
                    try {
                        JSONObject jsonPropValue = JSONObject.parseObject(propValue);
                        if (jsonPropValue.containsKey(propValuekey)) {
                            targetValue = jsonPropValue.getString(propValuekey);
                        }
                    } catch (Exception e) {
                        targetValue = propValue;
                    }

                    if (StrUtil.isNotBlank(targetValue)) {
                        String[] tempArrs = propValue.split(",");
                        boolean isOk = false;
                        for (String temp : tempArrs) {
                            if (temp.equals(checkChannel)) {
                                isOk = true;
                            }
                        }
                        if (!isOk) {
                            notOkSet.add(mProp.getOwnId());
                        }
                    }
                }
            }
        }

        if (isOkNode) {
            allSet.removeAll(notOkSet);
        } else {
            return notOkSet;
        }

        return allSet;
    }


    /**
     * 查询可以配置的扩展属性
     *
     * @param propConfig
     */
    public List<WfPropertiesConfig> selectEnableConfigProperis(WfPropertiesConfig propConfig) {
        // 进行数据查询
        List<WfPropertiesConfig> tempList = modelPropConfigMapper.selectConfigPropList(propConfig);
        List<WfPropertiesConfig> resultList = new ArrayList<WfPropertiesConfig>();
        // 循环操作进行父子关系对接
        for (WfPropertiesConfig root : tempList) {
            for (WfPropertiesConfig child : tempList) {
                if (root == child) {
                    continue;
                }
                if (root.getPropKey().equals(child.getGroupKey())) {
                    root.getChildList().add(child);
                }
            }
            if ("root".equals(root.getGroupKey())) {
                resultList.add(root);
            }
        }

        // 对扩展属性进行排序
        Comparator<WfPropertiesConfig> comparator = new Comparator<WfPropertiesConfig>() {
            @Override
            public int compare(WfPropertiesConfig o1, WfPropertiesConfig o2) {
                return o1.getOrderFlag().compareTo(o2.getOrderFlag());
            }
        };

        List<WfPropertiesConfig> delNoChild = new ArrayList<WfPropertiesConfig>();
        for (WfPropertiesConfig group : resultList) {
            if (CollectionUtils.isNotEmpty(group.getChildList())) {
                Collections.sort(group.getChildList(), comparator);
            } else {
                delNoChild.add(group);
            }
        }
        resultList.removeAll(delNoChild);
        Collections.sort(resultList, comparator);
        return resultList;
    }

    /**
     * 脚步执行器接口
     * @param js
     * @param businessMap
     * @return
     */
    @Override
    public String executeJavaScript(String js, HashMap<String, Object> businessMap) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        Set<String> keySet = businessMap.keySet();
        Iterator<String> iter = keySet.iterator();
        while (iter.hasNext()) {
            String s = (String) iter.next();
            engine.put(s, businessMap.get(s));
        }

        String result = null;
        if (engine instanceof Compilable) {
            Compilable compEngine = (Compilable) engine;
            try {
                CompiledScript script = compEngine.compile(js);
                Object temp = script.eval();
                if (temp != null) {
                    result = temp.toString();
                }
                temp = null;
            } catch (ScriptException e) {
                throw new ServiceException(e.getMessage());
            }
        } else {
            log.info("| - WfCommServiceImpl>>>>>getConditionResult>>>>>>Engine can't compile code");
        }
        log.info("| - WfCommServiceImpl>>>>>decisionScript result:[{}]",result);
        return result;
    }

    /**
     * 脚步执行器接口
     *
     * @param varMap
     * @return
     * @throws Exception
     */
    public String executeJavaScript(String javaScriptStr, Map<String, Object> varMap) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        Set<String> keySet = varMap.keySet();
        Iterator<String> iter = keySet.iterator();
        while (iter.hasNext()) {
            String s = (String) iter.next();
            engine.put(s, varMap.get(s));
        }

        String result = null;
        if (engine instanceof Compilable) {
            Compilable compEngine = (Compilable) engine;
            try {
                CompiledScript script = compEngine.compile(javaScriptStr);
                Object temp = script.eval();
                if (temp != null) {
                    result = temp.toString();
                }
                temp = null;
            } catch (ScriptException e) {
                throw new ServiceException(e.getMessage());
            }
        } else {
            log.info("| - WfCommServiceImpl>>>>>getConditionResult>>>>>>Engine can't compile code");
        }
        log.info("| - WfCommServiceImpl>>>>>decisionScript result:[{}]", result);
        return result;
    }

}
