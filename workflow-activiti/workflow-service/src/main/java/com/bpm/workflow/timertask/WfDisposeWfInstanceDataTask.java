package com.bpm.workflow.timertask;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.entity.WfHistTaskflow;
import com.bpm.workflow.entity.WfRuIdentitylink;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.service.WfHistTaskflowService;
import com.bpm.workflow.service.WfRuIdentitylinkService;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.util.constant.BpmConstants;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import java.util.ArrayList;
import java.util.List;

/**
 * 将审批记录信息数据转移至历史表中
 */
@Log4j2
public class WfDisposeWfInstanceDataTask extends WorkflowTimerTask {
    private static final long serialVersionUID = -4938904232317438081L;
    private String processInstanceId;
	@Resource
	private WfHistTaskflowService wfHistTaskflowService;
	@Resource
	private WfTaskflowService wfTaskflowService;
	@Resource
	private WfRuIdentitylinkService wfRuIdentitylinkService;

	/**
     * @param processInstId
     */
    public WfDisposeWfInstanceDataTask(String processInstId) {
        this.processInstanceId = processInstId;
    }

    @Override
    public void runImpl() throws Exception {
        if (StrUtil.isBlank(processInstanceId)) {
            return;
        }

        List<WfTaskflow> taskList = wfTaskflowService.selectByProcessInstId(processInstanceId);
        List<WfHistTaskflow> histTaskList = new ArrayList<>();
        for (WfTaskflow temp : taskList) {
            try {
                WfHistTaskflow wfHistTaskflow = new WfHistTaskflow();
                BeanUtil.copyProperties(temp, wfHistTaskflow);
                wfHistTaskflow.setWorkState(BpmConstants.WORK_STATE_1);
                histTaskList.add(wfHistTaskflow);
                wfTaskflowService.deleteById(temp.getId());
                WfRuIdentitylink wfRuIdentitylink = new WfRuIdentitylink();
                wfRuIdentitylink.setTaskId(temp.getTaskId());
                wfRuIdentitylinkService.deleteByTaskId(wfRuIdentitylink);
                wfHistTaskflowService.insertWfHistTaskflow(wfHistTaskflow);
            } catch (Exception e) {
                log.error("| - WfDisposeWfInstanceDataTask>>>>>将流程[{}]挪到历史表中失败！", processInstanceId, e);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("| - WfDisposeWfInstanceDataTask>>>>>将流程[{}]挪到历史表中", processInstanceId);
        }
    }

}
