package com.bpm.workflow.client;

import com.alibaba.fastjson2.JSONArray;
import com.bpm.workflow.dto.transfer.UserOutputData;
import java.util.List;
import java.util.Map;

public interface IQueryRelationTeller {

    public List<UserOutputData> queryTellerByPolicy(String taskId, String processDefId, String processInstId, String jsonStr, List<Map<String,Object>> userList, Map<String,Object> busMap, String branch) throws Exception ;

    public List<UserOutputData> queryUserList(JSONArray userAskListJson) throws Exception;

    public List<UserOutputData> queryUserList(JSONArray userAskListJson,Map<String,String> umap,Map<String,Object> varMap)  throws Exception;

    public List<UserOutputData> queryUserList(String userId);
}
