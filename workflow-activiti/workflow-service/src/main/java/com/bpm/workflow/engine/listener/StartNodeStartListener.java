package com.bpm.workflow.engine.listener;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.entity.WfBusinessData;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.WfBusinessDataService;
import com.bpm.workflow.service.WfInstanceService;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.ProcessUtil;
import com.bpm.workflow.util.SpringUtil;
import com.bpm.workflow.util.WfVariableUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 开始节点的启动监听
 */
@Log4j2
@Component("startNodeStartListener")
public class StartNodeStartListener extends SpringUtil implements ExecutionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6244959132818400740L;

	@Resource
	private WfCommService wfCommService;

	@Resource
	private WfInstanceService wfInstanceService;

	@Resource
	private ProcessUtil processUtil;

	@Resource
	private WfTaskflowService wfTaskflowService;
	
	@Resource
	private SequenceFlowCadition sequenceFlowCadition;
	@Resource
	private WfBusinessDataService wfBusinessDataService;
	@Override
	public void notify(DelegateExecution execution) {
		FlowElement flowElement = execution.getCurrentFlowElement();
		if (flowElement == null) {
			return;
		}

		Map<String, Object> variableMap = WfVariableUtil.getVariableMap();
		
		Map<String, Object> tempVarMap= new HashMap<String, Object>();
		// 处理流程分支
		boolean boolSkipNode = processUtil.isSikpFirstNode(execution.getProcessDefinitionId());
		boolean isRoulte = false;
		if (!boolSkipNode) {
			// 当首节点不跳过，需要做人工路由检查
			try {
				isRoulte = wfCommService.forkRouteSelect(execution.getProcessDefinitionId(),flowElement, variableMap);
			} catch (Exception e) {
				throw new ServiceException(e.getMessage());
			}
		} else {
			// 当首节点跳过，不做人工路由检查
			String routleOutValue = wfCommService.roultRuleCommService(execution.getProcessDefinitionId(),flowElement, variableMap);
			if (StrUtil.isNotBlank(routleOutValue)) {
				if (log.isInfoEnabled()) {
					log.info(" | - StartNodeStartListener>>>>>启动流程分支结果：{}" , routleOutValue);
				}
				tempVarMap.put(Constants.ROUTE_CONDITION_KEY, routleOutValue);
				isRoulte = true;
			}
			if (variableMap.containsKey(Constants.ROUTE_SELECT_KEY)) {
				JSONArray array = (JSONArray) (variableMap.get(Constants.ROUTE_SELECT_KEY));
				tempVarMap.put(Constants.ROUTE_CONDITION_KEY, array);
				isRoulte = true;
			}
		}
		
		tempVarMap.put(Constants.BEFOR_TASKID, "00000");
		variableMap.putAll(tempVarMap);
		WfVariableUtil.addAllVariableMap(variableMap);

		// 提前判断下路由分支是否满足
		if (isRoulte) {
			sequenceFlowCadition.mathcCaditionCheck(variableMap, flowElement);
		}
        
		
		// 增加开始流转记录
		addStartWfTaskflow(execution, variableMap, flowElement);

	}

	/**
	 * 增加开始流转记录
	 * 
	 * @param execution
	 * @param varibaleMap
	 * @param startElemet
	 */
	private void addStartWfTaskflow(DelegateExecution execution, Map<String, Object> varibaleMap,
			FlowElement startElemet) {
		WfTaskflow taskflow = new WfTaskflow();
		taskflow.setTaskId("00000");
		taskflow.setProcessId(execution.getProcessInstanceId());
		String processDefId = execution.getProcessDefinitionId();
		taskflow.setProcessCode(processDefId);
		taskflow.setTaskType(Constants.NODETYPE_START);
		WfInstance wfInstance = wfInstanceService.selectByProcessId(execution.getProcessInstanceId());
		if (wfInstance != null) {
			String nodePath = wfInstance.getSystemCode();
			taskflow.setSystemCode(nodePath);
			String startUserId = wfInstance.getStartUserId();
			taskflow.setWorkCreater(startUserId);
			taskflow.setWorkCreater(startUserId);
			taskflow.setDoTaskActor(startUserId);
			JSONObject startUserMap = JSONObject.parseObject(wfInstance.getStartUserMap());
			if(startUserMap != null){
				String startUserName = (String)startUserMap.get(Constants.USERNAME_KEY);
				taskflow.setWorkCreaterName(startUserName);
				taskflow.setDoTaskActorName(startUserName);
			}
			String desc = wfInstance.getProcessInstDesc();
			if(StrUtil.isBlank(desc)){
				desc= wfInstance.getProcessDesc();
			}
			taskflow.setWfDesc(desc);
		}
		taskflow.setTaskDefId(startElemet.getId());
		taskflow.setTaskName(startElemet.getName());
		taskflow.setParentTaskId(execution.getId());
		String taskUargent = "0";
		if (varibaleMap.containsKey(Constants.TASK_URGENT_KEY)) {
			taskUargent = (String) varibaleMap.get(Constants.TASK_URGENT_KEY);
		}

		String tranChannel = "";
		if (varibaleMap.containsKey(Constants.INPUT_TRANCHANNEL_KEY)) {
			tranChannel = (String) varibaleMap.get(Constants.INPUT_TRANCHANNEL_KEY);
		}
		taskflow.setTranChannel(tranChannel);
		taskflow.setTaskUrgent(Integer.valueOf(taskUargent.trim()));
		// 任务紧急程度、任务发起渠道修改
		taskflow.setTaskCode("startNode");
		LocalDateTime date = LocalDateTime.now();
		taskflow.setStartCreateDate(date);
		taskflow.setCreateDate(date);
		taskflow.setStartDate(date);
		taskflow.setUpdateDate(date);
		taskflow.setEndDate(date);
		taskflow.setWorkState(Constants.TASK_STATE_END);
		taskflow.setDuedata(0L);
		taskflow.setBussinessMap(BpmStringUtil.mapToJson(varibaleMap).toString());
		wfTaskflowService.insertWfTaskflow(taskflow);
		WfBusinessData businessData=new WfBusinessData(taskflow);
		wfBusinessDataService.insertWfBusinessData(businessData );

	}

}
