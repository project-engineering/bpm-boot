package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfBusinessData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfBusinessDataService extends IService<WfBusinessData> {

    void insertWfBusinessData(WfBusinessData businessData);

    WfBusinessData selectById(String id);
}
