package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfExceptionData;
import com.bpm.workflow.mapper.WfExceptionDataMapper;
import com.bpm.workflow.service.WfExceptionDataService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfExceptionDataServiceImpl extends ServiceImpl<WfExceptionDataMapper, WfExceptionData> implements WfExceptionDataService {
    @Resource
    private WfExceptionDataMapper wfExceptionDataMapper;

    @Override
    public void insertWfExceptionData(WfExceptionData wfExceptionData) {
        wfExceptionDataMapper.insert(wfExceptionData);
    }
}
