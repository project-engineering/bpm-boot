package com.bpm.workflow.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfParamChannel;
import com.bpm.workflow.mapper.WfParamChannelMapper;
import com.bpm.workflow.service.WfParamChannelService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfParamChannelServiceImpl extends ServiceImpl<WfParamChannelMapper, WfParamChannel> implements WfParamChannelService {
    @Resource
    private WfParamChannelMapper wfParamChannelMapper;

    @Override
    public void deleteByItemName(String itemName) {
        LambdaQueryWrapper<WfParamChannel> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfParamChannel::getItemName,itemName);
        wfParamChannelMapper.delete(queryWrapper);
    }

    /**
     * 批量保存渠道数据
     * @param itemName
     * @param itemChannelTypeList
     */
    @Override
    public void saveBatch(String itemName, List<String> itemChannelTypeList) {
        List<WfParamChannel> itemChannelList = new ArrayList<>();

        WfParamChannel wfParamChannel = null;
        // 组装保存数据
        for (String itemChannelType : itemChannelTypeList) {
            wfParamChannel = new WfParamChannel();
            wfParamChannel.setItemName(itemName);
            wfParamChannel.setItemChannelType(itemChannelType);
            itemChannelList.add(wfParamChannel);
        }
        // 批量插入数据
        if(ObjectUtil.isNotEmpty(itemChannelList)){
            this.saveBatch(itemChannelList);
        }
    }
}
