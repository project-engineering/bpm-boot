package com.bpm.workflow.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.HttpTemplateCloudService;
import com.bpm.workflow.client.WorkflowFeignClientService;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ServiceNameConstants;
import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.dto.transfer.WorkFlowServiceBean;
import com.bpm.workflow.entity.*;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.WfExceptionDataService;
import com.bpm.workflow.service.WfNodeStateService;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.vo.WorkflowCallBackRequest;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import java.util.*;

@Log4j2
public class EventServiceInvockUtil {
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private HttpTemplateCloudService httpTemplateCloudService;
    @Resource
    private WorkflowFeignClientService workflowFeignClientService;
    @Resource
    private WfExceptionDataService wfExceptionDataService;
    @Resource
    private WfNodeStateService wfNodeStateService;

    private Map<String, Object> inputMap;

    private Map<String, Object> outputMap;

    private Map<String, Object> variableMap;

    private Map<String, Object> eventDefMap;

    private Object result;

    private String confId;

    private String taskId;
    private String processDefId;
    private String processInstId;
    private String nodeType;
    private SysCommHead sysCommHead;
    private String nodeName;

    public EventServiceInvockUtil(Map<String, Object> variableMap, String confId, Map<String, Object> eventDefMap, String taskId, String processDefId, String processInstId, String nodeType, String nodeName, SysCommHead sysCommHead) {
        super();
        this.variableMap = variableMap;
        this.eventDefMap = eventDefMap;
        this.confId = confId;
        outputMap = new HashMap<>();
        inputMap = new HashMap<>();
        this.taskId = taskId;
        this.processDefId = processDefId;
        this.processInstId = processInstId;
        this.nodeType = nodeType;
        this.sysCommHead = sysCommHead;
        this.nodeName = nodeName;
        log.info("| - EventServiceInvockUtil>>>>>开始回调{任务ID[{}],流程定义ID[{}],流程实例ID[{}],业务参数[{}}]", taskId, processDefId, processInstId, variableMap.toString());

    }

    public EventServiceInvockUtil newEventServiceInstance(WfEvtServiceDef wfEvtServiceDef, Map<String, Object> variableMap, Map<String, Object> eventDefMap, String taskId, String processDefId, String processInstId, String nodeType, String nodeName, SysCommHead sysCommHead) throws Exception {
        EventServiceInvockUtil instance = new EventServiceInvockUtil(variableMap, wfEvtServiceDef.getId().toString(), eventDefMap, taskId, processDefId, processInstId, nodeType, nodeName, sysCommHead);
        result = instance.excuteAutoTaskService(wfEvtServiceDef);
        instance.setResult(result);
        return instance;
    }

    public EventServiceInvockUtil() {
        super();
    }

    public Map<String, Object> getInputMap() {
        return inputMap;
    }

    public void setInputMap(Map<String, Object> inputMap) {
        this.inputMap = inputMap;
    }


    public Map<String, Object> getOutputMap() {
        return outputMap;
    }

    public void setOutputMap(Map<String, Object> outputMap) {
        this.outputMap = outputMap;
    }

    public Map<String, Object> getVariableMap() {
        return variableMap;
    }

    public void setVariableMap(Map<String, Object> variableMap) {
        this.variableMap = variableMap;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    /**
     * 执行调用第三方服务接口
     *
     * @param wfEvtServiceDef 调用第三方方法的入参
     * @param wfEvtServiceDef 事件服务回调配置
     * @return
     */
    private String excuteAutoTaskService(WfEvtServiceDef wfEvtServiceDef)
            throws Exception {
        // 2、查询对应的配置信息
        if (wfEvtServiceDef == null) {
            log.info("| - EventServiceInvockUtil>>>>>找不到对应的服务配置[id=null]");
            throw new Exception("找不到对应的服务配置[id=null]信息，请确认是否已维护");
        }
        if (wfEvtServiceDef.getCallMode() == null) {
            log.info("| - EventServiceInvockUtil>>>>>流程定义时没有设置CallMode调用模式【{}-同步调动、{}-异步调用、{}-异步等待】,流程定义不正确", Constants.CALL_MODE_SYNC, Constants.CALL_MODE_ASYNC, Constants.CALL_MODE_ASYNC_WAIT);
            throw new Exception("流程定义时没有设置CallMode调用模式【" + Constants.CALL_MODE_SYNC + "-同步调动、" + Constants.CALL_MODE_ASYNC + "-异步调用、" + Constants.CALL_MODE_ASYNC_WAIT + "-异步等待】,流程定义不正确");
        }
        switch (wfEvtServiceDef.getCallMode()) {
            case Constants.CALL_MODE_SYNC:
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：同步不等待开始：");
                }
                String result = null;
                if (wfEvtServiceDef.getCallService().contains(ServiceNameConstants.RULE_SERVICES)) {
                    try {
                        result = this.invokeService(wfEvtServiceDef);
                    } catch (Exception e) {
                        log.error("同步调用服务RuleServices获取流水号异常：", e);
                    }

                } else {
                    result = this.invokeService(wfEvtServiceDef);
                }
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：同步不等待结束：更新出参：{}", variableMap.toString());
                }
                return result;
            case Constants.CALL_MODE_SYNC_WAIT:
                String result1 = "";
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：同步等待：");
                }
                if (wfEvtServiceDef.getCallService().contains(ServiceNameConstants.RULE_SERVICES)) {
                    try {
                        result1 = this.invokeService(wfEvtServiceDef);
                    } catch (Exception e) {
                        log.error("同步调用服务RuleServices获取流水号异常：", e);
                    }

                } else {
                    result1 = this.invokeService(wfEvtServiceDef);
                }
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：同步等待结束：更新出参：{}", variableMap.toString());
                }
                return result1;
            case Constants.CALL_MODE_ASYNC:
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：异步调动,自动调用");
                }
                this.synchInvokeService(wfEvtServiceDef);
                return "";
            case Constants.CALL_MODE_ASYNC_WAIT:
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>对外事件执行方式：异步等待");
                }
                this.synchInvokeService(wfEvtServiceDef);
                return null;
            default:
                if (log.isInfoEnabled()) {
                    log.info("| - EventServiceInvockUtil>>>>>waittype  default:{}", wfEvtServiceDef.getCallMode());
                }
                return null;

        }

    }

    /**
     * 异步调用第三方服务接口
     *
     * @param wfAutoTaskPropConf
     * @param wfAutoTaskPropConf
     */
    private void synchInvokeService(WfEvtServiceDef wfAutoTaskPropConf) {
        Thread mainThread = Thread.currentThread();
        Thread t1 = new Thread() {
            @Override
            public void run() {
                try {
                    invokeService(wfAutoTaskPropConf);
                } catch (Exception e) {
                    //异步调用事件服务失败增加catch块
                    log.error("| - EventServiceInvockUtil>>>>>异步调用第三方服务接口 异常：", e);
                    try {
                        WfExceptionData data = ErrorDataUtil.getTimeOutError(ExceptionUtils.getMessage(e), taskId, wfAutoTaskPropConf.getId().toString(),
                                processInstId, processDefId.split(":")[0], nodeType, nodeName, BpmStringUtil.mapToJson(variableMap).toJSONString());
                        wfExceptionDataService.insertWfExceptionData(data);
                    } catch (Exception e1) {
                        log.error("| - EventServiceInvockUtil>>>>>插入事件服务调用异常表wf_exception_data异常：", e1);
                    }
                } // 执行自动服务功能
            }
        };
        t1.start();

    }

    /**
     * 调用第三方服务
     *
     * @param wfEvtServiceDef
     * @return
     */
    public String invokeService(WfEvtServiceDef wfEvtServiceDef) {

        // 获取输入参数
        String inputBean = wfEvtServiceDef.getCallInput();
        // 获取输出参数
        String outputBean = wfEvtServiceDef.getCallOutput();
        String taskPropUrl = wfEvtServiceDef.getCallService();
        String flag = wfEvtServiceDef.getCallParamflag();
        //业务参数
        if (BpmConstants.PARAM_FLAG_1.equals(flag)) {
            // 设置入参
            outputMap = new HashMap<>();
            if (outputBean != null) {
                outputMap = parseJSONstr2Map(outputBean);
            }
            if (inputBean == null) {
                inputBean = configUtil.getEmptyHttpHeader();
            }
            inputMap = this.setMapValue(inputBean, variableMap);
        } else if (BpmConstants.PARAM_FLAG_3.equals(flag)) {
            inputMap = variableMap;
        } else {
            variableMap.putAll(eventDefMap);
            inputMap.putAll(variableMap);
            Map<String, Object> map = new HashMap<>();
            map = this.getNodeStateMap(inputBean);
            inputMap.putAll(map);
        }
        inputMap.putAll(eventDefMap);

        if (log.isInfoEnabled()) {
            log.info("| - EventServiceInvockUtil>>>>>调用服务[{}]", taskPropUrl);
            log.info("| - EventServiceInvockUtil>>>>>调用入参[{}]", inputMap);
            log.info("| - EventServiceInvockUtil>>>>>调用事件参数[{}]", eventDefMap);
            log.info("| - EventServiceInvockUtil>>>>>调用出参[{}]", outputMap);
            log.info("| - EventServiceInvockUtil>>>>>报文头[{}]", sysCommHead);
        }
        if (StrUtil.isBlank(taskPropUrl)) {
            log.info("| - EventServiceInvockUtil>>>>>调用服务配置为空");
            return null;
        }

        try {
            result = execSpringCloudService(wfEvtServiceDef, inputMap);
            if (log.isInfoEnabled()) {
                log.info("| - EventServiceInvockUtil>>>>>SPRINGCLOUD服务调用成功[" + wfEvtServiceDef.getCallService() + ":" + wfEvtServiceDef.getCallMethod()
                        + "outputMap:" + outputMap + ",\n result:" + result + "]");
            }
        } catch (ServiceException e) {
            log.info("| - EventServiceInvockUtil>>>>>服务调用失败！" + e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            throw e;

        } catch (Exception e) {
            log.info("| - EventServiceInvockUtil>>>>>服务调用失败！请检查服务调用时必须的参数是否正确！网络是否正常");
            log.error(ExceptionUtils.getStackTrace(e));
            throw new ServiceException("请求异常，请检查服务！");
        }
        this.setOutPutMap(variableMap, outputMap);
        return result == null ? null : result.toString();
    }

    /**
     * 执行springCloud服务
     *
     * @param wfEvtServiceDef
     * @param inputMap2
     * @return
     */
    @SuppressWarnings("unchecked")
    private Object execSpringCloudService(WfEvtServiceDef wfEvtServiceDef, Map<String, Object> inputMap2) {
        log.info("| - execSpringCloudService.start>>>>>>>>>>");
        HttpTemplateCloudService httpTemplateCloudService = SpringUtil.getBean(HttpTemplateCloudService.class);
        WorkflowCallBackRequest workflowCallBackRequest = new WorkflowCallBackRequest();
        // 区分终止流程、打回
        if (Constants.EVENT_P6.equals(inputMap2.get("eventCode"))) {
            workflowCallBackRequest.setApprovalResult("1");
        } else if (cn.hutool.core.util.ObjectUtil.isNotEmpty(inputMap2.get(Constants.BACK_FROM_TASK))) {
            String backFromTask = String.valueOf(inputMap2.get(Constants.BACK_FROM_TASK));
            if (BpmConstants.DO_RESULT_4.equals(backFromTask) || BpmConstants.DO_RESULT_5.equals(backFromTask) || BpmConstants.DO_RESULT_7.equals(backFromTask)) {
                workflowCallBackRequest.setApprovalResult(backFromTask);
            }
        }
        // 为空取流程参数
        if (cn.hutool.core.util.ObjectUtil.isEmpty(workflowCallBackRequest.getApprovalResult())) {
            workflowCallBackRequest.setApprovalResult((String) inputMap2.get(Constants.NODETYPE_APPROVE));
        }

        workflowCallBackRequest.setDataMap((Map<String, Object>) inputMap2.get(BpmConstants.DATA_MAP_KEY));
        workflowCallBackRequest.setProcessId((String) inputMap2.get(Constants.PROCESSID_KEY));
        workflowCallBackRequest.setTaskId((String) inputMap2.get(Constants.TASK_ID_CODE));
        String reqJsonStr = JSONObject.toJSONString(workflowCallBackRequest);

        log.info("| - EventServiceInvockUtil>>>>>springcloud 请求的报文信息[{}]", reqJsonStr);
        workflowFeignClientService.callBack(wfEvtServiceDef.getCallService(), workflowCallBackRequest);
        return ObjectUtil.unPackMessage(reqJsonStr);
    }


    private String getNodeStateMap(Map<String, Object> param) {
        List<WfNodeState> list = wfNodeStateService.selectList(param);
        if (list == null || list.isEmpty()) {
            return null;
        }
        WfNodeState node = list.get(0);
        return node.getNodeState();
    }

    /**
     * 调用消息发送服务
     *
     * @param message
     * @param businessMap
     * @return
     */
    public boolean sendMessage(WfMessage message, Map<String, Object> businessMap) {
        if (log.isInfoEnabled()) {
            log.info("| - EventServiceInvockUtil>>>>>开始进行消息发送DUBBO服务调用[{}]", message.getTaskId());
        }
        String busiSeqNo = "WF" + message.getTaskId() + "," + DateTime.now();
        String userId = message.getUserId();

        /*MsgRecordSingleSendRequest request = new MsgRecordSingleSendRequest();
        request.setBusiSeqNo(busiSeqNo);
        request.setTemplateId(message.getSmsCotent());
        request.setMsgObjectType("1");
        request.setMsgObjectInfo(userId);

        Map<String, String> msgParam = businessMap.entrySet().stream().filter(
                info -> info.getValue() instanceof String
        ).collect(Collectors.toMap(Map.Entry::getKey, map -> (String) map.getValue()));
        request.setMsgParam(msgParam);

        if (log.isInfoEnabled())
        {
            log.info("| - EventServiceInvockUtil>>>>>调用消息发送服务入参[模板id-templateId:" + message.getSmsCotent() + "]");
            log.info("| - EventServiceInvockUtil>>>>>调用消息发送服务入参[发送人id-userId[userId]:" + userId + "]");
            log.info("| - EventServiceInvockUtil>>>>>调用消息发送服务入参[模板参数-ParamMap:" + JSONUtil.toJsonStr(msgParam) + "]");
            log.info("| - EventServiceInvockUtil>>>>>调用消息发送服务入参[业务主键-BusinessId:" + busiSeqNo + "]");
        }
        try{
            Result<Void> result = iMessageHttpService.sendMsgSingle(request);
            log.info("| - EventServiceInvokeUtil>>>>>消息服务返回结果[result:{}]", JSONUtil.toJsonStr(result));
            return result.isSuccess();
        }catch (Exception e) {
            log.error("| - EventServiceInvokeUtil>>>>>消息服务调用异常", e);
        }*/
        return false;
    }

    /**
     * 设置出参
     *
     * @param variableMap
     * @param outMap
     */
    private void setOutPutMap(Map<String, Object> variableMap, Map<String, Object> outMap) {
        if (Objects.nonNull(variableMap) && Objects.nonNull(outMap)) {
            // 如果没有重复的key
            variableMap.putAll(outMap);
        }

    }

    /**
     * 把json串的值赋值给Map
     *
     * @param jsonStr
     * @param jsonStr
     * @return
     */
    @SuppressWarnings("rawtypes")
    public Map<String, Object> getNodeStateMap(String jsonStr) {
        Map<String, Object> result = new HashMap<String, Object>();
        // 通过遍历variableMap，查找匹配的替换字符串，替换为variableMap中对应的值
        if (StrUtil.isNotBlank(jsonStr)) {

            Map inputBeanMaps = parseJSONstr2Map(jsonStr); // 先转换成Object,Object强转换为Map
            for (Object inputObj : inputBeanMaps.entrySet()) {
                Map.Entry inputEntry = (Map.Entry) inputObj;
                if (inputEntry.getValue().toString().contains(Constants.NODE_STATE_VALUE)) {
                    String nodeState = getNodeStateMap(inputMap);
                    result.put(inputEntry.getKey().toString(), nodeState);
                } else {
                    result.put(inputEntry.getKey().toString(), inputEntry.getValue());
                }
            }
        }
        return result;
    }

    /**
     * 把json串的值赋值给Map
     *
     * @param jsonStr
     * @param variableMap
     * @return
     */
    @SuppressWarnings("rawtypes")
    public Map<String, Object> setMapValue(String jsonStr, Map<String, Object> variableMap) {
        Map<String, Object> result = new HashMap<String, Object>();
        // 通过遍历variableMap，查找匹配的替换字符串，替换为variableMap中对应的值
        if (StrUtil.isNotBlank(jsonStr)) {
            if (variableMap != null && !variableMap.isEmpty()) {
                Map inputBeanMaps = parseJSONstr2Map(jsonStr); // 先转换成Object,Object强转换为Map
                for (Object inputObj : inputBeanMaps.entrySet()) {
                    Map.Entry inputEntry = (Map.Entry) inputObj;
                    for (Map.Entry varEntry : variableMap.entrySet()) {
                        if (varEntry.getKey().toString().equals((String) inputEntry.getKey()) && varEntry.getValue() != null) {
                            result.put(inputEntry.getKey().toString(), varEntry.getValue());
                        }
                    }

                    if (inputEntry.getValue().toString().contains("#")) {
                        result.put(inputEntry.getKey().toString(), this.getMapValue(inputEntry.getValue().toString(), variableMap));
                    }

                    if (inputEntry.getValue().toString().contains(Constants.NODE_STATE_VALUE)) {
                        String nodeState = getNodeStateMap(inputMap);
                        result.put(inputEntry.getKey().toString(), nodeState);
                    }
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private Object getMapValue(String value, Map<String, Object> variableMap) {
        Map<String, Object> result = variableMap;
        String[] keys = value.split("#");
        for (int i = 0; i < keys.length - 1; i++) {
            result = (Map<String, Object>) result.get(keys[i]);
            if (result == null) {
                throw new ServiceException("参数中没有对应的" + value);
            }
        }
        return result.get(keys[keys.length - 1]);
    }

    /**
     * http请求
     *
     * @param url
     * @param params
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private Map<String, Object> execHttpClient(String url, Map<String, Object> params) {
        WorkFlowServiceBean requestBean = new WorkFlowServiceBean();
        if (params == null) {
            return null;
        }
        if (log.isInfoEnabled()) {
            log.info("| - EventServiceInvockUtil>>>>>请求地址[{}]", url);
            log.info("| - EventServiceInvockUtil>>>>>请求的参数[{}]", params.toString());
        }
        requestBean.getTranBody().getMapReqData().putAll(params);
        String myBusMap = (String) params.get("myBusMap");
        //如果是流程启动触发（例外审批检查规则）将业务参数统一封装进busMap中
        if (BpmConstants.MY_BODY.equals(myBusMap)) {
            Map<String, Object> busMap = new HashMap<>();
            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(params)) {
                busMap.putAll(params);
            }
            requestBean.getTranBody().getMapReqData().put("busMap", busMap);
        }
        if (sysCommHead != null) {
            requestBean.setTranHead(sysCommHead);
        } else if (WfVariableUtil.getSysCommHead() != null) {
            requestBean.setTranHead(WfVariableUtil.getSysCommHead());
        }

        String reqJsonStr = JSONObject.toJSONString(requestBean);
        log.info("| - EventServiceInvockUtil>>>>>http请求的报文信息[{}]", reqJsonStr);

        String thirdPartyInterfaceCallFlag = configUtil.getQueryRalationTellerType();
        String resp = "";
        if (StrUtil.isNotBlank(thirdPartyInterfaceCallFlag) && BpmConstants.THIRD_PARTY_INTERFACE_CALL_FLAG_1.equals(thirdPartyInterfaceCallFlag)) {
            resp = httpTemplateCloudService.postForObject(url, reqJsonStr);
        } else {
            int intTimeOut = configUtil.getBackInvokeServiceTimeOut();
            resp = ApacheHttpClientUtil.sendHttpRequestForPost(url, reqJsonStr, intTimeOut);
        }

        if (log.isInfoEnabled()) {
            log.info("| - EventServiceInvockUtil>>>>>[{}]的响应结果：[{}]", url, resp.toString());
        }

        return ObjectUtil.unPackMessage(resp);
    }

    @Override
    public String toString() {
        return "EventServiceInvockUtil [inputMap=" + inputMap + ", outputMap=" + outputMap + ", variableMap="
                + variableMap + ", result=" + result + "]";
    }

    /**
     * 生成日志记录
     *
     * @param processId
     * @return
     */
    public WfEvtServiceLog newEvtServiceLog(String processId) {
        WfEvtServiceLog wfEvtServiceLog = new WfEvtServiceLog();

        wfEvtServiceLog.setProcExtId(processId);
        if (outputMap != null) {
            String outstr = BpmStringUtil.mapToJson(outputMap).toJSONString();
            wfEvtServiceLog.setOutputParam(outstr);
        }
        if (inputMap != null) {
            String instr = BpmStringUtil.mapToJson(inputMap).toJSONString();
            wfEvtServiceLog.setInputParam(instr);
        }
        if (result != null) {
            wfEvtServiceLog.setInvockResult(result.toString());
        }
        wfEvtServiceLog.setServiceId(confId);
        wfEvtServiceLog.setStatus(1);
        if (variableMap != null) {
            String taskId = (String) variableMap.get(Constants.NODE_TASKID);
            wfEvtServiceLog.setTaskId(taskId);
            Map<String, Object> map = variableMap;
            try {
                String param = BpmStringUtil.mapToJson(map).toJSONString();
                wfEvtServiceLog.setVarinstExt(param);
            } catch (Exception e) {
                log.error("类型转换异常：", e);
            }
        }
        wfEvtServiceLog.setProcDefId(processId.split("\\.")[0]);
        return wfEvtServiceLog;
    }

    /**
     * 将json对象转换为HashMap
     *
     * @param json
     * @return
     */
    public Map<String, Object> parseJSON2Map(JSONObject json) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 最外层解析
        for (Object k : json.keySet()) {
            Object v = json.get(k);
            // 如果内层还是json数组的话，继续解析
            if (v instanceof JSONArray) {
                List<Object> list = new ArrayList<Object>();
                Iterator<Object> it = ((JSONArray) v).iterator();
                while (it.hasNext()) {
                    Object obj = it.next();
                    if (JSONObject.class.isInstance(obj)) {
                        JSONObject json2 = (JSONObject) obj;
                        list.add(parseJSON2Map(json2));
                    } else {
                        list.add(obj);
                    }


                }
                map.put(k.toString(), list);
            } else if (v instanceof JSONObject) {
                // 如果内层是json对象的话，继续解析
                map.put(k.toString(), parseJSON2Map((JSONObject) v));
            } else {
                // 如果内层是普通对象的话，直接放入map中
                map.put(k.toString(), v);
            }
        }
        return map;
    }

    /**
     * 将json字符串转换为Map
     *
     * @param jsonStr
     * @return
     */
    public Map<String, Object> parseJSONstr2Map(String jsonStr) {
        JSONObject json = JSONObject.parseObject(jsonStr);
        Map<String, Object> map = parseJSON2Map(json);
        return map;

    }

}
