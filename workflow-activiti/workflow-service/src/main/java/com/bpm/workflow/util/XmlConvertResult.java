package com.bpm.workflow.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XmlConvertResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -811539720330543548L;
	
	private boolean canUseLatestProperties;
	private String xmlStr;
	private Map<String,Map<String,String>> properties = null;
	
	
	
	public boolean isCanUseLatestProperties() {
		return canUseLatestProperties;
	}
	public void setCanUseLatestProperties(boolean canUseLatestProperties) {
		this.canUseLatestProperties = canUseLatestProperties;
	}
	
	public String getXmlStr() {
		return xmlStr;
	}
	public void setXmlStr(String xmlStr) {
		this.xmlStr = xmlStr;
	}
	
	public void setPropertiesValue(String taskCode, String propName,String propValue) {
		if(properties == null) {
			properties = new HashMap<String,Map<String,String>>();
		}
		if(properties.get(taskCode)==null) {
			properties.put(taskCode, new HashMap<String,String>());
		}
		properties.get(taskCode).put(propName, propValue);
	}
	public String getPropertiesValue(String taskCode, String propName) {
		if(!hasProperties(taskCode)) {
			return null;
		}
		return properties.get(taskCode).get(propName);
	}
	public Set<Map.Entry<String, String>> getPropEntries(String taskCode){
		if(!hasProperties(taskCode)) {
			return null;
		}
		return properties.get(taskCode).entrySet();
	}
	public boolean hasProperties() {
		if(properties == null||properties.size()==0) {
			return false;
		}
		return true;
	}
	public Set<String> getTaskCodes(){
		if(!hasProperties()) {
			return null;
		}
		return properties.keySet();
	}
	public boolean hasProperties(String taskCode) {
		if(properties == null||properties.size()==0) {
			return false;
		}else {
			if(properties.get(taskCode)==null || properties.get(taskCode).size()==0) {
				return false;
			}
		}
		return true;
	}
}
