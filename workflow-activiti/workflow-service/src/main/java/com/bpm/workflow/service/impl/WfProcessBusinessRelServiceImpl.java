package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfProcessBusinessRel;
import com.bpm.workflow.mapper.WfProcessBusinessRelMapper;
import com.bpm.workflow.service.WfProcessBusinessRelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfProcessBusinessRelServiceImpl extends ServiceImpl<WfProcessBusinessRelMapper, WfProcessBusinessRel> implements WfProcessBusinessRelService {

}
