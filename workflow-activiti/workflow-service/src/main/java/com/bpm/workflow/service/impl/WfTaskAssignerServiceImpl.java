package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfTaskAssigner;
import com.bpm.workflow.mapper.WfTaskAssignerMapper;
import com.bpm.workflow.service.WfTaskAssignerService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfTaskAssignerServiceImpl extends ServiceImpl<WfTaskAssignerMapper, WfTaskAssigner> implements WfTaskAssignerService {

}
