package com.bpm.workflow.timertask;

import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.entity.WfEvtServiceDef;
import com.bpm.workflow.entity.WfEvtServiceLog;
import com.bpm.workflow.service.WfEvtServiceLogService;
import com.bpm.workflow.util.EventServiceInvockUtil;
import lombok.extern.log4j.Log4j2;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Log4j2
public class LatchInvokeEventTask implements Runnable {

	/**
	 * 取得一个倒计时器
	 */
	private CountDownLatch countDownLatch;

	private Map<String, Object> variableMap;
	private Map<String, Object> eventDefMap;
	private String taskId;
	private String processDefId;
	private String processInstId;
	private String nodeType;
	private String nodeName;
	private SysCommHead sysCommHead;
	private WfEvtServiceDef wfEvtServiceDef;
	private WfEvtServiceLogService wfEvtServiceLogService;
	public LatchInvokeEventTask( CountDownLatch countDownLatch,WfEvtServiceLogService wfEvtServiceLogService,WfEvtServiceDef wfEvtServiceDef, Map<String, Object> variableMap,Map<String, Object> eventDefMap, String taskId, String processDefId, String processInstId, String nodeType, String nodeName, SysCommHead sysCommHead) {
		super();
		this.countDownLatch = countDownLatch;
		this.variableMap=variableMap;
		this.eventDefMap=eventDefMap;
		this.taskId=taskId;
		this.processDefId=processDefId;
		this.processInstId=processInstId;
		this.nodeType=nodeType;
		this.nodeName=nodeName;
		this.sysCommHead=sysCommHead;
		this.wfEvtServiceDef=wfEvtServiceDef;
		this.wfEvtServiceLogService=wfEvtServiceLogService;
	}

 
	@Override
	public void run() {
		synchronized (this) {
			log.info("| - LatchInvokeEventTask>>>>>开始异步调用事件服务 {}",wfEvtServiceDef.getId());
			try {
				EventServiceInvockUtil eventUtil = new EventServiceInvockUtil().newEventServiceInstance(wfEvtServiceDef, variableMap, eventDefMap, taskId, processDefId, processInstId, nodeType, nodeName, sysCommHead);
				WfEvtServiceLog log=eventUtil.newEvtServiceLog(processInstId);
				log.setTaskId(taskId);
				log.setProcDefId(processDefId);
				wfEvtServiceLogService.insertWfEvtServiceLog(log);
			} catch (Exception e) {
				log.error("事件服务执行失败",e);
			}
			log.info("| - LatchInvokeEventTask>>>>>完成异步调用事件服务 {}",wfEvtServiceDef.getId());
			countDownLatch.countDown();

		}
	}
}
