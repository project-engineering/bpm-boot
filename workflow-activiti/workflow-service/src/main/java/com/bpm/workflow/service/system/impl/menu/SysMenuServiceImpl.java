package com.bpm.workflow.service.system.impl.menu;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.system.menu.SysMenu;
import com.bpm.workflow.mapper.system.menu.SysMenuMapper;
import com.bpm.workflow.service.system.menu.SysMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
