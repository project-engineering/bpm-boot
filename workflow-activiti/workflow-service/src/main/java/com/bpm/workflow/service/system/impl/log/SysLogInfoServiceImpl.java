package com.bpm.workflow.service.system.impl.log;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import com.bpm.workflow.mapper.system.log.SysLogInfoMapper;
import com.bpm.workflow.service.system.log.SysLogInfoService;
import com.bpm.workflow.util.UtilValidate;
import com.bpm.workflow.vo.system.log.LogParam;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysLogInfoServiceImpl extends ServiceImpl<SysLogInfoMapper, SysLogInfo> implements SysLogInfoService {
    @Resource
    private SysLogInfoMapper sysLogInfoMapper;
    @Override
    public IPage<SysLogInfo> getList(LogParam param) {
        //构造分页对象
        IPage<SysLogInfo> page = new Page<>(param.getStart(),param.getLimit());
        //构造查询条件
        QueryWrapper<SysLogInfo> queryWrapper = new QueryWrapper<>();
        if (UtilValidate.isNotEmpty(param.getUserId())){
            queryWrapper.lambda().eq(SysLogInfo::getUserId,param.getUserId());
        }
        if (UtilValidate.isNotEmpty(param.getUserName())){
            queryWrapper.lambda().like(SysLogInfo::getUserName,param.getUserName());
        }
        if (UtilValidate.isNotEmpty(param.getMethod())){
            queryWrapper.lambda().like(SysLogInfo::getMethod,param.getMethod());
        }
        queryWrapper.lambda().orderByDesc(SysLogInfo::getCreateTime);
        //执行查询
        IPage<SysLogInfo> list = sysLogInfoMapper.selectPage(page,queryWrapper);
        return list;
    }
}
