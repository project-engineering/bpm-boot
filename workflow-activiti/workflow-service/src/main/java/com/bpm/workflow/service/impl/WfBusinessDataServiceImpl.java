package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfBusinessData;
import com.bpm.workflow.mapper.WfBusinessDataMapper;
import com.bpm.workflow.service.WfBusinessDataService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfBusinessDataServiceImpl extends ServiceImpl<WfBusinessDataMapper, WfBusinessData> implements WfBusinessDataService {
    @Resource
    private WfBusinessDataMapper wfBusinessDataMapper;

    @Override
    public void insertWfBusinessData(WfBusinessData businessData) {
        wfBusinessDataMapper.insert(businessData);
    }

    @Override
    public WfBusinessData selectById(String id) {
        return wfBusinessDataMapper.selectById(id);
    }
}
