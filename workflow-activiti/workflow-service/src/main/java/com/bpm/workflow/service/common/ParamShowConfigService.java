package com.bpm.workflow.service.common;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.ParamShowConfig;
import com.bpm.workflow.mapper.ParamShowConfigMapper;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.vo.ParamShowConfigQueryResponse;
import com.bpm.workflow.vo.ShowConfigListField;
import com.bpm.workflow.vo.ShowConfigQueryCondition;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * 参数展示配置服务
 */
@Service
@Log4j2
public class ParamShowConfigService extends ServiceImpl<ParamShowConfigMapper, ParamShowConfig> {

    /**
     * 不展示机构字段
     */
    private static final String NO_SHOW_ORG_FIELD = "0";

    /**
     * 查询展示配置根据事项小类
     *
     * @param systemIdCode
     * @return
     */
    public ParamShowConfigQueryResponse queryShowConfigByParam(String systemIdCode) {
        log.info("queryShowConfigByParam input is {}", systemIdCode);
        // 根据事项小类查询展示配置
        List<ParamShowConfig> showConfigList = lambdaQuery()
                .eq(StrUtil.isNotBlank(systemIdCode), ParamShowConfig::getSystemIdCode, systemIdCode)
                .list();

        log.info("queryShowConfigByParam showConfigList is {}", JSONUtil.toJsonStr(showConfigList));

        ParamShowConfigQueryResponse paramShowConfigQueryResponse = new ParamShowConfigQueryResponse();
        // 处理返回结果
        for (ParamShowConfig paramShowConfig : showConfigList) {
            // 查询条件配置处理
            if(BpmConstants.SHOW_CONFIG_1.equals(paramShowConfig.getType())){
                JSONArray jsonArray = JSONUtil.parseArray(paramShowConfig.getContent());
                List<ShowConfigQueryCondition> showConfigQueryConditionList = new ArrayList<>();
                for (Object json : jsonArray) {
                    ShowConfigQueryCondition showConfigQueryCondition = JSONUtil.toBean(json.toString(), ShowConfigQueryCondition.class);
                    showConfigQueryConditionList.add(showConfigQueryCondition);
                }
                paramShowConfigQueryResponse.setShowConfigQueryConditionList(showConfigQueryConditionList);
            // 展示字段配置处理
            }else if(BpmConstants.SHOW_CONFIG_2.equals(paramShowConfig.getType())) {
                JSONArray jsonArray = JSONUtil.parseArray(paramShowConfig.getContent());
                List<ShowConfigListField> showConfigListFieldList = new ArrayList<>();
                for (Object json : jsonArray) {
                    ShowConfigListField showConfigListField = JSONUtil.toBean(json.toString(), ShowConfigListField.class);
                    showConfigListFieldList.add(showConfigListField);
                }
                paramShowConfigQueryResponse.setShowConfigListFieldList(showConfigListFieldList);
            // 是否展示机构字段配置
            }else if(BpmConstants.SHOW_CONFIG_3.equals(paramShowConfig.getType())) {
                if(StrUtil.isNotEmpty(paramShowConfig.getType())){
                    paramShowConfigQueryResponse.setIsShowOrgField(paramShowConfig.getContent());
                }else {
                    paramShowConfigQueryResponse.setIsShowOrgField(NO_SHOW_ORG_FIELD);
                }
            }
        }
        log.debug("queryShowConfigByParam paramShowConfigQueryResponse is {}", JSONUtil.toJsonStr(paramShowConfigQueryResponse));
        return paramShowConfigQueryResponse;
    }
}
