package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfEvtServiceLog;
import com.bpm.workflow.mapper.WfEvtServiceLogMapper;
import com.bpm.workflow.service.WfEvtServiceLogService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfEvtServiceLogServiceImpl extends ServiceImpl<WfEvtServiceLogMapper, WfEvtServiceLog> implements WfEvtServiceLogService {
    @Resource
    private WfEvtServiceLogMapper wfEvtServiceLogMapper;
    @Override
    public void insertWfEvtServiceLog(WfEvtServiceLog wfEvtServiceLog) {
        wfEvtServiceLogMapper.insert(wfEvtServiceLog);
    }
}
