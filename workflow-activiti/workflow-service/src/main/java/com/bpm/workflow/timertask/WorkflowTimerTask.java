package com.bpm.workflow.timertask;

import lombok.extern.log4j.Log4j2;
import java.io.Serializable;

@Log4j2
public abstract class WorkflowTimerTask implements Runnable, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private long lastExecTime = -1;

    public long getLastExecTime() {
        return lastExecTime;
    }

    public void setLastExecTime(long lastExecTime) {
        this.lastExecTime = lastExecTime;
    }

    @Override
    public void run() {
        try {
            runImpl();
        } catch (Exception e) {
            log.error("| - WorkflowTimerTask>>>>>failed in exec WorkflowTimerTask", e);
        }
    }

    public abstract void runImpl() throws Exception;
}
