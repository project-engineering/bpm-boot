package com.bpm.workflow.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.QueryRelationTellerFactory;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.*;
import com.bpm.workflow.engine.command.JumpCommand;
import com.bpm.workflow.engine.listener.ExecutionEndListener;
import com.bpm.workflow.engine.listener.ExecutionStartListener;
import com.bpm.workflow.entity.*;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.*;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.service.common.WfDefinePropExtendsService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.timertask.TimerUtil;
import com.bpm.workflow.timertask.WfMsgNotifyTimerTask;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.*;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ProcessUtil {
    @Resource
    private ProcessCoreService processCoreService;
    @Resource
    private ProcessEngine processEngine;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private ActivitiBaseService activitiBaseServer;
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private TimerUtil timerUtil;
    @Resource
    private WfParamService wfParamService;
    @Resource
    private ExecutionStartListener executionStartListener;
    @Resource
    private ExecutionEndListener executionEndListener;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfRuIdentitylinkService wfRuIdentitylinkService;
    @Resource
    private WfDefinePropExtendsService wfDefinePropExtendsService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    @Resource
    private WfInstanceService wfInstanceService;
    @Resource
    private WfMessageService messageService;
    @Resource
    private UruleServiceFactory urule;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private QueryRelationTellerFactory queryRelationTellerFactory;
    @Resource
    private WfBusinessDataService wfBusinessDataService;

    /**
     * 清理任务的处理人员
     *
     * @param taskId
     * @param delUserId
     */
    public void deleteCandidateUser(String taskId, String delUserId) {

        WfRuIdentitylink para = new WfRuIdentitylink();
        para.setTaskId(taskId);
        para.setUserId(delUserId);
        try {
            wfRuIdentitylinkService.deleteByUserIdTaskId(para);
        } catch (Exception e) {
            log.info("| - ProcessUtil>>>>>无对应任务处理用户:taskId:{},userId:{}", taskId, delUserId, e);
        }
    }

    /**
     * 清理任务的处理人员
     *
     * @param taskId
     */
    public void deleteCandidateUser(String taskId) {

        WfRuIdentitylink para = new WfRuIdentitylink();
        para.setTaskId(taskId);
        try {
            wfRuIdentitylinkService.deleteByTaskId(para);
        } catch (Exception e) {
            log.info("| - ProcessUtil>>>>>无对应任务处理用户:taskId:{}", taskId, e);
        }
    }

    public void changeCandidateUser(String taskId, String oriUserId, String newUserId, int flag) {
        // 这里对超时回收再分配进行过滤，因为再分配会存在分配给多个人
        WfRuIdentitylink para = new WfRuIdentitylink();
        if (flag == Constants.TRANSFER_TYPE_RECOVER || flag == Constants.TRANSFER_TYPE_ALLOT) {
            para.setTaskId(taskId);
            wfRuIdentitylinkService.deleteByTaskId(para);
            if (newUserId.contains(SymbolConstants.COMMA)) {
                for (String userId : newUserId.split(SymbolConstants.COMMA)) {
                    WfRuIdentitylink wfRuIdentitylink = new WfRuIdentitylink();
                    wfRuIdentitylink.setTaskId(taskId);
                    wfRuIdentitylink.setUserId(userId);
                    String tempTaskId = taskId;
                    if (tempTaskId.length() > NumberConstants.INT_THIRTY) {
                        tempTaskId = tempTaskId.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                    }
                    String tempUser = userId;
                    if (tempUser.length() > NumberConstants.INT_THIRTY) {
                        tempUser = tempUser.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                    }
                    wfRuIdentitylink.setId(tempTaskId + tempUser);
                    wfRuIdentitylinkService.insertWfRuIdentitylink(wfRuIdentitylink);
                }
            } else {
                WfRuIdentitylink wfRuIdentitylink = new WfRuIdentitylink();
                wfRuIdentitylink.setTaskId(taskId);
                wfRuIdentitylink.setUserId(newUserId);
                String tempTaskId = taskId;
                if (tempTaskId.length() > NumberConstants.INT_THIRTY) {
                    tempTaskId = tempTaskId.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                }
                String tempUser = newUserId;
                if (tempUser.length() > NumberConstants.INT_THIRTY) {
                    tempUser = tempUser.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                }
                wfRuIdentitylink.setId(tempTaskId + tempUser);
                wfRuIdentitylinkService.insertWfRuIdentitylink(wfRuIdentitylink);
            }
            //2019-04-25 amt modify end 这里对超时回收再分配进行过滤，因为再分配会存在分配给多个人

        } else {
            WfRuIdentitylink wfRuIdentitylink = null;
            if (!StrUtil.isEmpty(oriUserId)) {
                para.setTaskId(taskId);
                para.setUserId(oriUserId);
                wfRuIdentitylink = wfRuIdentitylinkService.selectByUserIdTaskId(para);
                if (wfRuIdentitylink != null) {
                    wfRuIdentitylinkService.deleteByUserIdTaskId(wfRuIdentitylink);
                }
            }

            if (!StrUtil.isEmpty(newUserId)) {
                para.setTaskId(taskId);
                para.setUserId(newUserId);
                wfRuIdentitylink = wfRuIdentitylinkService.selectByUserIdTaskId(para);
                if (wfRuIdentitylink == null) {
                    wfRuIdentitylink = new WfRuIdentitylink();
                    wfRuIdentitylink.setTaskId(taskId);
                    wfRuIdentitylink.setUserId(newUserId);
                    String tempTaskId = taskId;
                    if (tempTaskId.length() > NumberConstants.INT_THIRTY) {
                        tempTaskId = tempTaskId.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                    }
                    String tempUser = newUserId;
                    if (tempUser.length() > NumberConstants.INT_THIRTY) {
                        tempUser = tempUser.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
                    }
                    wfRuIdentitylink.setId(tempTaskId + tempUser);
                    wfRuIdentitylinkService.insertWfRuIdentitylink(wfRuIdentitylink);
                }
            }
        }
    }

    public void batchAddCandidateUser(String taskId, String assigners) {
        String[] arrAssigners = getAssigners(assigners);
        if (arrAssigners.length == NumberConstants.INT_ZERO) {
            return;
        }
        List<WfRuIdentitylink> wfRuIdentitylinks = new ArrayList<>();
        Set<String> theUsers = new HashSet<>();
        for (String user : arrAssigners) {
            if (StrUtil.isEmpty(user)) {
                continue;
            }
            theUsers.add(user);
        }
        if (theUsers.size() == 0) {
            return;
        }
        for (String user : theUsers) {
            if (StrUtil.isEmpty(user)) {
                continue;
            }
            WfRuIdentitylink wfRuIdentitylink = new WfRuIdentitylink();
            String tempTaskId = taskId;
            if (tempTaskId.length() > NumberConstants.INT_THIRTY) {
                tempTaskId = tempTaskId.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
            }
            String tempUser = user;
            if (tempUser.length() > NumberConstants.INT_THIRTY) {
                tempUser = tempUser.substring(NumberConstants.INT_ZERO, NumberConstants.INT_THIRTY);
            }
            wfRuIdentitylink.setId(tempTaskId + tempUser);
            wfRuIdentitylink.setTaskId(taskId);
            wfRuIdentitylink.setUserId(user);
            wfRuIdentitylinks.add(wfRuIdentitylink);
        }
        if (wfRuIdentitylinks.size() > NumberConstants.INT_ZERO) {
            wfRuIdentitylinkService.saveBatch(wfRuIdentitylinks);
        }
    }

    protected static String[] getAssigners(String assigners) {
        if (assigners == null) {
            return new String[]{};
        }
        if (assigners.contains(SymbolConstants.COMMA)) {
            return assigners.split("\\,");
        } else {
            return new String[]{assigners};
        }
    }

    @SuppressWarnings("rawtypes")
    private FlowElement getFirstFlowelemntByType(BpmnModel bpmnModel, Class expectElementClass, ProcessInstance processInstance) {
        Collection<FlowElement> flowElements = bpmnModel.getProcessById(processInstance.getProcessDefinitionKey()).getFlowElements();
        for (FlowElement flowElement : flowElements) {
            if (expectElementClass.isInstance(flowElement)) {
                return flowElement;
            }
        }
        return null;
    }

    public boolean doAfterEndProcess(String processDefinitionId, String processInstanceId, FlowElement endElement) {
        // 调用结束
        if (endElement instanceof EndEvent) {
            if (log.isDebugEnabled()) {

                log.debug("| - ProcessUtil>>>>>结束流程");
                log.debug("| - ProcessUtil>>>>>执行executionEndListener.common({},{},{})", processDefinitionId, processInstanceId, endElement.getId());

            }
            executionEndListener.common(processDefinitionId, processInstanceId, endElement);
            return true;
        }
        // 以下是正常结束
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstanceId).list();
        if (tasks != null && tasks.size() != NumberConstants.INT_ZERO) {
            return false;
        }
        String eventName = WfVariableUtil.getProcessEndEventName();
        if (StrUtil.isEmpty(eventName)) {
            return false;
        }

        List<WfTaskflow> list = wfTaskflowService.selectByProcessInstId(processInstanceId);
        String beforeTaskId = (String) WfVariableUtil.getVariableMap().get(Constants.BEFOR_TASKID);
        list.sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
        FlowElement currentFlowElement = null;
        WfTaskflow last = null;
        for (WfTaskflow wfTask : list) {
            if (wfTask.getTaskId().equals(beforeTaskId)) {
                last = wfTask;
            }
        }
        if (last == null) {
            last = list.get(NumberConstants.INT_ZERO);
        }

        if (log.isDebugEnabled()) {
            log.debug("| - ProcessUtil>>>>>当前任务信息[taskId:" + last.getTaskId() + ",defId:" + last.getTaskDefId() + "taskName:" + last.getTaskName() + "]");
            log.debug("| - ProcessUtil>>>>>实例结束事件类型：[" + eventName + "]");
        }
        BpmnModel bpmnModel;
        try {
            bpmnModel = activitiBaseServer.getBpmnModelNode(processDefinitionId);
            Collection<FlowElement> nodeList1 = bpmnModel.getMainProcess().getFlowElements();
            //获取当前的元素信息
            for (FlowElement element : nodeList1) {
                if (element.getId().equals(last.getTaskDefId())) {
                    currentFlowElement = element;
                    break;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            throw new ServiceException("根据流程标识获取流程定义信息报错！");
        }

        FlowElement end = null;
        try {
            //便利当前元素的出去的线，确定是哪个结束节点
            List<SequenceFlow> seqflow = ((FlowNode) currentFlowElement).getOutgoingFlows();
            if (seqflow == null || seqflow.size() == NumberConstants.INT_ZERO) {
                end = currentFlowElement;
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("| - ProcessUtil>>>>{}节点出去的线的个数：[{}]", last.getTaskName(), seqflow.size());
                }
                for (SequenceFlow s : seqflow) {
                    FlowElement element = s.getTargetFlowElement();
                    if (element instanceof EndEvent) {
                        EndEvent endevt = (EndEvent) element;
                        List<ActivitiListener> lis = endevt.getExecutionListeners();
                        if (lis != null && lis.size() > NumberConstants.INT_ZERO && (StrUtil.isEmpty(eventName)
                                || eventName.equals(ModelProperties.flowEndErrorEvent.getName()))) {
                            end = element;
                            break;
                        } else {
                            if ((StrUtil.isEmpty(eventName)
                                    || !eventName.equals(ModelProperties.flowEndErrorEvent.getName()))
                                    && (lis == null || lis.size() == NumberConstants.INT_ZERO)) {
                                end = element;
                                break;
                            }
                        }
                    } else if (element instanceof InclusiveGateway) {
                        FlowElement theElement = getEndAfterGateWay(element, eventName);
                        if (theElement != null) {
                            end = theElement;
                            break;
                        }
                    }
                }
            }
            if (end != null) {
                log.debug("| - ProcessUtil>>>>计算出的截至节点id：[{}]", end.getId());
            } else {
                return false;
            }
        } catch (Exception e) {
            log.error("| - EndListener>>>>>不是正常结束也不是异常结束", e);
            throw new ServiceException("没有匹配到对应的结束节点");
        }
        executionEndListener.common(processDefinitionId, processInstanceId, end);
        return true;
    }

    private FlowElement getEndAfterGateWay(FlowElement gatewayElement, String eventName) {
        if (gatewayElement == null) {
            return null;
        }
        List<SequenceFlow> seqFlow = ((FlowNode) gatewayElement).getOutgoingFlows();
        if (seqFlow == null || seqFlow.size() != 1) {
            return null;
        }

        FlowElement element = seqFlow.get(0).getTargetFlowElement();
        if (element instanceof EndEvent) {

            EndEvent endevt = (EndEvent) element;
            List<ActivitiListener> lis = endevt.getExecutionListeners();

            if (lis != null && lis.size() > NumberConstants.INT_ZERO && (StrUtil.isEmpty(eventName)
                    || eventName.equals(ModelProperties.flowEndErrorEvent.getName()))) {
                return element;
            } else {
                if ((StrUtil.isEmpty(eventName)
                        || !eventName.equals(ModelProperties.flowEndErrorEvent.getName()))
                        && (lis == null || lis.size() == NumberConstants.INT_ZERO)) {
                    return element;
                }
            }
        }
        return null;
    }


    public FlowElement getTaskFlowElement(ProcessInstance processInstance, Task task) {
        BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel(processInstance.getProcessDefinitionId());
        return bpmnModel.getFlowElement(task.getTaskDefinitionKey());
    }

    public Map<Task, FlowElement> getNewTaskAfterStart(ProcessInstance processInstance, List<Task> tasks) {
        BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel(processInstance.getProcessDefinitionId());

        FlowElement startEvent = getFirstFlowelemntByType(bpmnModel, StartEvent.class, processInstance);

        Map<Task, FlowElement> result = getNewTaskInfo(processInstance, startEvent.getId(), true, tasks, bpmnModel);
        return result;
    }

    public Map<Task, FlowElement> getNewTaskInfo(ProcessInstance processInstance, String beforeTaskDefKey, boolean isNext, List<Task> tasks, BpmnModel bpmnModel) {
        Map<Task, FlowElement> result = new HashMap<Task, FlowElement>();
        for (Task theTask : tasks) {
            result.put(theTask, bpmnModel.getFlowElement(theTask.getTaskDefinitionKey()));
        }
        return result;
    }

    public List<Task> getTaskListByMap(Map<Task, FlowElement> theMap) {
        if (theMap == null || theMap.size() == NumberConstants.INT_ZERO) {
            return null;
        }
        List<Task> theTasks = new ArrayList<Task>();
        for (Task theTask : theMap.keySet()) {
            theTasks.add(theTask);
        }
        return theTasks;
    }

    public boolean isSikpFirstNode(String processCode) {
        WfModelPropExtends wfModelPropExtends = wfModelPropExtendsService.selectByOwnIdPropName(XmlUtil.getProcessIdOnly(processCode), ModelProperties.skipFistNode.getName());
        return isSikpFirstNode(processCode, wfModelPropExtends);
    }

    public boolean isFirstNode(String processCode, String taskDefKey) {
        if (processCode == null || taskDefKey == null) {
            return false;
        }
        BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel(processCode);
        return isFirstNode(bpmnModel, taskDefKey);
    }

    public boolean isFirstNode(BpmnModel bpmnModel, String taskDefKey) {
        if (bpmnModel == null || taskDefKey == null) {
            return false;
        }
        if (bpmnModel != null && bpmnModel.getMainProcess() != null
                && bpmnModel.getMainProcess().getFlowElements() != null) {
            for (FlowElement flowElement : bpmnModel.getMainProcess().getFlowElements()) {
                if (SequenceFlow.class.isInstance(flowElement)) {
                    SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                    if (sequenceFlow.getSourceFlowElement() != null && sequenceFlow.getTargetFlowElement() != null
                            && StartEvent.class.isInstance(sequenceFlow.getSourceFlowElement())
                            && taskDefKey.equals(sequenceFlow.getTargetFlowElement().getId())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isSikpFirstNode(String processCode, WfModelPropExtends wfModelPropExtends) {
        if (wfModelPropExtends == null) {
            log.debug("| - EndListener>>>>>can't find extends property for {} ", processCode);
            return false;
        }
        try {
            return Boolean.parseBoolean(wfModelPropExtends.getPropValue());
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e));
        }
        return false;
    }

    private static final String BUSINESS_MAP_SPECIFY_NEXT_OPERS_PRESERVE = "specifyNextOpersPreserve";

    public void dealWithSkipFirstNodePre(Map<String, Object> varMap, String userId, String processCode)
            throws Exception {
        if (!isSikpFirstNode(processCode)) {
            return;
        }

        if (varMap != null) {
            if (varMap.get(Constants.SPECIFYEXTOPERS) == null) {
                varMap.put(Constants.SPECIFYEXTOPERS, userId);
            } else {
                varMap.put(BUSINESS_MAP_SPECIFY_NEXT_OPERS_PRESERVE, varMap.get(Constants.SPECIFYEXTOPERS));
            }
        }
    }

    @Transactional
    public SubmitReturnData dealWithSkipFirstNode(Task task, boolean resetFlag, Map<String, Object> varMap,
                                                  String userId, ProcessInstance processInstance, SysCommHead sysCommHead, WorkFlowStartData workFlowStartData) {

        // 判断首节点是否跳过
        String isSkipStr = varMap.get(ModelProperties.skipFistNode.getName()).toString();
        if (!Boolean.parseBoolean(isSkipStr)) {
            return null;
        }
        //当首节点跳过，首节点为自动节点时，需要往wf_taskflow表里插入一条数据
        FlowElement flowElement = this.getTaskFlowElement(processInstance, task);
        String nodeType = getTaskProp(task.getProcessDefinitionId(), flowElement, Constants.NODETYPE);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
        if (nodeType.equals(Constants.NODE_TYPE_ATTO)) {
            {
                WfTaskflow taskflow = new WfTaskflow();
                taskflow.setTaskId(task.getId());
                taskflow.setTaskActors(configUtil.getAutoTaskActor());
                taskflow.setProcessId(processInstance.getId());
                taskflow.setProcessCode(processInstance.getProcessDefinitionId());
                taskflow.setTaskDefId(flowElement.getId());
                taskflow.setTaskType(Constants.NODE_TYPE_ATTO);
                taskflow.setTaskName(flowElement.getName());
                // 设置任务级别 0-一般任务 1-首节点任务 2-关键任务
                taskflow.setTaskRate(1);
                taskflow.setWorkState(Constants.WORK_STATE_RUNNING);

                // 待保存的关键业务参
                if (varMap.containsKey(Constants.BUSINESS_SAVE)) {
                    Map<String, Object> queryCondition = BpmStringUtil.jsonToMap(varMap.get(Constants.BUSINESS_SAVE));
                    if (queryCondition.containsKey(Constants.QUERY_INDEX_1)) {
                        taskflow.setQueryIndexa((String) queryCondition.get(Constants.QUERY_INDEX_1));
                    }
                    if (queryCondition.containsKey(Constants.QUERY_INDEX_2)) {
                        taskflow.setQueryIndexb((String) queryCondition.get(Constants.QUERY_INDEX_2));
                    }
                    if (queryCondition.containsKey(Constants.QUERY_INDEX_3)) {
                        taskflow.setQueryIndexc((String) queryCondition.get(Constants.QUERY_INDEX_3));
                    }
                    if (queryCondition.containsKey(Constants.QUERY_INDEX_4)) {
                        taskflow.setQueryIndexd((String) queryCondition.get(Constants.QUERY_INDEX_4));
                    }
                }

                Object systemcodeobj = varMap.get(Constants.SYSTEM_CODE_KEY);
                if (systemcodeobj != null) {
                    taskflow.setSystemCode(systemcodeobj.toString());
                }
                Object userIdObj = varMap.get(Constants.BEFOR_USERID);
                if (userIdObj != null) {
                    taskflow.setWorkCreater(userIdObj.toString());
                }
                taskflow.setBussinessMap(BpmStringUtil.mapToJson(varMap).toString());
                wfTaskflowService.insertWfTaskflow(taskflow);

                WfBusinessData businessData = new WfBusinessData(taskflow);
                wfBusinessDataService.insertWfBusinessData(businessData);
            }
        }
        if (log.isInfoEnabled()) {
            log.info("| - ProcessUtil>>>>>首节点跳过[ProcessDefinitionId:{},processInstanceId:{},taskId:{}]", processInstance.getProcessDefinitionId(), processInstance.getId(), task.getId());
        }

        WorkFlowTaskTransferData data = new WorkFlowTaskTransferData();
        data.setUserId(userId);
        data.setTaskId(task.getId());
        varMap.put(ModelProperties.skipFistNode.getName(), false);
        data.setBussinessMap(varMap);
        data.setResult("J");
        if (ObjectUtil.isNotEmpty(workFlowStartData.getDoRemark())) {
            data.setResultRemark(workFlowStartData.getDoRemark());
        } else {
            data.setResultRemark("提交审批");
        }
        data.setSysCommHead(sysCommHead);
        SubmitReturnData result = processCoreService.nextTask(data);
        return result;
    }

    public Map<String, Object> doExecutionStartListenerNotify(ProcessInstance processInstance, Task task, SysCommHead syscommonHead) {
        String processInstId = processInstance.getId();

        Map<String, Object> varMap = WfVariableUtil.getVariableMap();
        log.debug("| - ProcessUtil>>>>>doExecutionStartListenerNotify:taskId:{},processInstId:{}", task.getId(), processInstId);
        executionStartListener.doNotify(processInstance.getProcessDefinitionId(), varMap, processInstId,
                task.getId(), null, syscommonHead);

        return varMap;
    }

    public WorkFlowTaskTransferData constructEndProcessParaByTaskId(String taskId, String resultRemark) {
        WorkFlowTaskTransferData taskTransferData = new WorkFlowTaskTransferData();
        taskTransferData.setTaskId(taskId);
        taskTransferData.setResultRemark(resultRemark);
        return taskTransferData;
    }

    public WorkFlowTaskTransferData constructEndProcessParaByProcessId(String processInstId, String resultRemark) {
        WorkFlowTaskTransferData taskTransferData = new WorkFlowTaskTransferData();
        taskTransferData.setProcessInstId(processInstId);
        taskTransferData.setResultRemark(resultRemark);
        taskTransferData.setCancelType(Constants.COMPLETE_TYPE_TIMEEND);
        taskTransferData.setUserId(configUtil.getAutoTaskActor());
        return taskTransferData;
    }

    public void closeProcess(String userId, String processId, String taskId, String doRemark,
                             Map<String, Object> varMap) {
        log.debug("| - ProcessUtil>>>>>about to closeProcess  {}", processId);

        log.debug("| - ProcessUtil>>>>>closeProcess :{} ", processId);
    }

    public String compareBusiMap(String processDefId, String bussinessMap, Map<String, Object> exectMap) {
        List<WfParam> params = wfParamService.selectParamByParamTypeParamCode(Constants.PARAM_BUSINESS, XmlUtil.getProcessIdOnly(processDefId));
        if (params == null || params.size() == NumberConstants.INT_ZERO || StrUtil.isEmpty(bussinessMap)) {
            return null;
        }
        JSONObject oriMap = JSONObject.parseObject(bussinessMap);

        StringBuilder sbNew = new StringBuilder();
        StringBuilder sbDelete = new StringBuilder();
        StringBuilder sbChg = new StringBuilder();
        boolean isFirstNew = true;
        boolean isFirstDelete = true;
        boolean isFirstChg = true;
        for (WfParam wfParam : params) {
            Object ori = BpmStringUtil.getByPath(oriMap, wfParam.getParamObjCode());
            Object theNew = BpmStringUtil.getByPath(exectMap, wfParam.getParamObjCode());
            if (ori == null && theNew != null) {
                if (isFirstNew) {
                    sbNew.append(wfParam.getParamObjCode()).append(" ").append(theNew.toString());
                    isFirstNew = false;
                } else {
                    sbNew.append(SymbolConstants.COMMA).append(wfParam.getParamObjCode()).append(" ").append(theNew.toString());
                }
            } else if (ori != null && theNew == null) {
                if (isFirstDelete) {
                    sbDelete.append(wfParam.getParamObjCode()).append(" ").append(ori.toString());
                    isFirstDelete = false;
                } else {
                    sbDelete.append(SymbolConstants.COMMA).append(wfParam.getParamObjCode()).append(" ").append(ori.toString());
                }
            } else if (ori != null && !ori.equals(theNew)) {
                if (isFirstChg) {
                    sbChg.append(wfParam.getParamObjCode()).append(" 由 ").append(ori.toString());
                    sbChg.append(" 改为").append(theNew.toString());
                    isFirstChg = false;
                } else {
                    sbChg.append(SymbolConstants.COMMA).append(wfParam.getParamObjCode()).append(" 由 ").append(ori.toString());
                    sbChg.append(" 改为").append(theNew.toString());
                }
            }

        }
        StringBuilder sbResult = new StringBuilder();
        if (!StrUtil.isEmpty(sbNew)) {
            sbResult.append(" 新增：").append(sbNew);
        }
        if (!StrUtil.isEmpty(sbDelete)) {
            sbResult.append(" 去除：").append(sbDelete);
        }
        if (!StrUtil.isEmpty(sbChg)) {
            sbResult.append(" 修改：").append(sbChg);
        }
        return sbResult.toString();
    }

    private long priGetOverTimeMillisecondByRule(String taskId, String ruleValue) {
        if (StrUtil.isEmpty(ruleValue)) {
            return TaskTimoutUtil.NO_OVERTIME;
        }
        JSONObject ruleJsonObj = null;
        try {
            Map<String, Object> ruleConfigMap = JSONObject.parseObject(ruleValue);
            ruleJsonObj = JSONObject.parseObject(JSONObject.toJSONString(ruleConfigMap));
        } catch (Exception e) {
            log.error("| - ProcessUtil>>>>>解析超时规则失败", e);
            return TaskTimoutUtil.NO_OVERTIME;
        }
        WfTaskflow wfTaskflow = processCoreService.selectByTaskId(taskId);
        Map<String, Object> variaMap = null;
        if (StrUtil.isEmpty(wfTaskflow.getBussinessMap())) {
            variaMap = new HashMap<String, Object>();
        } else {
            variaMap = JSONObject.parseObject(wfTaskflow.getBussinessMap());
        }
        try {
            String resultStr = urule.getInstance().execUruleReusltList(variaMap, ruleJsonObj);
            //这是一个jsonArray,
            JSONArray array = JSONArray.parseArray(resultStr);
            JSONObject json = array.getJSONObject(0);
            String type = json.getString(Constants.TIMEOUT_DATA_TYPE);
            String intval = json.getString(Constants.TIMEOUT_DATA_INTERVAL);
            if (StrUtil.equals(type, Constants.TIMEOUT_DATA_TYPE_MILL_P) && intval != null) {
                return DateUtil.convertObjToTimestamp(intval).getTime() - DateUtil.convertObjToTimestamp(wfTaskflow.getStartCreateDate()).getTime();
            }
            if (StrUtil.equals(type, Constants.TIMEOUT_DATA_TYPE_MILL_T) && intval != null) {
                return DateUtil.convertObjToTimestamp(intval).getTime() - DateUtil.convertObjToTimestamp(wfTaskflow.getCreateDate()).getTime();
            }
            if (Objects.isNull(intval)) {
                intval = "0";
            }
            if (StrUtil.equals(type, Constants.TIMEOUT_DATA_TYPE_DAY)) {
                intval = Integer.parseInt(intval) * 24 + "";
            }
            log.info("| - ProcessUtil>>>>>get overtime:{} hours", intval);
            return TaskTimoutUtil.getOvertimeMilliSecond(intval);
        } catch (Exception e) {
            log.error("| - ProcessUtil>>>>>解析超时规则失败", e);
            return TaskTimoutUtil.NO_OVERTIME;
        }

    }

    /**
     * 处理流程实例超时
     *
     * @param processInstance
     * @param theFirstTask
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doTimeoutCleanSpecProcessInstRtnEnd(ProcessInstance processInstance, Task theFirstTask) {
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstance.getId());

        if (wfInstance == null) {
            return;
        }

        if (BpmConstants.TIMEOUT_FLAG_1.equals(wfInstance.getIsTimeout())) {
            return;
        }

        wfInstance.setEstEndDate(LocalDateTime.now());

        // 获取流程超时及超时预警配置信息
        ProcessTimoutInfo processTimoutInfo = ProcessTimoutInfo.getProcessTimeoutInfo(wfModelPropExtendsService, processInstance.getProcessDefinitionId());

        WfTaskflow wfTaskflow;

        // 流程超时预警处理
        if (processTimoutInfo.getOvertimePreWarnTime() != 0 && StrUtil.isNotEmpty(processTimoutInfo.getOvertimePreWarnTimeUnit())
                && StrUtil.isNotEmpty(processTimoutInfo.getOvertimePreWarnTemplate())) {
            // 超时预警信息不为空时，不做超时预警处理
            if (StrUtil.isNotEmpty(wfInstance.getTimeoutWarn())) {
                return;
            }
            // 计算预警时间
            Date preWarnDate = TaskTimoutUtil.calcTimeOutTime(DateUtil.convertObjToUtilDate(wfInstance.getStartTime()), processTimoutInfo.getOvertimePreWarnTime(), processTimoutInfo.getOvertimePreWarnTimeUnit());
            // 判断是否满足超时预警条件
            if (cn.hutool.core.date.DateUtil.compare(new Date(), preWarnDate, DatePattern.NORM_DATETIME_MS_PATTERN) >= 0) {

                wfTaskflow = processCoreService.selectByTaskId(theFirstTask.getId());
                //noinspection unchecked
                doSendTimeoutMsg(true, wfInstance, wfTaskflow, processTimoutInfo.getOvertimePreWarnTemplate(), JSONUtil.toBean(wfTaskflow.getBussinessMap(), Map.class));
            }
        }
        wfInstance.setUpdateTime(LocalDateTime.now());
        wfInstanceService.updateById(wfInstance);
        // 流程超时处理
        if (processTimoutInfo.getOvertimeTime() != 0 && StrUtil.isNotEmpty(processTimoutInfo.getOvertimeTimeUnit())
                && ObjectUtil.isNotEmpty(processTimoutInfo.getOvertimeExecution())) {

            // 计算超时时间
            Date preWarnDate = TaskTimoutUtil.calcTimeOutTime(DateUtil.convertObjToUtilDate(wfInstance.getStartTime()), processTimoutInfo.getOvertimeTime(), processTimoutInfo.getOvertimeTimeUnit());
            // 判断是否满足超时预警条件
            if (cn.hutool.core.date.DateUtil.compare(new Date(), preWarnDate, DatePattern.NORM_DATETIME_MS_PATTERN) >= 0) {

                wfInstance.setIsTimeout("1");
                wfInstance.setUpdateTime(LocalDateTime.now());
                wfInstanceService.updateById(wfInstance);
                // 流程超时处理
                if (processTimoutInfo.getOvertimeExecution() == TimeoutExecution.process_endProcessAbnormal) {
                    try {

                        WorkFlowTaskTransferData data = constructEndProcessParaByProcessId(processInstance.getProcessInstanceId(),
                                processTimoutInfo.getOvertimeExecution().getName());
                        data.setCancelType(Constants.COMPLETE_TYPE_TIMEEND);
                        processCoreService.endProcessInstance(data, null);

                        updateWfInstance(wfInstance.getProcessId());
                    } catch (Exception e) {
                        log.error(BpmStringUtil.getExceptionStack(e), e);
                    }
                } else if (processTimoutInfo.getOvertimeExecution() == TimeoutExecution.process_endProcessNormal) {
                    try {
                        WorkFlowTaskTransferData data = constructEndProcessParaByProcessId(processInstance.getProcessInstanceId(),
                                processTimoutInfo.getOvertimeExecution().getName());
                        data.setCancelType(Constants.COMPLETE_TYPE_TIMEEND);
                        processCoreService.endProcessInstance(data, null);
                        updateWfInstance(wfInstance.getProcessId());
                    } catch (Exception e) {
                        log.error(BpmStringUtil.getExceptionStack(e), e);
                    }
                }
            }
        }
    }

    private void updateWfInstance(String processId) {
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processId);
        wfInstance.setProcessId(processId);
        wfInstance.setEndType(Constants.COMPLETE_TYPE_TIMEEND);
        wfInstance.setUpdateTime(LocalDateTime.now());
        wfInstanceService.updateById(wfInstance);
    }

    @SuppressWarnings("unchecked")
    private void doSendTimeoutMsg(boolean isProcessTimeout, WfInstance wfInstance, WfTaskflow wfTaskflow, String templateContent, Map<String, Object> varMap) {

        List<WfMessage> messageList = new ArrayList<>();
        List<UserOutputData> userOutputDataList = BpmStringUtil.getDoTaskUserDataFromVarMap(wfTaskflow);

        if (ObjectUtil.isEmpty(userOutputDataList)) {
            log.info("| - ProcessUtil>>>>>can't finde user tos send msg");
            return;
        }

        for (UserOutputData userOutputData : userOutputDataList) {

            log.debug("| - ProcessUtil>>>>>wfInstance:{}", wfInstance);

            WfMessage message = new WfMessage();
            messageList.add(message);
            message.setUserId(userOutputData.getUserId());
            message.setTaskId(wfTaskflow.getTaskId());
            message.setProcExtId(wfTaskflow.getProcessId());
            message.setSmsCotent(templateContent);
            message.setSmsTime(0);
            message.setVersion(0);
            message.setResult(0);
            // 流程处理人信息
            varMap.put("userId", userOutputData.getUserId());
            varMap.put("userName", userOutputData.getUserName());
            // 流程发起人信息
            if(ObjectUtil.isNotEmpty(varMap.get("userIdMap"))) {
                Map<String, Object> userIdMap = (Map<String, Object>) varMap.get("userIdMap");
                varMap.put("startUserId", userIdMap.get("userId"));
                varMap.put("startUserName", userIdMap.get("userName"));
            }
            // 流程发起时间
            if(ObjectUtil.isNotEmpty(varMap.get("processCreateTime"))){
                if(NumberUtil.isLong(varMap.get("processCreateTime").toString())){
                    DateTime processCreateTime = cn.hutool.core.date.DateUtil.date(Long.parseLong(varMap.get("processCreateTime").toString()));
                    varMap.put("processCreateTime", cn.hutool.core.date.DateUtil.format(processCreateTime, DatePattern.NORM_DATETIME_PATTERN));
                }else{
                    varMap.put("processCreateTime", varMap.get("processCreateTime"));
                }
            }
            // 业务数据
            if(ObjectUtil.isNotEmpty(varMap.get("dataMap"))) {
                varMap.putAll((Map<String, Object>) varMap.get("dataMap"));
            }

            if (templateContent != null) {
                if (!isProcessTimeout) {
                    wfTaskflow.setTimeoutWarn(templateContent);
                } else {
                    wfInstance.setTimeoutWarn(templateContent);
                }
            }
            WfMsgNotifyTimerTask msgTimerTask = new WfMsgNotifyTimerTask(message, varMap);
            timerUtil.addWorkflowTimerTask(msgTimerTask, -1, -1, -1, -1, -1, 20);
        }
        messageService.saveBatch(messageList);
    }

    /**
     * <pre>
     * 根据任务id、查找任务的超时配置、根据超时时间、超时规则对超时任务进行清理
     * 处理过程
     * 1、根据节点信息查找对应的流程图及节点扩展属性中任务处理时间及超时处理策略的配置。如果没有找到对应的处理策略，则忽略。
     * 2、获取超时任务的详细信息、计算并设置超时时间。超时直接不断判断并设置，设置完成后返回。
     * 3、当任务超时时，调用对应的策略进行处理
     * </pre>
     *
     * @param processInstance
     * @param task
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doTimeoutCleanSpecTaskInst(ProcessInstance processInstance, Task task) {
        log.debug("| - ProcessUtil>>>>>doTimeoutCleanSpecProcessInst------task.getTaskDefinitionKey:{} task.getId:{}", task.getTaskDefinitionKey(), task.getId());

        // 实时获取超时信息需要更改获取签名
        TaskTimoutInfo taskTimoutInfo = getTaskTimeoutInfo(XmlUtil.getProcessIdOnly(task.getProcessDefinitionId()),
                BigInteger.valueOf(processInstance.getProcessDefinitionVersion()), task.getTaskDefinitionKey());

        if (taskTimoutInfo == null) {
            return;
        }

        // 任务已处理或者任务状态为霸屏或者超时，则直接返回
        WfTaskflow wfTaskflow = processCoreService.selectByTaskId(task.getId());
        if (wfTaskflow == null) {
            return;
        }

        if (wfTaskflow.getIsTimeout() != null && !BpmConstants.TIMEOUT_FLAG_0.equals(wfTaskflow.getIsTimeout())) {
            return;
        }
        wfTaskflowService.selectByIdForUpdate(wfTaskflow.getId());

        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstance.getId());

        // 任务超时预警处理
        if (taskTimoutInfo.getOvertimePreWarnTime() != 0 && StrUtil.isNotEmpty(taskTimoutInfo.getOvertimePreWarnTimeUnit())
                && StrUtil.isNotEmpty(taskTimoutInfo.getOvertimePreWarnTemplate())) {
            // 超时预警信息不为空时，不做超时预警处理
            if (StrUtil.isNotEmpty(wfInstance.getTimeoutWarn())) {
                return;
            }
            // 计算预警时间
            Date preWarnDate = TaskTimoutUtil.calcTimeOutTime(DateUtil.convertObjToUtilDate(wfTaskflow.getStartDate()),
                    taskTimoutInfo.getOvertimePreWarnTime(), taskTimoutInfo.getOvertimePreWarnTimeUnit());
            // 判断是否满足超时预警条件
            if (cn.hutool.core.date.DateUtil.compare(new Date(), preWarnDate, DatePattern.NORM_DATETIME_MS_PATTERN) >= 0) {

                //noinspection unchecked
                doSendTimeoutMsg(false, wfInstance, wfTaskflow, taskTimoutInfo.getOvertimePreWarnTemplate(), JSONUtil.toBean(wfTaskflow.getBussinessMap(), Map.class));
            }
        }

        // 任务超时处理
        if (taskTimoutInfo.getOvertimeTime() != 0 && StrUtil.isNotEmpty(taskTimoutInfo.getOvertimeTimeUnit())
                && ObjectUtil.isNotEmpty(taskTimoutInfo.getOvertimeExecution())) {

            // 计算超时时间
            Date timeOutTime = TaskTimoutUtil.calcTimeOutTime(DateUtil.convertObjToUtilDate(wfTaskflow.getStartDate()),
                    taskTimoutInfo.getOvertimeTime(), taskTimoutInfo.getOvertimeTimeUnit());
            // 判断是否满足超时条件
            if (cn.hutool.core.date.DateUtil.compare(new Date(), timeOutTime, DatePattern.NORM_DATETIME_MS_PATTERN) >= 0) {

                wfTaskflow.setIsTimeout(Constants.TIMEOUT_TIP_NO_MALL);
                // 流程超时处理
                try {
                    if (taskTimoutInfo.getOvertimeExecution() == TimeoutExecution.doNextAbnormal) {
                        log.debug("| - ProcessUtil>>>>>TimeoutExecution.doNextAbnormal {}", task.getName());
                        doNextTask(task.getId(), taskTimoutInfo.getOvertimeExecution(),
                                taskTimoutInfo.getExecVariable());
                    } else if (taskTimoutInfo.getOvertimeExecution() == TimeoutExecution.doNextNormal) {
                        log.debug("| - ProcessUtil>>>>>TimeoutExecution.doNextNormal {}", task.getName());
                        doNextTask(task.getId(), taskTimoutInfo.getOvertimeExecution(),
                                taskTimoutInfo.getExecVariable());
                    } else if (taskTimoutInfo.getOvertimeExecution() == TimeoutExecution.endProcessNormal) {
                        log.debug("| - ProcessUtil>>>>>TimeoutExecution.endProcessNormal {}", task.getName());
                        processCoreService.endProcessInstance(constructEndProcessParaByProcessId(
                                processInstance.getProcessInstanceId(), TimeoutExecution.endProcessNormal.getName()), null);
                        updateWfInstance(wfInstance.getProcessId());
                        log.debug("| - ProcessUtil>>>>>TimeoutExecution.endProcessNormal {}", task.getName());
                    } else if (taskTimoutInfo.getOvertimeExecution() == TimeoutExecution.endProcessAbnormal) {
                        processCoreService.endProcessInstance(constructEndProcessParaByProcessId(
                                processInstance.getProcessInstanceId(), TimeoutExecution.endProcessAbnormal.getName()), null);
                        updateWfInstance(wfInstance.getProcessId());
                        log.debug("| - ProcessUtil>>>>>TimeoutExecution.endProcessAbnormal {}", task.getName());
                    }else {
                        log.info("| - ProcessUtil>>>>>not supported TimeoutExecution:{} ", taskTimoutInfo.getOvertimeExecution().getName());
                    }
                } catch (Exception e) {
                    log.error(BpmStringUtil.getExceptionStack(e), e);
                }
            }
        }
        wfTaskflowService.updateById(wfTaskflow);
    }

    private SubmitReturnData doNextTask(String taskId, TimeoutExecution timeoutExecution, String execVariable)
            throws Exception {
        SubmitReturnData result;

        WorkFlowTaskTransferData workFlowTaskTransferData = getDoNextParam(taskId, timeoutExecution, execVariable);
        workFlowTaskTransferData.setDoFinishSign(true);
        result = processCoreService.nextTask(workFlowTaskTransferData);
        return result;
    }

    private WorkFlowTaskTransferData getDoNextParam(String taskInstId, TimeoutExecution timeoutExecution,
                                                    String timeoutRouteValue) {
        WfTaskflow wfTaskflow = processCoreService.selectByTaskId(taskInstId);
        if (StrUtil.isEmpty(wfTaskflow.getTaskActors())) {
            throw new RuntimeException("task actor can not be empty");
        }
        String[] taskActors = wfTaskflow.getTaskActors().split(SymbolConstants.COMMA);

        WorkFlowTaskTransferData data = new WorkFlowTaskTransferData();

        //2019-05-27 modify amt start 测试中发现超时流转时，没有报文头报错
        WorkFlowServiceBean respBean = new WorkFlowServiceBean();
        data.setSysCommHead(respBean.getTranHead());
        //2019-05-27 modify amt end 测试中发现超时流转时，没有报文头报错
        Map<String, Object> exectMap = null;
        String varStr = wfTaskflow.getBussinessMap();
        data.setBussinessMap(exectMap);

        log.info("| - ProcessUtil>>>>>taskInstId:{},timeoutRouteValue:{}", taskInstId, timeoutRouteValue);
        if (!StrUtil.isEmpty(timeoutRouteValue)) {
            exectMap = JSONObject.parseObject(varStr);
            exectMap.put(Constants.ROUTE_CONDITION_KEY, timeoutRouteValue);
            data.setPassingRouteFlag(timeoutRouteValue);
        }


        data.setUserId(taskActors[0]);
        data.setTaskId(taskInstId);
        if (timeoutExecution == TimeoutExecution.doNextNormal) {
            data.setResult(Constants.RESULT_PASS);
        } else if (timeoutExecution == TimeoutExecution.doNextAbnormal) {
            data.setResult(Constants.RESULT_OPPOSE);
        } else {
            data.setResult(Constants.RESULT_SYSUSE);
        }
        return data;
    }

    public TaskTimoutInfo getTaskTimeoutInfo(String taskInstId) {
        if (StrUtil.isEmpty(taskInstId)) {
            return null;
        }
        Task theTask = processEngine.getTaskService().createTaskQuery().taskId(taskInstId).singleResult();
        if (theTask == null) {
            return null;
        }
        if (StrUtil.isEmpty(theTask.getProcessDefinitionId())) {
            return null;
        }
        ProcessInstance pi = processEngine.getRuntimeService().createProcessInstanceQuery()
                .processInstanceId(theTask.getProcessDefinitionId()).singleResult();
        if (pi.getProcessDefinitionVersion() == null) {
            return null;
        }
        return getTaskTimeoutInfo(theTask.getProcessDefinitionId(), BigInteger.valueOf(pi.getProcessDefinitionVersion()),
                theTask.getTaskDefinitionKey());
    }

    public TaskTimoutInfo getTaskTimeoutInfo(String processDefId, BigInteger version, String taskDefId) {

        return TaskTimoutInfo.getTaskTimeoutInfo(wfModelPropExtendsService, cacheDaoSvc, processDefId, version,
                taskDefId);
    }

    /**
     * 获取节点的版本信息
     * <pre>
     * 1、当忽略版本时，flowElement.id 和attr作为联合主键查询，返回对应的扩展属性
     * 2、当不忽略版本时，或者忽略版本没有找到最新的属性时，返回原版本的属性为依据进行操作
     * </pre>
     * 属性对应的定义信息
     * <table width="400px;" border="1">
     * <thead><tr> <th>属性</th><th>key</th></tr></thead>
     * <tr><td>子流程\会签的等待模</td><td>waitType</td></tr>
     * <tr><td> 节点类型          </td><td>nodeType</td></tr>
     * <tr><td> 节点人员分配策略  </td><td>sysNodePath</td></tr>
     * <tr><td> 任务类型key </td><td>sysNodeId</td></tr>
     * <tr><td> 选人为空跳过      </td><td>isblock </td></tr>
     * <tr><td> 子流程节点的子流程编码属性名称 	</td><td>subFlowSelectRule</td></tr>
     * <tr><td> 子流程发起人属性 	</td><td>startPerson</td></tr>
     * <tr><td> 会签策略  			</td><td>	jointlySelect </td></tr>
     * <tr><td> 会签策略百分比		</td><td>	jointlyPercent</td></tr>
     * <tr><td> 会签指定人数		</td><td>jointlyManage </td></tr>
     * </table>
     *
     * @param flowElement 节点
     * @param attr        属性
     * @return 节点对应的扩展属性
     */
    public String getTaskProp(String processCode, FlowElement flowElement, String attr) {
        String prop = null;
        boolean isIgnore = configUtil.isIgnoreProcessVersion();
        if (isIgnore) {
            WfDefinePropExtends condition = new WfDefinePropExtends();
            condition.setTaskCode(flowElement.getId());
            condition.setPropName(attr);
            condition.setProcessCode(processCode.split(":")[0]);
            condition.setVersion(new BigInteger(processCode.split(":")[1]));
            WfDefinePropExtends propextends = wfDefinePropExtendsService.selectByProcessCodePropNameTaskCode(condition);
            if (propextends != null) {
                return propextends.getPropValue();
            }
        }
        if (StrUtil.isEmpty(prop)) {
            prop = flowElement.getAttributeValue(Constants.XMLNAMESPACE, attr);
        }
        return prop;
    }


    @Transactional
    public void nextSubprocNode4Auto(Task childNodeInSub, String resultRemark, String userId) throws Exception {
        if (childNodeInSub == null) {
            return;
        }
        WorkFlowTaskTransferData workFlowTaskTransferData = new WorkFlowTaskTransferData();
        workFlowTaskTransferData.setTaskId(childNodeInSub.getId());
        workFlowTaskTransferData.setProcessInstId(childNodeInSub.getProcessInstanceId());
        workFlowTaskTransferData.setCancelType(Constants.COMPLETE_TYPE_TIMEEND);
        workFlowTaskTransferData.setResultRemark(resultRemark);
        try {
            endProcessInstance(workFlowTaskTransferData, userId, false);
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e));
            return;
        }


        activitiBaseServer.commitTask(childNodeInSub.getId(), new HashMap<String, Object>());
    }

    @Transactional
    public void endProcessInstanceNew(WorkFlowTaskTransferData taskTransferData, String userId, boolean endParentProcess) {
        // 检查条件
        String taskId = taskTransferData.getTaskId();
        String processInstId = taskTransferData.getProcessInstId();

        if (StrUtil.isEmpty(processInstId)) {
            if (StrUtil.isEmpty(taskId)) {
                // 流程节点id不能为空
                throw new ServiceException(ErrorCode.WF000016);
            }
            Task theTask = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
            if (theTask == null) {
                // 流程节点id不能为空
                throw new ServiceException(ErrorCode.WF000016);
            }
            processInstId = theTask.getProcessInstanceId();
        }
    }

    private void endProcessInstance(WorkFlowTaskTransferData taskTransferData, String userId, boolean endParentProcess) throws Exception {
        // 检查条件
        // String userId = taskTransferData.getUserId();
        String taskId = taskTransferData.getTaskId();
        String processInstId = taskTransferData.getProcessInstId();
        String cancelType = taskTransferData.getCancelType();
        // 获取流程定义Id
        ProcessInstance processInstance = null;
        String errmsg = "当前任务唯一标识输入有误，请确认！";
        if (StrUtil.isBlank(processInstId)) {
            processInstance = activitiBaseServer.findProcessInstanceByTaskId(taskId);
            processInstId = processInstance.getId();
        } else {
            processInstance = activitiBaseServer.findProcessInstance(processInstId);
            errmsg = "当前流程实例唯一标识输入有误，请确认！";
        }
        if (processInstance == null) {
            throw new ServiceException(errmsg);
        }
        log.info("| - ProcessUtil>>>>>endProcessInstance >>>>流程实例Id：{}", processInstId);

        log.info("| - ProcessUtil>>>>>processInstance.getProcessDefinitionId():{}", processInstance.getProcessDefinitionId());
        // 找到流程的结束个节点
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processInstance.getProcessDefinitionId());
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();
        FlowElement endElement = null;
        for (FlowElement element : nodeList) {
            if (element instanceof EndEvent) {
                endElement = element;
                //如果节点是异常结束，则服务调用会出现问题
                List<ActivitiListener> lis = endElement.getExecutionListeners();
                if (lis == null || lis.isEmpty()) {
                    break;
                }
            }
        }
        //如果没有找到正常节点只要是结束节点，都调用
        if (endElement == null) {
            for (FlowElement element : nodeList) {
                if (element instanceof EndEvent) {
                    endElement = element;
                    break;
                }
            }
        }
        //理论上不会出现
        if (endElement == null) {
            throw new ServiceException("未找到结束节点！！！");
        }

        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstId).list();
        if (tasks == null || tasks.size() == 0) {
            return;
        }
        String mainProcessId = null;
        WfTaskflow mainTaskflow = null;
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstId);
        //说明是子流程，需要查找主流程
        if (!StrUtil.isEmpty(wfInstance.getParentTaskId())) {
            mainTaskflow = wfTaskflowService.selectByTaskId(wfInstance.getParentTaskId());
            mainProcessId = mainTaskflow.getProcessId();
        }

        for (Task theTask : tasks) {
            //如果当前节点是主流程的子流程节点，先结束子流程节点,递归结束主流程节点；
            WfTaskflow childTaskflow = wfTaskflowService.selectByTaskId(theTask.getId());
            if (childTaskflow != null && childTaskflow.getWorkState().equals(Constants.TASK_STATE_RUNING)) {
                boolean flag = false;
                List<WfTaskflow> list = wfTaskflowService.selectSubproListByProcessInstId(processInstId);
                for (WfTaskflow w : list) {
                    if (w.getWorkState().equals(Constants.TASK_STATE_RUNING)) {
                        WorkFlowTaskTransferData taskTransferData1 = new WorkFlowTaskTransferData();
                        taskTransferData1.setProcessInstId(w.getProcessId());
                        taskTransferData1.setTaskId(w.getTaskId());
                        taskTransferData1.setCancelType(cancelType);
                        endProcessInstance(taskTransferData1, userId, endParentProcess);
                        flag = true;
                    }
                }
                if (flag) {
                    return;
                }
            }
            endEachTaskProcess(theTask, endElement, taskTransferData.getResult(), taskTransferData.getResultRemark(),
                    userId, cancelType);
        }
        doAfterEndProcess(processInstance.getProcessDefinitionId(), processInstance.getProcessInstanceId(), endElement);

        //如果是子流程递归调用主流程
        if (mainProcessId != null) {
            //如果主流程结束，则结束
            tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(mainProcessId).list();
            if (tasks == null || tasks.size() == 0) {
                return;
            }
            WorkFlowTaskTransferData taskTransferData1 = new WorkFlowTaskTransferData();
            taskTransferData1.setProcessInstId(mainProcessId);
            taskTransferData1.setTaskId(mainTaskflow.getTaskId());
            taskTransferData1.setCancelType(cancelType);
            endProcessInstance(taskTransferData1, userId, endParentProcess);
        }


    }


    private void endEachTaskProcess(Task theTask, FlowElement endElement, String doResult, String resultRemark,
                                    String userId, String cancelType) {

        theTask = GatewayUtil.jumpOutGateway4EndTransaction(cacheDaoSvc, processEngine, theTask);
        if (theTask == null) {
            return;
        }

        // // 将流程跳转到最后节点

        Map<String, Object> inputMap = theTask.getProcessVariables();
        if (inputMap == null) {
            inputMap = new HashMap<>();
        }

        WfTaskflow taskflow = processCoreService.selectByTaskId(theTask.getId());
        if (taskflow != null) {
            if (StrUtil.isBlank(userId)) {
                if (!StrUtil.isEmpty(taskflow.getTaskActors())) {
                    taskflow.setDoTaskActor(taskflow.getTaskActors().split(SymbolConstants.COMMA)[0]);
                }
            } else {
                taskflow.setDoTaskActor(userId);
                JSONArray userAskListJson = JSONArray.parseArray(" [ { \"orderUserNo\":\"" + userId + "\"}]");
                List<UserOutputData> list;
                try {
                    list = queryRelationTellerFactory.getInstance().queryUserList(userAskListJson);
                    String doTaskActorName = userId;
                    if (list != null && !list.isEmpty()) {
                        doTaskActorName = list.get(0).getUserName();
                    }
                    taskflow.setDoTaskActorName(doTaskActorName);
                } catch (Exception e) {
                    log.error("| - ProcessUtil>>>>>获取用户信息【{}】失败", userId, e);
                }

            }
            inputMap.putAll(BpmStringUtil.jsonToMap(taskflow.getBussinessMap()));
            List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                    .selectModelExtendsPropByOwnId(theTask.getProcessDefinitionId().split(":")[0]);
            WfModelPropExtends cancelEvent = modelPropService.getSpcefiyModelProp(extendsPropList,
                    ModelProperties.cancelEvent.getName());
            if (cancelEvent != null) {
                // 用于表示流程是终止取消了，用户后续收尾处理
                inputMap.put(Constants.COMPLETE_TYPE_KEY, Constants.COMPLETE_TYPE_CANCLE);
            } else {
                if (StrUtil.isEmpty(cancelType)) {
                    cancelType = Constants.COMPLETE_TYPE_ERROR;
                }

                inputMap.put(Constants.COMPLETE_TYPE_KEY, cancelType);
            }
            inputMap.put(Constants.MANUAL_END_PROCESS, Constants.COMPLETE_TYPE_CANCLE);

            inputMap.put(Constants.BEFOR_TASKID, theTask.getId());
            log.info("| - ProcessUtil>>>>>ready to setVariableMap in endEachTaskProcess");
            WfVariableUtil.setVariableMap(inputMap);
        }


        theTask = processEngine.getTaskService().createTaskQuery().taskId(theTask.getId()).singleResult();
        if (theTask != null) {
            // 跳转至流程的结束节点
            activitiBaseServer.findCommandExecutor()
                    .execute(new JumpCommand(theTask.getId(), endElement.getId(), inputMap, ""));
        }


        if (taskflow != null) {
            // 更新流转记录表
            taskflow.setWorkState(Constants.TASK_STATE_END);
            taskflow.setDoResult(doResult);
            taskflow.setDoRemark(resultRemark);
            taskflow.setUpdateDate(LocalDateTime.now());
            taskflow.setEndDate(LocalDateTime.now());
            taskflow.setDoTaskActor(userId);
            taskflow.setUpdateRemark("任务取消,流程结束");
            wfTaskflowService.updateById(taskflow);
        }


    }

    /**
     * 获取所有配有超时处理的流程
     *
     * @return
     */
    public List<String> selectAllTimeOutProcess() {
        // 查询开启超时的流程
        WfModelPropExtends condition = new WfModelPropExtends();
        condition.setPropName("openTimeout");
        condition.setPropValue("true");
        List<WfModelPropExtends> processModel = wfModelPropExtendsService.selectList(condition);
        if (Objects.nonNull(processModel)) {
            return processModel.stream().map(WfModelPropExtends::getOwnId).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * 分页查询 【开启了超时的流程 & 流程实例未结束 & 流程未超时】的流程实例
     *
     * @param defIds
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public List<WfInstance> selectNoTimeOutList(List<String> defIds, int pageNumber, int pageSize) {
        PageData<WfInstance> pageData = new PageData<>();
        setPageData(pageData, pageNumber, pageSize);
        Map<String, Object> param = new HashMap<>();
        param.put("processDefIds", defIds);
        param.put("nowDate", cn.hutool.core.date.DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));
        return wfInstanceService.selectNoTimeOutList(param, pageData);
    }

    private void setPageData(PageData<?> page, int currentPage, int pageSize) {
        page.setCurPage(currentPage);
        page.setPageRows(pageSize);
        page.setStartRows((currentPage - 1) * pageSize);
    }

}
