package com.bpm.workflow.timertask;

import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.SpringUtil;
import com.bpm.workflow.util.WfVariableUtil;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class BackInvockAutoTask extends WorkflowTimerTask {
    private static final long serialVersionUID = 1L;
    @Resource
    private WfCommService wfCommService;
    @Resource
    private WfTaskflowService wfTaskflowService;
    private Map<String, Object> variableMap;

    private String taskId;

    public BackInvockAutoTask(String taskId, Map<String, Object> variableMap) {

        this.taskId = taskId;
        this.variableMap = variableMap;
    }

    @Override
    public void runImpl() throws Exception {
        ActivitiBaseService activitiBaseServer = SpringUtil.getBean(ActivitiBaseService.class);

        // 根据规则设置下一步执行路由，暂时不根据返回结果设置路径
        FlowElement flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(taskId);
        WfTaskflow taskflow = wfTaskflowService.selectByTaskId(taskId);
        wfCommService.forkRouteSelect(taskflow.getProcessCode(), flowElement, variableMap);

        if (log.isInfoEnabled()) {
            log.info("| - BackInvockAutoTask>>>>>开始执行自动跳转[taskId={}]", taskId);
        }
        WfVariableUtil.setVariableMap(variableMap);
        activitiBaseServer.commitTask(taskId, new HashMap<String, Object>());
        log.error("| - BackInvockAutoTask>>>>>完成[taskId={}]节点的自动跳转", taskId);
    }

}
