package com.bpm.workflow.service;

import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.util.CacheUtil;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.dom4j.Element;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.math.BigInteger;

@Service
public class CacheDaoSvc {

    @Resource
    private WfDefinitionService wfDefinitionService;
    @Resource
    private CacheUtil cacheUtil;


    public WfDefinition selectActiveFlowByDefId(String processCode) {
        WfDefinition result= (WfDefinition)cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getCacheValueByKey("selectActiveFlowByDefId", processCode,null);
        return result;
    }

    public WfDefinition selectByDefIdActVersion(String processDefId, BigInteger version) {
        return (WfDefinition)cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getCacheValueByKey("selectByDefIdActVersion",processDefId, version.toString());
    }



    public Element getTaskElementByDefIdActVersionTaskDefId(String processDefId, BigInteger version, String taskDefId) {
        return cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getTaskElementByDefIdActVersionTaskDefId(processDefId, version, taskDefId);
    }

    public FlowElement getBpmnModelNodeByTaskId(TaskEntity task) throws Exception {
        return cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getBpmnModelNodeByTaskId(task.getProcessDefinitionId(), task.getTaskDefinitionKey());
    }

    @Transactional
    public 	int updateByDefId(WfDefinition theArg) {
        int result = wfDefinitionService.updateByDefId(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {theArg.getDefId(), null}, true);
        return result;
    }
    @Transactional
    public 	int deleteByDefIdDefName(WfDefinition theArg) {
        int result = wfDefinitionService.deleteByDefIdDefName(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {theArg.getDefId(), null}, true);
        return result;
    }
    @Transactional
    public 	int deleteByDefId(String theArg) {
        int result = wfDefinitionService.deleteByDefId(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {theArg, null}, true);
        return result;
    }

    @Transactional
    public int updateByDefIdDefName(WfDefinition theArg) {
        int result = wfDefinitionService.updateByDefIdDefName(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {theArg.getDefId(), null}, true);
        return result;
    }
    @Transactional
    public void insertWfDefinition(WfDefinition theArg) {
        wfDefinitionService.insertWfDefinition(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {theArg.getDefId(), null}, true);
    }
    @Transactional
    public int updateByKeyId(WfDefinition wfDefinition) {
        int result = wfDefinitionService.updateByKeyId(wfDefinition);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).refreshByKey(CacheUtil.SvcType.wfDefinition, null, new String[] {wfDefinition.getDefId(), null}, true);
        return result;
    }

}
