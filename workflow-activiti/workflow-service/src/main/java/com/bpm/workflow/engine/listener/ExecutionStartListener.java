package com.bpm.workflow.engine.listener;

import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.service.WfEvtServiceDefService;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.ModelProperties;
import com.bpm.workflow.util.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 流程实例启动的监听器
 *
 */
@Log4j2
@Component("executionStartListener")
public class ExecutionStartListener extends SpringUtil implements ExecutionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1277262716680992319L;
	
	@Resource
	private WfEvtServiceDefService wfEvtServiceDefService;
	
	@Resource
	private ModelPropService modelPropService;
	
	@Resource
	private WfModelPropExtendsService wfModelPropExtendsService;
	
	@Resource
	private WfCommService wfCommService;


	@Override
	public void notify(DelegateExecution execution) {
//		doNotify(
//				wfEvtServiceDefService,modelPropService,modelPropMapper,
//				execution.getProcessDefinitionId(),
//				execution.getVariables(),
//				execution.getProcessInstanceId(),
//				execution.getId(),
//				execution.getCurrentFlowElement()
//				);
		
	}
	
	public  void doNotify(String execProcessDefinitionId, Map<String,Object> execVariables, String execProcessInstanceId, String taskId, FlowElement flowElement, SysCommHead sysCommonHead) {


		if(log.isDebugEnabled()) {
			log.info("| - ExecutionStartListener>>>>>流程实例启动的监听器[ProcessDefinitionId:{}]",execProcessDefinitionId);
			log.info("| - ExecutionStartListener>>>>>流程实例启动的监听器[execVariables:{}]",execVariables);
			log.info("| - ExecutionStartListener>>>>>流程实例启动的监听器[processInstanceId:{}]",execProcessInstanceId);

		}
		//获取扩展属性
		List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(execProcessDefinitionId.substring(0,execProcessDefinitionId.indexOf(":")));
		
		// 流程启动条件判断 ---应该在流程开始时判断
		wfCommService.conditionalVerification(ModelProperties.startCondition.getName(), extendsPropList, execVariables, "不满足流程启动条件，请查看！");

		//流程启动事件调用
		WfModelPropExtends  flowStartEvent = modelPropService.getSpcefiyModelProp(extendsPropList, ModelProperties.flowStartEvent.getName());

		wfEvtServiceDefService.executeEventService(flowStartEvent, taskId,
				execProcessDefinitionId, execProcessInstanceId, execVariables,
				flowElement,sysCommonHead);
	}

}
