package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfMessageContent;
import com.bpm.workflow.mapper.WfMessageContentMapper;
import com.bpm.workflow.service.WfMessageContentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfMessageContentServiceImpl extends ServiceImpl<WfMessageContentMapper, WfMessageContent> implements WfMessageContentService {

}
