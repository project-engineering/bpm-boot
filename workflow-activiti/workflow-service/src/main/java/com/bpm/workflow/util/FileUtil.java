package com.bpm.workflow.util;

import com.bpm.workflow.constant.SymbolConstants;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.*;

@Component
public class FileUtil {
    
    @Resource
	private ConfigUtil configUtil;
    
    public String getFileContentByRelPath(String relPath) {

    	String fileDefaultLocation = configUtil.getFileLocl().trim();
    	String filePath = null;

		if((fileDefaultLocation.endsWith(SymbolConstants.SYMBOL_007) || fileDefaultLocation.endsWith(SymbolConstants.SYMBOL_008))) {
			filePath = fileDefaultLocation+relPath;
		}else {
			filePath = fileDefaultLocation+SymbolConstants.SYMBOL_008 + relPath;
		}
		return getFileContentByPath(filePath);
    	
    }
    
    public String getFileContentByPath(String filePath) {
    	return getFileContent(filePath,configUtil.getFileEncode());
    }
    
    public static String getFileContent(String filePath,String encoding) {
    	return getFileContent(new File(filePath),encoding);
    }
    public static String getFileContent(File filePath,String encoding) {
    	if(!filePath.exists()) {
    		throw new RuntimeException("can't find file "+filePath.getAbsolutePath());
    	}
    	byte[] bytes = new byte[(int)filePath.length()];
    	String result = null;
    	try(BufferedInputStream br = new BufferedInputStream(new FileInputStream(filePath))){
    		br.read(bytes);    	
    		result = new String(bytes,encoding);
    	} catch (IOException e) { 
			throw new RuntimeException("can't read file "+filePath.getAbsolutePath(),e);
		} 
    	return result;
    }
    
    public static void writeFile(String dirPath,String fileName, String encoding,String content,boolean isAppend) {
    	File fdir = new File(dirPath);
    	if(!fdir.exists()) {
        	boolean isSucess = fdir.mkdirs();
        	if(!isSucess){
        		throw new RuntimeException("create fdir fail:"+fdir.mkdirs());
    		}
    	}

    	File f = new File(fdir,fileName);
    	if(f.exists()) {
    		if(isAppend) {
    			content = content+getFileContent(f,encoding);
    		}
    	}
    	try (
    			BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(f));
    			){
			bo.write(content.getBytes(encoding));
		} catch (Exception e) {
			throw new RuntimeException("can't write file "+f.getAbsolutePath(),e);
		}
    }
}
