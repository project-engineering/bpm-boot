package com.bpm.workflow.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.dto.transfer.EventNodeProp;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.engine.listener.SequenceFlowCadition;
import com.bpm.workflow.entity.*;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.*;
import com.bpm.workflow.service.*;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.DateUtil;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.timertask.LatchInvokeEventTask;
import com.bpm.workflow.timertask.TimerUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateTask;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Log4j2
@Service
public class WfEvtServiceDefServiceImpl extends ServiceImpl<WfEvtServiceDefMapper, WfEvtServiceDef> implements WfEvtServiceDefService {
    @Resource
    WfEvtServiceDefMapper wfEvtServiceDefMapper;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfEvtServiceLogService wfEvtServiceLogService;
    @Resource
    private WfCommService wfCommService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private WfBusinessDataService wfBusinessDataService;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private WfExceptionDataService wfExceptionDataService;
    @Resource
    TimerUtil timerUtil;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private WfInstanceService wfInstanceService;
    @Resource
    ActivitiBaseService activitiBaseService;

    @Transactional
    public void insertEvtServiceDef(WfEvtServiceDef wfEvtServiceDef) {
        //必输项检查
        checkMustInputField(wfEvtServiceDef, false);

        //事件服务编码唯一性检查
        WfEvtServiceDef targetDef = selectByServiceId(wfEvtServiceDef.getServiceId());
        if (targetDef != null) {
            throw new ServiceException("事件服务编码[" + wfEvtServiceDef.getServiceId() + "]已经存在，请确认！");
        }
        wfEvtServiceDefMapper.insert(wfEvtServiceDef);

    }

    @Transactional
    public void deleteEvtServiceDef(String ids) {
        String[] idsArray = ids.split(",");
        for (String id : idsArray) {
            WfEvtServiceDef wfEvtServiceDef = selectById(Integer.parseInt(id));
            if (wfEvtServiceDef == null) {
                throw new ServiceException("传入参数ID[" + id + "]错误或记录不存在，无法删除！");
            }

            //TODO xuesw  需要优化检查引用自服务的方式
            String likePropVal = "\"callMethod\":\"" + wfEvtServiceDef.getCallMethod() + "\"%\"serviceId\":\""
                    + wfEvtServiceDef.getServiceId() + "\"";
            log.info("| - WfEvtServiceDefService>>>>>likePropVal:{}", likePropVal);
            List<WfModelPropExtends> haveList = wfModelPropExtendsService.selectModelExtendsPropByPropVal(likePropVal);
            if (haveList.size() < 1) {
                //TODO xuesw 可以优化为批量删除
                wfEvtServiceDefMapper.deleteById(wfEvtServiceDef.getId());
            } else {
                throw new ServiceException("事件服务已被引用，无法删除！");
            }
        }

    }

    @Transactional
    public void updateEvtServiceDef(WfEvtServiceDef wfEvtServiceDef) {
        //必输项检查
        checkMustInputField(wfEvtServiceDef, true);

        //事件服务编码不允许修改，只能删除新建
        WfEvtServiceDef targetDef = wfEvtServiceDefMapper.selectById(wfEvtServiceDef.getId());
        if (!targetDef.getServiceId().equals(wfEvtServiceDef.getServiceId())) {
            throw new ServiceException("事件服务编码[" + targetDef.getServiceId() + "]不允许修改，请确认！");
        }

        wfEvtServiceDefMapper.updateById(wfEvtServiceDef);
    }

    public List<WfEvtServiceDef> selectList(Map<String, Object> param, PageData<WfEvtServiceDef> pageDate) {
        Page<WfEvtServiceDef> page = new Page<>(pageDate.getCurPage(),pageDate.getLimit());
        LambdaQueryWrapper<WfEvtServiceDef> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(param.get("serviceId"))) {
            queryWrapper.like(WfEvtServiceDef::getServiceId,param.get("serviceId"));
        }
        if (UtilValidate.isNotEmpty(param.get("callType"))) {
            queryWrapper.eq(WfEvtServiceDef::getCallType,param.get("callType"));
        }
        if (UtilValidate.isNotEmpty(param.get("callMode"))) {
            queryWrapper.eq(WfEvtServiceDef::getCallMethod,param.get("callMode"));
        }
        if (UtilValidate.isNotEmpty(param.get("name"))) {
            queryWrapper.eq(WfEvtServiceDef::getName,param.get("name"));
        }
        if (UtilValidate.isNotEmpty(param.get("callService"))) {
            queryWrapper.like(WfEvtServiceDef::getCallService,param.get("callService"));
        }
        if (UtilValidate.isNotEmpty(param.get("callMethod"))) {
            queryWrapper.like(WfEvtServiceDef::getCallMode,param.get("callMethod"));
        }
        queryWrapper.orderByDesc(WfEvtServiceDef::getId);
        return wfEvtServiceDefMapper.selectPage(page,queryWrapper).getRecords();
    }

    public WfEvtServiceDef selectById(Integer id) {
        return wfEvtServiceDefMapper.selectById(id);
    }


    public WfEvtServiceDef selectByServiceId(String serviceId) {
        LambdaQueryWrapper<WfEvtServiceDef> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfEvtServiceDef::getServiceId,serviceId);
        return wfEvtServiceDefMapper.selectOne(queryWrapper);
    }


    /**
     * 必输项检查
     *
     * @param wfEvtDef
     * @
     */
    private void checkMustInputField(WfEvtServiceDef wfEvtDef, boolean ischeckId) {
        //id检查
        if (ischeckId && wfEvtDef.getId() == null) {
            throw new ServiceException("输入参数Id不能为空,请确认！");
        }

        //服务编码
        if (StrUtil.isBlank(wfEvtDef.getServiceId())) {
            throw new ServiceException("事件服务编码不能为空,请确认！");
        }

        //服务名称
        if (StrUtil.isBlank(wfEvtDef.getName())) {
            throw new ServiceException("事件服务名称不能为空,请确认！");
        }

        //服务调用类型
        if (StrUtil.isBlank(wfEvtDef.getCallType())) {
            throw new ServiceException("事件服务调用类型不能为空,请确认！");
        }

        //调用服务
        if (StrUtil.isBlank(wfEvtDef.getCallService())) {
            throw new ServiceException("事件调用的服务不能为空,请确认！");
        }
    }

    /**
     * <pre>
     * 实现自动节点的调用
     * 1、检查配置参数是否正确，如果不正确服务挂起，并插入异常数据表
     * 2、自动执行主服务列表，同步出错时，节点挂起，并登记主服务执行失败异常信息
     * 3、设置下一步执行路由，设置失败服务挂起
     * 4、记录该节点执行路径，同时30s后调用commit方法进入下一个节点
     * </pre>
     *
     * @
     */
    @Transactional
    public String autoTaskNodeHandler(String taskId, String processDefId, String processInstId, Map<String, Object> variableMap, SysCommHead sysCommHead, FlowElement flowElement) {

        if (log.isInfoEnabled()) {
            log.info("| - WfEvtServiceDefServiceImpl>>>>>当前任务ID[{}]", taskId);
            log.debug("| - WfEvtServiceDefServiceImpl>>>>>当前流程定义ID[{}]", processDefId);
            log.debug("| - WfEvtServiceDefServiceImpl>>>>>当前流程实例ID[{}]", processInstId);
            log.debug("| - WfEvtServiceDefServiceImpl>>>>>业务参数[{}]", variableMap);
            log.info("| - WfEvtServiceDefServiceImpl>>>>>节点id[{}]", flowElement.getId());
            log.info("| - WfEvtServiceDefServiceImpl>>>>>节点名称[{}]", flowElement.getName());
            log.debug("| - WfEvtServiceDefServiceImpl>>>>>报文头[{}]", sysCommHead);
        }
        variableMap.remove(Constants.ROUTE_CONDITION_KEY);

        // 1、根据流程xml配置获取对应的事件触发调用服务接口
        String waittype = processUtil.getTaskProp(processDefId, flowElement, "waitType"); //flowElement.getAttributeValue(Constants.XMLNAMESPACE, "waitType");
        String ownId = processUtil.getTaskProp(processDefId, flowElement, Constants.NODEPROP_REFID);// flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
        WfModelPropExtends prop = wfModelPropExtendsService.selectByOwnIdPropName(ownId, ModelProperties.autoNodeServiceCall.getName());

        if (prop == null || prop.getPropValue() == null) {
            log.info("| - WfEvtServiceDefServiceImpl>>>>>流程扩展属性 {ownId[{}],PropName[autoNodeServiceCall]},找不到对应扩展", ownId);
            throw new ServiceException("找不到对应的服务配置");
        }
        JSONObject json = JSONObject.parseObject(prop.getPropValue());

        String confServiceId = json.getString("serviceId");
        if (StrUtil.isBlank(confServiceId)) {
            confServiceId = json.getString("id");
        }

        // 检查参数是否配置正确
        if (isErrorTaskConfig(taskId, processDefId, processInstId, variableMap, flowElement, waittype, confServiceId)) {
            return null;
        }
        // 调用其他系统的服务，返回所有主服务的调用结果
        this.executeEventService(confServiceId, waittype, taskId, processDefId, processInstId, variableMap, sysCommHead, flowElement);


        // 4、保存并执行下一步
        this.nextTask(taskId, processDefId, processInstId, variableMap, flowElement, waittype, sysCommHead);
        //自动节点的离开事件调用
        WfModelPropExtends prop1 = wfModelPropExtendsService.selectByOwnIdPropName(ownId, ModelProperties.endTaskEvent.getName());
        if (prop1 != null) {
            executeEventService(prop1, taskId, processDefId, processInstId, variableMap, flowElement, sysCommHead);
        }

        return null;
    }

    /**
     * <pre>
     * 实现自动节点的调用
     * 1、检查配置参数是否正确，如果不正确服务挂起，并插入异常数据表
     * 2、自动执行主服务列表，同步出错时，节点挂起，并登记主服务执行失败异常信息
     * 3、设置下一步执行路由，设置失败服务挂起
     * 4、记录该节点执行路径，同时30s后调用commit方法进入下一个节点
     * </pre>
     *
     * @
     */
    public String autoTaskNodeHandler(DelegateTask delegateTask) {
        FlowElement flowElement = delegateTask.getExecution().getCurrentFlowElement();
        Map<String, Object> variableMap = delegateTask.getVariables();
//		// 4、保存并执行下一步
        return this.autoTaskNodeHandler(delegateTask.getId(), delegateTask.getProcessDefinitionId(), delegateTask.getProcessInstanceId(), variableMap, WfVariableUtil.getSysCommHead(), flowElement);
    }

    private boolean isErrorTaskConfig(String taskId, String processDefId, String processInstId, Map<String, Object> variableMap, FlowElement flowElement, String waittype, String confServiceId) {
        String attr = null;
        if (waittype == null) {
            attr = "自动节点未配置[自动节点等待模式]";
        }
        if (confServiceId.isEmpty()) {
            attr = "自动节点未配置[自动节点主服务关联服务]";
        }
        if (attr == null) {
            return false;
        }
        WfExceptionData data = ErrorDataUtil.getTaskAttrError(attr, taskId, confServiceId, processDefId, processInstId, "自动节点", flowElement.getName(), variableMap.toString());
        wfExceptionDataService.insertWfExceptionData(data);
        return true;
    }


    /**
     * 执行下一步
     *
     * @
     */
    private void nextTask(String taskId, String processDefId, String processInstId, Map<String, Object> variableMap, FlowElement flowElement, String waittype, SysCommHead sysCommHead) {
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstId);
        Integer taskRate = 0;

        if (wfInstance == null) {
            taskRate = 1;
            wfInstance = new WfInstance();
            wfInstance.setProcessId(processInstId);
            WfDefinition wfDefinine = cacheDaoSvc.selectActiveFlowByDefId(processInstId.split(":")[0]);
            wfInstance.setProccessName(wfDefinine.getDefName());
            wfInstance.setStartUserId((String) variableMap.get(Constants.BEFOR_USERID));
            wfInstance.setStartUserMap(BpmStringUtil.objectToJsonObject(variableMap.get(Constants.BEFORUSERDATA)).toJSONString());
            wfInstance.setSystemCode((String) variableMap.get(Constants.SYSTEM_CODE_KEY));
        }
        String assginer = configUtil.getAutoTaskActor();
		//1 本次任务自动执行，记录执行痕迹
        WfTaskflow taskflow = new WfTaskflow();
        taskflow.setTaskCode(processUtil.getTaskProp(processDefId, flowElement, Constants.NODEPROP_REFID));
        taskflow.setTaskId(taskId);
        taskflow.setTaskRate(taskRate);
        taskflow.setProcessId(processInstId);
        taskflow.setProcessCode(processDefId);
        taskflow.setTaskDefId(flowElement.getId());
        taskflow.setTaskType(Constants.NODE_TYPE_ATTO);
        LocalDateTime createDate = LocalDateTime.now();
        taskflow.setCreateDate(createDate);
        taskflow.setStartDate(createDate);
        String desc = wfInstance.getProcessInstDesc();
        if (StrUtil.isBlank(desc)) {
            desc = wfInstance.getProcessDesc();
        }
        taskflow.setWfDesc(desc);

        taskflow.setSystemCode(wfInstance.getSystemCode());

        String systemCode = wfInstance.getSystemCode();
        taskflow.setItemFirstCode(systemCode.split(";")[0].split(":")[0]);
        taskflow.setItemSecondCode(systemCode.split(";")[1].split(":")[0]);
        taskflow.setItemSecondName(systemCode.split(";")[1].split(":")[1]);
        taskflow.setWorkCreater(wfInstance.getStartUserId());
        JSONObject startUserMap = JSONObject.parseObject(wfInstance.getStartUserMap());
        if (startUserMap != null) {
            taskflow.setWorkCreaterName((String) startUserMap.get(Constants.USERNAME_KEY));
        }
        Object processCreateTime = variableMap.get(Constants.PROCESS_CREATE_TIME);
        if (processCreateTime != null) {
            taskflow.setStartCreateDate(DateUtil.covertObjToLdt(processCreateTime));
        }
        taskflow.setTaskActorsData("{\"" + assginer + "\":{ \"userName\": \"" + assginer + "\", \"userId\": \"" + assginer + "\" }}");
        taskflow.setProcessName(wfInstance.getProccessName());
        taskflow.setTaskName(flowElement.getName());
        taskflow.setWorkType(1);
        taskflow.setParentTaskId((String) variableMap.get(Constants.BEFOR_TASKID));
        taskflow.setSuperTaskId((String) variableMap.get(Constants.BEFOR_TASKID));
        taskflow.setSystemCode((String) variableMap.get(Constants.SYSTEM_CODE_KEY));
        taskflow.setBussinessMap(BpmStringUtil.mapToJson(variableMap).toString());
        taskflow.setTaskActors(assginer);
        taskflow.setWorkCreaterAgencyId(wfInstance.getOrgId());
        taskflow.setWorkCreaterAgencyName(wfInstance.getOrgName());
        if (variableMap.get(Constants.ROOT_PROCESS_ID) != null) {
            taskflow.setRootProcessId(variableMap.get(Constants.ROOT_PROCESS_ID).toString());
        }
        if (variableMap.get(Constants.ROOT_PROCESS_CODE) != null) {
            taskflow.setRootProcessCode(variableMap.get(Constants.ROOT_PROCESS_CODE).toString());
        }
        if (variableMap.get(Constants.ROOT_PROCESS_NAME) != null) {
            taskflow.setRootProcessName(variableMap.get(Constants.ROOT_PROCESS_NAME).toString());
        }
        //超时任务sysCommHead回为空
        if (sysCommHead != null) {
            String globalSeqno = sysCommHead.getGlobalSeqNo();
            String bussSubFlow = null;
            if (globalSeqno != null && globalSeqno.length() > Constants.BUSS_MAN_FLOW_LEN) {
                bussSubFlow = globalSeqno.substring(Constants.BUSS_MAN_FLOW_LEN);
                globalSeqno = globalSeqno.substring(0, Constants.BUSS_MAN_FLOW_LEN);
            }
            taskflow.setBussManFlow(globalSeqno);
            taskflow.setBussSubFlow(bussSubFlow);
        }

        if (waittype != null && !waittype.equals(Constants.CALL_MODE_ASYNC_WAIT) && !waittype.equals(Constants.CALL_MODE_SYNC_WAIT)) {
            taskflow.setEndDate(createDate);
            taskflow.setUpdateDate(createDate);
            taskflow.setDoTaskActor(assginer);
            taskflow.setDuedata(0L);
            taskflow.setWorkState(Constants.TASK_STATE_END);
            taskflow.setDoResult("A");
            if (variableMap.containsKey(BpmConstants.DO_REMARK)) {
                taskflow.setDoRemark((String) variableMap.get(BpmConstants.DO_REMARK));
            } else {
                taskflow.setDoRemark("自动执行");
            }
            // cmz
            taskflow.setTaskType(Constants.NODE_TYPE_ATTO);
            taskflow.setTaskActors(configUtil.getAutoTaskActor());
            wfTaskflowService.insertWfTaskflow(taskflow);
            WfBusinessData businessData = new WfBusinessData(taskflow);
            wfBusinessDataService.insertWfBusinessData(businessData);
            variableMap.put(Constants.BEFOR_TASKID, taskflow.getTaskId());
            variableMap.remove(Constants.ROUTE_CONDITION_KEY);
            WfVariableUtil.setVariableMap(variableMap);

            boolean isRoute;
            try {
                isRoute = wfCommService.forkRouteSelect(taskflow.getProcessCode(), flowElement, variableMap);
                //提前判断下路由分支是否满足
                if (isRoute) {
                    SequenceFlowCadition sequenceFlowCadition = SpringUtil.getBean(SequenceFlowCadition.class);
                    sequenceFlowCadition.mathcCaditionCheck(variableMap, flowElement);
                }
                activitiBaseService.commitTask(taskId, new HashMap<String, Object>());
                WfVariableUtil.setVariableMap(variableMap);
            } catch (Exception e) {
                e.printStackTrace();
                throw new ServiceException("自动执行下一步出错了啊");
            }


        } else {
            taskflow.setWorkState(Constants.TASK_STATE_RUNING);
            variableMap.put(Constants.AUTO_USER_ID, configUtil.getAutoTaskActor());
            WfVariableUtil.setVariableMap(variableMap);
            taskflow.setDoResult(null);
            taskflow.setTaskType(Constants.NODE_TYPE_ATTO);
            wfTaskflowService.insertWfTaskflow(taskflow);
            WfBusinessData businessData = new WfBusinessData(taskflow);
            wfBusinessDataService.insertWfBusinessData(businessData);
        }
        //设置参数
        if (log.isInfoEnabled()) {
            log.info("| - WfEvtServiceDefServiceImpl>>>>>自动节点执行[taskId:{}]ing...", taskId);
        }

    }

    /**
     * 第三方服务调用的统一入口
     *
     * @return
     * @
     */
    public void executeEventService(JSONArray array,
                                    String waittype, String taskId, String processDefId, String processInstId, Map<String, Object> variableMap, FlowElement flowElement) {
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonobj = array.getJSONObject(i);
//			String id = jsonobj.getString("id");
            String serviceId = jsonobj.getString("serviceId");
            if (StrUtil.isBlank(serviceId)) {
                serviceId = jsonobj.getString("id");
            }
            this.executeEventService(serviceId, waittype, taskId, processDefId, processInstId, variableMap, WfVariableUtil.getSysCommHead(), flowElement);
        }


    }


    /**
     * 第三方服务调用的统一入口
     *
     * @return
     * @
     */
    public void executeEventService(String serviceId,
                                    String waittype, String taskId, String processDefId, String processInstId, Map<String, Object> variableMap, SysCommHead sysCommHead, FlowElement flowElement) {

        WfEvtServiceDef wfEvtServiceDef = selectByServiceId(serviceId);
        if (!StrUtil.isEmpty(waittype)) {
            wfEvtServiceDef.setCallMode(waittype);
        }

        String ownId = processUtil.getTaskProp(processDefId, flowElement, Constants.NODEPROP_REFID);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
        WfModelPropExtends prop = wfModelPropExtendsService.selectByOwnIdPropName(ownId, ModelProperties.autoNodeServiceCall.getName());

        Map<String, Object> eventDefMap = new HashMap<String, Object>();
        eventDefMap.put("processCode", processDefId.split(":")[0]);
        eventDefMap.put("nodeCode", prop.getOwnId());
        eventDefMap.put("eventCode", Constants.EVENT_T1);
        eventDefMap.put("taskIdCode", taskId);

        if (wfEvtServiceDef.getCallMode().equals(Constants.CALL_MODE_ASYNC_WAIT)) {
            WfVariableUtil.putAutoLocal("eventDefMap", eventDefMap);
            WfVariableUtil.putAutoLocal("variableMap", variableMap);
            WfVariableUtil.putAutoLocal("taskId", taskId);
            WfVariableUtil.putAutoLocal("processDefId", processDefId);
            WfVariableUtil.putAutoLocal("processInstId", processInstId);
            WfVariableUtil.putAutoLocal("taskName", flowElement.getName());
            WfVariableUtil.putAutoLocal("wfEvtServiceDef", wfEvtServiceDef);
        } else {
            // 2019-10-21 modify by amt end
            // 去掉线程变量操作，把自动节点的异步的调用需要用到的参数都放到线程变量中，事务提交后，调用自动节点服务

            EventServiceInvockUtil instance;
            try {
                instance = new EventServiceInvockUtil().newEventServiceInstance(wfEvtServiceDef,
                        variableMap, eventDefMap, taskId, processDefId, processInstId, "auto", flowElement.getName(), sysCommHead);
                WfEvtServiceLog log1 = instance.newEvtServiceLog(processInstId);
                log1.setNote(flowElement.getName());
                log1.setProcExtId(processDefId);
                log1.setTaskId(taskId);
                if (log.isDebugEnabled()) {
                    log.debug(log1.toString());
                }
                wfEvtServiceLogService.insertWfEvtServiceLog(log1);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }


    /**
     *
     */
    @Transactional
    public void executeEventService(WfModelPropExtends wfModelPropExtends, String taskId, String processDefId, String processInstId,
                                    Map<String, Object> variableMap, FlowElement flowElement, SysCommHead sysCommonHead) {
        String nodeTypePara = null;
        String nodeNamePara = null;
        if (flowElement != null) {
//			nodetype=flowElement.processUtil.getTaskProp(flowElement, Constants.NODETYPE);//();
            if (processUtil.getTaskProp(processDefId, flowElement, Constants.NODETYPE) != null) {
                nodeTypePara = processUtil.getTaskProp(processDefId, flowElement, Constants.NODETYPE);// flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
            }
            nodeNamePara = flowElement.getName();
        }
        executeEventService(wfModelPropExtends, taskId, processDefId,
                processInstId, variableMap, nodeTypePara, nodeNamePara,
                sysCommonHead);
    }

    public void executeEventService(WfModelPropExtends wfModelPropExtends, String taskId, String processDefId,
                                    String processInstId, Map<String, Object> variableMap, String nodeTypePara, String nodeNamePara,
                                    SysCommHead sysCommonHead) {
        if (wfModelPropExtends == null) {
            return;
        }
//		String systemCode ="";
        Map<String, Object> eventDefMap = new HashMap<String, Object>();
        eventDefMap.put("processCode", processDefId.split(":")[0]);
        eventDefMap.put("nodeCode", wfModelPropExtends.getOwnId());
        eventDefMap.put("processId", processInstId);
        String nodetype = "";
        String nodeName = "";
        if (wfModelPropExtends.getPropName().equals(ModelProperties.flowStartEvent.getName())) {
            nodeName = "开始节点";
            nodetype = "startEvent";
            eventDefMap.put("eventCode", Constants.EVENT_P1);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.flowEndEvent.getName())) {
            nodeName = "正常结束";
            nodetype = "endEvent";
            eventDefMap.put("eventCode", Constants.EVENT_P2);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.flowEndErrorEvent.getName())) {
            nodeName = "异常结束";
            nodetype = "endEvent";
            eventDefMap.put("eventCode", Constants.EVENT_P3);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.hangUpEvent.getName())) {
//			nodeName=wfModelPropExtends.getOwnId();
//			nodetype="hangUpEvent";
            eventDefMap.put("eventCode", Constants.EVENT_P4);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.recoverEvent.getName())) {
//			nodeName=wfModelPropExtends.getOwnId();
//			nodetype="recoverEvent";
            eventDefMap.put("eventCode", Constants.EVENT_P5);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.cancelEvent.getName())) {
            eventDefMap.put("eventCode", Constants.EVENT_P6);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.startTaskEvent.getName())) {
            eventDefMap.put("eventCode", Constants.EVENT_T1);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.endTaskEvent.getName())) {
            eventDefMap.put("eventCode", Constants.EVENT_T2);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.backTaskEvent.getName())) {
            eventDefMap.put("eventCode", Constants.EVENT_T3);
        }
        if (wfModelPropExtends.getPropName().equals(ModelProperties.recallTaskEvent.getName())) {
            eventDefMap.put("eventCode", Constants.EVENT_T4);
        }
        eventDefMap.put("taskIdCode", taskId);
        eventDefMap.put("processInstanceId", processInstId);

        //没有配置事件服务，直接返回
        String propvalue = modelPropService.getNomalModelPropValue(wfModelPropExtends);
        if (StrUtil.isBlank(propvalue)) {
            return;
        }
        if (log.isInfoEnabled()) {
            log.info("| - WfEvtServiceDefServiceImpl>>>>>处理外部事件回调 >>> ownId:{},propName:{},taskId:{},processDefId:{},processInstId:{}", wfModelPropExtends.getOwnId(), wfModelPropExtends.getPropName(), taskId, processDefId, processInstId);
        }

        if (log.isInfoEnabled()) {
            log.info("| - WfEvtServiceDefServiceImpl>>>>>propvalue:{}", propvalue);
        }
//		JSONObject obj=JSONObject.fromObject(propvalue);
        //暂时只支持脚本判断,如果不是js脚本判断进行返回
//		if(StrUtil.isEmpty(obj.getString("key"))||!obj.getString("key").equals(Constants.FILED_TYPE_JS)) return ;
        Map<String, List<EventNodeProp>> eventCountMap = new HashMap<>();
        //2、解析参数，解析成list对象。
//		List<EventNodeProp> list=new ArrayList<EventNodeProp>();
        JSONArray array = JSONArray.parseArray(propvalue);
        if (ObjectUtil.isNotEmpty(array)) {
            try {
                for (int i = 0; i < array.size(); i++) {
                    JSONObject obj1 = array.getJSONObject(i);
                    int orderNo = obj1.getString("order") == null || "null".equals(obj1.getString("order")) ? 0 : Integer.parseInt(obj1.getString("order"));
                    String scriptEval = obj1.getString("callCondition");
                    String callType = obj1.getString("callMode");
                    String serviceId = obj1.getString("serviceId");
                    if (StrUtil.isBlank(serviceId)) {
                        serviceId = obj1.getString("id");
                    }
                    EventNodeProp propnode = new EventNodeProp(orderNo, scriptEval, callType, serviceId);
                    if (StrUtil.isEmpty(propnode.getScriptEval()) || "null".equals(propnode.getScriptEval()) || "true".equals(wfCommService.executeJavaScript(propnode.getScriptEval(), variableMap))) {
                        if (eventCountMap.containsKey(orderNo + "")) {
                            List<EventNodeProp> list1 = eventCountMap.get(orderNo + "");
                            list1.add(propnode);

                        } else {
                            List<EventNodeProp> list1 = new ArrayList<>();
                            list1.add(propnode);
                            eventCountMap.put(orderNo + "", list1);
                        }
                    }
                }
            } catch (Exception e) {
                log.error("| - WfEvtServiceDefServiceImpl>>>>>参数不合法，转换失败", e);
                return;
            }

            //按照事件的orderNo排序，顺序执行事件服务
            List<Map.Entry<String, List<EventNodeProp>>> list2 = new ArrayList<Map.Entry<String, List<EventNodeProp>>>(eventCountMap.entrySet());
            Collections.sort(list2, new Comparator<Map.Entry<String, List<EventNodeProp>>>() {
                //升序排序
                @Override
                public int compare(Map.Entry<String, List<EventNodeProp>> o1,
                                   Map.Entry<String, List<EventNodeProp>> o2) {
                    return o1.getKey().compareTo(o2.getKey());
                }
            });

            if (log.isInfoEnabled()) {
                log.info("| - WfEvtServiceDefServiceImpl>>>>>list2:{}", list2.toString());
            }

            if (!StrUtil.isEmpty(nodeTypePara)) {
                nodetype = nodeTypePara;
            }
            if (!StrUtil.isEmpty(nodeNamePara)) {
                nodeName = nodeNamePara;
            }

            for (Map.Entry<String, List<EventNodeProp>> mapping : list2) {
                invockEvent(mapping.getValue(), variableMap, eventDefMap,
                        taskId, processDefId, processInstId, nodetype, nodeName, sysCommonHead);
            }


        }

    }

    /**
     * 回调服务
     *
     * @param list
     * @param variableMap
     * @param eventDefMap
     * @param taskId
     * @param processDefId
     * @param processInstId
     * @param nodeType
     * @param nodeName
     * @param sysCommHead
     */
    public void invockEvent(List<EventNodeProp> list, Map<String, Object> variableMap, Map<String, Object> eventDefMap, String taskId, String processDefId, String processInstId, String nodeType, String nodeName, SysCommHead sysCommHead) {
        if (list != null && list.size() == 1) {

            EventNodeProp propnode = list.get(0);
            String serviceId = propnode.getEvtServiceId();
            log.info("| - WfEvtServiceDefService>>>>>顺序调用：serviceId:{},taskId:{},nodeName:{}", serviceId, taskId, nodeName);
            WfEvtServiceDef wfEvtServiceDef = selectByServiceId(serviceId);
            //wfEvtServiceDefMapper.selectById(new BigInteger(propnode.getEvtId()));
            try {                                                                                                            // WfEvtServiceDef();
//				wfEvtServiceDef.setId(new BigInteger(propnode.getEvtId()));
                wfEvtServiceDef.setServiceId(serviceId);
                wfEvtServiceDef.setCallMode(propnode.getCallType());

                EventServiceInvockUtil instance = new EventServiceInvockUtil().newEventServiceInstance(wfEvtServiceDef,
                        variableMap, eventDefMap, taskId, processDefId, processInstId, nodeType, nodeName, sysCommHead);
                WfEvtServiceLog log1 = instance.newEvtServiceLog(processInstId);
                log1.setProcExtId(processDefId);
                log1.setTaskId(taskId);
                wfEvtServiceLogService.insertWfEvtServiceLog(log1);
            } catch (ServiceException e) {
                log.error("| - WfEvtServiceDefService>>>>>{}.{}服务调用异常，【{}】", nodeName, wfEvtServiceDef.getName(), e.getMessage());
                if (propnode.getCallType().equals(Constants.CALL_MODE_SYNC)) {
                    throw e;
                }
            } catch (Exception e) {
                log.error("| - WfEvtServiceDefService>>>>>{}.{}服务调用异常，【{}】", nodeName, wfEvtServiceDef.getName(), e.getMessage());
                if (propnode.getCallType().equals(Constants.CALL_MODE_SYNC)) {
                    throw new ServiceException(nodeName + "服务【" + wfEvtServiceDef.getName() + "】调用异常!" + e.getMessage());
                }
            }
        } else {
            int count = list.size();
            log.info("| - WfEvtServiceDefService>>>>>顺序调用：count->{},taskId->{},nodeName->{}", list.toString(), taskId, nodeName);

            ExecutorService executorpool = Executors.newFixedThreadPool(count);

            final CountDownLatch endGate = new CountDownLatch(count);

            for (int i = 0; i < count; i++) {

                EventNodeProp propnode = list.get(i);
                String serviceId = propnode.getEvtServiceId();
                WfEvtServiceDef wfEvtServiceDef = selectByServiceId(serviceId);
                try {
                    wfEvtServiceDef.setCallMode(propnode.getCallType());
                    LatchInvokeEventTask task = new LatchInvokeEventTask(endGate, wfEvtServiceLogService, wfEvtServiceDef,
                            variableMap, eventDefMap, taskId, processDefId, processInstId, nodeType, nodeName, sysCommHead);
                    executorpool.execute(task);
                } catch (ServiceException e) {
                    log.error("| - WfEvtServiceDefService>>>>>{}.{}服务调用异常，【{}】", nodeName, wfEvtServiceDef.getName(), e.getMessage());
                    if (propnode.getCallType().equals(Constants.CALL_MODE_SYNC)) {
                        throw e;
                    }
                } catch (Exception e) {
                    log.error("| - WfEvtServiceDefService>>>>>{}.{}服务调用异常，【{}】", nodeName, wfEvtServiceDef.getName(), e.getMessage());
                    if (propnode.getCallType().equals(Constants.CALL_MODE_SYNC)) {
                        throw new ServiceException(nodeName + "服务【" + wfEvtServiceDef.getName() + "】调用异常!" + e.getMessage());
                    }
                }
            }
            try {
                endGate.await();
            } catch (InterruptedException e) {
                log.error("| - WfEvtServiceDefService>>>>>线程结束失败！", e);
                throw new ServiceException(nodeName + "服务调用异常");
            }
            log.info("| - WfEvtServiceDefService>>>>>顺序调用完成：count->{},taskId->{},nodeName->{}", list.toString(), taskId, nodeName);

        }
    }

}
