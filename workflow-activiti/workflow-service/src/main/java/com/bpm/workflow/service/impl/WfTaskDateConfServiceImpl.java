package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfTaskDateConf;
import com.bpm.workflow.mapper.WfTaskDateConfMapper;
import com.bpm.workflow.service.WfTaskDateConfService;
import com.bpm.workflow.util.ConfigUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfTaskDateConfServiceImpl extends ServiceImpl<WfTaskDateConfMapper, WfTaskDateConf> implements WfTaskDateConfService {
    @Resource
    private WfTaskDateConfMapper wfTaskDateConfMapper;

    @Transactional
    public void insert(WfTaskDateConf wfTaskDateConf) {
        wfTaskDateConf.setNodeId(ConfigUtil.getNodeId());
        wfTaskDateConfMapper.insert(wfTaskDateConf);
    }

    @Transactional
    public void delete(WfTaskDateConf wfTaskDateConf) {
        LambdaQueryWrapper<WfTaskDateConf> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WfTaskDateConf::getConfId, wfTaskDateConf.getConfId());
        wfTaskDateConfMapper.delete(wrapper);
    }

    @Transactional
    public void update(WfTaskDateConf wfTaskDateConf) {
        wfTaskDateConfMapper.updateById(wfTaskDateConf);
    }

    public List<WfTaskDateConf> selectByNodeId(String nodeId) {
        LambdaQueryWrapper<WfTaskDateConf> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WfTaskDateConf::getNodeId, nodeId);
        List<WfTaskDateConf> selectList = wfTaskDateConfMapper.selectList(wrapper);
        return selectList;
    }
}
