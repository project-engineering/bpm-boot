package com.bpm.workflow.util;

import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.exception.ServiceException;
import lombok.extern.log4j.Log4j2;

import java.util.HashSet;
import java.util.Set;

@Log4j2
public class NodeInfo {
	private String nodeId;
	public NodeInfo(String nodeId, boolean isGatewayNode) {
		this.nodeId = nodeId;
		this.isGatewayNode = isGatewayNode;
	}
	public String getNodeId() {
		return nodeId;
	}

	private boolean isGatewayNode;

	
	public boolean isStartGatewayNode() {
		if(!isGatewayNode()) {
			return false;
		}
		if(beforeNodes.size()>1&&afterNodes.size()==1) {
			return false;
		}else if(beforeNodes.size()==1&&afterNodes.size()>1) {
			return true;
		}else {
			log.info("| - NodeInfo>>>>>gateway node format error,can't determine it is a fork node or a join node");
			// 网关节点格式异常，无法判定是汇聚网关节点还是分支网关节点
			throw new ServiceException(ErrorCode.WF000013);
		}
	}
	public boolean isGatewayNode() {
		return isGatewayNode;
	}

	private String belongStartGatewayNodeId;
	private String belongEndGatewayNodeId;
	
	
	public String getBelongStartGatewayNodeId() {
		return belongStartGatewayNodeId;
	}
	public void setBelongStartGatewayNodeId(String belongStartGatewayNodeId) {
		this.belongStartGatewayNodeId = belongStartGatewayNodeId;
	}
	public String getBelongEndGatewayNodeId() {
		return belongEndGatewayNodeId;
	}
	public void setBelongEndGatewayNodeId(String belongEndGatewayNodeId) {
		this.belongEndGatewayNodeId = belongEndGatewayNodeId;
	}

	private Set<String> beforeLoopBackNodes=null;
	private Set<String> afterLoopBackNodes=null;
	public void addNextLoopBackNodeId(String toId) {
		if(afterLoopBackNodes == null) {
			afterLoopBackNodes = new HashSet<String>();
		}	
		afterLoopBackNodes.add(toId);		
	}
	public void addBeforeLoopBackNodeId(String toId) {
		if(beforeLoopBackNodes == null) {
			beforeLoopBackNodes = new HashSet<String>();
		}	
		beforeLoopBackNodes.add(toId);		
	}
	
	private Set<String> beforeNodes=null;
	private Set<String> afterNodes=null;
	public void addNextNodeId(String toId) {
		if(afterNodes == null) {
			afterNodes = new HashSet<String>();
		}	
		afterNodes.add(toId);		
	}
	public void addBeforeNodeId(String toId) {
		if(beforeNodes == null) {
			beforeNodes = new HashSet<String>();
		}	
		beforeNodes.add(toId);		
	}
	
    public Set<String> getNeighborNodes(boolean isBefore) {
    	Set<String> neighborNodes = null;
    	if(isBefore) {
    		neighborNodes = beforeNodes;
    	}else {
    		neighborNodes = afterNodes;
    	}
    	return neighborNodes;
    }
    public Set<String> getLoopNeighborNodes(boolean isBefore) {
    	Set<String> neighborNodes = null;
    	if(isBefore) {
    		neighborNodes = beforeLoopBackNodes;
    	}else {
    		neighborNodes = afterLoopBackNodes;
    	}
    	return neighborNodes;
    }
    public boolean hasNeighborNode(boolean isBefore) {
    	Set<String> neighborNodes = getNeighborNodes(isBefore);
    	if(neighborNodes==null || neighborNodes.size()==0) {
    		return false;
    	}else {
    		return true;
    	}
    }
    public String getFirstNonLoopNeighborNodes(boolean isBefore) {
    	Set<String> neighborNodes = getNeighborNodes(isBefore);
    	Set<String> loopBackNeighborNodes = getLoopNeighborNodes(isBefore);
    	if(neighborNodes == null || neighborNodes.size() == 0) {
    		return null;
    	}else {
    		for(String id:neighborNodes) {
    			if(loopBackNeighborNodes!=null && loopBackNeighborNodes.contains(id)) {
    				continue;
    			}else {
    				return id;
    			}
    		}
    		return null;
    	}
    }
    

    private int gatewayJoinCount;
    public int getGatewayJoinCount() {
    	if(!isGatewayNode) {
    		return 0;
    	}
    	return gatewayJoinCount;
    }
    public void setGatewayJoinCount(int gatewayJoinCount) {
    	this.gatewayJoinCount = gatewayJoinCount;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("================NodeInfo====================");
    	sb.append("id:").append(nodeId);
    	sb.append(" belongStartGatewayNodeId:").append(belongStartGatewayNodeId);
    	sb.append(" belongEndGatewayNodeId:").append(belongEndGatewayNodeId);
    	sb.append(" beforeNodes:").append(beforeNodes);
    	sb.append(" afterNodes:").append(afterNodes);
    	return sb.toString();
    }
}
