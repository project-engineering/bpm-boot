package com.bpm.workflow.client;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.vo.UserAskListQueryRequest;
import com.bpm.workflow.vo.WorkflowCallBackRequest;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.List;

/**
 * 工作流Feign客户端
 */
@Service
@Slf4j
public class WorkflowFeignClientService {

    @Resource
    private RestTemplate restTemplate;
    
    /**
     * @Description: 工作流选人服务
     * @Param: [userAskListQueryRequest]
     * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.dto.UserOutputData>
     */
    public List<UserOutputData> findUserInfo(UserAskListQueryRequest userAskListQueryRequest){
        if(ObjectUtil.isEmpty(userAskListQueryRequest.getUserAskList())){
            throw new ServiceException("选人列表为空！");
        }
//        try {
//            log.info("findUserInfo input:{}", JSONUtil.toJsonStr(userAskListQueryRequest));
//            Result<UserAskQueryResponse> userInfoResult = iEmployeeHttpService.findUserInfo(userAskListQueryRequest);
//            log.info("findUserInfo output:{}", JSONUtil.toJsonStr(userInfoResult));
//
//            if(userInfoResult.isSuccess()){
//                if(ObjectUtil.isNotEmpty(userInfoResult.getData())) {
//                    List<UserAskQueryResponse> userList = (List<UserAskQueryResponse>) userInfoResult.getData();
//                    return userList.stream().map(userInfo -> {
//                        UserOutputData userOutputData = new UserOutputData();
//                        userOutputData.setUserId(userInfo.getUserId());
//                        userOutputData.setUserName(userInfo.getUserName());
//                        userOutputData.setUserBank(userInfo.getUserBank());
//                        userOutputData.setUserBankName(userInfo.getUserBankName());
//                        userOutputData.setUserPhone(userInfo.getPhone());
//                        return userOutputData;
//                    }).collect(Collectors.toList());
//                }
//            }else {
//                log.error("findUserInfo is error, code:{}, message:{}", userInfoResult.getCode(), userInfoResult.getMessage());
//                throw new ServiceException(ErrorCode.WF001003, userInfoResult.getMessage());
//            }
//        }catch (ServiceException e) {
//            throw e;
//        }catch (Exception e) {
//            log.error("iEmployeeHttpService.findUserInfo is error", e);
//            throw new ServiceException(ErrorCode.WF001001, e);
//        }
        return null;
    }
    
    /**
     * @Description: 工作流回调服务
     * @Param: [url, workflowCallBackRequest]
     * @Return: void
     */
    public void callBack(String url, WorkflowCallBackRequest workflowCallBackRequest){
        if(StrUtil.isNotEmpty(url)) {
            try{
                log.info("callback url is {},  param is {}", url, JSONUtil.toJsonStr(workflowCallBackRequest));
                Result result = restTemplate.postForObject(url, workflowCallBackRequest, Result.class);
                log.info("callback url is {}, result is {}", url, JSONUtil.toJsonStr(result));
                if (result != null && !result.isSuccess()) {
                    throw new ServiceException(ErrorCode.WF001003, result.getMessage());
                }
            }catch(ServiceException e) {
                throw e;
            }catch (Exception e) {
                log.error("callback url:【{}】 is error", url, e);
                throw new ServiceException(ErrorCode.WF001002, e);
            }
        }else{
            throw new ServiceException(ErrorCode.WF000035);
        }
    }
}
