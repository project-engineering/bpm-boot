package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfViewTaskLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfViewTaskLogService extends IService<WfViewTaskLog> {

    WfViewTaskLog selectByTaskIdUserId(WfViewTaskLog wfViewTaskLog);

    void insertWfViewTaskLog(WfViewTaskLog wfViewTaskLog);

    void updateByTaskIdUserId(WfViewTaskLog log);
}
