package com.bpm.workflow.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowDefinitionData;
import com.bpm.workflow.entity.WfDefinePropExtends;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfDefinitionMapper;
import com.bpm.workflow.service.CacheDaoSvc;
import com.bpm.workflow.service.WfDefinitionService;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.WfDefinePropExtendsService;
import com.bpm.workflow.util.ConfigUtil;
import com.bpm.workflow.util.XmlConvertResult;
import com.bpm.workflow.util.XmlUtil;
import com.bpm.workflow.vo.DefinitionQueryRequest;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 流程定义表服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Log4j2
@Service
public class WfDefinitionServiceImpl extends ServiceImpl<WfDefinitionMapper, WfDefinition> implements WfDefinitionService {
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private ActivitiBaseService activitiBaseService;
    @Resource
    private WfDefinitionMapper definitionMapper;
    @Resource
    private ProcessEngine processEngine;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    @Resource
    private WfDefinePropExtendsService wfDefinePropExtendsService;

    @Transactional
    public void addWorkFlow(WorkFlowDefinitionData wfDefinitionData) {
        // 先检查必输项
        checkDataProp(wfDefinitionData);
        // 检查重复
        WfDefinition wfDefinition = new WfDefinition();
        try {
            BeanUtil.copyProperties(wfDefinitionData, wfDefinition);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("复制流程定义出错，请查看！");
        }
        wfDefinition.setDefVersion(0);
        wfDefinition.setPublicFlag("0");
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId, wfDefinition.getDefId());
        queryWrapper.eq(WfDefinition::getDefVersion, 0);
        queryWrapper.eq(WfDefinition::getPublicFlag, "0");
        WfDefinition oldDefinition = definitionMapper.selectOne(queryWrapper);
        if (oldDefinition != null) {
            throw new ServiceException("流程定义已经存在，请查看！");
        }

        // xml定义文件第一次创建需要拼接
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        strBuf.append(
                "<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
        strBuf.append(
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:activiti=\"http://activiti.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" ");
        strBuf.append(
                "xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" ");
        strBuf.append(
                "expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://www.activiti.org/test\"> \n");

        strBuf.append("  <process id=\"" + wfDefinitionData.getDefId() + "\" name=\"" + wfDefinitionData.getDefName() + "\" isExecutable=\"true\"> \n");
        strBuf.append("    <extensionElements>\n");
        strBuf.append("      <activiti:executionListener event=\"start\" class=\"com.workflow.activiti.engine.listener.ExecutionStartListener\"></activiti:executionListener>\n");
        strBuf.append("      <activiti:executionListener event=\"end\" class=\"com.workflow.activiti.engine.listener.ExecutionEndListener\"></activiti:executionListener>\n");
        strBuf.append("    </extensionElements>\n");
        strBuf.append("    <startEvent id=\"startEvent1\" name=\"开始事件\">\n");
        strBuf.append("      <extensionElements>\n");
        strBuf.append("          <activiti:executionListener event=\"start\" class=\"com.workflow.activiti.engine.listener.StartNodeStartListener\"></activiti:executionListener>");
        strBuf.append("      </extensionElements>\n");
        strBuf.append("    </startEvent>\n");
        strBuf.append("  </process>\n");

        strBuf.append("  <bpmndi:BPMNDiagram id=\"BPMNDiagram_" + wfDefinitionData.getDefId() + "\"> \n");
        strBuf.append("    <bpmndi:BPMNPlane bpmnElement=\"" + wfDefinitionData.getDefId() + "\" id=\"BPMNPlane_" + wfDefinitionData.getDefId() + "\"></bpmndi:BPMNPlane> \n");
        strBuf.append("    <bpmndi:BPMNShape bpmnElement=\"startEvent1\" id=\"BPMNShape_startEvent1\">\n");
        strBuf.append("      <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"90.0\" y=\"150.0\"></omgdc:Bounds>\n");
        strBuf.append("    </bpmndi:BPMNShape>\n");
        strBuf.append("  </bpmndi:BPMNDiagram>\n");
        strBuf.append("</definitions>");

        wfDefinition.setXmlSource(strBuf.toString());
        wfDefinition.setParentId(wfDefinitionData.getParentId());
        cacheDaoSvc.insertWfDefinition(wfDefinition);
    }

    @Override
    public WfDefinition selectByDefIdVersion(WfDefinition wfDefinition) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId, wfDefinition.getDefId());
        if (UtilValidate.isNotEmpty(wfDefinition.getDefVersion())) {
            queryWrapper.eq(WfDefinition::getDefVersion, wfDefinition.getDefVersion());
        }
        if (UtilValidate.isNotEmpty(wfDefinition.getVersion())) {
            queryWrapper.eq(WfDefinition::getVersion, wfDefinition.getVersion());
        }
        WfDefinition definition = definitionMapper.selectOne(queryWrapper);
        if (definition != null) {
            return definition;
        }
        return null;
    }

    @Override
    public List<WfDefinition> selectByDefId(String processDefId) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId, processDefId);
        List<WfDefinition> definitionList = definitionMapper.selectList(queryWrapper);
        if (definitionList != null && definitionList.size() > 0) {
            return definitionList;
        }
        return null;
    }

    @Override
    public WfDefinition selectActiveFlowByDefId(String processCode) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId, processCode);
        queryWrapper.eq(WfDefinition::getPublicFlag, "0");
        WfDefinition definition = definitionMapper.selectOne(queryWrapper);
        if (definition != null) {
            return definition;
        }
        return null;
    }

    @Override
    public int updateByDefId(WfDefinition wfDefinition) {
        LambdaUpdateWrapper<WfDefinition> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(WfDefinition::getDefId,wfDefinition.getDefId());
        return definitionMapper.update(wfDefinition,updateWrapper);
    }

    @Override
    public int deleteByDefIdDefName(WfDefinition wfDefinition) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId,wfDefinition.getDefId());
        queryWrapper.eq(WfDefinition::getDefName,wfDefinition.getDefName());
        return definitionMapper.delete(queryWrapper);
    }

    @Override
    public int deleteByDefId(String defId) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId,defId);
        return definitionMapper.delete(queryWrapper);
    }

    @Override
    public int updateByDefIdDefName(WfDefinition wfDefinition) {
        LambdaUpdateWrapper<WfDefinition> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(WfDefinition::getDefId,wfDefinition.getDefId());
        updateWrapper.eq(WfDefinition::getDefName,wfDefinition.getDefName());
        return definitionMapper.update(wfDefinition,updateWrapper);
    }

    @Override
    public void insertWfDefinition(WfDefinition wfDefinition) {
        definitionMapper.insert(wfDefinition);
    }

    @Override
    public int updateByKeyId(WfDefinition wfDefinition) {
        return definitionMapper.updateById(wfDefinition);
    }

    @Override
    @Transactional
    public void deleteWorkFlow(int id, String defId)  {
        // 只要有一个发布版本就不允许删除
        if (StrUtil.isBlank(defId)) {
            WfDefinition wfDefinition = definitionMapper.selectById(id);
            defId = wfDefinition.getDefId();
        }
        wfModelPropExtendsService.deleteModelPropExtendByNodeId(defId);
//        cacheDaoSvc.deleteByDefId(defId);

        //卸载activiti中的流程定义资源
        List<ProcessDefinition> resultList = activitiBaseService.findProcessDefinitions(defId);
        if (resultList == null || resultList.size() < 1) {
            return ;
        }
        for(ProcessDefinition pdf :resultList){
            activitiBaseService.deleteWorkflow(pdf.getDeploymentId(), true);
        }
    }

    @Transactional
    @Override
    public void updateWorkFlow(WorkFlowDefinitionData workFlowDefinitionData)  {
        // 检查必输项
        checkDataProp(workFlowDefinitionData);

        // 修改属性
        WfDefinition wfDefinition = new WfDefinition();
        try {
            BeanUtil.copyProperties(workFlowDefinitionData, wfDefinition);
            cacheDaoSvc.updateByKeyId(wfDefinition);
        } catch (Exception e) {
            log.error("更新流程定义失败,原因：{}",e);
            throw new ServiceException("复制流程定义出错，请查看！");
        }

    }

    /**
     * 查看某一模块的流程定义列表
     *
     * @param definitionQueryRequest
     * @return
     * @
     */
    @Transactional
    @Override
    public  IPage<WfDefinition> queryWorkFlowList(DefinitionQueryRequest definitionQueryRequest)   {
        //构造分页对象
        IPage<WfDefinition> page = new Page<>(definitionQueryRequest.getStart(),definitionQueryRequest.getLimit());
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(definitionQueryRequest.getParentId())) {
            queryWrapper.eq(WfDefinition::getParentId,definitionQueryRequest.getParentId());
        }
        if (UtilValidate.isNotEmpty(definitionQueryRequest.getDefId())) {
            queryWrapper.eq(WfDefinition::getDefId,definitionQueryRequest.getDefId());
        }
        if (UtilValidate.isNotEmpty(definitionQueryRequest.getDefName())) {
            queryWrapper.like(WfDefinition::getDefName,definitionQueryRequest.getDefName());
        }
        if (UtilValidate.isNotEmpty(definitionQueryRequest.getDefVersion())) {
            queryWrapper.eq(WfDefinition::getDefVersion,definitionQueryRequest.getDefVersion());
        }
        queryWrapper.orderByDesc(WfDefinition::getId);
        IPage<WfDefinition> list = definitionMapper.selectPage(page,queryWrapper);
        return list;
    }

    /**
     * 查看指定流程定义
     * @param wfDefinitionData
     * @return
     */
    @Override
    public WfDefinition viewWorkFlow(WorkFlowDefinitionData wfDefinitionData)  {
        WfDefinition wdDefinition = definitionMapper.selectById(wfDefinitionData.getId().intValue());
        return wdDefinition;
    }

    /**
     * 保存流程定义的内容（来源于设计器）
     * @param workFlowDefData
     */
    @Override
    public boolean saveWorkFlowDefinition(WorkFlowDefinitionData workFlowDefData)  {
        // 检查输入内容是否为空
        if (StrUtil.isBlank(workFlowDefData.getDefId())) {
            throw new ServiceException("指定的流程标示错误!!");
        }

        // 根据Id查询持久化对象
        WfDefinition definition = new WfDefinition();
        definition.setDefId(workFlowDefData.getDefId());
        definition.setPublicFlag("0");
        definition.setDefVersion(0);
        definition = selectByDefIdVersion(definition);
        if (definition == null) {
            throw new ServiceException("流程定义信息不存在！！！");
        }

        log.info(workFlowDefData.getXmlSource());
        log.info(workFlowDefData.getJsonSource());

        String xmlSource = replaceXmlSources(workFlowDefData.getXmlSource());

        // 更新数据库的数据
        definition.setXmlSource(xmlSource);
        definition.setJsonSource(workFlowDefData.getJsonSource());
        cacheDaoSvc.updateByKeyId(definition);
        return true;
    }

    @Override
    public void deployWorkFlowByDefId(String defId)  {
        if (StrUtil.isEmpty(defId)) {
            throw new ServiceException(ErrorCode.WF000001);
        }
        WfDefinition theDefinition = cacheDaoSvc.selectActiveFlowByDefId(defId);
        deployWorkflowByEntity(theDefinition);
    }

    @Override
    public List<WfDefinition> selectVersionList(String defId, PageData<Object> pageData) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId,defId);
        Page<WfDefinition> page = new Page<>(pageData.getStartRows(),pageData.getLimit());
        return definitionMapper.selectPage(page,queryWrapper).getRecords();
    }

    /**
     * 历史版本回退
     * @param wfDefinitionData
     */
    @Override
    public void backHistVersion(WorkFlowDefinitionData wfDefinitionData) {
        int targetVersion = wfDefinitionData.getDefVersion().intValue();
        String defId = wfDefinitionData.getDefId();
        if (StrUtil.isBlank(defId)) {
            throw new ServiceException("流程定义标识不能为空！");
        }
        List<WfDefinition> list = selectByDefId(defId);
        WfDefinition targetDef = null;
        WfDefinition sourceDef = null;
        for(WfDefinition wfDef:list){
            if("0".equals(wfDef.getPublicFlag()) && wfDef.getDefVersion().intValue()==0){
                targetDef = wfDef;
            }else if(targetVersion == wfDef.getDefVersion().intValue()){
                sourceDef = wfDef;
            }
        }
        if(targetDef==null){
            throw new ServiceException("流程定义不存在，请确认！");
        }
        if(sourceDef==null){
            throw new ServiceException("指定的历史版本不存在，请确认！");
        }
        //复制数据
        targetDef.setDefName(sourceDef.getDefName());
        targetDef.setDefDesc(sourceDef.getDefDesc());
        targetDef.setXmlSource(sourceDef.getXmlSource());
        targetDef.setJsonSource(sourceDef.getJsonSource());

        //更新数据
        cacheDaoSvc.updateByKeyId(targetDef);
    }

    @Override
    public List<WfDefinition> selectWfDefinitionByXmlSource(String xmlSource) {
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(WfDefinition::getXmlSource,xmlSource);
        return definitionMapper.selectList(queryWrapper);
    }

    private void deployWorkflowByEntity(WfDefinition wfDefinition) {
        String workFlowCode = wfDefinition.getDefId();
        String defXmlContent = wfDefinition.getXmlSource();
        String defJsonContent=wfDefinition.getJsonSource();
        if (StrUtil.isBlank(workFlowCode)) {
            throw new ServiceException("流程编码为空，无法部署！");
        }
        if (StrUtil.isBlank(defXmlContent)) {
            throw new ServiceException("流程定义内容为空，无法部署流程！");
        }

        if (StrUtil.isBlank(defJsonContent)) {
            throw new ServiceException("流程定义内容为空，无法部署流程！");
        }
        log.debug(" | - WorkFlowDefinitionService>>>>>got from db");
        log.debug(defXmlContent);
        XmlConvertResult xmlConvertResult = XmlUtil.convertEditor2starndard(defXmlContent,true,curActiveProcessDefXml(wfDefinition.getDefId()),configUtil.isIgnoreProcessVersion());
        defXmlContent = xmlConvertResult.getXmlStr();

        log.debug(" | - WorkFlowDefinitionService>>>>>after cvt");
        log.info(defXmlContent);

        Deployment deployment = activitiBaseService.deployWorkFlow(workFlowCode, defXmlContent);

        log.info(" | - WorkFlowDefinitionService>>>>>流程[{}]部署成功,depoymentId={}" ,workFlowCode, deployment.getId());


        List<ProcessDefinition> processDefinitions = processEngine.getRepositoryService().createProcessDefinitionQuery().processDefinitionKey(wfDefinition.getDefId()).active().orderByProcessDefinitionVersion().desc().list();
        if(processDefinitions==null || processDefinitions.size() == 0) {
            throw new ServiceException("deployment failed!");
        }
        Integer version = Integer.valueOf(processDefinitions.get(0).getVersion());

        if(xmlConvertResult.isCanUseLatestProperties()&&configUtil.isIgnoreProcessVersion()) {
            if(xmlConvertResult.hasProperties()) {
                for(String taskCode:xmlConvertResult.getTaskCodes()) {
                    for(Map.Entry<String, String> defPropEntry:xmlConvertResult.getPropEntries(taskCode)) {
                        WfDefinePropExtends wfDefinePropExtends = new WfDefinePropExtends();
                        wfDefinePropExtends.setProcessCode(wfDefinition.getDefId());
                        wfDefinePropExtends.setTaskCode(taskCode);
                        wfDefinePropExtends.setPropName(defPropEntry.getKey());
                        wfDefinePropExtendsService.deleteByProcessCodePropNameTaskCode(wfDefinePropExtends);
                        wfDefinePropExtends.setProcessCode(wfDefinition.getDefId());
                        wfDefinePropExtends.setTaskCode(taskCode);
                        wfDefinePropExtends.setPropName(defPropEntry.getKey());
                        wfDefinePropExtends.setPropValue(defPropEntry.getValue());
                        wfDefinePropExtendsService.insertWfDefinePropExtends(wfDefinePropExtends);
                    }
                }
                wfModelPropExtendsService.addUseProcessLatesVersion(wfDefinition.getDefId(),version);
            }
        }


        // 增加一个发布版本
        WfDefinition newWfDefinition;
        try {
            newWfDefinition = (WfDefinition) BeanUtils.cloneBean(wfDefinition);
            newWfDefinition.setPublicFlag("1");
            newWfDefinition.setId(null);
            newWfDefinition.setDefVersion(version);
            //创建时间和更新时间  数据库会自动更新为当前时间，这也是流程发布时间
            newWfDefinition.setCreateTime(null);
            newWfDefinition.setUpdateTime(null);
            newWfDefinition.setVersion(version);
            cacheDaoSvc.insertWfDefinition(newWfDefinition);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("流程部署失败，请确认！");
        }
    }
    public Integer selectMaxDefVersion(String defId){
        LambdaQueryWrapper<WfDefinition> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfDefinition::getDefId,defId);
        queryWrapper.orderByDesc(WfDefinition::getDefVersion);
        List<WfDefinition> wfDefinitionList = definitionMapper.selectList(queryWrapper);
        int MaxDefVersion = 0;
        if (UtilValidate.isNotEmpty(wfDefinitionList)) {
            MaxDefVersion = wfDefinitionList.get(0).getDefVersion();
        }
        return MaxDefVersion;
    }

    /**
     * 检查必输项
     *
     * @param wfDefinitionData
     */
    private void checkDataProp(WorkFlowDefinitionData wfDefinitionData) {
        if (StrUtil.isBlank(wfDefinitionData.getDefId())) {
            throw new ServiceException("流程定义标识不能为空！");
        }
        if (StrUtil.isBlank(wfDefinitionData.getDefName())) {
            throw new ServiceException("流程定义名称不能为空！");
        }
    }

    /**
     * 替换xmlSource中的字段
     * @param xmlSources
     * @return
     */
    private String replaceXmlSources(String xmlSources) {
        String targetXmlSources = xmlSources;
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.ExecutionStartListener", "com.bpm.workflow.engine.listener.ExecutionStartListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.ExecutionEndListener","com.bpm.workflow.engine.listener.ExecutionEndListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.ExecutionEndErrorListener","com.bpm.workflow.engine.listener.ExecutionEndErrorListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.StartNodeStartListener", "com.bpm.workflow.engine.listener.StartNodeStartListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.TaskCreateListener", "com.bpm.workflow.engine.listener.TaskCreateListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.TaskAssignmentListener", "com.bpm.workflow.engine.listener.TaskAssignmentListener");
        targetXmlSources = targetXmlSources.replaceAll("com.workflow.activiti.engine.listener.TaskCompleteListener","com.bpm.workflow.engine.listener.TaskCompleteListener");

        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.ExecutionStartListener", "com.bpm.workflow.engine.listener.ExecutionStartListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.ExecutionEndListener","com.bpm.workflow.engine.listener.ExecutionEndListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.ExecutionEndErrorListener","com.bpm.workflow.engine.listener.ExecutionEndErrorListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.StartNodeStartListener", "com.bpm.workflow.engine.listener.StartNodeStartListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.TaskCreateListener", "com.bpm.workflow.engine.listener.TaskCreateListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.TaskAssignmentListener", "com.bpm.workflow.engine.listener.TaskAssignmentListener");
        targetXmlSources = targetXmlSources.replaceAll("com.yonyou.sml.admin.workflow.engine.listener.TaskCompleteListener","com.bpm.workflow.engine.listener.TaskCompleteListener");
        return targetXmlSources;
    }

    private String curActiveProcessDefXml(String defId) {
        int maxDefVersion= selectMaxDefVersion(defId);
        if(maxDefVersion<=0) {
            return null;
        }
        WfDefinition theArg = new WfDefinition();
        theArg.setDefId(defId);
        theArg.setDefVersion(maxDefVersion);
        WfDefinition result = selectByDefIdVersion(theArg);
        if(result ==null) {
            return null;
        }
        return result.getXmlSource();
    }
}
