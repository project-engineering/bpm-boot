package com.bpm.workflow.engine.command;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.util.WfVariableUtil;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.history.HistoryLevel;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.task.Task;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 跳转命令
 * 
 * @author xuesw
 *
 */
public class JumpCommand implements Command<Boolean> {
	
	private String taskId;

	private String targetActivitiId;

	private Map<String, Object> varMap;
	
	private String jumpMsg;

	public JumpCommand(String taskId, String targetActivitiId, Map<String, Object> varMap, String jumpMsg) {
		this.targetActivitiId = targetActivitiId;
		this.taskId = taskId;
		this.varMap = varMap;
		this.jumpMsg = jumpMsg;
	}

	@Override
	public Boolean execute(CommandContext commandContext) {
		// 数据准备
		ProcessEngineConfigurationImpl engineConfiguration = commandContext.getProcessEngineConfiguration();
		TaskService taskService = engineConfiguration.getTaskService();
		RuntimeService runtimeService = engineConfiguration.getRuntimeService();
		RepositoryService repositoryService = engineConfiguration.getRepositoryService();
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		if (task == null) {
			throw  new ActivitiException("流程未启动或已执行完成，找不到任务标识：" + taskId + "");
		}
		if (StrUtil.isBlank(targetActivitiId)) {
			throw  new ActivitiException("流程流转目标节点不能为空！！！");
		}

		if (varMap == null) {
			varMap = new HashMap<String, Object>(2);
		}

		// 查询并准备source 和 target FlowNode元素
		String processDefinitionId = task.getProcessDefinitionId();
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
		FlowNode targetFlowNode = (FlowNode) bpmnModel.getMainProcess().getFlowElement(targetActivitiId);

		Execution execution = runtimeService.createExecutionQuery().executionId(task.getExecutionId()).singleResult();
		String activityId = execution.getActivityId();
		FlowNode sourceFlowNode = (FlowNode) bpmnModel.getMainProcess().getFlowElement(activityId);

		// 记录原活动方向
		List<SequenceFlow> oriSequenceFlows = new ArrayList<SequenceFlow>();
		oriSequenceFlows.addAll(sourceFlowNode.getOutgoingFlows());

		// 最终执行的方向列表
		List<SequenceFlow> newSequenceFlowList = new ArrayList<SequenceFlow>();

		// 查找是否已经存在活动方向
		for (SequenceFlow sqFlow : oriSequenceFlows) {
			if (sqFlow.getSourceRef().equals(sourceFlowNode.getName())
					&& sqFlow.getTargetRef().equals(targetFlowNode.getName())) {
				newSequenceFlowList.add(sqFlow);
			}
		}

		// 清理活动方向
		sourceFlowNode.getOutgoingFlows().clear();

		// 建立新方向
		if (newSequenceFlowList.isEmpty()) {
			SequenceFlow newSequenceFlow = new SequenceFlow();
			newSequenceFlow.setId("newSequenceFlowId");
			newSequenceFlow.setSourceFlowElement(sourceFlowNode);
			newSequenceFlow.setTargetFlowElement(targetFlowNode);
			newSequenceFlowList.add(newSequenceFlow);
		}

		// 设置新最终的流转方向
		sourceFlowNode.setOutgoingFlows(newSequenceFlowList);

		// 记录操作备注
		if(engineConfiguration.getHistoryLevel() != HistoryLevel.NONE) {
			taskService.addComment(task.getId(), task.getProcessInstanceId(), jumpMsg);
		}

		// 完成任务
		WfVariableUtil.addAllVariableMap(varMap);
		taskService.complete(task.getId(), new HashMap<String,Object>());

		// 恢复原方向
		sourceFlowNode.setOutgoingFlows(oriSequenceFlows);
		return true;
	}

	/**
	 * 反向向后查找跳转了几步
	 * 
	 * @param bpmnModel
	 * @param sourceFlowNode
	 * @param count
	 *            1表示不是反向(即向下流转)，-1表示撤回操作，<-1表示表示反向驳回n步
	 * @return
	 */
	@SuppressWarnings("unused")
	private int countStepForBack(BpmnModel bpmnModel, FlowNode sourceFlowNode, int count) {
		if (sourceFlowNode.getId().equals(targetActivitiId)){
			return count;
		}

		List<SequenceFlow> inComFlows = sourceFlowNode.getIncomingFlows();
		for (SequenceFlow seqFlow : inComFlows) {
			int temp = countStepForBack(bpmnModel, (FlowNode) seqFlow.getSourceFlowElement(), count - 1);
			if (temp > 0){
				return temp;
			}
		}

		// 所有的都没有找到，即不是反向流转（撤回或驳回）
		return 1;
	}

}
