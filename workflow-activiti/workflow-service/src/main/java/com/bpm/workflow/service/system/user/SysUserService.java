package com.bpm.workflow.service.system.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.user.SysUser;
import com.bpm.workflow.vo.system.user.UserParam;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 新增用户
     * @param sysUser
     */
    boolean addUser(SysUser sysUser);
    /**
     * 修改用户
     * @param sysUser
     */
    boolean updateUser(SysUser sysUser);
    /**
     * 删除用户
     * @param userId
     */
    boolean deleteUser(String userId);
    IPage<SysUser> getList(UserParam param);

    /**
     * 密码重置
     *
     * @param userId
     * @return
     */
    int resetPwd(String userId);
}
