package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskFlowData;
import com.bpm.workflow.entity.TaskCodeCount;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.mapper.WfTaskflowMapper;
import com.bpm.workflow.service.WfInstanceService;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.util.UtilValidate;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfTaskflowServiceImpl extends ServiceImpl<WfTaskflowMapper, WfTaskflow> implements WfTaskflowService {
    @Resource
    private WfTaskflowMapper wfTaskflowMapper;
    @Resource
    private WfInstanceService wfInstanceService;

    @Override
    public WfTaskflow selectByTaskId(String taskId) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfTaskflow::getTaskId, taskId);
        return wfTaskflowMapper.selectOne(queryWrapper);
    }

    @Override
    public List<WfTaskflow> selectByCondition(WfTaskflow wfTaskflow) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(wfTaskflow.getId())) {
            queryWrapper.eq(WfTaskflow::getId, wfTaskflow.getId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskId())) {
            queryWrapper.eq(WfTaskflow::getTaskId, wfTaskflow.getTaskId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskCode())) {
            queryWrapper.eq(WfTaskflow::getTaskCode, wfTaskflow.getTaskCode());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskName())) {
            queryWrapper.eq(WfTaskflow::getTaskName, wfTaskflow.getTaskName());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskType())) {
            queryWrapper.eq(WfTaskflow::getTaskType, wfTaskflow.getTaskType());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskDefId())) {
            queryWrapper.eq(WfTaskflow::getTaskDefId, wfTaskflow.getTaskDefId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskType())) {
            queryWrapper.eq(WfTaskflow::getTaskType, wfTaskflow.getTaskType());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getWorkType())) {
            queryWrapper.eq(WfTaskflow::getWorkType, wfTaskflow.getWorkType());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskRate())) {
            queryWrapper.eq(WfTaskflow::getTaskRate, wfTaskflow.getTaskRate());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskUrgent())) {
            queryWrapper.eq(WfTaskflow::getTaskUrgent, wfTaskflow.getTaskUrgent());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskBussName())) {
            queryWrapper.eq(WfTaskflow::getTaskBussName, wfTaskflow.getTaskBussName());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getProcessCode())) {
            queryWrapper.eq(WfTaskflow::getProcessCode, wfTaskflow.getProcessCode());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getProcessName())) {
            queryWrapper.eq(WfTaskflow::getProcessName, wfTaskflow.getProcessName());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getProcessId())) {
            queryWrapper.eq(WfTaskflow::getProcessId, wfTaskflow.getProcessId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getWorkCreater())) {
            queryWrapper.eq(WfTaskflow::getWorkCreater, wfTaskflow.getWorkCreater());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getSystemCode())) {
            queryWrapper.eq(WfTaskflow::getSystemCode, wfTaskflow.getSystemCode());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getCreateDate())) {
            queryWrapper.eq(WfTaskflow::getCreateDate, wfTaskflow.getCreateDate());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getStartDate())) {
            queryWrapper.eq(WfTaskflow::getStartDate, wfTaskflow.getStartDate());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getUpdateDate())) {
            queryWrapper.eq(WfTaskflow::getUpdateDate, wfTaskflow.getUpdateDate());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getEndDate())) {
            queryWrapper.eq(WfTaskflow::getEndDate, wfTaskflow.getEndDate());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getSuperTaskId())) {
            queryWrapper.eq(WfTaskflow::getSuperTaskId, wfTaskflow.getSuperTaskId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getParentTaskId())) {
            queryWrapper.eq(WfTaskflow::getParentTaskId, wfTaskflow.getParentTaskId());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskActors())) {
            queryWrapper.eq(WfTaskflow::getTaskActors, wfTaskflow.getTaskActors());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getWorkState())) {
            queryWrapper.eq(WfTaskflow::getWorkState, wfTaskflow.getWorkState());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getDoTaskActor())) {
            queryWrapper.eq(WfTaskflow::getDoTaskActor, wfTaskflow.getDoTaskActor());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getDoTaskActorName())) {
            queryWrapper.eq(WfTaskflow::getDoTaskActorName, wfTaskflow.getDoTaskActorName());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getDoResult())) {
            queryWrapper.eq(WfTaskflow::getDoResult, wfTaskflow.getDoResult());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getDoRemark())) {
            queryWrapper.eq(WfTaskflow::getDoRemark, wfTaskflow.getDoRemark());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getWorkAllot())) {
            queryWrapper.eq(WfTaskflow::getWorkAllot, wfTaskflow.getWorkAllot());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getStepCount())) {
            queryWrapper.eq(WfTaskflow::getStepCount, wfTaskflow.getStepCount());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getStrikeTiming())) {
            queryWrapper.eq(WfTaskflow::getStrikeTiming, wfTaskflow.getStrikeTiming());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getTaskDoState())) {
            queryWrapper.eq(WfTaskflow::getTaskDoState, wfTaskflow.getTaskDoState());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getCheckInFlag())) {
            queryWrapper.eq(WfTaskflow::getCheckInFlag, wfTaskflow.getCheckInFlag());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getWorkDoFlag())) {
            queryWrapper.eq(WfTaskflow::getWorkDoFlag, wfTaskflow.getWorkDoFlag());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getQueryIndexa())) {
            queryWrapper.eq(WfTaskflow::getQueryIndexa, wfTaskflow.getQueryIndexa());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getQueryIndexb())) {
            queryWrapper.eq(WfTaskflow::getQueryIndexb, wfTaskflow.getQueryIndexb());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getQueryIndexc())) {
            queryWrapper.eq(WfTaskflow::getQueryIndexc, wfTaskflow.getQueryIndexc());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getQueryIndexd())) {
            queryWrapper.eq(WfTaskflow::getQueryIndexd, wfTaskflow.getQueryIndexd());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getIsTimeout())) {
            queryWrapper.eq(WfTaskflow::getIsTimeout, wfTaskflow.getIsTimeout());
        }
        if (UtilValidate.isNotEmpty(wfTaskflow.getDuedata())) {
            queryWrapper.eq(WfTaskflow::getDuedata, wfTaskflow.getDuedata());
        }
        return wfTaskflowMapper.selectList(queryWrapper);
    }

    @Override
    public void insertWfTaskflow(WfTaskflow taskflow) {
        wfTaskflowMapper.insert(taskflow);
    }

    @Override
    public List<WfTaskflow> selectByProcessInstId(String processInstanceId) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfTaskflow::getParentTaskId, processInstanceId);
        return wfTaskflowMapper.selectList(queryWrapper);
    }

    @Override
    public List<TaskCodeCount> selectUnoTaskCountByUsers(List<String> list) {
        return wfTaskflowMapper.selectUnoTaskCountByUsers(list);
    }

    @Override
    public void selectByIdForUpdate(Integer id) {
        wfTaskflowMapper.selectByIdForUpdate(id);
    }

    @Override
    public List<WfTaskflow> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> selectPageData) {
        return null;
    }

    @Override
    public List<WfTaskflow> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData) {
        return wfTaskflowMapper.queryTaskDoResultList(taskFlowData);
    }

    @Override
    public WfTaskflow overTimeTaskList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> selectPageData) {
        return wfTaskflowMapper.overTimeTaskList(taskFlowData);
    }

    /**
     * 通过实例id获取子流程所有执行过程
     * @param processInstId
     * @return
     */
    @Override
    public List<WfTaskflow> selectSubproListByProcessInstId(String processInstId) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(WfTaskflow::getProcessId,processInstId);
        List<String>taskIdList = wfTaskflowMapper.selectList(queryWrapper).stream().map(WfTaskflow::getTaskId).collect(Collectors.toList());
        if (UtilValidate.isNotEmpty(taskIdList)) {
            List<WfInstance> wfInstanceList = wfInstanceService.selectWfInstanceByTaskIds(taskIdList);
            List<String> processIdList = null;
            if (UtilValidate.isNotEmpty(wfInstanceList)) {
                processIdList = wfInstanceList.stream().map(WfInstance::getProcessId).collect(Collectors.toList());
            }
            if (UtilValidate.isNotEmpty(processIdList)) {
                LambdaQueryWrapper<WfTaskflow> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper.in(WfTaskflow::getProcessId,processIdList);
                return wfTaskflowMapper.selectList(lambdaQueryWrapper);
            }
        }
        return null;
    }

    @Override
    public void deleteById(Integer id) {
        wfTaskflowMapper.deleteById(id);
    }

    /**
     * 获取检出代办任务的个数
     * @param taskFlowData
     * @return
     */
    @Override
    public int selectCheckOutCount(WorkFlowTaskFlowData taskFlowData) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfTaskflow::getTaskCode,taskFlowData.getTaskCode());
        queryWrapper.eq(WfTaskflow::getTaskActors,taskFlowData.getUserId());
        queryWrapper.eq(WfTaskflow::getTaskPoolFlag,"2");
        queryWrapper.eq(WfTaskflow::getWorkState,"0");
        List<WfTaskflow> wfTaskflowList = wfTaskflowMapper.selectList(queryWrapper);
        return wfTaskflowList.size();
    }

    @Override
    public List<WfTaskflow> selectByTaskIds(List<String> taskIdList) {
        LambdaQueryWrapper<WfTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(WfTaskflow::getTaskId, taskIdList);
        return wfTaskflowMapper.selectList(queryWrapper);
    }

    @Override
    public List<WfTaskflow> selectUndoTaskFlow(Map<String, Object> param) {
        return wfTaskflowMapper.selectUndoTaskFlow(param);
    }

    @Override
    public List<TaskCodeCount> selectTaskCodeCount(Map<String, Object> param) {
        return wfTaskflowMapper.selectTaskCodeCount(param);
    }

    @Override
    public int checkGlobSeqNoIsExsit(Map<String, Object> paramMap) {
        return wfTaskflowMapper.checkGlobSeqNoIsExsit(paramMap);
    }
}
