package com.bpm.workflow.engine.listener;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.CacheDaoSvc;
import com.bpm.workflow.util.DateUtil;
import com.bpm.workflow.service.WfInstanceService;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.ModelProperties;
import com.bpm.workflow.util.WfVariableUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 任务节点指定代理人的监听
 *
 * @author xuesw
 */
@Log4j2
@Component
public class TaskAssignmentListener extends TaskCreateListener {
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private WfInstanceService wfInstanceService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    /**
     *
     */
    private static final long serialVersionUID = 2399312114949113194L;

    @SuppressWarnings("unchecked")
    @Override
    public void notify(DelegateTask delegateTask) {
        try {
            DelegateExecution execution = delegateTask.getExecution();
            FlowElement element = execution.getCurrentFlowElement();
            Map<String, Object> varMap = WfVariableUtil.getVariableMap();
            String nodeType = processUtil.getTaskProp(delegateTask.getProcessDefinitionId(), element, Constants.NODETYPE);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
            String globalSeqno = null;
            if (WfVariableUtil.getSysCommHead() != null) {
                //会签节点的流水号没有赋值
                globalSeqno = WfVariableUtil.getSysCommHead().getGlobalSeqNo();
            }
            switch (nodeType) {
                case Constants.NODE_TYPE_SIGN:
                    Map<String, UserOutputData> map = (Map<String, UserOutputData>) execution.getVariableLocal(Constants.SIGN_USER_LIST);
                    log.info(" | - TaskAssignmentListener>>>>>会签处理人员id：{}", delegateTask.getAssignee());
                    log.info(" | - TaskAssignmentListener>>>>>人员信息map：{}", map.toString());
                    UserOutputData userinfo = null;
                    if (map.containsKey(delegateTask.getAssignee())) {
                        userinfo = map.get(delegateTask.getAssignee());
                    }
                    if (userinfo == null) {
                        throw new ServiceException("会签选人失败！！！找不到userId=" + delegateTask.getAssignee());
                    }
                    String nodePath = processUtil.getTaskProp(delegateTask.getProcessDefinitionId(), element, Constants.NODEPATH);// element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPATH);
                    String nodeId = processUtil.getTaskProp(delegateTask.getProcessDefinitionId(), element, Constants.NODEPROP_REFID);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
                    String beforTaskId = (String) varMap.get(Constants.BEFOR_TASKID);

                    if (log.isInfoEnabled()) {
                        log.info("| - TaskAssignmentListener>>>>>处理会签节点的选人结果[{}]", userinfo);
                    }
                    List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);

                    String url = modelPropService.getNomalOrRulePropValue(extendsPropList, ModelProperties.functionList.getName(), varMap);
                    delegateTask.getExecution().setVariables(varMap);
                    WfInstance wfInstance = wfInstanceService.selectByProcessId(delegateTask.getProcessInstanceId());
                    if (wfInstance == null) {
                        wfInstance = new WfInstance();
                        wfInstance.setProcessId(delegateTask.getProcessInstanceId());
                        WfDefinition wfDefinine = cacheDaoSvc.selectActiveFlowByDefId(delegateTask.getProcessDefinitionId().split(":")[0]);
                        wfInstance.setProccessName(wfDefinine.getDefName());
                        Object processCreateTime = varMap.get(Constants.PROCESS_CREATE_TIME);
                        if (processCreateTime != null) {
                            wfInstance.setCreateTime(DateUtil.convertObjToLdt(processCreateTime));
                        }
                        wfInstance.setStartUserId((String) varMap.get(Constants.BEFOR_USERID));
                        wfInstance.setStartUserMap(BpmStringUtil.objectToJsonObject(varMap.get(Constants.BEFORUSERDATA)).toJSONString());
                        wfInstance.setSystemCode((String) varMap.get(Constants.SYSTEM_CODE_KEY));
                    }
                    addWfTaskflow(wfInstance, delegateTask, nodeType, url, null, beforTaskId, nodePath, null, globalSeqno, userinfo);
                    break;

                default:
                    break;
            }

        } catch (Exception e) {
            log.error("| - TaskAssignmentListener>>>>>会签节点选人异常", e);
            throw new ActivitiException(e.getMessage());
        }

    }

}
