package com.bpm.workflow.client.urule;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.util.constant.BpmConstants;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UruleServiceFactory {
	@Resource
	private RestUruleService restUruleService;;
	@Resource
	private RestTemplateUruleService restTemplateUruleService;

	@Value("${urule.callType.protocol:rest}")
	private String callType;

	public IUruleService getInstance() {
		if (StrUtil.isBlank(callType)) {
			return restUruleService;
		}
		if (BpmConstants.REST_TEMPLATE.equals(callType)) {
			return restTemplateUruleService;
		}
		return restUruleService;

	}
}