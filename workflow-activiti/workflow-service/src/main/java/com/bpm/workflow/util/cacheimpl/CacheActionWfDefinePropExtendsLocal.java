package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.entity.WfDefinePropExtends;
import com.bpm.workflow.service.common.WfDefinePropExtendsService;
import com.bpm.workflow.util.CacheUtil;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;

@Component
public class CacheActionWfDefinePropExtendsLocal extends CacheActionBase<WfDefinePropExtends>{
	
	@Resource
	private WfDefinePropExtendsService wfDefinePropExtendsService;

	@Override
	public WfDefinePropExtends getCacheValueByKey(String selectMethodName, String firstKey, String secondKey, String thirdKey) {
		
		if(StrUtil.isEmpty(firstKey)) {
			throw new RuntimeException("processCode can not be null");
		}
		if(StrUtil.isEmpty(secondKey)) {
			throw new RuntimeException("taskCode can not be null");
		}
		if(StrUtil.isEmpty(thirdKey)) {
			throw new RuntimeException("propName can not be null");
		}
		WfDefinePropExtends result = super.getCacheValueByKey(null, new String[]{firstKey, secondKey, thirdKey});
		if( result == null || (!CacheUtil.USE_CACHE)) {
			WfDefinePropExtends theArg = new WfDefinePropExtends();
			theArg.setProcessCode(firstKey);
			theArg.setTaskCode(secondKey);
			theArg.setPropName(thirdKey);

			result = wfDefinePropExtendsService.selectByProcessCodePropNameTaskCode(theArg);
			if(result!=null) {
				super.putCacheValueByKey(null,  new String[] {firstKey,secondKey,thirdKey}, result);
			}
		}

		return result;
	}
	
	@Override
	public WfDefinePropExtends getCacheValueByKey(String selectMethodName, String firstKey, String secondKey) {
		throw new RuntimeException("not support operation");
	}

	@Override
	public List<WfDefinePropExtends> getCacheListValueByKey(String selectMethodName, String firstKey, String secondKey) {
		throw new RuntimeException("not support operation");
	}
	
}
