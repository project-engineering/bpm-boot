package com.bpm.workflow.cache;

import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Log4j2
public class RedisCacheUtil implements ICacheHandlerUtil {
    private static Logger logger = LoggerFactory.getLogger(RedisCacheUtil.class);

    @SuppressWarnings("rawtypes")
    @Resource
    private RedisTemplate redisTemplate;

    @SuppressWarnings("rawtypes")
    public RedisCacheUtil(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Value("${redis.expireSeconds}")
    private long expireSeconds;

    @Value("${redisCacheFolder}" + ":")
    private String redisCacheFolder;


    /**
     * 更新缓存对象
     *
     * @param key
     * @param value
     */
    @Override
    public void updateObject(String key, Object value) {
        if (this.hasKey(key)) {
            this.remove(key);
        }
        this.setObject(key, value, expireSeconds);
    }

    @Override
    public void updateAllObject(String pattern, String key, Object value) {
        this.removeAll(pattern);
        this.setObject(key, value, expireSeconds);
    }


    /**
     * 写入缓存设置时效时间
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
    public boolean setObject(final String key, Object value, Long expireTime) {
        RedisCacheLock lock = new RedisCacheLock(redisTemplate, redisCacheFolder + key);
        boolean result = false;
        try {
            if (lock.lock()) {
                ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
                if (expireSeconds > 0) {
                    operations.set(redisCacheFolder + key, value, expireTime, TimeUnit.SECONDS);
                } else {
                    operations.set(redisCacheFolder + key, value);
                }
                log.info("|-RedisCacheUtil>>>>>>cache setObject :{}", key);
                result = true;
            }
            return result;
        } catch (Exception e) {
            ExceptionUtils.getStackTrace(e);
        } finally {
            lock.unlock();
        }
        log.info("|-RedisCacheUtil>>>>>>cache setObject :{}", key);
        return result;
    }

    /**
     * 读取缓存
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object getObject(final String key) {
        if (this.hasKey(key)) {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            log.debug("|-RedisCacheUtil>>>>>>cache getObject :{}", key);
            return operations.get(redisCacheFolder + key);
        }
        return null;
    }

    /**
     * 删除对应的value
     *
     * @param key
     */
    @SuppressWarnings("unchecked")
    @Override
    public void remove(final String key) {
        RedisCacheLock lock = new RedisCacheLock(redisTemplate, redisCacheFolder + key);
        if (this.hasKey(key)) {
            try {
                if (lock.lock() && this.hasKey(key)) {
                    redisTemplate.delete(redisCacheFolder + key);
                    log.info("|-RedisCacheUtil>>>>>>cache remove :{}", key);
                }

            } catch (Exception e) {
                log.error("|-RedisCacheUtil>>>>>>cache remove :{}", key, e);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * 判断缓存中是否有对应的value
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean hasKey(final String key) {
        Boolean result = redisTemplate.hasKey(redisCacheFolder + key);
        if (result == null) {
            return false;
        }
        return result.booleanValue();
    }

    /**
     * 批量删除对应的value
     */
    @SuppressWarnings("unchecked")
    @Override
    public void removeAll(String pattern) {
        Set<String> keys = redisTemplate.keys(redisCacheFolder + pattern + "*");
        try {
            redisTemplate.delete(keys);
            log.info("|-RedisCacheUtil>>>>>>cache removeAll :{} *", pattern);
        } catch (Exception e) {
            log.error("|-RedisCacheUtil>>>>>>cache removeAll :{}", pattern, e);
        }
    }

    /**
     * 缓存List数据
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public void setCacheList(String key, List<?> dataList) {
        this.setObject(key, dataList, expireSeconds);
    }

    @SuppressWarnings("unchecked")
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(redisCacheFolder + key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error("|-RedisCacheUtil>>>>>>expire[key:{} ,time: {}", key, time, e);
            return false;
        }
    }

    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存键值对应的数据
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> getCacheList(String key) {
        try {
            Object object = this.getObject(key);
            if (Objects.isNull(object)) {
                return null;
            }
            List<T> dataList = (List<T>) object;
            log.info("|-RedisCacheUtil>>>>>>cache getCacheList :{}", key);
            return dataList;
        } catch (Exception e) {
            log.error("|-RedisCacheUtil>>>>>>cache getCacheList :{}", key, e);
        }
        return null;
    }

    @Override
    public void updateCacheList(String k, List<?> v) {
        if (this.hasKey(k)) {
            this.remove(k);
        }
        this.setCacheList(k, v);
    }

    @Override
    public void updateElement(String key, Element element) {
        this.updateObject(key, SerializeUtil.objSerialiableToStr(element));
    }

    @Override
    public Element getElement(final String key) {
        String value = (String) this.getObject(key);
        if (value == null) {
            return null;
        }
        Element element = (Element) SerializeUtil.strUnSerializeToObj(value);
        return element;
    }


}
