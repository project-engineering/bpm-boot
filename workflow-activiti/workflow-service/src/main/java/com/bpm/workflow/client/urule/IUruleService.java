package com.bpm.workflow.client.urule;

import com.alibaba.fastjson2.JSONObject;
import java.util.Map;

public interface IUruleService {

	public String execByRuleName(String ruleName, Map<String, Object> params);
	/**
	 * 选人、流转、功能清单等规则执行接口
	 * @param param 业务参数
	 * @param uruleMap 规则配置
	 * 
	 * @return 返回规则的执行结果String型[result1,result2]
	 */
	public String execUruleReusltList(Map<String, Object> param, JSONObject uruleMap);
}
