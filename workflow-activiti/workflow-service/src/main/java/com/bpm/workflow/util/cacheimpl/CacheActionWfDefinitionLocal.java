package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.ServiceNameConstants;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.service.WfDefinitionService;
import com.bpm.workflow.util.CacheUtil;
import com.bpm.workflow.util.XmlUtil;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.RepositoryService;
import org.dom4j.Element;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.List;

@Component
public class CacheActionWfDefinitionLocal extends CacheActionBase<WfDefinition>{
	@Resource
	private WfDefinitionService wfDefinitionService;
	@Resource
	private RepositoryService repositoryService;


	@Override
	public FlowElement getBpmnModelNodeByTaskId(String processDefinitionId, String taskDefinitionKey) {
		String key = getKey(null, new String[] {processDefinitionId,taskDefinitionKey},2);
		FlowElement theElement = null;
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			theElement = flowElementByProcessDefIdTaskDefId.get(key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			theElement = (FlowElement)cacheUtil.getRedisCacheUtil().getObject("flowElementByProcessDefIdTaskDefId"+key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			theElement = (FlowElement)cacheUtil.getEhCacheUtil().getObject("flowElementByProcessDefIdTaskDefId"+key);
		}
		
		if(theElement==null||(!CacheUtil.USE_CACHE)) {
			BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
			theElement = bpmnModel.getFlowElement(taskDefinitionKey);
			if(theElement != null) {
				if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
					flowElementByProcessDefIdTaskDefId.put(key, theElement);
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
					cacheUtil.getRedisCacheUtil().setObject("flowElementByProcessDefIdTaskDefId"+key, theElement, Long.valueOf(-1));
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
					cacheUtil.getEhCacheUtil().putCache("flowElementByProcessDefIdTaskDefId"+key, theElement);
				}
				
			}
		}
		
		return theElement;
	}

	/**
	 *
	 * @param processDefId
	 * @param activitiVersion
	 * @param taskDefId
	 * @return
	 */
	@Override
	public Element getTaskElementByDefIdActVersionTaskDefId(String processDefId, BigInteger activitiVersion, String taskDefId) {
		String key = getKey(null, new String[] {processDefId, activitiVersion.toString(),taskDefId},3);
		Element theElement = null;
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			theElement = taskElementByDefIdActVersionTaskDefId.get(key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			theElement = (Element)cacheUtil.getRedisCacheUtil().getElement("taskElementByDefIdActVersionTaskDefId"+key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			theElement = (Element)cacheUtil.getEhCacheUtil().getElement("taskElementByDefIdActVersionTaskDefId"+key);
		}

		if(theElement==null||(!CacheUtil.USE_CACHE)) {
			WfDefinition theArg = new WfDefinition();
			theArg.setDefId(processDefId);
			theArg.setDefVersion(Integer.valueOf(String.valueOf(activitiVersion)));
			
			WfDefinition wfDefinition = wfDefinitionService.selectByDefIdVersion(theArg);
			if(wfDefinition == null || StrUtil.isEmpty(wfDefinition.getXmlSource())) {
				theElement = null;
			}else {
				theElement = XmlUtil.getDefinedTaskElement(wfDefinition.getXmlSource(), taskDefId);
			}

			if(theElement != null) {				
				if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
					taskElementByDefIdActVersionTaskDefId.put(key, theElement);
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
					cacheUtil.getRedisCacheUtil().updateElement("taskElementByDefIdActVersionTaskDefId"+key, theElement);
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
					cacheUtil.getEhCacheUtil().updateElement("taskElementByDefIdActVersionTaskDefId"+key, theElement);
				}
			}
		}
		
		return theElement;
	}
	
	@Override
	public Element getTaskElementByDefIdActivtiVersionTaskDefId(String processDefId, BigInteger version,String taskDefId) {
		String key = getKey(null, new String[] {processDefId, version.toString(),taskDefId},3);
		Element theElement = null;
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			theElement = taskElementByDefIdActivitiVersionTaskDefId.get(key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			theElement = (Element)cacheUtil.getRedisCacheUtil().getElement("taskElementByDefIdActivitiVersionTaskDefId"+key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			theElement = (Element)cacheUtil.getEhCacheUtil().getElement("taskElementByDefIdActivitiVersionTaskDefId"+key);
		}

		if(theElement==null||(!CacheUtil.USE_CACHE)) {
			WfDefinition wfDefinition = selectByDefIdActVersion(XmlUtil.getProcessIdOnly(processDefId),version.toString());
			if(wfDefinition == null || StrUtil.isEmpty(wfDefinition.getXmlSource())) {
				theElement = null;
			}else {
				theElement = XmlUtil.getDefinedTaskElement(wfDefinition.getXmlSource(), taskDefId);
			}

			if(theElement != null) {				
				if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
					taskElementByDefIdActivitiVersionTaskDefId.put(key, theElement);
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
					cacheUtil.getRedisCacheUtil().updateElement("taskElementByDefIdActivitiVersionTaskDefId"+key, theElement);
				}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
					cacheUtil.getEhCacheUtil().updateElement("taskElementByDefIdActivitiVersionTaskDefId"+key, theElement);
				}
			}
		}
		
		return theElement;
	}

	@Override
	public WfDefinition getCacheValueByKey(String selectMethodName,String firstKey, String secondKey) {
		if(ServiceNameConstants.SELECT_ACTIVE_FLOW_BY_DEF_ID.equals(selectMethodName)) {
			return selectActiveFlowByDefId(firstKey);
		}else if(ServiceNameConstants.SELECT_BY_DEF_ID_ACT_VERSION.equals(selectMethodName)) {
			return selectByDefIdActVersion(firstKey, secondKey);
		}else {
			throw new RuntimeException("not support select method:"+selectMethodName);
		}
	}
	
	@Override
	public List<WfDefinition> getCacheListValueByKey(String selectMethodName, String firstKey, String secondKey) {
		if(ServiceNameConstants.SELECT_BY_DEF_ID.equals(selectMethodName)) {
			return selectByDefId(firstKey);
		}
		throw new RuntimeException("not support select method:"+selectMethodName);
	}

	/**
	 * @Description: 根据流程编码查询流程定义列表
	 * @Param: [processDefId]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfDefinition>
	 * @Author:
	 * @Date: 2021/10/27
	 */
	private List<WfDefinition> selectByDefId(String processDefId) {
		if(StrUtil.isEmpty(processDefId)) {
			throw new RuntimeException("processDefId can't be null");
		}

		List<WfDefinition> result = super.getCacheListValueByKey(null, new String[] {processDefId});
		if( ObjectUtil.isEmpty(result) || (!CacheUtil.USE_CACHE)) {
			result = wfDefinitionService.selectByDefId(processDefId);
			if(ObjectUtil.isNotEmpty(result)) {
				super.putCacheListValueByKey(null,  new String[] {processDefId}, result);
			}			
		}
		return result;
	}

	private WfDefinition selectByDefIdActVersion(String processDefId, String versionStr) {
		if(StrUtil.isEmpty(processDefId) || versionStr == null) {
			throw new RuntimeException("processDefId and version can't be null");
		}
		Integer version = Integer.valueOf(versionStr);
		WfDefinition result =super.getCacheValueByKey(null, new String[] {processDefId,version.toString()});
		if( result == null || (!CacheUtil.USE_CACHE)) {
			WfDefinition param = new  WfDefinition();
			param.setDefId(XmlUtil.getProcessIdOnly(processDefId));
			param.setVersion(version);
			
			result = wfDefinitionService.selectByDefIdVersion(param);

			if(result != null) {
				super.putCacheValueByKey(null,  new String[] {processDefId,version.toString()}, result);
			}
		}
		return result;
	}

	private WfDefinition selectActiveFlowByDefId(String processCode) {
		if(StrUtil.isEmpty(processCode)) {
			throw new RuntimeException("processCode can not be null");
		}
		WfDefinition result =super.getCacheValueByKey(null, new String[] {processCode});
		if( result == null || (!CacheUtil.USE_CACHE)) {
			result = wfDefinitionService.selectActiveFlowByDefId(processCode);
			if(result!=null) {
				super.putCacheValueByKey(null,  new String[] {processCode}, result);
			}
		}
		
		return result;
	}

}
