package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigInteger;

/**
 * 流程超时处理信息
 */
@Setter
@Getter
public class ProcessTimoutInfo {

    private static Logger logger = LoggerFactory.getLogger(ProcessTimoutInfo.class);

    /**
     * 获取流程超时配置信息
     * @param wfModelpropExtendsService
     * @param processDefId
     * @return
     */
    public static ProcessTimoutInfo getProcessTimeoutInfo(WfModelPropExtendsService wfModelpropExtendsService, String processDefId) {
        // 流程超时信息
        ProcessTimoutInfo processTimoutInfo = new ProcessTimoutInfo();
        // 查询流程超时处理配置信息
        WfModelPropExtends wfModelPropExtends = wfModelpropExtendsService.selectByOwnIdPropName(XmlUtil.getProcessIdOnly(processDefId), ModelProperties.timeoutExecution.getName());

        if (wfModelPropExtends != null && StrUtil.isNotEmpty(wfModelPropExtends.getPropValue())) {

            JSONObject timeoutExecutionJb = JSONObject.parseObject(wfModelPropExtends.getPropValue());
            // 超时时间
            if (timeoutExecutionJb.get(BpmConstants.TIMEOUT_TIME) != null) {
                processTimoutInfo.overtimeTime = timeoutExecutionJb.getIntValue(BpmConstants.TIMEOUT_TIME);
            }
            // 超时时间单位 0-时 1-天
            if (timeoutExecutionJb.get(BpmConstants.TIME_UNIT) != null) {
                processTimoutInfo.overtimeTimeUnit = timeoutExecutionJb.getString(BpmConstants.TIME_UNIT);
            }
            // 超时处理机制
            if (timeoutExecutionJb.get(BpmConstants.OVERTIME_EXECUTION) != null) {
                processTimoutInfo.overtimeExecution = TimeoutExecution.getTimeoutExecutionByValue(timeoutExecutionJb.getString(BpmConstants.OVERTIME_EXECUTION));
            }
        }

        // 查询流程超时预警处理配置信息
        wfModelPropExtends = wfModelpropExtendsService.selectByOwnIdPropName(XmlUtil.getProcessIdOnly(processDefId), ModelProperties.timeoutPreWarnExecution.getName());

        if (wfModelPropExtends != null && StrUtil.isNotEmpty(wfModelPropExtends.getPropValue())) {

            JSONObject timeoutExecutionJb = JSONObject.parseObject(wfModelPropExtends.getPropValue());
            // 超时时间
            if (timeoutExecutionJb.get(BpmConstants.TIMEOUT_TIME) != null) {
                processTimoutInfo.overtimePreWarnTime = timeoutExecutionJb.getIntValue(BpmConstants.TIMEOUT_TIME);
            }
            // 超时时间单位 0-时 1-天
            if (timeoutExecutionJb.get(BpmConstants.TIME_UNIT) != null) {
                processTimoutInfo.overtimePreWarnTimeUnit = timeoutExecutionJb.getString(BpmConstants.TIME_UNIT);
            }
            // 消息模板编号
            if (timeoutExecutionJb.get(BpmConstants.MSG_TEMPLATE_NO) != null) {
                processTimoutInfo.overtimePreWarnTemplate = timeoutExecutionJb.getString(BpmConstants.MSG_TEMPLATE_NO);
            }
        }
        return processTimoutInfo;
    }

    /**
     * 超时预警时间
     */
    private int overtimePreWarnTime;
    /**
     * 超时预警时间单位 0-时 1-天
     */
    private String overtimePreWarnTimeUnit;
    /**
     * 超时预警模板
     */
    private String overtimePreWarnTemplate;

    /**
     * 超时时间
     */
    private int overtimeTime;
    /**
     * 超时时间单位 0-时 1-天
     */
    private String overtimeTimeUnit;
    /**
     * 超时处理机制
     */
    private TimeoutExecution overtimeExecution;

    /**
     * 流程参数
     */
    private String execVariable;
    /**
     * 流程定义编码
     */
    private String processDefId;
    /**
     * 版本
     */
    private BigInteger version;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("===========ProcessTimoutInfo=============").append(System.lineSeparator());
        sb.append(" version").append(version).append("  ");
        sb.append(" processDefId").append(processDefId).append("  ");
        sb.append(" execVariable").append(execVariable).append("  ");

        sb.append("overtimeTime").append(overtimeTime).append("  ");
        sb.append("overtimeTimeUnit").append(overtimeTimeUnit).append("  ");
        sb.append("overtimeExecution").append(overtimeExecution).append("  ");

        sb.append("overtimePreWarnTime").append(overtimePreWarnTime).append("  ");
        sb.append("overtimePreWarnTimeUnit").append(overtimePreWarnTimeUnit).append("  ");
        sb.append("overtimePreWarnTemplate").append(overtimePreWarnTemplate);
        return sb.toString();
    }
}
