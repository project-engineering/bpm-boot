package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.*;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Log4j2
@Component
public class TaskUtil {
    
    @Resource
    private TaskService taskService;
    @Resource
    private RepositoryService repositoryService;    
    @Resource
    private RuntimeService runtimeService;
    
    @Resource
    private UruleServiceFactory urule;

    public void completeServiceTask(String serviceTaskId,String executionId,String processInstanceId,String processDefinitionId,Map<String,Object> variables,boolean isCallback) {
    	log.info("| - TaskUtil>>>>>about to do service task complete work,taskid:{}",serviceTaskId);
    	if(isCallback) {    		
    		Task task = getTaskFollowServiceTask(serviceTaskId,executionId, processInstanceId, processDefinitionId);
    		dealRoute(serviceTaskId,executionId,processDefinitionId,task.getProcessInstanceId(), variables);
//    		log.info("about to complete auto add user task:"+task.getName()+" "+task.getId()+" getProcessInstanceId:"+task.getProcessInstanceId());
    		taskService.complete(task.getId());	
    	}else {
    		dealRoute(serviceTaskId,executionId,processDefinitionId,processInstanceId, variables);
    	}
    	
    }
    
    private String getUruleName(String serviceTaskId,String processDefinitionId) {
    	ServiceTask serviceTask = 	getDefinServiceTask(serviceTaskId,processDefinitionId);
    	if(serviceTask == null|| serviceTask.getExtensionElements() == null ||serviceTask.getExtensionElements().size()==0) {
    		return null;
    	}

    	for(Map.Entry<String, List<ExtensionElement>> anEntry:serviceTask.getExtensionElements().entrySet()) {
    		if("routeRule".equals(anEntry.getKey())) {
    			if(anEntry.getValue()!=null) {
        			for(ExtensionElement extensionElement:anEntry.getValue()) {
        				if(extensionElement.getAttributes()!=null) {
        					List<ExtensionAttribute> theExtensionAttribute = extensionElement.getAttributes().get("package");
        					if(theExtensionAttribute!=null &&theExtensionAttribute.size()>0) {
        						return theExtensionAttribute.get(0).getValue();
        					}
        				}
        			}
    			}
    		}
    	}
    	return null;
    }
    
    private void dealRoute(String serviceTaskId,String executionId,String processDefinitionId,String processInstanceId,Map<String,Object> variables) {
    	String uruleName = getUruleName(serviceTaskId, processDefinitionId);

    	if(StrUtil.isEmpty(uruleName)) {
    		return ;
    	}
    	Map<String,Object> execMap = runtimeService.getVariables(executionId);

    	if(execMap==null) {
    		runtimeService.setVariable(executionId, "emptyJustForDummyPurpose", "");
    		execMap = runtimeService.getVariables(executionId);
    	}

    	if(variables!=null && variables.size()>0) {
    		execMap.putAll(variables);
    	}
		String outFlowName = urule.getInstance().execByRuleName(uruleName, execMap);

		log.info("| - TaskUtil>>>>>outFlowName:{},executionId:{},processInstanceId:{}",outFlowName,executionId,processInstanceId);
		runtimeService.setVariable(executionId, XmlUtil.GATEWAY_ROUTE_CONDITION_KEY, GatewayRouteCondition.getInstance(outFlowName));

    }
    
    private ServiceTask getDefinServiceTask(String serviceTaskId,String processDefinitionId) {
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);		
    	if(bpmnModel == null) {
    		return null;
    	}

    	log.info("| - TaskUtil>>>>>get by taskUtil.getTaskFollowServiceTask bpmnModel  getId():{}",bpmnModel.getSourceSystemId());
    	Collection<FlowElement> flowElements = bpmnModel.getMainProcess().getFlowElements();  

    	if(flowElements == null||flowElements.size()==0) {
    		return null;
    	}

    	for(FlowElement flowElement:flowElements) {
    		log.info("| - TaskUtil>>>>>flowElement:  id:{}, getName:{} getAttributes:{} getDocumentation:{}",flowElement.getId(),flowElement.getName(),flowElement.getAttributes(),flowElement.getDocumentation());
    		if(serviceTaskId.equals(flowElement.getId())){
    			return (ServiceTask)flowElement;
    		}    		
    	}
    	return null;
    }
  
    public Task getTaskFollowServiceTask(String serviceTaskId,String executionId,String processInstanceId,String processDefinitionId) {
    	log.info("| - TaskUtil>>>>>TaskUtil.getTaskFollowServiceTask :{}",serviceTaskId);
    	ServiceTask defineServiceTask = 	getDefinServiceTask(serviceTaskId,processDefinitionId);
    	if(defineServiceTask == null) {
    		return null;
    	}
		SequenceFlow sequenceFlow = defineServiceTask.getOutgoingFlows().get(0);
    	UserTask followUserTask = (UserTask)sequenceFlow.getTargetFlowElement();
		log.info("| - TaskUtil>>>>>followUserTask:  id:{} ,getName:{} ,getAttributes:{} ,getDocumentation:{}",followUserTask.getId(),followUserTask.getName(),followUserTask.getAttributes(),followUserTask.getDocumentation());

    	List<Task> theTasks = taskService.createTaskQuery().taskDefinitionKey(followUserTask.getId()).list();
    	if(theTasks == null||theTasks.size()==0) {
    		return null;
    	}
    	Task theFollowTask = null;
    	for(Task task:theTasks) {
    		if(executionId.equals(task.getExecutionId())) {
    			theFollowTask =task;
    		}
    		
    		log.info("| - TaskUtil>>>>>passing executionId:{}, processInstanceId:{},theFollowTask instance: id:{} getExecutionId:{}",executionId,processInstanceId,theFollowTask.getId(),theFollowTask.getExecutionId());
    	}
		log.info("| - TaskUtil>>>>>theFollowTask instance: id:{} , getExecutionId:{}",theFollowTask.getId(),theFollowTask.getExecutionId());

    	return theFollowTask;
    }

    public static final String AUTO_ADD_POST_FIX = "_auto_add";
	public static String getAutoAddUserTaskPropertyKey(String svcTaskPropertyKey) {
		return svcTaskPropertyKey+AUTO_ADD_POST_FIX;
	}

}
