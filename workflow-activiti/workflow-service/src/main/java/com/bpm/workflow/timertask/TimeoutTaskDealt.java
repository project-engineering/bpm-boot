package com.bpm.workflow.timertask;

import com.bpm.workflow.service.WfParamService;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.TaskTimoutUtil;
import com.bpm.workflow.util.ThreadUtil;
import lombok.extern.log4j.Log4j2;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Log4j2
public class TimeoutTaskDealt extends Thread {
    private ThreadUtil threadUtil;
    private WfParamService wfParamService;

    public TimeoutTaskDealt(ThreadUtil threadUtil, WfParamService wfParamService) {
        this.threadUtil = threadUtil;
        this.wfParamService = wfParamService;
    }

    private long delaySecond = -1;

    public void setDelaySecond(long delaySecond) {
        log.info("| - TimeoutTaskDealt>>>>>set delay second:{}", delaySecond);
        this.delaySecond = delaySecond;
    }

    private Date startDate = null;

    public void setStartTime(String startTime) throws ParseException {
        log.info("| - TimeoutTaskDealt>>>>>set startTime:{}", startTime);
        String today = (new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
        this.startDate = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse(today + " " + startTime);
        log.info("| - TimeoutTaskDealt>>>>>after set startTime:{}", startTime);
    }

    private void waitTillStartDate() {
        while (Calendar.getInstance().getTime().compareTo(startDate) < 0) {
            try {
                log.debug("| - TimeoutTaskDealt>>>>>not reach:{} keep wating....", startDate);
                TimeUnit.SECONDS.sleep(60);
            } catch (InterruptedException e) {
                log.error("| - TimeoutTaskDealt>>>>>定时扫描线程异常", e);
            }
        }
        log.info("| - TimeoutTaskDealt>>>>>reach:{} stop wating and start timeoutDaemon....", startDate);
    }

    @Override
    public void run() {
        try {
            waitTillStartDate();
            runImpl();
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e), e);
        }
    }


    public void runImpl() throws Exception {
        boolean theValue = true;
        while (theValue) {
            while (TimerUtil.isStop) {
                TimeUnit.MILLISECONDS.sleep(100);
            }
            if (TaskTimoutUtil.isDebug) {
                delaySecond = 1;
            }
            if (wfParamService.canExecTimeoutTask(delaySecond) && delaySecond > 0) {
                doTimeout();
            }

            if (delaySecond == -1) {
                theValue = false;
            } else {
                if (TaskTimoutUtil.isDebug) {
                    TimeUnit.MILLISECONDS.sleep(100);
                } else {
                    TimeUnit.SECONDS.sleep(delaySecond);
                }

            }
        }

    }

    private void doTimeout() {
        TimeoutTaskExec timeoutTaskExec = new TimeoutTaskExec();
        threadUtil.execute(timeoutTaskExec);
    }

}
