package com.bpm.workflow.timertask;

import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.TaskTimoutUtil;
import lombok.extern.log4j.Log4j2;
import java.util.Calendar;

@Log4j2
public class TimeoutTaskExec implements Runnable{
	@Override
	public void run() {
		log.debug("| - TimeoutTaskExec>>>>>doTimeout begin {}",Calendar.getInstance().getTime());
		try {
			TaskTimoutUtil.doTimeoutClean();
		}catch (Exception e) {
			log.error(BpmStringUtil.getExceptionStack(e));
		}
		log.debug("| - TimeoutTaskExec>>>>>doTimeout end {}",Calendar.getInstance().getTime());
	}
}
