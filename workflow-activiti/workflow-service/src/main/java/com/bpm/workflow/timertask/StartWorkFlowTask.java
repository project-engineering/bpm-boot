package com.bpm.workflow.timertask;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.dto.transfer.WorkFlowStartData;
import com.bpm.workflow.entity.WfExceptionData;
import com.bpm.workflow.mapper.WfExceptionDataMapper;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.util.ErrorDataUtil;
import com.bpm.workflow.util.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import java.util.Map;

/**
 * 定时启动流程
 */
@Log4j2
public class StartWorkFlowTask extends WorkflowTimerTask {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String taskId;
	
	private String processInstId;
	
	private String processDefId;

	private WorkFlowStartData workFlowStartData;

	public StartWorkFlowTask(String workflowcode, String taskId, String beforUserId,  Map<String, Object> variableMap) {
		WorkFlowStartData workFlowStartData = new WorkFlowStartData();
		workFlowStartData.setProcessCode(workflowcode);
		workFlowStartData.setUserId(beforUserId);
		variableMap.put(Constants.ASSIGN_TASK_ID, taskId);
		workFlowStartData.setBussinessMap(variableMap);
	}

	public StartWorkFlowTask(WorkFlowStartData workFlowStartData) {
		this.workFlowStartData = workFlowStartData;
	}

	@Override
	public void runImpl() throws Exception {
		ProcessCoreService processCoreService = SpringUtil.getBean(ProcessCoreService.class);//.getBean("processCoreServiceImpl");
		WfExceptionDataMapper wfExceptionDataMapper = SpringUtil.getBean(WfExceptionDataMapper.class);//getBean("wfExceptionDataMapper");
		String workflowcode = workFlowStartData.getProcessCode();
		Map<String, Object> variableMap = workFlowStartData.getBussinessMap();
		// 防止出现死循环
		if (variableMap.containsKey(Constants.ASSIGN_TASK_ID)) {
			WfExceptionData data = ErrorDataUtil.getSystemError(
					"转办的流程[" + variableMap.get(Constants.ASSIGN_TASK_ID) + "]已经发起，并选人失败", taskId, workflowcode,
					processInstId, processDefId, "startWorkFlow", "选人失败，启动转办流程", variableMap.toString());
			wfExceptionDataMapper.insert(data);
			return;
		}

		try {
			processCoreService.startWorkflowDef(workFlowStartData);
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e),e);
			WfExceptionData data = ErrorDataUtil.getSystemError(ExceptionUtils.getStackTrace(e), taskId, workflowcode,
					processInstId, processDefId, "startWorkFlow", "选人失败，启动转办流程", variableMap.toString());
			wfExceptionDataMapper.insert(data);

		}
	}

}
