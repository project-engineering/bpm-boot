package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfHistTaskflow;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfHistTaskflowService extends IService<WfHistTaskflow> {

    void insertWfHistTaskflow(WfHistTaskflow wfHistTaskflow);

    void updateByTaskId(WfHistTaskflow wfHistTaskflow);

    WfHistTaskflow selectByTaskId(String taskId);
}
