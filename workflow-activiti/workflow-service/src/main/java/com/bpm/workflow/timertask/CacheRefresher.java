package com.bpm.workflow.timertask;

import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.CacheUtil;
import com.bpm.workflow.util.TaskTimoutUtil;
import lombok.extern.log4j.Log4j2;
import java.util.concurrent.TimeUnit;

@Log4j2
public class CacheRefresher extends Thread{
	private static long delayMilliSecond = 30000;

	private CacheUtil cacheUtil;
	
	public static long getDelayMilliSecond() {
		if(TaskTimoutUtil.isDebug) {
			delayMilliSecond = 100L;
		}
		return delayMilliSecond;
	}
	public CacheRefresher(CacheUtil cacheUtil) {
		this.cacheUtil = cacheUtil;
	}

	@Override
	public void run() {
		try {
			if(!CacheUtil.USE_CACHE) {
				return;
			}
			runImpl();
		}catch(Exception e) {
			log.error(BpmStringUtil.getExceptionStack(e));
		}
	}

	public void runImpl() throws Exception {
		boolean theValue = true;
		while(theValue) {
			doRefreshe();
			TimeUnit.MILLISECONDS.sleep(delayMilliSecond);
		}

	}
	
	private void doRefreshe() {		
		cacheUtil.refreshAll();
	}

}
