package com.bpm.workflow.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.mapper.WfParamMapper;
import com.bpm.workflow.service.*;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 定时任务，定时清理超时任务
 */
@Log4j2
public class TaskTimoutUtil {

    public static boolean isDebug = false;

    @SuppressWarnings("unused")
    private static ProcessCoreService processCoreService;

    private static ProcessEngine processEngine;
    @SuppressWarnings("unused")
    private static WfModelPropExtendsService wfModelpropExtendsService;
    @SuppressWarnings("unused")
    private static CacheDaoSvc cacheDaoSvc;
    @SuppressWarnings("unused")
    private static ActivitiBaseService activitiBaseServer;
    private static ProcessUtil processUtil;
    @SuppressWarnings("unused")
    private static WfParamMapper wfParamMapper;
    @SuppressWarnings("unused")
    private static WfTemplateService wfTemplateService;
    @SuppressWarnings("unused")
    private static WfParamService wfParamService;
    @SuppressWarnings("unused")
    private static WfEvtServiceDefService wfEvtServiceDefService;

    public static void init(ProcessEngine processEngine, WfModelPropExtendsService wfModelpropExtendsService,
                            CacheDaoSvc cacheDaoSvc, ProcessCoreService processCoreService,
                            ActivitiBaseService activitiBaseServer, ProcessUtil processUtil, WfParamService wfParamService,
                            WfTemplateService wfTemplateService,
                            WfEvtServiceDefService wfEvtServiceDefService) {

        TaskTimoutUtil.processEngine = processEngine;
        TaskTimoutUtil.wfModelpropExtendsService = wfModelpropExtendsService;
        TaskTimoutUtil.cacheDaoSvc = cacheDaoSvc;
        TaskTimoutUtil.processCoreService = processCoreService;
        TaskTimoutUtil.activitiBaseServer = activitiBaseServer;
        TaskTimoutUtil.processUtil = processUtil;
        TaskTimoutUtil.wfTemplateService = wfTemplateService;
        TaskTimoutUtil.wfParamService = wfParamService;
        TaskTimoutUtil.wfEvtServiceDefService = wfEvtServiceDefService;
    }

    private static final int PAGE_SIZE = 10000;

    private static boolean isRunning = false;

    private static synchronized boolean getSetIsRunning(boolean isStart) {
        if (isStart) {
            if (isRunning) {
                return true;
            } else {
                isRunning = true;
                return false;
            }
        } else {
            isRunning = false;
        }

        return isRunning;
    }

    public static void doTimeoutClean() {
        try {
            if (getSetIsRunning(true)) {
                return;
            }
            // 查询所有开启了超时的流程
            List<String> processDefIds = processUtil.selectAllTimeOutProcess();
            // 若为空，则结束
            if(ObjectUtil.isEmpty(processDefIds)){
                return;
            }
            List<String> piIds = new ArrayList<>();
            // 分页查询 【开启了超时的流程 & 流程实例未结束 & 流程未超时】的流程实例
            int pageNumber = 1;
            while (pageNumber != -1) {
                pageNumber = doSetPageOfProcessInstance(processDefIds, pageNumber, piIds);
            }
            log.info("| - TaskTimoutUtil>>>>>piIds:{}", piIds.size());
            if (piIds.size() == 0) {
                return;
            }
            // 循环处理流程实例
            for (String piId : piIds) {
                try {
                    doTimeoutCleanSpecProcessInst(piId);
                } catch (Exception e) {
                    log.error("| - TaskTimoutUtil>>>>>timeout process error:{}", piId, e);
                }

            }
        } finally {
            getSetIsRunning(false);
        }
    }

    /**
     * 流程实例超时
     * @param piId
     */
    private static void doTimeoutCleanSpecProcessInst(String piId) {
        // 查询流程实例
        ProcessInstance processInstance = processEngine.getRuntimeService().createProcessInstanceQuery()
                .processInstanceId(piId).singleResult();
        // 流程实例为空则结束
        if (processInstance == null) {
            return;
        }
        // 查询流程实例下任务
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(piId).list();
        // 流程实例下任务为空则结束
        if (tasks == null || tasks.size() == 0) {
            return;
        }
        // 处理流程超时
        processUtil.doTimeoutCleanSpecProcessInstRtnEnd(processInstance, tasks.get(0));
        log.info("| - TaskTimoutUtil>>>>>doTimeoutCleanSpecProcessInstRtnEnd-----------------------");

        // 处理任务超时
        for (Task task : tasks) {
            try {
                processUtil.doTimeoutCleanSpecTaskInst(processInstance, task);
            } catch (Exception e) {
                log.error("| - TaskTimoutUtil>>>>>timeout task error:{}", task.getId(), e);
            }

        }
    }

    /**
     * 分页查询 【开启了超时的流程 & 流程实例未结束 & 流程未超时】的流程实例
     * @param processDefIds
     * @param pageNumber
     * @param piIds
     * @return
     */
    private static int doSetPageOfProcessInstance(List<String> processDefIds, int pageNumber, List<String> piIds) {
        log.info("| - TaskTimoutUtil>>>>>pageNumber:{}", pageNumber);

        List<WfInstance> pis = processUtil.selectNoTimeOutList(processDefIds, pageNumber, PAGE_SIZE);

        if(ObjectUtil.isEmpty(pis)){
            log.info("| - TaskTimoutUtil>>>>>pageNumber:{} {} ", pageNumber, "empty");
            return -1;
        }else {
            log.info("| - TaskTimoutUtil>>>>>pageNumber:{} {} ", pageNumber, pis.size());
        }
        // 添加到piIds
        for (WfInstance pi : pis) {
            piIds.add(pi.getProcessId());
        }
        // 判断若不满分页数据，则返回-1
        if (pis.size() < PAGE_SIZE) {
            return -1;
        }

        return pageNumber + 1;
    }

    public static final long NO_OVERTIME = -1;

    public static long getOvertimeMilliSecond(String confValue) {
        long theValue;
        if (StrUtil.isEmpty(confValue)) {
            return NO_OVERTIME;
        }
        try {
            theValue = (new BigDecimal(confValue)).multiply(BigDecimal.valueOf(1000)).longValue();
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e));
            return NO_OVERTIME;
        }

        if (isDebug) {
            return theValue;
        } else {
            return theValue * 3600;
        }
    }

    public static Date calcEstEndDate(Date startDateTime, Date estEndTime, long dua) {
        if (dua == NO_OVERTIME) {
            return null;
        }
        Date calc = DateUtils.addTimeoutDateWithMillisecond(startDateTime, dua);
        if (estEndTime != null) {
            if (calc.compareTo(estEndTime) > 0) {
                return calc;
            } else {
                return estEndTime;
            }
        }
        return calc;
    }

    /**
     * 计算超时时间
     * @param startDateTime
     * @param timeoutTime
     * @param timeUnit
     * @return
     */
    public static Date calcTimeOutTime(Date startDateTime, int timeoutTime, String timeUnit) {
        long timeMillions = 0;
        if(BpmConstants.TIME_UNIT_HOUR.equals(timeUnit)){
            timeMillions = (long) timeoutTime * 60 * 60 * 1000;
        }else if(BpmConstants.TIME_UNIT_DAY.equals(timeUnit)){
            timeMillions = (long) timeoutTime * 24 * 60 * 60 * 1000;
        }else if(BpmConstants.TIME_UNIT_SECOND.equals(timeUnit)){
            timeMillions = (long) timeoutTime * 1000;
        }
        return DateUtils.addTimeoutDateWithMillisecond(startDateTime, timeMillions);
    }
}
