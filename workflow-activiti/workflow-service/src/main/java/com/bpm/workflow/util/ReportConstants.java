package com.bpm.workflow.util;

public class ReportConstants {

	public static final String DB_USERID = "userId";
	public static final String DB_VALUE = "value";
	public static final String DB_USERMAP = "userMap";
	public static final String DB_USERNAME = "userName";
	public static final String DB_PROCESSCODE = "processCode";
	public static final String DB_PROCESSNAME = "processName";
	public static final String DB_ENDTYPE = "endType";
	public static final String DB_ISTIMEOUT = "isTimeOut";
	public static final String DB_ORGID = "orgId";
	public static final String DB_ORGNAME = "orgName";
	
	
	/**
	 * 未完成数量
	 */
	public static final String BEAN_UNDOCOUNT = "undoCount";
	/**
	 * 正常结束数量
	 */
	public static final String BEAN_NORMALEND = "normalEndCount";
	/**
	 * 提前结束数量
	 */
	public static final String BEAN_EARLYEND = "earlyEndCount";
	/**
	 * 超时结束数量
	 */
	public static final String BEAN_TIMEOUTEND = "timeoutEndCount";
	/**
	 * 手动结束数量
	 */
	public static final String BEAN_MANUALEND = "manualEndCount";
	/**
	 * 平均处理时长
	 */
	public static final String BEAN_AVGDOTIME = "avgDoTime";
	/**
	 * 正常完成平均时长
	 */
	public static final String BEAN_NOMALAVGDOTIME = "nomalAvgDoTime";
	/**
	 * 超时完成平均时长
	 */
	public static final String BEAN_TIMOUTAVGDOTIME = "timOutAvgDoTime";
	/**
	 * 流程处理时长（t<=4）数量
	 */
	public static final String BEAN_DUEDATA4 = "duedata4";
	/**
	 * 流程处理时长（4<t<=8）数量
	 */
	public static final String BEAN_DUEDATA4T8 = "duedata4t8";
	/**
	 * 流程处理时长（8<t<=12）数量
	 */
	public static final String BEAN_DUEDATA8T12 = "duedata8t12";
	/**
	 * 流程处理时长（12<t<=24）数量
	 */
	public static final String BEAN_DUEDATA12T24 = "duedata12t24";
	/**
	 * 流程处理时长（24<t<=48）数量
	 */
	public static final String BEAN_DUEDATA24T48 = "duedata24t48";
	/**
	 * 流程处理时长（48<t）数量
	 */
	public static final String BEAN_DUEDATAT48 = "duedatat48";
	/**
	 * 未超时处理数量
	 */
	public static final String BEAN_NOTTIMEOUTCOUNT = "notTimeOutCount";
	/**
	 * 已超时处理数量
	 */
	public static final String BEAN_TIMEOUTCOUNT = "timeOutCount";
	/**
	 * 流程处理时长（t<=2）数量
	 */
	public static final String BEAN_DUEDATA2 = "duedata2";
	/**
	 * 流程处理时长（2<t<=6）数量
	 */
	public static final String BEAN_DUEDATA2T6 = "duedata2t6";
	/**
	 * 流程处理时长（6<t<=12）数量
	 */
	public static final String BEAN_DUEDATA6T12 = "duedata6t12";
	/**
	 * 流程处理时长（24<t）数量
	 */
	public static final String BEAN_DUEDATAT24 = "duedatat24";
	
	/**
	 * EndType 0 正在运行
	 */
	public static final String ENDTYPE_0 = "0";
	/**
	 * EndType 1 流程挂起
	 */
	public static final String ENDTYPE_1 = "1";
	/**
	 * EndType 2 正常结束
	 */
	public static final String ENDTYPE_2 = "2";
	/**
	 * EndType 3 提前结束
	 */
	public static final String ENDTYPE_3 = "3";
	/**
	 * EndType 4 超时结束
	 */
	public static final String ENDTYPE_4 = "4";
	/**
	 * EndType 5手动结束
	 */
	public static final String ENDTYPE_5 = "5";
	/**
	 * IsTimeOut 0 未超时
	 */
	public static final String ISTIMEOUT_0 = "0";
	/**
	 * IsTimeOut 1 已超时
	 */
	public static final String ISTIMEOUT_1 = "1";
}
