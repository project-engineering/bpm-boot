package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfAcceditRecord;
import com.bpm.workflow.mapper.WfAcceditRecordMapper;
import com.bpm.workflow.service.WfAcceditRecordService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfAcceditRecordServiceImpl extends ServiceImpl<WfAcceditRecordMapper, WfAcceditRecord> implements WfAcceditRecordService {
    @Resource
    private WfAcceditRecordMapper wfAcceditRecordMapper;

    @Override
    public List<WfAcceditRecord> selectList(Map<String, Object> param) {
        return wfAcceditRecordMapper.selectList(param);
    }
}
