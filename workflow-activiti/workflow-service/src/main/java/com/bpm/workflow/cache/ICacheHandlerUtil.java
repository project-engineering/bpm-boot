package com.bpm.workflow.cache;

import org.dom4j.Element;

import java.util.List;

/**
 * @author zgf
 * @version V1.0
 * @Description: TODO
 * @date 2018/12/3 11:32
 */
public interface ICacheHandlerUtil {

    /**
     * 更新对象信息
     * @param cacheKey
     * @param value
     */
    public void updateObject(String cacheKey, Object value);

    /**
     * 更新全部对象信息
     * @param pattern
     * @param key
     * @param value
     */
    public void updateAllObject(String pattern, String key, Object value);

    /**
     * 获取对象信息
     * @param key
     * @return
     */
    public Object getObject(final String key);

    /**
     * 清空全部信息
     * @param pattern
     */
    public void removeAll(String pattern);

    /**
     * 移除对象
     * @param key
     */
    public void remove(final String key);

    /**
     * 判断是否有Key对应的对象
     * @param key
     * @return
     */
    public boolean hasKey(final String key);

    /**
     * 更新标签
     * @param key
     * @param element
     */
    public void updateElement(String key, Element element);

    /**
     * 获取标签
     * @param key
     * @return
     */
    public Element getElement(final String key);

    /**
     * 更新缓存列表
     * @param k
     * @param v
     */
    public void updateCacheList(String k, List<?> v);

    /**
     * 获取缓存列表
     * @param key
     * @param <T>
     * @return
     */
    public <T> List<T> getCacheList(String key);
}
