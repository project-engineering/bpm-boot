package com.bpm.workflow.util;


import com.bpm.workflow.entity.WfExceptionData;

/**
 * <table width="1000px" >
 * <thead><td>异常类型</td><td>异常分类</td><td>处理</td></thead>
 * <tbody>
 * <tr><td>E1-事件触发异常</td><td>50-流程启动触发异常<br>51-节点进入触发异常<br>52-节点离开触发异常<br>53-流程正常结束触发异常<br>54-流程异常结束触发异常<br>55-任务回退触发异常<br>56-任务撤回触发异常<br>57-任务挂起触发异常<br>58-任务挂起取消触发异常<br>59-流程终止取消触发异常<br>60-任务取消触发异常</td><td>对于流转异常，可点击重新指定，指定流转的目标节点，重新开启流程流转处理</td></tr>
 * <tr><td>F1-流转异常</td><td>70-分支流转判断异常<br>71-选人为空节点跳过流转异常<br>72-任务取消流转异常</td><td>对于事件触发异常，可点击重新触发按钮，重新发起事件服务调用</td></tr>
 * <tr><td>R1-规则调用异常</td><td>80-选人规则调用异常，<br>81-流转分支规则调用异常<br>82-进入条件判定调用异常<br>83-离开条件判定调用异常<br>84-流程编码适配选取调用异常<br>85-子流程选择规则调用异常</td><td>对应规则调用异常，可点击重新触发按钮，重新调用规则，并在规则调用成功之后，继续相应的流程控制处理</td></tr>
 * </tbody>
 * </table>
 * @author amt
 *
 */
public class ErrorDataUtil {

	/**
	 * E1 71-选人为空节点跳过流转异常
	 * @return
	 */
	public static WfExceptionData getChosePersionServiceError(String expMessage, String expTaskId, String evtServiceId,
															  String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("E1", "71", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	/**
	 * R1 80-选人规则调用异常，
	 * @return
	 */
	public static WfExceptionData getChosePersionDataError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("R1", "80", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	/**
	 * 02-选人规则属性未配置
	 * @return
	 */
	public static WfExceptionData getChosePersionAttrError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("P0", "02", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	
	/**
	 * 02-系统功能异常
	 * @return
	 */
	public static WfExceptionData getSystemError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("F4", "41", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	
	/**
	 * 02-系统功能异常
	 * @return
	 */
	public static WfExceptionData getDBError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("F4", "40", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	
	/**
	 * 超时标准1异常
	 * @return
	 */
	public static WfExceptionData getTimeOutError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("S1", "30", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	 
	
	/**
	 * 节点结束回调失败
	 * @return
	 */
	public static WfExceptionData getTaskIvockError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("S1", "14", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	
	/**
	 * 04-服务编码属性未配置
	 * @return
	 */
	public static WfExceptionData getServiceAttrError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("P0", "04", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}


	/**
	 * 03-任务编码属性未配置
	 * @return
	 */
	public static WfExceptionData getTaskAttrError( String expMessage, String expTaskId, String evtServiceId,
			String expProcId, String expProcCode, String expNodeType, String expNodeName, String expTransData)
	{
		return new WfExceptionData("P0", "03", expMessage, expTaskId, evtServiceId, expProcId, expProcCode, expNodeType, expNodeName, expTransData);
	}
	

}
