package com.bpm.workflow.service.common;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.CacheDaoSvc;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.GatewayUtil;
import com.bpm.workflow.util.XmlUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.*;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class ActivitiBaseService {

	@Resource
	private ProcessEngine processEngine;

	@Resource
	private RepositoryService repositoryService;

	@Resource
	private RuntimeService runtimeService;

	@Resource
	private TaskService taskService;

	@Resource
	private HistoryService historyService;

	@Resource
	private CacheDaoSvc cacheDaoSvc;

	/**
	 * 部署流程
	 * 
	 * @param workflowCode
	 * @param defXmlContent
	 * @return
	 */
	public Deployment deployWorkFlow(String workflowCode, String defXmlContent) {

		DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
		deploymentBuilder.name(workflowCode);

		if (!workflowCode.toUpperCase().endsWith(BpmConstants.BPM_FILE_SUFFIX_UPPER)) {
			deploymentBuilder.addString(workflowCode + BpmConstants.BPM_FILE_SUFFIX_LOWER, defXmlContent);
		} else {
			deploymentBuilder.addString(workflowCode, defXmlContent);
		}

		// deploymentBuilder.tenantId(tenantId)
		return deploymentBuilder.deploy();
	}
	
	/**
	 * 卸载流程
	 * @param cascade
	 */
	public void deleteWorkflow(String processCode,boolean cascade){
		repositoryService.deleteDeployment(processCode, cascade);
	}

	/**
	 * 启动流程
	 * 
	 * @param workflowCode
	 * @param variablesMap
	 * @return ProcessInstance
	 */
	public ProcessInstance startWorkFlow(String workflowCode, Map<String, Object> variablesMap)  {
		return runtimeService.startProcessInstanceByKey(workflowCode, variablesMap);

	}
	
	public void commitTask(String taskId, Map<String, Object> variables)  {
		Task theTask = getTask(taskId);
		String taskDefId =null;
		String processInstanceId = null;
		BigInteger processDefVersion = null;
		if(theTask != null) {
			taskDefId = theTask.getTaskDefinitionKey();
			processInstanceId = theTask.getProcessInstanceId();
			processDefVersion = XmlUtil.getProcessVersionByDefId(theTask.getProcessDefinitionId());
		}
		taskService.complete(taskId, variables);
		
		try {
			GatewayUtil.completTask(cacheDaoSvc,null,processEngine, taskDefId, processInstanceId,processDefVersion, variables);
		}catch(Exception e) {
			log.error("Gateway complete task failed",e);
		}
		
	}
	
	private Task getTask(String taskId) {
		return taskService.createTaskQuery().taskId(taskId).singleResult();
	}

	public void assignerTask(String taskId, String userId) {
		taskService.setAssignee(taskId, userId);
	}

	/**
	 * 委派任务
	 * 
	 * @param taskId
	 * @param userId
	 */
	public void delegateTask(String taskId, String userId) {
		taskService.delegateTask(taskId, userId);
	}

	/**
	 * 查询子任务
	 * 
	 * @param parentTaskId
	 * @return
	 */
	public List<Task> findSubTaskList(String parentTaskId) throws Exception {
		return taskService.getSubTasks(parentTaskId);
	}

	/**
	 * 查询兄弟任务
	 * 
	 * @return
	 */
	public List<Task> findBrothersTaskList(String taskId) throws Exception {
		TaskEntity task = findTaskById(taskId);
		if (task != null) {
			return findSubTaskList(task.getParentTaskId());
		}
		return null;
	}

	/**
	 * 根据任务ID获得任务实例
	 * 
	 * @param taskId
	 *            任务ID
	 * @return TaskEntity
	 */
	public TaskEntity findTaskById(String taskId)  {
		TaskEntity task = (TaskEntity) taskService.createTaskQuery().taskId(taskId).singleResult();
		if (task == null) {
			throw new ServiceException(ErrorCode.WF000029, taskId);
		}
		return task;
	}

	/**
	 * 根据任务ID获取流程定义
	 * 
	 * @param taskId
	 *            任务ID
	 * @return
	 * @throws Exception
	 */
	public ProcessDefinitionEntity findProcessDefinitionEntityByTaskId(String taskId) throws Exception {
		// 取得流程定义
		ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(findTaskById(taskId).getProcessDefinitionId());

		if (processDefinition == null) {
			throw new Exception("流程定义未找到!");
		}
		return processDefinition;
	}

	/**
	 * 查询指定任务节点的最新记录
	 * 
	 * @param processInstance
	 *            流程实例
	 * @param activityId
	 * @return
	 */
	public HistoricActivityInstance findHistoricUserTask(ProcessInstance processInstance, String activityId) {
		HistoricActivityInstance rtnVal = null;
		// 查询当前流程实例审批结束的历史节点
		List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery()
				.activityType("userTask").processInstanceId(processInstance.getId()).activityId(activityId).finished()
				.orderByHistoricActivityInstanceEndTime().desc().list();
		if (historicActivityInstances.size() > 0) {
			rtnVal = historicActivityInstances.get(0);
		}

		return rtnVal;
	}

	/**
	 * 根据流程实例ID和任务key值查询所有同级任务集合
	 * 
	 * @param processInstanceId
	 * @param key
	 * @return
	 */
	public List<Task> findTaskListByKey(String processInstanceId, String key) {
		TaskQuery taskQuery = taskService.createTaskQuery();
		if (StrUtil.isNotBlank(processInstanceId)) {
			taskQuery = taskQuery.processInstanceId(processInstanceId);
		}
		if (StrUtil.isNotBlank(key)) {
			taskQuery = taskQuery.taskDefinitionKey(key);
		}
		return taskQuery.list();
	}

	/**
	 * 根据任务ID获取对应的流程实例
	 * 
	 * @param taskId
	 *            任务ID
	 * @return
	 * @throws Exception
	 */
	public ProcessInstance findProcessInstanceByTaskId(String taskId)  {
		// 找到流程实例
		String processInstId = findTaskById(taskId).getProcessInstanceId();
		return findProcessInstance(processInstId);
	}

	/**
	 * 根据流程实例ID获取流程实例
	 * 
	 * @param processInstId
	 * @return
	 * @throws Exception
	 */
	public ProcessInstance findProcessInstance(String processInstId) {
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstId)
				.singleResult();
		if (processInstance == null) {
			throw new ServiceException("流程实例未找到!");
		}
		return processInstance;
	}

	/**
	 * 获取activiti的命令执行器
	 */
	public CommandExecutor findCommandExecutor() {
		CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) processEngine
				.getProcessEngineConfiguration()).getCommandExecutor();
		return commandExecutor;
	}

	/**
	 * 获取历史任务实例列表
	 * 
	 * @param userId
	 * @param processInstanceId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<HistoricActivityInstance> findHistoricUserTasks(String userId, String processInstanceId, Date startTime,
			Date endTime) {
		// 查询当前流程实例审批结束的历史节点
		List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery()
				.activityType("userTask").processInstanceId(processInstanceId).finished()
				.orderByHistoricActivityInstanceEndTime().desc().list();
		return historicActivityInstances;
	}

	/**
	 * 获取对应的模型
	 * 
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	public FlowElement getBpmnModelNodeByTaskId(String taskId) {
		TaskEntity task = findTaskById(taskId);
		return getBpmnModelNodeByTaskId(task);
	}

	/**
	 * 获取对应的模型
	 * @return
	 * @throws Exception
	 */
	public FlowElement getBpmnModelNodeByTaskId(TaskEntity task)  {
		String processDefinitionId = task.getProcessDefinitionId();
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);

		Execution execution = runtimeService.createExecutionQuery().executionId(task.getExecutionId()).singleResult();
		String activityId = execution.getActivityId();

		return bpmnModel.getFlowElement(activityId);
	}

	/**
	 * 获取对应的模型
	 * @return
	 * @throws Exception
	 */
	public BpmnModel getBpmnModelNode(String processDefinitionId)  {
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
		return bpmnModel;
	}

	/**
	 * 创建新的子任务
	 * @param delegateTask
	 * @param userId
	 * @return
	 */
	public TaskEntity createNewTask(DelegateTask delegateTask, String userId) {
		String nextId = ((ProcessEngineConfigurationImpl) processEngine.getProcessEngineConfiguration())
				.getIdGenerator().getNextId();
		TaskEntity task = (TaskEntity) taskService.newTask(nextId);
		task.setAssignee(userId);
		task.setName(delegateTask.getName());
		task.setProcessDefinitionId(delegateTask.getProcessDefinitionId());
		task.setProcessInstanceId(delegateTask.getProcessInstanceId());
		task.setParentTaskId(delegateTask.getId());
		task.setDescription("jointProcess");
		task.setExecutionId(delegateTask.getExecutionId());
		taskService.saveTask(task);
		return task;
	}

	/**
	 * 查找流程定义对象
	 * 
	 * @param workflowCode
	 * @return
	 */
	public ProcessDefinition findProcessDefinition(String workflowCode) {
		List<ProcessDefinition> resultList = findProcessDefinitions(workflowCode);
		if (resultList != null && resultList.size() > 0) {
			return resultList.get(0);
		}
		return null;
	}
	
	/**
	 * 查找流程定义对象列表
	 * 
	 * @param workflowCode
	 * @return
	 */
	public List<ProcessDefinition> findProcessDefinitions(String workflowCode){
		 return repositoryService.createProcessDefinitionQuery()
				.processDefinitionKey(workflowCode).orderByProcessDefinitionVersion().desc().list();
		
	}

	/**
	 * 删除流程实例
	 * 
	 * @param processInstanceId
	 * @param reason
	 */
	public void deleteProcessInstance(String processInstanceId, String reason) {
		runtimeService.deleteProcessInstance(processInstanceId, reason);
	}

//	/**
//	 * 获取流程变量
//	 * 
//	 * @param taskId
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	public Map<String, Object> getTaskExectionVarMap(String taskId,String executionId) throws Exception {
//		if(StrUtil.isNotBlank(executionId)){
//			return getTaskExectionVarMap(executionId);
//		}
//		return getTaskExectionVarMap(findTaskById(taskId));
//	}
//
//	/**
//	 * 获取流程变量
//	 * 
//	 * @param task
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	public Map<String, Object> getTaskExectionVarMap(TaskEntity task) throws Exception {
//		return getTaskExectionVarMap(task.getExecutionId());
//	}
//	
//	
//	private Map<String, Object>  getTaskExectionVarMap(String executionId) throws Exception {
//		return runtimeService.getVariables(executionId);
//	}
//
//	/**
//	 * 设置流程变量
//	 * 
//	 * @param taskId
//	 * @param varMap
//	 * @throws Exception
//	 */
//	@Override
//	public void setTaskExectionVarMap(String taskId, Map<String, Object> varMap) throws Exception {
//		setTaskExectionVarMap(findTaskById(taskId), varMap);
//	}
//
//	/**
//	 * 设置流程变量
//	 * 
//	 * @param task
//	 * @param varMap
//	 */
//	@Override
//	public void setTaskExectionVarMap(TaskEntity task, Map<String, Object> varMap) {
//		runtimeService.setVariables(task.getExecutionId(), varMap);
//	}
//	
//	/**
//	 * 获取历史流程变量
//	 * 
//	 * @param exectionId
//	 * @param processInstId
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	public Map<String, Object> getHistExectionVarMap(String executionId, String processInstId) throws Exception{
//		HistoricVariableInstanceQuery histQuery = historyService.createHistoricVariableInstanceQuery();
//		if(StrUtil.isNoneBlank(executionId)){
//			histQuery = histQuery.executionId(executionId);
//		}
//		if(StrUtil.isNoneBlank(processInstId)){
//			histQuery =histQuery.processInstanceId(processInstId);
//		}
//		List<HistoricVariableInstance> tempList = histQuery.list();
//		Map<String,Object> resultMap = new HashMap<String,Object>();
//		for(HistoricVariableInstance instance:tempList){
//			resultMap.put(instance.getVariableName(), instance.getValue());
//		}
//		return resultMap;
//	}

	public void suspendProcessInstance(String workflowCode, String processInstId, boolean suspendProcessInstances,
			Date suspensionDate)  {
		if(!StrUtil.isEmpty(processInstId)) {
			runtimeService.suspendProcessInstanceById(processInstId);
		}else {
			repositoryService.suspendProcessDefinitionByKey(workflowCode, suspendProcessInstances, suspensionDate);
		}
	}

	public void activateProcessInstance(String workflowCode, String processInstId, boolean suspendProcessInstances,
			Date suspensionDate)  {
		if(StrUtil.isEmpty(workflowCode)) {
			runtimeService.activateProcessInstanceById(processInstId);
		}else {
			repositoryService.activateProcessDefinitionByKey(workflowCode, suspendProcessInstances, suspensionDate);
		}		
	}

}
