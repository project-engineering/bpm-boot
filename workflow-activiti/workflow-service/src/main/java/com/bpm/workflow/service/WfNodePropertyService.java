package com.bpm.workflow.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.WorkFlowNodePropData;
import com.bpm.workflow.entity.WfNodeProperty;
import com.bpm.workflow.vo.NodeDefinitionQueryRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfNodePropertyService extends IService<WfNodeProperty> {

    void insertNodeProperty(WorkFlowNodePropData nodeData);

    void deleteNodeProperty(String nodeId);

    void updateNodeProperty(WorkFlowNodePropData nodeData);

    IPage<WfNodeProperty> selectList(NodeDefinitionQueryRequest nodeDefinitionQueryRequest);
}
