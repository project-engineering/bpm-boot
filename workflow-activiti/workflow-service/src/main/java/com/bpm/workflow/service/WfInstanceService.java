package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfInstance;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfInstanceService extends IService<WfInstance> {

    WfInstance selectByProcessId(String processInstId);

    List<WfInstance> selectWfInstanceByTaskIds(List<String> taskIdList);

    List<WfInstance> selectNoTimeOutList(Map<String, Object> param, PageData<WfInstance> pageData);

    void insertWfInstance(WfInstance wfInstance);

    WfInstance selectInstanceByPtaskId(String taskId);
}
