package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfParamService extends IService<WfParam> {

    List<WfParam> selectParamList(WfParam wfParam);

    List<WfParam> selectParamByParamTypeParamCode(String paramTypeProcesscode, String systemIdCode);

    List<WfParam> selectPramByProcessCode(String processCode);

    int deleteById(String id);

    void insertWfParam(WfParam wfParam);

    boolean canExecTimeoutTask(long delaySecond);

    void insertWfParam(WfParam param, List<String> itemChannelTypeList);

    int updateById(WfParam param, List<String> itemChannelTypeList);

    List<WfParam> selectParamList(Map<String, Object> param, PageData<WfParam> pageData);
}
