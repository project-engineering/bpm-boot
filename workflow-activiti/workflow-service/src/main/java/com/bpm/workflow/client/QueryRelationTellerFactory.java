package com.bpm.workflow.client;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zgf
 * @version V1.0
 * @Description: TODO
 * @date 2019/1/30 18:02
 */
@Component
public class QueryRelationTellerFactory {

    @Resource
    private RestQueryRelationTeller restQueryRelationTeller;
    
    
    @Value("${callType.protocol:rest}")
    private String callType;

    public   IQueryRelationTeller getInstance(){
        StrUtil.isBlank(callType);
        return restQueryRelationTeller;
    }
}
