package com.bpm.workflow.timertask;

import com.bpm.workflow.service.WfRuIdentitylinkService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import java.util.concurrent.TimeUnit;

@Log4j2
public class WfRuIdentitylinkDisposeTask extends Thread {
    @Resource
    private WfRuIdentitylinkService wfRuIdentitylinkService;

    public void setWfRuIdentitylinkService(WfRuIdentitylinkService wfRuIdentitylinkService) {
        this.wfRuIdentitylinkService = wfRuIdentitylinkService;
    }

    private static WfRuIdentitylinkDisposeTask wfRuIdentitylinkDisposeTaskInst = null;

    private WfRuIdentitylinkDisposeTask() {

    }

    public static synchronized void startSingleton(WfRuIdentitylinkService wfRuIdentitylinkService) {
        if (wfRuIdentitylinkDisposeTaskInst == null) {
            wfRuIdentitylinkDisposeTaskInst = new WfRuIdentitylinkDisposeTask();
            wfRuIdentitylinkDisposeTaskInst.wfRuIdentitylinkService = wfRuIdentitylinkService;
            wfRuIdentitylinkDisposeTaskInst.setDaemon(true);
            wfRuIdentitylinkDisposeTaskInst.start();
        }
    }

    @Override
    public void run() {
        int i = 30;
        while (i > 0) {
            try {
                runImpl();
            } catch (Exception e) {
                log.error("| - WfRuIdentitylinkDisposeTask>>>>>定时任务执行失败！", e);
            }
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                log.error("| - WfRuIdentitylinkDisposeTask>>>>>任务休眠失败！", e);
            }
        }

    }

    private void runImpl() {
        wfRuIdentitylinkService.deleteNotActive();
    }
}
