package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfUploadDefinitionDetail;
import com.bpm.workflow.mapper.WfUploadDefinitionDetailMapper;
import com.bpm.workflow.service.WfUploadDefinitionDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfUploadDefinitionDetailServiceImpl extends ServiceImpl<WfUploadDefinitionDetailMapper, WfUploadDefinitionDetail> implements WfUploadDefinitionDetailService {

}
