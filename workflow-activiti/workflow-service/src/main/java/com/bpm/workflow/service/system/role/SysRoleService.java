package com.bpm.workflow.service.system.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.role.SysRole;
import com.bpm.workflow.vo.system.role.RoleParam;
import com.bpm.workflow.vo.system.role.RoleSelectType;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysRoleService extends IService<SysRole> {
    IPage<SysRole> getList(RoleParam param);

    List<RoleSelectType> getRoleSelectList();

    boolean addRole(SysRole sysRole);
}
