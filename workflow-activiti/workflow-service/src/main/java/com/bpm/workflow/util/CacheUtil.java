package com.bpm.workflow.util;

import com.bpm.workflow.cache.EhCacheUtil;
import com.bpm.workflow.cache.RedisCacheUtil;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.entity.WfParam;
import com.bpm.workflow.service.WfParamService;
import com.bpm.workflow.util.cacheimpl.*;
import com.bpm.workflow.timertask.CacheRefresher;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

@Log4j2
@Component
public class CacheUtil {

    private final Logger logger = LoggerFactory.getLogger(CacheUtil.class);
    public static final boolean USE_CACHE = true;
    public static CacheType cacheTypeSingleton = null;

    public CacheType getCacheType() {
        if(cacheTypeSingleton == null) {
            String theCacheType = configUtil.getCacheType();
            log.info("| - CacheUtil>>>>>CacheType-->{}",theCacheType);
            cacheTypeSingleton = CacheType.getEnumByValue(theCacheType);
        }
        return cacheTypeSingleton;
    }


    private static RedisCacheUtil redisCacheUtil;
    private static EhCacheUtil ehCacheUtil;
    public synchronized RedisCacheUtil getRedisCacheUtil() {
        if(getCacheType()== CacheType.redis) {
            if(redisCacheUtil == null) {
                redisCacheUtil = (RedisCacheUtil)SpringUtil.getConfigurableApplicationContext().getBean(RedisCacheUtil.class);
            }
            return redisCacheUtil;
        }

        return null;
    }

    public synchronized EhCacheUtil getEhCacheUtil() {
        if(getCacheType()== CacheType.echache) {
            if(ehCacheUtil == null) {
                ehCacheUtil = (EhCacheUtil)SpringUtil.getConfigurableApplicationContext().getBean(EhCacheUtil.class);
            }
            return ehCacheUtil;
        }
        return null;
    }

    public void testIfObjectInRedis(String key) {
        try {
            this.getRedisCacheUtil().getObject(CacheActionBase.VALUES_KEY + key);
        }catch(Exception e) {
            log.error("| - CacheUtil>>>>>获取缓存信息失败",e);
        }
    }

    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfParamService wfParamService;

    private static ReentrantLock wfParamBLock = null;
    private synchronized static ReentrantLock getWfParamLock() {
        if(wfParamBLock == null) {
            wfParamBLock = new ReentrantLock();
        }
        return wfParamBLock;
    }

    public enum CacheType{
        local("local"),echache("echache"),redis("redis");
        private String name;
        private CacheType(String name) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
        public static CacheType getEnumByValue(String theName)
        {
            for(CacheType cacheType: CacheType.values()) {
                if(cacheType.getName().equals(theName)){
                    return cacheType;
                }
            }
            throw new RuntimeException("not supported type:"+theName);
        }

    }

    public enum SvcType{
        wfDefinition("wfDefinition"),wfParam("wfParam"),wfModelPropExtends("wfModelPropExtends"),wfDefinePropExtends("wfDefinePropExtends");
        private String name;
        private SvcType(String name) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
        public static SvcType getEnumByValue(String theName)
        {
            for(SvcType taskConfType: SvcType.values()) {
                if(taskConfType.getName().equals(theName)){
                    return taskConfType;
                }
            }
            throw new RuntimeException("not supported type:"+theName);
        }

    }

    @Resource
    public CacheActionWfParamLocal cacheActionWfParamLocal;
    @Resource
    public CacheActionWfModelpropExtendsLocal cacheActionWfModelpropExtendsLocal;
    @Resource
    public CacheActionWfDefinitionLocal cacheActionWfDefinitionLocal;
    @Resource
    public CacheActionWfDefinePropExtendsLocal cacheActionWfDefinePropExtendsLocal;

    @SuppressWarnings("rawtypes")
    public CacheActionBase getCacheActionBase(SvcType theSvcType) {
        CacheActionBase cacheActionBase = null;
        if(theSvcType == SvcType.wfDefinition) {
            cacheActionBase =  cacheActionWfDefinitionLocal;
        }else if(theSvcType == SvcType.wfParam) {
            cacheActionBase =  cacheActionWfParamLocal;
        }else if(theSvcType == SvcType.wfModelPropExtends) {
            cacheActionBase =  cacheActionWfModelpropExtendsLocal;
        }else if(theSvcType == SvcType.wfDefinePropExtends) {
            cacheActionBase =  cacheActionWfDefinePropExtendsLocal;
        }else {
            throw new RuntimeException("not implements yet");
        }
        if(getCacheType() == CacheType.local) {
            return cacheActionBase;
        }else if(getCacheType() == CacheType.echache) {
            return cacheActionBase;
        //throw new RuntimeException("not support echache cache yet");
        }else if(getCacheType() == CacheType.redis) {
            return cacheActionBase;
        }else {
            throw new RuntimeException("unknown cache Type:"+getCacheType().getName());
        }
    }

    private Set<String> lastChecks = new HashSet<>();
    @Transactional
    public void refreshAll() {
        getWfParamLock().lock();
        try {
            Set<String> thisChecks = new HashSet<>();
            long latestCheck = System.currentTimeMillis() - 3 * CacheRefresher.getDelayMilliSecond();
            WfParam cond = new WfParam();
            cond.setParamType(Constants.PARAM_TYPE_CACHE_REFRESH);
            List<WfParam> toBeRefreshList = wfParamService.selectParamList(cond);
            Set<WfParam> toBeRemoved = new HashSet<>();
            for(WfParam refreshLog:toBeRefreshList) {
                if(Long.parseLong(refreshLog.getParamObjCode())<latestCheck) {
                    toBeRemoved.add(refreshLog);
                }else {
                    thisChecks.add(refreshLog.getId());
                    if(!lastChecks.contains(refreshLog.getId())) {
                        SvcType svcType = SvcType.getEnumByValue(refreshLog.getParamCode());
                        getCacheActionBase(svcType).refreshByKey(svcType, null, new String[] {refreshLog.getParamName(),refreshLog.getParamObjName()},false);
                    }
                }
            }
            for(WfParam wfParam:toBeRemoved) {
                wfParamService.deleteById(wfParam.getId());
            }
            lastChecks = thisChecks;
        }catch(Exception e) {
            log.error("| - CacheUtil>>>>>刷新缓存异常：",e);
            throw new RuntimeException(e);
        }finally {
            getWfParamLock().unlock();
        }

    }


    private static int uniqValue = NumberConstants.INT_ZERO;
    private static int getUniqValue() {
        if(uniqValue >= NumberConstants.INT_TEN) {
            uniqValue = NumberConstants.INT_ZERO;
        }else {
            uniqValue++;
        }
        return uniqValue;
    }
    public static synchronized String createUniqId(String prefix) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        sb.append(System.currentTimeMillis());
        sb.append(getUniqValue());

        return sb.toString();
    }



    @Transactional
    public void logRefresh(SvcType svcType,String refreshKey,String secondRefreshKey,boolean notChecked) {

        getWfParamLock().lock();
        try {
            WfParam wfParam = new WfParam();
            wfParam.setId(createUniqId("CACHE"));
            wfParam.setParamObjCode(""+System.currentTimeMillis());
            wfParam.setParamType(Constants.PARAM_TYPE_CACHE_REFRESH);
            wfParam.setParamCode(svcType.getName());
            wfParam.setParamName(refreshKey);
            if(secondRefreshKey==null) {
                wfParam.setParamObjName(CacheActionBase.NULL_KEY_VALUE);
            }else {
                wfParam.setParamObjName(secondRefreshKey);
            }

            wfParam.setParamObjType("1");
            wfParamService.insertWfParam(wfParam);
            if(notChecked) {
                lastChecks.add(wfParam.getId());
            }
        }catch(Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e),e);
            throw new RuntimeException(e);
        }finally {
            getWfParamLock().unlock();
        }
    }
}