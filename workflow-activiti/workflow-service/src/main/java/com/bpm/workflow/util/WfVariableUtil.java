package com.bpm.workflow.util;

import com.bpm.workflow.dto.transfer.SysCommHead;
import lombok.extern.log4j.Log4j2;

import java.util.*;

/**
 *
 * @Description: 对流程变量的各种内存操作汇集
 */
@Log4j2
public class WfVariableUtil {
	
	/**
	 * 
	 */
	private static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();
	
	private static ThreadLocal<String> threadLocalProcessEndEventName = new ThreadLocal<String>();

	private static ThreadLocal<SysCommHead> sysCommHeadLocal = new ThreadLocal<SysCommHead>();

	/**
	 * 2019-10-21 
	 * 异步自动节点线程变量
	 */
	private static ThreadLocal<Map<String, Object>> autoLocal = new ThreadLocal<Map<String, Object>>();

	private static ThreadLocal<List<Map<String, Object>>> taskIdList = new ThreadLocal<List<Map<String, Object>>>();


	
	public static void clearfVariable()
	{
		threadLocal.remove();
		sysCommHeadLocal.remove();
		autoLocal.remove();
		taskIdList.remove();
		log.info("---------clearfVariable-------");
	}

	/**
	 * @Description: 设置结束事件名
	 * @Param: [endEventName]
	 * @Return: void
	 * @Author:
	 * @Date: 2021/9/23
	 */
	public static void setProcessEndEventName(String endEventName) {
		threadLocalProcessEndEventName.set(endEventName);
	}

	public static void setsysCommHead(SysCommHead sysCommHead)
	{
		sysCommHeadLocal.set(sysCommHead);
	}
	
	public static SysCommHead getSysCommHead()
	{
		return sysCommHeadLocal.get();
	}
	
	public static void clearTaskIdList() {
		taskIdList.remove();

	}
	
	
	public static List<Map<String, Object>> getTaskIdList() {
		return taskIdList.get();
	}
	
	public static void addTaskId(String taskId,String taskCode) {
		List<Map<String, Object>> list=taskIdList.get();
		if(Objects.isNull(list))
		{
			list=new ArrayList<Map<String, Object>>();
		}
		Map<String,Object> taskInfo=new HashMap<String,Object>();
		taskInfo.put("taskId", taskId);
		taskInfo.put("taskCode", taskCode);
		list.add(taskInfo);
		taskIdList.set(list);
	}
	
	/**
	 * 获取结束事件名
	 * 
	 * @return
	 */
	public static String getProcessEndEventName() {
		return threadLocalProcessEndEventName.get();
	}
	
	/**
	 * 设置流程变量
	 * 
	 * @param variableMap
	 */
	public static void setVariableMap(Map<String, Object> variableMap) {
		threadLocal.set(variableMap);
		log.info("----------after setVariableMap------------------"+getVariableMap());
	}

	/**
	 * 获取流程变量
	 * 
	 * @return
	 */
	public static Map<String, Object> getVariableMap() {
		return threadLocal.get();
	}

	/**
	 * 移除指定key的流程变量
	 * 
	 * @param key
	 */
	public static void removeVariableMap(String key) {
		Map<String, Object> variableMap = getVariableMap();
		if (variableMap != null) {
			variableMap.remove(key);
			setVariableMap(variableMap);
		}
		log.debug("| - WfVariableUtil>>>>>after removeVariableMap:{}",getVariableMap());
	}

	/**
	 * 增加流程变量
	 * 
	 * @param key
	 * @param value
	 */
	public static void addVariableMap(String key, Object value) {
		Map<String, Object> variableMap = getVariableMap();
		if (variableMap == null) {
			variableMap = new HashMap<String, Object>();
		}
		variableMap.put(key, value);
		setVariableMap(variableMap);
		log.debug("| - WfVariableUtil>>>>>after addVariableMap:{}",getVariableMap());
	}

	/**
	 * @Description: 增加多个流程变量
	 * @Param: [m]
	 * @Return: void
	 * @Author:
	 * @Date: 2021/9/23
	 */
	public static void addAllVariableMap(Map<String, Object> m) {
		Map<String, Object> variableMap = getVariableMap();
		if (variableMap == null) {
			variableMap = new HashMap<String, Object>();
		}
		variableMap.putAll(m);
		setVariableMap(variableMap);
		log.debug("| - WfVariableUtil>>>>>after addAllVariableMap:{}",getVariableMap());
	}
	
	/**
	 * 获取自动节点异步调用参数
	 * @author anmingtao
	 * @serialData 
	 * @return
	 */
	public static Map<String,Object> getAutoLocal() {
		return autoLocal.get();
	}
	
	/**
	 * @Description: 设置自动节点异步参数
	 * @Param: [key, obj]
	 * @Return: void
	 */
	public static void putAutoLocal(String key,Object obj) {
		Map<String, Object> autoMap =autoLocal.get();
		if (autoMap == null) {
			autoMap = new HashMap<String, Object>();
		}
		autoMap.put(key, obj);
		autoLocal.set(autoMap);
	}
}
