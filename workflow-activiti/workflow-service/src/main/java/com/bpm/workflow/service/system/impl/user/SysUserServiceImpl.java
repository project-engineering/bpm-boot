package com.bpm.workflow.service.system.impl.user;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.system.SysUserRole;
import com.bpm.workflow.entity.system.user.SysUser;
import com.bpm.workflow.mapper.system.user.SysUserMapper;
import com.bpm.workflow.service.system.SysUserRoleService;
import com.bpm.workflow.service.system.user.SysUserService;
import com.bpm.workflow.util.UtilValidate;
import com.bpm.workflow.vo.system.user.UserParam;
import jakarta.annotation.Resource;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    SysUserMapper mapper;
    @Resource
    SysUserRoleService sysUserRoleService;
    @Value("${jasypt.encryptor.password}")
    private String salt;

    @Transactional
    @Override
    public boolean addUser(SysUser sysUser) {
        //加密密码
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        //加密所需的salt(盐)
        encryptor.setPassword(salt);
        sysUser.setPassword(encryptor.encrypt(sysUser.getPassword()));
        sysUser.setUserId(IdUtil.simpleUUID());
        //新增用户
        int insert = mapper.insert(sysUser);
        //设置用户角色
        if (insert > 0) {
            SysUserRole sysUserRole = SysUserRole.builder()
                    .userRoleId(IdUtil.simpleUUID())
                    .userId(sysUser.getUserId())
                    .roleId(sysUser.getRoleId())
                    .build();
            sysUserRoleService.save(sysUserRole);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @Override
    public boolean updateUser(SysUser sysUser) {
        //更新用户
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        //加密所需的salt(盐)
        encryptor.setPassword(salt);
        sysUser.setPassword(encryptor.encrypt(sysUser.getPassword()));
        int i = mapper.updateById(sysUser);
        //更新用户角色，判断角色是否发生变化
        if (i > 0) {
           //先删除，在插入
           sysUserRoleService.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,sysUser.getUserId()));
           SysUserRole sysUserRole = SysUserRole.builder()
                   .userRoleId(IdUtil.simpleUUID())
                   .userId(sysUser.getUserId())
                   .roleId(sysUser.getRoleId())
                   .build();
           sysUserRoleService.save(sysUserRole);
           return true;
        } else {
           return false;
        }
    }

    @Transactional
    @Override
    public boolean deleteUser(String userId) {
        int i = mapper.deleteById(userId);
        if (i > 0) {
            sysUserRoleService.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,userId));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public IPage<SysUser> getList(UserParam param) {
        //构造分页对象
        IPage<SysUser> page = new Page<>(param.getStart(),param.getLimit());
        //构造查询条件
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        if (UtilValidate.isNotEmpty(param.getUserId())){
            queryWrapper.lambda().eq(SysUser::getUserId,param.getUserId());
        }
        if (UtilValidate.isNotEmpty(param.getUserName())){
            queryWrapper.lambda().like(SysUser::getUserName,param.getUserName());
        }
        if (UtilValidate.isNotEmpty(param.getPhone())){
            queryWrapper.lambda().like(SysUser::getPhone,param.getPhone());
        }
        if (UtilValidate.isNotEmpty(param.getNickName())){
            queryWrapper.lambda().like(SysUser::getNickName,param.getNickName());
        }
        queryWrapper.lambda().orderByDesc(SysUser::getCreateTime);
        //执行查询
        IPage<SysUser> list = mapper.selectPage(page,queryWrapper);
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        //加密所需的salt(盐)
        encryptor.setPassword(salt);
        list.getRecords().stream().forEach(item->item.setPassword(encryptor.decrypt(item.getPassword())));
        return list;
    }

    @Override
    public int resetPwd(String userId) {
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getUserId, userId);
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        //加密所需的salt(盐)
        encryptor.setPassword(salt);
        updateWrapper.set(SysUser::getPassword, encryptor.encrypt("123456"));
        return mapper.update(updateWrapper);
    }
}
