package com.bpm.workflow.cache;

public class CacheException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -53637632815857954L;
	public CacheException(String string) {
        super(string);
    }

    public CacheException() {
        super();
    }
}
