package com.bpm.workflow.timertask;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.entity.WfMessage;
import com.bpm.workflow.service.WfMessageService;
import com.bpm.workflow.util.ConfigUtil;
import com.bpm.workflow.util.EventServiceInvockUtil;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import java.time.LocalDateTime;
import java.util.Map;

@Log4j2
public class WfMsgNotifyTimerTask extends WorkflowTimerTask {
    /**
     *
     */
    private static final long serialVersionUID = 91829630057792109L;

    private WfMessage wfMessage;
    @Resource
    private WfMessageService wfMessageService;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private EventServiceInvockUtil eventServiceInvockUtil;
    @Resource
    private TimerUtil timerUtil;

    /**
     * 消息发送成功
     */
    private static final String SUCCESS = "0";

    /**
     * 消息发送失败
     */
    private static final String FAIL = "1";

    private Map<String, Object> businessMap;

    /**
     * @param wfMessage
     */
    public WfMsgNotifyTimerTask(WfMessage wfMessage, Map<String, Object> businessMap) {
        this.wfMessage = wfMessage;
        this.businessMap = businessMap;
    }

    @Override
    public void runImpl() throws Exception {
        if (wfMessage == null) {
            return;
        }

        String result = SUCCESS;
        try {
            boolean r = eventServiceInvockUtil.sendMessage(wfMessage, businessMap);
            if (!r) {
                result = FAIL;
            }
        } catch (Exception e) {
            result = FAIL;
            log.error("| - WfMsgNotifyTimerTask>>>>>消息[{}]发送失败！", wfMessage.getTaskId(), e);
        }
        if (StrUtil.equals(result, SUCCESS)) {
            // 发送成功
            wfMessage.setSmsTime(wfMessage.getSmsTime()+1);
            wfMessage.setResult(1);
            wfMessage.setUpdateTime(LocalDateTime.now());
            wfMessageService.updateByKeyId(wfMessage);
            log.info("| - WfMsgNotifyTimerTask>>>>>taskId:{}消息发送成功", wfMessage.getTaskId());
        } else {
            // 发送失败
            wfMessage.setSmsTime(wfMessage.getSmsTime()+1);
            wfMessage.setResult(2);
            wfMessage.setUpdateTime(LocalDateTime.now());
            wfMessageService.updateByKeyId(wfMessage);
            log.info("| - WfMsgNotifyTimerTask>>>>>taskId:{}消息第{}次发送失败", wfMessage.getTaskId(), wfMessage.getSmsTime().toString());

            // 迭代发送
            if (wfMessage.getSmsTime().intValue() >= configUtil.getMessageRetry()) {
                return;
            }
            WfMsgNotifyTimerTask msgTimerTask = new WfMsgNotifyTimerTask(wfMessage, businessMap);
            timerUtil.addWorkflowTimerTask(msgTimerTask, -1, -1, -1, -1, -1, wfMessage.getSmsTime().intValue() * 20);
        }

    }

}
