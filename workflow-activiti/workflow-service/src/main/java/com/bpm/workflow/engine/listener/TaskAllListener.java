package com.bpm.workflow.engine.listener;

import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

/**
 * 任务节点全局监听器
 *
 */
@Log4j2
public class TaskAllListener implements TaskListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -407533752676255278L;

	@Override
	public void notify(DelegateTask delegateTask) {
		log.info("TaskAllListener");
	}

}
