package com.bpm.workflow.engine.listener;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.util.WfVariableUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.InclusiveGateway;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Log4j2
@Component("sequenceFlowCadition")
public class SequenceFlowCadition {

    /**
     * 用于流程流转时的判断接口
     *
     * @param execution
     * @param id        流程定义传入的SequenceFlow的id
     * @return
     */
    public boolean mathcCadition(DelegateExecution execution, String id) {
        Map<String, Object> variableMap = WfVariableUtil.getVariableMap();
        return mathcCaditionCommon(variableMap, execution.getCurrentFlowElement(), id);
    }

    /**
     * 判断是否有符合条件的流转方向
     *
     * @param varMap  流程变量
     * @param element 当前节点元素
     * @return
     */
    public boolean mathcCaditionCheck(Map<String, Object> varMap, FlowElement element) {
        boolean result = false;
        if (element instanceof FlowNode) {
            FlowNode flowNode = (FlowNode) element;
            List<SequenceFlow> outList = flowNode.getOutgoingFlows();
            for (SequenceFlow flow : outList) {
                if (mathcCaditionCommon(varMap, element, flow.getId())) {
                    result = true;
                }
            }
        }

        // 如果没有一条符合的流转方向，则抛出异常
        if (!result) {
            throw new ServiceException("无法确认路由分支，请确认流程定义是否正确！");
        }
        return result;
    }

    /**
     * @param varMap
     * @param element
     * @param id      流程定义传入的SequenceFlow的id
     * @return
     */
    public boolean mathcCaditionCommon(Map<String, Object> varMap, FlowElement element, String id) {
        //增加对包容性网关流转的过滤
        if (element instanceof InclusiveGateway) {
            return true;
        }
        boolean result = false;// 查看指定Id的流转方向是否满足流转条件
        String routeResult = getRouteRuleResult(varMap);
        log.info(" | - SequenceFlowCadition>>>>>最终确认流转标识：{}", routeResult);
        if (StrUtil.isNotBlank(routeResult) && (element instanceof FlowNode)) {
            for (String outStr : routeResult.split(SymbolConstants.COMMA)) {
                for (SequenceFlow tempOut : ((FlowNode) element).getOutgoingFlows()) {
                    if (outStr.startsWith(Constants.SPECIFYNEXTNODE)) {
                        outStr = outStr.substring(Constants.SPECIFYNEXTNODE.length());
                        if (outStr.equals(id)) {
                            return true;
                        }
                    } else {
                        String flowId = tempOut.getDocumentation();
                        if (StrUtil.isBlank(flowId)) {
                            log.info(" | - SequenceFlowCadition>>>>>{}[{}]流转方向没有配置流转标识", tempOut.getName(), tempOut.getId());
                        } else if (tempOut.getId().equals(id)) {
                            String[] outList = flowId.split(SymbolConstants.COMMA);
                            for (String out : outList) {
                                if (out.equals(outStr)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            result = true;
        }

        return result;
    }

    /**
     * 从流程变量中获取路由规则的结果，多个值以逗号分隔
     *
     * @param varMap
     * @return
     */
    @SuppressWarnings("unchecked")
    private String getRouteRuleResult(Map<String, Object> varMap) {
        Object outValueObject = varMap.get(Constants.ROUTE_CONDITION_KEY);
        String result = "";
        if (outValueObject != null) {
            if (outValueObject instanceof JSONArray) {
                Iterator<Object> iter = ((JSONArray) outValueObject).iterator();
                StringBuffer resultBuf = new StringBuffer();
                while (iter.hasNext()) {
                    Map<String, Object> map = (Map<String, Object>) iter.next();
                    Object branchObj = map.get("branch");
                    if (branchObj != null) {
                        String specifyValue = branchObj.toString();
                        resultBuf.append(SymbolConstants.COMMA + specifyValue);
                    }
                }
                result = resultBuf.toString();
                if (StrUtil.isNotBlank(result)) {
                    result = result.substring(1);
                }
            } else {
                result = outValueObject.toString();
            }
        }
        return result;
    }
}
