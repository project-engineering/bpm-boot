package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import java.io.*;

@Component
@PropertySource(value={"classpath:/workflow/business.properties"}, ignoreResourceNotFound = true)
public class ConfigUtil {
	public static boolean isTest = false;
	
	@Value("${input.public.sysflag:I03}")
    public   String inputPublicSysFlag;
  
    @Value("${input.private.sysflag:I02}")
    public   String inputPrivateSysFlag;
    
    @Value("${input.sametrade.sysflag:I04}")
    public   String inputSametradeSysFlag;
	
    @Value("${auto.doactor:auto}")
    private String autoTaskActor;
    
    @Value("${checkGlobalNumber:false}")
    private String checkGlobalNumber;
  
    @Value("${cacheType:local}")
    private String cacheType;

    @Value("${nobusiness:true}")
    private String noBusiness;


    @Value("${nourule:false}")
    private String noUrule;

    @Value("${noUserModel:false}")
    private String noUserModel;
    
    
    @Value("${sendMessage.fail.retry:3}")
	private int sendMessageRetry;
    
    
    @Value("${mail.password:default}")
	private String mailPasswd;
    
    
    @Value("${mail.smtp.auth:default}")
	private String mailAuth;
    
    @Value("${mail.smtp.host:default}")
	private String mailSmtpHost;
    
    
    @Value("${mail.userName:default}")
	private String mailUserName;
    
    
    @Value("${urule.query.header:default}")
	private String emptyHeader;
    
    
    @Value("${backInvokeTimeOut:60000}")
	private int backInvokeTimeOut;
    
    
    @Value("${task.result:{\"0\":\"\\u540C\\u610F\",\"1\":\"\\u5426\\u51B3\",\"2\":\"\\u6253\\u56DE\\u4E0A\\u4E00\\u8282\\u70B9\",\"3\":\"\\u8FFD\\u56DE\",\"4\":\"\\u7EC8\\u6B62\",\"5\":\"\\u6253\\u56DE\\u62DF\\u7A3F\\u4EBA\",\"6\":\"\\u4EFB\\u52A1\\u8F6C\\u529E\",\"7\":\"\\u9000\\u56DE\\u4EFB\\u610F\\u8282\\u70B9\",\"n\":\"\\u81EA\\u52A8\\u5173\\u95ED\",}}")
    private String taskResultInfo;
    @Value("${isIgnoreProcessVersion:false}")
	private String ignoreProcessVersion;
    
    @Value("${showProcessUrl:http://localhost:9001/workflow-app/editor/#/showProcesses}")
	private String showProcessUrl;
    
    
    @Value("${urule.url:http://127.0.0.1:8787/urule-server/}")
	private String uruleUrl;
    
    @Value("${urule.timeout:60000}")
	private String uruleTimeOut;
    
    
    @Value("${bank.msg.type:com.workflow.devdefault.msg.WorkFlowDevDefaultMsg}")
	private String msgBankType;
    
    @Value("${cloud.queryRelationTeller.url:default}")
	private String queryRelationTellerCloud;
    
    @Value("${queryRelationTeller.url:default}")
	private String queryRelationTellerRest;
    
    
    @Value("${thirdParty.interface.callFlag:0}")
	private String queryRelationTellerType;
    
    
    @Value("${timerout.period.conf.string:-1,-1,-1,12,-1,1}")
	private String timeOutConf;
    
    
    @Value("${timerout.period.type:range}")
	private String timeOutType;
    
   
    @Value("${ timerout.period.conf.startTime:00:00:01}")
	private String timeOutStartTime;
    
    
    @Value("${timer.task.check.inteval:500}")
	private String taskCheckIntval;
    
    
    @Value("${file.default.loaction:default}")
	private String fileLocal;
    @Value("${file.default.encoding:utf-8}")
	private String fileEncode;
    
    
    @Value("${threadpool.size:20}")
	private String threadPoolSize;
	
    

	
	protected static String nodeId=null;
	public static synchronized String getNodeId() {
		if(nodeId == null) {
			nodeId = getNodeIdByFile();
		}
		return nodeId;
	}
	
	private static final String NODE_ID_FILE_NAME= "nodeIdFile";
	public static final String TEST_NODE_ID_FILE_NAME= "testNodeIdFile";
	private static String getNodeIdByFile() {
		String writeInFileNodeId = null;
		
		File f = new File(NODE_ID_FILE_NAME);
		if(isTest) {
			f = new File(TEST_NODE_ID_FILE_NAME);
		}
		if(!f.exists()) {
			writeInFileNodeId =  writeNewNodeIdToFile(f);
		}else {
			try(BufferedReader br = new BufferedReader(new FileReader(f))) {
				String str = br.readLine();
				if(StrUtil.isNotEmpty(str)){
					writeInFileNodeId = str.trim();
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			if(StrUtil.isEmpty(writeInFileNodeId)) {
				writeInFileNodeId = writeNewNodeIdToFile(f);
			}
		}
		return writeInFileNodeId;
	}
	
	private static String writeNewNodeIdToFile(File f ) {
		String writeInFileNodeId=BpmStringUtil.getUniqId();
		try(BufferedOutputStream bo= new BufferedOutputStream(new FileOutputStream(f));) {
			bo.write(writeInFileNodeId.getBytes("UTF-8"));
			bo.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return writeInFileNodeId;
	}
	
	
	

	

	public static final String SYSTEM_AUTO_ACTION_USER = "SYSTEM_AUTO_ACTION_USER";
	public static final String BANK_MSG_TYPE="bank.msg.type";
//	public String getProperty(String key) {
//		if(key.equals(SYSTEM_AUTO_ACTION_USER)) {
//			return SYSTEM_AUTO_ACTION_USER;
//		}
//		String value = getProperties().getProperty(key);
//		if(key.equals(BANK_MSG_TYPE)&&value ==null) {
//			log.warn("load workflow.properties from class path failed ,reload");
//			properties = null;
//			value = getProperties().getProperty(key);
//		}
//		return value;
//	}
	
 

	public  String getAutoTaskActor()
	{
		return autoTaskActor;
	}
	
	
	public String getCacheType() {
    	return cacheType;	
	}
	public boolean isNotBusiness() {
		return Boolean.valueOf(noBusiness);
	}
	public boolean isNoUrule() {
		return Boolean.valueOf(noUrule);
	}
	public boolean isNoUserModel() {
		return Boolean.valueOf(noUserModel);
	}
	
	public  int getMessageRetry() {
    	 
		return sendMessageRetry;
	}

    public String getMailAuth()
    {
    	 return mailAuth;	
    }
    
    public String getMailPassWd()
    {
    	return  mailPasswd;	
    }
    
    public String getMailSmtpHost()
    {
   		return  mailSmtpHost;	
    }
    
    public  String getMailUser()
    {
    	return  mailUserName;	
    }
    
    public String getEmptyHttpHeader()
    {
    	return  emptyHeader;	
    }
    /**
     * 获取回调服务超时时间
     */
    public int getBackInvokeServiceTimeOut()
    {
    	 
    	return backInvokeTimeOut;
    }
 
    /**
     * 获取结果信息
     * @return
     */
    public String getTaskResultInfo()
    {
 
    	return taskResultInfo;
    }
    
  
    public static boolean debug_isIgnoreProcessVersion = false;
    
    /**
     * 是否忽略版本控制，即取最新的属性配置
     * 
     * @return
     */
	public boolean isIgnoreProcessVersion() {
		if(isTest) {
			return debug_isIgnoreProcessVersion;
		}
		return Boolean.valueOf(ignoreProcessVersion);
		
	}
	public String getDefaultProcessUrl() {

		 
		return showProcessUrl;
	}

	public String getUruleUrl() {
		return uruleUrl;
	}

	public String getUruleTimeOut() {
		return uruleTimeOut;
	}

	public String getMsgBankType() {
		return msgBankType;
	}

	public String getQueryRelationTellerCloud() {
		return queryRelationTellerCloud;
	}

	public String getQueryRelationTellerRest() {
		return queryRelationTellerRest;
	}

	public String getQueryRalationTellerType() {
		return queryRelationTellerType;
	}

	public String getTimeOutConf() {
		return timeOutConf;
	}

	public String getTimeOutType() {
		return timeOutType;
	}

	public String getTimeOutStartTime() {
		return timeOutStartTime;
	}

	public String getTaskCheckIntval() {
		return taskCheckIntval;
	}

	public String getFileLocl() {
		return fileLocal;
	}

	public String getFileEncode() {
		return fileEncode;
	}

	public String getThreadpoolSize() {
		return threadPoolSize;
	}

	public String getInputPublicSysFlag() {
		return inputPublicSysFlag;
	}

	public void setInputPublicSysFlag(String inputPublicSysFlag) {
		this.inputPublicSysFlag = inputPublicSysFlag;
	}

	public String getInputPrivateSysFlag() {
		return inputPrivateSysFlag;
	}

	public void setInputPrivateSysFlag(String inputPrivateSysFlag) {
		this.inputPrivateSysFlag = inputPrivateSysFlag;
	}
	
	public String getInputSametradeSysFlag() {
		return inputSametradeSysFlag;
	}

	public String getCheckGlobalNumber() {
		return checkGlobalNumber;
	}

	public void setCheckGlobalNumber(String checkGlobalNumber) {
		this.checkGlobalNumber = checkGlobalNumber;
	}
}
