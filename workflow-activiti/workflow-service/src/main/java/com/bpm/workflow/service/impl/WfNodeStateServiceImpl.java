package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfNodeState;
import com.bpm.workflow.mapper.WfNodeStateMapper;
import com.bpm.workflow.service.WfNodeStateService;
import com.bpm.workflow.util.UtilValidate;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfNodeStateServiceImpl extends ServiceImpl<WfNodeStateMapper, WfNodeState> implements WfNodeStateService {
    @Resource
    private WfNodeStateMapper wfNodeStateMapper;

    @Override
    public List<WfNodeState> selectList(Map<String, Object> param) {
        LambdaQueryWrapper<WfNodeState> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(param.get("eventCode"))) {
            queryWrapper.eq(WfNodeState::getEventType, param.get("eventCode"));
        }
        if (UtilValidate.isNotEmpty(param.get("stateType"))) {
            queryWrapper.eq(WfNodeState::getStateType, param.get("stateType"));
        }
        if (UtilValidate.isNotEmpty(param.get("processCode"))) {
            queryWrapper.eq(WfNodeState::getProcessCode, param.get("processCode"));
        }
        if (UtilValidate.isNotEmpty(param.get("nodeId"))) {
            queryWrapper.eq(WfNodeState::getNodeId, param.get("nodeCode"));
        }
        return wfNodeStateMapper.selectList(queryWrapper);
    }
}
