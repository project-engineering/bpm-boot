package com.bpm.workflow.client;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.util.ConfigUtil;
import com.bpm.workflow.util.WfVariableUtil;
import com.bpm.workflow.vo.UserAskListQueryRequest;
import com.bpm.workflow.vo.UserAskQueryRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service(value = "restQueryRelationTeller")
public class RestQueryRelationTeller extends QueryRelationTeller {

	private static final String USER_ASK_LIST = "userAskList";
	private static final String USER_LIST = "userList";

	@Resource
	private WorkflowFeignClientService workflowFeignClientService;
	@Resource
	private ConfigUtil configUtil;


	@Override
	public List<UserOutputData> queryUserList(JSONArray userAskListJson, Map<String, String> umap,
											  Map<String, Object> varMap) throws ServiceException {
		UserAskListQueryRequest queryRequest;
		Set<String> userAddSet = new HashSet<>();
		Set<String> userDelSet = new HashSet<>();

		for(int i = 0; i < userAskListJson.size(); i++){
			if (umap != null) {
				JSONObject jsonObj = userAskListJson.getJSONObject(i);
				String  userAdd = getUserProp(jsonObj, "userAdd");
				String  userDel = getUserProp(jsonObj, "userDel");
				if (StrUtil.isNotBlank(userAdd)) {
					String uid = this.getUserAddOp(umap, userAdd);
					if (uid != null) {
						userAddSet.add(uid);
					}
				}
				if (!StrUtil.isEmpty(userDel)) {
					String uid = this.getUserAddOp(umap, userDel);
					if (uid != null) {
						userDelSet.add(uid);
					}
				}
			}
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("userAskList", userAskListJson);
		queryRequest = JSONUtil.toBean(jsonObject.toJSONString(), UserAskListQueryRequest.class);
		List<UserAskQueryRequest> userAskList = queryRequest.getUserAskList();

		if(userAskList == null){
			userAskList = new ArrayList<>();
		}

		// 新增用户处理
		if (ObjectUtil.isNotEmpty(userAddSet)) {
			for (String str : userAddSet) {
				UserAskQueryRequest userAskQueryRequest = new UserAskQueryRequest();
				userAskQueryRequest.setOrderUserNo(str);
				userAskList.add(userAskQueryRequest);
			}
		}
		if(ObjectUtil.isEmpty(varMap)){
			varMap = WfVariableUtil.getVariableMap();
		}
		queryRequest.setUserAskList(userAskList);
		if(ObjectUtil.isNotEmpty(varMap)){
			if(varMap.containsKey("firstNode")){
				queryRequest.setStartUserFlag(Boolean.parseBoolean(varMap.get("firstNode").toString()));
			}
			queryRequest.setSystemIdCode((String) varMap.get("systemIdCode"));
		}
		List<UserOutputData> userInfoList = workflowFeignClientService.findUserInfo(queryRequest);
		// 删除用户处理
		if(ObjectUtil.isNotEmpty(userDelSet) && ObjectUtil.isNotEmpty(userInfoList)) {
			userInfoList = userInfoList.stream().filter(userInfo -> !userDelSet.contains(userInfo.getUserId())).collect(Collectors.toList());
		}
		return userInfoList;
 	}

	@Override
	public List<UserOutputData> queryUserList(String userId) {
		UserAskListQueryRequest queryRequest = new UserAskListQueryRequest();
		List<UserAskQueryRequest> userAskList = new ArrayList<>();

		// 调用规则后使用模拟用户数据取自Java
		if (configUtil.isNotBusiness()) {
			return generateUserData();
		}
		// 查询流程发起人信息
		UserAskQueryRequest userAskQueryRequest = new UserAskQueryRequest();
		userAskQueryRequest.setOrderUserNo(userId);
		userAskList.add(userAskQueryRequest);
		queryRequest.setUserAskList(userAskList);
		queryRequest.setStartUserFlag(true);
		return workflowFeignClientService.findUserInfo(queryRequest);
	}

	@Override
	public List<UserOutputData> queryUserList(JSONArray userAskListJson) throws ServiceException {
		return queryUserList(userAskListJson, null, null);
	}
}
