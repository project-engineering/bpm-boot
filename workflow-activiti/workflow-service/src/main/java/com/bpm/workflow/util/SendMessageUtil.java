package com.bpm.workflow.util;

public abstract class SendMessageUtil {
	    public abstract boolean send(String to, String title, String msg, String filePath);  
}
