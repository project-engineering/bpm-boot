package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskFlowData;
import com.bpm.workflow.entity.TaskCodeCount;
import com.bpm.workflow.entity.WfTaskflow;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfTaskflowService extends IService<WfTaskflow> {

    WfTaskflow selectByTaskId(String taskId);

    List<WfTaskflow> selectByCondition(WfTaskflow condation);

    void insertWfTaskflow(WfTaskflow taskflow);

    List<WfTaskflow> selectByProcessInstId(String processInstanceId);

    /**
     * 根据用户列表查询代办数量
     * @param list
     * @return
     */
    List<TaskCodeCount> selectUnoTaskCountByUsers(List<String> list);

    void selectByIdForUpdate(Integer id);

    /**
     * 任务处理结果 通知 （分页）
     * @param taskFlowData
     * @param selectPageData
     * @return
     */
    List<WfTaskflow> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> selectPageData);

    /**
     * 任务处理结果 通知
     * @param taskFlowData
     * @return
     */
    List<WfTaskflow> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData);

    WfTaskflow overTimeTaskList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> selectPageData);

    List<WfTaskflow> selectSubproListByProcessInstId(String processInstId);

    void deleteById(Integer id);

    int selectCheckOutCount(WorkFlowTaskFlowData taskFlowData);

    List<WfTaskflow> selectByTaskIds(List<String> taskIdList);

    List<WfTaskflow> selectUndoTaskFlow(Map<String, Object> param);

    List<TaskCodeCount> selectTaskCodeCount(Map<String, Object> param);

    int checkGlobSeqNoIsExsit(Map<String, Object> paramMap);
}
