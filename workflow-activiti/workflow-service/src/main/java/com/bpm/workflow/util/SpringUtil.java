package com.bpm.workflow.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringUtil implements ApplicationContextAware, BeanFactoryAware {

	private static ApplicationContext applicationContext = null;

	/**
	 *
	 */
	private static BeanFactory beanFactory;

	/**
	 *
	 */
	public SpringUtil() {
		if (beanFactory != null) {
			((AutowireCapableBeanFactory) beanFactory).autowireBeanProperties(this,
					AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);
		}
	}

	/**
	 *
	 */
	@Override
	public void setApplicationContext(ApplicationContext configurableApplicationContext) {
		if (SpringUtil.applicationContext == null) {
			SpringUtil.applicationContext = configurableApplicationContext;
		}
	}

	public static ApplicationContext getConfigurableApplicationContext() {
		return applicationContext;
	}

	/**
	 *
	 */
	public void shutdown() {
		if (applicationContext != null) {
			((ConfigurableApplicationContext) applicationContext).close();

		}
	}

	/**
	 * 取得存储在静态变量中的ApplicationContext.
	 */
	public static ApplicationContext getApplicationContext() {
		checkApplicationContext();
		return applicationContext;
	}

	/**
	 * 从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T getBean(String name) {
//		checkApplicationContext();
//		return (T) applicationContext.getBean(name);
//	}

	/**
	 * 从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	public static <T> T getBean(Class<T> clazz) {
		checkApplicationContext();
		return (T) applicationContext.getBean(clazz);
	}

	/**
	 * 清除applicationContext静态变量.
	 */
	public static void cleanApplicationContext() {
		applicationContext = null;
	}

	private static void checkApplicationContext() {
		if (applicationContext == null) {
			throw new IllegalStateException("applicaitonContext未注入,请在applicationContext.xml中定义SpringContextHolder");
		}
	}


	/**
	 *
	 */
	@SuppressWarnings("static-access")
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	public static Object getBean(String beanName) {
		if (beanFactory == null) {
			throw new NullPointerException("BeanFactory is null!");
		}
		return beanFactory.getBean(beanName);
	}

}
