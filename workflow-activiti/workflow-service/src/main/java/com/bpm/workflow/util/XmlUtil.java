package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.autotask.BaseServiceImpl;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.urule.URuleRepoTreeTransferData;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.common.WfDefinePropExtendsService;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultElement;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.*;

@Log4j2
public class XmlUtil {

    public static String formatXml(String str) {
        Document doc = XmlUtil.getDoc(str);
        return formatXml(doc);
    }

    public static String formatXml(Document doc) {
        try (StringWriter out = new StringWriter();) {
            OutputFormat formater = OutputFormat.createPrettyPrint();
            formater.setEncoding("utf-8");

            XMLWriter writer = new XMLWriter(out, formater);
            writer.write(doc);

            writer.close();

            return out.toString();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static Document getDoc(String xmlStr) {
        SAXReader reader = new SAXReader();
        reader.setValidation(false);
        Document doc = null;
        try (StringReader in = new StringReader(xmlStr);) {
            reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            doc = reader.read(in);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return doc;
    }

    public static Document getDoc(InputStream in) {
        SAXReader reader = new SAXReader();
        reader.setValidation(false);
        Document doc = null;
        try {
            reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            doc = reader.read(in);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return doc;
    }


    public static final String GATEWAY_ROUTE_CONDITION_KEY = "GATEWAY_ROUTE_CONDITION_KEY";

    public static XmlConvertResult convertEditor2starndard(String editorBpmn, String prevEditorBpmn, boolean isIgnoreProcessVersion) {
        return convertEditor2starndard(editorBpmn, false, prevEditorBpmn, isIgnoreProcessVersion);
    }

    public static final int USE_DB_PROCESS_XML_PROPERTY = -1;

    public static int getProcessAttrVersion(boolean isIgnoreProcessVersion, String defAttrVersionValues, BigInteger curVersion) {
        if (curVersion == null) {
            return 0;
        }
        int theCurVersion = curVersion.intValue();
        if (defAttrVersionValues == null) {
            return theCurVersion;
        }

        if (theCurVersion < 1) {
            return theCurVersion;
        }
        String[] versions = defAttrVersionValues.split(",");
        if (versions == null || versions.length == 0) {
            return theCurVersion;
        }
        TreeSet<Integer> versionsInOrder = new TreeSet<Integer>();
        for (int i = 0; i < versions.length; i++) {
            try {
                versionsInOrder.add(Integer.valueOf(versions[i]));
            } catch (Exception e) {
                return theCurVersion;
            }
        }

        if (isIgnoreProcessVersion) {
            Iterator<Integer> it = versionsInOrder.iterator();

            while (it.hasNext()) {
                int thisCheckVersion = it.next();
                if (theCurVersion == thisCheckVersion) {
                    return getAttrVersionXmlBiggest(it, thisCheckVersion);
                }
            }
            return theCurVersion;
        } else {
            return curVersion.intValue();
        }
    }

    private static int getAttrVersionXmlBiggest(Iterator<Integer> it, int curVersion) {
        int lastVersion = curVersion;
        while (it.hasNext()) {
            int thisVersion = it.next();
            if (thisVersion - lastVersion == 1) {
                lastVersion = thisVersion;
                continue;
            } else {
                return lastVersion;
            }
        }
        return USE_DB_PROCESS_XML_PROPERTY;
    }

    private static boolean chkPrevElementByThisElementData(Element parentPrevEle, Element element) {
        String elementId = element.attributeValue(XML_ELE_PROCESS_ID);
        if (element.getName() == null || elementId == null) {
            return true;
        }
        for (Object ele : parentPrevEle.elements()) {
            Element prevElement = (Element) ele;
            if (prevElement == null) {
                continue;
            }
            if (!element.getName().equals(prevElement.getName())) {
                continue;
            }
            if (elementId.equals(prevElement.attributeValue(XML_ELE_PROCESS_ID))) {
                if (XML_ELE_SEQUENCE_FLOW.equals(element.getName())) {
                    if (StrUtil.isEmpty(element.attributeValue(XML_ELE_SEQUENCE_FLOW_SOURCE_REF)) || StrUtil.isEmpty(element.attributeValue(XML_ELE_SEQUENCE_FLOW_TARGET_REF))) {
                        return true;
                    }
                    if (!element.attributeValue(XML_ELE_SEQUENCE_FLOW_SOURCE_REF).equals(prevElement.attributeValue(XML_ELE_SEQUENCE_FLOW_SOURCE_REF))) {
                        return false;
                    }
                    if (!element.attributeValue(XML_ELE_SEQUENCE_FLOW_TARGET_REF).equals(prevElement.attributeValue(XML_ELE_SEQUENCE_FLOW_TARGET_REF))) {
                        return false;
                    }
                    Element routeFlag = (Element) element.element(XML_ELE_SEQUENCE_FLOW_CONDITION_EXPRESSION);
                    if (routeFlag != null && routeFlag.getText() != null) {
                        Element prevRouteFlag = (Element) prevElement.element(XML_ELE_SEQUENCE_FLOW_CONDITION_EXPRESSION);
                        if (prevRouteFlag == null || prevRouteFlag.getText() == null) {
//    						log.info("because flag change to null");
                            return false;
                        }
//    					log.info("because flag diff:"+routeFlag.getText()+"---"+prevRouteFlag.getText()+"---");
                        if (!routeFlag.getText().equals(prevRouteFlag.getText())) {
//    						log.info("because flag diff:"+routeFlag.getText()+"---"+prevRouteFlag.getText()+"---");
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public static final String XML_ELE_SEQUENCE_FLOW = "sequenceFlow";
    public static final String XML_ELE_SEQUENCE_FLOW_SOURCE_REF = "sourceRef";
    public static final String XML_ELE_SEQUENCE_FLOW_TARGET_REF = "targetRef";
    public static final String XML_ELE_SEQUENCE_FLOW_CONDITION_EXPRESSION = "conditionExpression";

    public static final String XML_ELE_PROCESS_ID = "id";
    public static final String XML_ELE_PROCESS = "process";
    public static final String XML_ELE_EXTENSION_ELEMENTS = BpmConstants.EXTENSION_ELEMENTS;

    protected static boolean canUseLatestProperties(Element eleProcess, String prevEditorBpmn) {
        if (StrUtil.isEmpty(prevEditorBpmn)) {
            return true;
        }
        Document prevDocEditor = getDoc(prevEditorBpmn);
        if (prevDocEditor == null || prevDocEditor.getRootElement() == null || prevDocEditor.getRootElement().element(XML_ELE_PROCESS) == null) {
            return true;
        }
        Element prevEleProcess = (Element) prevDocEditor.getRootElement().element(XML_ELE_PROCESS);
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (element == null) {
                continue;
            }

            if (BpmStringUtil.isResultIn(element.getName(), new String[]{XML_ELE_EXTENSION_ELEMENTS})) {
                continue;
            }
            if (!chkPrevElementByThisElementData(prevEleProcess, element)) {
                return false;
            }
        }


        return true;
    }

    public static XmlConvertResult convertEditor2starndard(String editorBpmn, boolean doDeployCheck, String prevEditorBpmn, boolean isIgnoreProcessVersion) {
        XmlConvertResult xmlConvertResult = new XmlConvertResult();

        Document docEditor = getDoc(editorBpmn);


        List<Element> processElements = new ArrayList<>();
        Map<String, Element> processElementByIds = new HashMap<>();
        Element rootEle = docEditor.getRootElement();
        Element eleProcess = rootEle.element(XML_ELE_PROCESS);
        Element eleBPMNPlane = rootEle.element("BPMNDiagram").element("BPMNPlane");

        xmlConvertResult.setCanUseLatestProperties(canUseLatestProperties(eleProcess, prevEditorBpmn));

        Set<String> gatewayNodes = new HashSet<String>();

        boolean hasStartNode = false;
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (BpmStringUtil.isResultIn(element.getName(), new String[]{XML_ELE_EXTENSION_ELEMENTS})) {
                continue;
            }
            processElements.add(element);

            String elementId = element.attributeValue(XML_ELE_PROCESS_ID);
            setTaskPropInResult(element, xmlConvertResult, isIgnoreProcessVersion);
            // 流程节点id不能为空
            if (StrUtil.isEmpty(elementId)) {
                log.info("| - XmlUtil>>>>>empty id element:{}", element.asXML());
                throw new ServiceException(ErrorCode.WF000016);
            }
            // 流程节点id不能重复
            if (processElementByIds.keySet().contains(elementId)) {
                throw new ServiceException(ErrorCode.WF000017);
            }
            processElementByIds.put(element.attributeValue("id"), element);

            if (StrUtil.isEmpty(element.attributeValue("name"))) {
                element.addAttribute("name", elementId);
            }

//    		log.info("element.getName():"+element.getName()+" att name:"+element.attributeValue("name")+" att id:"+element.attributeValue("id"));
            if ("startEvent".equals(element.getName())) {
                hasStartNode = true;
            }

            if (GatewayUtil.isGatewayNode(element)) {
                gatewayNodes.add(element.attributeValue("id"));
            }
        }

        if (doDeployCheck) {
            // 不能部署没有开始节点的流程
            if (!hasStartNode) {
                throw new ServiceException(ErrorCode.WF000027);
            }
        }

        // 网关节点必须成对出现
        if (gatewayNodes.size() > NumberConstants.INT_ZERO && gatewayNodes.size() % NumberConstants.INT_TWO != NumberConstants.INT_ZERO) {
            throw new ServiceException(ErrorCode.WF000012);
        }

        List<Element> sequenceFlows = new ArrayList<Element>();
        for (Element element : processElements) {
            if ("sequenceFlow".equals(element.getName())) {
                sequenceFlows.add(element);
            }
        }

        // 网关节点必须成对出现
        doTasks(processElementByIds, processElements, sequenceFlows, ACTION_DO_SERVICE_TASK, eleProcess, eleBPMNPlane);


        xmlConvertResult.setXmlStr(formatXml(docEditor));
        return xmlConvertResult;
    }


    @SuppressWarnings("rawtypes")
    public static String getDefXmlAttr(Element taskElement, String propName) {
        if (taskElement == null || StrUtil.isEmpty(propName)) {
            return null;
        }

        WfDefinePropExtendsService.RuleAttr ruleAttr = WfDefinePropExtendsService.RuleAttr.getEnumByRuleName(propName);
        if (ruleAttr == null) {

            return taskElement.attributeValue(propName);
        } else {
            if (taskElement.hasContent()) {
                for (Object obj : taskElement.content()) {
                    if (DefaultElement.class.isInstance(obj)) {
                        DefaultElement obj1 = (DefaultElement) obj;
                        List list = obj1.content();
                        for (Object e : list) {
                            if (e != null && DefaultElement.class.isInstance(e)) {
                                DefaultElement e1 = (DefaultElement) e;
                                if (BpmConstants.ROUTE_RULE.equals(e1.getName())) {
                                    if (StrUtil.isNotEmpty(e1.attributeValue(ruleAttr.getName()))) {
                                        return e1.attributeValue(ruleAttr.getName());
                                    }
                                }
                            }
                        }
                    }
                }
                return null;
            } else {
                return null;
            }
        }
    }

    /**
     * @Description: 把流程图中的扩展属性提取出来存入wf_define_prop_extends表
     * @Param: [element, xmlConvertResult, isIgnoreProcessVersion]
     * @Return: void
     * @Author:
     * @Date: 2021/9/23
     */
    @SuppressWarnings("rawtypes")
    public static void setTaskPropInResult(Element element, XmlConvertResult xmlConvertResult, boolean isIgnoreProcessVersion) {
    	//当配置为忽略版本信息时，插入基础数据
        if (isIgnoreProcessVersion) {
            String elementId = element.attributeValue("id");
            String[] atttrs = Constants.use_db_process_def_properties;
            for (String attr : atttrs) {
                org.dom4j.Attribute attribute = element.attribute(attr);
                if (element.attribute(attr) != null) {
                    xmlConvertResult.setPropertiesValue(elementId, attr, attribute.getValue());
                }
            }
            if (element.hasContent()) {
                for (Object obj : element.content()) {
                    if (DefaultElement.class.isInstance(obj)) {
                        DefaultElement obj1 = (DefaultElement) obj;
                        List list = obj1.content();
                        for (Object e : list) {
                            if (e != null && DefaultElement.class.isInstance(e)) {
                                DefaultElement e1 = (DefaultElement) e;
                                if (BpmConstants.ROUTE_RULE.equals(e1.getName())) {
                                    for (WfDefinePropExtendsService.RuleAttr attrEnum : WfDefinePropExtendsService.RuleAttr.values()) {
                                        if (StrUtil.isNotEmpty(e1.attributeValue(attrEnum.getName()))) {
                                            xmlConvertResult.setPropertiesValue(elementId, attrEnum.getKey4WfDefinePropExtends(), e1.attributeValue(attrEnum.getName()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * 获取路由规则
     *
     * @param element
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static String getRouteRule(Element element, String ruleKey) {
        Iterator iter = element.content().iterator();
        while (iter.hasNext()) {
            Object obj = iter.next();
            if (DefaultElement.class.isInstance(obj)) {
                DefaultElement obj1 = (DefaultElement) obj;
                List list = obj1.content();
                for (Object e : list) {
                    if (DefaultElement.class.isInstance(e)) {
                        DefaultElement e1 = (DefaultElement) e;

                        if (StrUtil.isNotEmpty(e1.attributeValue("ruleMap"))) {
                            return e1.attributeValue("ruleMap");
                        }

                    }
                }
            }
        }
        return null;
    }

    public static Element getDefinedTaskElement(String editorBpmn, String taskDefId) {
        if (StrUtil.isEmpty(editorBpmn) || StrUtil.isEmpty(taskDefId)) {
            return null;
        }
        Document docEditor = getDoc(editorBpmn);
        if (docEditor == null) {
            return null;
        }
        Element rootEle = docEditor.getRootElement();
        if (rootEle == null) {
            return null;
        }
        Element eleProcess = (Element) rootEle.element("process");
        if (eleProcess == null) {
            return null;
        }
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (taskDefId.equals(element.attributeValue("id"))) {
                return element;
            }
        }
        return null;
    }

    public static String initGatewayInfo(BigInteger version, String editorBpmn) {
        Document docEditor = getDoc(editorBpmn);

        List<Element> processElements = new ArrayList<>();
        Map<String, Element> processElementByIds = new HashMap<>();
        Element rootEle = docEditor.getRootElement();
        Element eleProcess = rootEle.element("process");

        Element eleStartNode = null;

        Set<String> gatewayNodes = new HashSet<>();
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (BpmStringUtil.isResultIn(element.getName(), new String[]{BpmConstants.EXTENSION_ELEMENTS})) {
                continue;
            }
            processElements.add(element);
            String elementId = element.attributeValue("id");

            if (StrUtil.isEmpty(elementId)) {
                log.info("| - XmlUtil>>>>>empty id element:{}", element.asXML());
                // 流程节点id不能为空
                throw new ServiceException(ErrorCode.WF000016);
            }
            if (processElementByIds.keySet().contains(elementId)) {
                // 流程节点id不能重复
                throw new ServiceException(ErrorCode.WF000017);
            }
            processElementByIds.put(element.attributeValue("id"), element);

            if (StrUtil.isEmpty(element.attributeValue("name"))) {
                element.addAttribute("name", elementId);
            }


            if ("startEvent".equals(element.getName())) {
                eleStartNode = element;
            }
            if (GatewayUtil.isGatewayNode(element)) {
                gatewayNodes.add(element.attributeValue("id"));
            }
        }
        // 网关节点必须成对出现
        if (gatewayNodes.size() > NumberConstants.INT_ZERO && gatewayNodes.size() % NumberConstants.INT_TWO != NumberConstants.INT_ZERO) {
            throw new ServiceException(ErrorCode.WF000012);
        }

        List<Element> sequenceFlows = new ArrayList<Element>();
        for (Element element : processElements) {
            if ("sequenceFlow".equals(element.getName())) {

                sequenceFlows.add(element);
            }
        }

        initGatewayInfo(version, processElementByIds, sequenceFlows, eleProcess, eleStartNode);
        return formatXml(docEditor);
    }

    private static String getRuleMap(Element userTask) {
        if (userTask == null) {
            return null;
        }
        if (userTask.element(BpmConstants.EXTENSION_ELEMENTS) != null) {
            Element theExtensionElements = (Element) userTask.element(BpmConstants.EXTENSION_ELEMENTS);
            if (theExtensionElements.element(BpmConstants.ROUTE_RULE) != null) {
                Element elementRouteRule = (Element) theExtensionElements.element(BpmConstants.ROUTE_RULE);
                if (elementRouteRule != null) {
                    return elementRouteRule.attributeValue("ruleMap");
                }
            }
        }
        return null;
    }

    private static void doTasks(Map<String, Element> processElementByIds, List<Element> processElements, List<Element> sequenceFlows, int actionType, Element eleProcess, Element eleBPMNPlane) {
        Map<String, Element> tasksById = new HashMap<>();
        Map<String, Element> tasksByName = new HashMap<>();
        for (Element element : processElements) {
            if (actionType == ACTION_DO_TASKS_WITH_RULE_MAP) {
                if ("userTask".equals(element.getName()) || "serviceTask".equals(element.getName()) || "startEvent".equals(element.getName())) {
                    tasksById.put(element.attributeValue("id"), element);
                    tasksByName.put(element.attributeValue("name"), element);
                }
            } else if (actionType == ACTION_DO_SERVICE_TASK) {
                if ("serviceTask".equals(element.getName())) {
                    tasksById.put(element.attributeValue("id"), element);
                    tasksByName.put(element.attributeValue("name"), element);
                }
            }

        }
        for (Map.Entry<String, Element> anEntry : tasksByName.entrySet()) {
            if (actionType == ACTION_DO_TASKS_WITH_RULE_MAP) {
                String ruleMap = getRuleMap(anEntry.getValue());
                if (!StrUtil.isEmpty(ruleMap)) {
                    doTasksWithRuleMap(processElements, anEntry.getValue(), ruleMap, sequenceFlows, tasksById);
                }
            } else if (actionType == ACTION_DO_SERVICE_TASK) {
                Element serviceTaskEle = anEntry.getValue();
                if (isCallBackServiceTask(serviceTaskEle)) {
                    addUserTask4CallBackSvcTask(processElementByIds, serviceTaskEle, sequenceFlows, eleProcess, eleBPMNPlane);
                }
            }

        }
    }

    private static int shapeCoXy = 1;

    private static void addUserTask4CallBackSvcTask(Map<String, Element> processElementByIds, Element serviceTaskEle, List<Element> sequenceFlows, Element eleProcess, Element eleBPMNPlane) {
        Element targetSequnceFlow = null;
        for (Element ele : sequenceFlows) {
            if (serviceTaskEle.attributeValue("id").equals(ele.attributeValue("sourceRef"))) {
                targetSequnceFlow = ele;
                break;
            }
        }

        Element autoCreateUserTask = eleProcess.addElement("userTask");
        String newUserTaskId = TaskUtil.getAutoAddUserTaskPropertyKey(serviceTaskEle.attributeValue("id"));
        autoCreateUserTask.addAttribute("id", newUserTaskId);
        autoCreateUserTask.addAttribute("name", TaskUtil.getAutoAddUserTaskPropertyKey(serviceTaskEle.attributeValue("name")));
        autoCreateUserTask.addAttribute("activiti:candidateUsers", ConfigUtil.SYSTEM_AUTO_ACTION_USER);
        Element newFlow = eleProcess.addElement("sequenceFlow");
        String newFlowId = newUserTaskId + "_flow";
        newFlow.addAttribute("id", newFlowId);
        newFlow.addAttribute("sourceRef", serviceTaskEle.attributeValue("id"));
        newFlow.addAttribute("targetRef", newUserTaskId);

        targetSequnceFlow.attribute("sourceRef").setValue(newUserTaskId);

        processElementByIds.put(newUserTaskId, autoCreateUserTask);
        sequenceFlows.add(newFlow);

        shapeCoXy++;
        Element autoCreateUserTaskShape = eleBPMNPlane.addElement("bpmndi:BPMNShape");
        autoCreateUserTaskShape.addAttribute("bpmnElement", newUserTaskId);
        autoCreateUserTaskShape.addAttribute("id", "BPMNShape_" + newUserTaskId);
        Element autoCreateUserTaskBounds = autoCreateUserTaskShape.addElement("omgdc:Bounds");
        autoCreateUserTaskBounds.addAttribute("height", "55.0");
        autoCreateUserTaskBounds.addAttribute("width", "105.0");
        autoCreateUserTaskBounds.addAttribute("x", "" + shapeCoXy + ".0");
        autoCreateUserTaskBounds.addAttribute("y", "" + shapeCoXy + ".0");


        Element autoCreateUserTaskFlow = eleBPMNPlane.addElement("bpmndi:BPMNEdge");
        autoCreateUserTaskFlow.addAttribute("bpmnElement", newFlowId);
        autoCreateUserTaskFlow.addAttribute("id", "BPMNEdge_" + newFlowId);
        Element autoCreateUserTaskWaypointFrom = autoCreateUserTaskFlow.addElement("omgdi:waypoint");
        autoCreateUserTaskWaypointFrom.addAttribute("x", "" + shapeCoXy + ".0");
        autoCreateUserTaskWaypointFrom.addAttribute("y", "" + shapeCoXy + ".0");
        shapeCoXy++;
        Element autoCreateUserTaskWaypointTo = autoCreateUserTaskFlow.addElement("omgdi:waypoint");
        autoCreateUserTaskWaypointTo.addAttribute("x", "" + shapeCoXy + ".0");
        autoCreateUserTaskWaypointTo.addAttribute("y", "" + shapeCoXy + ".0");
    }

    private static boolean isCallBackServiceTask(Element serviceTaskEle) {
        if (serviceTaskEle == null) {
            return false;
        }
        Element documentationEle = serviceTaskEle.element("documentation");
        if (documentationEle == null || documentationEle.getText() == null) {
            return false;
        }
        if (documentationEle.getText().startsWith(BaseServiceImpl.SERVICE_TYPE_CALLBACK)) {
            return true;
        }
        return false;
    }


    private static Element getProcessNodeById(String targetId, List<Element> processElements) {
        if (processElements == null || processElements.size() == 0) {
            return null;
        }
        for (Element element : processElements) {
            if (targetId.equals(element.attributeValue("id"))) {
                return element;
            }
        }
        return null;
    }


    private static Map<String, Element>[] getTargetSequenceFlowByTargetNameAndTargetId(List<Element> processElements, String userTaskId, List<Element> sequenceFlows, Map<String, Element> userTasksById) {

        @SuppressWarnings("unchecked") Map<String, Element>[] result = new Map[3];

        Map<String, Element> targetSequenceFlowByTargetName = new HashMap<String, Element>();
        Map<String, Element> targetSequenceFlowByTargetId = new HashMap<String, Element>();
        result[0] = targetSequenceFlowByTargetName;
        result[1] = targetSequenceFlowByTargetId;
        Map<String, Element> gatewayFlowInMap = new HashMap<String, Element>();
        result[2] = gatewayFlowInMap;

        Element targetSequnceFlowStartNode = null;
        boolean meetGatewayNode = false;
        for (Element element : sequenceFlows) {
            if (userTaskId.equals(element.attributeValue("sourceRef"))) {
                Element neighborNode = getProcessNodeById(element.attributeValue("targetRef"), processElements);
                if (GatewayUtil.isGatewayNode(neighborNode)) {
                    if (meetGatewayNode) {
                        // 非网关发起的分支，目的节点中只允许出现一个网关节点
                        throw new ServiceException(ErrorCode.WF000020);
                    } else {
                        meetGatewayNode = true;
                    }
//    				if(targetSequnceFlowStartNode!=null) {
//    					throw new ServiceException("000011");// 000011=带有映射路径选择条件的工作节点必须指向一个网关节点，而且指向节点不能超过1个。
//    				}else {
                    targetSequnceFlowStartNode = neighborNode;
//    				}

                    gatewayFlowInMap.put(element.attributeValue("id"), element);
                } else {
                    targetSequenceFlowByTargetName.put(userTasksById.get(element.attributeValue("targetRef")).attributeValue("name"), element);
                    targetSequenceFlowByTargetId.put(userTasksById.get(element.attributeValue("targetRef")).attributeValue("id"), element);
                }
            }
        }
        if (targetSequnceFlowStartNode != null) {
            for (Element element : sequenceFlows) {
                if (targetSequnceFlowStartNode.attributeValue("id").equals(element.attributeValue("sourceRef"))) {
                    targetSequenceFlowByTargetName.put(userTasksById.get(element.attributeValue("targetRef")).attributeValue("name"), element);
                    targetSequenceFlowByTargetId.put(userTasksById.get(element.attributeValue("targetRef")).attributeValue("id"), element);
                }
            }
        }


        return result;
    }

    public static Map<String, Set<String>> getRuleMapByStr(String ruleMapStr) {
        JSONArray jsonArr = JSONArray.parseArray(ruleMapStr);
        Map<String, Set<String>> ruleMaps = new HashMap<String, Set<String>>();
        for (int i = 0; i < jsonArr.size(); i++) {
            JSONObject jsonObj = jsonArr.getJSONObject(i);
            if ((!StrUtil.isEmpty(jsonObj.getString(Constants.KEY))) && (!StrUtil.isEmpty(jsonObj.getString(Constants.VALUE)))) {
                for (String nodeName : jsonObj.getString(Constants.VALUE).split(SymbolConstants.COMMA)) {
                    Set<String> theRuleOutputs = ruleMaps.computeIfAbsent(nodeName, k -> new HashSet<>());
                    theRuleOutputs.addAll(Arrays.asList(jsonObj.getString(Constants.KEY).split(SymbolConstants.COMMA)));
                }
            }
        }
        return ruleMaps;
    }


    private static void checkTargetIdsNameUniq(Map<String, Element> targetSequenceFlows, Map<String, Element> userTasksById) {
        Set<String> nodeNames = new HashSet<>();
        for (Map.Entry<String, Element> anEntry : targetSequenceFlows.entrySet()) {
            String targetNodeName = userTasksById.get(anEntry.getValue().attributeValue("targetRef")).attributeValue("name");
            if (nodeNames.contains(targetNodeName)) {
                // 同一分支组的流程节点名称不能重复
                throw new ServiceException(ErrorCode.WF000018);
            } else {
                nodeNames.add(targetNodeName);
            }
        }

    }

    /*
     * 可能出现的情况：
     * out1   node1
     * out2   node2
     * out1,out3 node3
     * 则规则结果是
     * out1  执行node1,node3
     * out2  执行node2
     * out3  执行node3
     * out1,out3 执行 node1,node3
     * out1,out2 执行node1，node2
     * out1，out2,out3 执行node1，node2,node3
     */
    private static void doTasksWithRuleMap(List<Element> processElements, Element userTask, String ruleMapStr, List<Element> sequenceFlows, Map<String, Element> userTasksById) {
        Map<String, Element>[] targetSequenceFlows = getTargetSequenceFlowByTargetNameAndTargetId(processElements, userTask.attributeValue("id"), sequenceFlows, userTasksById);

        Map<String, Element> targetSequenceFlowByTargetName = targetSequenceFlows[0];
        Map<String, Element> targetSequenceFlowByTargetId = targetSequenceFlows[1];

        Map<String, Element> gatewayFlowInMap = targetSequenceFlows[2];
        Element gatewayFlow = null;
        if (gatewayFlowInMap.size() == 1) {
            gatewayFlow = gatewayFlowInMap.entrySet().iterator().next().getValue();
        }

        checkTargetIdsNameUniq(targetSequenceFlowByTargetId, userTasksById);

        Set<String> allNotGatewayRule = new HashSet<String>();

        Map<String, Set<String>> ruleMap = getRuleMapByStr(ruleMapStr);
        for (Map.Entry<String, Element> anEntry : targetSequenceFlowByTargetName.entrySet()) {
            if (ruleMap.keySet().contains(anEntry.getKey())) {
                Element eleCondition = anEntry.getValue().addElement("conditionExpression");

                eleCondition.addAttribute("xsi:type", "tFormalExpression");
                eleCondition.addCDATA(getUelCondition(ruleMap.get(anEntry.getKey()), true));
                if (anEntry.getValue().attributeValue("sourceRef").equals(userTask.attributeValue("id"))) {
                    allNotGatewayRule.addAll(ruleMap.get(anEntry.getKey()));
                }
            }
        }

        if (allNotGatewayRule.size() > 0 && gatewayFlow != null) {
            Element eleCondition = gatewayFlow.addElement("conditionExpression");

            eleCondition.addAttribute("xsi:type", "tFormalExpression");
            eleCondition.addCDATA(getUelCondition(allNotGatewayRule, false));
        }
    }

    private static String getUelCondition(Set<String> ruleOutputs, boolean isMatch) {
        StringBuilder sb = new StringBuilder();
        sb.append("${");
        boolean isFirst = true;
        StringBuilder output = new StringBuilder();
        for (String ruleOutput : ruleOutputs) {
            if (isFirst) {
                output.append(ruleOutput);
                isFirst = false;
            } else {
                output.append(",").append(ruleOutput);
            }
        }
        sb.append("GATEWAY_ROUTE_CONDITION_KEY.");
        if (isMatch) {
            sb.append("match(\"");
        } else {
            sb.append("notMatch(\"");
        }

        sb.append(output);
        sb.append("\")");
        sb.append("}");
        return sb.toString();
    }

    private static final int ACTION_DO_TASKS_WITH_RULE_MAP = 0;
    private static final int ACTION_DO_SERVICE_TASK = 1;


    private static void initGatewayInfo(BigInteger version, Map<String, Element> processElementByIds, List<Element> sequenceFlows, Element eleProcess, Element eleStartNode) {

        String processDefId = eleProcess.attributeValue("id");
        GatewayInfo gatewayInfo = new GatewayInfo(processDefId, eleStartNode.attributeValue("id"), sequenceFlows, processElementByIds);

        GatewayUtil.addGatewayInfo(version, processDefId, gatewayInfo);
    }

    public static String getProcessIdOnly(String processDefIdWithVersion) {
        if (processDefIdWithVersion == null) {
            return null;
        }
        if (processDefIdWithVersion.contains(SymbolConstants.COLON)) {
            String[] theSplits = processDefIdWithVersion.split(SymbolConstants.COLON);
            if (theSplits.length != NumberConstants.INT_THREE) {
                return null;
            }
            return theSplits[0];
        } else {
            return processDefIdWithVersion;
        }
    }

    public static BigInteger getProcessVersionByDefId(String processDefIdWithVersion) {
        //ExecutionSimpleGateway:73:290007
        if (processDefIdWithVersion == null) {
            return null;
        }
        String[] theSplits = processDefIdWithVersion.split(":");
        if (theSplits.length != NumberConstants.INT_THREE) {
            return null;
        }
        return BigInteger.valueOf(Integer.parseInt(theSplits[1]));
    }

    /**
     * <pre>
     * 根据流程图获取对应流转规则，方便规则的检查
     * </pre>
     *
     * @param xmlSource 流程图xml
     * @return 流程图中包含的流转规则<key:路径 ， value:规则树对应的结果>
     * @author anmingtao
     * @serialData 2019-08-07
     */
    @SuppressWarnings("unchecked")
    public static Map<String, URuleRepoTreeTransferData> getXMLElmentUrule(String xmlSource) {
        Map<String, URuleRepoTreeTransferData> uruleMap = new HashMap<>();
        Document docEditor = getDoc(xmlSource);
        Element rootEle = docEditor.getRootElement();
        Element eleProcess = (Element) rootEle.element("process");
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (BpmStringUtil.isResultIn(element.getName(), new String[]{BpmConstants.EXTENSION_ELEMENTS})) {
                continue;
            }

            if (element.getName().contains("userTask")) {
                Element extensionElement = (Element) element.elements().get(0);
                List<Element> es = extensionElement.elements();
                if (es.size() >= 3) {
                    for (int i = 0; i < es.size(); i++) {
                        Element urule = es.get(i);
                        JSONObject json = JSONObject.parseObject(urule.attributeValue("ruleMap"));
                        if (json != null) {
                            uruleMap.put(json.getString("fullPath"), new URuleRepoTreeTransferData(json.getString("id"), json.getString("label"), json.getString("fullPath"), json.getString("ruleType"), null, null));
                        }
                    }

                }
            }
        }
        return uruleMap;
    }

    /**
     * 以map形式返回流程的每一个属性，方便两个流程的差异比较
     *
     * @param xmlSource
     * @return
     * @author anmingtao
     * @serialData 2019-08-07
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getXMLElmentProps(String xmlSource) {
        Document docEditor = getDoc(xmlSource);
        Element rootEle = docEditor.getRootElement();
        Element eleProcess = (Element) rootEle.element("process");
        Map<String, Object> result = new HashMap<String, Object>();
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (BpmStringUtil.isResultIn(element.getName(), new String[]{BpmConstants.EXTENSION_ELEMENTS})) {
                continue;
            }
            Map<String, Object> attrMap = new HashMap<String, Object>();
            for (int i = 0; i < element.attributeCount(); i++) {
                attrMap.put(element.attribute(i).getName(), element.attribute(i).getStringValue());
            }
            if (element.elements() != null && element.elements().size() > 0) {
                Element extensionElement = (Element) element.elements().get(0);
                List<Element> es = extensionElement.elements();
                if (es.size() == 3) {
                    Element urule = es.get(2);
                    attrMap.put("ruleMap", urule.attributeValue("ruleMap"));
                }
            }
            result.put(element.getName() + "" + element.attributeValue("id"), attrMap);
        }
        return result;
    }

    /**
     * 获取两个流程图的差异
     *
     * @param oldXml 原有的流程图
     * @param newXml 新的流程图
     * @return
     * @author anmingtao
     * @serialData 2019-08-07
     */
    @SuppressWarnings("unchecked")
    public static String getDefinitionDiffXml(String oldXml, String newXml) {
        Map<String, Object> oldAttrMap = XmlUtil.getXMLElmentProps(oldXml);
        Map<String, Object> newAttrMap = XmlUtil.getXMLElmentProps(newXml);
        JSONArray jsonArray = JSONArray.parseArray("[]");
        for (Map.Entry<String, Object> entry : newAttrMap.entrySet()) {
            String mapKey = entry.getKey();
            if (oldAttrMap.containsKey(mapKey)) {
                Map<String, Object> attrMap1 = (Map<String, Object>) oldAttrMap.get(mapKey);
                Map<String, Object> attrMap2 = (Map<String, Object>) newAttrMap.get(mapKey);
                boolean diff = false;
                for (Map.Entry<String, Object> entrykey : attrMap2.entrySet()) {
                    String attrkey = entrykey.getKey();
                    if (attrMap1.containsKey(attrkey)) {
                        if (!StrUtil.equals((String) attrMap1.get(attrkey), (String) entrykey.getValue())) {
                            diff = true;
                        }
                    }
                }
                if (diff) {
                    jsonArray.add(attrMap2.get("name"));
                }

            } else {
                JSONObject json = BpmStringUtil.objectToJsonObject(entry.getValue());
                if (json != null) {
                    jsonArray.add(json.getString("id"));

                }
            }
        }
        if (jsonArray.isEmpty()) {
            return "流程定义相同";
        }
        return jsonArray.toJSONString() + "不一致";
    }


    /**
     * 以map形式返回流程的每一个属性，方便两个流程的差异比较
     */
    public static boolean isXMLElment(String xmlSource, Map<String, Object> taskMode) {
        Document docEditor = getDoc(xmlSource);
        Element rootEle = docEditor.getRootElement();
        Element eleProcess = rootEle.element("process");
        for (Object ele : eleProcess.elements()) {
            Element element = (Element) ele;
            if (BpmStringUtil.isResultIn(element.getName(), new String[]{BpmConstants.EXTENSION_ELEMENTS})) {
                continue;
            }
            for (int i = 0; i < element.attributeCount(); i++) {
                if (taskMode.containsKey(element.attribute(i).getStringValue())) {
                    return true;
                }
            }
        }
        return false;
    }

}
