package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfAccessory;
import com.bpm.workflow.mapper.WfAccessoryMapper;
import com.bpm.workflow.service.WfAccessoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfAccessoryServiceImpl extends ServiceImpl<WfAccessoryMapper, WfAccessory> implements WfAccessoryService {

}
