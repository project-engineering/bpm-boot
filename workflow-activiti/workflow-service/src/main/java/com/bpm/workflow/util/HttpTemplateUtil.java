package com.bpm.workflow.util;


import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.util.constant.BpmConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;


@Component("workflowHttpTemplateUtil")
public class HttpTemplateUtil {
    private static final Logger log = LoggerFactory.getLogger(HttpTemplateUtil.class);

    private RestTemplate getRestTemplate() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10000);
        requestFactory.setConnectTimeout(15000);
        return new RestTemplate(requestFactory);
    }

    public String postForObject(String url, String params) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<>(params, headers);
        String result = getRestTemplate().postForObject(url, requestEntity, String.class);
        return result;
    }

    public Map<String, Object> postForEntity(String url, String params) {
        Map<String, Object> resultMap = new HashMap<>();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<>(params, headers);
        try {
            ResponseEntity<String> responseEntity = getRestTemplate().postForEntity(url, requestEntity, String.class);
            HttpStatus status = (HttpStatus) responseEntity.getStatusCode();
            log.info("HttpStatus is:" + status.value());
            if (status.is2xxSuccessful()) {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPSUCC);
            } else {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPFAIL);
            }
            resultMap.put(Constants.HTTP_CONNECT_RSPHEAD, responseEntity.getHeaders());
            resultMap.put(Constants.HTTP_CONNECT_RSPDATA, responseEntity.getBody());
        } catch (Exception e) {
            log.error("Post Error!" + e.getMessage());
            resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RspTimeOut);
        }
        return resultMap;
    }

    public Map<String, Object> getForEntity(String url) {
        Map<String, Object> resultMap = new HashMap<>();
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        headers.setContentType(type);
        headers.add("Accept", BpmConstants.APPLICATION_JSON_UTF8_VALUE);
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> responseEntity = getRestTemplate()
                    .getForEntity(url, String.class, requestEntity);
            HttpStatus status = (HttpStatus) responseEntity.getStatusCode();
            log.info("HttpStatus is:" + status.value());
            if (status.is2xxSuccessful()) {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPSUCC);
            } else {
                resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RSPFAIL);
            }
            resultMap.put(Constants.HTTP_CONNECT_RSPHEAD, responseEntity.getHeaders());
            resultMap.put(Constants.HTTP_CONNECT_RSPDATA, responseEntity.getBody());
        } catch (Exception e) {
            log.error("Post Error!" + e.getMessage());
            resultMap.put(Constants.HTTP_CONNECT_RSPCODE, Constants.HTTP_CONNECT_RspTimeOut);
        }
        return resultMap;
    }

}
