package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfMessage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfMessageService extends IService<WfMessage> {

    void deleteByTaskId(String processInstanceId);

    void updateByKeyId(WfMessage wfMessage);
}
