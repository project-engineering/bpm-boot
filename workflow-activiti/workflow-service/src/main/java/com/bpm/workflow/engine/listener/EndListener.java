package com.bpm.workflow.engine.listener;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.dto.transfer.WorkFlowTaskTransferData;
import com.bpm.workflow.entity.WfBusinessData;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.*;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.*;
import com.bpm.workflow.timertask.TimerUtil;
import com.bpm.workflow.timertask.WfDisposeMessageDataTask;
import com.bpm.workflow.timertask.WfDisposeWfInstanceDataTask;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Log4j2
public class EndListener extends SpringUtil implements ExecutionListener {
    @Resource
    private TimerUtil timerUtil;
    /**
     *
     */
    private static final long serialVersionUID = -5930435194196325505L;

    @Resource
    private ActivitiBaseService activitiBaseService;

    @Resource
    private WfEvtServiceDefService wfEvtServiceDefService;

    @Resource
    private WfInstanceService wfInstanceService;

    @Resource
    private ModelPropService modelPropService;

    @Resource
    private WfTaskflowService wfTaskflowService;

    @Resource
    private WfCommService wfCommService;

    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;

    @Resource
    private ProcessCoreService processCoreService;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private WfBusinessDataService wfBusinessDataService;

    @Override
    public void notify(DelegateExecution execution) {
    }

    /**
     * 公共的处理方法
     *
     * @param execution
     */
    public void common(String eventName, DelegateExecution execution) {
        WfVariableUtil.setProcessEndEventName(eventName);
//		common(execution.getProcessDefinitionId(),execution.getProcessInstanceId()
//				,activitiBaseService,modelPropMapper,wfEvtServiceDefService,modelPropService
//				,wfInstanceMapper,wfTaskflowMapper,timerUtil);
    }

    public void common(String processDefinitionId, String processInstanceId, FlowElement currentFlowElement) {
        String eventName = WfVariableUtil.getProcessEndEventName();
        WfVariableUtil.setProcessEndEventName(null);
        Map<String, Object> variableMap = WfVariableUtil.getVariableMap();

        String executionId = processInstanceId;
        if (log.isInfoEnabled()) {
            log.info("| - EndListener>>>>>当前任务ID[{}]", executionId);
            log.info("| - EndListener>>>>>当前流程定义ID[{}]", processDefinitionId);
            log.info("| - EndListener>>>>>当前流程实例ID[{}]", processInstanceId);
            log.debug("| - EndListener>>>>>业务参数[{}]", variableMap);
            log.info("| - EndListener>>>>>节点id[{}]", currentFlowElement.getId());
            log.info("| - EndListener>>>>>节点名称[{}]", currentFlowElement.getName());
            if (WfVariableUtil.getSysCommHead() != null) {
                log.info("| - EndListener>>>>>报文头[{}]", WfVariableUtil.getSysCommHead().toString());
            }

            log.info("| - EndListener>>>>>实例结束事件类型：[{}]", eventName);
        }
        //获取扩展属性
        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(processDefinitionId.substring(0, processDefinitionId.indexOf(":")));

        //获取结束类型和结束事件类型
        String completeType = Constants.COMPLETE_TYPE_END;
        if (ModelProperties.flowEndErrorEvent.getName().equals(eventName)) {
            completeType = Constants.COMPLETE_TYPE_ERROR;
        } else if (variableMap.get(Constants.COMPLETE_TYPE_KEY) != null) {
            completeType = variableMap.get(Constants.COMPLETE_TYPE_KEY).toString();
        }
        if (completeType.equals(Constants.COMPLETE_TYPE_CANCLE) || completeType.equals(Constants.COMPLETE_TYPE_TIMEEND)) {
            eventName = ModelProperties.cancelEvent.getName();
        }
        if (completeType.equals(Constants.COMPLETE_TYPE_ERROR)) {
            eventName = ModelProperties.flowEndErrorEvent.getName();
        }
        if (completeType.equals(Constants.COMPLETE_TYPE_END)) {
            eventName = ModelProperties.flowEndEvent.getName();
        }
        if (log.isInfoEnabled()) {
			log.info("| - EndListener>>>>>结束实例类型：[{}]", completeType);
        }
        // 调用节点进入触发事件
        WfModelPropExtends flowEndEvent = modelPropService.getSpcefiyModelProp(extendsPropList, eventName);
        wfEvtServiceDefService.executeEventService(flowEndEvent, executionId,
                processDefinitionId, processInstanceId, variableMap,
                currentFlowElement, WfVariableUtil.getSysCommHead());

        // 流程结束条件判断 ---应该在流程结束时判断
        wfCommService.conditionalVerification(ModelProperties.endCondition.getName(), extendsPropList, variableMap, "不满足流程结束条件，请查看！");

        //增加流程结束的流转记录
        addEndWfTaskflow(processInstanceId, processDefinitionId, executionId, variableMap, currentFlowElement, wfInstanceService, wfTaskflowService, wfBusinessDataService);

        //2019-05-20 amt modify start 更新最后一个节点的变量，增加结束类型
        List<WfTaskflow> taskList = wfTaskflowService.selectByProcessInstId(processInstanceId);

        //2019-06-19 update by xuesw 但前一taskId为00000时，查询最后节点结果不唯一问题优化
        WfTaskflow befortaskflow = null;
        String beforTaskId = (String) variableMap.get(Constants.BEFOR_TASKID);
        for (WfTaskflow temp : taskList) {
            if (temp.getTaskId().equals(beforTaskId)) {
                befortaskflow = temp;
                if (befortaskflow != null) {
                    WfBusinessData businessdata = wfBusinessDataService.selectById(temp.getId().toString());
                    if (businessdata != null) {
                        befortaskflow.setBusinessData(businessdata);
                    }
                }
            }
        }
        //2019-06-19 update by xuesw 但前一taskId为00000时，查询最后节点结果不唯一问题优化
        Map<String, Object> map = BpmStringUtil.jsonToMap(befortaskflow.getBussinessMap());
        map.put(Constants.COMPLETE_TYPE_KEY, completeType);
        befortaskflow.setBussinessMap(JSONObject.toJSONString(map));
        wfTaskflowService.updateById(befortaskflow);
        WfBusinessData businessData = new WfBusinessData(befortaskflow);
        wfBusinessDataService.updateById(businessData);
        //2019-05-20 amt modify end 更新最后一个节点的变量，增加结束类型
        //修改流程实例数据
        try {
            updateWfInstanceDataNew(processCoreService, activitiBaseService, wfInstanceService, processInstanceId, completeType, variableMap);
        } catch (Exception e) {
            log.error("| - EndListener>>>>>主流程回调异常：", e);
            throw new ServiceException(e.getMessage());
        }

        // 流程实例结束后，清理不必要的数据
        clearWorkFlowDataNew(timerUtil, processInstanceId);
    }

    /**
     * 清理流程实例产生的部分数据
     *
     * @param processInstanceId
     * @param timerUtil
     */
    public static void clearWorkFlowDataNew(TimerUtil timerUtil, String processInstanceId) {

        // 将流程流转记录转移到历史库中
        WfDisposeWfInstanceDataTask disposeWfInstanceDataTask = new WfDisposeWfInstanceDataTask(processInstanceId);
        timerUtil.addWorkflowTimerTask(disposeWfInstanceDataTask, 0, 0, 0, 0, 0, 5);
        if (log.isInfoEnabled()) {
			log.info("| - EndListener>>>>>5s后： 清理流程实例[{}]产生的部分数据", processInstanceId);
        }
        // 将消息通知表的数据进行删除
        WfDisposeMessageDataTask disposeMessageDataTask = new WfDisposeMessageDataTask(
                processInstanceId);
        timerUtil.addWorkflowTimerTask(disposeMessageDataTask, 0, 0, 0, 0, 0, 30);
        if (log.isInfoEnabled()) {
			log.info("| - EndListener>>>>>30s后： 清理流程实例[{}]消息通知表部分数据", processInstanceId);
        }
    }

    /**
     * 修改流程实例的数据
     *
     * @param processCoreService
     * @throws Exception
     */
    private void updateWfInstanceDataNew(
            ProcessCoreService processCoreService, ActivitiBaseService activitiBaseService, WfInstanceService wfInstanceService, String processInstanceId, String completeType, Map<String, Object> varMap) throws Exception {

        WfInstance instance = wfInstanceService.selectByProcessId(processInstanceId);
        if (instance != null) {
            instance.setWorkState(Constants.WORK_STATE_END);
            long due = System.currentTimeMillis() - DateUtil.convertObjToTimestamp(instance.getStartTime()).getTime();
            instance.setDuedata(due);
            instance.setEndType(completeType);
            LocalDateTime now = LocalDateTime.now();
            instance.setEndTime(now);
            instance.setUpdateTime(now);
            wfInstanceService.updateById(instance);
            String manualEndProcess = (String) varMap.get(Constants.MANUAL_END_PROCESS);

            //以下是处理子流程回调主流程的逻辑
            if (StrUtil.isNotBlank(instance.getParentTaskId()) && StrUtil.isEmpty(manualEndProcess)) {
                //先查询节点的配置状态是否不等待，不等待的话，直接返回
                WfTaskflow mainTask = processCoreService.selectByTaskId(instance.getParentTaskId());
                if (mainTask == null) {
					log.error("| - EndListener>>>>>主流程【任务id:{}】不存在！", instance.getParentTaskId());
                    return;
                }

                FlowElement target = activitiBaseService.getBpmnModelNode(mainTask.getProcessCode()).getFlowElement(mainTask.getTaskDefId());
                String waitType = processUtil.getTaskProp(mainTask.getProcessCode(), target, Constants.WAITTYPE);//target.getAttributeValue(Constants.XMLNAMESPACE, Constants.WAITTYPE);
                //主流程与子流程taskCategoryId转换
                varMap.put(Constants.TASK_CATEGORY_ID, varMap.get(Constants.TASK_CATEGORY_ID_PARENT));
                varMap.remove(Constants.TASK_CATEGORY_ID_PARENT);
                //回调的时候，删除子流程的启动用户信息
                //varMap.remove(Constants.CHILD_START_USER_ID);
                //去掉控制参数
                varMap.remove(Constants.PROCESS_TASK_ID);
                varMap.remove(Constants.SPECIFYNEXTNODE);
                if (waitType.equals(Constants.WAITTYPE_NOWAIT)) {
                    //主流程已经结束不需要回调
                    return;
                }


                //以下是回调主流程的操作
                WfTaskflow wfTaskflow = processCoreService.selectByTaskId(instance.getParentTaskId());
                // 查询任务对应节点的配置信息

                Map<String, Object> exectMap = BpmStringUtil.jsonToMap(wfTaskflow.getBussinessMap());
                if (exectMap != null) {
                    exectMap.putAll(varMap);
                }
                String startUserId = instance.getStartUserId();
                if (varMap.containsKey(Constants.BEFOR_USERID)) {
                    startUserId = (String) varMap.get(Constants.BEFOR_USERID);
                }

                WorkFlowTaskTransferData data = new WorkFlowTaskTransferData();
                data.setUserId(startUserId);
                data.setTaskId(instance.getParentTaskId());
                if (exectMap != null) {
                    exectMap.put(Constants.COMPLETE_TYPE_KEY, completeType);
                    data.setBussinessMap(exectMap);
                    data.setResult("S" + completeType);
                    data.setResultRemark("子流程回调父流程");
                }


                data.setSysCommHead(WfVariableUtil.getSysCommHead());
				log.info("| - EndListener>>>>>开始回调主流程【任务id:{}】", instance.getParentTaskId());
				log.info("| - EndListener>>>>>开始回调主流程【用户:{}】", startUserId);
				log.info("| - EndListener>>>>>开始回调主流程【参数:{}】", exectMap);
                try {
                    processCoreService.nextTask(data);
                } catch (Exception e) {
					log.error("| - EndListener>>>>>不是正常结束也不是异常结束", e);
                    throw new ServiceException(e.getMessage());
                }
            }

        }

    }


    /**
     * 增加开始流转记录
     *
     * @param processInstId
     * @param varibaleMap
     * @param processDefId
     */
    private static void addEndWfTaskflow(String processInstId, String processDefId, String executionId, Map<String, Object> varibaleMap,
                                         FlowElement endElement, WfInstanceService wfInstanceService, WfTaskflowService wfTaskflowService, WfBusinessDataService wfBusinessDataService) {
        WfTaskflow taskflow = new WfTaskflow();
        taskflow.setTaskId("99999");
        taskflow.setProcessId(processInstId);
        taskflow.setProcessCode(processDefId);
        taskflow.setTaskType(Constants.NODETYPE_END);
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstId);
        if (wfInstance != null) {
            String nodePath = wfInstance.getSystemCode();
            taskflow.setSystemCode(nodePath);
        }
        taskflow.setTaskDefId(endElement.getId());
        taskflow.setTaskName(endElement.getName());
        taskflow.setParentTaskId(executionId);
        taskflow.setWorkState(Constants.TASK_STATE_END);
        taskflow.setDuedata(0L);
        if (varibaleMap != null) {
            taskflow.setSuperTaskId((String) varibaleMap.get(Constants.BEFOR_TASKID));
            taskflow.setBussinessMap(JSONObject.toJSONString(varibaleMap).toString());
        }
        String desc = wfInstance.getProcessInstDesc();
        if (StrUtil.isBlank(desc)) {
            desc = wfInstance.getProcessDesc();
        }
        taskflow.setWfDesc(desc);

        // 任务紧急程度、任务发起渠道修改
        String taskUargent = "0";
        if (varibaleMap.containsKey(Constants.TASK_URGENT_KEY)) {
            taskUargent = (String) varibaleMap.get(Constants.TASK_URGENT_KEY);
        }

        String tranChannel = "";
        if (varibaleMap.containsKey(Constants.INPUT_TRANCHANNEL_KEY)) {
            tranChannel = (String) varibaleMap.get(Constants.INPUT_TRANCHANNEL_KEY);
        }
        taskflow.setTranChannel(tranChannel);
        taskflow.setTaskUrgent(Integer.parseInt(taskUargent.trim()));
        // 任务紧急程度、任务发起渠道修改

        taskflow.setTaskCode("endNode");
        LocalDateTime date = LocalDateTime.now();
        taskflow.setStartCreateDate(date);
        taskflow.setCreateDate(date);
        taskflow.setStartDate(date);
        taskflow.setUpdateDate(date);
        taskflow.setEndDate(date);

        wfTaskflowService.insertWfTaskflow(taskflow);
        WfBusinessData businessData = new WfBusinessData(taskflow);
        wfBusinessDataService.insertWfBusinessData(businessData);
    }

}
