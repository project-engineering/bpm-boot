package com.bpm.workflow.util;

import com.bpm.workflow.constant.SymbolConstants;
import java.io.Serializable;

public class GatewayRouteCondition implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1153332418233093900L;
	
	private String theConditionStr;
	public GatewayRouteCondition(String theConditionStr) {
		this.theConditionStr = theConditionStr;
	}
	public boolean notMatch(String expectedResult) {
		return !match(expectedResult);
	}
	public boolean match(String expectedResult) {
		if(this.theConditionStr == null && expectedResult == null) {
			return true;
		}
		if(this.theConditionStr == null || expectedResult == null) {
			return false;
		}
		String[] expts = expectedResult.split(SymbolConstants.COMMA);
		for(String condition:theConditionStr.split(SymbolConstants.COMMA)) {
			for(String expt:expts) {
				if(condition.equals(expt)) {
					return true;
				}
			}
		}
		return false;		
	}
	
	public static GatewayRouteCondition getInstance(String theConditionStr) {
		return new GatewayRouteCondition(theConditionStr);
	}
	
	@Override
	public String toString() {
		return theConditionStr;
	}
}
