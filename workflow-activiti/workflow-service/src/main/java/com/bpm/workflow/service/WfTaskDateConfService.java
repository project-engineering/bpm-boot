package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfTaskDateConf;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfTaskDateConfService extends IService<WfTaskDateConf> {

    List selectByNodeId(String nodeId);

    void insert(WfTaskDateConf taskDateConfig);

    void delete(WfTaskDateConf wfTaskDateConf);

    void update(WfTaskDateConf wfTaskDateConf);
}
