package com.bpm.workflow.service.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.QueryRelationTellerFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.*;
import com.bpm.workflow.engine.command.JumpCommand;
import com.bpm.workflow.engine.listener.SequenceFlowCadition;
import com.bpm.workflow.engine.listener.TaskCreateListener;
import com.bpm.workflow.entity.*;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.*;
import com.bpm.workflow.service.*;
import com.bpm.workflow.util.DateUtil;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import com.bpm.workflow.timertask.BatchInvockNextTask;
import com.bpm.workflow.timertask.StartWorkFlowTask;
import com.bpm.workflow.timertask.TimerUtil;
import com.bpm.workflow.vo.BusinessDataUpdateRequest;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.*;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 流程流转业务逻辑
 */
@Log4j2
@Service
public class ProcessCoreService {
    @Resource
    private WorkFlowSelectMapper workFlowSelectMapper;
    @Resource
    private WfEvtServiceDefService wfEvtServiceDefService;
    @Resource
    private WfBusinessDataService wfBusinessDataService;
    @Resource
    private QueryRelationTellerFactory queryRelationTellerFactory;
    @Resource
    private ActivitiBaseService activitiBaseServer;
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private WfHistTaskflowService wfHistTaskflowService;
    @Resource
    private WfCommService wfCommService;
    @Resource
    private WfInstanceService wfInstanceService;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private WfSuspendLogService suspendLogService;
    @Resource
    private ProcessEngine processEngine;
    @Resource
    private TimerUtil timerUtil;
    @Resource
    private WfTemplateService wfTemplateService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    @Resource
    private WfParamService wfParamService;
    @Resource
    private TaskCreateListener taskCreateListener;
    @Resource
    private ManageTaskService manageTaskService;
    @Resource
    private WfRuIdentitylinkService wfRuIdentitylinkService;
    @Resource
    private SequenceFlowCadition sequenceFlowCadition;
    @Resource
    private WfViewTaskLogService wfViewTaskLogService;
    @Resource
    private WfAcceditRecordService wfAcceditRecordService;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private ParamShowConfigService paramShowConfigService;
    @Resource
    private WfProcessBusinessRelMapper wfProcessBusinessRelMapper;

    /**
     * 根据规则获取流程编码
     *
     * @param workFlowStartData
     */
    public void getProcessCodeByRule(WorkFlowStartData workFlowStartData) {
        WfVariableUtil.setsysCommHead(workFlowStartData.getSysCommHead());
        String processCode = workFlowStartData.getProcessCode();
        String systemIdCode = workFlowStartData.getSystemIdCode();
        Object objValue;
        Map<String, Object> varMap = workFlowStartData.getBussinessMap();

        if (StrUtil.isBlank(systemIdCode)) {
            // 从bussinessMap中获取systemIdCode信息
            if (varMap != null) {
                objValue = varMap.get(Constants.SYSTEMIDCODE_KEY);
                if (objValue != null) {
                    systemIdCode = objValue.toString();
                }
            }
            if (StrUtil.isBlank(systemIdCode)) {
                systemIdCode = Constants.PARAM_TYPE_DEFAULT;
            }
        }

        if (StrUtil.isBlank(processCode)) {
            // 根据流程编码查找规则,直接读取参数配置表信息
            List<WfParam> listParam = wfParamService.selectParamByParamTypeParamCode(Constants.PARAM_TYPE_PROCESSCODE,
                    systemIdCode);

            log.debug(" | - ProcessCoreServiceImpl>>>>>根据流程编码查找规则,直接读取参数配置表信息:{}", JSON.toJSON(listParam));
            if (listParam != null && listParam.size() > 0) {
                processCode = listParam.get(0).getParamObjCode();
            }
            // 获取流程编码
            if (StrUtil.isBlank(processCode)) {
                throw new ServiceException(ErrorCode.WF000009);
            }
        }
        workFlowStartData.setProcessCode(processCode);
        workFlowStartData.setSystemIdCode(systemIdCode);

    }

    /**
     * 启动流程
     *
     * @param workFlowStartData
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"deprecation", "unchecked"})
    @Transactional
    public SubmitReturnData startWorkflowDef(WorkFlowStartData workFlowStartData) {

        SubmitReturnData returnData = new SubmitReturnData();
        String userId = workFlowStartData.getUserId();                    // 流程发起人
        Map<String, Object> userMap = workFlowStartData.getUserIdMap();    // 流程发起人信息
        // 流程参数
        Map<String, Object> varMap = workFlowStartData.getBussinessMap();
        if (varMap == null) {
            varMap = new HashMap<>();
        }
        // 报文头处理
        SysCommHead sysCommHead = workFlowStartData.getSysCommHead();
        WfVariableUtil.setsysCommHead(sysCommHead);

        String specifyNextOpers = (String) varMap.get(Constants.SPECIFYEXTOPERS);    // 指定人员
        String specifyNextNode = (String) varMap.get(Constants.SPECIFYNEXTNODE);    // 指定节点

        // 0、 根据事项小类获取业务存储字段
        this.getBusiFieldByConfig(workFlowStartData.getSystemIdCode(), varMap);

        // 1、 获取流程定义信息和流程启动变量
        this.getStartWorkFlowVar(workFlowStartData, null, varMap);

        // 2、获取流程定义
        String processCode = workFlowStartData.getProcessCode();
        WfDefinition wfDefinition = cacheDaoSvc.selectActiveFlowByDefId(processCode);

        //2.流程渠道合法性检查
//		String inputChannelStr = sysCommHead.getTranChannel();
//		checkChannelLegal(null, processCode, inputChannelStr);

        //增加流水号唯一性验证,只在主流程做检查，子流程不做检查，否则子流程和主流流水号程重复
//		String globalSeqNo=sysCommHead.getGlobalSeqNo();
//		if(StrUtil.isNotEmpty(globalSeqNo) && !workFlowStartData.getChildFlag())
//		{
//			validGlobalSeqNo(globalSeqNo,varMap.get(Constants.SYSTEM_CODE_KEY),processCode);
//		}

        // 3、定时启动流程
        if (workFlowStartData.getStartTime() != null) {
            Date startDate = workFlowStartData.getStartTime();
            workFlowStartData.setStartTime(null);
            timerUtil.scheduleTask(new StartWorkFlowTask(workFlowStartData), 1, startDate.getMonth() + 1,
                    startDate.getDate(), -1, startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
            return returnData;
        }

        if (StrUtil.isBlank(processCode) && wfDefinition == null) {
            wfDefinition = cacheDaoSvc.selectActiveFlowByDefId(workFlowStartData.getProcessCode());
        }

        // 4、调用activity api启动流程
        ProcessInstance processInstance = activitiBaseServer.startWorkFlow(workFlowStartData.getProcessCode(),
                varMap);
        // 流程rootProcessId
        if (varMap.get(Constants.ROOT_PROCESS_ID) == null) {
            varMap.put(Constants.ROOT_PROCESS_ID, processInstance.getId());
        }
        // 流程rootProcessCode
        if (varMap.get(Constants.ROOT_PROCESS_CODE) == null) {
            varMap.put(Constants.ROOT_PROCESS_CODE, processInstance.getProcessDefinitionId());
        }
        // 流程rootProcessName
        if (varMap.get(Constants.ROOT_PROCESS_NAME) == null) {
            varMap.put(Constants.ROOT_PROCESS_NAME, wfDefinition.getDefName());
        }

        // 5、查询启动流程所生成的任务
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                .list();
        if (tasks == null || tasks.size() == 0) {
            throw new ServiceException(ErrorCode.WF000031);
        }

        // 6、触发流程启动回调
        Map<String, Object> tmpmap = processUtil.doExecutionStartListenerNotify(processInstance, tasks.get(0), sysCommHead);
        varMap.putAll(tmpmap);

        // 7、增加流程实例
        if (varMap.get(Constants.STARTUSERDATA) != null) {
            userMap = (Map<String, Object>) varMap.get(Constants.STARTUSERDATA);
        }

        WfInstance wfInstance = addWfInstance(processInstance, userId, userMap, wfDefinition.getDefName(),
                workFlowStartData.getProcessInstDesc(), wfDefinition.getDefDesc(), varMap);

        // 8、扩展表生成代办任务、代办任务的处理（首节点跳过、自动节点）等
        Map<Task, FlowElement> taskInfos = processUtil.getNewTaskAfterStart(processInstance, tasks);
        if (taskInfos != null && taskInfos.size() > 0) {
            for (Map.Entry<Task, FlowElement> anEntry : taskInfos.entrySet()) {
                // 首节点跳过可以少查一步
                SubmitReturnData returnData1 = null;
                Object obj = varMap.get(ModelProperties.skipFistNode.getName());
                if (obj != null && Boolean.parseBoolean(obj.toString())) {// 首节点跳过时,重置原前台指定的用户
                    varMap.put("firstNode", true);
                    taskCreateListener.doNotify(anEntry.getValue(), varMap, processInstance.getProcessDefinitionId(),
                            processInstance.getProcessInstanceId(), anEntry.getKey().getId(),
                            anEntry.getKey().getTaskDefinitionKey(), anEntry.getKey().getName(), wfInstance, sysCommHead);
                    if (taskInfos.size() == 0) {
                        throw new ServiceException("流程配置错误，首节点没有后续任务节点");
                    }
                    if (taskInfos.size() != 1) {
                        throw new ServiceException("流程配置错误，首节点不能发出多个并发");
                    }
                    varMap.put(Constants.SPECIFYEXTOPERS, specifyNextOpers);
                    varMap.put(Constants.SPECIFYNEXTNODE, specifyNextNode);
                    // 处理首节点跳过
                    returnData1 = processUtil.dealWithSkipFirstNode(anEntry.getKey(), true, varMap, userId,
                            processInstance, sysCommHead, workFlowStartData);
                } else {
                    taskCreateListener.doNotify(anEntry.getValue(), varMap, processInstance.getProcessDefinitionId(),
                            processInstance.getProcessInstanceId(), anEntry.getKey().getId(),
                            anEntry.getKey().getTaskDefinitionKey(), anEntry.getKey().getName(), wfInstance, sysCommHead);
                }
                // 如果是首节点跳过则不调用自动节点
                if (returnData1 != null) {
                    return returnData1;
                }
                returnData.setProcessState(Constants.WORK_STATE_RUNNING);
                returnData.setProcessInstId(processInstance.getId());
                varMap.put(Constants.BEFOR_TASKID, anEntry.getKey().getId());
                this.autoExec(anEntry.getKey(), processInstance, varMap, sysCommHead);

            }
        }
        return returnData;
    }

    /**
     * @Description: 根据配置获取业务字段
     * @Param: [workFlowStartData, varMap]
     * @Return: void
     * @Author:
     * @Date: 2021/11/15
     */
    @SuppressWarnings("unchecked")
    private void getBusiFieldByConfig(String systemIdCode, Map<String, Object> varMap) {
        // 根据事项小类查询业务配置字段
        List<ParamShowConfig> busiFieldList = paramShowConfigService.lambdaQuery()
                .eq(StrUtil.isNotBlank(systemIdCode), ParamShowConfig::getSystemIdCode, systemIdCode)
                .eq(ParamShowConfig::getType, BpmConstants.SHOW_CONFIG_0)
                .list();

        if (cn.hutool.core.util.ObjectUtil.isNotEmpty(busiFieldList)) {
            ParamShowConfig paramShowConfig = busiFieldList.get(0);
            // 获取业务数据Map
            Map<String, Object> dataMap = (Map<String, Object>) varMap.get(BpmConstants.DATA_MAP_KEY);
            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(dataMap)) {
                Map<String, Object> businessSave = new HashMap<>();
                // 配置信息转JSON
                cn.hutool.json.JSONArray contentArray = JSONUtil.parseArray(paramShowConfig.getContent());
                for (Object content : contentArray) {
                    // 解析配置内容
                    cn.hutool.json.JSONObject contentObj = JSONUtil.parseObj(content);
                    String targetKey = (String) contentObj.get(BpmConstants.TARGET_KEY);
                    String sourceKey = (String) contentObj.get(BpmConstants.SOURCE_KEY);
                    if (StrUtil.isNotEmpty(targetKey) && StrUtil.isNotEmpty(sourceKey)) {
                        businessSave.put(targetKey, dataMap.get(sourceKey));
                    }
                }
                varMap.put(Constants.BUSINESS_SAVE, businessSave);
            }
        }
    }

    /**
     * 验证流水号是否重复提交
     *
     * @param globalSeqNo
     * @param systemCode
     * @param processCode
     */
    private void validGlobalSeqNo(String globalSeqNo, Object systemCode, String processCode) {
        if (Constants.STRING_TRUE.equals(configUtil.getCheckGlobalNumber())) {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("globSeqNo", globalSeqNo);
            paramMap.put(BpmConstants.PROCESS_CODE, processCode);
            paramMap.put("systemCode", systemCode);
            int i = wfTaskflowService.checkGlobSeqNoIsExsit(paramMap);
            if (i > 0) {
                throw new ServiceException("该流程已经存在流水号【" + globalSeqNo + "】的流程，请检查是否重复提交");
            }
        }
    }

    /**
     * 设置并验证流程启动的必要参数
     */
    private Map<String, Object> getStartWorkFlowVar(WorkFlowStartData workFlowStartData, WfDefinition wfDefinine,
                                                    Map<String, Object> varMap) {
        String userId = workFlowStartData.getUserId();

        Object objValue;
        if (StrUtil.isBlank(userId)) {
            // 从userIdMap中获取userId信息
            if (workFlowStartData.getUserIdMap() != null) {
                objValue = workFlowStartData.getUserIdMap().get(Constants.USERID_KEY);
                if (objValue != null) {
                    userId = objValue.toString();
                }
            }
            // 流程发起人校验
            if (StrUtil.isBlank(userId)) {
                throw new ServiceException(ErrorCode.WF000008);
            }
        }

        // 检出并获取流程编码信息
        getProcessCodeByRule(workFlowStartData);
        String processCode = workFlowStartData.getProcessCode();
        String systemIdCode = workFlowStartData.getSystemIdCode();
        // 查询流程定义
        if (wfDefinine == null) {
            wfDefinine = cacheDaoSvc.selectActiveFlowByDefId(processCode);
        }
        if (wfDefinine == null) {
            throw new ServiceException("找不到流程编码为【" + processCode + "】的流程");
        }
        // 查询流程发起人信息
        Map<String, Object> userIdMap = null;
        if (BpmConstants.WORKFLOW_ADMIN_ID.equals(userId)) {
            userIdMap = BpmConstants.ADMIN_START_MAP;
        } else {
            userIdMap = this.queryUserList(userId);
        }

        if (varMap.get(Constants.ROOT_PROCESS_ID) == null) {
            Map<String, Object> beforeUserData = new HashMap<>(userIdMap);
            varMap.put(Constants.BEFORUSERDATA, beforeUserData);
            varMap.put(Constants.BEFOR_USERID, userId);
        }
        varMap.put(Constants.STARTUSERDATA, userIdMap);
        Object reqSystemCode = varMap.get(Constants.SYSTEM_CODE_KEY);

        // 流程事项大小类处理
        if (reqSystemCode == null || reqSystemCode.toString().trim().length() < 1) {
            String systemCodeDef = null;

            // 获取流程大类、小类信息,将大类、小类值替换为systemCode
            // 因为事项小类是唯一的,所以调整获取方法来获取信息
            List<WfParam> listParam = wfParamService.selectParamByParamTypeParamCode(Constants.PARAM_TYPE_L, systemIdCode);

            if (listParam != null && listParam.size() > 0) {
                // 小类必须挂在大类下面
                systemCodeDef = listParam.get(0).getParamObjCode() + SymbolConstants.COLON + listParam.get(0).getParamObjName();
                // 获取小类名称
                systemCodeDef = systemCodeDef + ";" + systemIdCode + SymbolConstants.COLON + listParam.get(0).getParamName();
            } else {
                //modify amt start 20190821 优化启动流程，当按照流程编码启动时，关联大小类，用对应的大小类里面进行拼装
                if (Objects.nonNull(processCode)) {
                    List<WfParam> wfParamList = wfParamService.selectPramByProcessCode(processCode);
                    if (wfParamList != null && wfParamList.size() == NumberConstants.INT_TWO) {
                        // 小类必须挂在大类下面
                        systemCodeDef = wfParamList.get(0).getParamObjCode() + SymbolConstants.COLON + wfParamList.get(0).getParamObjName();
                        // 获取小类名称
                        systemCodeDef = systemCodeDef + ";" + wfParamList.get(1).getParamCode() + SymbolConstants.COLON + wfParamList.get(1).getParamName();
                    }
                }
                //modify amt end 20190821 优化启动流程，当按照流程编码启动时，关联大小类，用对应的大小类里面进行拼装

                if (StrUtil.isEmpty(systemCodeDef)) {
                    systemCodeDef = processCode + SymbolConstants.COLON + wfDefinine.getDefName();
                    systemCodeDef = systemCodeDef + ";" + systemIdCode + SymbolConstants.COLON + Constants.PARAM_TYPE_DEFNAME;
                }

            }

            //update by xuesw 20180418 任务紧急程度修改
            Map<String, Object> temp = new HashMap<>();
            if (listParam != null && listParam.size() > 0) {
                temp.put(Constants.TASK_URGENT_KEY, listParam.get(0).getParamUrgent());
            }
            wfCommService.setTaskUargent(temp, varMap);
            // update  end by xuesw 20180418 任务紧急程度修改

            varMap.put(Constants.SYSTEM_CODE_KEY, systemCodeDef);
        }

        // 获取流程是否跳过首节点
        boolean boolSkipNode;
        if (varMap.containsKey("skipFistNode") && cn.hutool.core.util.ObjectUtil.isNotEmpty(varMap.get("skipFistNode"))) {
            boolSkipNode = Boolean.parseBoolean(varMap.get("skipFistNode").toString());
        } else {
            boolSkipNode = processUtil.isSikpFirstNode(processCode);
        }
        if (boolSkipNode) {
            // 首节点跳过,处理人员按照要求,必须为流程发起人员
            varMap.put(Constants.SPECIFYEXTOPERS, userId);
            varMap.put(Constants.SPECIFYNEXTNODE, null);
        }
        // 增加首节点跳过控制变量
        if (cn.hutool.core.util.ObjectUtil.isEmpty(varMap.get(ModelProperties.skipFistNode.getName()))) {
            varMap.put(ModelProperties.skipFistNode.getName(), boolSkipNode);
        }
        //设置流程的创建时间
        if (!varMap.containsKey(Constants.PROCESS_CREATE_TIME)) {
            varMap.put(Constants.PROCESS_CREATE_TIME, new Date());
        }

        log.info("| - ProcessCoreServiceImpl>>>>>ready to getStartWorkFlowVar in endEachTaskProcess");
        WfVariableUtil.setVariableMap(varMap);
        return varMap;
    }

    /**
     * 通过业务参数来获取交易发起的渠道号
     *
     * @return
     */
    private String mappChannelNubFormBusiness(String inputChannel) {
        if (StrUtil.isBlank(inputChannel)) {
            return inputChannel;
        }
        List<WfParam> list = wfParamService.selectParamByParamTypeParamCode(Constants.PARAM_CHANNEL_MAP, inputChannel);
        if (list != null && !list.isEmpty()) {
            return list.get(0).getParamObjCode();
        }
        return inputChannel;
    }

    /**
     * @Description: 检查渠道的合法性
     * @Param: [taskNodeId, processCode, inputChannelStr]
     * @Return: boolean
     * @Author:
     * @Date: 2021/9/23
     */
    private boolean checkChannelLegal(String taskNodeId, String processCode, String inputChannelStr) {
        // 1.获取渠道号，如果没有从业务参数映射出来
        if (StrUtil.isBlank(inputChannelStr)) {
            inputChannelStr = mappChannelNubFormBusiness(inputChannelStr);
            if (StrUtil.isBlank(inputChannelStr)) {
                return true;
            }
        }
        String[] codes = StrUtil.splitToArray(processCode, SymbolConstants.COLON);
        processCode = codes[0];

        // 2.获取任务的可处理的渠道列表：节点列表配置为空字符串标识获取流程定义的
        String channelValueList = "";
        if (StrUtil.isBlank(taskNodeId)) {
            // 流程启动的渠道合法性检查
            WfModelPropExtends channelList = wfModelPropExtendsService.viewModelPropExtend(processCode,
                    ModelProperties.channelFlowCtrlList.getName());
            if (channelList != null) {
                channelValueList = channelList.getPropValue();
            }
        } else {
            // 任务处理的渠道合法性检查
            WfModelPropExtends channelList = wfModelPropExtendsService.viewModelPropExtend(taskNodeId,
                    ModelProperties.channelNodeCtrlList.getName());
            if (channelList != null) {
                channelValueList = channelList.getPropValue();
                if (StrUtil.isBlank(channelValueList)) {
                    // 使用流程定义的渠道号进行检查
                    channelList = wfModelPropExtendsService.viewModelPropExtend(processCode,
                            ModelProperties.channelFlowCtrlList.getName());
                    if (channelList != null) {
                        channelValueList = channelList.getPropValue();
                    }
                }
            }
        }

        // 3.比对输入渠道是否合法
        if (StrUtil.isBlank(channelValueList)) {
            return true;// 渠道没有限制
        } else {
            boolean isInList = false;
            for (String str : channelValueList.split(SymbolConstants.COMMA)) {
                if (str.equals(inputChannelStr)) {
                    isInList = true;
                    break;
                }
            }
            if (!isInList) {
                if (!StrUtil.isBlank(taskNodeId)) {
                    throw new ServiceException("任务不能从渠道【" + inputChannelStr + "】进行处理!");
                } else {
                    throw new ServiceException("流程【" + processCode + "】不能从渠道【" + inputChannelStr + "】发起!");
                }
            }
        }
        return true;
    }


    /**
     * 处理首节点是自动节点的服务
     *
     * @param task
     * @param processInstance
     * @param varMap
     */
    private void autoExec(Task task, ProcessInstance processInstance, Map<String, Object> varMap, SysCommHead sysCommHead) {
        FlowElement flowElement = processUtil.getTaskFlowElement(processInstance, task);

        String nodeType = processUtil.getTaskProp(task.getProcessDefinitionId(), flowElement, Constants.NODETYPE);// flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
        if (nodeType.equals(Constants.NODE_TYPE_ATTO)) {
            if (log.isInfoEnabled()) {
                log.debug("| - ProcessCoreServiceImpl>>>>>开始执行自动跳转[taskId={}]", task.getId());
            }
            varMap.put(Constants.BEFOR_TASKID, BpmConstants.START_TASK_ID);
            wfEvtServiceDefService.autoTaskNodeHandler(task.getId(), task.getProcessDefinitionId(),
                    task.getProcessInstanceId(), varMap, sysCommHead, flowElement);

            log.debug("| - ProcessCoreServiceImpl>>>>>完成[taskId={}]节点的自动跳转", task.getId());
            List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
                    .list();
            if (tasks == null || tasks.isEmpty()) {
                // 5.流程结束的判断
                processUtil.doAfterEndProcess(processInstance.getProcessDefinitionId(), processInstance.getId(), null);

            }
            FlowNode flowNode = (FlowNode) flowElement;
            List<SequenceFlow> sequenceList = flowNode.getOutgoingFlows();
            //流程图的节点和代办任务匹配，调用本节点离开事件，确认下一个任务是否自动节点，如果是自动节点，则
            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(tasks)) {
                for (Task t : tasks) {
                    for (SequenceFlow sf : sequenceList) {
                        FlowElement target = sf.getTargetFlowElement();
                        if (t.getTaskDefinitionKey().equals(target.getId())) {
                            //自动节点调用
                            nodeType = processUtil.getTaskProp(t.getProcessDefinitionId(), target, Constants.NODETYPE);
                            if (nodeType != null && nodeType.equals(Constants.NODE_TYPE_ATTO)) {
                                //递归调用
                                Map<String, Object> newMap = WfVariableUtil.getVariableMap();
                                if (newMap == null) {
                                    newMap = new HashMap<>(varMap);
                                }
                                this.autoExec(t, processInstance, newMap, sysCommHead);
                            }
                        }
                    }
                }
            }
        } else if (nodeType.equals(Constants.NODETYPE_SUBPROC)) {
            taskCreateListener.commitSubFlowTask(varMap, task, flowElement);
        }
    }

    /**
     * 1、查询流程图中对应的下一步的节点信息。
     * 2、查询所有未办理的流程实例任务 、
     * 3、如果下一步是自动节点，则使用代办任务的节点id去匹配，匹配上则认为匹配的代办任务为自动节点的上一个节点
     * 4、如果找不到则认为是任务异常
     *
     * @param taskflow       下一步提交前的task信息
     * @param endEventInvock 节点类型
     * @param flowElement    提交前的节点
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private void autoExec(WfModelPropExtends endEventInvock, WfTaskflow taskflow, FlowElement flowElement,
                          Map<String, Object> exectMap, WorkFlowTaskTransferData taskTransferData, SysCommHead sysCommHead) throws Exception {
        //查询所有的代办任务
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(taskflow.getProcessId())
                .list();
        String nodeType = null;
        FlowNode flowNode = (FlowNode) flowElement;
        List<SequenceFlow> sequenceList = flowNode.getOutgoingFlows();
        Map<String, Object> varMap = BpmStringUtil.jsonToMap(taskflow.getBussinessMap());
        //流程图的节点和代办任务匹配，调用本节点离开事件，确认下一个任务是否自动节点，如果是自动节点，则
        if (cn.hutool.core.util.ObjectUtil.isNotEmpty(tasks)) {
            for (Task t : tasks) {
                for (SequenceFlow sf : sequenceList) {
                    FlowElement target = sf.getTargetFlowElement();
                    if (target instanceof InclusiveGateway) {
                        InclusiveGateway gw = (InclusiveGateway) target;
                        for (SequenceFlow sf1 : gw.getOutgoingFlows()) {
                            if (sf1.getTargetRef().equals(t.getTaskDefinitionKey())) {
                                target = sf1.getTargetFlowElement();
                                break;
                            }
                        }
                    }
                    if (t.getTaskDefinitionKey().equals(target.getId())) {
                        if (endEventInvock != null) {
                            // 事件调用
                            exectMap.put(Constants.DO_RESAULT, taskTransferData.getResult());
                            exectMap.put(Constants.LAST_TASK_NAME, taskflow.getTaskName());
                            //之所以放到这里是为了取下一个任务的节点名称及处理人员
                            exectMap.put(Constants.TASK_NAME, t.getName());
                            exectMap.put(Constants.TASK_USERS, exectMap.get(Constants.TASK_USERS));
                            //节点离开事件调用
                            wfEvtServiceDefService.executeEventService(endEventInvock, taskflow.getTaskId(),
                                    taskflow.getProcessCode(), taskflow.getProcessId(), exectMap, flowElement, sysCommHead);
                        }


                        //自动节点调用
                        nodeType = processUtil.getTaskProp(t.getProcessDefinitionId(), target, Constants.NODETYPE);//target.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
                        if (nodeType != null && nodeType.equals(Constants.NODE_TYPE_ATTO)) {
                            //2019-06-28 amt modify start 当生成多个代办任务时，已经生成了对应的代办，则返回继续遍历map
                            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(tasks)) {
                                WfTaskflow existTask = selectByTaskId(t.getId());
                                if (Objects.nonNull(existTask)) {
                                    continue;
                                }

                            }
                            //2019-06-28 amt modify end 当生成多个代办任务时，已经生成了对应的代办，则返回继续遍历map

                            if (log.isInfoEnabled()) {
                                log.info("| - ProcessCoreServiceImpl>>>>>开始执行自动跳转[taskId={}]", taskflow.getTaskId());
                            }
                            varMap.put(Constants.BEFOR_TASKID, taskflow.getTaskId());

                            Map<String, Object> beforUserData = new HashMap<>((Map<String, Object>) exectMap.get("beforUserData"));
                            // 自动节点，不能重置发起人信息  20231226
                            // exectMap.put("userIdMap",beforUserData);
                            exectMap.put("taskUsers", exectMap.get("beforUserId"));
                            varMap.putAll(exectMap);
                            varMap.remove(Constants.ROUTE_CONDITION_KEY);
                            varMap.remove(Constants.SPECIFYNEXTNODE);
                            varMap.remove(Constants.SPECIFYEXTOPERS);
                            varMap.remove(Constants.SPECIFY_NEXT_OPERS_LIST);
                            wfEvtServiceDefService.autoTaskNodeHandler(t.getId(), t.getProcessDefinitionId(),
                                    t.getProcessInstanceId(), varMap, sysCommHead, target);
                            String waitType = target.getAttributeValue(Constants.XMLNAMESPACE, Constants.WAITTYPE);

                            log.info("| - ProcessCoreServiceImpl>>>>>完成[taskId={}]节点的自动跳转", taskflow.getTaskId());

                            //以下是处理递归调用的，这里应该做个判断，如果本节点是异步等待或者同步等待，则直接返回
                            if (waitType.contains(Constants.CALL_MODE_ASYNC_WAIT) || waitType.contains(Constants.CALL_MODE_SYNC_WAIT)) {
                                //当生成多个代办任务时，已经生成了对应的代办，则返回继续遍历map
                                continue;
                            }
                            //查询所有的代办任务
                            tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(taskflow.getProcessId())
                                    .list();
                            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(tasks)) {
                                for (Task t1 : tasks) {
                                    //当生成多个代办任务时，已经生成了对应的代办，则返回继续遍历map
                                    if (tasks.size() > 1) {
                                        WfTaskflow existTask = selectByTaskId(t1.getId());
                                        if (Objects.nonNull(existTask)) {
                                            continue;
                                        }
                                    }
                                    FlowElement target1 = getFlowTaskTarget4ThisOut(t1.getTaskDefinitionKey(), target);
                                    if (target1 != null) {
                                        //自动节点调用
                                        nodeType = processUtil.getTaskProp(t1.getProcessDefinitionId(), target1, Constants.NODETYPE);
                                        String nodeId = processUtil.getTaskProp(t1.getProcessDefinitionId(), target1, Constants.NODEPROP_REFID);
                                        if (nodeType != null && nodeType.equals(Constants.NODE_TYPE_ATTO)) {
                                            List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);
                                            // 节点结束根据配置进行第三方回调
                                            WfModelPropExtends endEventInvock1 = modelPropService.getSpcefiyModelProp(extendsPropList,
                                                    ModelProperties.endTaskEvent.getName());
                                            exectMap.remove(Constants.ROUTE_CONDITION_KEY);
                                            WfTaskflow tf = new WfTaskflow();
                                            tf.setTaskId(t1.getId());
                                            tf.setProcessCode(t1.getProcessDefinitionId());
                                            tf.setTaskName(t1.getName());
                                            tf.setProcessId(t1.getProcessInstanceId());
                                            Map<String, Object> newMap = WfVariableUtil.getVariableMap();
                                            if (newMap == null) {
                                                newMap = new HashMap<>(varMap);
                                            }
                                            tf.setBussinessMap(BpmStringUtil.mapToJson(newMap).toJSONString());
                                            this.autoExec(endEventInvock1, tf, target, newMap, taskTransferData, sysCommHead);
                                        }

                                    }
                                    //自动节点的下一个是子流程节点时，增加子流程的处理
                                    else if (nodeType != null && nodeType.equals(Constants.NODETYPE_SUBPROC)) {
                                        //下一节点为子流程时，需要加入主流程的参数，否则如果前一个流程为子流程时，前一个子流程的参数不能传递过来
                                        varMap.putAll(exectMap);
                                        varMap.put(Constants.SPECIFYNEXTNODE, taskTransferData.getBussinessMap().get(Constants.SPECIFYNEXTNODE));
                                        varMap.put(Constants.SPECIFYEXTOPERS, taskTransferData.getBussinessMap().get(Constants.SPECIFYEXTOPERS));
                                        //避免历史的路径变量对流转的影响，以至于出现找不到路径的情况
                                        varMap.remove(Constants.ROUTE_CONDITION_KEY);
                                        taskCreateListener.commitSubFlowTask(varMap, t, target1);
                                    }
                                }
                            }
                        } else if (nodeType != null && nodeType.equals(Constants.NODETYPE_SUBPROC)) {
                            //下一节点为子流程时，需要加入主流程的参数，否则如果前一个流程为子流程时，前一个子流程的参数不能传递过来
                            varMap.putAll(exectMap);
                            varMap.put(Constants.SPECIFYNEXTNODE, taskTransferData.getBussinessMap().get(Constants.SPECIFYNEXTNODE));
                            varMap.put(Constants.SPECIFYEXTOPERS, taskTransferData.getBussinessMap().get(Constants.SPECIFYEXTOPERS));
                            //避免历史的路径变量对流转的影响，以至于出现找不到路径的情况
                            varMap.remove(Constants.ROUTE_CONDITION_KEY);
                            //wanglh 20190802 子流程节点启动时，需要删除首节点跳过key,否则,子流程中配置的首节点属性无效
                            varMap.remove(ModelProperties.skipFistNode.getName());
                            //wanglh 20190802 end
                            taskCreateListener.commitSubFlowTask(varMap, t, target);
                        }
                    }
                }
            }
        } else {
            // 流程结束
            if (endEventInvock != null) {
                exectMap.put(Constants.DO_RESAULT, taskTransferData.getResult());
                exectMap.put(Constants.LAST_TASK_NAME, taskflow.getTaskName());
                exectMap.put(Constants.TASK_NAME, sequenceList.get(0).getTargetFlowElement().getName());
                // varMap.put(Constants.TASK_USERS,exectMap.get(Constants.TASK_USERS));
                exectMap.remove(Constants.ROUTE_CONDITION_KEY);
                //节点离开事件调用
                wfEvtServiceDefService.executeEventService(endEventInvock, taskflow.getTaskId(), taskflow.getProcessCode(),
                        taskflow.getProcessId(), exectMap, flowElement, sysCommHead);
            }
        }
    }


    public WfTaskflow selectByTaskId(String taskId) {
        WfTaskflow taskflow = wfTaskflowService.selectByTaskId(taskId);
        if (taskflow != null) {
            WfBusinessData businessdata = wfBusinessDataService.selectById(taskflow.getId().toString());
            if (businessdata != null) {
                taskflow.setBusinessData(businessdata);
            }
        }
        return taskflow;
    }

    private FlowElement getFlowTaskTarget4ThisOut(String theTaskDefKey, FlowElement thisFlowNode) {
        if (StrUtil.isEmpty(theTaskDefKey) || thisFlowNode == null) {
            return null;
        }
        try {
            List<SequenceFlow> sequenceList1 = ((FlowNode) thisFlowNode).getOutgoingFlows();
            if (sequenceList1 == null || sequenceList1.size() == 0) {
                return null;
            }
            for (SequenceFlow sf1 : sequenceList1) {
                FlowElement target1 = sf1.getTargetFlowElement();
                if (target1 instanceof InclusiveGateway) {
                    FlowElement result = getFlowTaskTarget4ThisOut(theTaskDefKey, target1);
                    if (result != null) {
                        return result;
                    }
                } else if (theTaskDefKey.equals(target1.getId())) {
                    return target1;
                }
            }
            return null;
        } catch (Exception e) {
            log.error(BpmStringUtil.getBusinessErrorCode(e));
            return null;
        }
    }


    /**
     * 流程启动获取流程发起人明细信息
     *
     * @param userIdMap
     * @return
     */
    public Map<String, Object> queryUserList(Map<String, Object> userIdMap) {
        Map<String, Object> returnMap = new HashMap<String, Object>();
        if (userIdMap != null) {
            returnMap.putAll(userIdMap);
            List<Map<String, String>> idList = new ArrayList<Map<String, String>>();
            Map<String, String> tempMap = new HashMap<String, String>();
            for (String mapKey : userIdMap.keySet()) {// 临时处理
                if (userIdMap.get(mapKey) != null && !"null".equals(userIdMap.get(mapKey).toString())) {
                    if ("userId".equalsIgnoreCase(mapKey)) {
                        tempMap.put("orderUserNo", userIdMap.get("userId").toString());
                    } else {
                        tempMap.put(mapKey, userIdMap.get(mapKey).toString());
                    }
                }
            }
            idList.add(tempMap);
            List<UserOutputData> userDataList = null;
            if (log.isInfoEnabled()) {
                log.debug("| - ProcessCoreServiceImpl>>>>>开始查询流程发起人信息：{}", tempMap.toString());
            }
            try {
                userDataList = queryRelationTellerFactory.getInstance().queryUserList(BpmStringUtil.listToJson(idList));
            } catch (Exception e) {
                log.error("| - ProcessCoreServiceImpl>>>>>流程启动时，流程发起人信息查询失败：", e);
                returnMap.putAll(userIdMap);
                return returnMap;
            }
            if (userDataList != null && userDataList.size() > 0) {
                if (userDataList.size() == 1) {
                    UserOutputData userOutputData = userDataList.get(0);
                    Map<String, Object> result = BpmStringUtil.jsonToMap(JSONObject.toJSONString(userOutputData));
                    Map<String, Object> map1 = this.notNvlMap(result);
                    returnMap.putAll(map1);
                    return returnMap;
                } else if (userDataList == null || userDataList.size() == 0) {
                    throw new ServiceException("流程发起人不存在：" + userIdMap);
                }
                Map<String, Object> userIdMap1 = null;
                for (UserOutputData userOutputData : userDataList) {
                    Map<String, Object> userIdMap2 = BpmStringUtil.jsonToMap(JSONObject.toJSONString(userOutputData));
                    if (userIdMap.get(Constants.USERID_KEY).toString().equals(userIdMap2.get(Constants.USERID_KEY).toString())) {
                        userIdMap1 = userIdMap2;
                        break;
                    }
                }
                if (userIdMap1 == null) {
                    userIdMap1 = BpmStringUtil.jsonToMap(JSONObject.toJSONString(userDataList.get(0)));
                }
                Map<String, Object> map1 = this.notNvlMap(userIdMap1);
                returnMap.putAll(map1);
                return returnMap;
            }
        }
        return returnMap;
    }

    /**
     * 流程启动获取流程发起人明细信息
     *
     * @param userId
     * @return
     */
    public Map<String, Object> queryUserList(String userId) {
        Map<String, Object> returnMap = new HashMap<>();
        List<UserOutputData> userDataList = null;
        if (log.isInfoEnabled()) {
            log.debug("| - ProcessCoreServiceImpl>>>>>开始查询流程发起人信息：{}", userId);
        }
        try {
            userDataList = queryRelationTellerFactory.getInstance().queryUserList(userId);
        } catch (Exception e) {
            log.error("| - ProcessCoreServiceImpl>>>>>流程启动时，流程发起人信息查询失败：", e);
        }

        if (userDataList != null && userDataList.size() > 0) {
            UserOutputData userOutputData = userDataList.get(0);
            Map<String, Object> result = BpmStringUtil.jsonToMap(JSONObject.toJSONString(userOutputData));
            Map<String, Object> map1 = this.notNvlMap(result);
            returnMap.putAll(map1);
            return returnMap;
        }
        return returnMap;
    }

    private Map<String, Object> notNvlMap(Map<String, Object> map) {
        Map<String, Object> map1 = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                map1.put(entry.getKey(), entry.getValue());
            }
        }
        return map1;
    }

    /**
     * 流程执行下一步
     */
    @Transactional
    public SubmitReturnData nextTask(WorkFlowTaskTransferData taskTransferData) {

        SubmitReturnData returnData = new SubmitReturnData();
        String userId = taskTransferData.getUserId();
        String taskId = taskTransferData.getTaskId();
        String result = taskTransferData.getResult();
        SysCommHead sysCommHead = taskTransferData.getSysCommHead();
        WfVariableUtil.setsysCommHead(sysCommHead);

        // 对一些特殊的控制变量进行重置处理
        WfTaskflow taskflow = selectByTaskId(taskId);

        if (Objects.isNull(taskflow)) {
            throw new ServiceException("ID为" + taskId + "的任务不存在，请检查是否流程已经结束！");
        }
        if (StrUtil.equals(taskflow.getCheckInFlag(), Constants.TRAN_TYPE_UNDO_CHECKOUT)) {
            throw new ServiceException("ID为" + taskId + "的任务需要先做检出！");
        }

        //---2019-05-07 amt modify start 领用后需要判断是否处理人为领用人
        if (StrUtil.equals(taskflow.getTaskPoolFlag(), Constants.IS_HIST_TASK_POOL)) {
            WfRuIdentitylink condtion = new WfRuIdentitylink();
            condtion.setUserId(userId);
            condtion.setTaskId(taskId);
            WfRuIdentitylink users = wfRuIdentitylinkService.selectByUserIdTaskId(condtion);
            if (Objects.isNull(users)) {
                throw new ServiceException("操作失败！" + userId + "没有操作该任务[" + taskId + "]的权限！");
            }
        }
        //---2019-05-07 amt modify end 领用后需要判断是否处理人为领用人

        String taskType = taskflow.getTaskType();
        if (taskType != null && !taskType.equals(Constants.NODE_TYPE_ATTO)
                && !taskType.equals(Constants.NODETYPE_SUBPROC)) {
            UserOutputData userdata = getUserData(taskflow, userId);
            if (userdata == null || StrUtil.isEmpty(userdata.getUserName())) {
                throw new ServiceException(userId + "没有权限处理该任务！");
            }
        }


        Map<String, Object> exectMap = getNextTaskVar(taskTransferData, taskflow);
        //yezhengxian 20240223 会签节点解决之前会签节点选人人数和本节点不同的问题，清掉activity参数
        exectMap.remove("nrOfActiveInstances");
        exectMap.remove("nrOfInstances");
        exectMap.remove("nrOfCompletedInstances");
        taskTransferData.setBussinessMap(exectMap);


        // 新增调用业务接口，
//        asyncCall(userId, taskTransferData.getProcessInstId(), exectMap);

        // 0、 根据事项小类获取业务存储字段
        this.getBusiFieldByConfig(taskflow.getItemSecondCode(), exectMap);

        // taskId的合法性校验
        TaskEntity taskEntity = activitiBaseServer.findTaskById(taskId);
        // 查询任务对应节点的配置信息
        FlowElement taskNode = activitiBaseServer.getBpmnModelNodeByTaskId(taskEntity);
        String nodeType = processUtil.getTaskProp(taskEntity.getProcessDefinitionId(), taskNode, Constants.NODETYPE);

        String nodeId = processUtil.getTaskProp(taskEntity.getProcessDefinitionId(), taskNode, Constants.NODEPROP_REFID);

        //渠道合法性校验处理?超时任务sysCommHead为空，会报空指针异常
//		if(sysCommHead!=null) {
//			String inputChannelStr=sysCommHead.getTranChannel();
//			checkChannelLegal(nodeId, taskEntity.getProcessDefinitionId(), inputChannelStr);
//
//		}

        //附件处理
        wfCommService.processAccessories(taskTransferData, taskflow);

        // 动态加签任务处理，处理完直接返回
        int workType = taskflow.getWorkType().intValue();
        if (workType == Constants.WORK_TYPE_ADDITION_TASK || workType == Constants.WORK_TYPE_ADDITION_TASKNOTIF) {
            if (additionTaskCommit(taskflow, taskTransferData, exectMap)) {
                updateInstance(returnData, userId, taskflow);
                return returnData;
            }
        }
        String specifyNextNode = (String) exectMap.get(Constants.SPECIFYNEXTNODE);
        if (StrUtil.isNotBlank(specifyNextNode)) {
            BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(taskEntity.getProcessDefinitionId());
            FlowElement targetElement = bpmnModel.getFlowElement(specifyNextNode);
            if (targetElement instanceof FlowNode) {
                // 如果选择的是点，这需要自由跳转的目标节点
                taskTransferData.setTargetActivitiId(specifyNextNode);
                taskTransferData.setTranType(Constants.TRANTYPE_FREE);
                Map<String, Object> varMap = taskTransferData.getBussinessMap();
                if (varMap != null) {
                    varMap.remove(Constants.SPECIFYNEXTNODE);
                    taskTransferData.setBussinessMap(varMap);
                }
                taskTransferData.setAgentUserId((String) exectMap.get(Constants.SPECIFYEXTOPERS));
                manageTaskService.backFromTask(taskTransferData);
                updateInstance(returnData, userId, taskflow);
                return returnData;
            }
//			else if (targetElement instanceof SequenceFlow) {
//				// 如果是选择的为线，则是人工路由，此处不需要处理
//			}
        }

        // 2.特殊类型节点的处理，根据类型不同做不同操作
        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);
        // 节点结束根据配置进行第三方回调
        WfModelPropExtends endEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.endTaskEvent.getName());

        if (Constants.NODE_TYPE_SIGN.equals(nodeType)) {
            if (StrUtil.isBlank(result) || !(result.equals(Constants.RESULT_OPPOSE)
                    || result.equals(Constants.RESULT_PASS) || result.equals(Constants.RESULT_THINK))) {
                throw new ServiceException("会签节点需要指定审批结果result[0--通过，1--反对， 2 --再议]");
            }
            exectMap.put(Constants.SINGLASTTASKRESULT, result);
            exectMap.put("signResult", result);
        } else if (Constants.NODETYPE_SUBPROC.equals(nodeType)) {
            String waitType = processUtil.getTaskProp(taskEntity.getProcessDefinitionId(), taskNode, Constants.WAITTYPE);//taskNode.getAttributeValue(Constants.XMLNAMESPACE, Constants.WAITTYPE);
            if (waitType.equals(Constants.WAITTYPE_WAIT)) {
                Object subCountStr = exectMap.get("MainWfSubCount");
                if (subCountStr != null) {
                    int subCount = (int) subCountStr;
                    if (--subCount > 0) {
                        exectMap.put("MainWfSubCount", subCount);
                        taskflow.setBussinessMap(JSONObject.toJSONString(exectMap));
                        wfTaskflowService.updateById(taskflow);
                        WfBusinessData businessData = new WfBusinessData(taskflow);
                        wfBusinessDataService.updateById(businessData);
                        return returnData;
                    }
                }
            }
        }
        List<WfTaskflow> toBeFinishedSign = null;
        if (taskTransferData.isDoFinishSign()) {
            toBeFinishedSign = getToBeFinishedSignTask(taskId);
        }
        if (StrUtil.isNotBlank(taskTransferData.getPassingRouteFlag())) {
            exectMap.put(Constants.ROUTE_CONDITION_KEY_SYS, taskTransferData.getPassingRouteFlag());
        }

        // 3.真正进行流转

        if (exectMap.containsKey(Constants.ROUTE_CONDITION_KEY_SYS)) {
            WfVariableUtil.addVariableMap(Constants.ROUTE_CONDITION_KEY, exectMap.get(Constants.ROUTE_CONDITION_KEY_SYS));
        }
        wfTaskflowService.selectByIdForUpdate(taskflow.getId());
        //注意：由于WfVariableUtil底层是ThreadLoacl，所以commitTask调用activiti的底层有很多事情处理，比如
        //TaskCreateListener类的所有功能，所以对WfVariableUtil的操作慎重
//		activitiBaseServer.commitTask(taskId, new HashMap<String, Object>());
        WfVariableUtil.removeVariableMap(Constants.ROUTE_CONDITION_KEY);

        commitProcess(taskId, taskflow.getProcessCode(), exectMap, taskNode);

        if (StrUtil.isNotBlank(taskTransferData.getPassingRouteFlag())) {
            exectMap.remove(Constants.ROUTE_CONDITION_KEY_SYS);
        }
        // 4.其他操作(自动节点、子流程节点的自动流转)
        try {
            this.autoExec(endEventInvock, taskflow, taskNode, exectMap, taskTransferData, sysCommHead);
        } catch (Exception e) {
            log.error("| - ProcessCoreServiceImpl>>>>>自动节点调用失败：", e);

            throw new ServiceException(e.getMessage());
        }
        // 6.任务记录和实例记录的更新操作（只有放最后才能正确返回流程实例的状态）
        this.updateTaskflow(taskflow, taskTransferData, exectMap);
        // 更新流程实例
        updateInstance(returnData, userId, taskflow);
        // 5.流程结束的判断

        if (taskTransferData.isDoFinishSign() && toBeFinishedSign != null && toBeFinishedSign.size() > 0) {
            finishSignTaskAuto(taskId, userId, result, toBeFinishedSign, exectMap);
        }
        boolean isend = processUtil.doAfterEndProcess(taskflow.getProcessCode(), taskflow.getProcessId(), null);
        returnData.setEnd(isend);

        return returnData;
    }

    /**
     * 更新流程实例
     *
     * @param returnData
     * @param userId
     * @param taskflow
     */
    private void updateInstance(SubmitReturnData returnData, String userId, WfTaskflow taskflow) {
        WfInstance instance = wfInstanceService.selectByProcessId(taskflow.getProcessId());
        if (instance != null) {
            String ids = instance.getJoinUserIds();
            if (ids == null) {
                ids = "";
            }
            if (!ids.contains(userId)) {
                ids += SymbolConstants.COMMA + userId;
                // 打回操作情况流程实例的处理人员清空
                if (ids.startsWith(SymbolConstants.COMMA)) {
                    ids = ids.substring(1);
                }
                instance.setJoinUserIds(ids);
                instance.setUpdateTime(LocalDateTime.now());
                wfInstanceService.updateById(instance);
            }
            returnData.setProcessState(instance.getEndType());
        }
        returnData.setProcessInstId(taskflow.getProcessId());
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Map<String, Object> getNextTaskVar(WorkFlowTaskTransferData taskTransferData, WfTaskflow taskflow) {
        String userId = taskTransferData.getUserId();
        String taskId = taskTransferData.getTaskId();
        Map<String, Object> variables = taskTransferData.getBussinessMap();
        if (StrUtil.isBlank(userId)) {
            throw new ServiceException(ErrorCode.WF000024);
        }
        if (StrUtil.isBlank(taskId)) {
            throw new ServiceException(ErrorCode.WF000025);
        }

        if (variables == null) {
            variables = new HashMap<>();
        }
        variables.put(Constants.BUSSINESS_KEY_COMPARE, null);
        if (taskflow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }

        String varMapStr = taskflow.getBussinessMap();
        Map<String, Object> exectMap = JSONObject.parseObject(varMapStr);

        wfCommService.setTaskUargent(exectMap, variables);
        if (!variables.containsKey(Constants.INPUT_TRANCHANNEL_KEY)) {
//			String tranChannel = taskTransferData.getSysCommHead().getTranChannel();
//			variables.put(Constants.INPUT_TRANCHANNEL_KEY, tranChannel);
        }

        if (exectMap != null) {
            // 重置特殊的相关参数
            resetSpecifyParam(exectMap);

            exectMap.putAll(variables);
        } else {
            exectMap = variables;
        }
        exectMap.remove(Constants.ROUTE_CONDITION_KEY);

        JSONObject jsonObject = JSONObject.parseObject(taskflow.getTaskActorsData());
        if (jsonObject != null) {
            Map<String, Object> userData = (Map<String, Object>) jsonObject.get(userId);
            if (userData != null) {
                exectMap.put(Constants.BEFORUSERDATA, userData);
            }
        }
        //如果flowParam类型变为LinkedList；在这一步将LinkedList类型再次转为JSONArray
        if (exectMap.get(Constants.ROUTE_SELECT_KEY) != null && LinkedList.class.isInstance(exectMap.get(Constants.ROUTE_SELECT_KEY))) {
            LinkedList values = (LinkedList) exectMap.get(Constants.ROUTE_SELECT_KEY);
            if (values.size() > 0) {
                JSONArray jsonArray = new JSONArray();
                for (Object obj : values) {
                    jsonArray.add(obj);
                }
                exectMap.put(Constants.ROUTE_SELECT_KEY, jsonArray);
            }
        }
        if (exectMap.get(Constants.ROUTE_SELECT_KEY) != null) {
            log.debug("| - ProcessCoreServiceImpl>>>>>flowParam集合类型={} ", exectMap.get(Constants.ROUTE_SELECT_KEY).getClass().getName());
        }
        // 任务状态检查
        if (Constants.TASK_STATE_HUNG.equals(taskflow.getWorkState())) {
            throw new ServiceException("任务已经挂起，必须取消挂起，才能做相应操作！");
        }
        // 任务状态检查
        if (Constants.TASK_STATE_END.equals(taskflow.getWorkState())) {
            throw new ServiceException("任务已经完成！");
        }
        // 任务状态检查
        if (Constants.TASK_STATE_CANCEL.equals(taskflow.getWorkState())) {
            throw new ServiceException("任务已经取消！");
        }

        //update by  xueshunwen 20200629 start   将审批结果和审批意见放入业务变量中
        exectMap.put("approve", taskTransferData.getResult());
        exectMap.put("approveOpinion", taskTransferData.getResultRemark());
        //update by  xueshunwen 20200629 end    将审批结果和审批意见放入业务变量中

        // 将前一节点需要的信息存入变量
        exectMap.put(Constants.BEFOR_TASKID, taskId);
        exectMap.put(Constants.BEFOR_USERID, userId);
        WfVariableUtil.setVariableMap(exectMap);

        return exectMap;
    }

    /**
     * 更新任务的状态代办为已办，审批结果、审批意见、参数对比、业务参数、任务处理时间、更新时间（审批时间），任务结束时间
     *
     * @param taskflow
     * @param taskTransferData
     * @param exectMap
     * @throws Exception
     */
    private void updateTaskflow(WfTaskflow taskflow, WorkFlowTaskTransferData taskTransferData,
                                Map<String, Object> exectMap) {
        taskflow.setDoTaskActor(taskTransferData.getUserId());
        taskflow.setWorkState(Constants.TASK_STATE_END);
        taskflow.setDoResult(taskTransferData.getResult());
        taskflow.setDoRemark(taskTransferData.getResultRemark());
        wfCommService.processAccessories(taskTransferData, taskflow);
        if (exectMap == null) {
            throw new ServiceException("exectMap is null");
        }
        // 系统渠道号登记处理
        taskflow.setTranChannel((String) exectMap.get(Constants.INPUT_TRANCHANNEL_KEY));

        String compareResult = processUtil.compareBusiMap(taskflow.getProcessCode(), taskflow.getBussinessMap(),
                exectMap);
        if (!StrUtil.isEmpty(compareResult)) {
            JSONObject obj = null;
            if (exectMap != null && exectMap.size() > 0) {
                obj = BpmStringUtil.mapToJson(exectMap);
            } else {
                obj = new JSONObject();
            }
            obj.put(Constants.BUSSINESS_KEY_COMPARE, compareResult);
            taskflow.setBussinessMap(obj.toJSONString());
        } else {
            if (exectMap != null && exectMap.size() > 0) {
                taskflow.setBussinessMap(BpmStringUtil.mapToJson(exectMap).toJSONString());
            }
        }
        //更新扩展属性
        if (taskTransferData.getBussinessSave() != null) {
            Map<String, Object> queryCondition = taskTransferData.getBussinessSave();
            if (queryCondition.containsKey(Constants.QUERY_INDEX_1)) {
                taskflow.setQueryIndexa((String) queryCondition.get(Constants.QUERY_INDEX_1));
            }
            if (queryCondition.containsKey(Constants.QUERY_INDEX_2)) {
                taskflow.setQueryIndexb((String) queryCondition.get(Constants.QUERY_INDEX_2));
            }
            if (queryCondition.containsKey(Constants.QUERY_INDEX_3)) {
                taskflow.setQueryIndexc((String) queryCondition.get(Constants.QUERY_INDEX_3));
            }
            if (queryCondition.containsKey(Constants.QUERY_INDEX_4)) {
                taskflow.setQueryIndexd((String) queryCondition.get(Constants.QUERY_INDEX_4));
            }

        }
        //更新人员信息
        Map<String, Object> userIdMap = taskTransferData.getUserIdMap();
        if (userIdMap != null && !userIdMap.isEmpty()) {
            JSONObject taskActorsData = JSONObject.parseObject(taskflow.getTaskActorsData());
            JSONObject userdate = taskActorsData.getJSONObject(taskTransferData.getUserId());
            for (Map.Entry<String, Object> entry : userIdMap.entrySet()) {
                userdate.put(entry.getKey(), entry.getValue());
            }
            taskflow.setTaskActorsData(taskActorsData.toJSONString());
        }
        /*============根据querya，b，c，d查询的功能优化end===================*/
        long duedata = 0L;
        try {
            duedata = System.currentTimeMillis() - DateUtil.convertObjToTimestamp(taskflow.getCreateDate()).getTime();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        taskflow.setDuedata(duedata);
        taskflow.setEndDate(LocalDateTime.now());
        taskflow.setUpdateDate(LocalDateTime.now());
        if ((taskflow.getWorkType() == null || !taskflow.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_ALLOT_TASK))) && !taskflow.getTaskType().equals(Constants.NODE_TYPE_ATTO) && !taskflow.getTaskType().equals(Constants.NODETYPE_SUBPROC)) {
            UserOutputData userData = getUserData(taskflow, taskTransferData.getUserId());
            taskflow.setDoTaskActorName(userData.getUserName());
            taskflow.setDoTaskActorAgencyId(userData.getUserBank());
            taskflow.setDoTaskActorAgencyName(userData.getUserBankName());
        }

        if (StrUtil.isEmpty(taskflow.getTaskActors()) && StrUtil.isNotEmpty(taskflow.getDoTaskActor())) {
            if (userIdMap == null) {
                userIdMap = new HashMap<String, Object>();
            }
            userIdMap.put(Constants.USERID_KEY, taskflow.getDoTaskActor());
            Map<String, Object> users = this.queryUserList(userIdMap);
            taskflow.setTaskActorsData(BpmStringUtil.mapToJson(users).toJSONString());
            taskflow.setDoTaskActorName((String) users.get("userName"));
            taskflow.setDoTaskActorAgencyId((String) users.get("userBank"));
            taskflow.setDoTaskActorAgencyName((String) users.get("userBankName"));
        }
        wfTaskflowService.updateById(taskflow);
        WfBusinessData businessData = new WfBusinessData(taskflow);
        // 自动节点等待，工作流参数没提交，这里需判断是否有工作流参数，没有则新增 Cmz
        WfBusinessData wfBusinessData = wfBusinessDataService.selectById(businessData.getId());
        if (cn.hutool.core.util.ObjectUtil.isNotNull(wfBusinessData)) {
            wfBusinessDataService.updateById(businessData);
        } else {
            wfBusinessDataService.insertWfBusinessData(businessData);
        }
    }

    /**
     * 重置特殊的相关参数
     *
     * @param exectMap
     */
    private void resetSpecifyParam(Map<String, Object> exectMap) {
        if (exectMap.get(Constants.SPECIFYEXTOPERS) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYEXTOPERS).toString()))) {
            exectMap.put(Constants.SPECIFYEXTOPERS, "");
        }
        if (exectMap.get(Constants.SPECIFYNEXTNODE) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYNEXTNODE).toString()))) {
            exectMap.put(Constants.SPECIFYNEXTNODE, "");
        }

        if (exectMap.get(Constants.SPECIFYNEXTFLOW) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYNEXTFLOW).toString()))) {
            exectMap.put(Constants.SPECIFYNEXTFLOW, "");
        }
        if (exectMap.get(Constants.SPECIFYNEXTTIME) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYNEXTTIME).toString()))) {
            exectMap.put(Constants.SPECIFYNEXTTIME, "");
        }
        if (exectMap.get(Constants.SPECIFYNEXTJOBBANK) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYNEXTJOBBANK).toString()))) {
            exectMap.put(Constants.SPECIFYNEXTJOBBANK, "");
        }
        if (exectMap.get(Constants.SPECIFYADDMESSAGE) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYADDMESSAGE).toString()))) {
            exectMap.put(Constants.SPECIFYADDMESSAGE, "");
        }
        if (exectMap.get(Constants.SPECIFYADDUSERIDS) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYADDUSERIDS).toString()))) {
            exectMap.put(Constants.SPECIFYADDUSERIDS, "");
        }
        if (exectMap.get(Constants.SPECIFYNEXTBANKPOS) != null
                && (!StrUtil.isEmpty(exectMap.get(Constants.SPECIFYNEXTBANKPOS).toString()))) {
            exectMap.put(Constants.SPECIFYNEXTBANKPOS, "");
        }
    }

    /**
     * 增加流程实例信息
     *
     * @param processInstance
     * @param userId
     * @param userMap
     * @param procName
     * @param instDesc        流程实例描述（启动是传入）
     * @param processDesc     流程描述
     * @param varMap
     * @return
     */
    @SuppressWarnings("rawtypes")
    private WfInstance addWfInstance(ProcessInstance processInstance, String userId, Map userMap, String procName,
                                     String instDesc, String processDesc, Map<String, Object> varMap) {

        log.info("| - ProcessCoreServiceImpl>>>>>新增流程实例ID：{}", processInstance.getId());
        log.info("| - ProcessCoreServiceImpl>>>>>新增流程实例发起人信息：{}", userId);
        log.info("| - ProcessCoreServiceImpl>>>>>新增流程实例名称：{}", procName);

        WfInstance instance = new WfInstance();
        instance.setProcessId(processInstance.getId());
        if (!StrUtil.isEmpty(processInstance.getProcessDefinitionId())) {
            instance.setProcessCode(processInstance.getProcessDefinitionId());
        }
        instance.setSystemCode((String) varMap.get(Constants.SYSTEM_CODE_KEY));
        instance.setProccessName(procName);
        instance.setProcessDesc(processDesc);// 流程描述
        instance.setStartUserId(userId);
        instance.setStartUserMap(JSON.toJSONString(userMap));
        instance.setStartUserName((String) userMap.get("userName"));
        instance.setOrgId((String) userMap.get("userBank"));
        instance.setOrgName((String) userMap.get("userBankName"));
        instance.setJoinUserIds(userId);
        instance.setWorkState(Constants.WORK_STATE_RUNNING);
        instance.setEndType(Constants.COMPLETE_TYPE_ACTIVITY);
        if (StrUtil.isBlank(instDesc)) {
            instDesc = procName;
        }
        instance.setProcessInstDesc(instDesc);// 实例描述
        LocalDateTime createDateTime = DateUtil.covertObjToLdt(varMap.get(Constants.PROCESS_CREATE_TIME));
        instance.setStartTime(createDateTime);
        instance.setCreateTime(createDateTime);
        //计算流程实例超时时间

        instance.setParentTaskId((String) varMap.get(Constants.PROCESS_TASK_ID));
        wfInstanceService.insertWfInstance(instance);
        return instance;
    }


    private List<WfTaskflow> getToBeFinishedSignTask(String taskId) {
        Task theTask = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
        if (theTask == null) {
            return new ArrayList<WfTaskflow>();
        }
        FlowElement flowElement = null;
        try {
            flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(taskId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String taskType = processUtil.getTaskProp(theTask.getProcessDefinitionId(), flowElement, Constants.NODETYPE);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
        log.debug("| - ProcessCoreServiceImpl>>>>>getToBeFinishedSignTask taskType:{}", taskType);
        if (StrUtil.isEmpty(taskType) || (!Constants.NODE_TYPE_SIGN.equals(taskType))) {
            return new ArrayList<WfTaskflow>();
        }

        WfTaskflow selectCond = new WfTaskflow();
        selectCond.setTaskDefId(theTask.getTaskDefinitionKey());
        selectCond.setProcessId(theTask.getProcessInstanceId());
        selectCond.setWorkState(Constants.TASK_STATE_RUNING);
        log.debug("| - ProcessCoreServiceImpl>>>>>getToBeFinishedSignTask selectCond:{} theTaskId:{}", selectCond, taskId);
        List<WfTaskflow> signTaskList = wfTaskflowService.selectByCondition(selectCond);
        log.debug("| - ProcessCoreServiceImpl>>>>>getToBeFinishedSignTask signTaskList:{}", signTaskList);
        return signTaskList;
    }


    @SuppressWarnings("unchecked")
    private void finishSignTaskAuto(String taskId, String userId, String doResult, List<WfTaskflow> signTaskList, Map<String, Object> varMap) {
        // 如果是将会签节点驳回
        Map<String, Object> oldVarMap = new HashMap<String, Object>();
        for (WfTaskflow temp : signTaskList) {

            if (temp.getTaskId().equals(taskId)) {
                oldVarMap.putAll((Map<String, Object>) JSONObject.parse(temp.getBussinessMap()));
                continue;
            }
            if (temp.getWorkState().equals(Constants.TASK_STATE_END)) {
                continue;
            }
            // 剩下的会签任务都直接关掉
            oldVarMap.putAll(varMap);
            oldVarMap.put(Constants.SINGLASTTASKRESULT, Constants.RESULT_SYSUSE);
            oldVarMap.put(Constants.BEFOR_TASKID, temp.getTaskId());
            WfVariableUtil.addAllVariableMap(oldVarMap);

            Task theTask = processEngine.getTaskService().createTaskQuery().taskId(temp.getTaskId()).singleResult();
            if (theTask != null) {
                try {
                    activitiBaseServer.commitTask(temp.getTaskId(), new HashMap<>());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            LocalDateTime now = LocalDateTime.now();
            temp.setWorkState(Constants.TASK_STATE_END);
            temp.setDoResult(doResult);
            temp.setDoTaskActor(userId);
            temp.setEndDate(now);
            temp.setUpdateDate(now);
            temp.setUpdateRemark("会签自动结束");
            wfTaskflowService.updateById(temp);
            WfBusinessData businessData = new WfBusinessData(temp);
            wfBusinessDataService.updateById(businessData);
        }
    }

    /**
     * 转办流程
     *
     * @param taskId   当前任务节点ID
     * @param userCode 被转办人Code
     */
    @Transactional
    public void transferAssignee(String taskId, String userCode, String userId, String doResult, String doRemark) {
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>转办任务[taskId:{}]", taskId);
            log.info("| - ProcessCoreServiceImpl>>>>>转办给用户[agentUserId:{}]", userCode);
            log.info("| - ProcessCoreServiceImpl>>>>>转办人[userId:{}]", userId);
            log.info("| - ProcessCoreServiceImpl>>>>>处理结果[doResult:{}]", doResult);
        }

        this.pritransferAssignee(taskId, userCode, userId, doResult, 1, doRemark, null, null);
    }


    /**
     * 转办-拒绝
     *
     * @param taskId     目标任务
     * @param distUserId 目标人
     * @param operId     处理人
     * @param doResult
     * @param flag       1:转办，2、拒绝 3、分配,4、回收分配
     * @throws Exception
     */
    @Transactional
    public void pritransferAssignee(String taskId, String distUserId, String sourceUserId, String doResult, int flag, String doRemark, String operId, Map<String, Object> usermap) {
        String msg = "";
        if (flag == Constants.TRANSFER_TYPE_TRANSF) {
            msg = "转办";
        } else if (flag == Constants.TRANSFER_TYPE_REFUSE_TRANSF) {
            msg = "转办拒绝";
        } else if (flag == Constants.TRANSFER_TYPE_ALLOT) {
            msg = "分配";
        } else if (flag == Constants.TRANSFER_TYPE_RECOVER) {
            msg = "任务回收转发";
        }
        //将合法性判定移到前面
        WfTaskflow taskflow = selectByTaskId(taskId);
        if (taskflow.getWorkState().equals(Constants.TASK_STATE_END)) {
            throw new ServiceException("不能" + msg + "已经处理的任务");
        }
        if (taskflow.getWorkState().equals(Constants.TASK_STATE_HUNG)) {
            throw new ServiceException("任务已经挂起,必须取消挂起,才能做相应操作!");
        }
        //任务分配时，当处理人不为空时，设置新增的记录为原来的处理人
        if (Objects.nonNull(taskflow.getDoTaskActor()) && flag == Constants.TRANSFER_TYPE_ALLOT) {
            sourceUserId = taskflow.getDoTaskActor();
        }

        String olduid = taskflow.getTaskActors();
        if ((flag != Constants.TRANSFER_TYPE_RECOVER && flag != Constants.TRANSFER_TYPE_ALLOT) && olduid.contains(distUserId)) {
            throw new ServiceException("不能" + msg + "给该任务当前可处理人员");
        }

        String olduName = "";
        String disName = "";
        if (Objects.isNull(usermap)) {
            usermap = new HashMap<String, Object>();
            JSONArray userAskListJson = JSONArray.parseArray(" [ { \"orderUserNo\":\"" + distUserId + "\"},{ \"orderUserNo\":\"" + sourceUserId + "\"}]");
            List<UserOutputData> list = null;
            try {
                list = queryRelationTellerFactory.getInstance().queryUserList(userAskListJson);
            } catch (Exception e) {
                log.error("| - ProcessCoreServiceImpl>>>>>调用选人服务返回人员失败", e);
                throw new ServiceException(e.getMessage());
            }
            if (list == null) {
                throw new ServiceException("被转办人员不存在,请确认!");
            }
            usermap = new HashMap<String, Object>();
            //转办给目标人的用户姓名，之前是转办人的姓名
            for (UserOutputData u : list) {
                if (StrUtil.equals(sourceUserId, u.getUserId())) {
                    olduName = u.getUserName();
                }
                if (StrUtil.equals(distUserId, u.getUserId())) {
                    disName = u.getUserName();
                }
                usermap.put(u.getUserId(), u);
            }
        } else {
            JSONObject json = BpmStringUtil.mapToJson(usermap);

            if (json.containsKey(sourceUserId)) {
                olduName = json.getJSONObject(sourceUserId).getString("userName");
            }
            if (json.containsKey(distUserId)) {
                disName = json.getJSONObject(distUserId).getString("userName");
            }

        }

        log.info("| - ProcessCoreServiceImpl>>>>>处理人信息  usermap:{}", JSON.toJSON(usermap));
        //如果会签节点，会调用TaskAssignmentListener自动生成一条，因此这里不再新增
        LocalDateTime now = LocalDateTime.now();
        if (!taskflow.getTaskType().equals(Constants.NODE_TYPE_SIGN)) {
            processUtil.changeCandidateUser(taskId, sourceUserId, distUserId, flag);
            taskflow.setWorkAllot(olduid);
            taskflow.setTaskActors(distUserId);

            if (flag == Constants.TRANSFER_TYPE_ALLOT) {
                taskflow.setWorkType(Integer.valueOf(String.valueOf(Constants.WORK_TYPE_CHECK_OUT)));
            } else {
                taskflow.setWorkType(1);
            }

            WfTaskflow taskflowold = ObjectUtil.copyBeanProp(taskflow, WfTaskflow.class);

            if (flag != Constants.TRANSFER_TYPE_ALLOT) {
                Map<String, Object> map = new HashMap<String, Object>();
                if (flag == Constants.TRANSFER_TYPE_RECOVER) {
                    taskflow.setWorkType(Integer.valueOf(String.valueOf(Constants.WORK_TYPE_RECOVERY)));
                } else {
                    taskflow.setWorkType(Integer.valueOf(String.valueOf(Constants.WORK_TYPE_TRANSFER_OUT)));
                }

                if (distUserId.contains(SymbolConstants.COMMA)) {
                    for (String str : distUserId.split(SymbolConstants.COMMA)) {
                        map.put(str, usermap.get(str));
                    }
                } else {
                    map.put(distUserId, usermap.get(distUserId));
                    //2019-06-03 modify amt start 批量更换任务处理人时，存入处理人针对任务池任务
                    if (StrUtil.equals(taskflow.getTaskPoolFlag(), Constants.IS_HIST_TASK_POOL)) {
                        taskflow.setDoTaskActor(distUserId);
                    }
                    //2019-06-03 modify amt end  批量更换任务处理人时，存入处理人针对任务池任务
                    taskflow.setTaskActorsData(BpmStringUtil.mapToJson(map).toJSONString());
                }
            } else {
                taskflow.setWorkType(Integer.valueOf(String.valueOf(Constants.WORK_TYPE_ALLOT_TASK)));
                taskflow.setDoTaskActor(distUserId);
                taskflow.setTaskActorsData(BpmStringUtil.mapToJson(usermap).toJSONString());
                taskflow.setDoResult(doResult);
                taskflow.setDoRemark(doRemark);
                // 分配的任务标记为强制领用C0
                taskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKOUT);
                LocalDateTime checkOutTime = LocalDateTime.now();
                taskflow.setCheckOutTime(checkOutTime);
                taskflow.setDoTaskActorName(disName);
                taskflow.setTaskPoolFlag(Constants.IS_HIST_TASK_POOL);
            }
            wfTaskflowService.updateById(taskflow);

            WfBusinessData businessData = new WfBusinessData(taskflow);
            wfBusinessDataService.updateById(businessData);
            taskflowold.setId(null);
            taskflowold.setTaskId("000001");
            taskflowold.setTaskActors(olduid);
            taskflowold.setDoTaskActor(sourceUserId);
            taskflowold.setDoTaskActorName(olduName);
            Date date = DateUtil.convertObjToUtilDate(taskflowold.getCreateDate());
            //为了排序,将创建时间减1秒
            taskflowold.setCreateDate(DateUtil.convertObjToLdt(DateUtil.addTimeoutDateWithMillisecond(date, -1000)));
            taskflowold.setUpdateDate(now);
            taskflowold.setEndDate(now);
            taskflowold.setDoResult(doResult);
            if (doRemark != null && doRemark.trim().length() > 0) {
                taskflowold.setDoRemark(doRemark);
            } else {
                taskflowold.setDoRemark(msg + "任务" + taskId);
            }
            taskflowold.setWorkState(Constants.TASK_STATE_END);
            taskflowold.setWorkAllot(null);
            wfTaskflowService.insertWfTaskflow(taskflowold);
            WfBusinessData businessData1 = new WfBusinessData(taskflowold);
            wfBusinessDataService.insertWfBusinessData(businessData1);
        } else {
            if (distUserId.contains(SymbolConstants.COMMA)) {
                throw new ServiceException("会签节点不允许转发给多个人");
            }
            processUtil.deleteCandidateUser(taskId, sourceUserId);
            wfTaskflowService.deleteById(taskflow.getId());
            taskflow.setTaskId("000001");
            taskflow.setTaskActors(olduid);
            taskflow.setDoTaskActor(sourceUserId);
            taskflow.setUpdateDate(now);
            taskflow.setEndDate(now);
            taskflow.setDoResult(doResult);
            taskflow.setDoTaskActorName(olduName);
            taskflow.setWorkState(Constants.TASK_STATE_END);
            taskflow.setWorkAllot(null);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(distUserId, usermap.get(distUserId));
            Map<String, Object> variables = BpmStringUtil.jsonToMap(taskflow.getBussinessMap());
            if (flag != Constants.TRANSFER_TYPE_ALLOT) {
                if (flag == Constants.TRANSFER_TYPE_RECOVER) {
                    variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_RECOVERY);
                } else {
                    variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_TRANSFER_OUT);
                }
                if (doRemark != null && doRemark.trim().length() > 0) {
                    taskflow.setDoRemark(doRemark);
                } else {
                    taskflow.setDoRemark(msg + "任务" + taskId);
                }
            } else {
                taskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKOUT);
                taskflow.setCheckOutTime(now);
                taskflow.setTaskPoolFlag(Constants.IS_HIST_TASK_POOL);
                taskflow.setDoResult(msg + "任务" + taskId);
                taskflow.setTaskActorsData(BpmStringUtil.mapToJson(usermap).toJSONString());
                taskflow.setDoTaskActor(sourceUserId);
                variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_ALLOT_TASK);
            }
            wfTaskflowService.insertWfTaskflow(taskflow);
            WfBusinessData businessData = new WfBusinessData(taskflow);
            wfBusinessDataService.insertWfBusinessData(businessData);
            WfVariableUtil.addAllVariableMap(variables);
            WfVariableUtil.addVariableMap("list", map);
        }
        activitiBaseServer.assignerTask(taskId, distUserId);
        //删掉会签后续处理，因为已经从createListener里面处理过了
    }

    @Transactional
    public void transferRefuse(String taskId, String userId, String doResult, String doRemark) {
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>拒绝转办任务[taskId:{}]", taskId);
        }
        WfTaskflow taskflow = selectByTaskId(taskId);
        if (BpmConstants.WORK_STATE_1.equals(taskflow.getWorkState())) {
            throw new ServiceException("不能转办已经处理的任务");
        }
        if (taskflow.getWorkType() == null
                || !taskflow.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_TRANSFER_OUT))) {
            throw new ServiceException("不能拒绝不是转办的任务");
        }

        String uid = taskflow.getTaskActors();
        String oldid = taskflow.getWorkAllot();
        this.pritransferAssignee(taskId, oldid, uid, doResult, 2, doRemark, null, null);
    }

    /**
     * @param taskId      当前任务ID
     * @param variableMap 流程变量
     * @param taskNode    流程转向执行任务节点ID<br>
     *                    此参数为空，默认为提交操作
     * @throws Exception
     */
    private void commitProcess(String taskId, String processCode, Map<String, Object> variableMap, FlowElement taskNode) {
        FlowNode flowNode = (FlowNode) taskNode;
        List<SequenceFlow> outList = flowNode.getOutgoingFlows();
        if (outList.isEmpty()) {
            throw new ServiceException("流程定义存在问题，请确认！！！");
        } else {
            boolean isSysSetRouteKey = false;
            if (variableMap.containsKey(Constants.ROUTE_CONDITION_KEY_SYS)) {
//				variableMap.put(Constants.ROUTE_CONDITION_KEY, variableMap.get(Constants.ROUTE_CONDITION_KEY));
                WfVariableUtil.addVariableMap(Constants.ROUTE_CONDITION_KEY, variableMap.get(Constants.ROUTE_CONDITION_KEY_SYS));
                isSysSetRouteKey = true;
            } else {
                boolean isRoute = wfCommService.forkRouteSelect(processCode, taskNode, variableMap);
                WfVariableUtil.setVariableMap(variableMap);
                // 提前判断下路由分支是否满足
                if (isRoute) {
                    sequenceFlowCadition.mathcCaditionCheck(variableMap, taskNode);
                }
            }
            activitiBaseServer.commitTask(taskId, new HashMap<>());
            if (isSysSetRouteKey) {
                WfVariableUtil.removeVariableMap(Constants.ROUTE_CONDITION_KEY);
            }
        }


    }

    public void delegateTask(String taskId, String userId) {
        activitiBaseServer.delegateTask(taskId, userId);
    }


    private void endProcessInstance(WorkFlowTaskTransferData taskTransferData, String userId, boolean endParentProcess) {
        // 检查条件
        // String userId = taskTransferData.getUserId();
        String taskId = taskTransferData.getTaskId();
        String processInstId = taskTransferData.getProcessInstId();
        String cancelType = taskTransferData.getCancelType();
        // 获取流程定义Id
        ProcessInstance processInstance = null;
        String errmsg = "当前任务唯一标识输入有误，请确认！";
        if (StrUtil.isBlank(processInstId)) {
            processInstance = activitiBaseServer.findProcessInstanceByTaskId(taskId);
            processInstId = processInstance.getId();
        } else {
            processInstance = activitiBaseServer.findProcessInstance(processInstId);
            errmsg = "当前流程实例唯一标识输入有误，请确认！";
        }
        if (processInstance == null) {
            throw new ServiceException(errmsg);
        }

        log.info("| - ProcessCoreServiceImpl>>>>>processInstance.getProcessDefinitionId():{}", processInstance.getProcessDefinitionId());
        // 找到流程的结束个节点
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processInstance.getProcessDefinitionId());
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();
        FlowElement endElement = null;
        for (FlowElement element : nodeList) {
            if (element instanceof EndEvent) {
                endElement = element;
                //如果节点是异常结束，则服务调用会出现问题
                List<ActivitiListener> lis = endElement.getExecutionListeners();
                if (lis == null || lis.isEmpty()) {
                    break;
                }
            }
        }
        //如果没有找到正常节点只要是结束节点，都调用
        if (endElement == null) {
            for (FlowElement element : nodeList) {
                if (element instanceof EndEvent) {
                    endElement = element;
                    break;
                }
            }
        }
        //理论上不会出现
        if (endElement == null) {
            throw new ServiceException("未找到结束节点！！！");
        }

        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstId).list();
        if (tasks == null || tasks.size() == 0) {
            return;
        }
        String mainProcessId = null;
        WfTaskflow mainTaskflow = null;
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processInstId);
        //说明是子流程，需要查找主流程
        if (!StrUtil.isEmpty(wfInstance.getParentTaskId())) {
            mainTaskflow = selectByTaskId(wfInstance.getParentTaskId());
            mainProcessId = mainTaskflow.getProcessId();
        }

        for (Task theTask : tasks) {
            //如果当前节点是主流程的子流程节点，先结束子流程节点,递归结束主流程节点；
            WfTaskflow childTaskflow = selectByTaskId(theTask.getId());

            if (childTaskflow != null && childTaskflow.getWorkState().equals(Constants.TASK_STATE_RUNING) && childTaskflow.getTaskType().equals(Constants.NODETYPE_SUBPROC)) {
                //根据主流程的taskId,查找子流程节点对应的代办，然后终止子流程，因为这里的task是遍历的，因此不用再通过processId查询所有的主流程的子流程节点
                List<WfTaskflow> list = wfTaskflowService.selectSubproListByProcessInstId(processInstId);
                if (Objects.nonNull(list) && !list.isEmpty()) {
                    //遍历终止子流程
                    for (WfTaskflow w : list) {
                        WorkFlowTaskTransferData taskTransferData1 = new WorkFlowTaskTransferData();
                        taskTransferData1.setProcessInstId(w.getProcessId());
                        taskTransferData1.setTaskId(w.getTaskId());
                        taskTransferData1.setCancelType(cancelType);
                        endProcessInstance(taskTransferData1, userId);
                    }
                    return;
                }
            }
            endEachTaskProcess(theTask, endElement, taskTransferData.getResult(), taskTransferData.getResultRemark(),
                    userId, cancelType, taskTransferData.getDoRemark());
        }

        processUtil.doAfterEndProcess(processInstance.getProcessDefinitionId(), processInstance.getProcessInstanceId(), endElement);

        //如果是子流程递归调用主流程
        if (mainProcessId != null && !endParentProcess) {
            //如果主流程结束，则结束
            tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(mainProcessId).list();
            if (tasks == null || tasks.size() == 0) {
                return;
            }
            WorkFlowTaskTransferData taskTransferData1 = new WorkFlowTaskTransferData();
            taskTransferData1.setProcessInstId(mainProcessId);
            taskTransferData1.setTaskId(mainTaskflow.getTaskId());
            taskTransferData1.setCancelType(cancelType);
            endProcessInstance(taskTransferData1, userId);
        }
    }


    /**
     * 结束流程
     *
     * @param taskTransferData
     * @throws Exception
     */
    @Transactional
    public void endProcessInstance(WorkFlowTaskTransferData taskTransferData, String userId) {
        endProcessInstance(taskTransferData, userId, true);
    }

    private void endEachTaskProcess(Task theTask, FlowElement endElement, String doResult, String resultRemark,
                                    String userId, String cancelType, String doRemark) {

        theTask = GatewayUtil.jumpOutGateway4EndTransaction(cacheDaoSvc, processEngine, theTask);
        if (theTask == null) {
            return;
        }

        Map<String, Object> inputMap = theTask.getProcessVariables();
        if (inputMap == null) {
            inputMap = new HashMap<String, Object>();
        }

        WfTaskflow taskflow = selectByTaskId(theTask.getId());

        if (taskflow != null) {
            if (StrUtil.isBlank(userId)) {
                if (!StrUtil.isEmpty(taskflow.getTaskActors())) {
                    taskflow.setDoTaskActor(taskflow.getTaskActors().split(SymbolConstants.COMMA)[0]);
                }
            } else {
                taskflow.setDoTaskActor(userId);
                JSONArray userAskListJson = JSONArray.parseArray(" [ { \"orderUserNo\":\"" + userId + "\"}]");
                List<UserOutputData> list = null;
                try {
                    list = queryRelationTellerFactory.getInstance().queryUserList(userAskListJson);
                    String doTaskActorName = userId;
                    if (list != null && !list.isEmpty()) {
                        doTaskActorName = list.get(0).getUserName();
                    }
                    taskflow.setDoTaskActorName(doTaskActorName);
                } catch (Exception e) {
                    log.error("| - ProcessCoreServiceImpl>>>>>获取用户信息【{}】失败", userId, e);
                }

            }
            inputMap.putAll(BpmStringUtil.jsonToMap(taskflow.getBussinessMap()));
            List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                    .selectModelExtendsPropByOwnId(theTask.getProcessDefinitionId().split(SymbolConstants.COLON)[0]);
            WfModelPropExtends cancelEvent = modelPropService.getSpcefiyModelProp(extendsPropList,
                    ModelProperties.cancelEvent.getName());

            if (Objects.nonNull(cancelType)) {
                inputMap.put(Constants.COMPLETE_TYPE_KEY, cancelType);
            } else {
                if (cancelEvent != null) {
                    // 用于表示流程是终止取消了，用户后续收尾处理
                    inputMap.put(Constants.COMPLETE_TYPE_KEY, Constants.COMPLETE_TYPE_CANCLE);
                } else {
                    if (StrUtil.isEmpty(cancelType)) {
                        cancelType = Constants.COMPLETE_TYPE_ERROR;
                    }

                }
            }
            inputMap.put(Constants.MANUAL_END_PROCESS, Constants.COMPLETE_TYPE_CANCLE);

            inputMap.put(Constants.BEFOR_TASKID, theTask.getId());
            log.info("| - ProcessCoreServiceImpl>>>>>ready to setVariableMap in endEachTaskProcess");
            WfVariableUtil.setVariableMap(inputMap);
        }

        theTask = processEngine.getTaskService().createTaskQuery().taskId(theTask.getId()).singleResult();
        if (theTask != null) {
            // 跳转至流程的结束节点
            activitiBaseServer.findCommandExecutor()
                    .execute(new JumpCommand(theTask.getId(), endElement.getId(), inputMap, ""));
        }

        if (taskflow != null) {
            // 更新流转记录表
            taskflow.setWorkState(Constants.TASK_STATE_END);
            taskflow.setDoResult(doResult);
            taskflow.setDoRemark(resultRemark);

            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(doRemark)) {
                taskflow.setDoRemark(resultRemark);
            }
            LocalDateTime now = LocalDateTime.now();
            taskflow.setUpdateDate(now);
            taskflow.setEndDate(now);
            taskflow.setDoTaskActor(userId);
            taskflow.setUpdateRemark("任务取消,流程结束");
            wfTaskflowService.updateById(taskflow);
        }
    }

    @SuppressWarnings("unlikely-arg-type")
    public SubmitReturnData findNextNodeInfo(WorkFlowTaskTransferData taskTransferData) {
        // 必输项检查
        WfVariableUtil.setsysCommHead(taskTransferData.getSysCommHead());

        // 获取当前节点信息
        String taskId = taskTransferData.getTaskId();
        FlowElement flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(taskId);
        WfTaskflow taskflow = selectByTaskId(taskId);
        String varMapStr = taskflow.getBussinessMap();
        WfVariableUtil.setsysCommHead(taskTransferData.getSysCommHead());
        Map<String, Object> variableMap = JSONObject.parseObject(varMapStr);
        if (variableMap == null) {
            variableMap = new HashMap<>();
        }
        //上节处理人应替换为当前处理人员
        try {
            Map<String, Object> taskActorsData = JSONObject.parseObject(taskflow.getTaskActorsData());
            Object curTaskActor = taskActorsData.get(taskTransferData.getUserId());
            variableMap.put(Constants.BEFORUSERDATA, curTaskActor);
            variableMap.put(Constants.BEFOR_USERID, taskTransferData.getUserId());
        } catch (Exception e) {
            throw new ServiceException(ErrorCode.WF000030);
        }


        variableMap.putAll(taskTransferData.getBussinessMap());
        variableMap.remove(Constants.ROUTE_CONDITION_KEY);
        // 把flowParam先清下
        variableMap.remove(Constants.ROUTE_SELECT_KEY);
        // 获取节点的配置项信息
        boolean isSelectPerson = Boolean
                .parseBoolean(processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, ModelProperties.checkPer.getName()));//flowElement.getAttributeValue(Constants.XMLNAMESPACE, ModelProperties.checkPer.getName()));
        boolean isPrompt = Boolean
                .parseBoolean(processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, ModelProperties.confirInfo.getName()));//flowElement.getAttributeValue(Constants.XMLNAMESPACE, ModelProperties.confirInfo.getName()));
        boolean isOutSelect = Boolean
                .parseBoolean(processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, ModelProperties.personPro.getName()));//flowElement.getAttributeValue(Constants.XMLNAMESPACE, ModelProperties.personPro.getName()));

        SubmitReturnData result = findOutFlowNodes(taskflow.getProcessCode(), flowElement, variableMap, isSelectPerson, isPrompt, isOutSelect);

        if (StrUtil.isEmpty(taskflow.getSuperTaskId()) || StrUtil.equals(taskflow.getSuperTaskId(), BpmConstants.START_TASK_ID)) {
            return result;
        }
        String nodeId = processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, Constants.NODEPROP_REFID);

        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                .selectModelExtendsPropByOwnId(nodeId);
        WfModelPropExtends callBackJump = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.callBackJump.getName());

        if (callBackJump != null && (taskflow.getWorkType().equals(Constants.WORK_TYPE_REFULSE_TASK) || taskflow.getWorkType().equals(Constants.WORK_TYPE_RECOVER_TASK))) {
            //先这么处理，回头根据taskflow里面的字段决定是否进行查询
            WfTaskflow sourceTaskflow = selectByTaskId(taskflow.getSuperTaskId());
            if (sourceTaskflow == null) {
                return result;
            }
            if (StrUtil.equals(sourceTaskflow.getDoResult(), BpmConstants.DO_RESULT_2) || StrUtil.equals(sourceTaskflow.getDoResult(), BpmConstants.DO_RESULT_5)
                    || StrUtil.equals(sourceTaskflow.getDoResult(), BpmConstants.DO_RESULT_7)) {
                if (contansOutFlowId(sourceTaskflow.getProcessCode(), sourceTaskflow.getTaskDefId(), result)) {
                    return result;
                }
                addHisNodeInfo(result, sourceTaskflow);
            }
        }

        return result;
    }

    /**
     * 把原有的节点信息加入到findnextNodeInfo的分支中去
     *
     * @param result
     * @param sourceTaskflow
     */
    private void addHisNodeInfo(SubmitReturnData result, WfTaskflow sourceTaskflow) {
        SubmitReturnData node = new SubmitReturnData();
        node.setType("1");
        node.setNodeName(sourceTaskflow.getTaskName());
        node.setNodeType(sourceTaskflow.getTaskType());
        node.setOutFlowId(sourceTaskflow.getTaskDefId());
        node.setNodeId(sourceTaskflow.getTaskDefId());
        node.setNodeRefId(sourceTaskflow.getTaskCode());
//		node.setNextOutData(nextData);
        List<UserOutputData> selectUserList = new ArrayList<UserOutputData>();
        Map<String, Object> users = new HashMap<String, Object>();
        if (sourceTaskflow.getTaskType().equals(Constants.NODE_TYPE_SIGN)) {
            WfTaskflow wfTaskflow = new WfTaskflow();
            wfTaskflow.setTaskDefId(sourceTaskflow.getTaskDefId());
            wfTaskflow.setSuperTaskId(sourceTaskflow.getSuperTaskId());
            List<WfTaskflow> list = wfTaskflowService.selectByCondition(wfTaskflow);
            List<WfBusinessData> businessList = workFlowSelectMapper.selectTaskFlowBusinessMap(list);
            for (WfBusinessData w : businessList) {
                String taskdata = w.getTaskActorsData();
                Map<String, Object> m = BpmStringUtil.jsonToMap(taskdata);
                users.putAll(m);
            }
        } else {
            String taskdata = sourceTaskflow.getTaskActorsData();
            users = BpmStringUtil.jsonToMap(taskdata);
        }
        Iterator<Map.Entry<String, Object>> it = users.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            JSONObject json = JSONObject.parseObject(entry.getValue().toString());
            selectUserList.add(new UserOutputData(json));
        }
        node.setSelectUserList(selectUserList);

        result.addOutData(node);
    }

    /**
     * 判断是否已经包含改路径了
     * <pre>
     * 遍历流程图，如果流程图里面存在目标节点，则用目标节点的线id去匹配返回结果，如果返回结果里有线id，则说明返回结果已经包含该节点，不需要再重复返回
     * </pre>
     *
     * @param outFlowId 目标节点id
     * @param result    返回结果
     * @return
     * @throws Exception
     */
    private boolean contansOutFlowId(String processDefCode, String outFlowId, SubmitReturnData result) {
        //outFlowId线的id，匹配点的id
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processDefCode);
        Collection<FlowElement> nodeList1 = bpmnModel.getMainProcess().getFlowElements();
        StringBuilder stringBuilder = new StringBuilder();
        //获取当前的元素信息
        for (FlowElement element : nodeList1) {
            if (element instanceof FlowNode) {
                FlowNode node = (FlowNode) element;
                if (node.getIncomingFlows() != null) {
                    List<SequenceFlow> incoms = node.getIncomingFlows();
                    for (SequenceFlow s : incoms) {
                        if (s.getTargetRef().equals(outFlowId)) {
                            stringBuilder.append(s.getId()).append(Constants.COMMA);
                        }
                    }
                    if (stringBuilder.length() > 0) {
                        break;
                    }
                }
            }

        }
        if (StrUtil.isEmpty(stringBuilder) || result == null) {
            return false;
        }
        return recursiveMatchOutFlowId(stringBuilder.toString(), result);
    }

    /**
     * 递归匹配路由节点
     *
     * @param targetIdMore
     * @param result
     * @return
     */
    private boolean recursiveMatchOutFlowId(String targetIdMore, SubmitReturnData result) {
        boolean haveNodeFlag = false;
        String[] targetArray = targetIdMore.split(SymbolConstants.COMMA);
        String targetId = null;
        for (int i = 0; i < targetArray.length; i++) {
            targetId = targetArray[i];
            if (targetId != null && targetId.length() > 0) {
                if (result != null && result.getOutFlowId() != null && result.getOutFlowId().equals(targetId)) {
                    return true;
                }
                for (SubmitReturnData sub : result.getNextOutData()) {
                    if (sub.getOutFlowId() != null && sub.getOutFlowId().equals(targetId)) {
                        return true;
                    }
                    if (sub.getNextOutData() != null) {
                        haveNodeFlag = recursiveMatchOutFlowId(targetId, sub);
                        if (haveNodeFlag) {
                            break;
                        }
                    }

                }
            }
        }
        return haveNodeFlag;
    }

    /**
     * 查找每条线后面节点的执行人员
     *
     * @param flowElement
     * @param variableMap
     * @param isSelectPerson 是否前台选人
     * @param isPrompt       是否前台提示
     * @param isOutSelect    是否人工选择路由
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private SubmitReturnData findOutFlowNodes(String processCode, FlowElement flowElement, Map<String, Object> variableMap,
                                              boolean isSelectPerson, boolean isPrompt, boolean isOutSelect) {

        SubmitReturnData returnData = new SubmitReturnData();

        // 设置当前节点的信息
        returnData.setNodeName(flowElement.getName());
        returnData.setNodeId(flowElement.getId());

        isPrompt = true;
        boolean igNoreFlag = false;

        if (isSelectPerson || isPrompt || isOutSelect) {
            //规则调用之前判断是否满足进入条件
            String nodeId = flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
            if (nodeId != null) {
                // 查询节点扩展属性
                List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);

                wfCommService.conditionalVerification(ModelProperties.intputCondition.getName(), extendsPropList, variableMap, ErrorCode.WF000022);
            }
            returnData.setNodeRefId(nodeId);

            //1.路由选择处理
            Map<String, FlowElement> nextMap = wfCommService.routeSelection(processCode, flowElement, variableMap, isOutSelect);

            //2.指定处理人员 处理
            for (String outName : nextMap.keySet()) {
                SubmitReturnData tempData = new SubmitReturnData();
                FlowNode element = (FlowNode) nextMap.get(outName);
                String nodeType = processUtil.getTaskProp(processCode, element, Constants.NODETYPE);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
                String isblock = processUtil.getTaskProp(processCode, element, Constants.IS_NO_PERSION_SKIP);// element.getAttributeValue(Constants.XMLNAMESPACE, Constants.IS_NO_PERSION_SKIP);
                igNoreFlag = "true".equals(isblock);

                if (element instanceof EndEvent) {
                    nodeType = Constants.NODETYPE_END;
                }

                // 选择下一节点人员
                // 会签节点也加入判断（流程结束为true）
                if (isSelectPerson && StrUtil.isNotBlank(nodeType) && !(Constants.NODE_TYPE_ATTO.equals(nodeType) ||
                        Constants.NODETYPE_SUBPROC.equals(nodeType) || Constants.NODETYPE_END.equals(nodeType))) {
                    try {
                        List<UserOutputData> userList = wfCommService.findNodeUsers(element, variableMap);
                        if ((userList == null || userList.size() < 1) && igNoreFlag) {//未选到人员且选人为空跳过属性为真,不返回对应的节点
                            continue;
                        }
                        tempData.setSelectUserList(userList);
                        tempData.setSelectPerson(isSelectPerson);
                    } catch (ServiceException eb) {
                        if (igNoreFlag) {//选人为空跳过属性为真,忽略异常,继续下一个节点处理
                            continue;
                        }
                        throw eb;
                    }
                }


                //3.获取提示信息
                if (isPrompt) {
                    Object obj = variableMap.get(Constants.ROUTE_SELECT_KEY);
                    if (obj != null && (obj instanceof JSONArray)) {
                        JSONArray outArrays = (JSONArray) obj;
                        outflag:
                        for (int i = 0; i < outArrays.size(); i++) {
                            JSONObject item = outArrays.getJSONObject(i);
                            String branch = item.getString("branch");
                            if (!StrUtil.isBlank(branch)) {
                                for (SequenceFlow inFlow : element.getIncomingFlows()) {
                                    if (StrUtil.isNotEmpty(inFlow.getDocumentation()) && branch.indexOf(inFlow.getDocumentation()) != -1) {
                                        tempData.setReminder(item.getString(Constants.ROUTE_MESSAGE_KEY));
                                        break outflag;
                                    }
                                }
                            }
                        }
                    }
                    tempData.setPrompt(isPrompt);
                }

                // 设置节点信息
                tempData.setNodeType(nodeType);
                tempData.setNodeId(element.getId());
                tempData.setNodeName(element.getName());
                tempData.setNodeRefId(element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID));

                // 设置一条流转路线信息
                tempData.setOutFlowId(outName);
                //查找子流程的先去掉
                if (Constants.NODETYPE_SUBPROC.equals(nodeType)) {
                    WorkFlowStartData wfFlowStartData = new WorkFlowStartData();
                    String startPerson = processUtil.getTaskProp(processCode, element, Constants.START_PERSON);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.START_PERSION);
                    if (StrUtil.isEmpty(startPerson)) {
                        startPerson = "0";
                    }

                    Map<String, Object> userIdMap = taskCreateListener.getStartUserIdMap(startPerson, variableMap);
                    //重置流程发起人信息
                    variableMap.put(Constants.STARTUSERDATA, userIdMap);

                    //variableMap.put( Constants.CHILD_START_USER_ID, variableMap.get(Constants.BEFOR_USERID));
                    wfFlowStartData.setBussinessMap(variableMap);
                    String nodeRefId = processUtil.getTaskProp(processCode, element, Constants.NODEPROP_REFID);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
                    WfModelPropExtends selectCondation = new WfModelPropExtends();
                    selectCondation.setOwnId(nodeRefId);
                    selectCondation.setOwnType("0");
                    List<WfModelPropExtends> modelPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnIdAndOwnType(selectCondation);
                    String subconfig = modelPropService.getNomalModelPropValue(modelPropList,
                            ModelProperties.subprocessSelection.getName());
                    if (StrUtil.isNotBlank(subconfig)) {
                        // 规则获取的编码必须在扩展属性的子流程编码中存在，否则剔除
                        JSONArray subConfigObj = JSONArray.parseArray(subconfig);
                        for (Object obj : subConfigObj) {
                            Map<String, String> map = (Map<String, String>) obj;
                            wfFlowStartData.setProcessCode(map.get("defId"));
                            SubmitReturnData submitReturnData = this.findStartNextNodeInfo(wfFlowStartData);
                            List<SubmitReturnData> list = submitReturnData.getNextOutData();
                            for (SubmitReturnData data : list) {
                                data.setOutFlowId(outName + SymbolConstants.COMMA + data.getOutFlowId());
                            }
                            returnData.addOutData(submitReturnData);
                        }
                    }
                } else {
                    returnData.addOutData(tempData);
                }
            }
            //追加返回规则输出的业务参数信息
            returnData.setDataReturnMap(variableMap.get(Constants.RESULT_DATA_MAP_KEY));
            // 设置是否选择流转路径
            returnData.setSelectOutFlow(isOutSelect);
            returnData.setSelectPerson(isSelectPerson);
        }

        return returnData;
    }

    public SubmitReturnData findStartNextNodeInfo(WorkFlowStartData wfFlowStartData) {
        WfVariableUtil.setsysCommHead(wfFlowStartData.getSysCommHead());
        // 必要性检查处理
        String processCode = wfFlowStartData.getProcessCode();
        String userId = wfFlowStartData.getUserId();
        Map<String, Object> variableMap = wfFlowStartData.getBussinessMap();
        Object userIdMap = wfFlowStartData.getUserIdMap();

        if (userIdMap != null) {
            variableMap.put(Constants.BEFOR_USERID, userId);
            variableMap.put(Constants.STARTUSERDATA, userIdMap);
            variableMap.put(Constants.BEFORUSERDATA, userIdMap);
        }
        String systemIdCode = wfFlowStartData.getSystemIdCode();
        if (StrUtil.isBlank(systemIdCode)) {
            WfParam wfParam = new WfParam();
            wfParam.setParamType("F0");
            wfParam.setParamObjCode(processCode);
            List<WfParam> wfParamList = wfParamService.selectParamList(wfParam);
            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(wfParamList)) {
                systemIdCode = wfParamList.get(0).getParamCode();
            }
        }
        // 获取流程启动节点
        ProcessDefinition processDefinition = activitiBaseServer.findProcessDefinition(processCode);
        if (processDefinition == null) {
            throw new ServiceException("流程定义不存在或者未部署（发布），请确认！");
        }
        FlowElement firstElement = findStartFlowElement(processDefinition.getId());

        List<WfModelPropExtends> modelPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(processCode);
        WfModelPropExtends isSkipFistModelProp = modelPropService.getSpcefiyModelProp(modelPropList,
                ModelProperties.skipFistNode.getName());
        boolean isSkipFist = processUtil.isSikpFirstNode(processCode, isSkipFistModelProp);

        boolean isSelectPerson = false;
        boolean isPrompt = false;
        boolean isOutSelect = false;

        if (isSkipFist) {
            // 如果是首节点跳过
            variableMap.put("systemIdCode", systemIdCode);
            Map<String, FlowElement> nextMap = wfCommService.routeSelection(processCode, firstElement, variableMap, isOutSelect);

            // 获得首节点 //多分支的情况只去一个
            firstElement = nextMap.get(nextMap.keySet().iterator().next());

            // 获取第一个节点的配置信息
            isSelectPerson = Boolean.parseBoolean(processUtil.getTaskProp(processCode, firstElement, ModelProperties.checkPer.getName()));
            isPrompt = Boolean.parseBoolean(processUtil.getTaskProp(processCode, firstElement, ModelProperties.confirInfo.getName()));
            isOutSelect = Boolean.parseBoolean(processUtil.getTaskProp(processCode, firstElement, ModelProperties.personPro.getName()));
        } else {
            // 如果不是跳过首节点，由于无法从配置在开始节点，所有从流程的扩展属性中查找
            String checkPer = modelPropService.getNomalModelPropValue(modelPropList,
                    ModelProperties.checkPer.getName());
            if (StrUtil.isNotBlank(checkPer)) {
                isSelectPerson = Boolean.parseBoolean(checkPer);
            }
            String confirInfo = modelPropService.getNomalModelPropValue(modelPropList,
                    ModelProperties.confirInfo.getName());
            if (StrUtil.isNotBlank(confirInfo)) {
                isPrompt = Boolean.parseBoolean(confirInfo);
            }
            String personPro = modelPropService.getNomalModelPropValue(modelPropList,
                    ModelProperties.personPro.getName());
            if (StrUtil.isNotBlank(personPro)) {
                isOutSelect = Boolean.parseBoolean(personPro);
            }
        }

        return findOutFlowNodes(processCode, firstElement, variableMap, isSelectPerson, isPrompt, isOutSelect);
    }

    /**
     * 查找流程的开始节点
     *
     * @param processDefId
     * @return
     * @throws Exception
     */
    private FlowElement findStartFlowElement(String processDefId) {
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processDefId);
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();
        FlowElement startElement = null;
        for (FlowElement element : nodeList) {
            if (element instanceof StartEvent) {
                startElement = element;
                break;
            }
        }
        return startElement;
    }


    private Map<String, Object> getAllResultDef() {
        Map<String, Object> map = new HashMap<String, Object>();
        map = BpmStringUtil.jsonToMap(configUtil.getTaskResultInfo());
        return map;
    }

    public List<WorkFlowTaskData> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData,
                                                        PageData<WorkFlowTaskData> pageData) {
        if (StrUtil.isBlank(taskFlowData.getStartUserId())) {
            throw new ServiceException("查询任务处理结果通知信息,流程发起人不能为空!");
        }
        PageData<WfTaskflow> selectPageData = ObjectUtil.copyBeanPropPage(pageData, WfTaskflow.class);
        wfTaskflowService.queryTaskDoResultList(taskFlowData, selectPageData);

        // 处理流程变量
        List<WorkFlowTaskData> list = queryTaskDoResultList(selectPageData, pageData, taskFlowData.getMessageFlag());

        return list;

    }

    public void updateTaskDoResultList(WorkFlowTaskFlowData taskFlowData) throws Exception {
        List<WfTaskflow> list = wfTaskflowService.queryTaskDoResultList(taskFlowData);
        for (WfTaskflow t : list) {
            t.setDoResultCheck(BigInteger.ONE.toString());
            wfTaskflowService.updateById(t);
            WfHistTaskflow his = ObjectUtil.copyBeanProp(t, WfHistTaskflow.class);
            wfHistTaskflowService.updateByTaskId(his);

        }
    }

    private List<WorkFlowTaskData> queryTaskDoResultList(PageData<WfTaskflow> pageData,
                                                         PageData<WorkFlowTaskData> resultPageData, String messageFlag) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("type", "notify");
        List<WfTemplate> tmplates = wfTemplateService.selectTemplateList(param);
        Map<String, String> tmplateMap = new HashMap<String, String>();
        for (WfTemplate t : tmplates) {
            tmplateMap.put(t.getTemplateId().toString(), t.getContent());
        }
        Map<String, Object> doresultmap = this.getAllResultDef();
        // PageData<WorkFlowTaskData> resultPageData=new PageData<WorkFlowTaskData>();
        // 处理流程变量
        List<WorkFlowTaskData> resultList = new ArrayList<WorkFlowTaskData>();
        for (WfTaskflow temp : pageData.getResult()) {

            WorkFlowTaskData undoData = ObjectUtil.copyBeanProp(temp, WorkFlowTaskData.class);
            WorkFlowUnDoTaskGroupData groupData = new WorkFlowUnDoTaskGroupData();
            groupData.setSystemCode(undoData.getSystemCode());
            undoData.setBigClassNo(groupData.getBigClassNo());
            undoData.setBigClassName(groupData.getBigClassName());
            undoData.setLittleClassNo(groupData.getLittleClassNo());
            undoData.setLittleClassName(groupData.getLittleClassName());
            // 业务参数从快照中bussinessMap中获取

            if (temp.getBussinessMap() != null) {
                undoData.setVarMap(BpmStringUtil.jsonToMap(temp.getBussinessMap()));
            }
            if (!"1".equals(messageFlag)) {
                if (tmplateMap.containsKey(groupData.getLittleClassNo())) {

                    String content = BpmStringUtil.templatToMsgContent(tmplateMap.get(groupData.getLittleClassNo()),
                            undoData.getVarMap());
                    undoData.setDoResultInfo(content);
                } else {
                    String content = BpmStringUtil.templatToMsgContent(tmplateMap.get("00"), undoData.getVarMap());
                    undoData.setDoResultInfo(content);
                }
            }
            String doResultInfo = (String) doresultmap.get(temp.getDoResult());
            undoData.setDoResultInfo(doResultInfo);
            resultList.add(undoData);
        }

        ObjectUtil.copyPageData(resultPageData, pageData);
        resultPageData.setResult(resultList);
        return resultList;

    }

    /**
     * 查询流程定义的部分或全部节点
     *
     * @param taskFlowData
     * @return
     * @throws Exception
     */
    public List<WorkFlowDefNode> queryWorkflowDefNode(WorkFlowTaskFlowData taskFlowData) throws Exception {
        log.info("| - queryWorkflowDefNode>>>>>input param：{}", JSONUtil.toJsonStr(taskFlowData));
        String processCode = taskFlowData.getProcessCode();
        String processId = taskFlowData.getProcessInstId();
        String taskId = taskFlowData.getTaskId();
        String tranType = taskFlowData.getTranType();
        boolean isAll = taskFlowData.isAllNode();
        List<WorkFlowDefNode> resultList = new ArrayList<>();

        // 查询全部节点
        if (BpmConstants.TRAN_TYPE_M3.equals(tranType)) {
            // 校验查询参数是否正确
            if (StrUtil.isBlank(taskId) && StrUtil.isBlank(processId) && StrUtil.isBlank(processCode)) {
                throw new ServiceException("WF000034");
            }
            // 查询全部节点
            resultList = getAllNodeList(processCode, isAll, processId, taskId);
            // 查询可打回任务节点
        } else if (BpmConstants.TRAN_TYPE_M8.equals(tranType)) {
            // 校验入参
            if (StrUtil.isEmpty(taskId)) {
                throw new ServiceException("WF000033");
            }
            // 查找审批过的节点
            List<WfTaskflow> passList = findPaasTaskFlow(null, taskId);
            Collections.reverse(passList);
            // 获取下节点和上节点关系
            Map<String, List<String>> nextAndPreNodeRel = getNextAndPreNodeRel(taskId);
            log.info("| - queryWorkflowDefNode>>>>>found nextAndPreNodeRel：{}", JSONUtil.toJsonStr(nextAndPreNodeRel));
            // 递归获取可打回节点
            getPreNodeInfo(passList, wfTaskflowService.selectByTaskId(taskId), resultList, nextAndPreNodeRel);
        }
        return resultList;
    }

    /**
     * 查询上一节点信息
     *
     * @param passList
     * @param wfTaskflow
     * @param resultList
     * @param preAndNextNodeRel
     */
    private void getPreNodeInfo(List<WfTaskflow> passList, WfTaskflow wfTaskflow, List<WorkFlowDefNode> resultList, Map<String, List<String>> preAndNextNodeRel) {
        String taskDefId = wfTaskflow.getTaskDefId();
        List<String> preTaskDefIdList = preAndNextNodeRel.get(taskDefId);
        if (preTaskDefIdList == null || preTaskDefIdList.contains("startEvent1")) {
            return;
        }
        List<WfTaskflow> removeList = new ArrayList<>();
        for (WfTaskflow taskflow : passList) {
            if (preTaskDefIdList.contains(taskflow.getTaskDefId())) {
                //符合条件的都暂存起来
                WorkFlowDefNode defNode = new WorkFlowDefNode();
                defNode.setNodeId(taskflow.getTaskDefId());
                defNode.setNodeName(taskflow.getTaskName());
                defNode.setNodeType(taskflow.getTaskType());
                defNode.setUserId(taskflow.getDoTaskActor());
                defNode.setUserName(taskflow.getDoTaskActorName());
                resultList.add(defNode);
                break;
            }
            removeList.add(taskflow);
        }
        passList.removeAll(removeList);
        if (CollUtil.isEmpty(passList)) {
            return;
        }
        getPreNodeInfo(passList, passList.get(0), resultList, preAndNextNodeRel);
    }

    /**
     * 查询流程所有节点
     *
     * @param processCode
     * @return
     */
    private List<WorkFlowDefNode> getAllNodeList(String processCode, boolean isAll, String processId, String taskId) {
        // 流程编码为空时，根据taskId或processId获取
        if (StrUtil.isBlank(processCode)) {
            ProcessInstance processInstance = null;
            if (StrUtil.isNotBlank(processId)) {
                processInstance = activitiBaseServer.findProcessInstance(processId);
            } else {
                processInstance = activitiBaseServer.findProcessInstanceByTaskId(taskId);
            }
            if (processInstance == null) {
                throw new ServiceException("流程实例Id或当前任务Id输入有误，请查看！");
            }
            processCode = processInstance.getProcessDefinitionId();
            log.info("| - ProcessCoreServiceImpl>>>>>found processDefinitionId：{}", processCode);
        }

        List<WorkFlowDefNode> allNodeList = new ArrayList<>();
        // 查询流程所有节点
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processCode);
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();

        for (FlowElement element : nodeList) {
            // 过滤流程定义中的线
            if (element instanceof SequenceFlow) {
                continue;
            }
            // 过滤开始、结束等节点
            if (!isAll && !(element instanceof UserTask)) {
                continue;
            }
            //符合条件的都暂存起来
            WorkFlowDefNode defNode = new WorkFlowDefNode();
            defNode.setNodeId(element.getId());
            defNode.setNodeName(element.getName());
            defNode.setNodeType(element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE));
            defNode.setNodeMsg(element.getDocumentation());
            allNodeList.add(defNode);
        }
        return allNodeList;
    }

    /**
     * 获取下节点和上节点关系
     *
     * @param taskId
     * @return
     */
    private Map<String, List<String>> getNextAndPreNodeRel(String taskId) {
        // 查询流程实例
        ProcessInstance processInstance = activitiBaseServer.findProcessInstanceByTaskId(taskId);
        if (processInstance == null) {
            throw new ServiceException("流程实例Id或当前任务Id输入有误，请查看！");
        }
        // 查询流程定义
        String processCode = processInstance.getProcessDefinitionId();
        log.info("| - ProcessCoreServiceImpl>>>>>found processDefinitionId：{}", processCode);
        // 查询流程所有节点
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processCode);
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();
        // 组装下节点和上节点关系
        Map<String, List<String>> nextAndPreNodeRel = new HashMap<>();
        for (FlowElement element : nodeList) {
            if (element instanceof SequenceFlow) {
                SequenceFlow sequenceFlow = (SequenceFlow) element;
                if (nextAndPreNodeRel.containsKey(sequenceFlow.getTargetRef())) {
                    List<String> targetRefList = nextAndPreNodeRel.get(sequenceFlow.getTargetRef());
                    targetRefList.add(sequenceFlow.getSourceRef());
                    nextAndPreNodeRel.put(sequenceFlow.getTargetRef(), targetRefList);
                } else {
                    List<String> targetRefList = new ArrayList<>();
                    targetRefList.add(sequenceFlow.getSourceRef());
                    nextAndPreNodeRel.put(sequenceFlow.getTargetRef(), targetRefList);
                }
            }
        }
        return nextAndPreNodeRel;
    }

    public List<WorkFlowDefNode> findPreNodeList(String taskId) throws Exception {
        List<WorkFlowDefNode> tempList = new ArrayList<WorkFlowDefNode>();
        FlowNode flownode = (FlowNode) activitiBaseServer.getBpmnModelNodeByTaskId(taskId);
        WfTaskflow taskflow = wfTaskflowService.selectByTaskId(taskId);
        if (taskflow == null) {
            throw new ServiceException("系统中不存在对应的任务信息[taskId:" + taskId + "]");
        }
        Map<String, WorkFlowDefNode> map = new HashMap<String, WorkFlowDefNode>();
        tempList = this.findPreNodeList(taskflow.getProcessCode(), flownode, map);
        WfTaskflow condition = new WfTaskflow();
        condition.setProcessId(taskflow.getProcessId());
        condition.setWorkState(Constants.WORK_STATE_END);
        List<WfTaskflow> list = wfTaskflowService.selectByCondition(condition);
        for (WorkFlowDefNode wfnode : tempList) {
            for (WfTaskflow w : list) {
                if (wfnode.getNodeId().equals(w.getTaskDefId())) {
                    wfnode.setUserId(w.getDoTaskActor());
                    wfnode.setUserName(w.getDoTaskActorName());
                    JSONObject userJson = JSONObject.parseObject(w.getTaskActorsData());
                    JSONObject userInfoMap = userJson.getJSONObject(w.getDoTaskActor());
                    if (userInfoMap != null) {
                        String userPostName = userInfoMap.getString("userPostName");
                        if (StrUtil.isNotEmpty(userPostName)) {
                            wfnode.setUserPostName(userPostName);
                        }
                        String userPost = userInfoMap.getString("userPost");
                        if (StrUtil.isNotEmpty(userPost)) {
                            wfnode.setUserPost(userPost);

                        }
                    }
                }
            }
        }
        return tempList;
    }

    private List<WorkFlowDefNode> findPreNodeList(String processCode, FlowNode flownode, Map<String, WorkFlowDefNode> map) throws Exception {
        List<WorkFlowDefNode> tempList = new ArrayList<WorkFlowDefNode>();

        List<SequenceFlow> list = flownode.getIncomingFlows();
        for (SequenceFlow s : list) {
            FlowElement element = s.getSourceFlowElement();
            if (map.containsKey(element.getId())) {
                continue;
            }
            if (element instanceof StartEvent) {
                return tempList;
            } else {
                //符合条件的都暂存起来
                WorkFlowDefNode defNode = new WorkFlowDefNode();
                defNode.setNodeId(element.getId());
                defNode.setNodeName(element.getName());
                defNode.setNodeType(processUtil.getTaskProp(processCode, element, Constants.NODETYPE));//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE));
                defNode.setNodeMsg(element.getDocumentation());
                tempList.add(defNode);
                map.put(element.getId(), defNode);
                List<WorkFlowDefNode> ls1 = findPreNodeList(processCode, (FlowNode) element, map);
                if (ls1 != null) {
                    tempList.addAll(ls1);
                }
            }
        }

        return tempList;
    }

    /**
     * 查询当前未结束实例已审批的所有节点
     *
     * @param processId
     * @param taskId
     * @return
     * @throws Exception
     */
    private List<WfTaskflow> findPaasTaskFlow(String processId, String taskId) throws Exception {
        if (StrUtil.isBlank(processId)) {
            WfTaskflow currTaskflow = selectByTaskId(taskId);
            if (currTaskflow == null) {
                throw new ServiceException(ErrorCode.WF000026);
            }
            processId = currTaskflow.getProcessId();
        }

        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(processId);
        selectCondition.setWorkState(Constants.TASK_STATE_END);
        List<WfTaskflow> passList = wfTaskflowService.selectByCondition(selectCondition);

        // 至少有开始节点的记录，如果没有，说明processId错误
        if (passList.size() < 1) {
            throw new Exception("流程实例不存在，请查看！");
        }
        return passList;
    }

    /**
     * 修改流程任务处理意见
     *
     * @param taskTransferData
     * @return
     * @throws Exception
     */
    public boolean updateTaskOpinions(WorkFlowTaskTransferData taskTransferData) throws Exception {
        boolean result = false;
        String taskId = taskTransferData.getTaskId();
        String doRemark = taskTransferData.getResultRemark();
        WfTaskflow taskflow = selectByTaskId(taskId);
        if (taskflow != null) {
            taskflow.setDoRemark(doRemark);
            try {
                // 更新流程任务意见
                boolean flag = wfTaskflowService.updateById(taskflow);
                if (flag) {
                    result = true;
                }
            } catch (Exception e) {
                result = false;
                log.error(ExceptionUtils.getStackTrace(e));
            }
        }
        return result;
    }

    /**
     * 修改流程任务处理附件信息
     *
     * @param taskTransferData
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public boolean updateTaskAccessories(WorkFlowTaskTransferData taskTransferData) throws Exception {
        boolean result = false;
        String taskId = taskTransferData.getTaskId();
        WfTaskflow taskflow = selectByTaskId(taskId);
        if (taskflow != null) {
            String doAccessories = taskflow.getDoAccessories();// 原附件信息
            Map<String, Object> bussinessMap = taskTransferData.getBussinessMap();
            if (!Objects.isNull(bussinessMap) && !Objects.isNull(bussinessMap.get(BpmConstants.ATTACHMENT_MAP))) {
                Map<String, Object> attachmentMap = (Map<String, Object>) bussinessMap.get(BpmConstants.ATTACHMENT_MAP);
                String path = attachmentMap.get("path").toString();// path：附件路径
                String oType = attachmentMap.get("oType").toString();// oType：(append:添加附件信息、revoke:撤回附件信息)
                if (StrUtil.equals(BpmConstants.APPEND, oType)) {
                    if (StrUtil.equals("", doAccessories) || StrUtil.equals(null, doAccessories)) {
                        doAccessories = path;
                    } else {
                        doAccessories = doAccessories + SymbolConstants.COMMA + path;
                    }
                }
                if (StrUtil.equals(BpmConstants.REVOKE, oType)) {
                    // 业务系统传递的附件路径
                    List<String> newPath = Arrays.stream(path.split(SymbolConstants.COMMA)).map(String::trim)
                            .collect(Collectors.toList());
                    // 原附件路径信息数组
                    Collection<String> attachments = Arrays.stream(doAccessories.split(SymbolConstants.COMMA))
                            .map(String::trim).collect(Collectors.toList());
                    attachments.removeAll(newPath);
                    doAccessories = attachments.toString().substring(1, attachments.toString().length() - 1);
                }
                taskflow.setDoAccessories(doAccessories);
                try {
                    // 更新流程附件信息
                    boolean flag = wfTaskflowService.updateById(taskflow);

                    WfBusinessData businessData = new WfBusinessData(taskflow);
                    wfBusinessDataService.updateById(businessData);
                    if (flag) {
                        result = true;
                    }
                } catch (Exception e) {
                    result = false;
                    log.error("| - ProcessCoreServiceImpl>>>>>更改流程处理附件失败", e);
                }
            }
        }
        return result;
    }

    /**
     * 修改流程任务截止日期时间
     *
     * @param
     * @return
     * @throws Exception
     */
    @Transactional
    public boolean extendTaskEndDate(String taskId, String processId, Date estEndDate) {
        boolean result = false;

        log.info("| - ProcessCoreServiceImpl>>>>>修改截止日期时间：task:{}", taskId);
        log.info("| - ProcessCoreServiceImpl>>>>>修改截止日期时间：processId:{}", processId);
        log.info("| - ProcessCoreServiceImpl>>>>>修改截止日期时间：date:{}", estEndDate);

        if (estEndDate == null) {
            throw new ServiceException("截止日期时间不能为空！");
        }
        if (estEndDate.compareTo(new Date()) <= 0) {
            throw new ServiceException("截止日期时间不能小于当前时间！");
        }
        if (StrUtil.isBlank(taskId) && StrUtil.isBlank(processId)) {
            throw new ServiceException("流程实例ID和任务Id不能同时为空，否则无法确认修改记录！");
        }

        // 修改任务超时时间
        if (StrUtil.isNotBlank(taskId)) {
            TaskEntity taskEntity = activitiBaseServer.findTaskById(taskId);
            WfTaskflow taskflow = selectByTaskId(taskId);
            if (taskEntity == null) {
                throw new ServiceException("任务ID不存在，请确认！！");
            } else {
                String workState = taskflow.getWorkState();// 当前任务处理状态
                if (!StrUtil.equals(workState, Constants.TASK_STATE_RUNING)) {
                    throw new ServiceException("任务状态为非正常状态，无法修改超时时间，请确认！！");
                } else {
                    taskflow.setEstEndDate(DateUtil.convertObjToLdt(estEndDate));
                    taskflow.setIsTimeout("0");
                    boolean flag = wfTaskflowService.updateById(taskflow);
                    if (flag) {
                        result = true;
                    }
                }
            }
        } else if (StrUtil.isNotBlank(processId)) {
            // 修改流程超时时间
            WfInstance wfInstance = wfInstanceService.selectByProcessId(processId);
            ProcessInstance processInstance = activitiBaseServer.findProcessInstance(processId);
            wfInstance.setIsTimeout("0");
            if (processInstance == null) {
                throw new ServiceException("流程实例ID不存在，请确认");
            } else {
                String workState = wfInstance.getWorkState();
                if (!StrUtil.equals(workState, Constants.WORK_STATE_RUNNING)) {
                    throw new ServiceException("实例状态为非正常状态，无法修改超时时间，请确认！！");
                } else {
                    wfInstance.setEstEndDate(DateUtil.convertObjToLdt(estEndDate));
                    wfInstance.setUpdateTime(LocalDateTime.now());
                    boolean flag = wfInstanceService.updateById(wfInstance);
                    if (flag) {
                        result = true;
                    }
                }
            }

        }
        return result;
    }

    /**
     * 修改流程任务当前任务成员or当前操作任务成员
     *
     * @param taskTransferData
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public boolean checkOutorInUnDoTask(WorkFlowTaskTransferData taskTransferData) throws Exception {
        boolean result = false;
        String taskId = taskTransferData.getTaskId();// 业务系统传递的taskId
        String userId = taskTransferData.getUserId();// 业务系统传递的用户ID
        Map<String, Object> bussinessMap = taskTransferData.getBussinessMap();
        if (!Objects.isNull(bussinessMap) && !Objects.isNull(bussinessMap.get(BpmConstants.TODO_MAP))) {
            Map<String, Object> todoMap = (Map<String, Object>) bussinessMap.get(BpmConstants.TODO_MAP);
            String cType = todoMap.get("cType").toString();// 操作类型
            WfTaskflow taskflow = selectByTaskId(taskId);
            // 待办检出
            if (StrUtil.equals(cType, BpmConstants.CHECK_OUT)) {
                result = checkOutUnDoTask(taskflow, userId);
            }
            // 待办检入
            if (StrUtil.equals(cType, BpmConstants.CHECK_IN)) {
                // 待办检入
                result = checkInUnDoTask(taskflow, userId);
            }
        }
        return result;
    }

    public boolean checkInUnDoTask(WfTaskflow taskflow, String userId) throws Exception {
        boolean result = false;
        if (taskflow != null && StrUtil.equals(taskflow.getWorkState(), Constants.TASK_STATE_RUNING)) {
            // 待办检入

            taskflow.setDoTaskActor("");
            try {
                boolean flag = wfTaskflowService.updateById(taskflow);
                if (flag) {
                    result = true;
                }
            } catch (Exception e) {
                result = false;
                log.error("| - ProcessCoreServiceImpl>>>>>代办任务检入失败!", e);
            }
        }
        return result;
    }

    public boolean checkOutUnDoTask(WfTaskflow taskflow, String userId) throws Exception {
        boolean result = false;
        if (taskflow != null && StrUtil.equals(taskflow.getWorkState(), Constants.TASK_STATE_RUNING)) {
            String taskActors = taskflow.getTaskActors();// 当前任务成员
            if (StrUtil.contains(taskActors, userId)) {
                // 当前任务用户

                taskflow.setDoTaskActor(userId);
                try {
                    boolean flag = wfTaskflowService.updateById(taskflow);
                    if (flag) {
                        result = true;
                    }
                } catch (Exception e) {
                    result = false;
                    log.error("| - ProcessCoreServiceImpl>>>>>代办任务检出失败!", e);
                }
            }
        }
        return result;
    }

    @Transactional
    public void suspendProcessInstance(String workflowCode, String processInstId, SysCommHead sysCommHead) {
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>开始流程挂起[{}]", processInstId);
        }
        WfInstance wfInstance = null;
        if (!StrUtil.isEmpty(processInstId)) {
            wfInstance = wfInstanceService.selectByProcessId(processInstId);
        } else {
            throw new ServiceException("恢复的流程实例Id【processInstId】不能为空");
        }
        wfInstance.setEndType(Constants.PROCESS_STATE_HUNG);
        wfInstance.setUpdateTime(LocalDateTime.now());
        wfInstanceService.updateById(wfInstance);

        activitiBaseServer.suspendProcessInstance(workflowCode, processInstId, true, null);
        // 插入日志流水
        WfSuspendLog suspendlog = new WfSuspendLog();
        suspendlog.setOpitionActor(processInstId);
        suspendlog.setOpitionType(Constants.OPERATION_TYPE_PROCESS);
        suspendlog.setOpitionState(Constants.TASK_STATE_HUNG);
        suspendlog.setProcessCode(wfInstance.getProcessCode());
        suspendlog.setProcessId(processInstId);
        suspendLogService.insertWfSuspendLog(suspendlog);

        // 事件回调
        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                .selectModelExtendsPropByOwnId(wfInstance.getProcessCode().split(SymbolConstants.COLON)[0]);
        WfModelPropExtends startEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.hangUpEvent.getName());
        wfEvtServiceDefService.executeEventService(startEventInvock, processInstId, wfInstance.getProcessCode(),
                wfInstance.getProcessId(), new HashMap<String, Object>(), null, sysCommHead);

        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>完成流程挂起[{}]", processInstId);
        }
    }

    @Transactional
    public void activateProcessInstance(String workflowCode, String processInstId, SysCommHead sysCommHead) {
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>开始流程挂起恢复[{}]", processInstId);
        }
        WfInstance wfInstance = null;
        if (!StrUtil.isEmpty(processInstId)) {
            wfInstance = wfInstanceService.selectByProcessId(processInstId);
        } else {
            throw new ServiceException("恢复的流程实例Id【processInstId】不能为空");
        }
        wfInstance.setEndType(Constants.WORK_STATE_RUNNING);
        wfInstance.setUpdateTime(LocalDateTime.now());
        wfInstanceService.updateById(wfInstance);
        activitiBaseServer.activateProcessInstance(workflowCode, processInstId, true, null);
        // 插入日志流水
        WfSuspendLog suspendlog = new WfSuspendLog();
        suspendlog.setOpitionActor(processInstId);
        suspendlog.setOpitionType(Constants.OPERATION_TYPE_PROCESS);
        suspendlog.setOpitionState(Constants.TASK_STATE_RUNING);
        suspendlog.setProcessCode(wfInstance.getProcessCode());
        suspendlog.setProcessId(processInstId);
        suspendLogService.insertWfSuspendLog(suspendlog);
        // 事件回调
        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                .selectModelExtendsPropByOwnId(wfInstance.getProcessCode().split(SymbolConstants.COLON)[0]);
        WfModelPropExtends startEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.recoverEvent.getName());
        wfEvtServiceDefService.executeEventService(startEventInvock, processInstId, wfInstance.getProcessCode(),
                wfInstance.getProcessId(), new HashMap<String, Object>(), null, sysCommHead);
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>完成流程挂起恢复[{}]", processInstId);
        }
    }

    /**
     * 取消指定任务，流程继续
     *
     * @param taskId 任务标识
     * @throws Exception
     */
    public void cancelTask(String taskId, String userId) {

        if (StrUtil.isBlank(userId)) {
            throw new ServiceException(ErrorCode.WF000024);
        }
        if (StrUtil.isBlank(taskId)) {
            throw new ServiceException(ErrorCode.WF000025);
        }

        WfTaskflow currTaskFlow = selectByTaskId(taskId);
        if (currTaskFlow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }
        if (!Constants.TASK_STATE_RUNING.equals(currTaskFlow.getWorkState())) {
            throw new ServiceException("指定的待办任务状态不符合取消的状态！");
        }
        // 提交当前任务
        FlowElement flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(taskId);
        this.commitProcess(taskId, currTaskFlow.getProcessCode(), new HashMap<String, Object>(), flowElement);

        // 更改待办任务状态和信息
        currTaskFlow.setUpdateRemark("任务取消，流程继续");
        currTaskFlow.setWorkState(Constants.TASK_STATE_CANCEL);
        currTaskFlow.setDoTaskActor(userId);
        wfTaskflowService.updateById(currTaskFlow);
        WfBusinessData businessData = new WfBusinessData(currTaskFlow);
        wfBusinessDataService.updateById(businessData);

    }

    /**
     * 挂起或取消挂起任务错误
     *
     * @param taskId   任务标识
     * @param isCancel 挂起和取消标志 true：标识取消挂起
     * @throws Exception
     */
    public void suspendTask(String taskId, boolean isCancel, String userId) {
        if (StrUtil.isBlank(taskId)) {
            throw new ServiceException(ErrorCode.WF000025);
        }

        WfTaskflow currTaskFlow = selectByTaskId(taskId);
        if (currTaskFlow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }
        if (!isCancel && !Constants.TASK_STATE_RUNING.equals(currTaskFlow.getWorkState())) {
            throw new ServiceException("指定的待办任务状态不符合挂起的状态！");
        }
        if (isCancel && !Constants.TASK_STATE_HUNG.equals(currTaskFlow.getWorkState())) {
            throw new ServiceException("指定任务的状态为非挂起状态，无法取消！");
        }

        // 更改待办任务状态和信息
        currTaskFlow.setUpdateRemark("任务挂起");
        String workState = isCancel ? Constants.TASK_STATE_RUNING : Constants.TASK_STATE_HUNG;
        currTaskFlow.setWorkState(workState);
        currTaskFlow.setDoTaskActor(userId);
        wfTaskflowService.updateById(currTaskFlow);
        WfBusinessData businessData = new WfBusinessData(currTaskFlow);
        wfBusinessDataService.updateById(businessData);

        // 插入日志流水
        WfSuspendLog suspendlog = new WfSuspendLog();
        suspendlog.setOpitionActor(taskId);
        suspendlog.setOpitionType(Constants.OPERATION_TYPE_TASK);
        suspendlog.setTaskId(taskId);
        String optionState = isCancel ? Constants.TASK_STATE_RUNING : Constants.TASK_STATE_HUNG;
        suspendlog.setOpitionState(optionState);
        suspendlog.setProcessCode(currTaskFlow.getProcessCode());
        suspendlog.setProcessId(currTaskFlow.getProcessId());

        suspendLogService.insertWfSuspendLog(suspendlog);
    }

    public List<WorkFlowTaskData> listNoTimeOutTaskList(Map<String, Object> inputMap, WorkFlowTaskFlowData taskFlowData,
                                                        PageData<WorkFlowTaskData> pageData) throws Exception {

        if (StrUtil.isBlank(taskFlowData.getUserId())) {
            throw new Exception("待办任务的查询人员不能为空!");
        }
        // 执行状态必须为非执行
        taskFlowData.setWorkState(Constants.TASK_STATE_RUNING);

        // 超时预警时间
        if (!Objects.isNull(inputMap) && !Objects.isNull(inputMap.get(BpmConstants.ALARM_TIME))) {
            String alarmTime = inputMap.get(BpmConstants.ALARM_TIME).toString();
            String estEndDate = DateUtil.addDateByUnit(DateUtil.covertObjToString(DateUtil.getCurrentLocalTime(),DateUtil.DATE_FORMATTER_5),
                    alarmTime);
            taskFlowData.setEstEndDate(DateUtil.convertObjToUtilDate(estEndDate));
        }
        PageData<WfTaskflow> selectPageData = ObjectUtil.copyBeanPropPage(pageData, WfTaskflow.class);
        wfTaskflowService.overTimeTaskList(taskFlowData, selectPageData);

        // 处理流程变量
        List<WorkFlowTaskData> resultList = new ArrayList<WorkFlowTaskData>();
        for (WfTaskflow temp : selectPageData.getResult()) {
            WorkFlowTaskData undoData = ObjectUtil.copyBeanProp(temp, WorkFlowTaskData.class);
            WorkFlowUnDoTaskGroupData groupData = new WorkFlowUnDoTaskGroupData();
            groupData.setSystemCode(undoData.getSystemCode());
            undoData.setBigClassNo(groupData.getBigClassNo());
            undoData.setBigClassName(groupData.getBigClassName());
            undoData.setLittleClassNo(groupData.getLittleClassNo());
            undoData.setLittleClassName(groupData.getLittleClassName());
            // 业务参数从快照中bussinessMap中获取
            if (temp.getBussinessMap() != null) {
                undoData.setVarMap(BpmStringUtil.jsonToMap(temp.getBussinessMap()));
            }
            resultList.add(undoData);
        }
        ObjectUtil.copyPageData(selectPageData, pageData);
        pageData.setResult(resultList);
        return resultList;
    }

    public List<WorkFlowTaskData> listTimeOutTaskList(WorkFlowTaskFlowData taskFlowData,
                                                      PageData<WorkFlowTaskData> pageData) throws Exception {
        if (StrUtil.isBlank(taskFlowData.getUserId())) {
            throw new Exception("待办任务的查询人员不能为空!");
        }
        // 执行状态必须为非执行
        taskFlowData.setWorkState(Constants.TASK_STATE_RUNING);
        // 任务超时标识
        taskFlowData.setIsTimeout(Constants.TIME_OUT_STATE);

        PageData<WfTaskflow> selectPageData = ObjectUtil.copyBeanPropPage(pageData, WfTaskflow.class);
        wfTaskflowService.overTimeTaskList(taskFlowData, selectPageData);

        // 处理流程变量
        List<WorkFlowTaskData> resultList = new ArrayList<WorkFlowTaskData>();
        for (WfTaskflow temp : selectPageData.getResult()) {
            WorkFlowTaskData undoData = ObjectUtil.copyBeanProp(temp, WorkFlowTaskData.class);
            WorkFlowUnDoTaskGroupData groupData = new WorkFlowUnDoTaskGroupData();
            groupData.setSystemCode(undoData.getSystemCode());
            undoData.setBigClassNo(groupData.getBigClassNo());
            undoData.setBigClassName(groupData.getBigClassName());
            undoData.setLittleClassNo(groupData.getLittleClassNo());
            undoData.setLittleClassName(groupData.getLittleClassName());
            // 业务参数从快照中bussinessMap中获取
            if (temp.getBussinessMap() != null) {
                undoData.setVarMap(BpmStringUtil.jsonToMap(temp.getBussinessMap()));
            }
            resultList.add(undoData);
        }
        ObjectUtil.copyPageData(selectPageData, pageData);
        pageData.setResult(resultList);
        return resultList;
    }


    /**
     * 更改待办任务处理人员
     *
     * @param
     * @return
     * @throws Exception
     */


    public boolean modifyTaskActors(String taskId, String userId, String agentUserId) {

        if (StrUtil.isBlank(userId)) {
            throw new ServiceException(ErrorCode.WF000024);
        }
        if (StrUtil.isBlank(taskId)) {
            throw new ServiceException(ErrorCode.WF000025);
        }
        if (StrUtil.isBlank(agentUserId)) {
            throw new ServiceException("待办任务新的处理人员不能为空！");
        }
        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }
        if (!Constants.TASK_STATE_RUNING.equals(wfTaskflow.getWorkState())) {
            throw new ServiceException("指定的待办任务为非待办状态，无法更换处理人员！");
        }

        wfTaskflow.setTaskActors(agentUserId);
        JSONArray userAskListJson = JSONArray.parseArray(" [ { \"orderUserNo\":\"" + agentUserId + "\"}]");
        List<UserOutputData> list;
        try {
            list = queryRelationTellerFactory.getInstance().queryUserList(userAskListJson);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("替换失败，查找不到对应的用户信息");
        }
        if (list == null || list.size() < 1) {
            throw new ServiceException("替换失败，查找不到对应的用户信息");
        }
        JSONObject data = JSONObject.parseObject(wfTaskflow.getTaskActorsData());
        data.put(agentUserId, list.get(0));
        wfTaskflow.setTaskActorsData(data.toJSONString());
        wfTaskflowService.updateById(wfTaskflow);
        //更换处理人的时候，同时要处理业务数据表
        WfBusinessData businessData = new WfBusinessData(wfTaskflow);
        wfBusinessDataService.updateById(businessData);
        processUtil.changeCandidateUser(taskId, userId, agentUserId, 1);

        return true;
    }

    /**
     * @param list  最终返回集合
     * @param maps1 外层循环list
     * @param maps2 内层循环list
     * @Title: exceptionEndReport_Loop
     * @Description: (循环DB查询的List集合, 将异常流程结束实例数与超时异常流程结束实例数拼在一个集合里)
     * @date 2018年8月22日 上午10:09:03
     */
    private void exceptionEndReportLoop(List<Map<String, Object>> list, List<Map<String, Object>> maps1,
                                        List<Map<String, Object>> maps2, String type) {
        if (StrUtil.equals(type, Constants.NOT_TIME_OUT_STATE)) {
            if (!maps1.isEmpty() && maps1.size() > 0) {
                for (int i = 0; i < maps1.size(); i++) {
                    Map<String, Object> map1 = (Map<String, Object>) maps1.get(i);
                    String systemCode1 = map1.get("systemCode").toString();
                    String processCode1 = map1.get(BpmConstants.PROCESS_CODE).toString();
                    String timeEndProcessNums1 = map1.get("timeEndProcessNums").toString();
                    if (!maps2.isEmpty() && maps2.size() > 0) {
                        for (int j = 0; j < maps2.size(); j++) {
                            Map<String, Object> map2 = (Map<String, Object>) maps2.get(j);
                            String systemCode2 = map2.get("systemCode").toString();
                            String processCode2 = map2.get(BpmConstants.PROCESS_CODE).toString();
                            String timeEndProcessNums2 = map2.get("timeEndProcessNums").toString();
                            // 系统分类、流程编码相同
                            if (systemCode1.equals(systemCode2) && processCode1.equals(processCode2)) {
                                int timeEndProcessNums = Integer.parseInt(timeEndProcessNums1)
                                        + Integer.parseInt(timeEndProcessNums2);
                                map1.put("timeEndProcessNums", String.valueOf(timeEndProcessNums));
                                maps1.set(i, map1);
                                // 删除第j个map
                                maps2.remove(j);
                                j--;
                            }
                        }
                    }
                }
                if (!maps2.isEmpty()) {
                    maps1.addAll(maps2);
                }
                list.addAll(maps1);
            }
        }
        if (StrUtil.equals(type, Constants.TIME_OUT_STATE)) {
            if (!maps1.isEmpty() && maps1.size() > 0) {
                for (int i = 0; i < maps1.size(); i++) {
                    Map<String, Object> map1 = (Map<String, Object>) maps1.get(i);
                    String systemCode1 = map1.get("systemCode").toString();
                    String processCode1 = map1.get(BpmConstants.PROCESS_CODE).toString();
                    String doEndProcessNums1 = map1.get("doEndProcessNums").toString();
                    if (!maps2.isEmpty() && maps2.size() > 0) {
                        for (int j = 0; j < maps2.size(); j++) {
                            Map<String, Object> map2 = (Map<String, Object>) maps2.get(j);
                            String systemCode2 = map2.get("systemCode").toString();
                            String processCode2 = map2.get(BpmConstants.PROCESS_CODE).toString();
                            String doEndProcessNums2 = map2.get("doEndProcessNums").toString();
                            // 系统分类、流程编码相同
                            if (systemCode1.equals(systemCode2) && processCode1.equals(processCode2)) {
                                long doEndProcessNums = Integer.parseInt(doEndProcessNums1)
                                        + Integer.parseInt(doEndProcessNums2);
                                map1.put("doEndProcessNums", String.valueOf(doEndProcessNums));
                                maps1.set(i, map1);
                                // 删除第j个map
                                maps2.remove(j);
                                j--;
                            }
                        }
                    }
                }
                if (!maps2.isEmpty()) {
                    maps1.addAll(maps2);
                }
                list.addAll(maps1);
            }
        }
    }

    /**
     * 动态加签
     *
     * @param userId
     * @param taskId
     * @param addTaskType
     * @param targetUserId
     * @param targetUserMap
     * @param bussinessWhere
     * @param remark
     * @throws Exception
     */
    @Transactional
    public void additionTask(String userId, String taskId, String addTaskType, String targetUserId,
                             Map<String, Object> targetUserMap, Map<String, Object> bussinessWhere, String remark) {
        // 初始数据准备
        WfTaskflow taskflow = selectByTaskId(taskId);
        String mapKey = Constants.ADDITION_MAPKEY;
        if (taskflow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }
        Map<String, Object> variables = JSONObject.parseObject(taskflow.getBussinessMap());
        if (variables == null) {
            variables = new HashMap<String, Object>(2);
        }
        // 加签类型不能为空，默认设置为 加签通知
        Long addType = Constants.WORK_TYPE_ADDITION_TASKNOTIF;
        if (StrUtil.isNotBlank(addTaskType) && BpmConstants.ADD_TASK_TYPE_1.equals(addTaskType)) {
            addType = Constants.WORK_TYPE_ADDITION_TASK;
        }
        String olduid = taskflow.getTaskActors();

        // 操作合法性检查
        Map<String, String> addSignMap = additionTaskCheck(taskflow, userId, addTaskType, targetUserId, olduid, addType,
                mapKey, variables);

        // 加签信息日志打印
        if (log.isInfoEnabled()) {
            log.info("| - ProcessCoreServiceImpl>>>>>动态加签任务[taskId:{}]", taskId);
            log.info("| - ProcessCoreServiceImpl>>>>>动态加签原用户[old_user:{}]", olduid);
            log.info("| - ProcessCoreServiceImpl>>>>>动态加签给用户[userId:{}]", targetUserId);
        }

        // 查询动态加签目标人员的信息
        if (targetUserMap == null || targetUserMap.isEmpty()) {
            targetUserMap = new HashMap<>();
            try {
                List<Map<String, String>> tempList = new ArrayList<>();
                Map<String, String> tempMap = new HashMap<>();
                tempMap.put("orderUserNo", targetUserId);
                tempList.add(tempMap);
                List<UserOutputData> userDataList = queryRelationTellerFactory.getInstance()
                        .queryUserList(BpmStringUtil.listToJson(tempList));
                if (userDataList == null || userDataList.size() < 1) {
                    throw new ServiceException("查询动态加签目标人员[" + targetUserId + "]非系统人员！");
                }
                // 应该只有一条，取第一条记录即可
                for (UserOutputData userData : userDataList) {
                    targetUserMap.put(userData.getUserId(), userData);
                }
            } catch (Exception e) {
                log.error("| - ProcessCoreServiceImpl>>>>>查询动态加签目标人员[{}]信息失败!", targetUserId, e);
                throw new ServiceException("查询动态加签目标人员[" + targetUserId + "]信息失败！");
            }
        }

        // 修改业务数据
        if (bussinessWhere != null) {
            variables.putAll(bussinessWhere);
        }

        // 增加动态加签的记录信息
        WfTaskflow taskflowold = ObjectUtil.copyBeanProp(taskflow, WfTaskflow.class);
        LocalDateTime now = LocalDateTime.now();
        taskflowold.setId(null);
        taskflowold.setTaskId("00001");
        taskflowold.setTaskActors(olduid);
        taskflowold.setWorkType(Integer.parseInt(String.valueOf(addType)));
        taskflowold.setWorkState(Constants.TASK_STATE_END);
        taskflowold.setEndDate(now);
        taskflowold.setDoResult("D");
        taskflowold.setDoRemark(remark);
        taskflowold.setUpdateDate(now);
        taskflowold.setDoTaskActor(userId);
        taskflowold.setBussinessMap(JSONObject.toJSONString(variables));

        long due = 0L;
        try {
            due = System.currentTimeMillis() - DateUtil.convertObjToTimestamp(taskflowold.getStartDate()).getTime();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        taskflowold.setDuedata(due);
        JSONObject json = JSONObject.parseObject(taskflow.getTaskActorsData());
        if (json != null && json.containsKey(userId)) {
            taskflowold.setDoTaskActorName(json.getJSONObject(userId).getString("userName"));
        }
        wfTaskflowService.insertWfTaskflow(taskflowold);
        WfBusinessData businessData = new WfBusinessData(taskflowold);
        wfBusinessDataService.insertWfBusinessData(businessData);

        // 揉合原处理人和动态加签人员信息
        String taskActors = taskflow.getTaskActors();
        String taskActorsData = taskflow.getTaskActorsData();
        if (addType.equals(Constants.WORK_TYPE_ADDITION_TASKNOTIF)) {
            taskActors = taskActors + SymbolConstants.COMMA + targetUserId;
            Map<String, Object> taskUsersMap = JSONObject.parseObject(taskActorsData);
            taskUsersMap.putAll(targetUserMap);
            taskActorsData = JSON.toJSONString(taskUsersMap);
        } else {
            taskActors = targetUserId;
            taskActorsData = JSON.toJSONString(targetUserMap);
            processUtil.deleteCandidateUser(taskId);
        }
        processUtil.batchAddCandidateUser(taskId, targetUserId);

        // 添加加签控制参数
        if (addSignMap.isEmpty()) {
            // 记录初始的 任务处理人员
            addSignMap.put(addSignMap.size() + "", taskflow.getTaskActorsData());
        }
        addSignMap.put(addSignMap.size() + "", JSON.toJSONString(targetUserMap));
        variables.put(mapKey, addSignMap);

        // 修改taskflow记录参数
        taskflow.setWorkAllot(userId);
        taskflow.setTaskActors(targetUserId);
        taskflow.setDoRemark("");
        taskflow.setWorkType(Integer.parseInt(String.valueOf(addType)));
        taskflow.setTaskActors(taskActors);
        taskflow.setTaskActorsData(taskActorsData);
        taskflow.setCreateDate(now);
        taskflow.setBussinessMap(JSONObject.toJSONString(variables));
        wfTaskflowService.updateById(taskflow);
        WfBusinessData oldbusinessData = new WfBusinessData(taskflow);
        wfBusinessDataService.updateById(oldbusinessData);

        // 此处应该给targetUser发送一个消息通知

    }

    /**
     * 动态加签合法性检查处理
     *
     * @param taskflow
     * @param userId
     * @param addTaskType
     * @param targetUserId
     * @param olduid
     * @param addType
     * @param mapKey
     * @param variables
     * @return
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> additionTaskCheck(WfTaskflow taskflow, String userId, String addTaskType,
                                                  String targetUserId, String olduid, Long addType, String mapKey, Map<String, Object> variables) {
        // 操作合法性检查
        if (taskflow.getWorkState().equals(Constants.TASK_STATE_END)) {
            throw new ServiceException("不能动态加签已经处理的任务");
        }
        if (taskflow.getWorkState().equals(Constants.TASK_STATE_HUNG)) {
            throw new ServiceException("任务已经挂起,必须取消挂起,才能做相应操作!");
        }
        if (userId.trim().equals(targetUserId)) {
            throw new ServiceException("不能自己动态加签给自己");
        }
        String[] oldUids = StrUtil.splitToArray(olduid, SymbolConstants.COMMA);
        if (!ArrayUtils.contains(oldUids, userId)) {
            throw new ServiceException("用户没有权限进行动态加签操作！");
        }

        long workType = taskflow.getWorkType().longValue();
        if ((workType == Constants.WORK_TYPE_ADDITION_TASK
                && addType.longValue() == Constants.WORK_TYPE_ADDITION_TASKNOTIF.longValue())
                || (workType == Constants.WORK_TYPE_ADDITION_TASKNOTIF
                && addType.longValue() == Constants.WORK_TYPE_ADDITION_TASK.longValue())) {
            throw new ServiceException("不能转换加签类型，加签类型必须一致，请确认！");
        }

        Map<String, String> addSignMap = new HashMap<String, String>();
        if (variables.containsKey(mapKey) && variables.get(mapKey) != null) {
            addSignMap = (Map<String, String>) variables.get(mapKey);
        }
        // 不能给同一个人加两次，或不能形成循环加签
        if (ArrayUtils.contains(oldUids, targetUserId)) {
            throw new ServiceException("加签目标人员已经在任务处理人员列表[" + olduid + "]中了，不允许在做同样的处理了！");
        } else { // (addType==Constants.ADDITION_TASK的多次加签情况
            String value = null;
            Set<String> tempSet = new HashSet<String>();
            for (int i = 0; i < addSignMap.size(); i++) {
                value = addSignMap.get("" + i);
                tempSet.addAll(JSON.parseObject(value).keySet());
            }
            if (tempSet.contains(targetUserId)) {
                throw new ServiceException("加签目标人员已经在任务处理人员列表[" + StrUtil.join(SymbolConstants.COMMA, tempSet) + "]中了，不允许在做同样的处理了！");
            }
        }
        return addSignMap;
    }

    /**
     * 动态加签任务的审批处理
     *
     * @throws Exception
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private boolean additionTaskCommit(WfTaskflow taskflow, WorkFlowTaskTransferData taskTransferData,
                                       Map<String, Object> exectMap) {
        boolean result = true;
        Map<String, Object> variables = JSONObject.parseObject(taskflow.getBussinessMap());
        String userId = taskTransferData.getUserId();
        Integer workType = taskflow.getWorkType();

        String mapKey = Constants.ADDITION_MAPKEY;
        if (!variables.containsKey(mapKey) || variables.get(mapKey) == null) {
            // 说明不是加签任务
            return result;
        }

        Map<String, Object> oldAddSignMap = (Map) variables.get(mapKey);
        String taskActorData = (String) oldAddSignMap.get("0");
        Map<String, Object> initTaskActors = JSON.parseObject(taskActorData);
        if (initTaskActors.containsKey(userId)) {
            exectMap.remove(mapKey);
            return false;
        }

        String currTimeStr = "" + System.currentTimeMillis();
        String newTaskId = currTimeStr.substring(currTimeStr.length() - 6);
        // 新增一条审批及记录

        WfTaskflow taskflowold = ObjectUtil.copyBeanProp(taskflow, WfTaskflow.class);
        taskflowold.setId(null);
        taskflowold.setTaskId(newTaskId);
        taskflowold.setDoTaskActor(userId);
        taskflowold.setWorkState(Constants.TASK_STATE_END);
        taskflowold.setDoResult(taskTransferData.getResult());
        taskflowold.setDoRemark(taskTransferData.getResultRemark());
        taskflowold.setWorkType(workType);
        long oldCreateTime = 0L;
        try {
            oldCreateTime = DateUtil.convertObjToTimestamp(taskflow.getCreateDate()).getTime();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        taskflowold.setDuedata(System.currentTimeMillis() - oldCreateTime);
        taskflowold.setCreateDate(DateUtil.convertObjToLdt(oldCreateTime - 1000));
        LocalDateTime now = LocalDateTime.now();
        taskflowold.setEndDate(now);
        taskflowold.setUpdateDate(now);

        // 出栈当前执行人，找出上一个加签的人（即：转办给当前人的）
        Map<String, Object> addSignMap = new HashMap<String, Object>();
        if (workType.intValue() == Constants.WORK_TYPE_ADDITION_TASK) {
            // 移除最后一个，因为只有最后一个才能看到任务
            addSignMap.putAll(oldAddSignMap);
            addSignMap.remove("" + (addSignMap.size() - 1));

            String taskActorsData = (String) addSignMap.get("" + (addSignMap.size() - 1));
            Map<String, Object> actorsListMap = JSONObject.parseObject(taskActorsData);
            // 将待办任务转已给上一加签人
            taskflow.setTaskActors(StrUtil.join(SymbolConstants.COMMA, actorsListMap.keySet()));
            taskflow.setTaskActorsData(taskActorsData);
        } else if (workType.intValue() == Constants.WORK_TYPE_ADDITION_TASKNOTIF) {
            String value = null;
            Map<String, Object> temp = null;
            // 保证任务原始处理人的key为"0"不变，同时移除当前处理的userId，其他key重新排序放入
            addSignMap.put("0", oldAddSignMap.get("0"));
            for (String key : oldAddSignMap.keySet()) {
                value = (String) oldAddSignMap.get(key);
                temp = JSON.parseObject(value);
                if (temp.containsKey(userId)) {
                    continue;
                }
                if ("0".equals(key)) {
                    continue;
                }
                // 顺序调整了key
                addSignMap.put("" + addSignMap.size(), value);
            }

            // 在现有的taskActors 和taskActorsData 移除当前处理人的信息记录
            String taskActorsData = taskflow.getTaskActorsData();

            Map<String, Object> taskActorMap = JSON.parseObject(taskActorsData);
            taskActorMap.remove(userId);
            taskActorsData = JSON.toJSONString(taskActorMap);

            taskflow.setTaskActors(StrUtil.join(SymbolConstants.COMMA, taskActorMap.keySet()));
            taskflow.setTaskActorsData(taskActorsData);
        }

        variables.put(mapKey, addSignMap);
        taskflow.setBussinessMap(JSONObject.toJSONString(variables));
        taskflow.setSuperTaskId(newTaskId);

        wfTaskflowService.insertWfTaskflow(taskflowold);
        wfTaskflowService.updateById(taskflow);
        WfBusinessData oldbusinessData = new WfBusinessData(taskflowold);
        wfBusinessDataService.insertWfBusinessData(oldbusinessData);
        WfBusinessData businessData = new WfBusinessData(taskflow);
        wfBusinessDataService.updateById(businessData);
        processUtil.deleteCandidateUser(taskflow.getTaskId(), userId);
        if (workType.intValue() == Constants.WORK_TYPE_ADDITION_TASK) {
            processUtil.batchAddCandidateUser(taskflow.getTaskId(), taskflow.getTaskActors());
        }

        // TODO 此处应该给taskflow.getTaskActors()再发送一次消息通知

        return result;
    }

    @SuppressWarnings("unused")
    public void updateWorkFlow(Map<String, Object> inputMap) throws Exception {
        String taskId = (String) inputMap.get("taskId");
        String userId = (String) inputMap.get("userId");
        if (taskId == null) {
            throw new ServiceException("修改任务时，需要指定任务ID");
        }
        if (userId == null) {
            throw new ServiceException("修改任务时，需要上宋用户id");
        }

        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (!wfTaskflow.getTaskActors().contains(userId)) {
            throw new ServiceException("用户[" + userId + "]没有处理任务[" + taskId + "]的权限");
        }

        String index1 = (String) inputMap.get(Constants.QUERY_INDEX_1);
        String index2 = (String) inputMap.get(Constants.QUERY_INDEX_2);
        String index3 = (String) inputMap.get(Constants.QUERY_INDEX_3);
        String index4 = (String) inputMap.get(Constants.QUERY_INDEX_4);

        if (wfTaskflow == null) {
            WfHistTaskflow wfHistTaskflow = wfHistTaskflowService.selectByTaskId(taskId);
            if (wfHistTaskflow == null) {
                throw new ServiceException("找不到对应的任务");
            }
            wfHistTaskflow.setQueryIndexa(index1);
            wfHistTaskflow.setQueryIndexb(index2);
            wfHistTaskflow.setQueryIndexc(index3);
            wfHistTaskflow.setQueryIndexd(index4);
            wfHistTaskflowService.updateByTaskId(wfHistTaskflow);
        } else {
            wfTaskflow.setQueryIndexa(index1);
            wfTaskflow.setQueryIndexb(index2);
            wfTaskflow.setQueryIndexc(index3);
            wfTaskflow.setQueryIndexd(index4);
        }

        wfTaskflowService.updateById(wfTaskflow);
        WfBusinessData businessData = new WfBusinessData(wfTaskflow);
        wfBusinessDataService.updateById(businessData);
    }

    public UserOutputData getUserData(WfTaskflow taskflow, String userId) {
        String taskActorsData = taskflow.getTaskActorsData();
        JSONObject jsonobj = JSONObject.parseObject(taskActorsData);
        if (jsonobj.containsKey(userId)) {
            JSONObject userObj = jsonobj.getJSONObject(userId);
            if (userObj != null) {
                return new UserOutputData(userObj);
            }
        }
        return null;
    }


    public void taskAssginUser(String userId, String taskId) throws Exception {

        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException(ErrorCode.WF000026);
        }
        if (!Constants.TASK_STATE_RUNING.equals(wfTaskflow.getWorkState())) {
            throw new ServiceException("指定的待办任务为非待办状态，无法设置处理人员！");
        }

        JSONArray userAskListJson = JSONArray.parseArray(" [ { \"orderUserNo\":\"" + userId + "\"}]");
        List<UserOutputData> list = queryRelationTellerFactory.getInstance().queryUserList(userAskListJson);
        if (list == null || list.size() < 1) {
            throw new ServiceException("替换失败，查找不到对应的用户信息");
        }
        JSONObject data = JSONObject.parseObject(wfTaskflow.getTaskActorsData());
        data.put(userId, list.get(0));
        wfTaskflow.setTaskActorsData(data.toJSONString());
        wfTaskflowService.updateById(wfTaskflow);
        WfBusinessData businessData = new WfBusinessData(wfTaskflow);
        wfBusinessDataService.updateById(businessData);
        processUtil.batchAddCandidateUser(taskId, userId);

    }

    /**
     * 待办任务检出(领用)接口,去掉之前流程上的扩展属性，统计加到节点上
     *
     * @param taskId    领用任务id
     * @param userId    操作人id
     * @param userIdMap 领用人信息
     * @return
     * @throws Exception
     */
    @Transactional
    public WfTaskflow checkOutUnDoTask(String taskId, String userId, Map<String, Object> userIdMap) {
        if (StrUtil.isEmpty(taskId)) {
            throw new ServiceException("任务Id不能为空，请确认!");
        }
        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException("指定的待办任务不存在，请确认!");
        }
        if (!Constants.TASK_STATE_RUNING.equals(wfTaskflow.getWorkState())) {
            throw new ServiceException("指定的任务为非待办状态，领用失败！");
        }
        //2019-04-27 amt modfiy start 根据任务池状态来判断
        if (StrUtil.equals(wfTaskflow.getTaskPoolFlag(), Constants.IS_HIST_TASK_POOL)) {
            throw new ServiceException("此任务已被领用/分配，请刷新界面，重新领用其他任务");
        }
        //2019-04-27 amt modfiy end 根据任务池状态来判断
        UserOutputData user = this.getUserData(wfTaskflow, userId);
        if (user == null) {
            throw new ServiceException("用户[" + userId + "]没有领用任务[" + taskId + "]的权限！");
        }
        //扩展属性变了，这里解析规则也变了
        //设置任务为任务池历史，因为已经被领用
//		String processCode=wfTaskflow.getProcessCode().split(SymbolConstants.COILON)[0];
        //检查任务的扩展属性，判断领用数量，判断是否超过上线
        List<WfModelPropExtends> processPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(wfTaskflow.getTaskCode());
        WfModelPropExtends checkInOut = modelPropService.getSpcefiyModelProp(processPropList, ModelProperties.checkInOut.getName());
        if (checkInOut != null) {
            Map<String, Object> param = BpmStringUtil.jsonToMap(checkInOut.getPropValue());
            int maxNum = (int) param.get(Constants.MAX_CHECK_NUM);
            //根据任务节点判断是否超过任务上线
            WorkFlowTaskFlowData taskFlowData = new WorkFlowTaskFlowData();
            taskFlowData.setTaskCode(wfTaskflow.getTaskCode());
            taskFlowData.setUserId(userId);
            int c = wfTaskflowService.selectCheckOutCount(taskFlowData);
            if (c >= maxNum) {
                throw new ServiceException("领用失败。待办任务已达上限,请先处理待办列表。");
            }
            //这里对配置的参数进行转换，之前没有做转换，导致CheckInFlag存储不正确
            //扩展属性里面有任务是否允许退回的限制
            if (StrUtil.equals((String) param.get(Constants.CHECK_IN_FLAG), BpmConstants.CHECK_IN_FLAG_3)) {
                wfTaskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKOUT);
            } else {
                wfTaskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKOUT_NOTIN);
            }
            //这里针对已经取消扩展属性的配置，取消以后对最大上限不做限制
        } else if (wfTaskflow.getTaskPoolFlag().equals(Constants.IS_TASK_POOL)) {
            wfTaskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKOUT);
        } else {
            throw new ServiceException("非任务池任务不能领用");

        }
        //设置任务为任务池历史，因为已经被领用
        wfTaskflow.setTaskPoolFlag(Constants.IS_HIST_TASK_POOL);
        //检出操作
        // 领用时把领用人放入dotaskActor
        wfTaskflow.setDoTaskActor(userId);

        wfTaskflow.setWorkType(Integer.valueOf(String.valueOf(Constants.WORK_TYPE_CHECK_OUT)));
        wfTaskflow.setCheckOutTime(LocalDateTime.now());
        wfTaskflowService.updateById(wfTaskflow);
        //清理任务的人员信息
        processUtil.deleteCandidateUser(taskId);
        processUtil.batchAddCandidateUser(taskId, userId);
        return wfTaskflow;
    }

    @Transactional
    public WfTaskflow checkInUnDoTask(String taskId, String userId) {
        if (StrUtil.isEmpty(taskId)) {
            throw new ServiceException("任务Id不能为空，请确认!");
        }
        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException("指定的待办任务不存在，请确认!");
        }
        if (!Constants.TASK_STATE_RUNING.equals(wfTaskflow.getWorkState())) {
            throw new ServiceException("指定的任务为非待办状态，退回领用失败！");
        }
        if (wfTaskflow.getCheckInFlag().equals(Constants.TRAN_TYPE_CHECKOUT_NOTIN)) {
            throw new ServiceException("指定的任务不支持退回领用！");
        }
        // 根据任务池状态来判断
        if (!wfTaskflow.getTaskPoolFlag().equals(Constants.IS_HIST_TASK_POOL)) {
            throw new ServiceException("指定的任务为非领用状态，退回领用失败！");
        }

        if (!StrUtil.equals(wfTaskflow.getTaskActors(), userId)) {
            throw new ServiceException("用户[" + userId + "]没有领用任务[" + taskId + "]的权限！");
        }

        //检入时把任务标记为待领用
        wfTaskflow.setCheckInFlag(Constants.TRAN_TYPE_UNDO_CHECKOUT);

        //重置为任务池任务、方便二次领用，检出，流转判断
        wfTaskflow.setTaskPoolFlag(Constants.IS_TASK_POOL);
        wfTaskflow.setCheckOutTime(LocalDateTime.now());
        JSONObject jsonobj = JSONObject.parseObject(wfTaskflow.getTaskActorsData());
        Set<String> users = jsonobj.keySet();
        StringBuffer buf = new StringBuffer();
        for (String s : users) {
            buf.append(SymbolConstants.COMMA).append(s);
        }
        String assigner = buf.toString();
        wfTaskflow.setTaskActors(assigner.substring(1));
        wfTaskflowService.updateById(wfTaskflow);
        //清理任务的人员信息
        processUtil.deleteCandidateUser(taskId);
        processUtil.batchAddCandidateUser(taskId, assigner);
        return wfTaskflow;
    }

    /**
     * 待检出(领用)分配任务批量指定处理人员接口
     * <pre>
     * 对于需检出但还未被检出的待办任务，批量指定处理的人员，对于任务池、任务池历史中的未领用、已领用任务，权限管理人员可进行强制分配或二次分配处理同时，此接口可支持待领用处理人员的批量领用
     * </pre>
     *
     * @param userId     操作人员id
     * @param userIdMap  指定的处理用户Map
     * @param taskIdList 待领用/分配 的未处理任务列表信息 ["12345","23456"]
     * @param type       交易类型 0-批量领用 1-批量任务池任务分配 2-批量任务池历史任务分配（二次分配）
     * @return
     */
    @Transactional
    public void taskBatchCheckIn(String userId, Map<String, Object> userIdMap, List<String> taskIdList, String type) {

        if (Objects.isNull(type)) {
            throw new ServiceException("交易类型tranType不能为空[0-批量领用 ,1-批量任务池任务分配 ,2-批量任务池历史任务分配（二次分配）]，请检查后重试!");
        }
        // 批量领用,遍历所有的任务id，对每一个任务进行领用
        if (StrUtil.equals(type, Constants.BATCH_CHECK_OUT)) {
            for (String taskIdStr : taskIdList) {
                if (Objects.isNull(userId)) {
                    userId = (String) userIdMap.get(Constants.USERID_KEY);
                }
                checkOutUnDoTask(taskIdStr, userId, userIdMap);
            }
            // 批量分配
        } else {

            if (Objects.isNull(userIdMap)) {
                throw new ServiceException("指定的处理用户不能为空，请检查后重试");
            }
            // 判断上送的任务列表中是否已经存在已办任务等
            List<WfTaskflow> list = wfTaskflowService.selectByTaskIds(taskIdList);
            Map<String, Object> userIdMap1 = this.queryUserList(userIdMap);
            if (list == null || taskIdList.size() != list.size()) {
                throw new ServiceException("部分任务不存在，请检查后重试");
            }
            // 批量分配任务
            allotTaskList(list, userIdMap1, userId, type);
        }

    }

    /**
     * 复制检出，更新检出人信息
     *
     * @param taskId
     * @param userIdMap
     * @return
     * @throws Exception
     */
    @Transactional
    public WfTaskflow checkOutUnDoTask(String taskId, Map<String, Object> userIdMap) throws Exception {
        String userId = (String) userIdMap.get(Constants.USERID_KEY);

        if (StrUtil.isEmpty(taskId)) {
            throw new ServiceException("任务Id不能为空，请确认!");
        }
        WfTaskflow wfTaskflow = selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException("指定的待办任务不存在，请确认!");
        }
        if (!Constants.TASK_STATE_RUNING.equals(wfTaskflow.getWorkState())) {
            throw new ServiceException("指定的任务为非待办状态，领用失败！");
        }
        if (wfTaskflow.getCheckInFlag().equals(Constants.TRAN_TYPE_CHECKOUT_NOTIN)) {
            throw new ServiceException("指定的任务不支持领用！");
        }
        if (!wfTaskflow.getCheckInFlag().equals(Constants.TRAN_TYPE_CHECKOUT)) {
            throw new ServiceException("指定的任务为非检出状态，领用失败！");
        }

        //判断流程是否允许检入
        wfTaskflow.setCheckInFlag(Constants.TRAN_TYPE_CHECKIN);
        wfTaskflow.setCheckOutTime(LocalDateTime.now());

        if (userIdMap != null && !userIdMap.isEmpty()) {
            JSONObject taskActorsData = JSONObject.parseObject(wfTaskflow.getTaskActorsData());
            JSONObject userdate = taskActorsData.getJSONObject(userId);
            if (userdate != null) {
                for (Map.Entry<String, Object> entry : userIdMap.entrySet()) {
                    userdate.put(entry.getKey(), entry.getValue());
                }
            } else {
                wfTaskflow.setTaskActors(wfTaskflow.getTaskActors() + SymbolConstants.COMMA + userId);
                taskActorsData.put(userId, userIdMap);
            }
            wfTaskflow.setTaskActorsData(taskActorsData.toJSONString());
        }
        processUtil.deleteCandidateUser(taskId, userId);
        processUtil.batchAddCandidateUser(taskId, userId);
        wfTaskflowService.updateById(wfTaskflow);
        return wfTaskflow;
    }

    /**
     * 按照规则或者任务id查询人员信息
     *
     * @param
     * @return
     * @throws Exception
     * @author anmingtao
     * @serialData 2019-05-05
     */
    public List<UserOutputData> queryDoUserInfo(WorkFlowTaskTransferData data) throws Exception {
        if ((Objects.isNull(data.getUruleMap()) || data.getUruleMap().isEmpty()) && Objects.isNull(data.getTaskId())) {
            throw new ServiceException("操作失败。uruleMap或taskId必须有一个非空！");
        }
        //2020-03-23 modify by amt start 没有报文头
        WfVariableUtil.setsysCommHead(data.getSysCommHead());
        //2020-03-23 modify by amt end 没有报文头
        Map<String, Object> variableMap = data.getBussinessMap();
        if (Objects.nonNull(data.getTaskId())) {
            WfTaskflow wfTaskflow = selectByTaskId(data.getTaskId());
            Map<String, Object> map = BpmStringUtil.jsonToMap(wfTaskflow.getBussinessMap());
            if (Objects.isNull(map)) {
                map = new HashMap<String, Object>();
            }
            map.putAll(variableMap);
            FlowNode element = (FlowNode) activitiBaseServer.getBpmnModelNodeByTaskId(data.getTaskId());
            List<UserOutputData> userList = wfCommService.findNodeUsers(element, variableMap);
            return userList;
        } else {
            List<UserOutputData> list = wfCommService.findNodeUsers(data.getUruleMap(), variableMap);
            return list;
        }
    }

    /**
     * 批量分配任务
     *
     * @param list      上送的任务信息列表
     * @param userIdMap 分配目标人员
     * @param userId    操作人员
     * @param type      分配类型：1分配、2强制分配
     * @throws Exception 分配失败原因
     * @author anmingtao
     * @serialData 2019-04-27
     **/
    private void allotTaskList(List<WfTaskflow> list, Map<String, Object> userIdMap, String userId, String type) {
        //检查任务列表里面的任务是否可以被分配
        checkAllotTaskExceed(list, userIdMap, type);
        for (WfTaskflow wf : list) {
            String doResult = "分配转发";
            String doRemark = wf.getTaskId() + "被" + userId + "分配转发";
            if (Objects.nonNull(wf.getDoTaskActor())) {
                doRemark = wf.getTaskId() + "被" + userId + "二次分配转发";
            }
            Map<String, Object> usermaps = new HashMap<String, Object>();
            usermaps.put((String) userIdMap.get(Constants.USERID_KEY), userIdMap);
            this.pritransferAssignee(wf.getTaskId(), (String) userIdMap.get(Constants.USERID_KEY), userId, doResult, 3,
                    doRemark, userId, usermaps);
        }
    }

    /**
     * 检查任务列表里面的任务是否可以被分配
     *
     * @param list      上送的任务列表
     * @param userIdMap 被分配的人员信息
     * @param type      1分配，2强制分配
     * @throws ServiceException 分配失败的原因
     * @author anmingtao
     * @serialData 2019-04-27
     */
    private void checkAllotTaskExceed(List<WfTaskflow> list, Map<String, Object> userIdMap, String type) {
        Map<String, Object> param = new HashMap<String, Object>();
        Map<String, TaskCodeCount> tmp = new HashMap<String, TaskCodeCount>();
        List<String> taskCodeList = new ArrayList<String>();
        for (WfTaskflow task : list) {
            //根据type判断是否强制分配，如果强制分配不做领用检查
            if (StrUtil.equals(type, Constants.BACTH_NOMAL_ALLOT)) {
                if (StrUtil.equals(task.getTaskPoolFlag(), Constants.IS_HIST_TASK_POOL)) {
                    throw new ServiceException("此任务" + task.getTaskId() + "已被领用，请刷新界面，重新分配其他任务。");
                }
            }
            //判断加上领用的节点数是否超过最大领用数，如果超过则不能领用
            taskCodeList.add(task.getTaskCode());
            if (tmp.containsKey(task.getTaskCode())) {
                TaskCodeCount tcc = tmp.get(task.getTaskCode());
                tcc.setNum(tcc.getNum().add(BigInteger.ONE));
            } else {
                TaskCodeCount tcc = new TaskCodeCount();
                tcc.setTaskCode(task.getTaskCode());
                tcc.setNum(BigInteger.ONE);
                tcc.setTaskName(task.getTaskName());
                // 直接从缓存里面查询，因此没有必要一次查出来
                WfModelPropExtends mode = wfModelPropExtendsService.selectByOwnIdPropName(task.getTaskCode(),
                        ModelProperties.checkInOut.getName());
                if (mode == null) {
                    throw new ServiceException("不能分配不是任务池的任务");
                }
                Map<String, Object> map = BpmStringUtil.jsonToMap(mode.getPropValue());
                //判断分配的任务是否超过上限
                log.info("| - ProcessCoreServiceImpl>>>>>{}->{}", tcc.getNum(), BigInteger.valueOf((int) map.get(Constants.MAX_CHECK_NUM)));
                if (tcc.getNum().intValue() > (BigInteger.valueOf((int) map.get(Constants.MAX_CHECK_NUM))).intValue()) {
                    throw new ServiceException("任务超过可处理个数，请重新分配");
                }
                tcc.setMaxCount(BigInteger.valueOf((int) map.get(Constants.MAX_CHECK_NUM)));
            }
        }
        param.put("taskCodeList", taskCodeList);
        param.put(Constants.USERID_KEY, userIdMap.get(Constants.USERID_KEY));
        //分配的数量+已经领用的代办数量如果超过限制则不能领用
        List<TaskCodeCount> undolist = wfTaskflowService.selectTaskCodeCount(param);
        for (TaskCodeCount task : undolist) {
            TaskCodeCount old = tmp.get(task.getTaskCode());
            if (old != null && (old.getNum().add(task.getNum())).compareTo(task.getMaxCount()) > 0) {
                throw new ServiceException("操作失败。该员工待办任务已达上限");
            }
        }

    }


    public void saveViewTaskLog(String taskId, String userId) {
        WfTaskflow wfTaskflow = wfTaskflowService.selectByTaskId(taskId);
        if (wfTaskflow == null) {
            throw new ServiceException("指定的待办任务不存在，请确认!");
        }
        this.saveViewTaskLog(wfTaskflow, userId, null, null);
    }

    private void saveViewTaskLog(WfTaskflow wfTaskflow, String userId, String userName, String userInfo) {
        WfViewTaskLog condition = new WfViewTaskLog();
        condition.setTaskId(wfTaskflow.getTaskId());
        condition.setUserId(userId);
        WfViewTaskLog log = wfViewTaskLogService.selectByTaskIdUserId(condition);
        LocalDateTime now = LocalDateTime.now();
        if (Objects.isNull(log)) {
            log = new WfViewTaskLog();
            log.setUserId(userId);
            log.setTaskId(wfTaskflow.getTaskId());
            log.setViewCount(1);
            log.setViewCurTime(now);
            log.setViewFirstTime(now);
            if (Objects.isNull(userName)) {
                Map<String, Object> userMap = BpmStringUtil.jsonToMap(wfTaskflow.getTaskActorsData());
                if (userMap != null) {
                    JSONObject jsonObject = (JSONObject) userMap.get(userId);
                    if (Objects.isNull(jsonObject)) {
                        throw new ServiceException(userId + "没有权限查看任务" + wfTaskflow.getTaskId());
                    }
                    userName = jsonObject.getString("userName");
                    log.setUserName(userName);
                    if (Objects.isNull(userInfo)) {
                        userInfo = jsonObject.toJSONString();
                    }
                }
            } else {
                log.setUserName(userName);
            }
            if (Objects.isNull(userInfo)) {
                Map<String, Object> userMap = BpmStringUtil.jsonToMap(wfTaskflow.getTaskActorsData());
                if (userMap != null) {
                    JSONObject jsonObject = (JSONObject) userMap.get(userId);
                    log.setUserInfo(jsonObject.toJSONString());
                }
            } else {
                log.setUserInfo(userInfo);
            }
            wfViewTaskLogService.insertWfViewTaskLog(log);
        } else {
            log.setViewCount(log.getViewCount()+1);
            log.setViewCurTime(now);
            wfViewTaskLogService.updateByTaskIdUserId(log);
        }


    }

    /**
     * 针对待人工分配的待办任务，可根据需要，指定对应处理人范围的待分配的任务，批量进行自动分配处理。
     */
    public Map<String, Object> userAllotTaskListManage(WorkFlowTaskTransferData data) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put(Constants.USERID_KEY, data.getUserId());
        param.put("processCodeList", data.getProcessCodeList());
        param.put("nodeIdCodeList", data.getNodeIdCodeList());
        param.put("processIdList", data.getProcessIdList());
        param.put("bussinessList", data.getBussinessList());
        param.put("startTime", data.getStartTime());
        param.put("endTime", data.getEndTime());
        List<WfTaskflow> list = wfTaskflowService.selectUndoTaskFlow(param);
        Map<String, Object> param1 = new HashMap<String, Object>();
        param1.put("validFlag", "1");
        param1.put("type", "2");
        param1.put("sendId", new String[]{data.getUserId()});
        param.put("currentTime", new Date());
        List<WfAcceditRecord> accediteList = wfAcceditRecordService.selectList(param1);
        Map<String, WfModelPropExtends> propMap = wfModelPropExtendsService.getAutoAllotExtends();
        List<WfTaskflow> record = new ArrayList<WfTaskflow>();
        List<String> taskList = new ArrayList<String>();
        if (Objects.isNull(list) || list.isEmpty()) {
            throw new ServiceException("找不到符合条件的代办任务");
        }
        for (WfTaskflow w : list) {
            // 代办任务查询时，当节点为true，遍历人员是否符合自动分配，如果复合则设置isAutoAllot=true
            //如果为false检查流程是否配置，如果流程配置为true遍历人员是否符合自动分配，如果复合则设置isAutoAllot=true
            if (propMap.containsKey(w.getTaskCode())) {
                WfModelPropExtends prop = propMap.get(w.getTaskCode());
                if ("true".equals(prop.getPropValue())) {
                    for (WfAcceditRecord a : accediteList) {
                        if (w.getTaskActors().contains(a.getSendId())) {
                            record.add(w);
                            taskList.add(w.getTaskId());
                            break;
                        }
                    }
                } else {
                    if (propMap.containsKey(w.getProcessCode().split(SymbolConstants.COLON)[0])) {
                        prop = propMap.get(w.getProcessCode().split(SymbolConstants.COLON)[0]);
                        if ("true".equals(prop.getPropValue())) {
                            for (WfAcceditRecord a : accediteList) {
                                if (w.getTaskActors().contains(a.getSendId())) {
                                    record.add(w);
                                    taskList.add(w.getTaskId());
                                    break;
                                }
                            }
                        }
                    }

                }
            }
            //代办任务查询时，当节点为true，遍历人员是否符合自动分配，如果复合则设置isAutoAllot=true
            //如果为false检查流程是否配置，如果流程配置为true遍历人员是否符合自动分配，如果复合则设置isAutoAllot=true
        }
        if (Objects.isNull(record) || record.isEmpty()) {
            throw new ServiceException("找不到符合条件的代办任务");
        }

        BatchInvockNextTask batche = new BatchInvockNextTask(data, record);
        timerUtil.addWorkflowTimerTask(batche, -1, -1, -1, -1, -1, 1);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("taskNum", record.size());
        map.put("taskList", taskList);
        return map;
    }

    /**
     * @Description: 检查单个任务的处理人员重复匹配情况
     * @Param: [processId, tasksUser, taskCode, recvCommUserList]
     * @Return: int
     */
    public int sameExecutorCheck(String processId, String tasksUser, String taskCode, List<String> recvCommUserList) {
        // 查询流程实例并获取所有审批人员
        WfInstance wfInstance = wfInstanceService.selectByProcessId(processId);
        String passUsers = wfInstance.getJoinUserIds();
        String processCode = wfInstance.getProcessCode();
        String processCodeSub = processCode.substring(0, processCode.indexOf(SymbolConstants.COLON));

        // 判断流程是否需要自动执行:先判断流程，有配置按流程处理，没配置在看节点
        int autoExectFlag = -1;
        WfModelPropExtends wfModelPropExtends = wfModelPropExtendsService.selectByOwnIdPropName(processCodeSub,
                ModelProperties.flowAutoRepeatProcess.getName());
        if (wfModelPropExtends != null) {
            // propValue的取值：0-不自动处理，1-部分人员适配处理，2-全部人员适配处理
            String value = JSON.parseObject(wfModelPropExtends.getPropValue()).getString("type");
            if (BpmConstants.FLOW_AUTO_REPEAT_PROCESS_0.equals(value) || BpmConstants.FLOW_AUTO_REPEAT_PROCESS_1.equals(value)
                    || BpmConstants.FLOW_AUTO_REPEAT_PROCESS_2.equals(value)) {// 不自动执行
                autoExectFlag = Integer.parseInt(value);
            } else {
                String msg = "流程【" + processCode + "】的自动流转扩展属性配置错误！！";
                log.info(msg);
                throw new ServiceException(msg);
            }
        }

        // 判断节点是否需要自动执行
        if (autoExectFlag < 0) {
            wfModelPropExtends = wfModelPropExtendsService.selectByOwnIdPropName(taskCode,
                    ModelProperties.nodeAutoRepeatProcess.getName());
            if (wfModelPropExtends != null) {
                String value = JSON.parseObject(wfModelPropExtends.getPropValue()).getString("type");
                if (BpmConstants.FLOW_AUTO_REPEAT_PROCESS_0.equals(value) || BpmConstants.FLOW_AUTO_REPEAT_PROCESS_1.equals(value)
                        || BpmConstants.FLOW_AUTO_REPEAT_PROCESS_2.equals(value)) {// 不自动执行
                    autoExectFlag = Integer.parseInt(value);
                } else {
                    String msg = "节点【" + wfModelPropExtends.getOwnId() + "】的自动流转扩展属性配置错误！！";
                    log.info(msg);
                    throw new ServiceException(msg);
                }
            }
        }

        // 无论流程 还是节点，必须要满足自动流转的配置，否则不能继续自动流转
        if (autoExectFlag <= 0) {
            return autoExectFlag;
        }

        // 匹配处理人员和待办人员
        String[] passUsersArry = StrUtil.splitToArray(passUsers, SymbolConstants.COMMA);
        String[] taskUsersArry = StrUtil.splitToArray(tasksUser, SymbolConstants.COMMA);
        if (recvCommUserList == null) {
            recvCommUserList = new ArrayList<String>();
        }
        for (String pass : passUsersArry) {
            for (String actor : taskUsersArry) {
                if (pass.equals(actor)) {
                    recvCommUserList.add(pass);
                }
            }
        }

        if (autoExectFlag == BpmConstants.AUTO_EXEC_FLAG_2 && recvCommUserList.size() != taskUsersArry.length) {// 部分人员匹配成功，但配置要求全匹配
            autoExectFlag = BpmConstants.AUTO_EXEC_FLAG_3;
        }
        return autoExectFlag;
    }


    @Transactional
    public void taskManage(WorkFlowTaskTransferData taskData) {
        String userId = taskData.getUserId();

        String tranType = taskData.getTranType();
        String taskId = taskData.getTaskId();
        String processInstId = taskData.getProcessInstId();
        if (StrUtil.isEmpty(taskId) && StrUtil.isEmpty(processInstId)) {
            throw new ServiceException("管理人员任务管理【taskId】【processInstId】不能同时为空");
        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_CANCEL)) {
            cancelTask(taskId, userId);
        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_HANGUP)) {
            // 任务挂起
            if (StrUtil.isNotBlank(taskId)) {
                suspendTask(taskId, false, taskData.getAgentUserId());
            } else if (StrUtil.isNotBlank(processInstId)) {
                // 流程实例挂起
                suspendProcessInstance(taskData.getProcessCode(), processInstId, null);
            }
        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_DELETE)) {
            endProcessInstance(taskData, userId);

        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_HANGDOWN)) {
            // 任务解除挂起
            if (StrUtil.isNotBlank(taskId)) {
                suspendTask(taskId, true, taskData.getAgentUserId());
            } else if (StrUtil.isNotBlank(processInstId)) {
                // 流程实例解除挂起
                activateProcessInstance(taskData.getProcessCode(), processInstId, null);
            }

        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_USERCHG)) {
            modifyTaskActors(taskId, userId, taskData.getAgentUserId());
        }
        if (StrUtil.equals(tranType, Constants.TRANTYPE_TIMECHG)) {
            extendTaskEndDate(taskId, processInstId, taskData.getEstEndDate());
        }
        WfVariableUtil.clearfVariable();
    }

    /**
     * 更新业务数据
     *
     * @param businessDataUpdateRequest
     */
    @SuppressWarnings("unchecked")
    public void updateBusinessData(BusinessDataUpdateRequest businessDataUpdateRequest) {
        // 根据任务编号查询任务
        WfTaskflow wfTaskflow = selectByTaskId(businessDataUpdateRequest.getTaskId());
        if (wfTaskflow == null) {
            throw new ServiceException("找不到对应的任务");
        }
        if (!wfTaskflow.getTaskActors().contains(businessDataUpdateRequest.getUserId())) {
            throw new ServiceException("该用户没有处理该任务[" + businessDataUpdateRequest.getTaskId() + "]的权限");
        }
        // 业务数据-新
        Map<String, Object> dataMapNew = businessDataUpdateRequest.getDataMap();
        // 查询流程数据
        WfBusinessData wfBusinessData = wfBusinessDataService.selectById(wfTaskflow.getId().toString());
        String businessMapStr = wfBusinessData.getBussinessMap();
        cn.hutool.json.JSONObject jsonObject = JSONUtil.parseObj(businessMapStr);
        Map<String, Object> businessMap = jsonObject.toBean(Map.class);
        if (cn.hutool.core.util.ObjectUtil.isNotEmpty(businessMap)) {
            // 业务数据-旧
            Map<String, Object> dataMapOld = (Map<String, Object>) businessMap.get("dataMap");
            if (cn.hutool.core.util.ObjectUtil.isEmpty(dataMapOld)) {
                dataMapOld = new HashMap<>();
            }
            dataMapOld.putAll(dataMapNew);
            businessMap.put("dataMap", dataMapOld);
            wfBusinessData.setBussinessMap(JSONUtil.toJsonStr(businessMap));

            // 根据事项小类查询业务配置字段
            String systemIdCode = wfTaskflow.getItemSecondCode();
            List<ParamShowConfig> busiFieldList = paramShowConfigService.lambdaQuery()
                    .eq(StrUtil.isNotBlank(systemIdCode), ParamShowConfig::getSystemIdCode, systemIdCode)
                    .eq(ParamShowConfig::getType, BpmConstants.SHOW_CONFIG_0)
                    .list();
            // 根据配置展示内容修改bussinessSave的内容
            if (cn.hutool.core.util.ObjectUtil.isNotEmpty(busiFieldList)) {
                ParamShowConfig paramShowConfig = busiFieldList.get(0);
                // 配置信息转JSON
                cn.hutool.json.JSONArray contentArray = JSONUtil.parseArray(paramShowConfig.getContent());
                Map<String, Object> businessSave = new HashMap<>();
                for (Object content : contentArray) {
                    // 解析配置内容
                    cn.hutool.json.JSONObject contentObj = JSONUtil.parseObj(content);
                    String targetKey = (String) contentObj.get(BpmConstants.TARGET_KEY);
                    String sourceKey = (String) contentObj.get(BpmConstants.SOURCE_KEY);
                    if (dataMapOld.containsKey(sourceKey)) {
                        businessSave.put(targetKey.replace("_", ""), dataMapOld.get(sourceKey));
                    }
                }
                BeanUtil.copyProperties(businessSave, wfTaskflow);
            }
        }
        // 更新流程信息
        wfTaskflowService.updateById(wfTaskflow);
        wfBusinessDataService.updateById(wfBusinessData);
    }

    /**
     * <p>插入流程业务关联表</p>
     *
     */
    public void insertProcessBusinessRel(String systemIdCode, Object businessId, String processId) {
        WfProcessBusinessRel wfProcessBusinessRel = new WfProcessBusinessRel();
        try {
            wfProcessBusinessRel.setBusinessId(String.valueOf(businessId));
            wfProcessBusinessRel.setParamCode(systemIdCode);
            wfProcessBusinessRel.setProcessId(processId);
            wfProcessBusinessRelMapper.insert(wfProcessBusinessRel);
            log.info("systemIdCode:{},businessId:{},processId:{}", systemIdCode, businessId, processId);
        } catch (Exception e) {
            log.error("insertProcessBusinessRel.error:", e);
        }
    }

    /**
     * <p>通过taskId查proceeId</p>
     */
    public String queryProcessIdByTaskId(String taskId) {
        WfTaskflow wf = wfTaskflowService.selectByTaskId(taskId);
        if (cn.hutool.core.util.ObjectUtil.isNotEmpty(wf)) {
            return wf.getProcessId();
        }
        return null;
    }

}
