package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.service.CacheDaoSvc;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Element;
import java.math.BigInteger;

@Getter
@Setter
@Log4j2
public class TaskTimoutInfo {

    public static TaskTimoutInfo getTaskTimeoutInfo(WfModelPropExtendsService wfModelpropExtendsService, CacheDaoSvc cacheDaoSvc,
                                                    String processDefId, BigInteger version, String taskDefId) {
        try {
            TaskTimoutInfo taskTimoutInfo = new TaskTimoutInfo();
            // 查询节点配置
            Element taskElement = cacheDaoSvc.getTaskElementByDefIdActVersionTaskDefId(processDefId, version, taskDefId);
            // 节点配置
            if (taskElement == null) {
                return null;
            }
            taskTimoutInfo.taskElement = taskElement;
            // 流程配置超时时间
            String timeOut = taskElement.attributeValue(BpmConstants.TIME_OUT);
            if (StrUtil.isEmpty(timeOut) || BpmConstants.TIME_OUT_0.equals(timeOut.trim())) {

                String nodeId = taskElement.attributeValue("sysNodeId");

                if(StrUtil.isEmpty(nodeId)){
                    return null;
                }
                // 查询节点配置的超时配置信息
                WfModelPropExtends wfModelPropExtends = wfModelpropExtendsService.selectByOwnIdPropName(nodeId, ModelProperties.timeoutExecution.getName());

                if (wfModelPropExtends != null && StrUtil.isNotEmpty(wfModelPropExtends.getPropValue())) {

                    JSONObject timeoutExecutionJb = JSONObject.parseObject(wfModelPropExtends.getPropValue());
                    // 超时时间
                    if (timeoutExecutionJb.get(BpmConstants.TIMEOUT_TIME) != null) {
                        taskTimoutInfo.overtimeTime = timeoutExecutionJb.getIntValue(BpmConstants.TIMEOUT_TIME);
                    }
                    // 超时时间单位 0-时 1-天
                    if (timeoutExecutionJb.get(BpmConstants.TIME_UNIT) != null) {
                        taskTimoutInfo.overtimeTimeUnit = timeoutExecutionJb.getString(BpmConstants.TIME_UNIT);
                    }
                    // 超时处理机制
                    if (timeoutExecutionJb.get(BpmConstants.OVERTIME_EXECUTION) != null) {
                        taskTimoutInfo.overtimeExecution = TimeoutExecution.getTimeoutExecutionByValue(timeoutExecutionJb.getString(BpmConstants.OVERTIME_EXECUTION));
                    }
                    taskTimoutInfo.execVariable = taskElement.attributeValue("timeFlowFlag");
                }
                // 查询节点配置的超时预警配置信息
                wfModelPropExtends = wfModelpropExtendsService.selectByOwnIdPropName(nodeId, ModelProperties.timeoutPreWarnExecution.getName());

                if (wfModelPropExtends != null && StrUtil.isNotEmpty(wfModelPropExtends.getPropValue())) {

                    JSONObject timeoutExecutionJb = JSONObject.parseObject(wfModelPropExtends.getPropValue());
                    // 超时时间
                    if (timeoutExecutionJb.get(BpmConstants.TIMEOUT_TIME) != null) {
                        taskTimoutInfo.overtimePreWarnTime = timeoutExecutionJb.getIntValue(BpmConstants.TIMEOUT_TIME);
                    }
                    // 超时时间单位 0-时 1-天
                    if (timeoutExecutionJb.get(BpmConstants.TIME_UNIT) != null) {
                        taskTimoutInfo.overtimePreWarnTimeUnit = timeoutExecutionJb.getString(BpmConstants.TIME_UNIT);
                    }
                    // 消息模板编号
                    if (timeoutExecutionJb.get(BpmConstants.MSG_TEMPLATE_NO) != null) {
                        taskTimoutInfo.overtimePreWarnTemplate = timeoutExecutionJb.getString(BpmConstants.MSG_TEMPLATE_NO);
                    }
                }
            }
            return taskTimoutInfo;
        } catch (Exception e) {
            log.error("| - TaskTimoutInfo>>>>>组装TaskTimoutInfo对象失败！", e);
            return null;
        }

    }

    public String getExecVariable() {
        if (StrUtil.isEmpty(execVariable)) {
            return null;
        }
        if (!execVariable.contains(SymbolConstants.SEMICOLON)) {
            return execVariable;
        }
        String[] theVariables = execVariable.split(SymbolConstants.SEMICOLON);

        if (overtimeExecution == TimeoutExecution.doNextNormal) {
            if (StrUtil.isEmpty(theVariables[0])) {
                return null;
            } else {
                return theVariables[0];
            }
        } else {
            if (StrUtil.isEmpty(theVariables[1])) {
                return null;
            } else {
                return theVariables[1];
            }
        }
    }

    /**
     * 超时预警时间
     */
    private int overtimePreWarnTime;
    /**
     * 超时预警时间单位 0-时 1-天
     */
    private String overtimePreWarnTimeUnit;
    /**
     * 超时预警模板
     */
    private String overtimePreWarnTemplate;

    /**
     * 超时时间
     */
    private int overtimeTime;
    /**
     * 超时时间单位 0-时 1-天
     */
    private String overtimeTimeUnit;
    /**
     * 超时处理机制
     */
    private TimeoutExecution overtimeExecution;

    private String processDefId;
    private BigInteger version;
    private String taskDefId;
    private String execVariable;
    private Element taskElement;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("===========TaskTimoutInfo=============").append(System.lineSeparator());
        sb.append("processDefId").append(processDefId).append("  ");
        sb.append(" version").append(version).append("  ");
        sb.append(" taskDefId").append(taskDefId).append("  ");
        sb.append(" execVariable").append(execVariable).append("  ");
        sb.append(" taskElement").append(JSONUtil.toJsonStr(taskElement)).append("  ");

        sb.append("overtimePreWarnTime").append(overtimePreWarnTime).append("  ");
        sb.append("overtimePreWarnTimeUnit").append(overtimePreWarnTimeUnit).append("  ");
        sb.append("overtimePreWarnTemplate").append(overtimePreWarnTemplate).append("  ");

        sb.append("overtimeTime").append(overtimeTime).append("  ");
        sb.append("overtimeTimeUnit").append(overtimeTimeUnit).append("  ");
        sb.append("overtimeExecution").append(overtimeExecution);
        return sb.toString();
    }
}
