package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfExceptionDo;
import com.bpm.workflow.mapper.WfExceptionDoMapper;
import com.bpm.workflow.service.WfExceptionDoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfExceptionDoServiceImpl extends ServiceImpl<WfExceptionDoMapper, WfExceptionDo> implements WfExceptionDoService {

}
