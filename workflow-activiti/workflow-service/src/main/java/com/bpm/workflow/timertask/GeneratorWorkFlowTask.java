package com.bpm.workflow.timertask;

import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.service.WfInstanceService;
import com.bpm.workflow.service.WfTaskflowService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class GeneratorWorkFlowTask extends WorkflowTimerTask {
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private WfInstanceService wfInstanceService;
    private static final long serialVersionUID = 1L;

    private WfTaskflow taskFlow;

    public GeneratorWorkFlowTask(WfTaskflow taskFlow) {

        this.taskFlow = taskFlow;
    }

    @Override
    public void runImpl() throws Exception {
        WfInstance wfInstance = wfInstanceService.selectByProcessId(taskFlow.getProcessId());
        if (wfInstance != null) {
            taskFlow.setSystemCode(wfInstance.getSystemCode());
        }
        if (log.isInfoEnabled()) {
			log.info("| - GeneratorWorkFlowTask>>>>>开始保存节点执行信息[taskFlow={}]", taskFlow);
        }
        wfTaskflowService.insertWfTaskflow(taskFlow);
		log.error("| - GeneratorWorkFlowTask>>>>>完成[taskId={}]保存节点执行信息", taskFlow.getTaskId());
    }

}
