package com.bpm.workflow.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.system.SysUserRole;
import com.bpm.workflow.mapper.system.SysUserRoleMapper;
import com.bpm.workflow.service.system.SysUserRoleService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {
    @Resource
    SysUserRoleMapper mapper;
    /**
     * 根据用户id查询角色信息
     * @param userId
     * @return
     */
    @Override
    public SysUserRole getRoleByUserId(String userId) {
        //构造查询条件
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUserRole::getUserId, userId);
        //执行查询
        return mapper.selectOne(queryWrapper);
    }
}
