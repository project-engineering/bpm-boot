package com.bpm.workflow.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.SysUserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    SysUserRole getRoleByUserId(String userId);
}
