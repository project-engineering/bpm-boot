package com.bpm.workflow.service.common;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.QueryRelationTellerFactory;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.dto.transfer.WorkFlowStartData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskTransferData;
import com.bpm.workflow.engine.listener.SequenceFlowCadition;
import com.bpm.workflow.entity.WfDefinePropExtends;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.entity.WfTemplate;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfModelPropExtendsMapper;
import com.bpm.workflow.service.*;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.*;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.script.*;
import java.math.BigInteger;
import java.util.*;


/**
 * @Description: 流程流转过程中的公共方法
 */
@Log4j2
@Service
public class WfCommService {

    @Resource
    private QueryRelationTellerFactory queryRelationTellerFactory;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private WfTemplateService wfTemplateService;
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private ActivitiBaseService activitiBaseService;
    @Resource
    private ProcessCoreService processCoreService;
    @Resource
    private WfModelPropExtendsService wfModelpropExtendsService;
    @Resource
    private WfNodePropertyService wfNodePropertyService;
    @Resource
    private UruleServiceFactory urule;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private WfModelPropExtendsMapper modelPropExtendsMapper;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfDefinePropExtendsService wfDefinePropExtendsService;
    @Resource
    protected WfEvtServiceDefService wfEvtServiceDefService;
    @Resource
    private SequenceFlowCadition sequenceFlowCadition;

    /**
     * 选取节点的执行人员
     *
     * @param element
     * @param variableMap
     * @return
     */
    public List<UserOutputData> findNodeUsers(FlowElement element, Map<String, Object> variableMap) {
        return findNodeUsers(null, null, null, variableMap, element);
    }

    /**
     * 选取节点的执行人员
     *
     * @param element
     * @return
     * @throws Exception
     */
    public List<UserOutputData> findNodeUsers(DelegateTask delegateTask, FlowElement element) throws Exception {
        Map<String, Object> variableMap = delegateTask.getExecution().getVariables();

        return this.findNodeUsers(delegateTask.getId(), delegateTask.getProcessDefinitionId(),
                delegateTask.getProcessInstanceId(), variableMap, element);
    }

    /**
     * 获取当前分支流程
     *
     * @param element
     * @param beforeTaskId
     * @return
     */
    private String getFlowBranch(FlowElement element, String beforeTaskId) {
        WfTaskflow taskFlow = new WfTaskflow();
        String defId = null;
        FlowNode flowNode = (FlowNode) element;
        if (beforeTaskId != null && !BpmConstants.START_TASK_ID.equals(beforeTaskId)) {
            //查询上一个任务的流程定义id
            taskFlow = wfTaskflowService.selectByTaskId(beforeTaskId);
            //如果是子流程启动的首节点，他的上一个节点为主流程对应的节点，因此需要过滤调，从开始节点找路径
            if (taskFlow != null && !taskFlow.getTaskType().equals(Constants.NODETYPE_SUBPROC)) {
                defId = taskFlow.getTaskDefId();
            }
        }
        List<SequenceFlow> sequenceList = flowNode.getIncomingFlows();
        //当没有找到流程定义id时，说明要么没有传beforeTaskId（异常情况），要么首节点说明这条线时从首节点进入的，返回这条线上的document
        for (SequenceFlow flow : sequenceList) {
            if (defId == null) {
                if (flow.getSourceFlowElement() instanceof StartEvent) {
                    return flow.getDocumentation();
                }
            }
            //当流程定义不为空找到改节点的上一个节点，返回线上的document
            if (defId != null && defId.equals(flow.getSourceFlowElement().getId())) {
                return flow.getDocumentation();
            }
        }
        return null;
    }

    /**
     * 选取节点的执行人员
     *
     * @param element
     * @param variableMap
     * @return
     * @throws Exception
     */
    public List<UserOutputData> findNodeUsers(String taskId, String processDefId, String processInstId,
                                              Map<String, Object> variableMap, FlowElement element) {

        // 得到选人规则的配置
        String modelId = processUtil.getTaskProp(processDefId, element, Constants.NODEPROP_REFID);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
        if (StrUtil.isBlank(modelId)) {
            log.info("| - WfCommServiceImpl>>>>>节点映射为空！");
            return new ArrayList<UserOutputData>();
        }
        String rulePropValue = wfModelpropExtendsService.getModelPropExtendsValue(modelId,
                ModelProperties.userRule.getName());

        if (StrUtil.isBlank(rulePropValue)) {
            Object flowParam = variableMap.get(Constants.ROUTE_SELECT_KEY);
            if (flowParam != null) {
                if (flowParam instanceof JSONArray) {
                    JSONArray array = (JSONArray) flowParam;
                    if (array.size() > 0) {
                        JSONObject obj = array.getJSONObject(0);
                        if (!obj.containsKey(BpmConstants.USE_ASK_LIST) || obj.getJSONArray(BpmConstants.USE_ASK_LIST) == null) {
                            log.info("| - WfCommServiceImpl>>>>>选人规则配置为空{}！！！", obj.toJSONString());
                            return new ArrayList<UserOutputData>();
                        }
                    }
                }

            } else {
                log.info("| - WfCommServiceImpl>>>>>选人规则配置为空！！！");
                return new ArrayList<UserOutputData>();
            }

        }
        log.debug("| - WfCommServiceImpl>>>>>rulePropValue:{}", rulePropValue);
        Map<String, Object> ruleConfigMap = JSONObject.parseObject(rulePropValue);
        // TODO userRule的值还需要处理

        // 获取干系人（信息）列表
        List<Map<String, Object>> userList = getQueryTellerData(variableMap);

        List<Map<String, Object>> ruleList = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> tempMap : userList) {
            Map<String, Object> subMap = new HashMap<String, Object>();
            subMap.put("source", tempMap.get("userId"));
            subMap.put("target", tempMap.get("userType"));
            ruleList.add(subMap);
        }
        if (ruleConfigMap == null) {
            ruleConfigMap = new HashMap<String, Object>();
        }
        ruleConfigMap.put("list", ruleList);
        log.debug("| - WfCommServiceImpl>>>>>ruleList:{}", ruleList);
        log.debug("| - WfCommServiceImpl>>>>>ruleConfigMap:{}", ruleConfigMap);
        // 根据选人策略进行选人
        List<UserOutputData> userDataList = null;
        try {
            String beforeTaskId = (String) variableMap.get(Constants.BEFOR_TASKID);
            userDataList = queryRelationTellerFactory.getInstance().queryTellerByPolicy(taskId, processDefId, processInstId,
                    JSONObject.toJSONString(ruleConfigMap), userList, variableMap, this.getFlowBranch(element, beforeTaskId));

        } catch (ServiceException eb) {
            log.error("| - WfCommServiceImpl>>>>>获取人员信息失败：", eb);
            throw eb;
        } catch (Exception e) {
            log.error("| - WfCommServiceImpl>>>>>获取人员信息失败：", e);
            throw new ServiceException("选人服务调用失败" + e.getMessage());
        }

        return userDataList;
    }

    /**
     * 组装干系人（信息）列表
     *
     * @param variableMap
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> getQueryTellerData(Map<String, Object> variableMap) {
        List<Map<String, Object>> userList = new ArrayList<>();
        // 组装上一节点执行人信息
        Object obj = variableMap.get(Constants.BEFORUSERDATA);
        if (obj != null) {
            JSONObject befMap = BpmStringUtil.mapToJson((Map<String, Object>) obj);
            befMap.put("userType", "3");
            if (variableMap.containsKey(Constants.BEFOR_USERID) && variableMap.get(Constants.BEFOR_USERID) != null) {
                befMap.put("userId", variableMap.get(Constants.BEFOR_USERID).toString());
            }

            userList.add(befMap);
        }

        // 组装流程实例发起人信息
        Object obj1 = variableMap.get(Constants.STARTUSERDATA);
        if (obj1 != null) {
            JSONObject startMap = BpmStringUtil.mapToJson((Map<String, Object>) obj1);
            startMap.put("userType", "2");
            userList.add(startMap);
        }

        // 组装客户经理信息
        Object obj2 = variableMap.get(Constants.CUSTOMERMUSER);
        if (obj2 != null) {
            JSONObject custMaketMap = BpmStringUtil.mapToJson((Map<String, Object>) obj2);
            custMaketMap.put("userType", "0");
            userList.add(custMaketMap);
        }

        // 组装对方客户经理信息
        Object obj3 = variableMap.get(Constants.OPPOSIETUSER);
        if (obj3 != null) {
            JSONObject otherCustMaketMap = BpmStringUtil.mapToJson((Map<String, Object>) obj3);
            otherCustMaketMap.put("userType", "1");
            userList.add(otherCustMaketMap);
        }
        return userList;
    }

    /**
     * 根据信息模板和变量集获取信息的最终内容
     *
     * @param variableMap
     * @return
     */
    public String templatToMsgContent(String templateId, Map<String, Object> variableMap) {
        if (StrUtil.isBlank(templateId)) {
            return "";
        }
        WfTemplate wfTemplate = null;
        try {
            wfTemplate = wfTemplateService.viewTemplate(templateId);
        } catch (Exception e) {
            log.error("| - WfCommServiceImpl>>>>>信息模板记录不存在！", e);
        }
        if (wfTemplate == null) {
            log.info("| - WfCommServiceImpl>>>>>信息模板记录不存在！");
            return "";
        }
        String template = wfTemplate.getContent();
        //消息模板支持流程变量  update by xuesw  20181026  start
        if (StrUtil.isNotBlank(template)) {
            template = getMessageTemplate(template, variableMap);
        }
        //消息模板支持流程变量  update by xuesw  20181026  end
        return BpmStringUtil.templatToMsgContent(template, variableMap);
    }

    /**
     * @param template
     * @param varMap
     * @return
     */
    private String getMessageTemplate(String template, Map<String, Object> varMap) {
        String spiltStr = "%%%";
        int index = template.indexOf(spiltStr);
        String temp = template;
        if (index != -1) {

            // 用正则表达式匹配是否满足格式
            if (!template.matches(SymbolConstants.SYMBOL_013 + spiltStr + SymbolConstants.SYMBOL_014 + spiltStr + SymbolConstants.SYMBOL_015)) {
                throw new ServiceException("无法识别下列消息模板：" + temp);
            }

            // 开始解析消息模板
            String javaScriptStr = null;
            while (index >= 0) {
                // 先截取脚本
                temp = temp.substring(index + spiltStr.length());
                index = temp.indexOf(spiltStr);
                javaScriptStr = temp.substring(0, index);

                // 执行脚本
                try {
                    String jsBoolean = executeJavaScript(javaScriptStr, varMap);
                    temp = temp.substring(javaScriptStr.length() + spiltStr.length());
                    if (Boolean.valueOf(jsBoolean)) {
                        // 在截取 模板值
                        if (temp.indexOf(spiltStr) != -1) {
                            temp = temp.substring(0, temp.indexOf(spiltStr));
                        }
                        return temp;
                    } else {
                        index = temp.indexOf(spiltStr);
                    }
                } catch (Exception e) {
                    log.error("| - WfCommServiceImpl>>>>>计算执行表达式时失败！不执行相关配置", e);
                    throw new ServiceException(e.getMessage());
                }
            }
        }
        return temp;
    }

    /**
     * 获取指定节点的流转出路列表
     *
     * @param flowElement
     * @param variableMap
     * @return Map：key--流转的线的id；value--流转的下一个节点
     * @throws Exception
     */
    public Map<String, FlowElement> routeSelection(String processCode, FlowElement flowElement, Map<String, Object> variableMap, boolean isOutSelect) {
        // TODO强转可能存在问题，后续处理
        FlowNode flowNode = (FlowNode) flowElement;
        List<SequenceFlow> sequenceList = flowNode.getOutgoingFlows();

        Map<String, FlowElement> resultMap = new HashMap<String, FlowElement>();
        if (sequenceList.isEmpty()) {
            throw new ServiceException("流程定义存在问题，请确认！！！");
        } else {

            // 根据路由规则配置调用规则引擎确认出口
            String roultOutValue = "";
            String nodeType = processUtil.getTaskProp(processCode, flowNode, Constants.NODETYPE);//flowNode.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
            if (nodeType == null || !nodeType.equals(Constants.NODE_TYPE_SIGN)) {
                roultOutValue = roultRuleCommService(processCode, flowElement, variableMap);
            }

            if (StrUtil.isNotBlank(roultOutValue) && !isOutSelect) {
                Map<String, Object> tempMap = new HashMap<String, Object>();
                tempMap.put(Constants.ROUTE_CONDITION_KEY, roultOutValue);
                for (SequenceFlow outLine : sequenceList) {
                    if (sequenceFlowCadition.mathcCaditionCommon(tempMap, flowElement, outLine.getId())) {
                        resultMap.put(outLine.getId(), outLine.getTargetFlowElement());
                    }
                }

                if (resultMap.isEmpty()) {
                    log.info("| - WfCommServiceImpl>>>>>无法确认流转路径");
                    throw new ServiceException("无法确认流转路径！！！");
                }
            } else {
                for (SequenceFlow sequenceFlow : sequenceList) {
                    resultMap.put(sequenceFlow.getId(), sequenceFlow.getTargetFlowElement());
                }
            }
        }
        return resultMap;
    }

    /**
     * 会签选人
     *
     * @param execution
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<String> signUserList(DelegateExecution execution) {
        List<String> result = new ArrayList<String>();
        List<UserOutputData> tempList = null;
        Map<String, Object> variableMap = WfVariableUtil.getVariableMap();
        FlowElement flowElement = execution.getCurrentFlowElement();
        String nodeId = processUtil.getTaskProp(execution.getProcessDefinitionId(), flowElement, Constants.NODEPROP_REFID);//execution.getCurrentFlowElement().getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
        List<WfModelPropExtends> extendsPropList = wfModelpropExtendsService.selectModelExtendsPropByOwnId(nodeId);
        // 调用节点进入触发事件
        WfModelPropExtends startEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.startTaskEvent.getName());
        wfEvtServiceDefService.executeEventService(startEventInvock, execution.getId(), execution.getProcessDefinitionId(), execution.getProcessInstanceId(), variableMap,
                flowElement, WfVariableUtil.getSysCommHead());
        if (log.isInfoEnabled()) {
            log.info("| - WfCommServiceImpl>>>>>开始进行会签任务的选人[节点id：{}]", execution.getCurrentActivityId());
        }
        try {
            Map<String, UserOutputData> tempMap = new HashMap<String, UserOutputData>();
            String varKeyName = Constants.SIGN_USER_LIST;
            Object obj = execution.getVariableLocal(varKeyName);
            //会签任务执行的时候查询人员
            if (obj != null && variableMap.containsKey(varKeyName)) {
                tempMap = (Map<String, UserOutputData>) obj;
                result.addAll(tempMap.keySet());
                execution.removeVariableLocal(varKeyName);
                WfVariableUtil.removeVariableMap(varKeyName);
                log.info("| - WfCommServiceImpl>>>>>会签任务处理过程中的选人[节点id：{}，人员id:{}]", flowElement.getId(), tempMap);
            } else {
                //会签节点任务创建的时候查询人员
                if (variableMap.containsKey(Constants.SPECIFYEXTOPERS)
                        && variableMap.get(Constants.SPECIFYEXTOPERS) != null
                        && variableMap.get(Constants.SPECIFYEXTOPERS).toString().trim().length() > 0) {
                    String userIds = variableMap.get(Constants.SPECIFYEXTOPERS).toString();
                    List<Map<String, String>> idList = new ArrayList<Map<String, String>>();
                    for (String id : userIds.split(SymbolConstants.COMMA)) {
                        Map<String, String> tempIdMap = new HashMap<String, String>();
                        tempIdMap.put("orderUserNo", id);
                        idList.add(tempIdMap);
                    }
                    tempList = queryRelationTellerFactory.getInstance().queryUserList(BpmStringUtil.listToJson(idList));
                } else {
                    tempList = findNodeUsers(execution.getId(), execution.getProcessDefinitionId(),
                            execution.getProcessInstanceId(), variableMap, execution.getCurrentFlowElement());
                }
                for (UserOutputData userData : tempList) {
                    if (StrUtil.isEmpty(userData.getUserId())) {
                        throw new ServiceException(ErrorCode.WF000021);
                    }
                    result.add(userData.getUserId());
                    tempMap.put(userData.getUserId(), userData);
                }
                execution.setVariableLocal(varKeyName, tempMap);
                WfVariableUtil.addVariableMap(varKeyName, tempMap);
                log.info("| - WfCommServiceImpl>>>>>完成会签任务的选人[节点id：{}，人员id；{}]", flowElement.getId(), tempMap);
            }
        } catch (ServiceException e) {
            log.error(BpmStringUtil.getExceptionStack(e), e);
            throw e;
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e), e);
        }
        if (result.isEmpty()) {
            throw new ServiceException("会签节点[" + flowElement.getName() + "]选人为空请确认！");
        }
        return result;
    }

    /**
     * 会签是否完成
     *
     * @param execution
     * @return
     */
    public boolean isCompleteForSign(DelegateExecution execution) {
        return isCompleteForSignComm(execution, WfVariableUtil.getVariableMap(), false);
    }

    /**
     * 会签是否完成
     *
     * @param execution
     * @param isAllComplete 是否完成后调用的标志 true:表示是最后一个会签任务
     * @return
     */
    public boolean isCompleteForSignComm(DelegateExecution execution, Map<String, Object> variableMap, boolean isAllComplete) {
        FlowElement element = execution.getCurrentFlowElement();
		// 0-未通过 1-通过
        int signResult = Integer.parseInt(Constants.RESULT_OPPOSE);
        String signType = processUtil.getTaskProp(execution.getProcessDefinitionId(), element, "jointlySelect");// element.getAttributeValue(Constants.XMLNAMESPACE, "jointlySelect");
        String waitType = processUtil.getTaskProp(execution.getProcessDefinitionId(), element, Constants.WAITTYPE);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.WAITTYPE);

        if (log.isInfoEnabled()) {
            log.info("| - WfCommServiceImpl>>>>>开始会签表达式计算[节点id：{}]", element.getId());
            log.info("| - WfCommServiceImpl>>>>>开始会签表达式计算[业务参数：{}]", variableMap);
            log.info("| - WfCommServiceImpl>>>>>开始会签表达式计算[会签策略：{}]", signType);
            log.info("| - WfCommServiceImpl>>>>>开始会签表达式计算[等待模式：{}]", waitType);
        }

        // 会签是 等待模式 并且 不止最后一个会签任务的时候，返回false（没有结束）
        if (Constants.WAITTYPE_WAIT.equals(waitType) && !isAllComplete) {
            return false;
        }

        String currTaskId = (String) variableMap.get(Constants.BEFOR_TASKID);
        // 统计之前已审批的会签结果
        Map<String, Double> result = getSignResultStatistics(execution, element.getId(), currTaskId);

        // 添加本次的会签结果
        String lastResult = (String) variableMap.get(Constants.SINGLASTTASKRESULT);
        Double count = result.get(lastResult);
        if (count == null) {
            count = 0.0;
        }
        count = count + 1;
        result.put(lastResult, count);
        log.info("| - WfCommServiceImpl>>>>>会签计算[审批结果：{}]", result);
        double allInstCount = ((int) execution.getVariable("nrOfInstances")) * 1.0;

        switch (signType) {
            case "yes":// 一票通过
                if (result.get(Constants.RESULT_PASS) > 0) {
                    isAllComplete = true;
                    signResult = Integer.parseInt(Constants.RESULT_PASS);
                } else {
                    Double oppose = result.get(Constants.RESULT_OPPOSE);
                    Double think = result.get(Constants.RESULT_THINK);
                    signResult = Integer.parseInt(oppose >= think ? Constants.RESULT_OPPOSE : Constants.RESULT_THINK);
                }
                break;
            case "no":// 一票否定
                if (result.get(Constants.RESULT_OPPOSE) > 0) {
                    isAllComplete = true;
                    signResult = Integer.parseInt(Constants.RESULT_OPPOSE);
                } else {
                    Double pass = result.get(Constants.RESULT_PASS);
                    Double think = result.get(Constants.RESULT_THINK);
                    signResult = Integer.parseInt(pass >= think ? Constants.RESULT_PASS : Constants.RESULT_THINK);
                }
                break;
            case "percent":// 百分比
                String signValue = processUtil.getTaskProp(execution.getProcessDefinitionId(), element, "jointlyPercent");// element.getAttributeValue(Constants.XMLNAMESPACE, "jointlyPercent");
                double percent = Double.parseDouble(signValue);
                if (log.isInfoEnabled()) {
                    log.info("| - WfCommServiceImpl>>>>>百分比结果[分子{}]", (result.get(Constants.RESULT_PASS)));
                }
                //计算通过的 通过的大于等于设置的百分百
                if ((result.get(Constants.RESULT_PASS) / allInstCount) * NumberConstants.INT_100 >= percent) {
                    isAllComplete = true;
                    signResult = Integer.parseInt(Constants.RESULT_PASS);
                } else {
                    Double oppose = result.get(Constants.RESULT_OPPOSE);
                    Double think = result.get(Constants.RESULT_THINK);
                    if (((oppose + think) / allInstCount) * NumberConstants.INT_100 > (NumberConstants.INT_100 - percent)) {
                        isAllComplete = true;
                    }
                    signResult = Integer.parseInt(oppose >= think ? Constants.RESULT_OPPOSE : Constants.RESULT_THINK);
                }
                break;
            case "num":// 指定人数
                //指定人数必须要等所有任务完成
                if (!isAllComplete) {
                    return false;
                }
                signValue = processUtil.getTaskProp(execution.getProcessDefinitionId(), element, "jointlyManage");//element.getAttributeValue(Constants.XMLNAMESPACE, "jointlyManage");
                // 会签指定人数配置
                JSONArray item = JSONArray.parseArray(signValue);
                for (int i = 0; i < item.size(); i++) {
                    JSONObject itemObject = item.getJSONObject(i);
                    String pall = itemObject.getString("pall");// 参与人数
                    allInstCount = (int) variableMap.get("nrOfInstances");
                    if (Integer.valueOf(pall) == allInstCount) {
                        // 目标：同意人数
                        String tongyi = itemObject.getString("tongyi");
                        // 目标：同意+再议人数
                        String zaiyi = itemObject.getString("zaiyi");
                        // 实际：同意任务
                        Double accept = result.get(Constants.RESULT_PASS);
                        // 实际：同意+再议人数
                        Double sumCount = accept + result.get(Constants.RESULT_THINK);
                        if (accept >= Integer.parseInt(tongyi)) {
                            signResult = Integer.parseInt(Constants.RESULT_PASS);
                        } else if (sumCount >= Integer.parseInt(zaiyi)) {
                            signResult = Integer.parseInt(Constants.RESULT_THINK);
                        }
                    }
                }
                break;
            default:
                break;
        }
        if (log.isInfoEnabled()) {
            log.info("| - WfCommServiceImpl>>>>>开始会签表达式计算[计算结果：{}]", signResult);
        }
        // 会签完成，统计会签结果，将结果放入流程变量中
        if (isAllComplete) {
            variableMap.put("signResult", signResult);
            // 移除不需要的变量
            FlowNode node = (FlowNode) element;
            if (node.getOutgoingFlows().size() > 1) {
                String ruleMapStr = analysisRouteRule(execution.getProcessDefinitionId(), element);
                if (StrUtil.isBlank(ruleMapStr)) {
                    variableMap.put(Constants.ROUTE_CONDITION_KEY, signResult);
                } else {
                    try {
                        boolean isRoute = forkRouteSelect(execution.getProcessDefinitionId(), node, variableMap);
                        WfVariableUtil.setVariableMap(variableMap);
                        // 提前判断下路由分支是否满足
                        if (isRoute) {
                            SequenceFlowCadition sequenceFlowCadition = SpringUtil.getBean(SequenceFlowCadition.class);
                            sequenceFlowCadition.mathcCaditionCheck(variableMap, node);
                        }
                    } catch (Exception e) {
                        log.error("会签计算失败！", e);

                        throw new ServiceException(e.getMessage());
                    }
                }
            }
            variableMap.remove("nrOfActiveInstances");
            variableMap.remove("nrOfInstances");
            variableMap.remove("nrOfCompletedInstances");
            // 将wf_taskflow表中未执行的都更新为执行，防止有待办任务
            WfTaskflow condation = new WfTaskflow();
            condation.setProcessId(execution.getProcessInstanceId());
            condation.setTaskDefId(element.getId());
            condation.setWorkState(Constants.TASK_STATE_RUNING);
            List<WfTaskflow> noFinshList = wfTaskflowService.selectByCondition(condation);

            // TODO 此处后续需要改为批量更新
            for (WfTaskflow temp : noFinshList) {
                temp.setWorkState(Constants.TASK_STATE_END);
                temp.setDoResult(Constants.RESULT_SYSUSE);
                temp.setUpdateRemark("会签自动结束");
                temp.setBussinessMap(JSONObject.toJSONString(variableMap));
                wfTaskflowService.updateById(temp);
            }

        }
        if (log.isInfoEnabled()) {
            log.info("| - WfCommServiceImpl>>>>>完成会签表达式计算[计算结果：{}]", isAllComplete);
        }
        return isAllComplete;
    }

    /**
     * 获取会签审批结果的统计信息
     *
     * @param execution
     * @return
     */
    private Map<String, Double> getSignResultStatistics(DelegateExecution execution, String taskDefId, String currTaskId) {

        //查找出已经审批过的会签节点任务
        WfTaskflow selectData = new WfTaskflow();
        selectData.setProcessId(execution.getProcessInstanceId());
        selectData.setTaskDefId(taskDefId);
        selectData.setWorkState(Constants.TASK_STATE_END);
        List<WfTaskflow> doTaskList = wfTaskflowService.selectByCondition(selectData);
        WfTaskflow currTaskFlow = wfTaskflowService.selectByTaskId(currTaskId);

        //后去父节点

        Map<String, Double> result = new HashMap<String, Double>();
        double pass = 0, oppose = 0, think = 0, sysuse = 0, count = 0;
        for (WfTaskflow tempTaskFlow : doTaskList) {
            //利用节点的上一处理节点ID不同来过滤重复的记录（有回退等情况下，会重复）
            if (StrUtil.isNotEmpty(tempTaskFlow.getSuperTaskId()) && !tempTaskFlow.getSuperTaskId().equals(currTaskFlow.getSuperTaskId())) {
                continue;
            }

            String tempResult = tempTaskFlow.getDoResult();
            if (!StrUtil.isEmpty(tempResult)) {
                if (tempResult.equals(Constants.RESULT_PASS)) {
                    pass++;
                }
                if (tempResult.equals(Constants.RESULT_OPPOSE)) {
                    oppose++;
                }
                if (tempResult.equals(Constants.RESULT_THINK)) {
                    think++;
                }
                if (tempResult.equals(Constants.RESULT_SYSUSE)) {
                    sysuse++;
                }
                count = count + 1.0;
            }
        }
        result.put(Constants.RESULT_OPPOSE, oppose);
        result.put(Constants.RESULT_PASS, pass);
        result.put(Constants.RESULT_THINK, think);
        result.put(Constants.RESULT_SYSUSE, sysuse);
        return result;
    }

    /**
     * 启动子流程的流程编码列表
     *
     * @return
     */
    public List<String> subProcessIdList(DelegateExecution execution) {
        FlowElement element = execution.getCurrentFlowElement();
        String docStr = element.getDocumentation();
        JSONObject docMap = JSONObject.parseObject(docStr);
        String codeListStr = null;
        if (docMap.get(Constants.SBUPROC_CODELIST) == null) {
            codeListStr = docMap.get(Constants.SBUPROC_CODELIST).toString();
        }
        String waitType = null;
        if (docMap.get(Constants.WAITTYPE) == null) {
            waitType = docMap.get(Constants.WAITTYPE).toString();
        }

        String[] codeArrs = codeListStr.split(SymbolConstants.COMMA);
        List<String> resultList = new ArrayList<String>();
        if (waitType.equals(Constants.WAITTYPE_WAIT)) {
            for (String code : codeArrs) {
                resultList.add(code);
            }
        } else if (waitType.equals(Constants.WAITTYPE_NOWAIT)) {
            for (String code : codeArrs) {
                WorkFlowStartData workFlowStartData = new WorkFlowStartData();
                workFlowStartData.setUserId("system");
                workFlowStartData.setBussinessMap(execution.getVariables());
                workFlowStartData.setProcessCode(code);
                try {
                    processCoreService.startWorkflowDef(workFlowStartData);
                } catch (Exception e) {
                    log.error("| - WfCommServiceImpl>>>>>流程[{}]启动失败", code, e);
                }
            }
        } else {
            log.info("| - WfCommServiceImpl>>>>>配置错误");
        }

        return resultList;
    }

    /**
     * 子流程是否结束
     *
     * @param execution
     * @return
     */
    public boolean isSubProcEnd(DelegateExecution execution) {
        return false;
    }

    /**
     * 设置分支路由流转变量
     *
     * @param varMap
     * @throws Exception
     */
    public boolean forkRouteSelect(String processCode, FlowElement element, Map<String, Object> varMap) {

        String routeOutValue = "";

        Object specifyNextNode = varMap.get(Constants.SPECIFYNEXTNODE);
        //如果指定流转分支，就使用指定的
        if (specifyNextNode != null && (StrUtil.isNotBlank(specifyNextNode.toString()))) {

            //找到所有从start节点出去的线的ID
            FlowNode currNode = (FlowNode) element;
            Map<String, SequenceFlow> seqflowMap = new HashMap<String, SequenceFlow>();
            for (SequenceFlow tempOutFlow : currNode.getOutgoingFlows()) {
                seqflowMap.put(tempOutFlow.getId(), tempOutFlow);
            }

            //指定的线的ID必须在定义的线中
            String nextNodeId = specifyNextNode.toString();
            boolean flag = false;
            for (String specifyId : nextNodeId.split(SymbolConstants.COMMA)) {
                if (seqflowMap.containsKey(specifyId.replaceAll(Constants.SPECIFYNEXTNODE, ""))) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                throw new ServiceException("人工路由选择的流转方向不存在，请确认！");
            }
            //添加前缀是为了后续判断的时候好区分
            routeOutValue = Constants.SPECIFYNEXTNODE + nextNodeId;
        } else {
            routeOutValue = roultRuleCommService(processCode, element, varMap);
        }

        // 添加流程流转变量
        boolean result = false;
        if (StrUtil.isNotBlank(routeOutValue)) {
            WfVariableUtil.addVariableMap(Constants.ROUTE_CONDITION_KEY, routeOutValue);
            result = true;
        }
        return result;
    }

    /**
     * 调用路由规则引擎的统一入口服务
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public String roultRuleCommService(String processCode, FlowElement flowElement, Map<String, Object> variableMap) {

        String ruleMapStr = analysisRouteRule(processCode, flowElement);

        if (StrUtil.isBlank(ruleMapStr)) {
            return "";
        }
        //start by amt 2019-03-12 当该节点支持循环时
        String forEnd = processUtil.getTaskProp(processCode, flowElement, Constants.FOR_END);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
        if (StrUtil.equalsIgnoreCase(forEnd, Constants.STRING_TRUE)) {
            String taskDefId = flowElement.getId();
            variableMap.put(Constants.TASK_DEF_ID, taskDefId);
            //新的需要添加，否则直接透传
            if (!variableMap.containsKey(Constants.TASK_LOOP_RESULT)) {
                Map<String, Integer> map = new HashMap<String, Integer>();
                map.put(taskDefId, 1);
                variableMap.put(Constants.TASK_LOOP_RESULT, map);
            } else {
                Map<String, Integer> map = new HashMap<String, Integer>();
                if (variableMap.containsKey(Constants.TASK_LOOP_RESULT)) {
                    map = (Map<String, Integer>) variableMap.get(Constants.TASK_LOOP_RESULT);
                }

                //当不包含时，说明时新的节点，添加进变量中重新流转
                if (!map.containsKey(taskDefId)) {
                    map.put(taskDefId, 1);
                    variableMap.put(Constants.TASK_LOOP_RESULT, map);
                }
            }

            variableMap.remove(Constants.TASK_LOOP_COUNT);
        }
        //end  by amt 2019-03-12 当该节点支持循环时
        variableMap.remove(Constants.ROUTE_CONDITION_KEY);
        String outFlowName = urule.getInstance().execUruleReusltList(variableMap, JSONObject.parseObject(ruleMapStr));
        // 找出满足条件的出口名称
        if (StrUtil.isNotBlank(outFlowName)) {
            JSONArray outArrays = JSONArray.parseArray(outFlowName);
            variableMap.put(Constants.ROUTE_SELECT_KEY, outArrays);

            StringBuffer buf = new StringBuffer();
            for (int i = 0; i < outArrays.size(); i++) {
                JSONObject obj = outArrays.getJSONObject(i);
                buf.append(SymbolConstants.COMMA);
                buf.append(obj.getString("branch"));
                //当该节点支持循环时
                if (StrUtil.equalsIgnoreCase(forEnd, "true")) {
                    //肯定有taskLoopCount
                    Map<String, Integer> map = (Map<String, Integer>) variableMap.get(Constants.TASK_LOOP_RESULT);
                    //获取当前节点下次的执行次数
                    int defIdCount = obj.getInteger(Constants.TASK_LOOP_COUNT);
                    //当下次执行次数为0，说明本循环已经完成
                    if (defIdCount == 0) {
                        map.remove(flowElement.getId());
                    } else {
                        //否则保存下次循环次数
                        map.put(flowElement.getId(), defIdCount);
                    }
                    //移除针对该功能新增的变量
                    variableMap.remove(Constants.TASK_DEF_ID);
                    //当参数中不存在执行次数时，移除根路径taskLoopCount
                    //否则覆盖当前的循环
                    if (!map.isEmpty()) {
                        variableMap.put(Constants.TASK_LOOP_RESULT, map);
                    } else {
                        variableMap.remove(Constants.TASK_LOOP_RESULT);
                    }
                }
            }
            String result = buf.toString();
            if (outArrays.size() > 0) {
                result = result.substring(1);
            }
            variableMap.put(Constants.ROUTE_CONDITION_KEY, result);

            return result;
        }
        return null;
    }

    /**
     * 脚本执行器接口
     *
     * @param javaScriptStr js脚本
     * @param varMap        参数
     */
    public String executeJavaScript(String javaScriptStr, Map<String, Object> varMap) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        // 为空数据处理，判断js时，存在不传值的情况，防止报错判断不出为空的情况
        if (StrUtil.isNotEmpty(javaScriptStr)) {
            if (javaScriptStr.contains("==")) {
                String[] split = javaScriptStr.split("==");
                if (split.length == 2) {
                    engine.put(split[0], null);
                }
            } else if (javaScriptStr.contains("!=")) {
                String[] split = javaScriptStr.split("!=");
                if (split.length == 2) {
                    engine.put(split[0], null);
                }
            }
        }
        // varMap数据处理
        for (Map.Entry<String, Object> entry : varMap.entrySet()) {
            engine.put(entry.getKey(), entry.getValue());
        }
        String result = "false";
        // js脚本执行
        if (engine instanceof Compilable) {
            Compilable compEngine = (Compilable) engine;
            try {
                CompiledScript script = compEngine.compile(javaScriptStr);
                Object temp = script.eval();
                if (temp != null) {
                    result = temp.toString();
                }
            } catch (ScriptException e) {
                // js脚本执行异常处理，若异常，返回false
                log.error("| - WfCommServiceImpl>>>>>executeJavaScript is error:", e);
            }
        } else {
            log.error("| - WfCommServiceImpl>>>>>getConditionResult>>>>>>Engine can't compile code");
        }
        log.info("| - WfCommServiceImpl>>>>>decisionScript result:[{}]", result);
        return result;
    }

    /**
     * 流程开始、结束，节点进入、离开条件的执行入口
     *
     * @param conditionName
     * @param extendsPropList
     * @param varMap
     */
    public void conditionalVerification(String conditionName, List<WfModelPropExtends> extendsPropList, Map<String, Object> varMap, String errMsg) {
        try {
            String condationResult = modelPropService.getRuleOrJavaScriptPropValue(extendsPropList, conditionName, varMap);
            if (StrUtil.isNotBlank(condationResult)) {
                boolean result = Boolean.parseBoolean(condationResult);
                if (!result) {
                    throw new ServiceException(errMsg);
                }
            }
        } catch (Exception e) {
            log.error("| - WfCommServiceImpl>>>>>条件的执行入口conditionalVerification计算失败", e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * 解析节点的路由配置规则
     *
     * @param element
     * @return
     */
    private String analysisRouteRule(String processCode, FlowElement element) {
        //节点的路由配置
        boolean isIgnore = configUtil.isIgnoreProcessVersion();
        if (isIgnore) {
            WfDefinePropExtends condition = new WfDefinePropExtends();
            condition.setTaskCode(element.getId());
            condition.setPropName("rule.ruleMap");
            condition.setProcessCode(processCode.split(":")[0]);
            condition.setVersion(new BigInteger(processCode.split(":")[1]));
            WfDefinePropExtends propextends = wfDefinePropExtendsService.selectByProcessCodePropNameTaskCode(condition);
            if (propextends != null) {
                return propextends.getPropValue();
            }
        }
        List<ExtensionElement> elementList = element.getExtensionElements().get("routeRule");
        if (elementList != null) {
            for (ExtensionElement extenElement : elementList) {
                Map<String, List<ExtensionAttribute>> attsMap = extenElement.getAttributes();
                for (Iterator<String> iter = attsMap.keySet().iterator(); iter.hasNext(); ) {
                    String key = iter.next();
                    for (ExtensionAttribute att : attsMap.get(key)) {
                        if ("ruleMap".equals(att.getName())) {
                            return att.getValue();
                        }
                    }
                }

            }
        }
        return null;
    }

    /**
     * 功能清单结果获取方法
     *
     * @param modelPropExtends
     * @param varMap
     * @return
     * @throws ActivitiException
     */
    public String getNodeFunctionList(WfModelPropExtends modelPropExtends, Map<String, Object> varMap)
            throws ActivitiException {
        if (modelPropExtends == null) {
            return "";
        }
        String propValue = modelPropExtends.getPropValue();
        Map<String, Object> map = JSONObject.parseObject(propValue);
        String type = (String) map.get("type");
        Object valueObj = map.get("value");
        if (Constants.FILED_TYPE_NOMEAL.equals(type)) {
            return valueObj.toString();
        } else if (Constants.FILED_TYPE_URULE.equals(type)) {//非0就是3  表示规则引擎
            return urule.getInstance().execUruleReusltList(varMap, BpmStringUtil.objectToJsonObject(valueObj));
        }
        return "";

    }


    //------------------------------------2019-05-05 amt modify start 根据规则查询选人信息 ----

    /**
     * @Description: 选取节点的执行人员
     * @Param: [ruleConfigMap, variableMap]
     * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.dto.UserOutputData>
     * @Author:
     * @Date: 2021/9/23
     */
    public List<UserOutputData> findNodeUsers(Map<String, Object> ruleConfigMap,
                                              Map<String, Object> variableMap) throws Exception {
        // 获取干系人（信息）列表
        List<Map<String, Object>> userList = getQueryTellerData(variableMap);
        List<Map<String, Object>> ruleList = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> tempMap : userList) {
            Map<String, Object> subMap = new HashMap<String, Object>();
            subMap.put("source", tempMap.get("userId"));
            subMap.put("target", tempMap.get("userType"));
            ruleList.add(subMap);
        }
        if (ruleConfigMap == null) {
            ruleConfigMap = new HashMap<String, Object>();
        }
        ruleConfigMap.put("list", ruleList);
        log.debug("| - WfCommServiceImpl>>>>>ruleList:{}", ruleList);
        log.debug("| - WfCommServiceImpl>>>>>ruleConfigMap:{}", ruleConfigMap);
        // 根据选人策略进行选人
        List<UserOutputData> userDataList = null;
        try {
            userDataList = queryRelationTellerFactory.getInstance().queryTellerByPolicy(null, null, null,
                    JSONObject.toJSONString(ruleConfigMap), userList, variableMap, null);

        } catch (ServiceException eb) {
            log.error(BpmStringUtil.getExceptionStack(eb), eb);
            throw eb;
        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e), e);
            throw new ServiceException("选人服务调用失败" + e.getMessage());
        }

        return userDataList;
    }
    //------------------------------------2019-05-05 amt modify end 根据规则查询选人信息 ----


    /**
     * 设置上下文的任务紧急程度
     * @param myVarMap 流程变量map
     * @param inputVarMap  流转过程中上送变量map
     */
    /**
     * @param myVarMap
     * @param inputVarMap
     * @return 最终得到的紧急状态
     */
    public String setTaskUargent(Map<String, Object> myVarMap, Map<String, Object> inputVarMap) {

        String innerUargent = "0";
        String outUargent = "0";
        if (myVarMap != null && myVarMap.containsKey(Constants.TASK_URGENT_KEY)) {
            innerUargent = (String) myVarMap.get(Constants.TASK_URGENT_KEY);
        }
        if (inputVarMap != null && inputVarMap.containsKey(Constants.TASK_URGENT_KEY)) {
            outUargent = (String) inputVarMap.get(Constants.TASK_URGENT_KEY);
            if (!BpmConstants.TASK_URGENT_0.equals(outUargent) && !BpmConstants.TASK_URGENT_1.equals(outUargent)) {
                throw new ServiceException("紧急程度字段【" + Constants.TASK_URGENT_KEY + "】指定错误,必须为【0-正常，1-紧急】！");
            }

            //上送的紧急程度优先
            innerUargent = outUargent;
        } else {
            outUargent = innerUargent;
        }

        if (!BpmConstants.TASK_URGENT_0.equals(outUargent) && !BpmConstants.TASK_URGENT_1.equals(outUargent)) {
            innerUargent = BpmConstants.TASK_URGENT_0;
            outUargent = innerUargent;
        }

        //最后将一致的紧急程度放入各自map中
        if (myVarMap != null) {
            myVarMap.put(Constants.TASK_URGENT_KEY, innerUargent);
        }
        if (inputVarMap != null) {
            inputVarMap.put(Constants.TASK_URGENT_KEY, outUargent);
        }

        return outUargent;

    }

    /**
     * 专门用来出来任务或实例的附件
     */
    public String processAccessories(WorkFlowTaskTransferData taskTransferData, WfTaskflow taskflow) {
        // 处理附件
        String accessories = taskTransferData.getAccessories();
        String accessoriesKey = "_accessories_";
        if (ObjectUtil.isNotEmpty(taskflow) && StrUtil.isNotEmpty(accessories)) {
            taskflow.setDoAccessories(accessories);
            WfVariableUtil.addVariableMap(accessoriesKey, accessories);
        }
        return accessories;
    }
}
