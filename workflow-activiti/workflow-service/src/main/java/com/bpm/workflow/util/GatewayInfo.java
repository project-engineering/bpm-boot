package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.exception.ServiceException;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Element;
import java.io.Serializable;
import java.util.*;

@Log4j2
public class GatewayInfo implements Serializable {
    private static final long serialVersionUID = -8367242202774969182L;

    public String processDefId;


    public GatewayInfo(String processDefId, String startNodeId, List<Element> sequenceFlows, Map<String, Element> processElementByIds) {
        this.processDefId = processDefId;
        init(startNodeId, sequenceFlows, processElementByIds);
    }

    private Map<String, NodeInfo> nodeInfosByIdMap = new HashMap<>();

    private synchronized void init(String startNodeId, List<Element> sequenceFlows, Map<String, Element> processElementByIds) {
        for (Map.Entry<String, Element> anEntry : processElementByIds.entrySet()) {
            if ("sequenceFlow".equals(anEntry.getValue().getName())) {
                continue;
            }
            NodeInfo nodeInfo = new NodeInfo(anEntry.getKey(), GatewayUtil.isGatewayNode(anEntry.getValue()));
            if (nodeInfo.isGatewayNode()) {
                String jointCountStr = anEntry.getValue().attributeValue("joinCount");
                if (StrUtil.isEmpty(jointCountStr)) {
                    nodeInfo.setGatewayJoinCount(0);
                } else {
                    nodeInfo.setGatewayJoinCount(Integer.parseInt(jointCountStr));
                }
            }
            nodeInfosByIdMap.put(anEntry.getKey(), nodeInfo);
        }

        for (Element flowElement : sequenceFlows) {
            String fromId = flowElement.attributeValue("sourceRef");
            String toId = flowElement.attributeValue("targetRef");
            nodeInfosByIdMap.get(fromId).addNextNodeId(toId);
            nodeInfosByIdMap.get(toId).addBeforeNodeId(fromId);
        }

        for (Map.Entry<String, NodeInfo> anEntry : nodeInfosByIdMap.entrySet()) {
            if (!anEntry.getValue().hasNeighborNode(true)) {
                if (processElementByIds.get(anEntry.getKey()).getName().indexOf("startEvent") < 0 && processElementByIds.get(anEntry.getKey()).getName().indexOf("StartEvent") < 0) {
                    log.info("| - GatewayInfo>>>>>error node:{}", anEntry.getValue());
                    // 流程错误，非开始节点且没有前节点
                    throw new ServiceException(ErrorCode.WF000014);
                }
            }
            if (!anEntry.getValue().hasNeighborNode(false)) {
                if (processElementByIds.get(anEntry.getKey()).getName().indexOf("endEvent") < 0
                        && processElementByIds.get(anEntry.getKey()).getName().indexOf("EndEvent") < 0) {
                    log.info("| - GatewayInfo>>>>>error node:{}", anEntry.getValue());
                    // 流程错误，非结束节点且没有后节点
                    throw new ServiceException(ErrorCode.WF000015);
                }
            }
        }

        infoContexts = new HashMap<>();

        InfoContext rootInfoContext = new InfoContext(null);
        infoContexts.put(startNodeId, rootInfoContext);
        preSetNoneGatewayNodeInfo(startNodeId, rootInfoContext);
        finalSetNodeInfo(rootInfoContext);
    }

    public NodeInfo getNodeInfo(String nodeId) {
        return nodeInfosByIdMap.get(nodeId);
    }
    private Map<String, InfoContext> infoContexts = null;

    private class InfoContext {
        private InfoContext parentInfoContext = null;

        public InfoContext getParentInfoContext() {
            return parentInfoContext;
        }

        private Set<InfoContext> childrenInfoContext = null;

        public void addChilder(InfoContext childInfoContext) {
            if (childrenInfoContext == null) {
                childrenInfoContext = new HashSet<InfoContext>();
            }
            childrenInfoContext.add(childInfoContext);
        }

        public Set<InfoContext> getChildrenInfoContext() {
            if (childrenInfoContext == null) {
                childrenInfoContext = new HashSet<InfoContext>();
            }
            return childrenInfoContext;
        }

        public InfoContext(InfoContext parentInfoContext) {
            this.parentInfoContext = parentInfoContext;
            if (parentInfoContext == null) {
                return;
            }
            parentInfoContext.addChilder(this);

        }

        private String curStartGatewayNode;
        private String curEndGatewayNode;

        public String getCurStartGatewayNode() {
            return curStartGatewayNode;
        }

        public void setCurStartGatewayNode(String curStartGatewayNode) {
            this.curStartGatewayNode = curStartGatewayNode;
        }

        public String getCurEndGatewayNode() {
            return curEndGatewayNode;
        }

        public void setCurEndGatewayNode(String curEndGatewayNode) {
            this.curEndGatewayNode = curEndGatewayNode;
        }

        private Set<String> dealtNodeId = new HashSet<String>();

        public Set<String> getDealtNodeId() {
            return dealtNodeId;
        }

        public boolean isDealtNode(String nodeId) {
            if (dealtNodeId == null || dealtNodeId.size() == 0) {
                return false;
            }
            return dealtNodeId.contains(nodeId);
        }

        public void addNewDealt(String nodeId) {
            if (dealtNodeId == null) {
                dealtNodeId = new HashSet<String>();
            }
            dealtNodeId.add(nodeId);
        }
    }

    private void preSetNoneGatewayNodeInfo(String curNodeId, InfoContext parentInfoContex) {
        NodeInfo curNode = nodeInfosByIdMap.get(curNodeId);

        if (parentInfoContex.isDealtNode(curNodeId)) {
            return;
        }

        if (curNode.getNeighborNodes(false) == null || curNode.getNeighborNodes(false).size() == 0) {
            //reachEnd
            return;
        } else if (curNode.getNeighborNodes(false).size() == 1) {
            String nextNodeId = curNode.getNeighborNodes(false).iterator().next();
            parentInfoContex.addNewDealt(curNodeId);
            if (parentInfoContex.isDealtNode(nextNodeId)) {
//    			throw new RuntimeException("single loop back flow cause the dead loop:curNode:"+curNodeId+"   nextNodeId:"+nextNodeId);
                return;
            } else {
                preSetNextNodeInfo(parentInfoContex, nextNodeId);
            }
        } else {
            parentInfoContex.addNewDealt(curNodeId);
            for (String nextNodeId : curNode.getNeighborNodes(false)) {
                if (parentInfoContex.isDealtNode(nextNodeId)) {
                    continue;
                }
                preSetNextNodeInfo(parentInfoContex, nextNodeId);
            }
        }
    }

    private void preSetNextNodeInfo(InfoContext parentInfoContex, String nextNodeId) {
        NodeInfo nextNode = nodeInfosByIdMap.get(nextNodeId);
        if (nextNode.isGatewayNode()) {
            preSetGatewayNodeInfo(nextNode, parentInfoContex);
        } else {
            preSetNoneGatewayNodeInfo(nextNode.getNodeId(), parentInfoContex);
        }
    }


    private void finalSetNodeInfo(InfoContext rootInfoContex) {
        for (String dealtNodeId : rootInfoContex.getDealtNodeId()) {
            NodeInfo theNode = nodeInfosByIdMap.get(dealtNodeId);
            theNode.setBelongStartGatewayNodeId(rootInfoContex.getCurStartGatewayNode());
            theNode.setBelongEndGatewayNodeId(rootInfoContex.getCurEndGatewayNode());
        }

        for (InfoContext childInfoContext : rootInfoContex.getChildrenInfoContext()) {
            finalSetNodeInfo(childInfoContext);
        }
    }


    private void preSetGatewayNodeInfo(NodeInfo curGatewayNode, InfoContext parentInfoContex) {
        if (curGatewayNode == null || !curGatewayNode.isGatewayNode()) {
            throw new RuntimeException("this method can deal gateway node only");
        }
        if (parentInfoContex.isDealtNode(curGatewayNode.getNodeId())) {
            return;
        }
        if (curGatewayNode.getNeighborNodes(false) == null || curGatewayNode.getNeighborNodes(false).size() == 0) {
            throw new RuntimeException("gateway node can't be the end node");
        }
        if (curGatewayNode.isStartGatewayNode()) {
            parentInfoContex.addNewDealt(curGatewayNode.getNodeId());
            for (String nextNodeId : curGatewayNode.getNeighborNodes(false)) {
                InfoContext newInfoContex = new InfoContext(parentInfoContex);
                newInfoContex.setCurStartGatewayNode(curGatewayNode.getNodeId());
                NodeInfo nextNode = nodeInfosByIdMap.get(nextNodeId);
                if (parentInfoContex.isDealtNode(nextNodeId)) {
                    throw new RuntimeException("start gateway node can't be point back ");
                }
                if (nextNode.isGatewayNode()) {
                    // 网关节点的下一个节点不允许是网关节点
                    throw new ServiceException(ErrorCode.WF000019);
                } else {
                    preSetNextNodeInfo(newInfoContex, nextNodeId);
                }
            }
        } else {
            parentInfoContex.setCurEndGatewayNode(curGatewayNode.getNodeId());
            parentInfoContex.getParentInfoContext().addNewDealt(curGatewayNode.getNodeId());
            if (curGatewayNode.getNeighborNodes(false).size() > 1) {
                throw new RuntimeException("end gateway node can have only 1 follow node");
            }
            for (String nextNodeId : curGatewayNode.getNeighborNodes(false)) {
                nodeInfosByIdMap.get(nextNodeId);
//		    	if(nextNode.isGatewayNode()) {
//		    		throw new ServiceException("000019");// 000019=网关节点的下一个节点不允许是网关节点
//		    	}else {
                preSetNextNodeInfo(parentInfoContex.getParentInfoContext(), nextNodeId);
//		    	}
            }
        }
    }

}
