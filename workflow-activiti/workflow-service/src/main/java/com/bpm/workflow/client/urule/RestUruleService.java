package com.bpm.workflow.client.urule;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.WorkFlowServiceBean;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service(value = "restUruleService")
public class RestUruleService implements IUruleService {

    @Resource
    public ConfigUtil configUtil;

    /**
     * 根据规则名称执行规则引擎
     *
     * @param ruleName
     * @param params
     * @return
     */
    @Override
    public String execByRuleName(String ruleName, Map<String, Object> params) {
        WorkFlowServiceBean requestBean = new WorkFlowServiceBean();
        requestBean.getTranBody().getMapReqData().put("knowledgeId", ruleName);
        String reqJsonStr = prepareReqMapDataByParamsMap(params, requestBean);
        String resp = ApacheHttpClientUtil.sendHttpRequestForPost(configUtil.getUruleUrl() + "validateRules",
                reqJsonStr, Integer.parseInt(configUtil.getUruleTimeOut()));
        if (log.isInfoEnabled()) {
            log.info("| - RestUruleService>>>>> execByRuleName:{} resp:{}", ruleName, resp);
            log.info(resp);
        }

        return getResultByRuleName(resp);
    }

    public String getResultByRuleName(String ruleName) {
        Map<String, Object> responseMap = ObjectUtil.unPackMessage(ruleName);
        Object obj = responseMap.get("result");
        return (obj == null ? null : obj.toString());
    }

    /**
     * json串转map，支持json多层嵌套
     *
     * @param params
     * @param requestBean
     * @return
     */
    @SuppressWarnings("unused")
    private void prepareReqMapDataMap(Map<String, Object> params, WorkFlowServiceBean requestBean) {
        Map<String, Object> busMap = new HashMap<>();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, Object> anEntry : params.entrySet()) {
                Object v = anEntry.getValue();
                busMap.put(anEntry.getKey(), anEntry.getValue());
            }
        }
        if (WfVariableUtil.getSysCommHead() != null) {
            requestBean.setTranHead(WfVariableUtil.getSysCommHead());
        }
        requestBean.getTranBody().getMapReqData().put("busMap", busMap);
    }

    /**
     * json串转map，支持json多层嵌套
     *
     * @param params
     * @param requestBean
     * @return
     */
    public String prepareReqMapDataByParamsMap(Map<String, Object> params, WorkFlowServiceBean requestBean) {
        Map<String, Object> busMap = new HashMap<String, Object>();
        if (params != null && params.size() > 0) {
            busMap.putAll(params);
        }
        if (WfVariableUtil.getSysCommHead() != null) {
            requestBean.setTranHead(WfVariableUtil.getSysCommHead());
        }
        requestBean.getTranBody().getMapReqData().put("busMap", busMap);
        String reqJsonStr = JSONObject.toJSONString(requestBean);
        if (log.isInfoEnabled()) {
            log.info("| - RestUruleService>>>>> searchProcessDefinitionId req:{}", reqJsonStr);
        }
        return reqJsonStr;
    }


    public static final String TEST_TIMEOUT_HOUR_RULE = "test_timeoutHour";
    public static final String TEST_PRICE = "test_price";

    /**
     * @param param
     * @param uruleMap
     * @return
     */
    public String noUruleReturn(Map<String, Object> param, JSONObject uruleMap) {
        if (uruleMap != null && uruleMap.getString(BpmConstants.RULE_NO) != null
                && TEST_TIMEOUT_HOUR_RULE.equals(uruleMap.getString(BpmConstants.RULE_NO))) {
            if (param != null && param.get(TEST_PRICE) != null) {
                int testPrice = -1;
                try {
                    testPrice = Integer.parseInt((String) param.get(TEST_PRICE));
                } catch (Exception e) {
                    log.error("| - RestUruleService>>>>>param:{}", param, e);
                }
                if (testPrice != -1) {
                    return "" + testPrice;
                }
            }
        }
        return null;
    }


    /**
     * @Description: 选人、流转、功能清单等规则执行接口
     * @Param: [param, uruleMap]
     * @Return: java.lang.String
     */
    @Override
    public String execUruleReusltList(Map<String, Object> param, JSONObject uruleMap) {
        if (configUtil.isNoUrule()) {
            return noUruleReturn(param, uruleMap);
        }
        JSONObject paramObject = JSONObject.parseObject(JSONUtil.toJsonStr(param));
        uruleMap.put("busMap", paramObject);

        String resp = ApacheHttpClientUtil.sendHttpRequestForPost(configUtil.getUruleUrl() + "validateRules",
                uruleMap.toJSONString(), Integer.parseInt(configUtil.getUruleTimeOut()));
        if (log.isInfoEnabled()) {
            log.info("| - RestUruleService>>>>>响应结果[{}]", resp);
        }
        return getReturnResult(resp, param, uruleMap);
    }

    /**
     * 组装execSelectPersion方法执行时需要的报文头
     *
     * @param param
     * @param uruleMap
     * @return
     */
    public WorkFlowServiceBean getRequetParams(Map<String, Object> param, JSONObject uruleMap) {
        param.remove("returnCode");
        WorkFlowServiceBean requestBean = new WorkFlowServiceBean();
        if (log.isInfoEnabled()) {
            log.info("| - RestUruleService>>>>>请求地址[{}validateRules]", configUtil.getUruleUrl());
            log.info("| - RestUruleService>>>>>请求的业务参数[{}]", param.toString());
            log.info("| - RestUruleService>>>>>规则引擎需要的参数[{}]", uruleMap.toString());
        }
        if (!uruleMap.isEmpty()) {
            uruleMap.remove("list");
            requestBean.getTranBody().getMapReqData().putAll(uruleMap);
        }
        return requestBean;

    }

    /**
     * @Description: 组装execSelectPersion方法返回的执行结果
     * @Param: [response, param, uruleMap]
     * @Return: java.lang.String
     */
    public String getReturnResult(String response, Map<String, Object> param, JSONObject uruleMap) {
        // 判断uRule返回是否成功
        Map<String, Object> responseMap = ObjectUtil.unPackMessage(response);

        Object obj = responseMap.get("result_list");
        if (obj == null || StrUtil.isEmpty(obj.toString()) || SymbolConstants.SYMBOL_006.equals(obj.toString())) {
			log.info("没有与规则匹配的结果，请检查参数与规则是否匹配：" + uruleMap.getString("fullPath"));
            throw new ServiceException("没有与规则匹配的结果，请检查参数与规则是否匹配：" + uruleMap.getString("fullPath"));
        }
		log.info("| - RestUruleService >>>>>result_list :{}", obj.toString());
        // 规则调用返回的业务参数中，补充追加到当前业务参数
        Object saveBussinessData = responseMap.get(Constants.RESULT_DATA_MAP_KEY);
        if (saveBussinessData instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> saveBussinessMap = (Map<String, Object>) saveBussinessData;
			log.info("| - RestUruleService >>>>>result_map:{}", saveBussinessData.toString());
            param.putAll(saveBussinessMap);
            param.put(Constants.RESULT_DATA_MAP_KEY, BpmStringUtil.mapToJson(saveBussinessMap));
        }
        return obj.toString();
    }
}
