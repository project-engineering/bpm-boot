package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.ServiceNameConstants;
import com.bpm.workflow.entity.WfParam;
import com.bpm.workflow.mapper.WfParamMapper;
import com.bpm.workflow.util.CacheUtil;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class CacheActionWfParamLocal extends CacheActionBase<WfParam>{
	@Resource
	private WfParamMapper wfParamMapper;

	@Override
	public List<WfParam> getCacheListValueByKey(String selectMethodName,String firstKey, String secondKey) {
		if(ServiceNameConstants.SELECT_PARAM_BY_PARAM_TYPE.equals(selectMethodName)) {
			return selectParamByParamType(firstKey);
		}else if(ServiceNameConstants.SELECT_PARAM_BY_PARAM_TYPE_PARAM_CODE.equals(selectMethodName)){
			return selectParamByParamTypeParamCode(firstKey,secondKey);
		}else {
			throw new RuntimeException("not support select method:"+selectMethodName);
		}
	}
	
	@Override
	public WfParam getCacheValueByKey(String selectMethodName, String firstKey, String secondKey) {
		throw new RuntimeException("not support select method:"+selectMethodName);
	}
	

	private List<WfParam> selectParamByParamType(String paramType){
		if(StrUtil.isEmpty(paramType)) {
			throw new RuntimeException("paramType can not be null");
		}
		List<WfParam> result =super.getCacheListValueByKey(null, new String[] {paramType});
		if( result== null||(!CacheUtil.USE_CACHE)) {
			WfParam thePara = new WfParam();
			thePara.setParamType(paramType);
			result = wfParamMapper.selectParamList(thePara);
			if(result!=null) {
				super.putCacheListValueByKey(null,  new String[] {paramType}, result);
			}else {
				super.putCacheListValueByKey(null,  new String[] {paramType}, new ArrayList<WfParam>());
			}
		}
		
		return result;
	}
	
	private List<WfParam> selectParamByParamTypeParamCode(String paramType,String paramCode){
		if(StrUtil.isEmpty(paramCode)) {
			return selectParamByParamType(paramType);
		}
		List<WfParam> result =super.getCacheListValueByKey(null, new String[] {paramType,paramCode});
		if( result== null||(!CacheUtil.USE_CACHE)) {
			WfParam thePara = new WfParam();
			thePara.setParamCode(paramCode);
			thePara.setParamType(paramType);
			result = wfParamMapper.selectParamList(thePara);

			if(result!=null) {
				super.putCacheListValueByKey(null,  new String[] {paramType,paramCode}, result);
			}else {
				super.putCacheListValueByKey(null,  new String[] {paramType,paramCode}, new ArrayList<WfParam>());
			}
			

		}
		return result;
	}










}
