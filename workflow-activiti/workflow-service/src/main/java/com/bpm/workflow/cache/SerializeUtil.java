package com.bpm.workflow.cache;

import lombok.extern.log4j.Log4j2;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class SerializeUtil {

    /**
     * 序列化对象
     * @param object
     * @return
     */
    public static byte[] serialize(Object object) {
        ObjectOutputStream oos = null;
        ByteArrayOutputStream baos = null;
        byte[] bytes = null;
        try {
            // 序列化
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            bytes = baos.toByteArray();
        } catch (Exception e) {
            log.error("| - SerializeUtil>>>>>object:{}",object,e);
        } finally {
            close(oos);
            close(baos);
        }
        return bytes;
    }

    /**
     * 反序列化对象
     * @param bytes
     * @return
     */
    public static Object unserialize(byte[] bytes) {
        Object obj = null;
        // 反序列化
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bais)) {
            obj = ois.readObject();
        } catch (Exception e) {
            log.error("| - SerializeUtil>>>>>unserialize失败",e);
        }
        return obj;
    }

    /**
     * 关闭的数据源或目标。调用 close()方法可释放对象保存的资源（如打开文件）
     * 关闭此流并释放与此流关联的所有系统资源。如果已经关闭该流，则调用此方法无效。
     * @param closeable
     */
    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                log.error("| - SerializeUtil>>>>>Unable to close",e);

            }
        }
    }

    /**
     * 列表序列化（用于Redis整存整取）
     * @param value
     * @return
     */
    public static <T> byte[] serialize(List<T> value) {
        if (value == null) {
            throw new NullPointerException("Can't serialize null");
        }
        byte[] rv=null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream os = null;
        try {
            bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            for(T obj : value){
                os.writeObject(obj);
            }
            os.writeObject(null);
            os.close();
            bos.close();
            rv = bos.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Non-serializable object", e);
        } finally {
            close(os);
            close(bos);
        }
        return rv;
    }

    /**
     * 反序列化列表（用于Redis整存整取）
     * @param in
     * @return
     */
    @SuppressWarnings("unchecked")
	public static <T> List<T> unserializeForList(byte[] in) {
        List<T> list = new ArrayList<T>();
        ByteArrayInputStream bis = null;
        ObjectInputStream is = null;
        try {
            if(in != null) {
                bis=new ByteArrayInputStream(in);
                is=new ObjectInputStream(bis);
                while (true) {
                    T obj = (T) is.readObject();
                    if(obj == null){
                        break;
                    }else{
                        list.add(obj);
                    }
                }
                is.close();
                bis.close();
            }
        } catch (IOException e) { 
            log.error("| - SerializeUtil>>>>>Caught IOException decoding %d bytes of data",e);

        } catch (ClassNotFoundException e) { 
            log.error("| - SerializeUtil>>>>>Caught CNFE decoding %d bytes of data",e);

        } finally {
            close(is);
            close(bis);
        }
        return list;
    }

    //对象序列化为字符串
    public static String objSerialiableToStr(Object obj){
        String serStr = null;
        // 2023-04-03 20230527@王玉玺 start 根据行内代码安全审计系统扫描结果调整流未关闭的地方
        ByteArrayOutputStream byteArrayOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            serStr = byteArrayOutputStream.toString("ISO-8859-1");
            serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
        } catch (UnsupportedEncodingException e) {
             log.error("| - SerializeUtil>>>>>序列化obj:{}",obj,e);
        } catch (IOException e) {
            log.error("| - SerializeUtil>>>>>关闭流失败",e);
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                log.error("| - SerializeUtil>>>>> finally objectOutputStream 关闭流失败",e);
            }
            try {
                if (byteArrayOutputStream != null) {
                    byteArrayOutputStream.close();
                }
            } catch (IOException e) {
                log.error("| - SerializeUtil>>>>> finally byteArrayOutputStream 关闭流失败",e);
            }
        }
        // 2023-04-03 20230527@王玉玺 end   根据行内代码安全审计系统扫描结果调整流未关闭的地方
        return serStr;
    }

    //字符串反序列化为对象
    public static Object strUnSerializeToObj(String serStr){
        Object newObj = null;
        // 2023-04-03 20230527@王玉玺 start 根据行内代码安全审计系统扫描结果调整流未关闭的地方
        ByteArrayInputStream byteArrayInputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            String redStr = java.net.URLDecoder.decode(serStr, "UTF-8");
            byteArrayInputStream = new ByteArrayInputStream(redStr.getBytes("ISO-8859-1"));
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            newObj = objectInputStream.readObject();
        } catch (UnsupportedEncodingException e) {
            log.error("| - SerializeUtil>>>>>编码格式异常",e);
        } catch (ClassNotFoundException e) {
            log.error("| - SerializeUtil>>>>>类找不到",e);
        } catch (IOException e) {
            log.error("| - SerializeUtil>>>>>关闭流失败",e);
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
            } catch (IOException e) {
                log.error("| - SerializeUtil>>>>> finally objectInputStream 关闭流失败",e);
            }
            try {
                if (byteArrayInputStream != null) {
                    byteArrayInputStream.close();
                }
            } catch (IOException e) {
                log.error("| - SerializeUtil>>>>> finally byteArrayInputStream 关闭流失败",e);
            }
        }
        // 2023-04-03 20230527@王玉玺 end   根据行内代码安全审计系统扫描结果调整流未关闭的地方
        return newObj;
    }

}
