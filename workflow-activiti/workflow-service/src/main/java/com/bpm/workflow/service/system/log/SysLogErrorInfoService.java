package com.bpm.workflow.service.system.log;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.log.SysLogErrorInfo;

/**
 * <p>
 * 异常信息表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-17
 */
public interface SysLogErrorInfoService  extends IService<SysLogErrorInfo> {

}
