package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.DateConstants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.util.StringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Component;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Log4j2
public class DateUtils {
	private static int startFromSunday = 0;


	public static Date getNow() {
		return Calendar.getInstance().getTime();
	}

	public static synchronized boolean isStartFromSunday() {
		if (startFromSunday == NumberConstants.INT_NEGATIVE_100) {
			Calendar cal = Calendar.getInstance();
			// 2018-6-13周三
			cal.setTime(getDateByString("2018-6-13 05:00:01"));
			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
				startFromSunday = 0;
			} else {
				startFromSunday = -1;
			}
		}
		return startFromSunday == 0;
	}

	private static final int WEEK_DAY_SUNDAY = 7;

	private static int formatWeekDay(int passingIn) {
		if (isStartFromSunday()) {
			if (passingIn == WEEK_DAY_SUNDAY) {
				return 1;
			} else {
				return passingIn + 1;
			}
		} else {
			return passingIn;
		}
	}

	public static Date getDateByString(String theDateStr) {
		return getDateByString("yyyy-MM-dd HH:mm:ss", theDateStr);
	}

	public static Date getDateByString(String mask, String theDateStr) {
		try {
			return (new SimpleDateFormat(mask)).parse(theDateStr);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static String getStringByDate(String mask, Date theDate) {
		return (new SimpleDateFormat(mask)).format(theDate);
	}

	public static Date getDateByIntevalFromNow(int yearsFromNow, int monthsFromNow, int daysFromNow, int hoursFromNow,
			int minutesFromNow, int secondFromNow) {
		Calendar cal = Calendar.getInstance();
		return getDateByIntevalFromNow(cal.getTime(), yearsFromNow, monthsFromNow, daysFromNow, hoursFromNow,
				minutesFromNow, secondFromNow);
	}

	public static Date getDateByIntevalFromNow(Date current, int yearsFromNow, int monthsFromNow, int daysFromNow,
			int hoursFromNow, int minutesFromNow, int secondFromNow) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(current);
		if (yearsFromNow > 0) {
			cal.add(Calendar.YEAR, yearsFromNow);
		}
		if (monthsFromNow > 0) {
			cal.add(Calendar.MONTH, monthsFromNow);
		}
		if (daysFromNow > 0) {
			cal.add(Calendar.DAY_OF_YEAR, daysFromNow);
		}
		if (hoursFromNow > 0) {
			cal.add(Calendar.HOUR, hoursFromNow);
		}
		if (minutesFromNow > 0) {
			cal.add(Calendar.MINUTE, minutesFromNow);
		}
		if (secondFromNow > 0) {
			cal.add(Calendar.SECOND, secondFromNow);
		}
		return cal.getTime();
	}

	public enum DateCheckResult {
		onYear, onMonth, onDayOfMonthAndSameDayOfWeek, onRunDayOfWeek, onHour, onMinute, onSecond, noDiffer, notOnSchedule
	}

	public static DateCheckResult getDateDiffer(Date current, Date lastExecute, int checkInteval) {
		Calendar calCurrent = Calendar.getInstance();
		if (current != null) {
			calCurrent.setTime(current);
		}

		Calendar calLastExecute = Calendar.getInstance();
		calLastExecute.setTime(lastExecute);
		if (lastExecute == null) {
			return DateCheckResult.onYear;
		}

		if (calLastExecute.after(calCurrent)) {
			throw new ServiceException(ErrorCode.WF000006);
		}

		if (calCurrent.get(Calendar.YEAR) != calLastExecute.get(Calendar.YEAR)) {
			return DateCheckResult.onYear;
		}
		if (calCurrent.get(Calendar.MONTH) != calLastExecute.get(Calendar.MONTH)) {
			return DateCheckResult.onMonth;
		}
		if (calCurrent.get(Calendar.DAY_OF_MONTH) != calLastExecute.get(Calendar.DAY_OF_MONTH)) {
			if (calCurrent.get(Calendar.DAY_OF_WEEK) != calLastExecute.get(Calendar.DAY_OF_WEEK)) {
				return DateCheckResult.onRunDayOfWeek;
			} else {
				return DateCheckResult.onDayOfMonthAndSameDayOfWeek;
			}

		}
		if (calCurrent.get(Calendar.HOUR_OF_DAY) != calLastExecute.get(Calendar.HOUR_OF_DAY)) {
			return DateCheckResult.onHour;
		}
		if (calCurrent.get(Calendar.MINUTE) != calLastExecute.get(Calendar.MINUTE)) {
			return DateCheckResult.onMinute;
		}
		if (calCurrent.get(Calendar.SECOND) * NumberConstants.INT_1000 - checkInteval > calLastExecute.get(Calendar.SECOND) * NumberConstants.INT_1000
				+ getAllowSecondRange(checkInteval)
				|| calCurrent.get(Calendar.SECOND) * 1000 + checkInteval < calLastExecute.get(Calendar.SECOND) * NumberConstants.INT_1000
						- getAllowSecondRange(checkInteval)) {
			return DateCheckResult.onSecond;
		}
		return DateCheckResult.noDiffer;
	}

	public static boolean isOnSchedule(Date current, Date lastExecute, int runMonth, int runDayOfMonth,
			int runDayOfWeek, int runHourOfDay, int runMinuteOfHour, int runSecondOfMinute, int checkInteval) {
		if (current == null) {
			throw new ServiceException(ErrorCode.WF000004);
		}

		if (runMonth == -1 && runDayOfMonth == -1 && runDayOfWeek == -1 && runHourOfDay == -1 && runMinuteOfHour == -1
				&& runSecondOfMinute == -1) {
			throw new ServiceException(ErrorCode.WF000005);
		}
		if (lastExecute == null) {
			// log.info(" | - DateUtil>>>>>lastExecute is null");
			DateCheckResult onScheduleType = getOnScheduleType(current, runMonth, runDayOfMonth, runDayOfWeek,
					runHourOfDay, runMinuteOfHour, runSecondOfMinute, checkInteval);
			return (onScheduleType != DateCheckResult.notOnSchedule);
		} else {
			// log.info("lastExecute is :"+lastExecute);
			DateCheckResult dateDiffer = getDateDiffer(current, lastExecute, checkInteval);
			if (dateDiffer == DateCheckResult.noDiffer) {
				return false;
			}
			DateCheckResult onScheduleType = getOnScheduleType(current, runMonth, runDayOfMonth, runDayOfWeek,
					runHourOfDay, runMinuteOfHour, runSecondOfMinute, checkInteval);
			return (onScheduleType != DateCheckResult.notOnSchedule);
		}
	}

	private static int getAllowSecondRange(int checkInteval) {
		return (int) (checkInteval * 0.6);
	}

	public static DateCheckResult getOnScheduleType(Date checkDate, int runMonth, int runDayOfMonth, int runDayOfWeek,
			int runHourOfDay, int runMinuteOfHour, int runSecondOfMinute, int checkInteval) {

		if (checkDate == null) {
			throw new ServiceException(ErrorCode.WF000004);
		}
		if (runMonth == -1 && runDayOfMonth == -1 && runDayOfWeek == -1 && runHourOfDay == -1 && runMinuteOfHour == -1
				&& runSecondOfMinute == -1) {
			throw new ServiceException(ErrorCode.WF000005);
		}

		Calendar calCurrent = Calendar.getInstance();
		calCurrent.setTime(checkDate);

		DateCheckResult result = null;
		if (runMonth > 0) {
			result = DateCheckResult.onMonth;
			if (calCurrent.get(Calendar.MONTH) != runMonth - 1) {
				return DateCheckResult.notOnSchedule;
			}
		}
		if (runDayOfMonth > 0) {
			result = DateCheckResult.onDayOfMonthAndSameDayOfWeek;
			if (calCurrent.get(Calendar.DAY_OF_MONTH) != runDayOfMonth) {
				return DateCheckResult.notOnSchedule;
			}
		}
		if (runDayOfWeek > 0) {
			result = DateCheckResult.onRunDayOfWeek;

			if (calCurrent.get(Calendar.DAY_OF_WEEK) != formatWeekDay(runDayOfWeek)) {
				return DateCheckResult.notOnSchedule;
			}
		}
		if (runHourOfDay > 0) {
			result = DateCheckResult.onHour;
			if (calCurrent.get(Calendar.HOUR_OF_DAY) != runHourOfDay) {
				return DateCheckResult.notOnSchedule;
			}
		}
		if (runMinuteOfHour > 0) {
			result = DateCheckResult.onMinute;
			if (calCurrent.get(Calendar.MINUTE) != runMinuteOfHour) {
				return DateCheckResult.notOnSchedule;
			}
		}

		if (runSecondOfMinute > 0) {
			result = DateCheckResult.onSecond;

			if (calCurrent.get(Calendar.SECOND) * NumberConstants.INT_1000 > runSecondOfMinute * NumberConstants.INT_1000 + getAllowSecondRange(checkInteval)
					|| calCurrent.get(Calendar.SECOND) * NumberConstants.INT_1000 < runSecondOfMinute * NumberConstants.INT_1000
							- getAllowSecondRange(checkInteval)) {
				return DateCheckResult.notOnSchedule;
			}
		}

		return result;
	}

	public static Date getNowTime() {
		return Calendar.getInstance().getTime();
	}

	public static final String NORMAL_MASK = "yyyy-MM-dd HH:mm:ss";

	public static String formatDate2Str(Date date) {
		return (new SimpleDateFormat(NORMAL_MASK)).format(date);
	}

	public static Date parseStr2Date(String str) {
		try {
			return (new SimpleDateFormat(NORMAL_MASK)).parse(str);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static int getSecondFromNow(Date now, int afterSecond) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.SECOND, afterSecond);
		return cal.get(Calendar.SECOND);
	}

	public static boolean isEqual(int toField, Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
			return false;
		}
		if (Calendar.YEAR == toField) {
			return true;
		}
		if (cal1.get(Calendar.MONTH) != cal2.get(Calendar.MONTH)) {
			return false;
		}
		if (Calendar.MONTH == toField) {
			return true;
		}
		if (cal1.get(Calendar.DAY_OF_YEAR) != cal2.get(Calendar.DAY_OF_YEAR)) {
			return false;
		}
		if (Calendar.DAY_OF_YEAR == toField) {
			return true;
		}
		if (cal1.get(Calendar.HOUR_OF_DAY) != cal2.get(Calendar.HOUR_OF_DAY)) {
			return false;
		}
		if (Calendar.HOUR_OF_DAY == toField) {
			return true;
		}
		if (cal1.get(Calendar.MINUTE) != cal2.get(Calendar.MINUTE)) {
			return false;
		}
		if (Calendar.MINUTE == toField) {
			return true;
		}
		if (cal1.get(Calendar.SECOND) != cal2.get(Calendar.SECOND)) {
			return false;
		}
		if (Calendar.SECOND == toField) {
			return true;
		}
		return date1.equals(date2);
	}

	public static Date getJustLaterDate(Date runDate, Set<Date> theDates, boolean canEqual) {
		if (runDate == null) {
			return null;
		}
		if (theDates == null || theDates.size() == 0) {
			return null;
		}
		TreeSet<Date> toBeCheckDates = new TreeSet<>(theDates);

		Date result = null;
		for (Iterator<Date> it = toBeCheckDates.descendingIterator(); it.hasNext();) {
			Date theDate = it.next();
			// log.info("================\n"+theDate.getTime()+"\n"+runDate.getTime()+"
			// "+theDate.equals(runDate));
			if (theDate.before(runDate) || (canEqual && isEqual(Calendar.SECOND, theDate, runDate))) {
				result = theDate;
				break;
			}
		}
		return result;
	}


	public static long getMilliSecond(Date dt) {
		if (dt == null) {
			throw new RuntimeException("null date can't use caculate getMilliSecond");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		return cal.getTimeInMillis();
	}

	public static long getLaterMilliSecondFromNow(Date dt) {
		if (dt == null) {
			throw new RuntimeException("null date can't use caculate LaterMilliSecondFromNow");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		return System.currentTimeMillis() - cal.getTimeInMillis();
	}

	public static Date addDateWithHour(Date ori, int hour) {
		if (ori == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(ori);
		cal.setTimeInMillis(cal.getTimeInMillis() + (long) hour * 3600 * 1000);
		return cal.getTime();
	}

	public static Date addDateWithMillisecond(Date ori, long milli) {
		if (ori == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(ori);
		cal.setTimeInMillis(cal.getTimeInMillis() + milli);
		return cal.getTime();
	}

	public static Date addTimeoutDateWithMillisecond(Date ori, long milli) {
		if (ori == null) {
			return null;
		}
		if (milli == TaskTimoutUtil.NO_OVERTIME || milli == 0) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(ori);
		cal.setTimeInMillis(cal.getTimeInMillis() + milli);
		return cal.getTime();
	}

	/**
	 * 按照yyyy-MM-dd HH:mm:ss的格式，字符串转日期
	 *
	 * @param date
	 * @return
	 */
	public static Date strToDate(String date) {
		if (StrUtil.isNotBlank(date)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				return sdf.parse(date);
			} catch (ParseException e) {
				log.error("| - DateUtil>>>>>类型转换失败",e);
			}
			return new Date();
		} else {
			return null;
		}
	}

	/**
	 * 给当前时间加上几分钟、几个小时、几天
	 *
	 * @param cDate
	 *            当前时间 格式：yyyy-MM-dd HH:mm:ss
	 * @param val
	 *            需要加的时间
	 * @return
	 */
	public static String addDateByUnit(String cDate, String val) {
		// 引号里面个格式也可以是 HH:mm:ss或者HH:mm等等，很随意的，不过在主函数调用时，要和输入的变量day格式一致
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 24小时制
		Date date = null;
		try {
			date = format.parse(cDate);
		} catch (ParseException ex) {
			log.error(ExceptionUtils.getStackTrace(ex),ex);
		}
		if (date == null) {
			return "";
		}
		if (StrUtil.isBlank(val) || StrUtil.isBlank(val)) {
			return "";
		}
		log.info("| - DateUtil>>>>>front:{}" , format.format(date)); // 显示输入的日期
		Calendar cal = Calendar.getInstance();
		try {
			if (StringUtil.indexOf(val, DateConstants.MINUTE) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.MINUTE));
				int minut = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.MINUTE));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.MINUTE, minut);
				date = cal.getTime();
			}
			if (StringUtil.indexOf(val, DateConstants.HOUR) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.HOUR));
				int hour = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.HOUR));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.HOUR_OF_DAY, hour);
				date = cal.getTime();
			}
			if (StringUtil.indexOf(val, DateConstants.DAY) > 0) {
				log.info("| - DateUtil>>>>>{}",StringUtil.substringBefore(val, DateConstants.DAY));
				int day = Integer.parseInt(StringUtil.substringBefore(val, DateConstants.DAY));
				cal.setTime(date);
				// 24小时制
				cal.add(Calendar.DAY_OF_MONTH, day);
				date = cal.getTime();
			}
		} catch (NumberFormatException ex) {
			log.error(ExceptionUtils.getStackTrace(ex),ex);
		}
		cal = null;
		log.info("| - DateUtil>>>>>after:{}" , format.format(date));
		return format.format(date);
	}

	/**
	 *
	 * 毫秒转天
	 *
	 * @param timeStamp
	 * @return 参数 String 返回类型
	 * @date 2018年8月16日 下午5:30:14
	 */
	public static String getDays(long timeStamp) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String strDay = decimalFormat.format(timeStamp / 86400000.00);
		if (SymbolConstants.POINT.equals(strDay.substring(0, 1))) {
			strDay = "0" + strDay;
		}
		return strDay;
	}

	/**
	 *
	 * 毫秒转时
	 *
	 * @param timeStamp
	 * @return 参数 String 返回类型
	 * @date 2018年8月16日 下午5:30:14
	 */
	public static String getHours(long timeStamp) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		return decimalFormat.format(timeStamp / 3600000.00);
	}

	/**
	 *
	 * 毫秒转分
	 *
	 * @param timeStamp
	 * @return 参数 String 返回类型
	 * @date 2018年8月16日 下午5:30:14
	 */
	public static String getMinutes(long timeStamp) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		return decimalFormat.format(timeStamp / 60000.00);
	}

	/**
	 *
	 * 毫秒转秒
	 *
	 * @param timeStamp
	 * @return 参数 String 返回类型
	 * @date 2018年8月16日 下午5:30:14
	 */
	public static String getSeconds(long timeStamp) {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		return decimalFormat.format(timeStamp / 1000.00);
	}

	public static Date getDateByObject(Object obj) {
		if(obj==null) {
			return null;
		}
		if(obj instanceof Date) {
			return (Date)obj;
		}else if(obj instanceof Long) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis((Long) obj);
			return cal.getTime();
		}else {
			throw new ServiceException("not support data type");
		}
	}

}
