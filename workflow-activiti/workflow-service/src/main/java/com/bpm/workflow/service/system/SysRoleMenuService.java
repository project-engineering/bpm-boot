package com.bpm.workflow.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.system.SysRoleMenu;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
