package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.entity.WfEvtServiceDef;
import com.bpm.workflow.entity.WfModelPropExtends;
import org.activiti.bpmn.model.FlowElement;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfEvtServiceDefService extends IService<WfEvtServiceDef> {

    void executeEventService(WfModelPropExtends startEventInvock, String id, String processDefinitionId
            , String processInstanceId, Map<String, Object> variableMap, FlowElement flowElement, SysCommHead sysCommHead);

    WfEvtServiceDef selectByServiceId(String serviceId);

    void insertEvtServiceDef(WfEvtServiceDef wfEvtServiceDef);

    void deleteEvtServiceDef(String ids);

    WfEvtServiceDef selectById(Integer id);

    List<WfEvtServiceDef> selectList(Map<String, Object> param, PageData<WfEvtServiceDef> pageDate);

    String autoTaskNodeHandler(String id, String processDefinitionId, String processInstanceId, Map<String, Object> varMap, SysCommHead sysCommHead, FlowElement flowElement);
}
