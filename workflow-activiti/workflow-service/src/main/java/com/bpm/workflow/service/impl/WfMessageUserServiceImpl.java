package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfMessageUser;
import com.bpm.workflow.mapper.WfMessageUserMapper;
import com.bpm.workflow.service.WfMessageUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfMessageUserServiceImpl extends ServiceImpl<WfMessageUserMapper, WfMessageUser> implements WfMessageUserService {

}
