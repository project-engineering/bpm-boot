package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowModelPropExtData;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfPropertiesConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfModelPropExtendsService extends IService<WfModelPropExtends> {

    WfModelPropExtends selectByOwnIdPropName(WfModelPropExtends wfNodepropExtends);

    void insertWfModelPropExtends(WfModelPropExtends wfNodepropExtends);

    void deleteByOwnIdPropName(WfModelPropExtends wfNodepropExtends);

    List<WfModelPropExtends> selectModelExtendsPropByOwnId(String nodeId);

    void deleteModelExtendPropByOwnId(String nodeId);

    WfModelPropExtends selectById(Integer id);

    void updateByOwnIdPropName(WfModelPropExtends nodepropExtends);

    List<WfModelPropExtends> selectModelExtendsPropByOwnIdAndOwnType(WfModelPropExtends model);

    List<WfModelPropExtends> selectModelExtendsPropByPropVal(String propValue);

    List<WfModelPropExtends> selectAutoAllotList();

    List<WfModelPropExtends> selectList(WfModelPropExtends selectCondition, PageData<Object> objectPageData);

    List<WfModelPropExtends> selectList(WfModelPropExtends condition);

    WfModelPropExtends viewModelPropExtend(String taskNodeId, String name);

    WfModelPropExtends selectByOwnIdPropName(String taskCode, String name);

    Map<String, WfModelPropExtends> getAutoAllotExtends();

    String getModelPropExtendsValue(String modelId, String name);

    void deleteModelPropExtendByNodeId(String defId);

    void addUseProcessLatesVersion(String defId, Integer version);

    void insertModelPropExtend(WorkFlowModelPropExtData workFlowModelPropExtData);

    void deleteModelPropExtend(WorkFlowModelPropExtData workFlowModelPropExtData);

    void updateModelPropExtend(WfModelPropExtends wfModelPropExtends);

    List<WfModelPropExtends> selectModelPropExtendList(WorkFlowModelPropExtData modelProp);

    List<WfPropertiesConfig> selectEnableConfigProperis(WfPropertiesConfig propConfig);

    String executeJavaScript(String js, HashMap<String, Object> businessMap);
}
