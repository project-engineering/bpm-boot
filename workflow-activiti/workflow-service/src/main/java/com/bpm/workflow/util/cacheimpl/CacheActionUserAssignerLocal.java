package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.entity.TaskCodeCount;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.util.CacheUtil;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <p>把每个节点的用户分配信息保存到缓存中</p>
 * <pre>
 * 	其中：用户信息在缓存中的存储结构为Map其中的key=节点id，value=人员列表
 *
 * 方法 1：getUserAssginerFromCache(defId,taskAllocationType.value,percentage);
 * 根据策略获取下一个人员id
 *
 * 方法 2： putUserAssginer2Cache(defId,users)
 * 替换缓存中对应节点的人员信息列表
 * </pre>
 */
@Log4j2
@Component
public class CacheActionUserAssignerLocal {
    @Resource
    protected CacheUtil cacheUtil;
    @Resource
    protected WfTaskflowService wfTaskflowService;
    private static Logger logger = LoggerFactory.getLogger(CacheActionUserAssignerLocal.class);

    /**
     * 存储key=节点id，value=人员列表的map
     */
    private static Map<String, String> listValues = new ConcurrentHashMap<String, String>();

    /**
     * 当前执行结果,key=节点id，value=当前执行人
     */
    private static Map<String, String> concurrentValue = new ConcurrentHashMap<String, String>();

    /**
     * 总分配次数,key=节点id，value=节点执行次数
     */
    private static Map<String, Integer> totalAssigner = new ConcurrentHashMap<String, Integer>();

    /**
     * 当前执行结果,key=节点id，value=人员执行次数
     */
    private static Map<String, Map<String, Integer>> percentageValue = new ConcurrentHashMap<String, Map<String, Integer>>();

    public void clearAll(String defId) {
        log.info("| - CacheActionUserAssignerLocal>>>>>开始清理节点：{}", defId);
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            percentageValue.clear();
            listValues.clear();
            concurrentValue.clear();
            totalAssigner.clear();
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            cacheUtil.getRedisCacheUtil().remove("allocation:" + defId + ":actors");
            cacheUtil.getRedisCacheUtil().remove("allocation:" + defId + ":userCount");
            cacheUtil.getRedisCacheUtil().remove("allocation:" + defId + ":total");
            cacheUtil.getRedisCacheUtil().remove("allocation:" + defId + ":concurrent");
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            cacheUtil.getEhCacheUtil().remove("allocation:" + defId + ":actors");
            cacheUtil.getEhCacheUtil().remove("allocation:" + defId + ":userCount");
            cacheUtil.getEhCacheUtil().remove("allocation:" + defId + ":total");
            cacheUtil.getEhCacheUtil().remove("allocation:" + defId + ":concurrent");
        }
    }

    /**
     * 缓存中存储节点每一个可处理人分配次数
     *
     * @param defId 节点id
     * @param map   每一个可处理人分配次数
     */
    public void putUserCountValue(String defId, Map<String, Integer> map) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            percentageValue.put(defId, map);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            cacheUtil.getRedisCacheUtil().setObject("allocation:" + defId + ":userCount", map, -1L);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            cacheUtil.getEhCacheUtil().putCache("allocation:" + defId + ":userCount", map);
        }
    }

    /**
     * 缓存中获取节点及对应的当前每一个可处理人分配次数
     *
     * @param defId 节点id
     * @return 每一个可处理人分配次数
     */
    @SuppressWarnings("unchecked")
    private Map<String, Integer> getUserCountValue(String defId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            return percentageValue.get(defId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            return (Map<String, Integer>) cacheUtil.getRedisCacheUtil().getObject("allocation:" + defId + ":userCount");
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            return (Map<String, Integer>) cacheUtil.getEhCacheUtil().getObject("allocation:" + defId + ":userCount");
        }
        return null;
    }

    /**
     * 缓存中存储节点及当前的分配次数，用来计算百分比
     *
     * @param defId 节点id
     * @param count 总共分配次数
     */
    private void putTotalValue(String defId, Integer count) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            totalAssigner.put(defId, count);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            cacheUtil.getRedisCacheUtil().setObject("allocation:" + defId + ":total", count, -1L);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            cacheUtil.getEhCacheUtil().putCache("allocation:" + defId + ":total", count);
        }
    }

    /**
     * 缓存中获取节点及对应的当前的的分配次数，用来计算百分比
     *
     * @param defId 节点id
     * @return 总共分配次数
     */
    private Integer getTotalValue(String defId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            return totalAssigner.get(defId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            return (Integer) cacheUtil.getRedisCacheUtil().getObject("allocation:" + defId + ":total");
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            return (Integer) cacheUtil.getEhCacheUtil().getObject("allocation:" + defId + ":total");
        }
        return null;
    }

    /**
     * 缓存中存储节点及对应的可处理人员
     *
     * @param defId  节点id
     * @param userId 节点id所拥有的用户id
     */
    private void putListValue(String defId, String userId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            listValues.put(defId, userId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            cacheUtil.getRedisCacheUtil().setObject("allocation:" + defId + ":actors", userId, -1L);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            cacheUtil.getEhCacheUtil().putCache("allocation:" + defId + ":actors", userId);
        }
    }

    /**
     * 缓存中获取节点及对应的可处理人员
     *
     * @param defId 节点id
     * @return 节点id对应的可处理人员id
     */
    private String getListValue(String defId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            return listValues.get(defId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            return (String) cacheUtil.getRedisCacheUtil().getObject("allocation:" + defId + ":actors");
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            return (String) cacheUtil.getEhCacheUtil().getObject("allocation:" + defId + ":actors");
        }
        return null;
    }

    /**
     * 缓存中存储节点及当前的分配人员
     *
     * @param defId  节点id
     * @param userId 节点id所拥有的用户id
     */
    private void putConcurrentValue(String defId, String userId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            concurrentValue.put(defId, userId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            cacheUtil.getRedisCacheUtil().setObject("allocation:" + defId + ":concurrent", userId, -1L);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            cacheUtil.getEhCacheUtil().putCache("allocation:" + defId + ":concurrent", userId);
        }
    }

    /**
     * 缓存中获取节点及对应的当前的处理人员
     *
     * @param defId 节点id
     * @return 节点id对应的当前的处理人员id
     */
    private String getConcurrentValue(String defId) {
        if (cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
            return concurrentValue.get(defId);
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
            return (String) cacheUtil.getRedisCacheUtil().getObject("allocation:" + defId + ":concurrent");
        } else if (cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
            return (String) cacheUtil.getEhCacheUtil().getObject("allocation:" + defId + ":concurrent");
        }
        return null;
    }

    /**
     * 任务分配机制定义
     *
     * @author anmingtao
     */
    public enum TaskAllocationType {
        all("all", "0"), order("order", "1"), percentage("percentage", "2"), minConcurrent("minConcurrent", "3");
        private String name;
        private String value;

        private TaskAllocationType(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return this.name;
        }

        public String getValue() {
            return value;
        }

        public static TaskAllocationType getEnumByValue(String value) {
            for (TaskAllocationType taskConfType : TaskAllocationType.values()) {
                if (taskConfType.getValue().equals(value)) {
                    return taskConfType;
                }
            }
            throw new RuntimeException("not supported type:" + value);
        }

    }

    /**
     * @Description:
     * @Param: [defId, taskAllocationType, percentage]
     * @Return: java.lang.String
     * @Author:
     * @Date: 2021/9/23
     */
    public String getUserAssginerFromCache(String defId, String taskAllocationType, String percentage) {
        String us = getListValue(defId);
        if (Objects.isNull(us)) {
            return null;
        }

        if (taskAllocationType.equals(TaskAllocationType.percentage.getValue())) {
            return percentageAllocation(us, defId, percentage);
        }

        // 改造为从redis中查询
        if (taskAllocationType.equals(TaskAllocationType.order.getValue())) {
            return orderAllocation(us, defId);
        }
        // 改造为从redis中查询
        if (taskAllocationType.equals(TaskAllocationType.minConcurrent.getValue())) {
            return minAllocation(us);
        }
        return us;
    }

    /**
     * 按照百分比给相关人员分配任务
     *
     * @param us
     * @param defId
     * @return
     */
    private String percentageAllocation(String us, String defId, String percentage) {

        if (!us.contains(Constants.COMMA)) {
            return us;
        }
        String[] allusers = us.split(Constants.COMMA);

        Double[] allpercentages = new Double[allusers.length];
        double total = 100;
        if (StrUtil.isEmpty(percentage)) {
            throw new RuntimeException("任务分配配置错误：没有设置百分比");
        }
        // 获取%百分比，当设置一个50%则其他的以剩余的50%均分
        String[] percentages = percentage.replaceAll(Constants.PERCENTAGE, "").split(Constants.COMMA);
        // 如果用户数大于100%的设置，检查是否总和大于100，如果大于100，则提示错误，否则剩余的用100减去，进行均分；

        if (allusers.length > percentages.length) {
            for (int i = 0; i < percentages.length; i++) {
                allpercentages[i] = Double.valueOf(percentages[i]);
                total = total - allpercentages[i];
            }
            if (total < 0) {
                throw new RuntimeException("任务分配配置错误：设置的百分比大于100");
            }
            // 剩余的百分比
            double other = total / (allusers.length - percentages.length);
            for (int i = 0; i < allusers.length - percentages.length; i++) {
                allpercentages[percentages.length + i] = other;
            }
        } else {
            for (int i = 0; i < allusers.length; i++) {
                allpercentages[i] = Double.valueOf(percentages[i]);
                total = total - allpercentages[i];
            }
            if (total < 0) {
                throw new RuntimeException("任务分配配置错误：前" + (allusers.length + 1) + "个用户设置的百分比大于100");
            }

        }
        log.info("| - CacheActionUserAssignerLocal>>>>>所有的百分比：{}", JSONObject.toJSONString(allpercentages));
        // 下面时计算人员的逻辑,当缓存中已经有执行次数时，查找符合条件的人员，并对该人员的执行次数加1
        Map<String, Integer> map = getUserCountValue(defId);
        if (Objects.nonNull(map)) {
            Integer cishu = getTotalValue(defId);
            if (cishu == null) {
                putTotalValue(defId, 1);
                putUserCountValue(defId, map);
                return map.keySet().iterator().next();
            } else {
                cishu = cishu + 1;
                putTotalValue(defId, cishu);
                log.info("| - CacheActionUserAssignerLocal>>>>>节点执行情况：{}", map.toString());
                for (int i = 0; i < allpercentages.length; i++) {
                    int dangqiancishu = 0;
                    if (map.containsKey(allusers[i])) {
                        dangqiancishu = map.get(allusers[i]);
                    } else {
                        map.put(allusers[i], 1);
                        putUserCountValue(defId, map);
                        return allusers[i];
                    }
                    double c = (dangqiancishu * 1.0 / cishu) * 100;
                    log.info("| - CacheActionUserAssignerLocal>>>>>节点执行情况：{}占比{} %", allusers[i], c);
                    if (i == 0) {
                        if (c < allpercentages[0]) {
                            map.put(allusers[0], dangqiancishu + 1);
                            putUserCountValue(defId, map);
                            return allusers[0];
                        }
                    } else {
                        if ((c <= allpercentages[i - 1] && c >= allpercentages[i])
                                || (c >= allpercentages[i - 1] && c <= allpercentages[i])) {
                            map.put(allusers[i], dangqiancishu + 1);
                            putUserCountValue(defId, map);
                            return allusers[i];
                        }
                    }
                }
                int dangqiancishu = 0;
                if (map.containsKey(allusers[0])) {
                    dangqiancishu = map.get(allusers[0]);
                }
                map.put(allusers[0], dangqiancishu + 1);
                putUserCountValue(defId, map);
                return allusers[0];
            }
        } else {
            // 不存在符合条件的人员时，取第一个人，并对第一个人的执行次数加1
            map = new HashMap<String, Integer>();
            map.put(allusers[0], 1);
            putUserCountValue(defId, map);
            putTotalValue(defId, 1);
            return allusers[0];
        }

    }

    /**
     * 顺序给相关人员分配任务
     *
     * @param us
     * @param defId
     * @return
     */
    private String orderAllocation(String us, String defId) {
        if (!us.contains(SymbolConstants.COMMA)) {
            return us;
        }
        String[] allUsers = us.split(SymbolConstants.COMMA);
        String userId = getConcurrentValue(defId);
        if (userId == null) {
            putConcurrentValue(defId, allUsers[0]);
            return allUsers[0];
        }
        int k = 0;
        for (; k < allUsers.length; k++) {
            if (allUsers[k].equals(userId)) {
                break;
            }
        }
        if (k >= allUsers.length - 1) {
            putConcurrentValue(defId, allUsers[0]);
            return allUsers[0];
        }
        putConcurrentValue(defId, allUsers[k + 1]);
        return allUsers[k + 1];
    }

    /**
     * @Description: 按照最小并发数给相关人员分配任务
     * @Param: [us]
     * @Return: java.lang.String
     * @Author:
     * @Date: 2021/9/23
     */
    private String minAllocation(String us) {
        if (!us.contains(SymbolConstants.COMMA)) {
            return us;
        }
        String[] allusers = us.split(SymbolConstants.COMMA);
        List<TaskCodeCount> userCountList;

        userCountList = wfTaskflowService.selectUnoTaskCountByUsers(Arrays.asList(allusers));

        userCountList.sort(Comparator.comparingInt(o -> o.getNum().intValue()));

        // 取分配次数最少的
        return userCountList.get(0).getUserId();
    }

    /**
     * 审批、打回节点以后，节点的处理人分配次数减一
     *
     * @param defId
     * @param userId
     */
    public void subtractionUserCount(String defId, String userId) {
        if (Objects.isNull(defId) || Objects.isNull(userId)) {
            return;
        }
        Map<String, Integer> map = getUserCountValue(defId);
        if (Objects.nonNull(map)) {
            if (map.containsKey(userId)) {
                Integer count = map.get(userId);
                if (count != null) {
                    if (count == 0) {
                        List<TaskCodeCount> userCountList = wfTaskflowService.selectUnoTaskCountByUsers(Arrays.asList(new String[]{userId}));
                        map = new HashMap<String, Integer>();
                        for (TaskCodeCount u1 : userCountList) {
                            map.put(u1.getUserId(), u1.getNum().intValue());
                        }
                        putUserCountValue(defId, map);
                    } else {
                        map.put(userId, count - 1);
                        putUserCountValue(defId, map);
                    }
                }
            }
        }
    }

    /**
     * @param defId
     * @param users
     */
    public void putUserAssginer2Cache(String defId, String users) {
        String u = getListValue(defId);
        // 当缓存中已经有该节点的用户信息时，选择性更新，否则
        if (Objects.nonNull(u)) {
            if (u.equals(users)) {

            } else {
                log.info("| - CacheActionUserAssignerLocal>>>>>开始替换缓存节点 [defId:{},new users:{},old users:{}]", defId, users, u);
                putListValue(defId, users);
            }
        } else {
            log.info("| - CacheActionUserAssignerLocal>>>>>开始缓存节点  [defId:{},users:{}]", defId, users);
            putListValue(defId, users);
            putTotalValue(defId, 0);
        }
    }


}
