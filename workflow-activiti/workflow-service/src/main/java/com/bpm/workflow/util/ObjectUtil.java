package com.bpm.workflow.util;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.vo.PageDataRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ObjectUtil {

	public static <T> T copyBeanProp(Object soruce, Class<T> clazz) {
		try {
			T targetBean = clazz.newInstance();
			BeanUtil.copyProperties(soruce, targetBean);
			return targetBean;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> List<T> copyBeanPropList(List<?> soruce, Class<T> clazz) {
		List<T> list = new ArrayList<T>(soruce.size());
		for(Object obj:soruce) {
			list.add(copyBeanProp(obj, clazz));
		}
		return list;
	}

	public static <T> PageData<T> copyBeanPropPage(PageData<?> pageData, Class<T> clazz) {
		PageData<T> targetPageData = new PageData<T>();
		targetPageData.setCurPage(pageData.getCurPage());
		targetPageData.setPageRows(pageData.getPageRows());
		targetPageData.setStartRows(pageData.getStartRows());
		targetPageData.setTotalRows(pageData.getTotalRows());
		targetPageData.setResult(copyBeanPropList(pageData.getResult(), clazz));
		return targetPageData;
	}

	public static <T> PageData<T> getPageData(PageDataRequest pageRequest, Class<T> clazz) {
		PageData<T> pageData = new PageData<>();
		pageData.setStartRows((pageRequest.getStart() - 1) * pageRequest.getLimit());
		pageData.setPageRows(pageRequest.getLimit());
		return pageData;
	}

	public static void  copyPageData(PageData<?> result, PageData<?> input) {
		try {
			BeanUtil.copyProperties(result, input);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 返回报文的公共解析方式
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> unPackMessage(String response) {

		Map<String, Object> responseMap = JSONObject.parseObject(response);
		if(responseMap != null){
			Map<String, Object> data = (Map<String, Object>) responseMap.get("data");
			if(data != null){
				return (Map<String, Object>) data.get("mapRspData");
			}
		}
		return null;
	}
}
