package com.bpm.workflow.cache;

import cn.hutool.core.util.StrUtil;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import java.util.List;
import java.util.Objects;

@Log4j2
public class EhCacheUtil implements ICacheHandlerUtil {

	@Resource
	private CacheManager cacheManager;

	@Resource
	private Cache cache;

	/**
	 * 获取Ehcache对象
	 *
	 * @param cacheName
	 * @return
	 */

	public Cache getEhcache(String cacheName) {
		return cacheManager.getCache(cacheName);
	}

	/**
	 * 获取缓存
	 *
	 * @param key
	 * @return
	 */

	public Object getCache(String key) {
		if (null == cache || Objects.isNull(cache.get(key))) {
			return null;
		}
		return cache.get(key).getObjectValue();
	}

	/**
	 * 保存缓存--没有则创建一个
	 *
	 * @param key
	 * @param value
	 */

	public void putCache(String key, Object value) {
		cache.put(new Element(key, value));
	}

	@Override
	public void removeAll(String keyPrefix) {
		@SuppressWarnings("rawtypes")
		List cacheKeys = cache.getKeys();
		if (Objects.isNull(cacheKeys)) {
			return;
		}
		for (Object key : cacheKeys) {
			if (StrUtil.containsIgnoreCase(String.valueOf(key), keyPrefix)) {
				remove(String.valueOf(key));
			}
		}
		log.info("|-EhCacheUtil>>>>>>[removeAll] :{}", keyPrefix);
	}

	/**
	 * 删除缓存
	 */

	@Override
	public void remove(String key) {
		if (null != cache || Objects.nonNull(cache.get(key))) {
			cache.remove(key);
		} else {
			log.info("|-EhCacheUtil>>>>>>cache is null...");
		}
	}

	/**
	 * 替换缓存
	 *
	 * @param key
	 * @param value
	 */

	public void replaceCache(String key, Object value) {
		if (null == cache) {
			return;
		}
		if (Objects.nonNull(cache.get(key))) {
			cache.replace(new Element(key, value));
		} else {
			putCache(key, value);
		}
	}

	/**
	 * 关闭缓存
	 */

	public void shutDownCache() {
		cacheManager.shutdown();
	}

	@Override
	public void updateAllObject(String pattern, String key, Object value) {
		this.removeAll(pattern);
		this.putCache(key, value);
	}

	@Override
	public Object getObject(final String key) {
		return this.getCache(key);
	}

	@Override
	public void updateObject(String cacheKey, Object value) {
		this.replaceCache(cacheKey, value);
	}

	@Override
	public void updateElement(String key, org.dom4j.Element element) {
		if (this.hasKey(key)) {
			this.replaceCache(key, element);
			log.info("|-EhCacheUtil>>>>>>cache updateElement :{}" , key);
		}
	}

	@Override
	public <T> List<T> getCacheList(String key) {
		@SuppressWarnings("unchecked")
		List<T> dataList = (List<T>) this.getCache(key);
		log.info("|-EhCacheUtil>>>>>>cache getCacheList :{}" , key);
		return dataList;
	}

	@Override
	public void updateCacheList(String k, List<?> v) {
		this.replaceCache(k, v);
		log.info("|-EhCacheUtil>>>>>>cache updateCacheList :{}" , k);
	}

	@Override
	public boolean hasKey(String key) {
		Object obj = this.getCache(key);
		if (Objects.nonNull(obj)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public org.dom4j.Element getElement(String key) {
		String value = (String) this.getObject(key);
		if (StrUtil.isBlank(value)) {
			return null;
		}
		org.dom4j.Element element = (org.dom4j.Element) SerializeUtil.strUnSerializeToObj(value);
		return element;
	}
}
