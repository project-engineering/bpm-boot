package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfSuspendLog;
import com.bpm.workflow.mapper.WfSuspendLogMapper;
import com.bpm.workflow.service.WfSuspendLogService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfSuspendLogServiceImpl extends ServiceImpl<WfSuspendLogMapper, WfSuspendLog> implements WfSuspendLogService {
    @Resource
    private WfSuspendLogMapper wfSuspendLogMapper;
    @Override
    public void insertWfSuspendLog(WfSuspendLog suspendlog) {
        wfSuspendLogMapper.insert(suspendlog);
    }
}
