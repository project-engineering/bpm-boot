package com.bpm.workflow.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowDefinitionData;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.vo.DefinitionQueryRequest;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  流程定义表服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfDefinitionService extends IService<WfDefinition> {

    void addWorkFlow(WorkFlowDefinitionData definitionData);

    WfDefinition selectByDefIdVersion(WfDefinition wfDefinition);

    List<WfDefinition> selectByDefId(String processDefId);

    WfDefinition selectActiveFlowByDefId(String processCode);

    int updateByDefId(WfDefinition wfDefinition);

    int deleteByDefIdDefName(WfDefinition wfDefinition);

    int deleteByDefId(String defId);

    int updateByDefIdDefName(WfDefinition wfDefinition);

    void insertWfDefinition(WfDefinition wfDefinition);

    int updateByKeyId(WfDefinition wfDefinition);

    void deleteWorkFlow(int i, String defId);

    void updateWorkFlow(WorkFlowDefinitionData definitionData);

    IPage<WfDefinition> queryWorkFlowList(DefinitionQueryRequest definitionQueryRequest);

    WfDefinition viewWorkFlow(WorkFlowDefinitionData data);

    boolean saveWorkFlowDefinition(WorkFlowDefinitionData definitionData);

    void deployWorkFlowByDefId(String defId);

    List<WfDefinition> selectVersionList(String defId, PageData<Object> objectPageData);

    void backHistVersion(WorkFlowDefinitionData definitionData);

    List<WfDefinition> selectWfDefinitionByXmlSource(String xmlSource);
}
