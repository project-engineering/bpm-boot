package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.ServiceNameConstants;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.CacheUtil;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class CacheActionWfModelpropExtendsLocal extends CacheActionBase<WfModelPropExtends>{
	@Resource
	private WfModelPropExtendsService wfModelPropExtendsService;


	@Override
	public WfModelPropExtends getCacheValueByKey(String selectMethodName,String firstKey, String secondKey) {
		if(ServiceNameConstants.SELECT_BY_OWN_ID_PROP_NAME.equals(selectMethodName)) {
			return selectByOwnIdPropName(firstKey, secondKey);
		}else {
			throw new RuntimeException("not support select method:"+selectMethodName);
		}
	}
	
	@Override
	public List<WfModelPropExtends> getCacheListValueByKey(String selectMethodName,String firstKey, String secondKey) {
		if(ServiceNameConstants.SELECT_MODEL_EXTENDS_PROP_BY_OWN_ID.equals(selectMethodName)) {
			return selectModelExtendsPropByOwnId(firstKey);
		}else {
			throw new RuntimeException("not support select method:"+selectMethodName);
		}
	}

	private WfModelPropExtends selectByOwnIdPropName(String ownId, String propName) {
		if(StrUtil.isEmpty(ownId)||StrUtil.isEmpty(propName)) {
			throw new RuntimeException("ownId and propName can't be null");
		}
		WfModelPropExtends result =super.getCacheValueByKey(null, new String[] {ownId,propName});
		if(result!=null && result.getPropName()==null) {
			return null;
		}
		if( result== null||(!CacheUtil.USE_CACHE)) {
			WfModelPropExtends thePara = new WfModelPropExtends();
			thePara.setOwnId(ownId);
			thePara.setPropName(propName);
			result = wfModelPropExtendsService.selectByOwnIdPropName(thePara);

			if(result!=null) {
				super.putCacheValueByKey(null,  new String[] {ownId,propName}, result);
			}else {
				super.putCacheValueByKey(null,  new String[] {ownId,propName}, new WfModelPropExtends());
			}
			

		}
		return result;
	}

	private List<WfModelPropExtends> selectModelExtendsPropByOwnId(String ownId) {
		if(StrUtil.isEmpty(ownId)) {
			throw new RuntimeException("ownId can not be null");
		}
		List<WfModelPropExtends> result =super.getCacheListValueByKey(null, new String[] {ownId});
		if( result== null||(!CacheUtil.USE_CACHE)) {
			result = wfModelPropExtendsService.selectModelExtendsPropByOwnId(ownId);
			if(result!=null) {
				super.putCacheListValueByKey(null,  new String[] {ownId}, result);
			}else {
				super.putCacheListValueByKey(null,  new String[] {ownId}, new ArrayList<WfModelPropExtends>());
			}
		}
		
		return result;
	}
}
