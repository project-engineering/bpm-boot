package com.bpm.workflow.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Base64;
import java.util.Properties;

public class EmailUtil extends SendMessageUtil {
	// 主机服务名称
	private static String hostService =null;// configUtil.getMailSmtpHost();// 例如：网易的主机服务为"smtp.126.com"

	private static String userName = null;//configUtil.getMailAuth();// 用户名

	private static String passWord = null;//configUtil.getMailPassWd();// 密码

	private static String sendPerson = null;//configUtil.getMailUser();// 邮件发送人

	private  final Logger log = LoggerFactory.getLogger(EmailUtil.class);

	/**
	 * 
	 * @param to  邮件收件人
	 * @param title   邮件标题
	 * @param msg     邮件内容信息
	 * @param filePath 附件路径
	 * @return
	 */
	@Override
	public boolean send(String to, String title, String msg, String filePath) {
		if (filePath == null) {
			filePath = "";
		}
			
		if (to == null){
			return false;
		}
		return sendMsg(sendPerson, to, title, msg, new File(filePath));
	}

	private boolean sendMsg(String from, String to, String title, String msg, File file) {
		Properties props = new Properties();

		// 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
		props.put("mail.smtp.host", hostService);
		// 需要经过授权，也就是有户名和密码的校验，这样才能通过验证（一定要有这一条）
		props.put("mail.smtp.auth", "true");

		// 用刚刚设置好的props对象构建一个session
		Session session = Session.getDefaultInstance(props);

		// 有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使
		// 用（你可以在控制台（console)上看到发送邮件的过程）
		// session.setDebug(true);

		// 用session为参数定义消息对象
		MimeMessage message = new MimeMessage(session);
		try {
			// 加载发件人地址
			message.setFrom(new InternetAddress(from));
			// 加载收件人地址
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			// 加载标题
			message.setSubject(title);

			// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
			Multipart multipart = new MimeMultipart();

			// 设置邮件的文本内容
			BodyPart contentPart = new MimeBodyPart();
			contentPart.setText(msg);
			multipart.addBodyPart(contentPart);
			// 添加附件
			if (file.isFile() && file.exists()) {
				BodyPart messageBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(file.getAbsolutePath());
				// 添加附件的内容
				messageBodyPart.setDataHandler(new DataHandler(source));
				// 添加附件的标题
				// 这里很重要，通过下面的Base64编码的转换可以保证你的中文附件标题名在发送时不会变成乱码
//				sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
//				messageBodyPart.setFileName("=?GBK?B?" + enc.encode(file.getName().getBytes()) + "?=");
				Base64.Encoder encoder = Base64.getEncoder();
				messageBodyPart.setFileName("=?GBK?B?" + encoder.encodeToString(file.getName().getBytes()) + "?=");
				multipart.addBodyPart(messageBodyPart);
			}

			// 将multipart对象放到message中
			message.setContent(multipart);
			// 保存邮件
			message.saveChanges();
			// 发送邮件
			Transport transport = session.getTransport("smtp");
			// 连接服务器的邮箱
			transport.connect(hostService, userName, passWord);
			// 把邮件发送出去
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			log.info("| - EmailUtil>>>>>send email success to {}",to);
			return true;
		} catch (Exception e) {
			log.error("| - EmailUtil>>>>>send email fail:{}",e.getMessage(),e);
		}
		return false;
	}

	public static String getHostService() {
		return hostService;
	}

	public static void setHostService(String hostService) {
		EmailUtil.hostService = hostService;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		EmailUtil.userName = userName;
	}

	public static String getPassWord() {
		return passWord;
	}

	public static void setPassWord(String passWord) {
		EmailUtil.passWord = passWord;
	}

	public static String getSendPerson() {
		return sendPerson;
	}

	public static void setSendPerson(String sendPerson) {
		EmailUtil.sendPerson = sendPerson;
	}

}
