package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfUploadDefinition;
import com.bpm.workflow.mapper.WfUploadDefinitionMapper;
import com.bpm.workflow.service.WfUploadDefinitionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfUploadDefinitionServiceImpl extends ServiceImpl<WfUploadDefinitionMapper, WfUploadDefinition> implements WfUploadDefinitionService {

}
