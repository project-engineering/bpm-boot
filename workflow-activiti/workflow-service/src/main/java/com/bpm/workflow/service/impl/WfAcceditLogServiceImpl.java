package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfAcceditLog;
import com.bpm.workflow.mapper.WfAcceditLogMapper;
import com.bpm.workflow.service.WfAcceditLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfAcceditLogServiceImpl extends ServiceImpl<WfAcceditLogMapper, WfAcceditLog> implements WfAcceditLogService {

}
