package com.bpm.workflow.util.constant;

import com.bpm.workflow.dto.transfer.UserOutputData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class BpmConstants {

    /**
     * bpm文件后缀名-小写
     */
    public static final String BPM_FILE_SUFFIX_LOWER = ".bpmn";
    /**
     * bpm文件后缀名-大写
     */
    public static final String BPM_FILE_SUFFIX_UPPER = ".BPMN";

    /**
     * application/json;charset=UTF-8
     */
    public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";

    /**
     * 业务参数
     */
    public static final String PARAM_FLAG_1 = "1";

    /**
     * 追加参数
     */
    public static final String PARAM_FLAG_2 = "2";

    /**
     * 标准参数
     */
    public static final String PARAM_FLAG_3 = "3";

    /**
     * myBody
     */
    public static final String MY_BODY = "myBody";

    /**
     * 标准参数
     */
    public static final String QUERY = "query";

    /**
     * 特定taskId，登记记录时使用
     */
    public static final String TASK_ID_000001 = "000001";

    /**
     * 任务状态-未处理
     */
    public static final String WORK_STATE_0 = "0";

    /**
     * 任务状态-已处理
     */
    public static final String WORK_STATE_1 = "1";

    /**
     * 开始节点taskId
     */
    public static final String START_TASK_ID = "00000";

    /**
     *
     */
    public static final String DO_RESULT_2 = "2";

    /**
     * 退回
     */
    public static final String DO_RESULT_4 = "4";
    /**
     * 撤回
     */
    public static final String DO_RESULT_5 = "5";

    /**
     * 退回（无需重新审批）
     */
    public static final String DO_RESULT_7 = "7";

    /**
     * attachmentMap
     */
    public static final String ATTACHMENT_MAP = "attachmentMap";

    /**
     * 流程编码
     */
    public static final String PROCESS_CODE = "processCode";

    /**
     * 任意跳转
     */
    public static final String TRAN_TYPE_M3 = "M3";
    /**
     * 可打回节点查询
     */
    public static final String TRAN_TYPE_M8 = "M8";

    /**
     * 加签类型
     */
    public static final String ADD_TASK_TYPE_1 = "1";

    /**
     * 检入标识
     */
    public static final String CHECK_IN_FLAG_3 = "3";

    /**
     * 处理人重复自动处理-0-不自动处理
     */
    public static final String FLOW_AUTO_REPEAT_PROCESS_0 = "0";

    /**
     * 处理人重复自动处理-1-部分人员适配处理
     */
    public static final String FLOW_AUTO_REPEAT_PROCESS_1 = "1";

    /**
     * 处理人重复自动处理-2-全部人员适配处理
     */
    public static final String FLOW_AUTO_REPEAT_PROCESS_2 = "2";

    /**
     *
     */
    public static final int AUTO_EXEC_FLAG_2 = 2;

    /**
     *
     */
    public static final int AUTO_EXEC_FLAG_3 = 3;

    /**
     * 第三方接口调用标识
     */
    public static final String THIRD_PARTY_INTERFACE_CALL_FLAG_1 = "1";

    /**
     * 下节点未处理可追回
     */
    public static final String CRTL_TYPE_0 = "0";

    /**
     * range
     */
    public static final String RANGE = "range";

    /**
     * append
     */
    public static final String APPEND = "append";

    /**
     * revoke
     */
    public static final String REVOKE = "revoke";

    /**
     * todoMap
     */
    public static final String TODO_MAP = "todoMap";

    /**
     * checkOut
     */
    public static final String CHECK_OUT = "checkOut";

    /**
     * checkIn
     */
    public static final String CHECK_IN = "checkIn";

    /**
     * alarmTime
     */
    public static final String ALARM_TIME = "alarmTime";

    /**
     * fullPath
     */
    public static final String FULL_PATH = "fullPath";


    /**
     * timeoutTime 超时时间
     */
    public static final String TIMEOUT_TIME = "timeoutTime";

    /**
     * timeUnit 超时时间单位 0-时 1-天
     */
    public static final String TIME_UNIT = "timeUnit";

    /**
     * msgTemplateNo 消息模板编号
     */
    public static final String MSG_TEMPLATE_NO = "msgTemplateNo";

    /**
     * overtimeExecution 超时处理机制
     */
    public static final String OVERTIME_EXECUTION = "overtimeExecution";

    /**
     * 超时时间单位 0-时
     */
    public static final String TIME_UNIT_HOUR = "0";

    /**
     * 超时时间单位 1-天
     */
    public static final String TIME_UNIT_DAY = "1";

    /**
     * 超时时间单位 2-秒
     */
    public static final String TIME_UNIT_SECOND = "2";

    /**
     * 超时标识
     */
    public static final String TIMEOUT_FLAG_1 = "1";

    /**
     * 超时标识
     */
    public static final String TIMEOUT_FLAG_0 = "0";

    /**
     * 规则标识
     */
    public static final String RULE_NO = "ruleNo";

    /**
     * STR_USER_FLAG_1
     */
    public static final String STR_USER_FLAG_1 = "1";

    /**
     * ALLOCATION_TYPE_0
     */
    public static final String ALLOCATION_TYPE_0 = "0";

    /**
     * timeOut
     */
    public static final String TIME_OUT = "timeOut";
    /**
     * timeOut-0
     */
    public static final String TIME_OUT_0 = "0";

    /**
     * 未生效
     */
    public static final String ACCEDIT_RECORD_STATE_0 = "0";

    /**
     * sendId
     */
    public static final String SEND_ID = "sendId";

    /**
     * rule.
     */
    public static final String RULE_ = "rule.";

    /**
     * routeRule
     */
    public static final String ROUTE_RULE = "routeRule";

    /**
     * restTemplate
     */
    public static final String REST_TEMPLATE = "restTemplate";

    /**
     * useAskList
     */
    public static final String USE_ASK_LIST = "useAskList";

    /**
     * doRemark
     */
    public static final String DO_REMARK = "doRemark";

    /**
     * Firefox
     */
    public static final String FIREFOX = "Firefox";

    /**
     * Event
     */
    public static final String EVENT = "Event";

    /**
     * paramType
     */
    public static final String PARAM_TYPE = "paramType";

    /**
     * id
     */
    public static final String ID = "id";

    /**
     * operate
     */
    public static final String OPERATE = "operate";

    /**
     * xmlSource
     */
    public static final String XML_SOURCE = "xmlSource";

    /**
     * extensionElements
     */
    public static final String EXTENSION_ELEMENTS = "extensionElements";

    /**
     * 0
     */
    public static final String ACCEDIT_RECORD_TYPE_0 = "0";
    /**
     * 1
     */
    public static final String ACCEDIT_RECORD_TYPE_1 = "1";

    /**
     * 正常
     */
    public static final String TASK_URGENT_0 = "0";
    /**
     * 紧急
     */
    public static final String TASK_URGENT_1 = "1";

    /**
     * 参数类型
     */
    public static final String PARAM_OBJ_TYPE_1 = "1";
    /**
     * 参数类型
     */
    public static final String PARAM_OBJ_TYPE_2 = "2";

    /**
     * 管理员标识
     */
    public static final String USER_MANAGER_FLAG_1 = "1";

    /**
     * SYS_FLAG_0
     */
    public static final String SYS_FLAG_0 = "0";

    /**
     * OPERATE_1
     */
    public static final String OPERATE_1 = "1";

    /**
     * 1-已查看
     */
    public static final String IS_VIEW_1 = "1";
    /**
     * 0-未查看
     */
    public static final String IS_VIEW_0 = "0";

    /**
     * 0-存储字段
     */
    public static final String SHOW_CONFIG_0 = "0";
    /**
     * 1-查询条件
     */
    public static final String SHOW_CONFIG_1 = "1";
    /**
     * 2-展示字段
     */
    public static final String SHOW_CONFIG_2 = "2";
    /**
     * 3-是否展示机构字段
     */
    public static final String SHOW_CONFIG_3 = "3";

    /**
     * dataMap
     */
    public static final String DATA_MAP_KEY = "dataMap";
    /**
     * targetKey
     */
    public static final String TARGET_KEY = "targetKey";
    /**
     * sourceKey
     */
    public static final String SOURCE_KEY = "sourceKey";

    /**
     * 工作流-系统启动流程工号
     */
    public static final String WORKFLOW_ADMIN_ID = "sys-auto";

    /**
     * 系统启动发起信息
     */
    public static final Map<String, Object> ADMIN_START_MAP = new HashMap<>(2);
    static {
        ADMIN_START_MAP.put("userId", "sys-auto");
        ADMIN_START_MAP.put("userName", "系统自动");
    }

    /**
     * 系统启动发起信息
     */
    public static final List<UserOutputData> SYS_USER_INFO = new ArrayList<>();
    static {
        UserOutputData userOutputData = new UserOutputData();
        userOutputData.setUserId("sys-auto");
        userOutputData.setUserName("系统自动");
        SYS_USER_INFO.add(userOutputData);
    }

    /**
     * 审批结果key
     */
    public static final String APPROVAL_RESULT_KEY = "approve";
}
