package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfPropConfig;
import com.bpm.workflow.mapper.WfPropConfigMapper;
import com.bpm.workflow.service.WfPropConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfPropConfigServiceImpl extends ServiceImpl<WfPropConfigMapper, WfPropConfig> implements WfPropConfigService {

}
