package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfTemplate;
import com.bpm.workflow.mapper.WfTemplateMapper;
import com.bpm.workflow.service.WfTemplateService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfTemplateServiceImpl extends ServiceImpl<WfTemplateMapper, WfTemplate> implements WfTemplateService {
    @Resource
    private WfTemplateMapper wfTemplateMapper;

    @Override
    public WfTemplate viewTemplate(String templateId) {
        LambdaQueryWrapper<WfTemplate> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfTemplate::getTemplateId, templateId);
        return wfTemplateMapper.selectOne(queryWrapper);
    }

    @Override
    public List<WfTemplate> selectTemplateList(Map<String, Object> param) {
        LambdaQueryWrapper<WfTemplate> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfTemplate::getType, param.get("type"));
        return wfTemplateMapper.selectList(queryWrapper);
    }
}
