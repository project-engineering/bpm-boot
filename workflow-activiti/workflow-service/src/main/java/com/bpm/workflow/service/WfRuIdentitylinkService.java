package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfRuIdentitylink;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfRuIdentitylinkService extends IService<WfRuIdentitylink> {

    int deleteNotActive();

    void insertWfRuIdentitylink(WfRuIdentitylink wfRuIdentitylink);

    void deleteByTaskId(WfRuIdentitylink wfRuIdentitylink);

    void deleteByUserIdTaskId(WfRuIdentitylink para);

    WfRuIdentitylink selectByUserIdTaskId(WfRuIdentitylink wfRuIdentitylink);
}
