package com.bpm.workflow.timertask;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.service.WfMessageService;
import jakarta.annotation.Resource;

public class WfDisposeMessageDataTask extends WorkflowTimerTask {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3585639984613316263L;
	private String processInstanceId;
	@Resource
	private WfMessageService wfMessageService;

	/**
	 * 
	 * @param processInstId
	 */
	public WfDisposeMessageDataTask(String processInstId) {
		this.processInstanceId = processInstId;
	}

	@Override
	public void runImpl() throws Exception {
		if (StrUtil.isBlank(processInstanceId)) {
			return;
		}
		wfMessageService.deleteByTaskId(processInstanceId);
	}

}
