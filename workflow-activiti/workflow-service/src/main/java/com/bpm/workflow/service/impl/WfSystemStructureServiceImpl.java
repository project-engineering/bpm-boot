package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfSystemStructure;
import com.bpm.workflow.mapper.WfSystemStructureMapper;
import com.bpm.workflow.service.WfSystemStructureService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfSystemStructureServiceImpl extends ServiceImpl<WfSystemStructureMapper, WfSystemStructure> implements WfSystemStructureService {
    @Resource
    private WfSystemStructureMapper wfSystemStructureMapper;

    @Override
    public WfSystemStructure selectByKeyId(int id) {
        return wfSystemStructureMapper.selectById(id);
    }
}
