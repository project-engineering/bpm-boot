package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfMessage;
import com.bpm.workflow.mapper.WfMessageMapper;
import com.bpm.workflow.service.WfMessageService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfMessageServiceImpl extends ServiceImpl<WfMessageMapper, WfMessage> implements WfMessageService {
    @Resource
    private WfMessageMapper wfMessageMapper;

    @Override
    public void deleteByTaskId(String taskId) {
        LambdaQueryWrapper<WfMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfMessage::getTaskId,taskId);
        wfMessageMapper.delete(queryWrapper);
    }

    @Override
    public void updateByKeyId(WfMessage wfMessage) {
        wfMessageMapper.updateById(wfMessage);
    }
}
