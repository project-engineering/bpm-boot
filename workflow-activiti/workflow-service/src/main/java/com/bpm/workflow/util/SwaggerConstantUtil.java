package com.bpm.workflow.util;
 
/**
 * //api的标题、描述、版本等信息
 * @author anmingtao
 *
 */
public class SwaggerConstantUtil {

	public static final String QUERY_TASK_DO_RESULT_LIST_VALUE = "任务处理结果";
	public static final String QUERY_TASK_DO_RESULT_LIST_NOTES ="任务处理结果";
	public static final String QUERY_TASK_DO_RESULT_LIST_PARAM ="";

	public static final String START_WORKFLOW_DEF_VALUE ="流程启动";
	public static final String START_WORKFLOW_DEF_NOTES = "根据流程编码启动对应流程,当没有传递流程编码时，根据大类systemIdCode查找对应的流程编码进行启动，如果没有上送系统大类根据规则进行计算流程编码,详细实现见<a href=\"http://192.168.30.41/BRM_cpghsyb/bpm/blob/activiti_cloud_dev/springcloud-workflow/workflow-activiti/doc/md/startWorkflowDef.md\">文档</a>";
	public static final String START_WORKFLOW_DEF_PARAM = "例：启动流程编码为GFZFCLC的流程，并指定下一个流转路径为sid-8DD97F16-C2F7-4FD0-A5AE-42636B30C277，<br/><code>	\"bussinessMap\":{	\r\n 		\"approvalStatus\":\"000\",	 \r\n 		\"specifyNextNode\":\"sid-8DD97F16-C2F7-4FD0-A5AE-42636B30C277\",\r\n 		\"branch\":\"1\",\r\n 		\"bussinessSave\":{\"query_Indexc\":\"000\",\"query_Indexa\":\"12030001\"}\r\n 	},\r\n 	\"processCode\":\"GFZFCLC\",\r\n 	\"userIdMap\":{\"userId\":\"0500\"}</code>";
	
	public static final String NEXT_TASK_VALUE = "流程流转到下一步";
	public static final String NEXT_TASK_NOTES = "根据任务编码taskId进行流转,处理人必须拥有审批taskId的权限，可以通过specifyNextOpers指定下一个节点处理人，specifyNextNode指定下一个节点的处理分支，否则系统根据配置的规则进行匹配。";
	public static final String NEXT_TASK_PARAM = "	例：流程流转<br/><code>\"bussinessMap\":{ \r\n 		\"custNo\":\"1000010001\",\r\n 		\"custName\":\"ww\", \r\n 		\"productName\":\"个人住房按揭贷款\",\r\n 		\"custRequestNo\":\"1260101\",\r\n 		\"businessType\":\"03\",\r\n 		\"productNo\":\"12030001\",	\r\n 		\"branch\":\"1\",\r\n  		\"bussinessSave\":{\r\n 			\"query_Indexc\":\"000\",\r\n 			\"query_Indexa\":\"12030001\"\r\n 		}\r\n 	},\r\n 	\"approveMap\":{\r\n 			\"result\":\"0\",\r\n 			\"info\":\"通过\"\r\n 		},\r\n 	\"taskId\":\"20690260\",\r\n 	\"userId\":\"0510\"</code>";
	
	public static final String USER_TASK_MANAGE_VALUE ="任务管理";
	public static final String USER_TASK_MANAGE_NOTES ="注意维护任务的终止、回退、打回等操作";
	public static final String USER_TASK_MANAGE_PARAM ="例：任务20690206跳转到sid-6B58CF10-C52F-4F09-A338-2BC6B727B9AA节点，可以通过findNextNodeInfo查询打回的节点，用于直接跳转<br/><code>  \"userId\": \"1444\",\r\n   \"taskId\": \"20690206\",\r\n    \"tranType\": \"B3\",\r\n    \"backDirect\":\"1\",\r\n    \"targetId\":\"sid-6B58CF10-C52F-4F09-A338-2BC6B727B9AA\"</code>";

	public static final String TASK_MANAGE_VALUE ="任务管理";
	public static final String TASK_MANAGE_NOTES ="用于流程的挂起、恢复、催办、终止等操作";
	public static final String TASK_MANAGE_PARAM ="例：挂起流程实例为5890001的流程实例<br/><code>\r\n  \"userId\": \"0500\",\r\n  \"processId\": \"5890001\",\r\n   \"tranType\": \"M4\"</code>";

	public static final String TASK_CHECK_IN_OUT_VALUE ="检入检出";
	public static final String TASK_CHECK_IN_OUT_NOTES ="用来实现任务的检入检出操作";
	public static final String TASK_CHECK_IN_OUT_PARAM ="例：0500检出任务7825006，则其他人不能看到<br><code>	\"tranType\":\"C1\",\r\n 	\"taskId\":\"7825006\",\r\n 	\"userId\":\"0500\"</code>";

	public static final String TASK_BATCH_CHECK_IN_VALUE ="待领用任务批量指定处理人员接口";
	public static final String TASK_BATCH_CHECK_IN_NOTES ="待领用任务批量指定处理人员接口<br>对于需userId领用但还未被领用的待办任务taskIdList[taskId1,taskId2]，批量指定处理的人员userIdMap";
	public static final String TASK_BATCH_CHECK_IN_PARAM ="例:用户0500领用任务19340063，19340005，17165007 <br/><code> \"userId\":\"0500\",\r\n   \"taskIdList\":[{\"taskId\":\"19340063\"},{\"taskId\":\"19340005\"},{\"taskId\":\"17165007\"}],\r\n   \"userIdMap\":{\r\n   	\"userId\":\"0500\"  }</code>";
	
	

	public static final String GET_SHOW_PROCESS_URL_VALUE ="获取流程图地址";
	public static final String GET_SHOW_PROCESS_URL_NOTES ="返回流程对应的url";
	public static final String GET_SHOW_PROCESS_URL_PARAM ="例:流程设计器中查看流程编码为19315026的流程实例 ，其中流程实例的访问地址为100.50.1.126:9001/workflow-app/editor/#/showProcesses <br><code> \"processId\":\"19315026\", \r\n       \"urlIpAddr\":\"100.50.1.126:9001\"</code>";
	
	
	public static final String QUERY_UN_DO_TASK_LIST_VALUE ="代办任务查询";
	public static final String QUERY_UN_DO_TASK_LIST_NOTES ="查询指定条件下的待办任务信息<br>可通过管理界面进行查看";
	public static final String QUERY_UN_DO_TASK_LIST_PARAM ="";
	
	public static final String ADDITION_TASK_VALUE ="加签";
	public static final String ADDITION_TASK_NOTES ="任务加签";
	public static final String ADDITION_TASK_PARAM ="例如处理人为0287的任务【20690224】加签用户1333：<br/> <code>\"taskType\":\"1\",\r\n	\"taskId\":\"20690224\",\r\n	\"targetUserId\":\"1333\",\r\n\"userId\":\"0287\"</code>";
	
	public static final String FIND_NEXT_NODE_INFO_VALUE ="查找下一个节点处理人";
	public static final String FIND_NEXT_NODE_INFO_NOTES ="根据业务参数和任务ID，获取下一节点的选人列表";
	public static final String FIND_NEXT_NODE_INFO_PARAM ="例如查找用户1333的任务【20690224】的下一个节点的处理人及节点信息：<br/><code> \"taskId\":\"20690224\", \"userIdMap\":{\"userId\":\"1333\"}, \"bussinessMap\":{\"money\":11105,\"id\":\"1\",\"signResult\":\"0\" </code>";
	
	
	
	
	
}
