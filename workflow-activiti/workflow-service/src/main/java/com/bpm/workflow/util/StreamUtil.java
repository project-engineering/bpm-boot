package com.bpm.workflow.util;

import java.io.*;


public class StreamUtil {
    
	public static String serializeObjInStr(Serializable obj) {
		return BpmStringUtil.formatByteInBase64(serializeObj(obj));
	}
	public static byte[] serializeObj(Serializable obj) {
		try(
				ByteArrayOutputStream byteos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(byteos);				
				) {
			oos.writeObject(obj);
			return  byteos.toByteArray();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Object deSerializeObjByStr(String serialObjStr) {
		return deSerializeObj(BpmStringUtil.parseBas64String(serialObjStr));
	}
	public static Object deSerializeObj(byte[] objBytes) {		
		try(
				ByteArrayInputStream byteis = new ByteArrayInputStream(objBytes);
				ObjectInputStream ois = new ObjectInputStream(byteis);
				) {
			return ois.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
