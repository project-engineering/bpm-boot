package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfHistTaskflow;
import com.bpm.workflow.mapper.WfHistTaskflowMapper;
import com.bpm.workflow.service.WfHistTaskflowService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfHistTaskflowServiceImpl extends ServiceImpl<WfHistTaskflowMapper, WfHistTaskflow> implements WfHistTaskflowService {
    @Resource
    private WfHistTaskflowMapper wfHistTaskflowMapper;
    @Override
    public void insertWfHistTaskflow(WfHistTaskflow wfHistTaskflow) {
        wfHistTaskflowMapper.insert(wfHistTaskflow);
    }

    @Override
    public void updateByTaskId(WfHistTaskflow wfHistTaskflow) {
        wfHistTaskflowMapper.updateById(wfHistTaskflow);
    }

    @Override
    public WfHistTaskflow selectByTaskId(String taskId) {
        LambdaQueryWrapper<WfHistTaskflow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfHistTaskflow::getTaskId,taskId);
        return wfHistTaskflowMapper.selectOne(queryWrapper);
    }
}
