package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.engine.command.JumpCommand;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.service.CacheDaoSvc;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.interceptor.CommandExecutor;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.dom4j.Element;
import java.math.BigInteger;
import java.util.*;


public class GatewayUtil {
    
    private static Map<BigInteger,Map<String,GatewayInfo>> processDefId2GatewayInfosByUseDate = null;
    public static synchronized void addGatewayInfo(BigInteger version,String processDefId, GatewayInfo gatewayInfo) {
    	if(processDefId2GatewayInfosByUseDate== null) {
    		processDefId2GatewayInfosByUseDate=new HashMap<BigInteger,Map<String,GatewayInfo>>();
    	}
    	if(processDefId2GatewayInfosByUseDate.get(version ) ==  null) {
    		processDefId2GatewayInfosByUseDate.put(version, new HashMap<String,GatewayInfo>());
    	}
    	processDefId2GatewayInfosByUseDate.get(version).put(processDefId, gatewayInfo);
    }
    public static GatewayInfo getGatewayInfo(String processDefId,BigInteger version) {
    	return getGatewayInfo(null,null,processDefId, version);
    }
    public static synchronized  GatewayInfo getGatewayInfo(CacheDaoSvc cacheDaoSvc, String processXmlString, String processDefId, BigInteger version) {
//    	log.info("=====processDefId2GatewayInfosByUseDate==="+processDefId2GatewayInfosByUseDate);
    	if(processDefId2GatewayInfosByUseDate == null) {
    		processDefId2GatewayInfosByUseDate=new HashMap<BigInteger,Map<String,GatewayInfo>>();
    	}

//    	Map<String,GatewayInfo> processDefId2GatewayInfos =  processDefId2GatewayInfosByUseDate.get(version);
//    	log.info("=====processDefId2GatewayInfos==="+processDefId2GatewayInfos);
    	if(!
    			(processDefId2GatewayInfosByUseDate.get(version) == null || 
    			processDefId2GatewayInfosByUseDate.get(version).size()==0||
    			processDefId2GatewayInfosByUseDate.get(version).get(processDefId) == null)
    			){
    		return processDefId2GatewayInfosByUseDate.get(version).get(processDefId);
    	}
    	
		if(StrUtil.isEmpty(processXmlString)) {
//			WfDefinition param = new WfDefinition();
//			param.setDefId(processDefId);
//			param.setVersion(version);
//			
//			WfDefinition wfDefinition = wfDefinitionMapper.selectByDefIdActVersion(param);
			WfDefinition wfDefinition = cacheDaoSvc.selectByDefIdActVersion(processDefId,version);
//			log.info("=========+++++++========="+wfDefinition);
			XmlUtil.initGatewayInfo(version, wfDefinition.getXmlSource());
		}else {
			XmlUtil.initGatewayInfo(version, processXmlString);
		}
		return processDefId2GatewayInfosByUseDate.get(version).get(processDefId);
    }
    
    public static String getStartGatewayDefId(String processDefId,BigInteger version,String taskDefId) {
    	GatewayInfo gatewayInfo= getGatewayInfo(processDefId,version);
    	if(gatewayInfo == null||gatewayInfo.getNodeInfo(taskDefId) == null) {
    		return null;
    	}
    	return gatewayInfo.getNodeInfo(taskDefId).getBelongStartGatewayNodeId();
    }
    
    public static String getEndGatewayDefId(String processDefId,BigInteger version,String taskDefId) {
    	GatewayInfo gatewayInfo= getGatewayInfo(processDefId,version);
    	if(gatewayInfo == null||gatewayInfo.getNodeInfo(taskDefId) == null) {
    		return null;
    	}
    	return gatewayInfo.getNodeInfo(taskDefId).getBelongEndGatewayNodeId();
    }
    
    public static final String[] GATEWAY_TYPE = new String[] {"inclusiveGateway","exclusiveGateway","parallelGateway","eventBasedGateway"};

	public static boolean isGatewayNode(Element element) {
		return BpmStringUtil.isResultIn(element.getName(), GATEWAY_TYPE);
	}
	
	public static void completTask(CacheDaoSvc cacheDaoSvc,String processXmlString,ProcessEngine processEngine,Task theTask,
			Map<String, Object> variables) {
		completTask(cacheDaoSvc,processXmlString,processEngine, theTask.getTaskDefinitionKey(), theTask.getProcessInstanceId(),XmlUtil.getProcessVersionByDefId(theTask.getProcessDefinitionId()),variables);
	}
	public static void completTask(CacheDaoSvc cacheDaoSvc,String processXmlString,ProcessEngine processEngine,String taskDefId, String processInstanceId,BigInteger version,
			Map<String, Object> variables) {
		completTask(cacheDaoSvc,processXmlString,processEngine,  taskDefId,  processInstanceId, null, version,variables);
	}
	public static void completTask(CacheDaoSvc cacheDaoSvc,String processXmlString,ProcessEngine processEngine, String taskDefId, String processInstanceId,String processDefId,BigInteger version,
			Map<String, Object> variables) {
		GatewayInfo curGatewayInfo = null;
		if(StrUtil.isEmpty(processDefId)||version ==null) {
			ProcessInstance processInstance = processEngine.getRuntimeService().createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
			if(processInstance == null) {
				return;
			}
			processDefId = processInstance.getProcessDefinitionId();
			if(processDefId.indexOf(SymbolConstants.COLON)>0) {
				processDefId = processDefId.substring(0,processDefId.indexOf(":"));
			}
			
			version =BigInteger.valueOf(processInstance.getProcessDefinitionVersion());
		}
		curGatewayInfo = getGatewayInfo( cacheDaoSvc, processXmlString,processDefId,version);
		NodeInfo curNode = curGatewayInfo.getNodeInfo(taskDefId);
		if(!curNode.hasNeighborNode(false)) {
			return;
		}
		NodeInfo nextJoinEndGateway = null;
		for(String nextNodeId:curNode.getNeighborNodes(false)) {
			NodeInfo nextNode= curGatewayInfo.getNodeInfo(nextNodeId);
			if((nextNode.isGatewayNode())&&(!nextNode.isStartGatewayNode())&&(nextNode.getGatewayJoinCount()> 0)) {
				nextJoinEndGateway = nextNode;
				break;
			}
		}
		if(nextJoinEndGateway == null) {
			return;
		}

		int doneTaskWithinThisGateway =0;
        for(Execution execution:processEngine.getRuntimeService().createExecutionQuery().processInstanceId(processInstanceId).list()) {            
			if(nextJoinEndGateway.getNodeId().equals(execution.getActivityId())) {
				doneTaskWithinThisGateway++;
			}
        }

		if(nextJoinEndGateway.getGatewayJoinCount()<=doneTaskWithinThisGateway) {
	        Set<Task> jumpTasks = null;
	        jumpTasks = setJumpVariables(processEngine, processInstanceId,
					curGatewayInfo, nextJoinEndGateway,jumpTasks);
	        if(jumpTasks.size()>0) {
				doJump(processEngine, processInstanceId, variables, curGatewayInfo, nextJoinEndGateway,
						jumpTasks);
	        }
		}
	}
	
	private static Set<Task> setJumpVariables(ProcessEngine processEngine, String processInstanceId,
			GatewayInfo curGatewayInfo, NodeInfo nextJoinEndGateway,
			Set<Task> jumpTasks) {
		List<Task> allTasks=null;
		if(jumpTasks == null) {
			jumpTasks= new HashSet<Task>();
		}
		allTasks =processEngine.getTaskService().createTaskQuery().processInstanceId(processInstanceId).list();			
		jumpTasks.clear();
		for(Task task:allTasks) {
			if(
					(!nextJoinEndGateway.getNodeId().equals(task.getTaskDefinitionKey()))
							&&
				(isNodeEndInThis(curGatewayInfo,nextJoinEndGateway.getNodeId(),task.getTaskDefinitionKey()))
				){
				jumpTasks.add(task);
			}
		}
		return jumpTasks;
	}

	private static void doJump(ProcessEngine processEngine, String processInstanceId, Map<String, Object> variables,
			GatewayInfo curGatewayInfo, NodeInfo nextJoinEndGateway,Set<Task> jumpTasks) {
		for(Task task:jumpTasks) {
			NodeInfo theNode = curGatewayInfo.getNodeInfo(task.getTaskDefinitionKey());
			jumpToEndGateway(processEngine,task.getTaskDefinitionKey(),task.getId(),theNode.getBelongEndGatewayNodeId(),variables);
		}

		setJumpVariables(processEngine, processInstanceId,
				curGatewayInfo, nextJoinEndGateway,jumpTasks);
		if(jumpTasks.size()>0) {
			doJump(processEngine, processInstanceId,  variables,
					curGatewayInfo, nextJoinEndGateway, jumpTasks);
		}

	}
	public static boolean isNodeEndInThis(GatewayInfo curGatewayInfo,String endGatewayNodeId,String thisTaskId) {
		NodeInfo curNode = curGatewayInfo.getNodeInfo(thisTaskId);
		String belongEndGatewayId = curNode.getBelongEndGatewayNodeId();
		if(StrUtil.isEmpty(belongEndGatewayId)) {
			return false;
		}else {
			if(belongEndGatewayId.equals(endGatewayNodeId)) {
				return true;
			}else {
				return isNodeEndInThis(curGatewayInfo,endGatewayNodeId,belongEndGatewayId);
			}
		}
		
	}
	
	public static void jumpToEndGateway(ProcessEngine processEngine,String jumpFromTaskDefId,String jumpFromTaskId,String jumpToGatewayId,Map<String, Object> variables) {
		CommandExecutor commandExecutor = ((ProcessEngineConfigurationImpl) processEngine
				.getProcessEngineConfiguration()).getCommandExecutor();
		commandExecutor.execute(new JumpCommand(jumpFromTaskId, jumpToGatewayId, variables,""));
	}
	public static Task jumpOutGateway4EndTransaction(CacheDaoSvc cacheDaoSvc,ProcessEngine processEngine, Task theTask) {
		List<Task> allTasks=null;
		allTasks =processEngine.getTaskService().createTaskQuery().processInstanceId(theTask.getProcessInstanceId()).list();
		if(allTasks==null || allTasks.size()==0) {
			return theTask;
		}
		ProcessInstance processInstance = processEngine.getRuntimeService().createProcessInstanceQuery().processInstanceId(theTask.getProcessInstanceId()).singleResult();
		if(processInstance == null) {
			return theTask;
		}
		jumpOutGateway4EndTransaction(cacheDaoSvc,processEngine, allTasks,processInstance);
		allTasks =processEngine.getTaskService().createTaskQuery().processInstanceId(theTask.getProcessInstanceId()).list();
		if(allTasks!=null && allTasks.size()>0) {
			return allTasks.get(0);
		}else {
			return null;
		}
	}
	public static List<Task> jumpOutGateway4EndTransaction(CacheDaoSvc cacheDaoSvc,ProcessEngine processEngine, List<Task> allTasks,ProcessInstance processInstance ) {
		if(allTasks == null||allTasks.size()==0) {
			return allTasks;
		}

		String processDefId = processInstance.getProcessDefinitionId();
		if(processDefId.indexOf(SymbolConstants.COLON)>0) {
			processDefId = processDefId.substring(0,processDefId.indexOf(":"));
		}
		
		BigInteger version =BigInteger.valueOf(processInstance.getProcessDefinitionVersion());
		WfDefinition wfDefinition = cacheDaoSvc.selectByDefIdActVersion(processDefId,version);
		String processXmlString = wfDefinition.getXmlSource();
		
		for(Task curTask:allTasks) {
			GatewayInfo curGatewayInfo = getGatewayInfo( cacheDaoSvc, processXmlString,processDefId,version);
			if(curGatewayInfo!=null) {
				NodeInfo curNode = curGatewayInfo.getNodeInfo(curTask.getTaskDefinitionKey());
				if(curNode!=null) {
					if(curNode.getBelongEndGatewayNodeId()!=null
							&&(
									!curNode.getBelongEndGatewayNodeId().equals(curNode.getNodeId())
									)
							){
						jumpToEndGateway(processEngine,curTask.getTaskDefinitionKey(),curTask.getId(),curNode.getBelongEndGatewayNodeId(),null);
					}
				}
			}			
		}
		
		List<Task> result = new ArrayList<>();
		allTasks =processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		if(allTasks!=null && allTasks.size()>0) {
			for(Task curTask:allTasks) {
				if(!StrUtil.isEmpty(GatewayUtil.getEndGatewayDefId(processDefId, version, curTask.getTaskDefinitionKey()))) {
					result.add(curTask);
				}
			}
		}
		
		return result;
	}
}
