package com.bpm.workflow.service.common;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskTransferData;
import com.bpm.workflow.engine.command.JumpCommand;
import com.bpm.workflow.entity.WfBusinessData;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfBusinessDataMapper;
import com.bpm.workflow.service.WfEvtServiceDefService;
import com.bpm.workflow.service.WfInstanceService;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.*;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

@Log4j2
@Service
public class ManageTaskService {

    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfTaskflowService wfTaskflowService;
    @Resource
    private ProcessCoreService processCoreService;
    @Resource
    private ActivitiBaseService activitiBaseServer;
    @Resource
    private WfInstanceService wfInstanceService;
    @Resource
    private WfBusinessDataMapper wfBusinessDataMapper;
    @Resource
    private ModelPropService modelPropService;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private ProcessEngine processEngine;
    @Resource
    private WfCommService wfCommService;
    @Resource
    private WfEvtServiceDefService wfEvtServiceDefService;

    @Transactional
    public void backFromTask(WorkFlowTaskTransferData taskTransferData) {
        if (StrUtil.isEmpty(taskTransferData.getTaskId()) && StrUtil.isEmpty(taskTransferData.getProcessInstId())) {
            throw new ServiceException("任务标识和流程实例id不能同时为空！");
        }
        String userId = taskTransferData.getUserId();
        String taskId = taskTransferData.getTaskId();
        String doResult = taskTransferData.getResult();
        String tranType = taskTransferData.getTranType();
        //结束类型
        String cancelType = taskTransferData.getCancelType();
        String targetActivitiId = taskTransferData.getTargetActivitiId();
        String agentUserId = taskTransferData.getAgentUserId();
        String resultRemark = taskTransferData.getResultRemark();
        Map<String, Object> varMap = taskTransferData.getBussinessMap();
        SysCommHead sysCommHead = taskTransferData.getSysCommHead();
        WfVariableUtil.setsysCommHead(sysCommHead);
        if (varMap == null) {
            varMap = new HashMap<>();
        }
        // 检查操作节点时候的状态是否合法，合法则返回任务信息
        WfTaskflow workflow = this.checkBackTaskData(taskTransferData);
        // 存储跳转以后的处理人员信息
        Set<String> taskActorSet = new HashSet<>();
        // 针对不同的操作调用不同的事件服务
        String eventName = null;
        // 目标节点
        WfTaskflow targetWorkflow = null;
        // 流程变量处理
        Map<String, Object> variables = JSONObject.parseObject(workflow.getBussinessMap());
        if (variables == null) {
            variables = new HashMap<>(2);
        }
        //update by xuesw 20180425 任务紧急程度、系统渠道号登记处理
        wfCommService.setTaskUargent(variables, varMap);
//		String tranChannel = taskTransferData.getSysCommHead().getTranChannel();
//		if (!varMap.containsKey(Constants.INPUT_TRANCHANNEL_KEY)) {
//			variables.put(Constants.INPUT_TRANCHANNEL_KEY, tranChannel);
//		}
        //update by xuesw 20180425 任务紧急程度、系统渠道号登记处理

        variables.putAll(varMap);
        /** 2022/9/26 17:05 流程终止增加终止原因 start by lingengming */
        variables.put("doRemark", taskTransferData.getDoRemark());
        /** 2022/9/26 17:05 流程终止增加终止原因 end by lingengming */

        // 默认为撤回操作
        switch (tranType) {
            case Constants.TRANTYPE_TRANSER:
                processCoreService.transferAssignee(taskId, agentUserId, userId, doResult, resultRemark);
                return;
            case Constants.TRANTYPE_REJECT:
                processCoreService.transferRefuse(taskId, userId, doResult, resultRemark);
                return;
            case Constants.TRANTYPE_DEL: // 发起人取消流程
                // 如果是会签任务，就处理其他没完成的会签任务
//			finishSignTask(workflow, variables, tranType);
                endProcessInstance(workflow.getProcessId(), variables, cancelType, userId);
                return;

            case Constants.TRANTYPE_ASKDEL: // 发起人取消流程
                // 如果是会签任务，就处理其他没完成的会签任务
                finishSignTask(workflow, variables, tranType);
                // 调用流程结束方法
                WorkFlowTaskTransferData cancelData = new WorkFlowTaskTransferData();
                cancelData.setProcessInstId(taskTransferData.getProcessInstId());
                cancelData.setResult(taskTransferData.getResult());
                cancelData.setResultRemark(taskTransferData.getResultRemark());
                cancelData.setCancelType(cancelType);
                processCoreService.endProcessInstance(cancelData, userId);
                return;
            case Constants.TRANTYPE_CHANGE:
                updateTaskFlowRemark(workflow, userId, taskTransferData.getResultRemark());
                return;
            case Constants.TRANTYPE_FILEADD:
                addTaskFlowAccessories(workflow, userId, taskTransferData.getAccessories());
                return;
            case Constants.TRANTYPE_FILEDEL:
                delTaskFlowAccessories(workflow, userId, taskTransferData.getAccessories());
                return;
            case Constants.TRANTYPE_FREE:// 自由跳转
                finishSignTask(workflow, variables, tranType);
                taskActorSet.add(agentUserId);
                break;
            case Constants.TRANTYPE_REFUSE:// 指定节点驳回
                // 如果是会签任务，就处理其他没完成的会签任务
                finishSignTask(workflow, variables, tranType);
                // 以下处理主要是因为会签节点有多个处理人，如果指定节点为会签节点，则目标人有多个
                // 查询已经审批过的节点
                List<WfTaskflow> passList = findPaasTaskFlow(workflow.getProcessId(), workflow.getTaskId());
                // 获取目标节点的处理人
                taskActorSet = getDistTaskActor(passList, targetActivitiId);
                // 获取目标节点
                targetWorkflow = getDistTaskFlow(passList, targetActivitiId);
                eventName = ModelProperties.backTaskEvent.getName();
                this.checkContainsSubProcess(targetWorkflow.getProcessId(), targetWorkflow.getTaskId());
                variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_REFULSE_TASK);
                break;
            case Constants.TRANTYPE_REJECT_ONESTEP:// 驳回上一节点
                // 如果当前节点是会签任务，就处理其他没完成的会签任务
                finishSignTask(workflow, variables, tranType);
                List<WfTaskflow> superTaskList = getLastTaskWorkFlows(workflow);
                // //获取目标节点的处理人
                taskActorSet = getDistTaskActor(superTaskList, null);
                // 获取目标节点
                targetWorkflow = getDistTaskFlow(superTaskList, null);
                eventName = ModelProperties.backTaskEvent.getName();
                this.checkContainsSubProcess(targetWorkflow.getProcessId(), targetWorkflow.getTaskId());
                variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_REFULSE_TASK);

                break;
            case Constants.TRANTYPE_REJECT_BEGIN: // 驳回至首节点
                // 如果当前节点是会签任务，就处理其他没完成的会签任务
                finishSignTask(workflow, variables, tranType);
                List<WfTaskflow> firstTaskList = getFirstTaskWorkFlows(workflow);
                // 获取目标节点的处理人
                taskActorSet = getDistTaskActor(firstTaskList, null);
                // 获取目标节点
                targetWorkflow = getDistTaskFlow(firstTaskList, null);
                eventName = ModelProperties.backTaskEvent.getName();
                this.checkContainsSubProcess(targetWorkflow.getProcessId(), targetWorkflow.getTaskId());
                variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_REFULSE_TASK);

                break;
            default:

                //节点可撤回检查的控制
                checkRecallExtendProps(taskId, workflow);

                //已审批节点是会签节点的话，必须全部完成
                finishSignTask(workflow, variables, null);

                //查找所有的待办任务，如果待办任务是会签节点，留一个任务用来跳转，其他全部自动关闭
                List<WfTaskflow> currTaskList = getNextTaskFlowsByTaskId(workflow);
                finishSignTask(currTaskList.get(0), varMap, Constants.TRANTYPE_UNDO);

                // 获取目标节点的处理人
                passList = findPaasTaskFlow(workflow.getProcessId(), workflow.getTaskId());
                if (Constants.NODE_TYPE_SIGN.equals(workflow.getTaskType())) {
                    taskActorSet = getDistTaskActor(passList, workflow.getTaskDefId());
                } else {
                    taskActorSet.add(taskTransferData.getUserId());
                }

                // 获取目标节点，workflow就是目标节点
                targetWorkflow = workflow;
                // TODO 多分支情况还需要处理下taskId
                taskId = currTaskList.get(0).getTaskId();

                //2019-05-07 amt modify start 任务撤回服务接口改造、只有在下一个待办任务认领之前才可以撤回
                //只有在下一个待办任务认领之前才可以撤回。
                if (StrUtil.equals(tranType, Constants.TRANTYPE_REJECT_CHECK)) {
                    //判断目标任务是否已经领用
                    if (StrUtil.equals(currTaskList.get(0).getTaskPoolFlag(), Constants.IS_HIST_TASK_POOL)) {
                        throw new ServiceException("处理失败，任务已经被领用");
                    }
                }
                //2019-05-07 amt modify end 任务撤回服务接口改造、只有在下一个待办任务认领之前才可以撤回
                if (targetWorkflow.getTaskDefId().equals(currTaskList.get(0).getTaskDefId())) {
                    throw new ServiceException("该笔任务正在由您处理，无法撤回");
                }

                taskTransferData.setTaskId(taskId);
                eventName = ModelProperties.recallTaskEvent.getName();
                this.checkContainsSubProcess(targetWorkflow.getProcessId(), targetWorkflow.getTaskId());
                variables.put(Constants.BACK_FROM_TASK, Constants.WORK_TYPE_RECOVER_TASK);
                break;
        }

        //针对自由跳转
        if (targetWorkflow != null) {
            targetActivitiId = targetWorkflow.getTaskDefId();
            //处理子流程节点
            if (StrUtil.isEmpty(taskTransferData.getProcessInstId())) {
                taskTransferData.setProcessInstId(targetWorkflow.getProcessId());
            }
            boolean isreturn = this.isSubProcess(taskTransferData, variables, targetWorkflow.getProcessCode(), targetActivitiId);
            if (isreturn) {
                return;
            }
        }


        //控制变量赋值
        variables.put(Constants.BEFOR_USERID, userId);
        variables.put(Constants.BEFOR_TASKID, taskId);
        // 至上一节点时需要把之前的task_actor放进去
        StringBuffer actors = new StringBuffer();
        for (String strActor : taskActorSet) {
            if (StrUtil.isNotEmpty(strActor)) {
                actors.append(SymbolConstants.COMMA + strActor);
            }

        }
        String taskActor = actors.toString();
        taskActor = taskActor.length() > 0 ? taskActor.substring(1) : taskActor;
        if (actors.length() > 0) {
            variables.put(Constants.SPECIFYEXTOPERS, actors.toString().substring(1));
        } else {
            variables.remove(Constants.SPECIFYEXTOPERS);
        }
        WfVariableUtil.setsysCommHead(sysCommHead);
        WfVariableUtil.setVariableMap(variables);

        if (log.isInfoEnabled()) {
            log.info("| - ManageTaskServiceImpl>>>>>回退、驳回、跳转前：参数[tranType:{},taskId:{},targetActivitiId:{},variables:{}]"
                    , tranType, taskId, targetActivitiId, JSON.toJSONString(variables));
        }
        //执行跳转
        boolean jumpOk = activitiBaseServer.findCommandExecutor()
                .execute(new JumpCommand(taskId, targetActivitiId, variables, ""));
        if (!jumpOk) {
            throw new ServiceException("操作失败，请联系管理人员处理！");
        }

        //更新流程及当前（待办）任务信息
        BpmnModel bm = activitiBaseServer.getBpmnModelNode(workflow.getProcessCode());
        FlowElement flowElement = bm.getFlowElement(workflow.getTaskDefId());
        variables.put("firstNode", true);
        WfVariableUtil.setVariableMap(variables);
        WfTaskflow taskflow = updateTaskFlowInfo(taskTransferData, flowElement);
        variables.remove("firstNode");
        WfVariableUtil.setVariableMap(variables);
        //执行当前（待办）任务的第三方服务
        if (!StrUtil.isEmpty(eventName)) {
            this.execEventInvock(taskId, doResult, taskActor, eventName, taskflow, flowElement, sysCommHead);
        }

        //执行自动节点
        this.execAuto(taskTransferData, taskId, targetActivitiId, taskflow, variables);
    }

    /**
     * <pre>
     * 附件申请撤销接口
     * 当处理人员提交自己的处理意见后，当后一处理人员未处理提交时，可在不撤回任务的情况下，申请撤回已提交的附件链接信息
     * </pre>
     *
     * @param workflow
     * @param userId
     * @param accessories
     * @author anmingtao
     * @serialData 2019-07-23
     */
    private void delTaskFlowAccessories(WfTaskflow workflow, String userId, String accessories) {
        if (!workflow.getTaskActors().contains(userId)) {
            throw new ServiceException("没有权限修改改任务处理附近信息!");
        }
        if (Objects.isNull(accessories)) {
            throw new ServiceException("附件信息不能为空!");
        }
        String accessoriesStr = workflow.getDoAccessories();
        if (Objects.isNull(accessoriesStr)) {
            throw new ServiceException("附件信息为空,不能撤销!");
        }
        checkNextTaskIsUndo(workflow);

        List<String> accessorieList = new ArrayList<>();
        if (accessoriesStr.contains(SymbolConstants.COMMA)) {
            accessorieList.addAll(Arrays.asList(accessoriesStr.split(SymbolConstants.COMMA)));
        } else {
            accessorieList.add(accessoriesStr);
        }

        if (accessories.contains(SymbolConstants.COMMA)) {
            String[] strs = accessories.split(SymbolConstants.COMMA);
            for (String str : strs) {
                if (!accessoriesStr.contains(str)) {
                    throw new ServiceException("附件信息不存在,不能撤销" + str + "的附件!");
                }
                accessorieList.remove(str);
            }
        } else {
            if (!accessoriesStr.contains(accessories)) {
                throw new ServiceException("附件信息不存在,不能撤销" + accessories + "的附件!");
            }
            accessorieList.remove(accessories);
        }

        String result = SymbolConstants.COMMA;
        if (accessorieList != null && !accessorieList.isEmpty()) {
            for (String str : accessorieList) {
                if (StrUtil.isNotEmpty(str)) {
                    result = result + str;
                }
            }
            workflow.setDoAccessories(result.substring(1));
        } else {
            workflow.setDoAccessories("");
        }
        wfTaskflowService.updateById(workflow);

    }

    /**
     * <pre>
     * 附件补充提交
     * 当处理人员提交自己的处理意见后，当后一处理人员未处理提交时，可在不撤回任务的情况下，补充提交相应的附件信息
     * </pre>
     *
     * @param workflow
     * @param userId
     * @param accessories
     */
    private void addTaskFlowAccessories(WfTaskflow workflow, String userId, String accessories) {
        if (!workflow.getTaskActors().contains(userId)) {
            throw new ServiceException("没有权限修改任务处理附近信息!");
        }
        if (Objects.isNull(accessories)) {
            throw new ServiceException("附件信息不能为空!");
        }
        checkNextTaskIsUndo(workflow);

        String accessoriesStr = workflow.getDoAccessories();
        if (Objects.isNull(accessoriesStr)) {
            workflow.setDoAccessories(accessories);
        } else {
            workflow.setDoAccessories(accessoriesStr + SymbolConstants.COMMA + accessories);
        }
        wfTaskflowService.updateById(workflow);
    }

    private void checkNextTaskIsUndo(WfTaskflow workflow) {
        if (!workflow.getWorkState().equals(Constants.TASK_STATE_END)) {
            throw new ServiceException("处理失败,请检查任务状态是否正确!");
        }
        WfTaskflow condition = new WfTaskflow();
        String supertaskId = workflow.getTaskId();
        condition.setSuperTaskId(supertaskId);
        condition.setWorkState("0");
        List<WfTaskflow> list = wfTaskflowService.selectByCondition(condition);
        if (list == null || list.isEmpty()) {
            throw new ServiceException("处理失败,请检查任务状态是否正确!");
        }
    }

    /**
     * <pre>
     * 任务处理意见修改
     * 当处理人员提交自己的处理意见后，当后一处理人员未处理提交时，可在不撤回任务的情况下，重新修改提交自己的处理意见
     * </pre>
     *
     * @param workflow
     * @param userId
     * @param resultRemark
     * @author anmingtao
     * @serialData 2019-07-23
     */
    private void updateTaskFlowRemark(WfTaskflow workflow, String userId, String resultRemark) {
        if (!workflow.getTaskActors().contains(userId)) {
            throw new ServiceException("没有权限修改改任务处理意见!");
        }
        checkNextTaskIsUndo(workflow);
        workflow.setDoRemark(resultRemark);
        wfTaskflowService.updateById(workflow);
    }


    /**
     * 对撤回的扩展属性控制进行检查
     *
     * @param taskId
     * @param workflow
     */
    private void checkRecallExtendProps(String taskId, WfTaskflow workflow) {

        // 通过当前taskId查询所有下一节点任务列表
        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(workflow.getProcessId());
        selectCondition.setSuperTaskId(taskId);
        List<WfTaskflow> currTaskList = wfTaskflowService.selectByCondition(selectCondition);

        // 当前的待办任务都不能处理
        for (WfTaskflow currTask : currTaskList) {
            if (!currTask.getWorkState().equals(Constants.TASK_STATE_RUNING)) {
                throw new ServiceException("下一节点任务已处理，无法撤回！");
            }
        }
    }

    /**
     * 强制终止流程
     *
     * @param userId
     * @
     */
    private void endProcessInstance(String processInstId, Map<String, Object> variables, String cancelType, String userId) {
        WfTaskflow condition = new WfTaskflow();
        String processCode = null;
        condition.setProcessId(processInstId);
        condition.setWorkState(Constants.WORK_STATE_RUNNING);
        List<WfTaskflow> undoList = wfTaskflowService.selectByCondition(condition);
        for (WfTaskflow undo : undoList) {
            processCode = undo.getProcessCode();
            Map<String, Object> userIdMap = new HashMap<String, Object>();
            userIdMap.put(Constants.USERID_KEY, userId);
            Map<String, Object> users = processCoreService.queryUserList(userIdMap);
            if (users == null) {
                undo.setDoTaskActorName(userId);
            } else {
                undo.setDoTaskActorName((String) users.get("userName"));
                undo.setDoTaskActorAgencyId((String) users.get("userBank"));
                undo.setDoTaskActorAgencyName((String) users.get("userBankName"));
            }
			LocalDateTime now = LocalDateTime.now();
            undo.setUpdateDate(now);
            undo.setEndDate(now);
            undo.setDoRemark("废弃流程");
            if (ObjectUtil.isNotEmpty(variables.get("doRemark"))) {
                undo.setDoRemark((String) variables.get("doRemark"));
            }
            undo.setDoResult("5");
            wfTaskflowService.updateById(undo);
            if (undo.getTaskType().equals(Constants.NODETYPE_SUBPROC)) {
                WfInstance instance = wfInstanceService.selectInstanceByPtaskId(undo.getTaskId());
                this.endProcessInstance(instance.getProcessId(), variables, cancelType, userId);
            }

        }
        // 找到流程的结束个节点
        BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processCode);
        Collection<FlowElement> nodeList = bpmnModel.getMainProcess().getFlowElements();
        FlowElement endElement = null;
        for (FlowElement element : nodeList) {
            if (element instanceof EndEvent) {
                endElement = element;
                break;
            }
        }
        if (endElement == null) {
            throw new ServiceException("未找到结束节点！！！");
        }


        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService
                .selectModelExtendsPropByOwnId(processCode.split(":")[0]);
        WfModelPropExtends cancelEvent = modelPropService.getSpcefiyModelProp(extendsPropList,
                ModelProperties.cancelEvent.getName());
        if (cancelEvent != null) {
            variables.put(Constants.COMPLETE_TYPE_KEY, Constants.COMPLETE_TYPE_CANCLE);
        } else {
            if (StrUtil.equals(cancelType, Constants.COMPLETE_TYPE_ERROR)) {
                WfVariableUtil.setProcessEndEventName(ModelProperties.flowEndErrorEvent.getName());
            } else {
                WfVariableUtil.setProcessEndEventName(ModelProperties.flowEndEvent.getName());
            }
            variables.put(Constants.COMPLETE_TYPE_KEY, cancelType);

        }

        WfVariableUtil.setVariableMap(variables);
        processUtil.doAfterEndProcess(processCode, processInstId, endElement);

    }

    /**
     * @Description: 执行自动节点
     * @Param: [taskTransferData, taskId, targetActivitiId, taskflow, variables]
     * @Return: void
     * @Author:
     * @Date: 2021/9/23
     */
    private void execAuto(WorkFlowTaskTransferData taskTransferData, String taskId, String targetActivitiId,
                          WfTaskflow taskflow, Map<String, Object> variables) {
        // 如果回退到自动节点，则自动执行到下个节点
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId(taskflow.getProcessId())
                .list();
        for (Task t : tasks) {
            FlowElement flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(t.getId());
            String nodeType = processUtil.getTaskProp(t.getProcessDefinitionId(), flowElement, Constants.NODETYPE);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);

            if (t.getTaskDefinitionKey().endsWith(targetActivitiId)) {
                if (nodeType.equals(Constants.NODE_TYPE_ATTO)) {
                    if (log.isInfoEnabled()) {
						log.info("| - ManageTaskServiceImpl>>>>>开始执行自动跳转[taskId={}]", t.getId());
                    }
                    variables.put(Constants.BEFOR_USERID, taskTransferData.getUserId());
                    variables.put(Constants.BEFOR_TASKID, taskId);
                    variables.put("workState", Constants.TASK_STATE_END);
                    variables.put("doResult", taskTransferData.getResult());
                    variables.put("doRemark", taskTransferData.getResultRemark());
                    variables.remove(Constants.SPECIFYEXTOPERS);
                    wfEvtServiceDefService.autoTaskNodeHandler(t.getId(), t.getProcessDefinitionId(),
                            t.getProcessInstanceId(), variables, WfVariableUtil.getSysCommHead(), flowElement);
					log.error("| - ManageTaskServiceImpl>>>>>完成[taskId={}]节点的自动跳转", t.getId());
                }
            }
        }
    }

    private void execEventInvock(String taskId, String doResult, String taskActor, String eventName,
                                 WfTaskflow taskflow, FlowElement flowElement, SysCommHead sysCommHead) {
        // 事件回调
        String nodeId = processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, Constants.NODEPROP_REFID);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
        List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);
        // 回退事件服务加上一审批状态，上一审批阶段、当前审批阶段，当前任务成员
        Map<String, Object> exectMap = BpmStringUtil.jsonToMap(taskflow.getBussinessMap());
        exectMap.put(Constants.DO_RESAULT, doResult);
        exectMap.put(Constants.LAST_TASK_NAME, taskflow.getTaskName());
        exectMap.put(Constants.TASK_NAME, flowElement.getName());
        exectMap.put(Constants.TASK_USERS, taskActor);
        WfModelPropExtends startEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList, eventName);
        wfEvtServiceDefService.executeEventService(startEventInvock, taskId, taskflow.getProcessCode(),
                taskflow.getProcessId(), exectMap, flowElement, sysCommHead);
    }

    /**
     * 跟新taskid对应的节点信息及流程信息
     *
     * @param taskTransferData
     * @param flowElement
     */
    private WfTaskflow updateTaskFlowInfo(WorkFlowTaskTransferData taskTransferData, FlowElement flowElement) {
        // 更新流转记录表
        WfTaskflow taskflow = processCoreService.selectByTaskId(taskTransferData.getTaskId());
        //附件处理
        wfCommService.processAccessories(taskTransferData, taskflow);

        String nodeType = processUtil.getTaskProp(taskflow.getProcessCode(), flowElement, Constants.NODETYPE);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);

        if (taskflow != null) {
            if (nodeType != null && !Constants.NODE_TYPE_ATTO.equals(nodeType)) {
                taskflow.setDoTaskActor(taskTransferData.getUserId());
                taskflow.setDoResult(taskTransferData.getResult());
                taskflow.setDoRemark(taskTransferData.getResultRemark());
                wfCommService.processAccessories(taskTransferData, taskflow);
            }
            taskflow.setWorkState(Constants.TASK_STATE_END);
			LocalDateTime now = LocalDateTime.now();
            taskflow.setUpdateDate(now);
            taskflow.setEndDate(now);
            if (!taskflow.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_ALLOT_TASK))) {
                if (!Constants.NODE_TYPE_ATTO.equals(taskflow.getTaskType())
                        && !Constants.NODETYPE_SUBPROC.equals(taskflow.getTaskType())) {
                    UserOutputData userData = processCoreService.getUserData(taskflow, taskTransferData.getUserId());
                    if (userData == null) {
                        if (Constants.TRANTYPE_REJECT_ONESTEP.equals(taskTransferData.getTranType())
                                || Constants.TRANTYPE_REJECT_BEGIN.equals(taskTransferData.getTranType())
                                || Constants.TRANTYPE_REFUSE.equals(taskTransferData.getTranType())
                                || Constants.TRANTYPE_TRANSER.equals(taskTransferData.getTranType())
                                || Constants.TRANTYPE_REJECT.equals(taskTransferData.getTranType())) {
                            throw new ServiceException(taskTransferData.getUserId() + "没有权限处理该任务");
                        }
                        Map<String, Object> userIdMap = new HashMap<String, Object>();
                        userIdMap.put(Constants.USERID_KEY, taskTransferData.getUserId());
                        Map<String, Object> users = processCoreService.queryUserList(userIdMap);
                        if (users == null) {
                            taskflow.setDoTaskActorName(taskTransferData.getUserId());
                        } else {
                            taskflow.setDoTaskActorName((String) users.get("userName"));
                            taskflow.setDoTaskActorAgencyId((String) users.get("userBank"));
                            taskflow.setDoTaskActorAgencyName((String) users.get("userBankName"));
                        }
                    } else {

                        taskflow.setDoTaskActorName(userData.getUserName());
                        taskflow.setDoTaskActorAgencyId(userData.getUserBank());
                        taskflow.setDoTaskActorAgencyName(userData.getUserBankName());
                    }
                }
            }

            wfTaskflowService.updateById(taskflow);
            WfBusinessData businessData = new WfBusinessData(taskflow);
            wfBusinessDataMapper.updateById(businessData);

            WfInstance instance = wfInstanceService.selectByProcessId(taskflow.getProcessId());
            if (instance != null) {
                String ids = instance.getJoinUserIds();
                // 打回操作情况流程实例的处理人员清空
                String tranType = taskTransferData.getTranType();
                if (Constants.TRANTYPE_REJECT.equals(tranType) ||
                        Constants.TRANTYPE_REJECT_BEGIN.equals(tranType) ||
                        Constants.TRANTYPE_REJECT_ONESTEP.equals(tranType)) {
                    ids = "";
                } else if (!ids.contains(taskTransferData.getUserId())) {
                    ids += SymbolConstants.COMMA + taskTransferData.getUserId();
                    if (ids.startsWith(SymbolConstants.COMMA)) {
                        ids = ids.substring(1);
                    }
                } else {
                    //此处可以少更新一下数据库
                    return taskflow;
                }
                instance.setJoinUserIds(ids);
                instance.setUpdateTime(LocalDateTime.now());
                wfInstanceService.updateById(instance);
            }
        }

        return taskflow;
    }

    /**
     * 根据taskId获取执行结果
     *
     * @param workflow
     * @return
     * @
     */
    private List<WfTaskflow> getNextTaskFlowsByTaskId(WfTaskflow workflow) {
        // 判断要撤回的当前任务是否存在
        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(workflow.getProcessId());
        selectCondition.setWorkState(Constants.TASK_STATE_RUNING);
        List<WfTaskflow> currTaskList = wfTaskflowService.selectByCondition(selectCondition);

        if (currTaskList.size() < 1) {
            throw new ServiceException("流程实例不存在或实例已经结束，无法撤回！");
        }
        Set<String> tempSet = new HashSet<String>();
        for (WfTaskflow temp : currTaskList) {
            if (tempSet.isEmpty()) {
                tempSet.add(temp.getTaskDefId());
            } else {
                if (!tempSet.contains(temp.getTaskDefId())) {
                    //如果taskDefId不一样，说明是存在分支的情况，这种无法撤回，taskDefId一样应该是会签节点
                    throw new ServiceException("流程实例存在多实例情况，无法撤回！");
                }
            }
        }

        return currTaskList;

    }

    /**
     * @param currTaskflow
     * @return
     * @
     */
    private List<WfTaskflow> getFirstTaskWorkFlows(WfTaskflow currTaskflow) {

        // 查询当前有无分支任务在处理
        WfTaskflow branchflow = new WfTaskflow();
        branchflow.setProcessId(currTaskflow.getProcessId());
        branchflow.setWorkState(Constants.TASK_STATE_RUNING);
        List<WfTaskflow> branchflowList = wfTaskflowService.selectByCondition(branchflow);
        if (branchflowList.size() > 1 && !branchflowList.get(0).getTaskType().equals(Constants.NODE_TYPE_SIGN)) {
            throw new ServiceException("当前节点有分支任务待处理，不能驳回直接驳回至首节点！");
        }

        WfTaskflow whileTask = currTaskflow;
        while (whileTask.getSuperTaskId() != null && whileTask.getTaskRate() != null
                && whileTask.getTaskRate().intValue() != 1) {
            // 分支撤回时，super_task_id 存的是【taskId，taskId】字符串
            String superTaskId2 = whileTask.getSuperTaskId().split(SymbolConstants.COMMA)[0];
            whileTask = wfTaskflowService.selectByTaskId(superTaskId2);
            if (whileTask == null) {
                throw new ServiceException("当前节点的上一任不存在，请确认！");
            }
        }
        //这里第一次查询所有的目标节点的数据
        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(whileTask.getProcessId());
        selectCondition.setSuperTaskId(whileTask.getSuperTaskId());
        selectCondition.setTaskDefId(whileTask.getTaskDefId());
        List<WfTaskflow> befSignTaskList = wfTaskflowService.selectByCondition(selectCondition);

        return befSignTaskList;
    }

    /**
     * 获取上一个节点,如果不是会签节点list就一条，如果是会签节点则返回多条
     *
     * @param currTask
     * @return
     * @
     */
    private List<WfTaskflow> getLastTaskWorkFlows(WfTaskflow currTask) {
        // 取节点的上一节点列表
        FlowElement flowElement = activitiBaseServer.getBpmnModelNodeByTaskId(currTask.getTaskId());
        FlowNode flowNode = (FlowNode) flowElement;
        List<SequenceFlow> sequenceList = flowNode.getIncomingFlows();
        Map<String, Object> sequenceMap = new HashMap<String, Object>();
        if (sequenceList == null || sequenceList.size() == 0) {
            throw new ServiceException("当前节点不存在上一节点，请确认！");
        }

        for (SequenceFlow sq : sequenceList) {
            sequenceMap.put(sq.getSourceRef(), sq);
        }
        // A->B,A-C分支时,B,C节点不能驳回
        WfTaskflow branchCondition = new WfTaskflow();
        branchCondition.setSuperTaskId(currTask.getSuperTaskId());
        List<WfTaskflow> branchTaskList = wfTaskflowService.selectByCondition(branchCondition);
        if (branchTaskList.size() > 1 && !branchTaskList.get(0).getTaskType().equals(Constants.NODE_TYPE_SIGN)) {
            throw new ServiceException("当前节点有分支任务待处理，不能驳回！");
        }

        // 分支撤回时，super_task_id 存的是【taskId，taskId】字符串
        String superTaskId = currTask.getSuperTaskId().split(SymbolConstants.COMMA)[0];
        // 取上一任务往回找节点
        WfTaskflow befTask = wfTaskflowService.selectByTaskId(superTaskId);
        // 如果上一任没务有在上一节点的Map中，继续往上取
        while (!sequenceMap.containsKey(befTask.getTaskDefId())) {
            if (befTask.getSuperTaskId() == null) {
                throw new ServiceException("当前任务不存在上一任务，请确认！");
            }
            String superTaskIds = befTask.getSuperTaskId().split(SymbolConstants.COMMA)[0];
            WfTaskflow wfTaskflow = wfTaskflowService.selectByTaskId(superTaskIds);
            if (wfTaskflow == null) {
                throw new ServiceException("当前节点的上一任务不存在，请确认！");
            }
            befTask = wfTaskflow;
        }
        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(befTask.getProcessId());
        selectCondition.setSuperTaskId(befTask.getSuperTaskId());
        selectCondition.setTaskDefId(befTask.getTaskDefId());
        List<WfTaskflow> befSignTaskList = wfTaskflowService.selectByCondition(selectCondition);
        return befSignTaskList;
    }

    /**
     * 获取目标节点
     *
     * @param passList
     * @param targetActivitiId
     * @return
     * @
     */
    private WfTaskflow getDistTaskFlow(List<WfTaskflow> passList, String targetActivitiId) {
        WfTaskflow target = null;
        for (WfTaskflow temp : passList) {
            if (StrUtil.isEmpty(targetActivitiId) || temp.getTaskDefId().equals(targetActivitiId)) {
                target = temp;
            }
        }
        if (target == null) {
            throw new ServiceException("驳回的目标节点不在审批记录中，无法驳回！");
        }
        return target;
    }

    private Set<String> getDistTaskActor(List<WfTaskflow> passList, String targetActivitiId) {
        Set<String> taskActorSet = new HashSet<String>();
        for (WfTaskflow temp : passList) {
            //过滤掉回收及分配的任务
            if ((Objects.isNull(temp.getWorkType())
                    ||
                    (!temp.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_ALLOT_TASK))
                            && !temp.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_RECOVERY))
                            && !temp.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_CHECK_OUT))
                    )) && (StrUtil.isEmpty(targetActivitiId) || temp.getTaskDefId().equals(targetActivitiId))) {
                if (StrUtil.isNotBlank(temp.getTaskActors())) {
                    for (String actor : temp.getTaskActors().split(SymbolConstants.COMMA)) {
                        taskActorSet.add(actor);
                    }
                }
            }
        }
        return taskActorSet;
    }

    /**
     * 查询会签节点，当返回结果为null或者为空时，表示不是会签节点，不做相应处理
     *
     * @param workflow
     * @return
     */
    private List<WfTaskflow> querySignTaskList(WfTaskflow workflow) {
        List<WfTaskflow> signTaskList = new ArrayList<WfTaskflow>();
        if (Constants.NODE_TYPE_SIGN.equals(workflow.getTaskType())) {
            WfTaskflow selectCond = new WfTaskflow();
            selectCond.setTaskDefId(workflow.getTaskDefId());
            selectCond.setProcessId(workflow.getProcessId());
            signTaskList = wfTaskflowService.selectByCondition(selectCond);
            // 驳回的情况此处可以不考虑，因为查出来后，只处理没执行过的
            for (WfTaskflow taskflow : signTaskList) {
                WfBusinessData businessdata = wfBusinessDataMapper.selectById(taskflow.getId().toString());
                if (businessdata != null) {
                    taskflow.setBusinessData(businessdata);
                }
            }
        }
        return signTaskList;
    }

    /**
     * 检查返回时任务的状态是否允许，如果不允许则进行提示
     *
     * @param taskTransferData
     * @
     */
    private WfTaskflow checkBackTaskData(WorkFlowTaskTransferData taskTransferData) {
        // 做必要的检查
        if (StrUtil.isBlank(taskTransferData.getUserId())) {
            throw new ServiceException(ErrorCode.WF000024);
        }
        if (StrUtil.equals(taskTransferData.getTranType(), Constants.TRANTYPE_ASKDEL) && StrUtil.isBlank(taskTransferData.getTaskId()) || StrUtil.equals(taskTransferData.getTranType(), Constants.TRANTYPE_DEL) && StrUtil.isBlank(taskTransferData.getTaskId())) {
            List<WfTaskflow> list = wfTaskflowService.selectByProcessInstId(taskTransferData.getProcessInstId());
            if (list != null && !list.isEmpty()) {
                for (WfTaskflow w : list) {
                    if (w.getWorkState().equals(Constants.WORK_STATE_RUNNING)) {
                        return w;
                    }
                }
            } else {

                throw new ServiceException("系统找不到对应的流程实例[" + taskTransferData.getProcessInstId() + "]");
            }

            return list.get(0);
        }
        if (StrUtil.isBlank(taskTransferData.getTaskId())) {
            throw new ServiceException(ErrorCode.WF000025);
        }
        if (BpmConstants.TASK_ID_000001.equals(taskTransferData.getTaskId())) {
            throw new ServiceException("操作失败，不允许进行该操作！");
        }
        // 获取当前任务的 taskId为当前任务的任务Id(撤回为原任务Id)
        WfTaskflow workflow = wfTaskflowService.selectByTaskId(taskTransferData.getTaskId());
        if (workflow != null) {
            WfBusinessData businessdata = wfBusinessDataMapper.selectById(workflow.getId().toString());
            if (businessdata != null) {
                workflow.setBusinessData(businessdata);
            }
        }
        // 任务状态检查
        if (workflow == null) {
            String message = "";
            switch (taskTransferData.getTranType()) {
                case Constants.TRANTYPE_FREE:
                    message = ",操作失败";
                    break;
                case Constants.TRANTYPE_ASKDEL:
                    message = ",发起人取消流程失败";
                    break;
                case Constants.TRANTYPE_REFUSE:// 指定节点驳回
                case Constants.TRANTYPE_REJECT_ONESTEP:// 驳回上一节点
                case Constants.TRANTYPE_REJECT_BEGIN: // 驳回至首节点
                    message = ",驳回无效";
                    break;
                default:
                    message = ",追回无效";
                    break;
            }
            throw new ServiceException("该审批事项流程已结束" + message);
        } else {
            switch (taskTransferData.getTranType()) {
                case Constants.TRANTYPE_FREE:
                    if (StrUtil.isBlank(taskTransferData.getTargetActivitiId())) {
                        throw new ServiceException("目标节点标识不能为空，请确认！");
                    }
                    // 如果处理的任务是会签节点，则查询所有会签节点
                    List<WfTaskflow> signTaskList = querySignTaskList(workflow);
                    if (signTaskList.size() > 0) {
                        throw new ServiceException("会签任务不能进行指定操作，请确认！");
                    }
                    break;
                case Constants.TRANTYPE_ASKDEL:
                    // 先判断流程处理人员是否为流程发起人。
                    if (!taskTransferData.getUserId().equals(workflow.getWorkCreater())) {
                        throw new ServiceException("必须是流程发起人,才能做取消流程实例操作!");
                    }
                    break;
                case Constants.TRANTYPE_REFUSE:// 指定节点驳回
                    if (StrUtil.isBlank(taskTransferData.getTargetActivitiId())) {
                        throw new ServiceException("目标节点标识不能为空，请确认！");
                    }
                    break;
                case Constants.TRANTYPE_REJECT_ONESTEP:// 驳回上一节点
                    if (!Constants.TASK_STATE_RUNING.equals(workflow.getWorkState())) {
                        throw new ServiceException("当前任务已结束不能驳回，请确认！");
                    }
                    // super_task_id 为null 时,说明是首节点
                    if (workflow.getSuperTaskId() == null) {
                        throw new ServiceException("当前任务为首节点不能驳回，请确认！");
                    }
                case Constants.TRANTYPE_REJECT_BEGIN: // 驳回至首节点
                    if (workflow.getSuperTaskId() == null) {
                        throw new ServiceException("当前节点为首节点不能驳回，请确认！");
                    }
                    break;
                case Constants.TRANTYPE_TRANSER://转办任务
                    break;
                default:
                    if (BpmConstants.TASK_ID_000001.equals(workflow.getTaskId()) || workflow.getWorkType().equals(BigInteger.valueOf(Constants.WORK_TYPE_TRANSFER_OUT))) {
                        throw new ServiceException("转办任务[" + workflow.getTaskName() + "]，不允许撤回！");
                    }
                    if (!workflow.getTaskActors().contains(taskTransferData.getUserId())) {
                        throw new ServiceException("撤回失败！" + taskTransferData.getUserId() + "没有任务操作权限");
                    }
                    break;
            }
        }
        if (Constants.TASK_STATE_HUNG.equals(workflow.getWorkState())) {
            throw new ServiceException("任务已经挂起,必须取消挂起,才能做相应操作!");
        }
        return workflow;
    }

    /**
     * 检查是否目标节点与当前操作任务直接是否存在子流程
     *
     * @param processId
     * @param tartetTaskId
     */
    public void checkContainsSubProcess(String processId, String tartetTaskId) {
        List<WfTaskflow> list = wfTaskflowService.selectByProcessInstId(processId);
        if (list != null && list.size() > 0) {

            for (WfTaskflow w : list) {
                if (w.getTaskId().equals(tartetTaskId)) {
                    for (WfTaskflow w1 : list) {
                        if (w1.getId().compareTo(w.getId()) >= 0) {
                            if (w1.getTaskType().equals(Constants.NODETYPE_SUBPROC)) {
                                throw new ServiceException("操作失败！任务【ID:" + tartetTaskId + "】到目标节点存在子流程节点");
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 直接自动关掉未处理的会签任务
     *
     * @param varMap
     * @param workflow
     * @
     */
    @SuppressWarnings("unchecked")
    private void finishSignTask(WfTaskflow workflow, Map<String, Object> varMap,
                                String tranType) {
        // 如果不是会签节点返回
        List<WfTaskflow> signTaskList = this.querySignTaskList(workflow);
        if (signTaskList == null || signTaskList.isEmpty()) {
            return;
        }


        if (StrUtil.isEmpty(tranType)) {
            for (WfTaskflow temp : signTaskList) {
                if (!temp.getWorkState().equals(Constants.TASK_STATE_END)) {
                    throw new ServiceException("当前的所有会签任务还没有完成，无法撤回！");
                }
            }
            return;
        }
        // 否则处理调会签节点
        // 如果是将会签节点驳回
        Map<String, Object> oldVarMap = new HashMap<String, Object>();
        for (WfTaskflow temp : signTaskList) {
            oldVarMap.putAll((Map<String, Object>) JSONObject.parse(temp.getBussinessMap()));
            if (temp.getTaskId().equals(workflow.getTaskId())) {
                continue;
            }
            if (temp.getWorkState().equals(Constants.TASK_STATE_END)) {
                continue;
            }
            LocalDateTime now = LocalDateTime.now();
            // 剩下的会签任务都直接关掉
            oldVarMap.putAll(varMap);
            oldVarMap.put(Constants.SINGLASTTASKRESULT, Constants.RESULT_SYSUSE);
            oldVarMap.put(Constants.BEFOR_TASKID, temp.getTaskId());
            WfVariableUtil.addAllVariableMap(oldVarMap);
            activitiBaseServer.commitTask(temp.getTaskId(), new HashMap<>());
            temp.setWorkState(Constants.TASK_STATE_END);
            temp.setDoResult(Constants.RESULT_SYSUSE);
            temp.setDoTaskActor(configUtil.getAutoTaskActor());
            temp.setEndDate(now);
            temp.setUpdateDate(now);
            temp.setUpdateRemark("会签自动结束");
            /** 流程终止增加终止原因  */
            temp.setDoRemark((String) varMap.get("doRemark"));
            wfTaskflowService.updateById(temp);
            WfBusinessData wfBusinessData = new WfBusinessData(temp);
            wfBusinessDataMapper.updateById(wfBusinessData);
        }
    }

    private boolean isSubProcess(WorkFlowTaskTransferData taskTransferData, Map<String, Object> variables, String processDefinitionId, String targetActivitiId) {
        //如果是对子流程进行相应的操作
        if (variables.containsKey(Constants.PROCESS_TASK_ID)) {
            BpmnModel bpmnModel = activitiBaseServer.getBpmnModelNode(processDefinitionId);
            FlowElement targetElement = bpmnModel.getFlowElement(targetActivitiId);
            if (targetElement == null) {
                //说明 目标节点是主流程的
                //TODO 1.直接结束子流程，但不需要回调主流程
//            	WorkFlowTaskTransferData cancelData = new WorkFlowTaskTransferData();
//    			cancelData.setProcessInstId(taskTransferData.getProcessInstId());
//    			cancelData.setResult(taskTransferData.getResult());
//    			cancelData.setResultRemark(taskTransferData.getResultRemark());
////    			cancelData.setPassingRouteFlag(passingRouteFlag);;
//    			processCoreService.endProcessInstance(cancelData, taskTransferData.getUserId());
                //2.(递归)调用主流程中的自由跳转
                taskTransferData.setTaskId((String) variables.get(Constants.PROCESS_TASK_ID));
                backFromTask(taskTransferData);
                return true;
            }
        }
        return false;

    }

    /**
     * 查询当前未结束实例已审批的所有节点
     *
     * @param processId
     * @param taskId
     * @return
     * @
     */
    private List<WfTaskflow> findPaasTaskFlow(String processId, String taskId) {
        if (StrUtil.isBlank(processId)) {
            WfTaskflow currTaskflow = wfTaskflowService.selectByTaskId(taskId);
            if (currTaskflow == null) {
                throw new ServiceException(ErrorCode.WF000026);
            }
            processId = currTaskflow.getProcessId();
        }

        WfTaskflow selectCondition = new WfTaskflow();
        selectCondition.setProcessId(processId);
        selectCondition.setWorkState(Constants.TASK_STATE_END);
        List<WfTaskflow> passList = wfTaskflowService.selectByCondition(selectCondition);

        // 至少有开始节点的记录，如果没有，说明processId错误
        if (passList.size() < 1) {
            throw new ServiceException("流程实例不存在，请查看！");
        }
        return passList;
    }

}
