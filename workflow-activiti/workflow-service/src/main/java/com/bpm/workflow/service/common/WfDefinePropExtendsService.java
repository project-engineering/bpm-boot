package com.bpm.workflow.service.common;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.entity.WfDefinePropExtends;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Element;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigInteger;

@Log4j2
@Service
public class WfDefinePropExtendsService {
    @Resource
    private WfDefinePropExtendsService wfDefinePropExtendsService;
    @Resource
    private WfModelPropExtendsService wfModelpropExtendsService;
    @Resource
    private CacheUtil cacheUtil;
    @Resource
    private ConfigUtil configUtil;

    public enum RuleAttr {
        ruleProject("project"), rulePath("path"), ruleVersion("version"), rulePackage("package"), ruleMap("ruleMap");
        private String name;

        private RuleAttr(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String getKey4WfDefinePropExtends() {
            return BpmConstants.RULE_ + this.name;
        }

        public static RuleAttr getEnumByValue(String theName) {
            for (RuleAttr taskConfType : RuleAttr.values()) {
                if (taskConfType.getName().equals(theName)) {
                    return taskConfType;
                }
            }
            throw new RuntimeException("not supported type:" + theName);
        }

        public static RuleAttr getEnumByRuleName(String ruleName) {
            if (ruleName.startsWith(BpmConstants.RULE_)) {
                String theName = ruleName.substring(BpmConstants.RULE_.length(), ruleName.length());
                return getEnumByValue(theName);
            } else {
                return null;
            }
        }

        public static boolean hasAttrByKey4WfDefinePropExtends(String propName) {
//			log.info("----propName:"+propName);
            if (propName == null || !propName.startsWith(BpmConstants.RULE_)) {
                return false;
            }
            propName = propName.substring(BpmConstants.RULE_.length());

            for (RuleAttr theRuleAttr : RuleAttr.values()) {
//				log.info("----propName:"+propName+" theRuleAttr.name():"+theRuleAttr.getName()+" "+(theRuleAttr.getName().equals(propName)));
                if (theRuleAttr.getName().equals(propName)) {
                    return true;
                }
            }
            return false;
        }

    }

    public void insertWfDefinePropExtends(WfDefinePropExtends theArg) {
        wfDefinePropExtendsService.insertWfDefinePropExtends(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinePropExtends).refreshByKey(CacheUtil.SvcType.wfDefinePropExtends, null, new String[]{theArg.getProcessCode(), theArg.getTaskCode(), theArg.getPropName()}, true);
    }

    public WfDefinePropExtends selectByProcessCodePropNameTaskCode(WfDefinePropExtends theArg) {
        WfModelPropExtends wfModelpropExtends = wfModelpropExtendsService.selectByOwnIdPropName(theArg.getProcessCode(), ModelProperties.ignoreProcessVersion.getName());
        if (theArg.getVersion() == null) {
            throw new ServiceException(ErrorCode.WF000028);
        }
        int version = XmlUtil.getProcessAttrVersion(configUtil.isIgnoreProcessVersion(), wfModelpropExtends.getPropValue(), theArg.getVersion());
        if (version == XmlUtil.USE_DB_PROCESS_XML_PROPERTY && (BpmStringUtil.isResultIn(theArg.getPropName(), Constants.use_db_process_def_properties) || RuleAttr.hasAttrByKey4WfDefinePropExtends(theArg.getPropName()))) {
            theArg.setVersion(null);
            return (WfDefinePropExtends) cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinePropExtends).getCacheValueByKey(null, theArg.getProcessCode(), theArg.getTaskCode(), theArg.getPropName());
        } else {
            Element theTaskElement = cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinition).getTaskElementByDefIdActivtiVersionTaskDefId(theArg.getProcessCode(), BigInteger.valueOf(version), theArg.getTaskCode());
            theArg.setPropValue(XmlUtil.getDefXmlAttr(theTaskElement, theArg.getPropName()));
            return theArg;
        }
    }

    public int deleteByProcessCodePropNameTaskCode(WfDefinePropExtends theArg) {
        int result = wfDefinePropExtendsService.deleteByProcessCodePropNameTaskCode(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinePropExtends).refreshByKey(CacheUtil.SvcType.wfDefinePropExtends, null, new String[]{theArg.getProcessCode(), theArg.getTaskCode(), theArg.getPropName()}, true);

        return result;
    }

    public int updateByProcessCodePropNameTaskCode(WfDefinePropExtends theArg) {
        int result = wfDefinePropExtendsService.updateByProcessCodePropNameTaskCode(theArg);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfDefinePropExtends).refreshByKey(CacheUtil.SvcType.wfDefinePropExtends, null, new String[]{theArg.getProcessCode(), theArg.getTaskCode(), theArg.getPropName()}, true);
        return result;
    }

}
