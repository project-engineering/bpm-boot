package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.mapper.WfInstanceMapper;
import com.bpm.workflow.service.WfInstanceService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfInstanceServiceImpl extends ServiceImpl<WfInstanceMapper, WfInstance> implements WfInstanceService {
    @Resource
    private WfInstanceMapper wfInstanceMapper;
    @Override
    public WfInstance selectByProcessId(String processInstId) {
        LambdaQueryWrapper<WfInstance> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfInstance::getProcessId, processInstId);
        return wfInstanceMapper.selectOne(queryWrapper);
    }

    @Override
    public List<WfInstance> selectWfInstanceByTaskIds(List<String> taskIdList) {
        LambdaQueryWrapper<WfInstance> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(WfInstance::getParentTaskId,taskIdList);
        return wfInstanceMapper.selectList(queryWrapper);
    }

    @Override
    public List<WfInstance> selectNoTimeOutList(Map<String, Object> param, PageData<WfInstance> pageData) {
        return wfInstanceMapper.selectNoTimeOutList(param);
    }

    @Override
    public void insertWfInstance(WfInstance wfInstance) {
        wfInstanceMapper.insert(wfInstance);
    }

    @Override
    public WfInstance selectInstanceByPtaskId(String taskId) {
        LambdaQueryWrapper<WfInstance> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfInstance::getParentTaskId,taskId);
        return wfInstanceMapper.selectOne(queryWrapper);
    }

}
