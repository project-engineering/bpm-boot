package com.bpm.workflow.engine.listener;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.QueryRelationTellerFactory;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.SysCommHead;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.dto.transfer.WorkFlowStartData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskTransferData;
import com.bpm.workflow.entity.*;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.service.*;
import com.bpm.workflow.service.common.ModelPropService;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import com.bpm.workflow.util.cacheimpl.CacheActionUserAssignerLocal;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 任务节点创建的监听器
 *
 * @author xuesw
 *
 */
@Log4j2
@Component
public class TaskCreateListener extends SpringUtil implements TaskListener {
	@Resource
	protected ProcessUtil processUtil;
	@Resource
	private WfModelPropExtendsService wfModelPropExtendsService;
	@Resource
	private WfBusinessDataService wfBusinessDataService;
	@Resource
	protected WfCommService commService;
	@Resource
	protected WfEvtServiceDefService wfEvtServiceDefService;
	@Resource
	private ProcessCoreService processCoreService;
	@Resource
	private ModelPropService modelPropService;
	@Resource
	private WfParamService wfParamService;
	@Resource
	private UruleServiceFactory uRule;
	@Resource
	private QueryRelationTellerFactory queryRelationTellerFactory;
	@Resource
	private WfInstanceService wfInstanceService;
	@Resource
	private WfTaskflowService wfTaskflowService;
	@Resource
	private ConfigUtil configUtil;
	@Resource
	private CacheActionUserAssignerLocal cacheActionUserAssignerLocal;
	/**
	 *
	 */
	private static final long serialVersionUID = 1970367836446022669L;

	private static final String JSON_OBJECT_CLASS = "com.alibaba.fastjson.JSONObject";

	@Override
	public void notify(DelegateTask delegateTask) {
		Map<String, Object> varMap = WfVariableUtil.getVariableMap();
		SysCommHead sysCommHead=WfVariableUtil.getSysCommHead();
		if (log.isInfoEnabled()) {
			log.info("| - TaskCreateListener>>>>>创建代办任务[taskDefId:{},taskDefName:{}]",delegateTask.getTaskDefinitionKey(),delegateTask.getName() );
			log.info("| - TaskCreateListener>>>>>创建当前任务ID[{}]",delegateTask.getId());
			log.info("| - TaskCreateListener>>>>>创建当前流程定义ID[{}]",delegateTask.getProcessDefinitionId());
			log.info("| - TaskCreateListener>>>>>创建当前流程实例ID[{}]", delegateTask.getProcessInstanceId());
			log.info("| - TaskCreateListener>>>>>创建业务参数[{}]",varMap.toString());
			log.info("| - TaskCreateListener>>>>>请求的报文头[{}]",sysCommHead);
		}
		if (!isStart(delegateTask)) {
			Map<String, Object> variableMap = WfVariableUtil.getVariableMap();

			doNotify(delegateTask.getExecution().getCurrentFlowElement(), variableMap,
					delegateTask.getProcessDefinitionId(), delegateTask.getProcessInstanceId(), delegateTask.getId(),
					delegateTask.getTaskDefinitionKey(), delegateTask.getName(), null,sysCommHead);

		}
	}

	private boolean isStart(DelegateTask delegateTask) {
		List<WfTaskflow> wfTaskflows = wfTaskflowService.selectByProcessInstId(delegateTask.getProcessInstanceId());
		if (wfTaskflows == null || wfTaskflows.size() == 0
				|| (wfTaskflows.size() == 1 && "00000".equals(wfTaskflows.get(0).getTaskId()))) {
			return true;
		} else {
			return false;
		}
	}

	public void doNotify(FlowElement flowElement, Map<String, Object> varMap, String processDefId, String processInstId,
						 String taskInstId, String taskDefKey, String taskName, WfInstance wfInstance, SysCommHead sysCommHead) {
		if(varMap == null){
			throw new ServiceException("varMap is null");
		}
		// 获取节点的配置信息
		String nodeType = processUtil.getTaskProp(processDefId,flowElement, Constants.NODETYPE);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
		String nodeId =  processUtil.getTaskProp(processDefId,flowElement,Constants.NODEPROP_REFID);//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);

		if(StrUtil.isBlank(nodeId)){
			String errmsg = "流程定义["+processDefId+"]中的["+flowElement.getName()+"]节点没有配置<建模节点映射ID>，请查看！";
			log.info(errmsg);
			throw new ServiceException(errmsg);
		}

		List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);
		String globalSeqno = null;
		if(sysCommHead != null)
		{
			globalSeqno = sysCommHead.getGlobalSeqNo();
		}

		log.info("| - TaskCreateListener>>>>>node type : {},主流水号【{}】", nodeType, globalSeqno);
		// 进入条件判断 ---应该在节点开始是判断
		commService.conditionalVerification(ModelProperties.intputCondition.getName(), extendsPropList, varMap, ErrorCode.WF000022);

		Long backFromTask = (Long) varMap.get(Constants.BACK_FROM_TASK);
		WfTaskflow taskflow = null;

		// 节点分类处理
		switch (nodeType) {
			case Constants.NODE_TYPE_ATTO:
				break;
			case Constants.NODE_TYPE_SIGN:
				// 会签功能
				return;
			case Constants.NODETYPE_SUBPROC:
				String beforTaskId = (String) varMap.get(Constants.BEFOR_TASKID);
				taskflow = priAddWfTaskflow(wfInstance, taskInstId, processInstId, processDefId,
						taskDefKey, taskName, varMap, flowElement, nodeType, "", null, beforTaskId, null,null,
						globalSeqno);
				break;
			case Constants.NODETYPE_TASK:
			case Constants.NODETYPE_APPROVE:
				taskflow = taskNodeExecution(wfInstance, taskInstId, processInstId, processDefId, taskDefKey, taskName, varMap,
						flowElement, nodeType, globalSeqno, extendsPropList);
				break;
			default:
				log.info("node type error!!");
				break;
		}
		Map<String, Object> newMap = new HashMap<>(varMap);
		if(backFromTask != null && backFromTask > 0)
		{
			newMap.put(Constants.BACK_FROM_TASK,BigInteger.valueOf(backFromTask));// 0-通知任务、1-处理任务、2-转办任务、3-加签任务
		}else {
			newMap.put(Constants.BACK_FROM_TASK,BigInteger.ZERO);// 0-通知任务、1-处理任务、2-转办任务、3-加签任务
		}

		// 调用节点进入触发事件
		WfModelPropExtends startEventInvock = modelPropService.getSpcefiyModelProp(extendsPropList,
				ModelProperties.startTaskEvent.getName());
		wfEvtServiceDefService.executeEventService(startEventInvock, taskInstId, processDefId, processInstId, newMap,
				flowElement,sysCommHead);

		//start 2019-04-09 去掉直接插入taskflow的操作，在服务调用完成以后进行插入，否则会导致处理的任务和生成代码任务的流转参数不一致
		if(Objects.nonNull(taskflow))
		{
			this.insertIntoWfTaskflow(taskflow, varMap, extendsPropList, globalSeqno);
		}
		//end 2019-04-09 去掉直接插入taskflow的操作，在服务调用完成以后进行插入，否则会导致处理的任务和生成代码任务的流转参数不一致
		varMap.remove(Constants.BACK_FROM_TASK);
	}

	/**
	 * 插入流程流转
	 * @param taskflow
	 * @param variableMap
	 * @param modelPropList
	 */
	public void insertIntoWfTaskflow(WfTaskflow taskflow,Map<String,Object> variableMap,List<WfModelPropExtends> modelPropList,String glob)
	{

		if(Objects.nonNull(variableMap))
		{
			taskflow.setBussinessMap(BpmStringUtil.mapToJson(variableMap).toString());
		}else {
			variableMap=WfVariableUtil.getVariableMap();
		}
		//2019-04-16 amt add 插入之前判断是否满足任务池配置
		WfModelPropExtends taskPoolProp = modelPropService.getSpcefiyModelProp(modelPropList,
				ModelProperties.checkInOut.getName());
		if(taskPoolProp!=null)
		{
			taskflow.setTaskPoolFlag(Constants.IS_TASK_POOL);
		}
		//2019-04-16 amt add 插入之前判断是否满足任务池配置
		processUtil.batchAddCandidateUser(taskflow.getTaskId(), taskflow.getTaskActors());
		wfTaskflowService.insertWfTaskflow(taskflow);
		WfBusinessData businessData = new WfBusinessData(taskflow);
		wfBusinessDataService.insertWfBusinessData(businessData);

	}

	/**
	 * pecifyNextOpersList非空时，
	 * 此时系统将不再调用公共选人服务接口获取对应的信息，而是直接取此上送的内容作为对应的可处理人员信息；
	 * @param specifyNextOpersList
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<UserOutputData> getUserList(Object specifyNextOpersList) {
		List<UserOutputData> result=null;
		if(JSON_OBJECT_CLASS.equals(specifyNextOpersList.getClass().getTypeName()))
		{
			result=new ArrayList<UserOutputData>();
			Map<String, JSONObject> map= (Map<String,JSONObject>) specifyNextOpersList;
			for (Map.Entry<String, JSONObject> entry : map.entrySet()) {
				UserOutputData user = constructUserOutputData(BpmStringUtil.jsonToMap(entry.getValue()));
				result.add(user);
			}
		}else  if(specifyNextOpersList.getClass().isInstance(List.class))
		{
			result=new ArrayList<UserOutputData>();
			List specifyNextOpersList1=(List) specifyNextOpersList;
			for(Object obj:specifyNextOpersList1)
			{
				UserOutputData user = constructUserOutputData(BpmStringUtil.jsonToMap(obj));
				result.add(user);
			}
		}
		else {
			JSONArray specifyNextOpersList1=(JSONArray) specifyNextOpersList;
			result=new ArrayList<UserOutputData>();
			for(int i=0;i<specifyNextOpersList1.size();i++)
			{
				JSONObject json=specifyNextOpersList1.getJSONObject(i);
				UserOutputData user = constructUserOutputData(json);
				result.add(user);
			}
		}
		return result;
	}

	/**
	 * 将字符串或map形式的UserOutputData数据转化为bean形式
	 * @param userDataMap
	 * @return
	 */
	private UserOutputData constructUserOutputData(Map<String,Object> userDataMap) {
		UserOutputData user=new UserOutputData();
		user.setUserId((String) userDataMap.get("userId"));
		user.setUserBank((String)userDataMap.get("userBank"));
		user.setUserBankName((String)userDataMap.get("userBankName"));
		user.setUserEmail((String)userDataMap.get("userEmail"));
		user.setUserJobBank((String)userDataMap.get("userJobBank"));
		user.setUserJobBankName((String)userDataMap.get("userJobBankName"));
		user.setUserPhone((String)userDataMap.get("userPhone"));
		user.setUserName((String)userDataMap.get("userName"));
		user.setUserPost((String)userDataMap.get("userPost"));
		user.setUserPostName((String)userDataMap.get("userPostName"));
		user.setUserRole((String)userDataMap.get("userRole"));
		user.setUserRoleName((String)userDataMap.get("userRoleName"));
		user.setUserState((String)userDataMap.get("userState"));
		user.setUserWeChat((String)userDataMap.get("userWeChat"));
		return user;
	}

	/**
	 * 跳过当前节点，自动调用
	 *
	 * @param variableMap
	 */
//	@SuppressWarnings({ "unchecked", "unused" })
//	private static void skipSelectPersion(TimerUtil timerUtil, String taskId, Map<String, Object> variableMap) {
//		boolean next = false;
//		if (variableMap.containsKey(Constants.PROCESS_CTRL)) {
//			Map<String, Object> map = (Map<String, Object>) variableMap.get(Constants.PROCESS_CTRL);
//
//			if (map.containsKey(Constants.IS_NO_PERSION_SKIP)) {
//				String isNoPersionSkip = map.get(Constants.IS_NO_PERSION_SKIP).toString();
//				if (isNoPersionSkip != null && isNoPersionSkip.equals("true")) {
//					timerUtil.addWorkflowTimerTask(new BackInvockAutoTask(taskId, variableMap), 0, 0, 0, 0, 0, 60);
//					next = true;
//				}
//			}
//
//		}
//		if (!next) {
//			throw new ServiceException("选人失败！！！");
//		}
//	}

	/**
	 * 已经在上一个节点流转之前选人了
	 *
	 * @throws Exception
	 *
	 */
	/**
	 * 清楚业务数据中的每一用的参数
	 *
	 * @param variableMap
	 * @param element
	 */
	private static void clearPreSelectPersion(Map<String, Object> variableMap, FlowElement element) {

		if (variableMap.containsKey(Constants.ROUTE_SELECT_KEY)) {
			FlowNode flowNode = (FlowNode) element;
			List<SequenceFlow> sequenceList = flowNode.getIncomingFlows();

			JSONArray array = new JSONArray();
			if (ObjectUtil.isNotEmpty(variableMap.get(Constants.ROUTE_SELECT_KEY))) {
				if (variableMap.get(Constants.ROUTE_SELECT_KEY) instanceof JSONArray) {
					array = (JSONArray) (variableMap.get(Constants.ROUTE_SELECT_KEY));
				} else if (variableMap.get(Constants.ROUTE_SELECT_KEY) instanceof ArrayList) {
					array = JSONArray.parseArray(JSONUtil.toJsonStr(variableMap.get(Constants.ROUTE_SELECT_KEY)));
				}
				for (int i = 0; i < array.size(); i++) {
					JSONObject obj = array.getJSONObject(i);
					if (obj.getString("branch").equals(sequenceList.get(0).getDocumentation())) {
						JSONArray userAskListArray = obj.getJSONArray("useAskList");
						if (userAskListArray == null || userAskListArray.isEmpty()){
							return;
						}

						array.remove(obj);
					}
				}
			}
		}
		return;

	}

	private WfTaskflow taskNodeExecution(WfInstance wfInstance, String taskInstId, String processInstId, String processDefId,
										 String taskDefKey, String taskName, Map<String, Object> variableMap, FlowElement element, String nodeType,String globalSeqno,
										 List<WfModelPropExtends> modelPropList) {

		// 获取上一节点
		String beforTaskId = (String) variableMap.get(Constants.BEFOR_TASKID);

		String nodePath = processUtil.getTaskProp(processDefId,element,Constants.NODEPATH);

		// 选择任务执行人
		List<UserOutputData> userDataList = null;

		try {
			String userIds = (String) variableMap.get(Constants.SPECIFYEXTOPERS);
			log.info("variableMap.containsKey(Constants.SPECIFYEXTOPERS):"+variableMap.containsKey(Constants.SPECIFYEXTOPERS)+
					" \nStrUtil.isNotEmpty(userIds) :"+StrUtil.isNotEmpty(userIds)
					+"\n!Constants.STR_NULL_UPPER.equals(userIds.toUpperCase()) "+(userIds )
			);
			// 前台指定执行人员 处理
			if (variableMap.containsKey(Constants.SPECIFYEXTOPERS) && StrUtil.isNotEmpty(userIds) && !Constants.STR_NULL_UPPER.equals(userIds.toUpperCase()) ) {
				List<Map<String, String>> idList = new ArrayList<>();
				boolean sysAutoFlag = false;
				for (String id : userIds.split(SymbolConstants.COMMA)) {
					Map<String, String> tempMap = new HashMap<>();
					if(StrUtil.isNotEmpty(id)) {
						if(BpmConstants.WORKFLOW_ADMIN_ID.equals(id)){
							sysAutoFlag = true;
							break;
						}
						tempMap.put("orderUserNo", id);
						idList.add(tempMap);
					}

				}
				if(sysAutoFlag){
					log.info("--sysAutoFlag");
					userDataList = BpmConstants.SYS_USER_INFO;
				}else {
					log.info("--not sysAutoFlag"+variableMap.containsKey(Constants.BACK_FROM_TASK));
					if(variableMap.containsKey(Constants.BACK_FROM_TASK)){

						String backFromTask = String.valueOf(variableMap.get(Constants.BACK_FROM_TASK));
						// 退回节点选人处理
						if (BpmConstants.DO_RESULT_4.equals(backFromTask) || BpmConstants.DO_RESULT_5.equals(backFromTask) || BpmConstants.DO_RESULT_7.equals(backFromTask)) {
							variableMap.put("firstNode", true);
							WfVariableUtil.setVariableMap(variableMap);
						}
					}
					log.info("--queryRelationTellerFactory.getInstance().queryUserList(BpmStringUtil.listToJson(idList))"+idList);
					userDataList = queryRelationTellerFactory.getInstance().queryUserList(BpmStringUtil.listToJson(idList));
					log.info("--userDataList"+userDataList);
					variableMap.remove("firstNode");
					WfVariableUtil.setVariableMap(variableMap);
				}
			}else if(variableMap.containsKey(Constants.SPECIFYNEXTBANKPOS)) {
				String specifyNextBankPost=(String) variableMap.get(Constants.SPECIFYNEXTBANKPOS);
				if(!specifyNextBankPost.contains(SymbolConstants.COMMA)) {
					throw new ServiceException("指定的目标机构、目标岗位格式错误");
				}
				JSONArray array=JSONArray.parseArray("[]");
				String [] strs=specifyNextBankPost.split(";");
				if(strs!=null)
				{
					for(int i=0;i<strs.length;i++) {
						if(!strs[i].contains(SymbolConstants.COMMA)) {
							continue;
						}
						Map<String, String> tempMap = new HashMap<String, String>();

						String orderBankNo=strs[i].split(SymbolConstants.COMMA)[0];
						String orderUserPost=strs[i].split(SymbolConstants.COMMA)[1];
						tempMap.put("orderBankNo", orderBankNo);
						tempMap.put("orderUserPost", orderUserPost);
						array.add(tempMap);

					}
				}
				userDataList = queryRelationTellerFactory.getInstance().queryUserList(array);
			}else if(variableMap.containsKey(Constants.SPECIFY_NEXT_OPERS_LIST)) {
				userDataList=this.getUserList(variableMap.get(Constants.SPECIFY_NEXT_OPERS_LIST));
			} else {
				userDataList = commService.findNodeUsers(taskInstId, processDefId, processInstId, variableMap, element);

				//过滤掉重复处理人员
				List<String> recvCommUserList = new ArrayList<String>();
				String taskCode = element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
				StringBuffer buf = new StringBuffer();
				for (UserOutputData userData : userDataList) {
					buf.append(SymbolConstants.COMMA).append(userData.getUserId());
				}
				String assigner = buf.toString();
				if (userDataList.size() > 0) {
					assigner = assigner.substring(1);
				}
				int exectType= processCoreService.sameExecutorCheck(processInstId, assigner, taskCode, recvCommUserList);
				List<UserOutputData> targetList = new ArrayList<UserOutputData>();
				if (exectType == BpmConstants.AUTO_EXEC_FLAG_3) {// 要求全部匹配，但只适配了部分人员，则移除这部分匹配人员
					recvCommUserList.add((String)variableMap.get(Constants.BEFOR_USERID));//添加上一处理人
					//过滤掉重复的人员
					go: for (UserOutputData userData : userDataList) {
						for (String commUserId : recvCommUserList) {
							if (userData.getUserId().equals(commUserId)) {
								continue go;
							}
						}
						targetList.add(userData);
					}
					userDataList=targetList;
				}
				//update  end by xuesw   20190520 过滤掉重复处理人员
			}
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			log.error(BpmStringUtil.getExceptionStack(e),e);
			throw new ServiceException(e.getMessage());
		}
		if (userDataList == null || userDataList.isEmpty()) {
			//在执行的时候禁止选人为空,选人为空的处理只在findNextNodeInfo中进行处理,跳过为空的人员节点供前端选择
			log.error("任务:"+taskInstId+"选人为空,请确认选人规则配置是否正确！！！");
			throw new ServiceException("根据规则选人为空，请确认选人策略配置是否正确或有对应的人员、岗位、机构信息!");
		}
		variableMap.remove("firstNode");
		// 新增一条流转信息
		String url;
		try {
			url = modelPropService.getNomalOrRulePropValue(modelPropList, ModelProperties.functionList.getName(),
					variableMap);
		} catch (Exception e) {
			log.error("| - TaskCreateListener>>>>>任务:{}功能清单解析失败",taskInstId,e);
			throw new ActivitiException(e.getMessage());
		}
		WfModelPropExtends taskAllocationType = modelPropService.getSpcefiyModelProp(modelPropList,
				ModelProperties.taskAllocationType.getName());
		String taskAllocationTypeValue = null;
		if(taskAllocationType != null)
		{
			taskAllocationTypeValue = taskAllocationType.getPropValue();
		}
		return priAddWfTaskflow(wfInstance, taskInstId, processInstId, processDefId,
				taskDefKey, taskName, variableMap, element, nodeType, url, "", beforTaskId, null, taskAllocationTypeValue, globalSeqno,
				userDataList.toArray(new UserOutputData[userDataList.size()]));

	}

	/**
	 * 子流程节点处理之--1.获取子流程列表；2.启动子流程
	 *
	 * @param modelPropList
	 */
	public void subProceExecution(FlowElement element, Map<String, Object> varMap, String taskInstId,
								  String processDefId, String processInstId, String taskDefKey, String executionId, String taskName,
								  List<WfModelPropExtends> modelPropList) {

		// 1.通过子流程规则配置获取子流程编码
		List<String> ruleSubList = new ArrayList<String>();
		String subCodeconfig = processUtil.getTaskProp(processDefId,element,Constants.SBUPROC_CODELIST);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.SBUPROC_CODELIST);
		log.info("| - TaskCreateListener>>>>>子流程规则配置：{}" , subCodeconfig);
		if (StrUtil.isBlank(subCodeconfig)) {
			log.info("| - TaskCreateListener>>>>>子流程选取规则配置为空！");
		} else {
			// 通过规则引擎获取子流程列表
			String subCodeList = uRule.getInstance().execUruleReusltList(varMap, JSONObject.parseObject(subCodeconfig));
			log.info("| - TaskCreateListener>>>>>通过子流程选取规则得到流程编码：{}" , subCodeList);
			if (StrUtil.isNotBlank(subCodeList)) {
				JSONArray codeArrs = JSONArray.parseArray(subCodeList);
				if (codeArrs.isEmpty()) {
					log.info("| - TaskCreateListener>>>>>子流程选取规则得到的流程编码为空！");
				} else {
					Iterator<Object> iter = codeArrs.iterator();
					String temp = "";
					while (iter.hasNext()) {
						temp = iter.next().toString();
						for (String code : temp.split(SymbolConstants.COMMA)) {
							ruleSubList.add(code);
						}
					}
				}
			}
		}

		// 2.获取扩展属性中配置的流程编码
		String subconfig = modelPropService.getNomalModelPropValue(modelPropList,
				ModelProperties.subprocessSelection.getName());
		log.info("| - TaskCreateListener>>>>>扩展属性配置的子流程编码：{}" , subconfig);

		List<String> subCodeResultList = new ArrayList<String>();
		if (StrUtil.isNotBlank(subconfig)) {
			// 规则获取的编码必须在扩展属性的子流程编码中存在，否则剔除
			JSONArray subConfigObj = JSONArray.parseArray(subconfig);
			StringBuffer strbuf = new StringBuffer();
			for (Object obj : subConfigObj) {
				@SuppressWarnings("unchecked")
				Map<String, String> map = (Map<String, String>) obj;
				if (ruleSubList.isEmpty()) {
					subCodeResultList.add(map.get("defId"));
				} else {
					strbuf.append(SymbolConstants.COMMA);
					strbuf.append(map.get("defId"));
				}
			}
			if (!ruleSubList.isEmpty()) {
				strbuf.append(SymbolConstants.COMMA);
				String matcahStr = strbuf.toString();
				for (String item : ruleSubList) {
					for (String temp : item.split(SymbolConstants.COMMA)) {
						if (matcahStr.indexOf(SymbolConstants.COMMA + temp + SymbolConstants.COMMA) != -1) {
							subCodeResultList.add(temp);
						} else {
							log.info("| - TaskCreateListener>>>>>流程[{}]不在指定的子流程列表范围内，故排除！",temp);
						}
					}
				}
			}
			if (subCodeResultList.size() == 0) {
				log.info("| - TaskCreateListener>>>>>根据子流程选取规则和子流程扩展属性综合处理得到的流程编码为空！");
				throw new ServiceException("根据子流程选取规则和子流程扩展属性综合处理得到的流程编码为空！");
			}
		} else {
			// 扩展属性的流程编码没有配置，抛出异常
			log.info("| - TaskCreateListener>>>>>子流程扩展属性得到的流程编码为空！");
			throw new ServiceException("子流程扩展属性得到的流程编码为空！");
		}

		String startPerson =  processUtil.getTaskProp(processDefId,element,Constants.START_PERSON);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.START_PERSION);
		if(StrUtil.isEmpty(startPerson))
		{
			startPerson="0";
		}
		Map<String, Object> userIdMap=getStartUserIdMap(startPerson,varMap);
		String startUserId=configUtil.getAutoTaskActor();
		if( userIdMap==null)
		{
			userIdMap = new HashMap<String, Object>();
			userIdMap.put(Constants.USERID_KEY, startUserId);
		}
		else
		{
			startUserId=(String) userIdMap.get(Constants.USERID_KEY);
		}



		// 3.启动子流程
		for (String code : subCodeResultList) {
			WorkFlowStartData workFlowStartData = new WorkFlowStartData();
			workFlowStartData.setUserId(startUserId);
			workFlowStartData.setChildFlag(true);
			//新增发起子流程的主流程中的上一处理处理人标识
//			按照要求,剔除此流程节点的上一处理人干系人信息,因在子流程节点属性中已加了相应的控制标志
			//	varMap.put( Constants.CHILD_START_USER_ID, varMap.get(Constants.BEFOR_USERID));


			varMap.put(Constants.PROCESS_TASK_ID, taskInstId);

			varMap.put(Constants.TASK_CATEGORY_ID_PARENT, varMap.get(Constants.TASK_CATEGORY_ID));
			varMap.put(Constants.TASK_CATEGORY_ID, code);
			//获取主流程的小类编码
			WfParam wfParam=new WfParam();
			wfParam.setParamObjCode(code);
			wfParam.setParamObjType(Constants.PARAM_TYPE_PROCESSCODE);
			//根据流程编码获取流程小类
			//根据TZ这边的流程编码查找规则,直接读取参数配置表信息
			List<WfParam> listParam=null;
			try {
				listParam = wfParamService.selectParamList(wfParam);
				if(listParam!=null&&listParam.size()==1) {
					varMap.put(Constants.TASK_CATEGORY_ID,listParam.get(0).getParamCode());
				}

			} catch (Exception e1) {
				log.error("| - TaskCreateListener>>>>>根据流程编码获取流程小类失败！",e1);

			}
			String specifyNextNode = (String) varMap.get(Constants.SPECIFYNEXTNODE);
			String specifyNextOpers = (String) varMap.get(Constants.SPECIFYEXTOPERS);

			//删除主流程中对应的控制参数信息
			varMap.remove(Constants.ROUTE_SELECT_KEY);
			varMap.remove(Constants.ROUTE_CONDITION_KEY);
			varMap.remove(Constants.SPECIFYEXTOPERS);
			varMap.remove(Constants.SPECIFYNEXTNODE);
			varMap.remove(Constants.SPECIFYNEXTFLOW);
			if(StrUtil.isNotEmpty(specifyNextNode))
			{
				FlowNode  flowNode=(FlowNode)element;
				Iterator<SequenceFlow> iter=flowNode.getIncomingFlows().iterator();
				while(iter.hasNext())
				{
					SequenceFlow seq=iter.next();
					specifyNextNode=specifyNextNode.replaceAll(seq.getId()+SymbolConstants.COMMA, "");
					varMap.put(Constants.SPECIFYNEXTNODE, specifyNextNode);
					varMap.put(Constants.SPECIFYEXTOPERS,specifyNextOpers);
				}
			}
			workFlowStartData.setSystemIdCode(String.valueOf(varMap.get(Constants.SYSTEMIDCODE_KEY)));
			workFlowStartData.setBussinessMap(varMap);
			workFlowStartData.setUserIdMap(userIdMap);
			workFlowStartData.setProcessCode(code);
			workFlowStartData.setSysCommHead(WfVariableUtil.getSysCommHead());
			try {
				processCoreService.startWorkflowDef(workFlowStartData);
				log.info("| - TaskCreateListener>>>>>子流程[{}]启动成功！",code);
			} catch (Exception e) {
				log.info("| - TaskCreateListener>>>>>子流程[{}]启动失败：{}",code, e.getMessage());
				log.error(BpmStringUtil.getExceptionStack(e), e);
				throw new ServiceException("子流程[" + code + "]启动失败！"+e.getMessage());
			}
		}

		// 添加子流程数量
		varMap.put("MainWfSubCount", subCodeResultList.size());

		WfVariableUtil.addAllVariableMap(varMap);
	}
	/**
	 * 获取子流程的流程发起人
	 * @param varMap
	 * @return
	 */
//	private String getStartUserId( Map<String, Object> varMap) {
//		if(varMap == null) {
//			return null;
//		}
//		Map<String, Object> userIdMap =(Map<String, Object>)varMap.get(Constants.STARTUSERDATA);
//		if(userIdMap==null||userIdMap.isEmpty()||userIdMap.get(Constants.USERID_KEY)==null) {
//			return null;
//		}
//		return userIdMap.get(Constants.USERID_KEY).toString();
//	}

	/**
	 * 获取流程发起人信息
	 * @param strUserFlag
	 * @param varMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getStartUserIdMap( String strUserFlag,Map<String, Object> varMap) {
		if(varMap == null) {
			return null;
		}
		Map<String, Object> userIdMap;
		if(BpmConstants.STR_USER_FLAG_1.equals(strUserFlag))
		{//取上一处理人为子流程的流程发起人
			userIdMap =(Map<String, Object>) varMap.get(Constants.BEFORUSERDATA);
		}
		else
		{//缺省取主流程的发起人维子流程的发起人
			userIdMap =(Map<String, Object>)varMap.get(Constants.STARTUSERDATA);
		}

		return userIdMap;
	}
	/**
	 * 子流程节点的处理之 -- 根据配置进行流转
	 * @param varMap
	 * @param t
	 * @param target
	 * @throws Exception
	 */
	public void commitSubFlowTask(Map<String, Object> varMap, Task t, FlowElement target) {
		// 子流程调用,根据配置判断是否向下流转
		String nodeRefId =  processUtil.getTaskProp(t.getProcessDefinitionId(),target,Constants.NODEPROP_REFID);//target.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
		WfModelPropExtends selectCondation = new WfModelPropExtends();
		selectCondation.setOwnId(nodeRefId);
		selectCondation.setOwnType("0");
		List<WfModelPropExtends> modelPropList = wfModelPropExtendsService
				.selectModelExtendsPropByOwnIdAndOwnType(selectCondation);

		// 获取子流程列表并启动子流程
		subProceExecution(target, varMap, t.getId(), t.getProcessDefinitionId(), t.getProcessInstanceId(),
				t.getTaskDefinitionKey(), t.getExecutionId(), t.getName(), modelPropList);

		// 如果子流程为不等待，则直接进入下一步
		String waitType =  processUtil.getTaskProp(t.getProcessDefinitionId(),target,Constants.WAITTYPE);//target.getAttributeValue(Constants.XMLNAMESPACE, Constants.WAITTYPE);
		if (waitType.equals(Constants.WAITTYPE_NOWAIT)) {
			WorkFlowTaskTransferData data = new WorkFlowTaskTransferData();
			data.setBussinessMap(WfVariableUtil.getVariableMap());
			data.setTaskId(t.getId());
			data.setUserId("system");
			processCoreService.nextTask(data);
		}
//		else {
//			// 等待模式不需要自动流转，等待外界触发
//		}
	}

	/**
	 * 增加流程流转信息
	 *
	 * @param delegateTask
	 */
	protected void addWfTaskflow(WfInstance wfInstance, DelegateTask delegateTask, String type, String url,
								 String parentTaskId, String beforTaskId, String systemId,String taskAllocationType,String globalSeqno,UserOutputData... userdataList) {
		String nodeId = delegateTask.getExecution().getCurrentFlowElement().getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);

		List<WfModelPropExtends> extendsPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(nodeId);

		WfTaskflow taskflow=priAddWfTaskflow(wfInstance, delegateTask.getId(),
				delegateTask.getProcessInstanceId(), delegateTask.getProcessDefinitionId(),
				delegateTask.getTaskDefinitionKey(), delegateTask.getName(), delegateTask.getExecution().getVariables(),
				delegateTask.getExecution().getCurrentFlowElement(), type, url, parentTaskId, beforTaskId, systemId,taskAllocationType, globalSeqno,
				userdataList);
		this.insertIntoWfTaskflow(taskflow, null, extendsPropList,globalSeqno);

	}

	public  WfTaskflow priAddWfTaskflow(WfInstance wfInstance, String taskInstId, String processInstId, String processDefId,
										String taskDefKey, String taskName, Map<String, Object> variableMap, FlowElement flowElement, String type,
										String url, String parentTaskId, String beforTaskId, String systemId,String taskAllocationType ,String globalSeqno, UserOutputData... userdataList) {
		Map<String, UserOutputData> map = new HashMap<>();
		// 给任务分配人员
		StringBuffer buf = new StringBuffer();
		for (UserOutputData userData : userdataList) {
			buf.append(SymbolConstants.COMMA).append(userData.getUserId());
			map.put(userData.getUserId(), userData);
		}
		String assigner = buf.toString();
		if (userdataList.length > 0) {
			assigner = assigner.substring(1);
		}
		//{"type":"2SymbolConstants.COMMAdescp":"百分比分配SymbolConstants.COMMApercentage":"10,40,50"}
		log.info("| - TaskCreateListener>>>>>节点人员分配策略[{}]",taskAllocationType);
		if (StrUtil.isNotBlank(taskAllocationType)) {
			//增加人员分配策略为最小并发数
			if(StrUtil.equals(taskAllocationType, CacheActionUserAssignerLocal.TaskAllocationType.minConcurrent.getValue()))
			{
				List<TaskCodeCount> list= wfTaskflowService.selectUnoTaskCountByUsers(Arrays.asList(assigner.split(SymbolConstants.COMMA)));
				if(list!=null&&!list.isEmpty())
				{
					assigner=list.get(0).getUserId();
				}else {
					log.info("| - TaskCreateListener>>>>>节点对应人员信息[{}]没有代办任务",assigner);
					assigner=userdataList[0].getUserId();
					log.info("| - TaskCreateListener>>>>>分配给人员id[{}]",assigner);
				}
			}else {
				Map<String, Object> taskAllocationMap = BpmStringUtil.jsonToMap(taskAllocationType);
				String allocationType = (String) taskAllocationMap.get("type");
				if (!BpmConstants.ALLOCATION_TYPE_0.equals(allocationType)) {
					String percentage = (String) taskAllocationMap.get("percentage");
					// //把人员信息存入内存中
					cacheActionUserAssignerLocal.putUserAssginer2Cache(taskDefKey, assigner);
					assigner = cacheActionUserAssignerLocal.getUserAssginerFromCache(taskDefKey, allocationType,
							percentage);
				}
			}
		}
		if(variableMap == null){
			throw new ServiceException("variableMap is null");
		}
		variableMap.put(Constants.TASK_USERS, assigner);
		log.debug("| - TaskCreateListener>>>>>priAddWfTaskflow当前任务处理人员assigner:{}" , assigner);
		WfVariableUtil.addVariableMap(Constants.TASK_USERS, assigner);
		JSONObject jsonObj = BpmStringUtil.mapToJson(map);
		String userDataStr = jsonObj.toString();

		// 设置任务级别 0-一般任务 1-首节点任务 2-关键任务
		BigInteger taskRate = BigInteger.ZERO;
		List<SequenceFlow> list = ((FlowNode) flowElement).getIncomingFlows();
		for (SequenceFlow inFlow : list) {
			if (inFlow.getSourceFlowElement() instanceof StartEvent) {
				taskRate = BigInteger.ONE;
			}
		}

		// 设置功能清单
		if (StrUtil.isNotBlank(url)) {
			// 去掉[]
			if (url.startsWith(SymbolConstants.SYMBOL_003) && url.endsWith(SymbolConstants.SYMBOL_004)) {
				url = url.substring(1, url.length() - 1);
				if (url.startsWith(SymbolConstants.SYMBOL_012) && url.endsWith(SymbolConstants.SYMBOL_012)) {
					url = url.substring(1, url.length() - 1);
				}
			}
		}
		String bussSubFlow = null;
		if(globalSeqno != null && globalSeqno.length() > Constants.BUSS_MAN_FLOW_LEN)
		{
			bussSubFlow = globalSeqno.substring(Constants.BUSS_MAN_FLOW_LEN);
			globalSeqno = globalSeqno.substring(0, Constants.BUSS_MAN_FLOW_LEN);
		}


		WfTaskflow taskflow = new WfTaskflow();
		taskflow.setTaskId(taskInstId);
		taskflow.setTaskActors(assigner);
		taskflow.setProcessId(processInstId);
		taskflow.setProcessCode(processDefId);
		taskflow.setTaskDefId(taskDefKey);
		taskflow.setBussSubFlow(bussSubFlow);
		taskflow.setBussManFlow(globalSeqno);
		taskflow.setTaskCode(processUtil.getTaskProp(processDefId,flowElement,Constants.NODEPROP_REFID));//flowElement.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID));
		taskflow.setTaskType(type);
		taskflow.setTaskName(taskName);
		taskflow.setSystemCode(systemId);
		taskflow.setWorkState(Constants.TASK_STATE_RUNING);

		// taskName变量替换
		taskflow.setTaskName(taskName);
		if (StrUtil.isNotBlank(taskName)) {
			String taskNameTemp = BpmStringUtil.templatToMsgContent(taskName, variableMap);
			if (StrUtil.isNotBlank(taskNameTemp)) {
				taskflow.setTaskName(taskNameTemp);
			}
		}

		//不会出现类型转换异常
		Long backFromTask = 0L;
		if(variableMap.containsKey(Constants.BACK_FROM_TASK)) {
			backFromTask=Long.parseLong(variableMap.get(Constants.BACK_FROM_TASK).toString());
		}
		/*start 2019-02-15 modify amt  审批被打回后针对无需审批节点查找的实现*/
		//worktype:0-通知任务、1-处理任务、2-转办任务、3-加签任务、4-需要重新审批的打回任务、5-撤回（追回）、6-动态加签通知、7-无需审批的打回任务
		if(backFromTask!=null&&backFromTask>0)
		{
			taskflow.setWorkType(Integer.parseInt(String.valueOf(backFromTask)));// 0-通知任务、1-处理任务、2-转办任务、3-加签任务
			variableMap.remove(Constants.BACK_FROM_TASK);
		}else {
			taskflow.setWorkType(1);// 0-通知任务、1-处理任务、2-转办任务、3-加签任务
		}
		/* 审批被打回后针对无需审批节点查找的实现*/

		// 任务紧急程度、任务发起渠道修改
		String taskUargent = "0";
		if (variableMap.containsKey(Constants.TASK_URGENT_KEY)) {
			taskUargent = (String) variableMap.get(Constants.TASK_URGENT_KEY);
		}

		String tranChannel = "";
		if (variableMap.containsKey(Constants.INPUT_TRANCHANNEL_KEY)) {
			tranChannel = (String) variableMap.get(Constants.INPUT_TRANCHANNEL_KEY);
		}
		taskflow.setTranChannel(tranChannel);
		taskflow.setTaskUrgent(Integer.parseInt(taskUargent.trim()));
		// update end by xuesw 20180418 任务紧急程度、任务发起渠道修改

		taskflow.setTaskActorsData(userDataStr);
		taskflow.setTaskRate(Integer.parseInt(taskRate.toString()));
		taskflow.setWorkDoFlag("0");
		taskflow.setWorkUsrService(url);
		taskflow.setTaskDoState("0");// 0-未催办1-催办2-再次催办
		LocalDateTime createDate = LocalDateTime.now();
		taskflow.setCreateDate(createDate);
		taskflow.setStartDate(createDate);
		if(variableMap.get(Constants.ROOT_PROCESS_ID) != null) {
			taskflow.setRootProcessId(variableMap.get(Constants.ROOT_PROCESS_ID).toString());
		}
		if(variableMap.get(Constants.ROOT_PROCESS_CODE) != null) {
			taskflow.setRootProcessCode(variableMap.get(Constants.ROOT_PROCESS_CODE).toString());
		}
		if(variableMap.get(Constants.ROOT_PROCESS_NAME) != null) {
			taskflow.setRootProcessName(variableMap.get(Constants.ROOT_PROCESS_NAME).toString());
		}
		if(BpmConstants.START_TASK_ID.equals(beforTaskId) &&
				(!processInstId.equals(variableMap.get(Constants.ROOT_PROCESS_ID)))) {
			taskflow.setSuperTaskId((String) variableMap.get(Constants.PROCESS_TASK_ID));
		}else {
			taskflow.setSuperTaskId(beforTaskId);
		}
		//检查流程是否支持检出
		List<WfModelPropExtends> processPropList = wfModelPropExtendsService.selectModelExtendsPropByOwnId(taskflow.getTaskCode());
		WfModelPropExtends  checkInOut = modelPropService.getSpcefiyModelProp(processPropList,ModelProperties.checkInOut.getName());
		if(checkInOut!=null)
		{
			//检查是否有最大的检出数限制
			taskflow.setCheckInFlag(Constants.TRAN_TYPE_UNDO_CHECKOUT);
			taskflow.setTaskPoolFlag(Constants.IS_TASK_POOL);
		}


		if (variableMap != null) {
			variableMap.put(Constants.SPECIFYNEXTNODE, null);
			taskflow.setBussinessMap(BpmStringUtil.mapToJson(variableMap).toString());
		}
		clearPreSelectPersion(variableMap, flowElement);

		// 待保存的关键业务参
		if (variableMap.containsKey(Constants.BUSINESS_SAVE)&&variableMap.get(Constants.BUSINESS_SAVE)!=null) {
			Map<String, Object> queryCondition = BpmStringUtil.jsonToMap(variableMap.get(Constants.BUSINESS_SAVE));
			if (queryCondition.containsKey(Constants.QUERY_INDEX_1)) {
				taskflow.setQueryIndexa((String) queryCondition.get(Constants.QUERY_INDEX_1));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_2)) {
				taskflow.setQueryIndexb((String) queryCondition.get(Constants.QUERY_INDEX_2));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_3)) {
				taskflow.setQueryIndexc((String) queryCondition.get(Constants.QUERY_INDEX_3));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_4)) {
				taskflow.setQueryIndexd((String) queryCondition.get(Constants.QUERY_INDEX_4));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_5)) {
				taskflow.setQueryIndexe((String) queryCondition.get(Constants.QUERY_INDEX_5));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_6)) {
				taskflow.setQueryIndexf((String) queryCondition.get(Constants.QUERY_INDEX_6));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_7)) {
				taskflow.setQueryIndexg((String) queryCondition.get(Constants.QUERY_INDEX_7));
			}
			if (queryCondition.containsKey(Constants.QUERY_INDEX_8)) {
				taskflow.setQueryIndexh((String) queryCondition.get(Constants.QUERY_INDEX_8));
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("| - TaskCreateListener>>>>>创建流程实例savewfTaskflow[processId:{},taskId:{}]",taskflow.getProcessId(),taskflow.getTaskId());
		}
		if (wfInstance == null) {
			wfInstance = wfInstanceService.selectByProcessId(processInstId);
		}

		Object systemcodeobj = variableMap.get(Constants.SYSTEM_CODE_KEY);
		if (systemcodeobj != null) {
			taskflow.setSystemCode(systemcodeobj.toString());
		}
		//2019-09-12 amt modify start 增加workTaskDesc描述字段
//		if(variableMap.containsKey(Constants.WORK_TASK_DESC_KEY)) {
//			taskflow.setWfDesc((String) variableMap.get(Constants.WORK_TASK_DESC_KEY));
//		}
		if (wfInstance != null) {

			if(StrUtil.isBlank(taskflow.getWfDesc())) {
				taskflow.setWfDesc(wfInstance.getProcessInstDesc());
			}
			//2019-09-12 amt modify end 增加workTaskDesc描述字段
			if(Objects.isNull(taskflow.getSystemCode()))
			{
				taskflow.setSystemCode(wfInstance.getSystemCode());
			}
			taskflow.setWorkCreater(wfInstance.getStartUserId());
			JSONObject startUserMap = JSONObject.parseObject(wfInstance.getStartUserMap());
			if (startUserMap != null) {
				taskflow.setWorkCreaterName((String) startUserMap.get(Constants.USERNAME_KEY));
			}
			taskflow.setWorkCreaterAgencyId(wfInstance.getOrgId());
			taskflow.setWorkCreaterAgencyName(wfInstance.getOrgName());
			taskflow.setProcessName(wfInstance.getProccessName());
			taskflow.setStartCreateDate(wfInstance.getCreateTime());
		} else {
			Object  processCreateTime=variableMap.get(Constants.PROCESS_CREATE_TIME);
			if (processCreateTime != null) {
				taskflow.setStartCreateDate(DateUtil.convertObjToLdt(processCreateTime));
			}

			Object userIdObj = variableMap.get(Constants.BEFOR_USERID);
			if (userIdObj != null) {
				taskflow.setWorkCreater(userIdObj.toString());
			}
		}
		String systemCode = taskflow.getSystemCode();
		taskflow.setItemFirstCode(systemCode.split(";")[0].split(":")[0]);
		taskflow.setItemSecondCode(systemCode.split(";")[1].split(":")[0]);
		taskflow.setItemSecondName(systemCode.split(";")[1].split(":")[1]);


		//处理附件功能
		commService.processAccessories(new WorkFlowTaskTransferData(), null);

		return taskflow;

	}



}
