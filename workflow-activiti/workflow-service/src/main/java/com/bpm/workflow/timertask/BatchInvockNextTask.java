package com.bpm.workflow.timertask;

import com.bpm.workflow.dto.transfer.WorkFlowTaskTransferData;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.SpringUtil;
import com.bpm.workflow.util.ThreadUtil;
import com.bpm.workflow.util.WfVariableUtil;
import lombok.extern.log4j.Log4j2;
import java.util.List;
import java.util.Map;

/**
 * @Description: 批量处理自动分配
 */
@Log4j2
public class BatchInvockNextTask extends WorkflowTimerTask {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    private WorkFlowTaskTransferData taskTransferData;

    private List<WfTaskflow> taskflowList;

    public BatchInvockNextTask(WorkFlowTaskTransferData workFlowStartData, List<WfTaskflow> taskflowList) {
        this.taskTransferData = workFlowStartData;
        this.taskflowList = taskflowList;
    }

    @Override
    public void runImpl() throws Exception {
        log.info(" | - BatchInvockNextTask>>>>>开始调用自动通过{} ", taskflowList.size());
        try {
            ProcessCoreService processCoreService = SpringUtil.getBean(ProcessCoreService.class);
            for (WfTaskflow w : taskflowList) {
                WorkFlowTaskTransferData data = new WorkFlowTaskTransferData();
                data.setSysCommHead(taskTransferData.getSysCommHead());
                data.setUserId(taskTransferData.getUserId());
                data.setTaskId(w.getTaskId());
                Map<String, Object> map = BpmStringUtil.jsonToMap(w.getBussinessMap());
                data.setBussinessMap(map);
                processCoreService.nextTask(data);
            }
        } catch (Exception e) {
            log.error("| - BatchInvockNextTask>>>>>批量执行自动分配异常", e);
        } finally {
            ThreadUtil.clearAutoTaskId(Thread.currentThread().getId() + "");
            WfVariableUtil.clearfVariable();
        }


    }

}
