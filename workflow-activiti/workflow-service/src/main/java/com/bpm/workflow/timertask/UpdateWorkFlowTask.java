package com.bpm.workflow.timertask;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.mapper.WfTaskflowMapper;
import com.bpm.workflow.service.WfTaskflowService;
import com.bpm.workflow.util.SpringUtil;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class UpdateWorkFlowTask extends WorkflowTimerTask {
    private static final long serialVersionUID = 1L;
    @Resource
    private WfTaskflowService wfTaskflowService;

    public UpdateWorkFlowTask(WfTaskflow taskFlow) {
        this.taskFlow = taskFlow;
    }

    public UpdateWorkFlowTask() {
        super();
        // TODO Auto-generated constructor stub
    }

    private WfTaskflow taskFlow;

    @Override
    public void runImpl() throws Exception {
        log.info("| - UpdateWorkFlowTask>>>>>开始更新[taskId={}]节点执行信息", taskFlow.toString());
        WfTaskflowMapper wfTaskflowMapper = SpringUtil.getBean(WfTaskflowMapper.class);//.getBean("wfTaskflowMapper");
        WfTaskflow taskFlow1 = wfTaskflowService.selectByTaskId(taskFlow.getTaskId());
        //超时以后，霸屏以前，如果已经处理则不做处理
        if (taskFlow1 == null || taskFlow1.getWorkState().equals(Constants.TASK_STATE_END)) {
            return;
        }
        wfTaskflowMapper.updateById(taskFlow);
        log.info("| - UpdateWorkFlowTask>>>>>更新[taskId={}]保存节点执行信息", taskFlow.toString());
    }

}
