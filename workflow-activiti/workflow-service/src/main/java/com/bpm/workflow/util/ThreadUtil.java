package com.bpm.workflow.util;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.NumberConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Log4j2
@Component
public class ThreadUtil {

	@Resource
	private ConfigUtil configUtil = null;

	private ExecutorService executorService = null;

	private synchronized ExecutorService getExecutorService() {
		if (executorService == null) {
			executorService = Executors.newFixedThreadPool(Integer.parseInt(configUtil.getThreadpoolSize()));
		}
		return executorService;
	}

	public void execute(Runnable taskRunner) {
		getExecutorService().execute(taskRunner);
	}

	private static Map<String, Long> syncFlagExpiredTime = null;
	private static Map<String, String> syncFlag = null;
	private static ReentrantLock syncFlagLock = new ReentrantLock();
	
	
	private static long lastCleanup = -1;
	
	private static Map<String,String>  tempAutoTaskIdMap= null;
	private static Object synLockObject = new Object();

	private static synchronized void doCleanSyncFlag() {
		if (syncFlag == null) {
			syncFlagExpiredTime = null;
			return;
		}
		if (syncFlagExpiredTime == null) {
			syncFlag = null;
			return;
		}
		if(syncFlag.size() < NumberConstants.INT_20000) {
			return;
		}
		long now = System.currentTimeMillis();
		if(TaskTimoutUtil.isDebug) {
			if(now-lastCleanup > NumberConstants.INT_300) {
				lastCleanup = now;
			}else {
				return;
			}
		}else {
			if(now-lastCleanup > NumberConstants.INT_10000) {
				lastCleanup = now;
			}else {
				return;
			}
		}

		
		Set<String> toBeRemoved = new HashSet<>();
		for (Map.Entry<String, Long> anEntry : syncFlagExpiredTime.entrySet()) {
//			log.info("---doCleanSyncFlag anEntry.getValue().longValue() :"+anEntry.getValue().longValue() +" now:"+now);
			if (anEntry.getValue() < now) {
				toBeRemoved.add(anEntry.getKey());
			}
		}
		for (String key : toBeRemoved) {
			syncFlagExpiredTime.remove(key);
			syncFlag.remove(key);
//			log.info("remove:"+key);
		}
	}
	protected static int getSyncFlagSize() {
		int result = -1;
		ReentrantLock lock = syncFlagLock;//getSyncFlagLock();
		lock.lock();
		try {
			if(syncFlag==null) {
				result = 0;
			}else {
				result = syncFlag.size();
			}
			
		} catch (Exception e) {
			log.error(BpmStringUtil.getBusinessErrorCode(e));
		} finally {
			lock.unlock();
		}
		return result;
	}

	public static synchronized void setSyncFlag(String key, String flag, long expired) {
		if (key == null || flag == null || expired < 0) {
			throw new IllegalArgumentException("wrong argument:" + key + " " + flag + " " + expired);
		}
		ReentrantLock lock =  syncFlagLock;//getSyncFlagLock();
		lock.lock();
		try {
			if (syncFlag == null) {
				syncFlag = new HashMap<String, String>();
			}
			if (syncFlagExpiredTime == null) {
				syncFlagExpiredTime = new HashMap<String, Long>();
			}
			syncFlag.put(key, flag);
			long now = System.currentTimeMillis();
			syncFlagExpiredTime.put(key, now + expired);
//			log.info("set put time:"+syncFlagExpiredTime+" now:"+now);
			doCleanSyncFlag();
		} catch (Exception e) {
			log.error(BpmStringUtil.getBusinessErrorCode(e));
		} finally {
			lock.unlock();
		}
	}

	public static void setSyncFlag(String key, String flag) {
		setSyncFlag(key, flag, 300000);
	}

	public static synchronized int checkSyncFlag(String key, String readyValue, long timeout) {
		if (key == null || readyValue == null || timeout < 0) {
			throw new IllegalArgumentException("wrong argument:" + key + " " + readyValue + " " + timeout);
		}
		int result = Constants.syncFlag_check_timeout;

		log.warn("| - ThreadUtil>>>>>#####Wait Main Thread Commit:{}" , key );
		if (syncFlag == null) {
			syncFlag = new HashMap<>();
		}
		long timeoutTimeStamp = System.currentTimeMillis() + timeout;
		ReentrantLock theLock =  syncFlagLock;
		while (System.currentTimeMillis() < timeoutTimeStamp) {

			log.info("------checkSyncFlag 加锁");
			theLock.lock();
			try {
				String theValue = null;
				theValue = syncFlag.get(key);
				if (readyValue.equals(theValue)) {
					result = Constants.syncFlag_check_ready;
					syncFlag.remove(key);
				}
			} catch (Exception e) {
				log.error(BpmStringUtil.getBusinessErrorCode(e));
			} finally {
				theLock.unlock();
			}
			if (result == Constants.syncFlag_check_ready) {
				break;
			}
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				log.error("| - ThreadUtil>>>>>checkSyncFlag休眠100毫秒失败",e);
			}
		}

		log.warn("| - ThreadUtil>>>>>#####Recive  Main Thread Commit OK:{},result:{}" , key, result );
		return result;
	}
	
	
	public static void addAutoTaskId(String taskId, String threadId) {
		synchronized (synLockObject) {
			if (tempAutoTaskIdMap == null) {
				tempAutoTaskIdMap = new HashMap<String, String>();
			}
			tempAutoTaskIdMap.put(taskId, threadId);
		}
	}
	
	
	public static void removeAutoTaskId(String threadId) {
		synchronized (synLockObject) {
			if (tempAutoTaskIdMap == null) {
				tempAutoTaskIdMap = new HashMap<String, String>();
			}
			Set<String> tempSet = tempAutoTaskIdMap.keySet();
			Set<String> delTaskSet = new HashSet<String>();
			for (String taskId : tempSet) {
				if (threadId.equals(tempAutoTaskIdMap.get(taskId))) {
					log.warn("| - ThreadUtil>>>>>Main Thread Send Sign to Auto taskId:{},Thread Id:{}" ,taskId, threadId);
					setSyncFlag(taskId, threadId);
					delTaskSet.add(taskId);
				}
			}
			for (String taskId : delTaskSet) {
				
				tempAutoTaskIdMap.remove(taskId);
			}
		}
		log.warn("| - ThreadUtil>>>>>Main Thread Send Sign to Auto:{}" , threadId);
	}
	
	//20190613 wanglh add 添加直接清理tempAutoTaskIdMap 方法,拷贝自removeAutoTaskId
	public static void clearAutoTaskId(String threadId) 
	{
		synchronized (synLockObject) {
			if (tempAutoTaskIdMap == null) {
				tempAutoTaskIdMap = new HashMap<String, String>();
			}
			Set<String> tempSet = tempAutoTaskIdMap.keySet();
			Set<String> delTaskSet = new HashSet<String>();
			for (String taskId : tempSet) {
				if (threadId.equals(tempAutoTaskIdMap.get(taskId))) {
					delTaskSet.add(taskId);
				}
			}
			for (String taskId : delTaskSet) {
				tempAutoTaskIdMap.remove(taskId);
			}
		}
	}}
