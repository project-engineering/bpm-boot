package com.bpm.workflow.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfParam;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfParamMapper;
import com.bpm.workflow.service.WfParamChannelService;
import com.bpm.workflow.service.WfParamService;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.CacheUtil;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Log4j2
@Service
public class WfParamServiceImpl extends ServiceImpl<WfParamMapper, WfParam> implements WfParamService {
    @Resource
    private WfParamMapper wfParamMapper;
    @Resource
    private CacheUtil cacheUtil;
    @Resource
    private WfParamChannelService wfParamChannelService;

    @Transactional
    public int updateById(WfParam param, List<String> itemChannelTypeList) {
        String id = param.getId();
        String type = param.getParamType();
        String paramCode = param.getParamCode();


        WfParam target = viewParam(id);
        if (target == null) {
            throw new ServiceException("需要修改的记录不存在，请查看Id");
        }
        if (StrUtil.isBlank(type)) {
            type = target.getParamType();
        } else if (!type.equals(target.getParamType())) {
            throw new ServiceException("输入参入id和类型不匹配！");
        }

        if (Constants.PARAM_LARGE.equals(type)) {
            duplicateRecordCheck(type, paramCode, null, id, "事项大类编码[" + paramCode + "]已经存在！");

            param.setParamOrder(target.getParamOrder());

        } else if (Constants.PARAM_SMALL.equals(type)) {
            // 事项渠道类型为空校验
            if (ObjectUtil.isEmpty(itemChannelTypeList)) {
                throw new ServiceException(ErrorCode.WF000032);
            }

            duplicateRecordCheck(type, paramCode, param.getParamObjCode(), id, "事项小类编码[" + paramCode + "]已经存在！");

            // 检查所属事项大类的存在性
            WfParam paramQuery = new WfParam();
            paramQuery.setParamType(Constants.PARAM_LARGE);
            paramQuery.setParamCode(param.getParamObjCode());
            paramQuery.setId(param.getUpid());
            List<WfParam> parentList = selectParamList(paramQuery);
            if (parentList == null || parentList.isEmpty()) {
                throw new ServiceException("事项小类所属的大类不存在，请检查！");
            }

            // 更新事项小类和流程编码的关系信息:先删除 后增加
            WfParam selectParam = new WfParam();
            selectParam.setParamCode(target.getParamCode());
            selectParam.setParamType(Constants.PARAM_PROCESS_CODE);
            List<WfParam> childParams = selectParamList(selectParam);

            if (childParams != null) {
                for (WfParam tempWfParam : childParams) {
                    deleteById(tempWfParam.getId());
                }
            }

            WfParam newParam = new WfParam();
            newParam.setId(IdUtil.simpleUUID());
            newParam.setUpid(target.getId());
            newParam.setParamCode(param.getParamCode());
            newParam.setParamName(param.getParamName());
            newParam.setParamType(Constants.PARAM_PROCESS_CODE);
            newParam.setParamObjCode(param.getProcessCode());
            newParam.setParamObjName(param.getProcessName());
            insertWfParam(newParam, null);

            //处理原数据的信息
            param.setParamOrder(target.getParamOrder());
            // 删除原先事项小类渠道配置
            wfParamChannelService.deleteByItemName(param.getParamCode());
            // 将事项小类渠道配置插入表中
            wfParamChannelService.saveBatch(param.getParamCode(), itemChannelTypeList);
        } else if (Constants.PARAM_BUSINESS.equals(type)) {
            // 检查业务比较参数的重复性
            duplicateRecordCheck(type, param.getProcessCode(), param.getParamObjCode(), null, "该流程编码对应的业务参数编码已经存在！");

            // param.getParamObjType()//0-字符,1-整形,2-浮点,3-金额
            param.setUpid(Constants.PARAM_UPID);
            param.setParamName(param.getProcessName());
            param.setParamCode(param.getProcessCode());

        } // 更换处理处理人流程配置
        else if (Constants.PARAM_MODIFY_USER.equals(type)) {
            // 重复项检查
            duplicateRecordCheck(type, paramCode, null, id, "该流程编码对应的处理配置已经存在！");

        }
        // 渠道映射类型
        else if (Constants.PARAM_CHANNEL_MAP.equals(type)) {
            // 渠道映射重复性检查
            duplicateRecordCheck(type, paramCode, null, id, "该渠道对应的映射配置已经存在！");
        }
        // 全局事件配置
        else if (Constants.PARAM_GLOBALEVENT.equals(type)) {
            // 检查全局事件的重复性
            duplicateRecordCheck(type, paramCode, null, id, "该渠道对应的映射配置已经存在！");

        } else {
            throw new ServiceException("上送的参数类型[" + param.getParamType() + "]不再操作类型范围内，请检查！");
        }

        checkDataProp(param);
        int result = wfParamMapper.updateById(param);


        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).refreshByKey(CacheUtil.SvcType.wfParam, null, new String[]{param.getParamType(), param.getParamCode()}, true);
        return result;
    }

    @Transactional
    public void insertWfParam(WfParam param, List<String> itemChannelTypeList) {
        String type = param.getParamType();
        String paramCode = param.getParamCode();

        param.setId(IdUtil.simpleUUID());

        // 事项大类
        if (Constants.PARAM_LARGE.equals(type)) {
            // 检查事项大类的重复性
            duplicateRecordCheck(type, paramCode, null, null, "事项大类编码[" + paramCode + "]已经存在！");

            // 查询所有的事项大类记录并设置排序值
            int index = getParamOrderMaxIndex(param.getParamType());
            param.setParamOrder(BigInteger.valueOf((index + 1)));

        }
        // 事项小类
        else if (Constants.PARAM_SMALL.equals(type)) {
            // 事项渠道类型为空校验
            if (ObjectUtil.isEmpty(itemChannelTypeList)) {
                throw new ServiceException(ErrorCode.WF000032);
            }
            // 检查事项小类的重复性
            duplicateRecordCheck(type, paramCode, param.getParamObjCode(), null, "事项小类编码[" + paramCode + "]已经存在！");
            // 检查所属事项大类的存在性
            WfParam paramQuery = new WfParam();
            paramQuery.setParamType(Constants.PARAM_LARGE);
            paramQuery.setParamCode(param.getParamObjCode());
            paramQuery.setId(param.getUpid());
            List<WfParam> parentList = selectParamList(paramQuery);
            if (CollectionUtils.isEmpty(parentList)) {
                throw new ServiceException("事项小类所属的大类不存在，请检查！");
            }
            // 查询所有的事项小类记录并设置排序值
            int index = getParamOrderMaxIndex(param.getParamType());
            param.setParamOrder(BigInteger.valueOf((index + 1)));
            // 将事项小类和流程编码的关系在插入表中
            WfParam codeParam = new WfParam();
            codeParam.setUpid(param.getId());
            codeParam.setParamCode(param.getParamCode());
            codeParam.setParamName(param.getParamName());
            codeParam.setParamObjCode(param.getProcessCode());
            codeParam.setParamObjName(param.getProcessName());
            codeParam.setParamType(Constants.PARAM_PROCESS_CODE);
            insertWfParam(codeParam, null);
            // 将事项小类和事项大类的关系先插入表中
            param.setUpid(parentList.get(0).getId());
            // 删除原先事项小类渠道配置
            wfParamChannelService.deleteByItemName(param.getParamCode());
            // 将事项小类渠道配置插入表中
            wfParamChannelService.saveBatch(param.getParamCode(), itemChannelTypeList);
        } else if (Constants.PARAM_PROCESS_CODE.equals(type)) {
            //小类和流程编码关系
        }
        // 业务数据比较参数
        else if (Constants.PARAM_BUSINESS.equals(type)) {
            //检查业务比较参数的重复性
            duplicateRecordCheck(type, param.getProcessCode(), param.getParamObjCode(), null, "该流程编码对应的业务参数编码已经存在！");

            // param.getParamObjType()//0-字符,1-整形,2-浮点,3-金额
            param.setUpid(Constants.PARAM_UPID);
            param.setParamName(param.getProcessName());
            param.setParamCode(param.getProcessCode());

        }
        // 更换处理处理人流程配置
        else if (Constants.PARAM_MODIFY_USER.equals(type)) {
            // 重复项检查
            duplicateRecordCheck(type, paramCode, null, null, "该流程编码对应的处理配置已经存在！");

        }
        // 渠道映射类型
        else if (Constants.PARAM_CHANNEL_MAP.equals(type)) {
            // 渠道映射重复性检查
            duplicateRecordCheck(type, paramCode, null, null, "该渠道对应的映射配置已经存在！");
        }
        // 全局事件配置
        else if (Constants.PARAM_GLOBALEVENT.equals(type)) {
            // 检查全局事件的重复性
            duplicateRecordCheck(type, paramCode, null, null, "该渠道对应的映射配置已经存在！");

        } else {
            throw new ServiceException("上送的参数类型[" + param.getParamType() + "]不再操作类型范围内，请检查！");
        }

        // 最后将数据项插入到表中
        checkDataProp(param);

        wfParamMapper.insert(param);
        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).refreshByKey(CacheUtil.SvcType.wfParam, null, new String[]{param.getParamType(), param.getParamCode()}, true);

    }


    /**
     * 查询所有信息记录
     *
     * @param param
     * @param pageData
     * @return
     * @
     */
    public List<WfParam> selectParamList(Map<String, Object> param, PageData<WfParam> pageData) {
        // 事项小类，追加流程编码
        if (Constants.PARAM_SMALL.equals(param.get(BpmConstants.PARAM_TYPE))) {
            List<WfParam> wfParamList = pageData.getResult();
            List<WfParam> childList = selectParamByParamType(Constants.PARAM_PROCESS_CODE);
            for (WfParam child : wfParamList) {
                if (StrUtil.isNotEmpty(child.getItemChannelType())) {
                    String itemChannelType = child.getItemChannelType();
                    String[] itemChannelTypeArray = itemChannelType.split(",");
                    child.setItemChannelTypeList(Arrays.asList(itemChannelTypeArray));
                }
                for (WfParam childParam : childList) {
                    if (childParam.getParamCode().equals(child.getParamCode())) {
                        child.setProcessCode(childParam.getParamObjCode());
                        child.setProcessName(childParam.getParamObjName());
                    }
                }
            }
        }

        return pageData.getResult();
    }


    public WfParam viewParam(String id) {
        return wfParamMapper.selectById(id);
    }


    /**
     * 基本数据检查
     *
     * @
     */
    private void checkDataProp(WfParam theArg) {
        if (StrUtil.isBlank(theArg.getParamCode())) {
            throw new ServiceException("参数编码不能为空！");
        }
        if (StrUtil.isBlank(theArg.getParamName())) {
            throw new ServiceException("参数编码名称不能为空！");
        }
        if (StrUtil.isBlank(theArg.getParamType())) {
            throw new ServiceException("参数类型不能为空！");
        }
    }


    @Transactional
    public int deleteById(String id) {

        // 可以检查记录在不在
        WfParam delParam = viewParam(id);
        if (delParam == null) {
            throw new ServiceException("id指定的记录不存在，请查看");
        }

        // 删除相关的附带记录
        String delParamType = delParam.getParamType();
        if (Constants.PARAM_LARGE.equals(delParamType)) {

            // 种子数据不能删除
            String paramObjType = delParam.getParamObjType();
            if (BpmConstants.PARAM_OBJ_TYPE_1.equals(paramObjType) || BpmConstants.PARAM_OBJ_TYPE_2.equals(paramObjType)) {
                throw new ServiceException("种子数据不允许删除，请找管理员！");
            }

            WfParam paramQuery = new WfParam();
            paramQuery.setUpid(delParam.getId());
            paramQuery.setParamType(Constants.PARAM_SMALL);
            paramQuery.setParamObjCode(delParam.getParamCode());
            List<WfParam> childList = selectParamList(paramQuery);
            if (childList.size() > 0) {
                throw new ServiceException("事项大类下有事项小类数据，无法删除！");
            }
        }
        if (Constants.PARAM_SMALL.equals(delParamType)) {
            // 删除存量流程编码数据
            WfParam paramQuery = new WfParam();
            paramQuery.setParamType(Constants.PARAM_PROCESS_CODE);
            paramQuery.setUpid(delParam.getId());
            paramQuery.setParamCode(delParam.getParamCode());
            List<WfParam> childList = selectParamList(paramQuery);
            if (childList.size() > 0) {
                for (WfParam tempParam : childList) {
                    deleteById(tempParam.getId());
                }
            }
            // 删除原先事项小类渠道配置
            wfParamChannelService.deleteByItemName(delParam.getParamCode());
        }

        cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).refreshByKey(CacheUtil.SvcType.wfParam, null,
                new String[]{delParam.getParamType(), delParam.getParamCode()}, true);
        return wfParamMapper.deleteById(id);
    }

    @Override
    public void insertWfParam(WfParam wfParam) {
        wfParamMapper.insert(wfParam);
    }

    public List<WfParam> selectAllList() {
        return wfParamMapper.selectParamList(new WfParam());
    }

    @Override
    public List<WfParam> selectParamList(WfParam wfParam) {
        return wfParamMapper.selectParamList(wfParam);
    }

    @Transactional
    public int deleteByUpid(String upid) {
        WfParam wfParamQuery = new WfParam();
        wfParamQuery.setUpid(upid);
        List<WfParam> tobeDeletes = wfParamMapper.selectParamList(wfParamQuery);
        if (tobeDeletes != null && tobeDeletes.size() > 0) {
            for (WfParam tobeDelete : tobeDeletes) {
                cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).refreshByKey(CacheUtil.SvcType.wfParam, null, new String[]{tobeDelete.getParamType(), tobeDelete.getParamCode()}, true);
            }
        }
        LambdaQueryWrapper<WfParam> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfParam::getUpid,upid);
        return wfParamMapper.delete(queryWrapper);
    }


    @SuppressWarnings("unchecked")
    public List<WfParam> selectParamByParamType(String paramType) {
        return cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).getCacheListValueByKey("selectParamByParamType", paramType, null);
    }

    @SuppressWarnings("unchecked")
    public List<WfParam> selectParamByParamTypeParamCode(String paramType, String paramCode) {
        return cacheUtil.getCacheActionBase(CacheUtil.SvcType.wfParam).getCacheListValueByKey("selectParamByParamTypeParamCode", paramType, paramCode);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean canExecTimeoutTask(long delaySecond) {
        try {
            long alowSecond = delaySecond * 2 / 3;

            String mask = "yyyyMMddHHmmss";

            WfParam wfParam = wfParamMapper.selectLastExecTimeout();
            if (wfParam == null || StrUtil.isEmpty(wfParam.getId()) || StrUtil.isEmpty(wfParam.getParamObjCode())) {
                wfParam = new WfParam();
                wfParam.setId(Constants.PARAM_TIMEOUT_LASTEXEC_ID);
                wfParam.setUpid("979998");
                wfParam.setParamName("lastexec_param");
                wfParam.setParamType(Constants.PARAM_TIMEOUT_LASTEXEC_PARATYPE);
                wfParam.setParamCode("lastexec_param");
                wfParam.setParamObjType("0");
                wfParam.setParamObjCode((new SimpleDateFormat(mask)).format(Calendar.getInstance().getTime()));
                wfParam.setParamObjName("lastexec_param");
                wfParamMapper.insert(wfParam);
                return true;
            } else {
                Date lastExec = (new SimpleDateFormat(mask)).parse(wfParam.getParamObjCode());
                Calendar cal = Calendar.getInstance();
                cal.setTime(lastExec);
                if ((System.currentTimeMillis() - cal.getTimeInMillis()) > (alowSecond * NumberConstants.INT_1000)) {
                    wfParam.setParamObjCode((new SimpleDateFormat(mask)).format(Calendar.getInstance().getTime()));
                    wfParamMapper.updateById(wfParam);
                    return true;
                } else {
                    return false;
                }
            }

        } catch (Exception e) {
            log.error(BpmStringUtil.getExceptionStack(e));
            return false;
        }
    }

    /**
     * 对数据记录进行排序
     */
    @Transactional
    public void orderWfParam(String ids) {
        if (StrUtil.isBlank(ids)) {
            throw new ServiceException("传入的参数不对，必须传入ID.");
        }
        String[] idArry = ids.split(",");
        if (idArry.length != NumberConstants.INT_TWO) {
            throw new ServiceException("传入的参数不对，必须传入两个ID.");
        }
        WfParam pOne = wfParamMapper.selectById(idArry[0]);
        WfParam pTwo = wfParamMapper.selectById(idArry[1]);

        if (pOne == null) {
            throw new ServiceException("传入的参数不对，指定id" + idArry[0] + "不存在！");
        }
        if (pTwo == null) {
            throw new ServiceException("传入的参数不对，指定id" + idArry[1] + "不存在！");
        }

        if (!pOne.getParamType().equals(pTwo.getParamType())) {
            throw new ServiceException("传入的参数为不同类型，无法排序！");
        }

        // 调换顺序
        BigInteger order = pOne.getParamOrder();
        pOne.setParamOrder(pTwo.getParamOrder());
        pTwo.setParamOrder(order);

        // 更新数据
        wfParamMapper.updateById(pOne);
        wfParamMapper.updateById(pTwo);
    }

    // 获取更换处理人流程列表
    public List<String> getModifyUserList() {
        WfParam paramQuery = new WfParam();
        paramQuery.setParamType(Constants.PARAM_MODIFY_USER);
        List<WfParam> selectList = selectParamList(paramQuery);
        List<String> resultList = new ArrayList<String>();
        for (WfParam wfp : selectList) {
            resultList.add(wfp.getParamCode());
        }
        return resultList;
    }
    // 获取更换处理人流程列表

    // 根据流程编码获取大小类配置

    /**
     * 根据流程编码返回对应的大小类
     *
     * @return List 大小必须为2是才正确，其他情况均为存在问题
     * List[0] 为大类   List[1]为小类
     * @
     */
    public List<WfParam> selectPramByProcessCode(String processCode) {
        WfParam paramQuery = new WfParam();
        paramQuery.setParamObjCode(processCode);
        paramQuery.setParamType("F0");
        List<WfParam> xlParam = null;
        List<WfParam> result = new ArrayList<WfParam>();
        try {
            xlParam = selectParamList(paramQuery);
        } catch (Exception e) {
            log.error("根据流程编码[{}]返回对应的小类失败,原因：{}",processCode,e);
            return result;
        }
        if (xlParam != null && xlParam.size() > 0) {
            String xlCode = xlParam.get(0).getParamCode();
            List<WfParam> dlParam = selectParamByParamTypeParamCode(Constants.PARAM_TYPE_L, xlCode);
            result.add(dlParam.get(0));
            result.add(xlParam.get(0));
        }
        return result;
    }
    // 根据流程编码获取大小类配置


    /**
     * 获取相应类型的排序的最大值
     *
     * @param paramType
     * @return
     * @
     */
    private int getParamOrderMaxIndex(String paramType) {
        WfParam selectCond = new WfParam();
        selectCond.setParamType(paramType);
        List<WfParam> allListForType = selectParamList(selectCond);

        int result = 0;
        for (WfParam temp : allListForType) {
            if (temp.getParamOrder().intValue() > result) {
                result = temp.getParamOrder().intValue();
            }
        }
        return result;
    }

    /**
     * 重复项检查处理
     *
     * @param type
     * @param paramCode
     * @param paramObjCode
     * @param id
     * @param errmsg
     * @return
     * @
     */
    private List<WfParam> duplicateRecordCheck(String type, String paramCode, String paramObjCode, String id, String errmsg) {
        WfParam paramQuery = new WfParam();
        paramQuery.setParamType(type);
        paramQuery.setParamCode(paramCode);
        paramQuery.setParamObjCode(paramObjCode);
        List<WfParam> childList = selectParamList(paramQuery);
        if (StrUtil.isBlank(id)) {
            if (childList.size() > 0) {
                throw new ServiceException(errmsg);
            }
        } else {
            for (WfParam temp : childList) {
                if (temp.getId().equals(id)) {
                    continue;
                }
                throw new ServiceException(errmsg);
            }
        }
        return childList;
    }
}
