package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfSystemStructure;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfSystemStructureService extends IService<WfSystemStructure> {

    WfSystemStructure selectByKeyId(int i);
}
