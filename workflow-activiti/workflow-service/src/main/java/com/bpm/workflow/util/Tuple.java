package com.bpm.workflow.util;

public class Tuple<T> {
	T first;
	T second;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("------------").append("Tuple").append("------------").append(System.lineSeparator());
		sb.append("first:").append(first).append("  second:").append(second);
		return sb.toString();
	}
}
