package com.bpm.workflow.util.cacheimpl;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.util.CacheUtil;
import org.activiti.bpmn.model.FlowElement;
import org.dom4j.Element;
import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class CacheActionBase<T> {
	
    @Resource
    protected CacheUtil cacheUtil;
     
    
	public static final String NULL_KEY_VALUE = "can't be any key here*&%$#";
    public static final String SEPERATOR = "&_*";
    
    
    
    private Map<String, List<T>> listValues = new ConcurrentHashMap<>();
    private Map<String, T> values = new ConcurrentHashMap<>();
	protected static Map<String, Element> taskElementByDefIdActVersionTaskDefId = new ConcurrentHashMap<>();
	protected static Map<String,Element> taskElementByDefIdActivitiVersionTaskDefId = new ConcurrentHashMap<>();
	protected static Map<String,FlowElement> flowElementByProcessDefIdTaskDefId = new ConcurrentHashMap<>();
   

	private int getKeysCnt(String[] keys) {
		for(int i=1; i <= keys.length; i++) {
			if(keys[i-1] == null) {
				return i;
			}
		}
		return keys.length;
	}
	
	public void clearAll() {
		if(taskElementByDefIdActVersionTaskDefId!=null && taskElementByDefIdActVersionTaskDefId.size()>0) {
			taskElementByDefIdActVersionTaskDefId.clear();
		}
		
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			if(listValues!=null && listValues.size()>0) {
				listValues.clear();
			}
			if(values!=null && values.size()>0) {
				values.clear();
			}

			if(flowElementByProcessDefIdTaskDefId!=null && flowElementByProcessDefIdTaskDefId.size()>0) {
				flowElementByProcessDefIdTaskDefId.clear();
			}
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			cacheUtil.getRedisCacheUtil().removeAll(FLOW_ELEMENT_BY_PROCESS_DEF_ID_TASK_DEF_ID_KEY);
			cacheUtil.getRedisCacheUtil().removeAll(TASK_ELEMENT_BY_DEF_ID_ACT_VERSION_TASK_DEF_ID_KEY);
			cacheUtil.getRedisCacheUtil().removeAll(LIST_VALUES_KEY);
			cacheUtil.getRedisCacheUtil().removeAll(VALUES_KEY);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			cacheUtil.getEhCacheUtil().removeAll(FLOW_ELEMENT_BY_PROCESS_DEF_ID_TASK_DEF_ID_KEY);
			cacheUtil.getEhCacheUtil().removeAll(TASK_ELEMENT_BY_DEF_ID_ACT_VERSION_TASK_DEF_ID_KEY);
			cacheUtil.getEhCacheUtil().removeAll(LIST_VALUES_KEY);
			cacheUtil.getEhCacheUtil().removeAll(VALUES_KEY);
		}

	}
    private void refreshValueByKeyGroup(String keyGroupName,String[] keys) {
    	if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
    		refreshValueByCalcKey(getKey(keyGroupName,keys,1));	
    	}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
    		refreshValueRedisByCalcKey(getKey(keyGroupName,keys,1));	
    	}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
    		refreshValueEncacheByCalcKey(getKey(keyGroupName,keys,1));	
    	}
	}
    private void refreshValueRedisByCalcKey(String caculateKey) {
    	cacheUtil.getRedisCacheUtil().removeAll(FLOW_ELEMENT_BY_PROCESS_DEF_ID_TASK_DEF_ID_KEY + caculateKey);
    	cacheUtil.getRedisCacheUtil().removeAll(TASK_ELEMENT_BY_DEF_ID_ACT_VERSION_TASK_DEF_ID_KEY + caculateKey);
    	cacheUtil.getRedisCacheUtil().removeAll(LIST_VALUES_KEY + caculateKey);
    	cacheUtil.getRedisCacheUtil().removeAll(VALUES_KEY + caculateKey);
    }
    private void refreshValueEncacheByCalcKey(String caculateKey) {
    	cacheUtil.getEhCacheUtil().removeAll(FLOW_ELEMENT_BY_PROCESS_DEF_ID_TASK_DEF_ID_KEY + caculateKey);
    	cacheUtil.getEhCacheUtil().removeAll(TASK_ELEMENT_BY_DEF_ID_ACT_VERSION_TASK_DEF_ID_KEY + caculateKey);
    	cacheUtil.getEhCacheUtil().removeAll(LIST_VALUES_KEY + caculateKey);
    	cacheUtil.getEhCacheUtil().removeAll(VALUES_KEY + caculateKey);
    }
    
    
    public static final String FLOW_ELEMENT_BY_PROCESS_DEF_ID_TASK_DEF_ID_KEY = "flowElementByProcessDefIdTaskDefId";
    public static final String TASK_ELEMENT_BY_DEF_ID_ACT_VERSION_TASK_DEF_ID_KEY = "taskElementByDefIdActVersionTaskDefId";
    public static final String LIST_VALUES_KEY = "listValues";
    public static final String VALUES_KEY = "values";
    
    private void refreshValueByCalcKey(String caculateKey) {
    	
    	if(listValues!=null && listValues.size()>0) {
        	for(String key:listValues.keySet()) {
        		if(key.contains(caculateKey)) {
        			listValues.remove(key);
        		}
        	}
    	}
    	if(values!=null && values.size()>0) {
        	for(String key:values.keySet()) {
        		if(key.contains(caculateKey)) {
        			values.remove(key);
        		}
        	}
    	}
    	if(taskElementByDefIdActVersionTaskDefId!=null && taskElementByDefIdActVersionTaskDefId.size()>0) {
        	for(String key:taskElementByDefIdActVersionTaskDefId.keySet()) {
        		if(key.contains(caculateKey)) {
        			taskElementByDefIdActVersionTaskDefId.remove(key);
        		}
        	}
    	}
    	if(flowElementByProcessDefIdTaskDefId!=null && flowElementByProcessDefIdTaskDefId.size()>0) {
        	for(String key:flowElementByProcessDefIdTaskDefId.keySet()) {
        		if(key.contains(caculateKey)) {
        			flowElementByProcessDefIdTaskDefId.remove(key);
        		}
        	}
    	}
	}
    
	public static String getKey(String keyGroupName,String[] keys,int getKeyCnt) {	
		StringBuilder sbKey = new StringBuilder();
    	if(!StrUtil.isEmpty(keyGroupName)) {
    		sbKey.append(keyGroupName);
    		sbKey.append(SEPERATOR);
    	}
    	
    	if(keys == null|| keys.length==0) {
    		throw new RuntimeException("keys can not be all null");
    	}
    	int validKeysCnt = 0;
    	for(int i=0;i<keys.length&&i<getKeyCnt;i++) {
    		if(keys[i] == null || NULL_KEY_VALUE.equals(keys[i])) {
			}else {
        		if(i != 0) {
        			sbKey.append(SEPERATOR);
        		}
    			sbKey.append(keys[i]);
    			validKeysCnt++;
    		}
    	}
    	if(validKeysCnt == 0) {
    		throw new RuntimeException("keys can not be all null");
    	}
		return sbKey.toString();
	}
	
	public void refreshByKey(CacheUtil.SvcType svcType,String keyGroupName, String[] keys, boolean dolog) {
		refreshValueByKeyGroup(keyGroupName,keys) ;
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			if(dolog) {
				if(keys == null) {
					return;
				}
				if(keys.length == 1) {
					cacheUtil.logRefresh(svcType, keys[0], null, true);
				}else if(keys.length == NumberConstants.INT_TWO) {
					cacheUtil.logRefresh(svcType, keys[0],  keys[1], true);
				}
				
			}
		}else {
			cacheUtil.getCacheType();
		}
	}

	@SuppressWarnings("unchecked")
	protected T getCacheValueByKey(String keyGroupName, String[] keys){
		String key = getKey(keyGroupName,keys,getKeysCnt(keys));
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			return values.get(key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			return (T)cacheUtil.getRedisCacheUtil().getObject(VALUES_KEY + key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			return (T)cacheUtil.getEhCacheUtil().getObject(VALUES_KEY + key);
		}else {
			throw new RuntimeException("not support operation");
		}
		
	}
	protected void putCacheValueByKey(String keyGroupName, String[] keys,T resultValues){
		String key = getKey(keyGroupName,keys,getKeysCnt(keys));
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			values.put(key,resultValues);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			cacheUtil.getRedisCacheUtil().setObject(VALUES_KEY + key,resultValues,Long.valueOf(-1));
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			cacheUtil.getEhCacheUtil().putCache(VALUES_KEY + key, resultValues);
		}else {
			throw new RuntimeException("not support operation");
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<T> getCacheListValueByKey(String keyGroupName, String[] keys){
		String key = getKey(keyGroupName,keys,getKeysCnt(keys));
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			return listValues.get(key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			return (List)cacheUtil.getRedisCacheUtil().getObject(LIST_VALUES_KEY + key);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			return (List)cacheUtil.getEhCacheUtil().getObject(LIST_VALUES_KEY + key);
		}else {
			throw new RuntimeException("not support operation");
		}
	}
	protected void putCacheListValueByKey(String keyGroupName, String[] keys,List<T> values){
		String key = getKey(keyGroupName,keys,getKeysCnt(keys));
		if(cacheUtil.getCacheType() == CacheUtil.CacheType.local) {
			listValues.put(getKey(keyGroupName,keys,getKeysCnt(keys)),values);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.redis) {
			cacheUtil.getRedisCacheUtil().setObject(LIST_VALUES_KEY + key,values, NumberConstants.LONG_NEGATIVE_1);
		}else if(cacheUtil.getCacheType() == CacheUtil.CacheType.echache) {
			cacheUtil.getEhCacheUtil().putCache(LIST_VALUES_KEY + key, values);
		}else {
			throw new RuntimeException("not support operation");
		}
		
	}
	
	public T getCacheValueByKey(String selectMethodName,String firstKey,String secondKey,String thirdKey) {
		throw new RuntimeException("not support operation");
	}

	/**
	 * 根据key获取缓存值
	 * @param selectMethodName
	 * @param firstKey
	 * @param secondKey
	 * @return
	 */
	public abstract T getCacheValueByKey(String selectMethodName,String firstKey,String secondKey);

	/**
	 * 根据key获取缓存列表值
	 * @param selectMethodName
	 * @param firstKey
	 * @param secondKey
	 * @return
	 */
	public abstract List<T> getCacheListValueByKey(String selectMethodName,String firstKey,String secondKey);
	

	public Element getTaskElementByDefIdActVersionTaskDefId(String processDefId, BigInteger version, String taskDefId) {
		throw new RuntimeException("not support operation");
	}
	
	public Element getTaskElementByDefIdActivtiVersionTaskDefId(String processDefId, BigInteger actitivitiVersion, String taskDefId) {
		throw new RuntimeException("not support operation");
	}

	public FlowElement getBpmnModelNodeByTaskId(String processDefinitionId, String taskDefinitionKey) {
		throw new RuntimeException("not support operation");
	}

}
