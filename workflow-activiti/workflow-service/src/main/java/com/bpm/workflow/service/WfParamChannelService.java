package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfParamChannel;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfParamChannelService extends IService<WfParamChannel> {

    void deleteByItemName(String itemName);

    void saveBatch(String itemName, List<String> itemChannelTypeList);
}
