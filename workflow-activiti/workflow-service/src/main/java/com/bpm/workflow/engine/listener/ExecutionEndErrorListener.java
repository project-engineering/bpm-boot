package com.bpm.workflow.engine.listener;

import com.bpm.workflow.util.ModelProperties;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

/**
 * 流程示列结束的监听器
 */
@Component("executionEndErrorListener")
public class ExecutionEndErrorListener extends EndListener{
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateExecution execution) {
		// 调用节点进入触发事件
		String eventName = ModelProperties.flowEndErrorEvent.getName();
		this.common(eventName, execution);
	}
	 
}
