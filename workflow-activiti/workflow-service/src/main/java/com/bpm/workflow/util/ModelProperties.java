package com.bpm.workflow.util;

/**
 * 
 * @author xuesw
 * @version V1.0
 * @Description: 节点建模的扩展属性列表
 * @date 2018年7月2日
 */
public enum ModelProperties {

	//节点建模的扩展属性   第四个参数为0
	userRule("userRule", "", "1","0", "选人规则"), 
	autoAllot("autoAllot", "", "1","0", "自动分配"), 

	
//	maxCheckOutNum("maxCheckOutNum","","1","0","最大检出数"),
	//{"maxCheckOutNum":"3","type":"1","descp":"只检出禁止检入"}
	//其中type为2-已领用可退回（领用后可退回到任务池中） 3-已领用不可退回（领用后不可再退回到任务池中）
	checkInOut("checkInOut","","1","0","任务池任务"),

	functionList("functionList", "", "0","0", "功能清单"),
	notifyType("notifyType", "no", "0","0", "任务通知"),
	timeStart("timeStart", "", "0","0", "定时启动"),
	accessoryProp("accessoryProp", "", "0","0", "附件属性"),
	selectOption("selectOption", "", "0","0", "可选意见"),
	taskAllocationType("taskAllocationType", "", "0","0", "任务分配方式"),
	featureTaskNameDisplay("featureTaskNameDisplay", "", "0","0", "特性任务名称展示"),
	confirmation("confirmation", "", "0","0", "确认信息提示"),
	autoNodeServiceCall("autoNodeServiceCall", "", "0","0","自动节点服务调用"),
	subprocessSelection("subprocessSelection", "", "0","0","子流程选择列表"),
	taskRecallCrtl("taskRecallCrtl", "", "0","0", "节点撤回控制属性"),

	cancelTaskEvent("cancelTaskEvent", "", "0","0","节点任务取消触发事件"),
	recallTaskEvent("recallTaskEvent", "", "0","0","节点任务撤回触发事件"),
	backTaskEvent("backTaskEvent", "", "0","0","节点任务退回触发事件"),
	startTaskEvent("startTaskEvent", "", "0","0","节点进入触发事件"),
	endTaskEvent("endTaskEvent", "", "0","0","节点离开触发事件"),
	accessoryControllerProp("ccessoryControllerProp", "", "0","0", "附件控制属性"),
	intputCondition("intputCondition", "", "0","0", "节点进入条件"),
	outputCondition("outputCondition", "", "0","0", "节点离开条件"),
	channelNodeCtrlList("channelNodeCtrlList", "", "0","0", "节点渠道控制列表"),
	channelNodeQuelList("channelNodeQueList", "", "0","0", "节点渠道查询列表"),
	nodeAutoRepeatProcess("nodeAutoRepeatProcess", "", "0","0", "处理人重复自动处理"),
	nodeAutoAllot("nodeAutoAllot", "", "0","0", "自动分配"), 

	//流程建模的扩展属性  第四个参数为1
	callBackJump("callBackJump", "", "0","1","打回重提跳转"),

	skipFistNode("skipFistNode", "fasle", "0","1", "首节点跳过"),
	checkPer("checkPer", "fasle", "0","1", "启动节点前台选人"),
	confirInfo("confirInfo", "fasle", "0","1", "启动节点路由提示"),
	personPro("personPro", "fasle", "0","1", "启动节点人工路由"),
	startCondition("startCondition", "", "0","1", "启动条件"),
	endCondition("endCondition", "", "0","1", "结束条件"),
	timeToStart("timeToStart","","0","1","定时启动"),
	flowStartEvent("flowStartEvent", "", "0","1","流程启动触发事件"),
	flowEndEvent("flowEndEvent", "", "0","1","流程结束触发事件"),
	flowEndErrorEvent("flowEndErrorEvent", "", "0","1","流程异常结束触发事件"),
//	startEvent("startEvent", "", "0","1","启动触发调用"),
//	endEvent("endEvent", "", "0","1","结束触发调用"),
	hangUpEvent("hangUpEvent", "", "0","1","挂起触发调用"),
	recoverEvent("recoverEvent", "", "0","1","挂起恢复触发调用"),
	cancelEvent("cancelEvent", "", "0","1","终止取消触发调用"),
	timeoutExecution("timeoutExecution", "", "0","1", "超时处理机制"),
	timeoutPreWarnExecution("timeoutPreWarnExecution", "", "0","1", "超时预警机制"),
	channelFlowCtrlList("channelFlowCtrlList", "", "0","1", "流程渠道控制列表"),
	channelFlowQueList("channelFlowQueList", "", "0","1", "流程渠道查询列表"),
	ignoreProcessVersion("ignoreProcessVersion", "", "0","1", "是否使用流程图的最新属性"),
	flowAutoRepeatProcess("flowAutoRepeatProcess", "", "0","1", "处理人重复自动处理"),
	flowAutoAllot("flowAutoAllot", "", "0","1", "自动分配"),
	isFlowAuthorize("isFlowAuthorize", "", "0","1", "是否流程支持转授权"),
	notifyForProcessCreater("notifyForProcessCreater", "", "0","1", "流程流转通知(发起人)")//{"type":"eventTrigger","descp":"动态提醒","eventId":"30"}
	;

	
	public static final String SYS_OWNER_ID = "sysOwnerId";

	private String name;
	private String valeu;
	private String desc;
	//0-流程引擎 1-规则引擎
	private String type;
	//0-节点建模扩展属性    1-流程建模扩展属性
	private String ownType;

	private ModelProperties(String name, String value, String type,String ownType, String desc) {
		this.name = name;
		this.valeu = value;
		this.type = type;
		this.desc = desc;
		this.ownType = ownType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValeu() {
		return valeu;
	}

	public void setValeu(String valeu) {
		this.valeu = valeu;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOwnType() {
		return ownType;
	}

	public void setOwnType(String ownType) {
		this.ownType = ownType;
	}
}
