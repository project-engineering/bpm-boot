package com.bpm.workflow.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.dto.transfer.WorkFlowNodePropData;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.entity.WfNodeProperty;
import com.bpm.workflow.entity.WfSystemStructure;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfNodePropertyMapper;
import com.bpm.workflow.service.WfDefinitionService;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.service.WfNodePropertyService;
import com.bpm.workflow.service.WfSystemStructureService;
import com.bpm.workflow.util.*;
import com.bpm.workflow.vo.NodeDefinitionQueryRequest;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfNodePropertyServiceImpl extends ServiceImpl<WfNodePropertyMapper, WfNodeProperty> implements WfNodePropertyService {
    @Resource
    private WfNodePropertyMapper wfNodePropertyMapper;
    @Resource
    private WfSystemStructureService wfSystemStructureService;
    @Resource
    private WfDefinitionService wfDefinitionService;
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;

    @Override
    public void insertNodeProperty(WorkFlowNodePropData nodeProperty)  {
        // 必输项检查
        checkDataProp(nodeProperty);
        // 检查父节点是否存在
        WfSystemStructure sysStruct = wfSystemStructureService.selectByKeyId(nodeProperty.getParentId().intValue());
        if (sysStruct == null) {
            throw new ServiceException("节点所属系统节点不存在！");
        }
        WfNodeProperty checkData = selectByNodeId(nodeProperty.getNodeId());
        if(null != checkData){
            throw new ServiceException("节点【" + nodeProperty.getNodeId() + "】已存在！");
        }
        // 新增节点属性
        wfNodePropertyMapper.insert(nodeProperty);
    }

    /**
     * 删除节点定义
     * @param nodeId
     */
    @Override
    public void deleteNodeProperty(String nodeId) {
        //先判断节点是否有被引用,如果有不能删除
        String param = "activiti:sysNodeId=\""+ nodeId + "\"";
        List<WfDefinition> wfDefinitionList = wfDefinitionService.selectWfDefinitionByXmlSource(param);
        if(wfDefinitionList.size() > 0){
            throw new ServiceException("节点有引用, 不能删除！");
        }
        // 删除前需要删除附带的扩展属性
        wfModelPropExtendsService.deleteModelPropExtendByNodeId(nodeId);
        // 删除节点记录
        LambdaQueryWrapper<WfNodeProperty> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfNodeProperty::getNodeId,nodeId);
        wfNodePropertyMapper.delete(queryWrapper);
    }

    @Override
    public void updateNodeProperty(WorkFlowNodePropData workFlowNodePropData) {
        // 必输项检查
        checkDataProp(workFlowNodePropData);
        WfNodeProperty targetNodeProp = wfNodePropertyMapper.selectById(workFlowNodePropData.getId().intValue());
        if (targetNodeProp == null) {
            throw new ServiceException("目标节点不存在！");
        }
        targetNodeProp.setNodeId(workFlowNodePropData.getNodeId());
        targetNodeProp.setNodeName(workFlowNodePropData.getNodeName());
        targetNodeProp.setType(workFlowNodePropData.getType());

        // 此处应该以主键ID为主进行修改
        // 修改节点属性
        wfNodePropertyMapper.updateById(targetNodeProp);
    }

    @Override
    public IPage<WfNodeProperty> selectList(NodeDefinitionQueryRequest nodeDefinitionQueryRequest) {
        LambdaQueryWrapper<WfNodeProperty> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(nodeDefinitionQueryRequest.getNodeId())) {
            queryWrapper.eq(WfNodeProperty::getNodeId,nodeDefinitionQueryRequest.getNodeId());
        }
        if (UtilValidate.isNotEmpty(nodeDefinitionQueryRequest.getNodeName())) {
            queryWrapper.like(WfNodeProperty::getNodeName,nodeDefinitionQueryRequest.getNodeName());
        }
        if (UtilValidate.isNotEmpty(nodeDefinitionQueryRequest.getParentId())) {
            queryWrapper.eq(WfNodeProperty::getParentId,nodeDefinitionQueryRequest.getParentId());
        }
        if (UtilValidate.isNotEmpty(nodeDefinitionQueryRequest.getType())) {
            queryWrapper.eq(WfNodeProperty::getType,nodeDefinitionQueryRequest.getType());
        }
        IPage<WfNodeProperty> page = new Page<>(nodeDefinitionQueryRequest.getStart(),nodeDefinitionQueryRequest.getLimit());
        return wfNodePropertyMapper.selectPage(page,queryWrapper);
    }


    /**
     * 基础数据检查
     *
     * @param nodeProperty
     * @
     */
    private void checkDataProp(WorkFlowNodePropData nodeProperty)  {
        if (StrUtil.isBlank(nodeProperty.getNodeId())) {
            throw new ServiceException("节点唯一标示不能为空！");
        }
        if (StrUtil.isBlank(nodeProperty.getNodeName())) {
            throw new ServiceException("节点名称不能为空！");
        }
        if (StrUtil.isBlank(nodeProperty.getType())) {
            throw new ServiceException("节点类型不能为空！");
        }
    }

    public WfNodeProperty selectByNodeId(String nodeId)  {
        LambdaQueryWrapper<WfNodeProperty> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WfNodeProperty::getNodeId,nodeId);
        return wfNodePropertyMapper.selectOne(queryWrapper);
    }

}
