package com.bpm.workflow.engine;

import org.activiti.spring.SpringProcessEngineConfiguration;

import java.util.ArrayList;

public class BpmProcessEngineConfiguration extends SpringProcessEngineConfiguration {

	/**
	* 重写初始化命令拦截器方法
	*/
	@Override
	public void initCommandInterceptors() {
		// 为父类的命令集合添加拦截器
		customPreCommandInterceptors = new ArrayList<>();
		// 再调用父类的实始化方法
		super.initCommandInterceptors();
	}
}
