package com.bpm.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bpm.workflow.entity.WfTemplate;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
public interface WfTemplateService extends IService<WfTemplate> {
    WfTemplate viewTemplate(String templateId);

    List<WfTemplate> selectTemplateList(Map<String, Object> param);
}
