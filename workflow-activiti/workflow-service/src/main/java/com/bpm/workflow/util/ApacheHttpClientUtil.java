package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.hc.client5.http.ClientProtocolException;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Log4j2
public class ApacheHttpClientUtil {

    private static final Logger logger = LoggerFactory.getLogger(ApacheHttpClientUtil.class);

	public static String sendHttpRequestForGet(String url){
		if(StrUtil.isEmpty(url)){
			log.info("| - ApacheHttpClientUtil>>>>>url is null!");
		}
		CloseableHttpClient httpclient = HttpClients.createDefault();
		String returnStr = "";
		try {
			// 创建httpget.
            HttpGet httpget = new HttpGet(url);
            log.info("| - ApacheHttpClientUtil>>>>>executing request:{}", url);
            // 执行get请求.
            CloseableHttpResponse response = httpclient.execute(httpget);
            try {
                // 获取响应实体
                HttpEntity entity = response.getEntity();
                // 打印响应状态
				log.info("| - ApacheHttpClientUtil>>>>>executing status:{}", response.getCode());
                if (entity != null) {
                    // 打印响应内容长度
                	log.info("| - ApacheHttpClientUtil>>>>>Response content length:{} " , entity.getContentLength());
                    // 打印响应内容
                	log.info("| - ApacheHttpClientUtil>>>>>Response content: {}" , EntityUtils.toString(entity));
                }
            } finally {
                response.close();
            }
		} catch (ClientProtocolException e) {
			log.error("| - ApacheHttpClientUtil>>>>>httpclient executeMethod ClientProtocolException:{}",e.getMessage(), e);
			return null;
		} catch (Exception e) {
			log.error("| - ApacheHttpClientUtil>>>>>httpclient executeMethod error:{}",e.getMessage(), e);
			return null;
		} finally{
			// 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
            	log.error("| - ApacheHttpClientUtil>>>>>close httpclient fail!!",e);
            }
		}

		return returnStr;
	}

	public static String sendHttpRequestForPost(String invokeUrl, String paramJson,int timeOutUse) {
		long execHttpClientStartTime = System.currentTimeMillis();
		log.info("| - ApacheHttpClientUtil>>>>>invokeHttpPostService:  [invokeUrl]:{}  | [paramJson]:{} " ,invokeUrl, paramJson);
		int timeOut = 60000;
		if(timeOutUse>0) {
			timeOut = timeOutUse;
		}
		HttpTemplateUtil httpTemplate = new HttpTemplateUtil();
		String result = httpTemplate.postForObject(invokeUrl, paramJson);
		log.info("| - ApacheHttpClientUtil>>>>>invokeHttpPostService result:{}" , result);
		long execHttpClientEndTime = System.currentTimeMillis()-execHttpClientStartTime;
		log.info("| - ApacheHttpClientUtil>>>>> ApacheHttpClientUtil execHttpClient调用规则耗时：{} ms" , execHttpClientEndTime );
		return result;
	}

	/**
	 * https 绕过证书
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static SSLContext createIgnoreVerifySSL() throws NoSuchAlgorithmException, KeyManagementException {
	    SSLContext sc = SSLContext.getInstance("SSLv3");

	    // 实现一个X509TrustManager接口，用于绕过验证，不用修改里面的方法
	    X509TrustManager trustManager = new X509TrustManager() {
	        @Override
	        public void checkClientTrusted(
	                java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
	                String paramString) throws CertificateException {
	        }

	        @Override
	        public void checkServerTrusted(
	                java.security.cert.X509Certificate[] paramArrayOfX509Certificate,
	                String paramString) throws CertificateException {
	        }

	        @Override
	        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	            return null;
	        }
	    };
	    sc.init(null, new TrustManager[] { trustManager }, null);
	    return sc;
	}
}
