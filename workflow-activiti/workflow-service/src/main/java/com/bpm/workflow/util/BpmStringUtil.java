package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.constant.SymbolConstants;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.entity.WfTaskflow;
import com.bpm.workflow.exception.ServiceException;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.task.Task;

import java.util.*;

@Log4j2
public class BpmStringUtil {
    
    public static int getIntByString(Object theStr) {
    	if(theStr == null) {
    		return 0;
    	}
    	
    	return Integer.parseInt(StrUtil.toString(theStr));
    }
    
	public static String formatByteInBase64(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}
	
	public static byte[] parseBas64String(String theStr) {
		return Base64.getDecoder().decode(theStr);
	}
	
	public static String getCamelFromDbCol(String columnName) {

		if (columnName == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		boolean foundUndline = false;
		for (int i = 0; i < columnName.length(); i++) {
			char c = columnName.charAt(i);
			if (c == '_') {
				foundUndline = true;
				continue;
			}
			if (foundUndline) {
				sb.append(Character.toUpperCase(c));
				foundUndline = false;
			} else {
				sb.append(Character.toLowerCase(c));
			}
		}

		return sb.toString();
	}

	public static String getFirstUpperWord(String name) {
		if (name.length() == 1) {
			return name.toUpperCase();
		}
		return name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
	}

	/**
	 * 给ori字符串前或者后添加digit长度的替代字符
	 * 
	 * @param ori
	 * @param thefillChar
	 * @param digit
	 * @param isPrefix
	 * @return
	 */
	public static String fillStrWithDigit(String ori, char thefillChar, int digit, boolean isPrefix) {
		StringBuilder sb = new StringBuilder();
		sb.append(ori);
		fillStrWithDigit(sb, thefillChar, digit, isPrefix);
		return sb.toString();
	}

	/**
	 * 给ori StringBuilder前或者后添加digit长度的替代字符
	 * 
	 * @param ori
	 * @param thefillChar
	 * @param digit
	 * @param isPrefix
	 * @return
	 */
	public static void fillStrWithDigit(StringBuilder ori, char thefillChar, int digit, boolean isPrefix) {
		if (ori == null) {
			ori = new StringBuilder();
		}

		if (isPrefix) {
			for (int i = 0; i < digit; i++) {
				ori.insert(0, thefillChar);
			}
		} else {
			for (int i = 0; i < digit; i++) {
				ori.append(thefillChar);
			}
		}

	}

	private static int nIndex = -1;
	private static int[] seqPool = null;
	protected static final int POOL_SIZE = 100000;
	protected static int poolDigit = -1;
	private static synchronized int getSeqInt() {
		if(seqPool ==null) {
			nIndex = 0;
			seqPool=new int[POOL_SIZE];
			poolDigit = ("" + POOL_SIZE).length();
			for(int i = 0; i < seqPool.length; i++) {
				seqPool[i] = i;
			}
		}

		int result = seqPool[nIndex];
		if(nIndex == seqPool.length-1) {
			nIndex =0;
		}else {
			nIndex++;
		}
		return result;
	}


	public static synchronized String getUniqId() {
		StringBuilder result = new StringBuilder();
		result.append(getSeqInt());
		fillStrWithDigit(result,'0',poolDigit,true);
		result.insert(0, System.currentTimeMillis());
		return result.toString();
	}
	
	public static boolean isResultIn(String result,String[] expectations) {
		if(result == null) {
			return false;
		}
		for(String expectation:expectations) {
			if(result.equals(expectation)) {
				return true;
			}
		}
		return false;
	}
	

	
	public static boolean isUniqMatchTasksNames(List<Task> tasks,String[] expectations) {
		if(tasks == null||tasks.size() == 0) {
			return false;
		}
		Set<String> names = new HashSet<>();
		for(Task task: tasks) {
			if(names.contains(task.getName())) {
				return false;
			}
			if(isResultIn(task.getName(), expectations)) {
				return true;
			}
		}
		return false;
	}
	
	public static String getExceptionStack(Exception e) {
		if(e ==null) {
			return Constants.STR_NULL;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(e.getClass().getName()).append("n: ").append(e.getMessage()).append(System.lineSeparator());
		for(StackTraceElement ele:e.getStackTrace()) {
			sb.append("	at ").append(ele.getClassName()).append(".").append(ele.getMethodName());
			sb.append("(").append(ele.getFileName()).append(":").append(ele.getLineNumber()).append(")");
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
	
	public static String arrayToString(Object[] objs) {
		if(objs ==null) {
			return Constants.STR_NULL;
		}
		if(objs.length ==0) {
			return SymbolConstants.SYMBOL_006;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(SymbolConstants.SYMBOL_003);
		for(int i = 0; i<objs.length; i++) {
			if(i == 0) {
				sb.append(objs[i]);
			}else {
				sb.append(SymbolConstants.COMMA).append(objs[i]);
			}
			
		}
		sb.append(SymbolConstants.SYMBOL_004);
		return sb.toString();
	}

	@SuppressWarnings("rawtypes")
	public static JSONArray listToJson(List theList) {
		if(theList == null || theList.size()==0) {
			return null;
		}
		return JSONArray.parseArray(JSONArray.toJSONString(theList));
	}

	public static JSONObject objectToJsonObject(Object object) {
		if(object == null ) {
			return null;
		}
		
		if(object instanceof String) {
			return JSONObject.parseObject((String) object);
		}
		
		return JSONObject.parseObject(JSONObject.toJSONString(object));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static JSONObject mapToJson(Map theMap) {
		JSONObject jsonObject = new JSONObject();

		if(theMap == null || theMap.size()==0) {
			return jsonObject;
		}
		for(Object theMapEntryObj:theMap.entrySet()) {
			Map.Entry anEntry = (Map.Entry)theMapEntryObj;
			if(anEntry.getKey() == null || anEntry.getValue()==null) {
				continue;
			}
			if(anEntry.getValue() instanceof Map) {
				jsonObject.put(anEntry.getKey().toString(), mapToJson((Map<Object,Object>)anEntry.getValue()));
			}else {
				jsonObject.put(anEntry.getKey().toString(), anEntry.getValue());
			}
		}
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public static Map<String,Object> jsonToMap(Object obj) {
		if(obj == null) {
			return null;
		}
		if(obj instanceof String) {
			return JSONObject.parseObject(obj.toString());
		}
		if(obj instanceof Map) {
			return (Map<String,Object>)obj;
		}
		if(JSONObject.class.isInstance(obj) ) {
			return (JSONObject)obj;
		}
		if(JSONObject.class.isInstance(obj) ) {
			JSONObject nfJsonObj = (JSONObject)obj;
			return JSONObject.parseObject(nfJsonObj.toString());
		}
		return null;
	}
	
	/**
	 * 
	 * @param path mapkey1.mapkey2.mapkey3[listIndx].mapkey4.mapkey5[listIndex]
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object getByPath(Map<String,Object>theMap,String path) {
		if(theMap == null || theMap.size()==0|| StrUtil.isEmpty(path)) {
			return null;
		}
		String[] pathArr = path.split(SymbolConstants.SYMBOL_005);
		if(pathArr.length == 0) {
			pathArr = new String[] {path};
		}
		Object temp = theMap;
		try {
			for(String theKey: pathArr) {
				if(!theKey.contains(SymbolConstants.SYMBOL_003)) {
					temp = ((Map<String,Object>)temp).get(theKey);
				}else {				
					int index = Integer.parseInt(theKey.substring(theKey.indexOf(SymbolConstants.SYMBOL_003) + 1, theKey.indexOf(SymbolConstants.SYMBOL_004)));
					theKey = theKey.substring(0, theKey.indexOf(SymbolConstants.SYMBOL_003));
					temp = ((Map<String, Object>) temp).get(theKey);
					if(temp instanceof List) {
						temp= ((List)temp).get(index);
					}else if(temp instanceof JSONArray) {
						temp= ((JSONArray)temp).get(index);
					}else {
						throw new RuntimeException("not support tye:" + temp.getClass().getName());
					}
				}
			}
		} catch(Exception e) {
			log.error("| - BpmStringUtil>>>>>try to get {} from {}",path,theMap,e);
			return null;
		}
		return temp;
	}

	public static String templatToMsgContent(String template, Map<String, Object> varMap) {
		if (StrUtil.isBlank(template) || varMap == null || varMap.size() == 0) {
			return template;
		}
		
		for (Map.Entry<String, Object> anEntry:varMap.entrySet()) {
			String templateKey = SymbolConstants.SYMBOL_001 + anEntry.getKey() + SymbolConstants.SYMBOL_002;

			if (template.contains(templateKey)) {
				template = template.substring(0, template.indexOf(templateKey))+anEntry.getValue()
						+template.substring(template.indexOf(templateKey)+templateKey.length());
				
			}
		}
		
		while(template.contains(SymbolConstants.SYMBOL_001)) {
			String temp =template.substring(0, template.indexOf(SymbolConstants.SYMBOL_001));
			template = template.substring(template.indexOf(SymbolConstants.SYMBOL_001));
			template =temp +template.substring(template.indexOf(SymbolConstants.SYMBOL_002) + 1);
		}
		return template;
	}

	public static List<UserOutputData> getDoTaskUserDataFromVarMap(WfTaskflow wfTaskflow) {
		if(wfTaskflow == null ) {
			throw new RuntimeException("task not found");
		}
		if(StrUtil.isEmpty(wfTaskflow.getTaskActors()) || StrUtil.isEmpty(wfTaskflow.getTaskActorsData())) {
			throw new RuntimeException("task has no getTaskActors:"+wfTaskflow.getTaskId());
		}
		
		List<UserOutputData> result = new ArrayList<>();
		
		Set<String> userIds;
		if(StrUtil.isEmpty(wfTaskflow.getDoTaskActor()) ) {
			userIds = getSetByStrArr(wfTaskflow.getTaskActors().split(Constants.COMMA));
		}else {
			userIds = getSetByStrArr(new String[] {wfTaskflow.getDoTaskActor()});
		}
		JSONObject actorDataObject = JSONObject.parseObject(wfTaskflow.getTaskActorsData());
		for(String userId: userIds) {
			JSONObject actorData = actorDataObject.getJSONObject(userId);
			UserOutputData userOutputData = new UserOutputData();
			userOutputData.setUserId(userId);
			userOutputData.setUserName(actorData.getString("userName"));
			userOutputData.setUserEmail(actorData.getString("userEmail"));
			userOutputData.setUserPhone(actorData.getString("userPhone"));
			result.add(userOutputData);
		}
		return result;
	}
	
    public static Set<String> getSetByStrArr(String[] arr){
    	if(arr == null ) {
    		return null;
    	}
		return new HashSet<>(Arrays.asList(arr));
    }
    
	private static String getErrorCodeFromServiceException(Throwable e) {	
		if(e.getMessage() != null && e.getMessage().contains(ServiceException.class.getName())) {
			String s= "com.bpm.workflow.exception.ServiceException: ";
			return e.getMessage().substring(e.getMessage().indexOf(s)+s.length());
		}
		if(e.getStackTrace().length>0) {
			if(ServiceException.class.getName().equals(e.getStackTrace()[0].getClassName())) {
				return e.getMessage();
			}
			if(ServiceException.class.getName().equals(e.getStackTrace()[0].getClassName())) {
				return e.getMessage();
			}
		}

		return null;
	}

	public static String getBusinessErrorCode(Exception e) {
		try {
			String result = getErrorCodeFromServiceException(e);
			if(result != null) {
				return result;
			}
			Throwable theTh = e.getCause();
			int i=0;
			while(theTh != null && i < NumberConstants.INT_20) {
				theTh = e.getCause();
				result = getErrorCodeFromServiceException(theTh);
				if(result != null) {
					return result;
				}
				i++;
				if(theTh.equals(theTh.getCause())) {
					break;
				}
			}
			return e.getMessage();
		}catch(Exception e1) {
			log.error("getBusinessErrorCode失败！",e1);
		}
		return null;
	}    
}
