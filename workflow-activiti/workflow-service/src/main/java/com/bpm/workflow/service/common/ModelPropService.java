package com.bpm.workflow.service.common;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.util.BpmStringUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 模型（节点或流程）扩展属性最终值获取服务实现
 * 扩展属性最终值获取
 */
@Log4j2
@Service
public class ModelPropService {

	@Resource
	private UruleServiceFactory uRule;

	@Resource
	private WfCommService wfCommService;

	/**
	 * 仅获取简单配置值入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getNomalModelPropValue(WfModelPropExtends modelPropExtends) {
		if (modelPropExtends != null) {
			return modelPropExtends.getPropValue();
		}
		return null;
	}

	/**
	 * 仅获取简单配置值入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getNomalModelPropValue(List<WfModelPropExtends> modelPropList, String propName) {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		return getNomalModelPropValue(modelProp);
	}

	/**
	 * 仅通过规则引擎获取值的入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getRuleModelPropValue(WfModelPropExtends modelPropExtends, Map<String, Object> varibaleMap)
			throws Exception {

		String jsonObj = getNomalModelPropValue(modelPropExtends);
		if (StrUtil.isNotBlank(jsonObj)) {
			return uRule.getInstance().execUruleReusltList(varibaleMap, JSONObject.parseObject(jsonObj));
		}
		return null;
	}

	/**
	 * 仅通过规则引擎获取值的入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getRuleModelPropValue(List<WfModelPropExtends> modelPropList, String propName,
			Map<String, Object> varibaleMap) throws Exception {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		return getRuleModelPropValue(modelProp, varibaleMap);
	}

	/**
	 * 仅通过js脚本取值的入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getJavaScriptModelPropValue(WfModelPropExtends modelPropExtends, Map<String, Object> varibaleMap)
			throws Exception {
		String javaScript = getNomalModelPropValue(modelPropExtends);
		if (StrUtil.isNotBlank(javaScript)) {
			return wfCommService.executeJavaScript(javaScript, varibaleMap);
		}
		return null;
	}

	/**
	 * 仅通过js脚本取值的入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getJavaScriptModelPropValue(List<WfModelPropExtends> modelPropList, String propName,
			Map<String, Object> varibaleMap) throws Exception {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		return getJavaScriptModelPropValue(modelProp, varibaleMap);
	}

	/**
	 * 通过简单配置或从规则引擎获取值的入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getNomalOrRulePropValue(WfModelPropExtends modelPropExtends, Map<String, Object> varibaleMap)
			throws Exception {
		String propValue = getNomalModelPropValue(modelPropExtends);
		if (StrUtil.isNotBlank(propValue)) {
			Map<String, Object> tempMap = JSONObject.parseObject(propValue);
			String type = (String) tempMap.get("type");
			Object valueObj = tempMap.get("value");
			if (Constants.FILED_TYPE_NOMEAL.equals(type)) {
				return valueObj.toString();
			} else if (Constants.FILED_TYPE_URULE.equals(type)) {
				return uRule.getInstance().execUruleReusltList(varibaleMap, BpmStringUtil.objectToJsonObject(valueObj));
			}
		}
		return null;
	}

	/**
	 * 通过简单配置或从规则引擎获取值的入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getNomalOrRulePropValue(List<WfModelPropExtends> modelPropList, String propName,
			Map<String, Object> varibaleMap) throws Exception {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		return getNomalOrRulePropValue(modelProp, varibaleMap);
	}

	/**
	 * 通过简单配置或js脚本获取值的入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getNomalOrJavaScriptPropValue(WfModelPropExtends modelPropExtends, Map<String, Object> varibaleMap)
			throws Exception {
		String propValue = getNomalModelPropValue(modelPropExtends);
		if (StrUtil.isNotBlank(propValue)) {
			Map<String, Object> tempMap = JSONObject.parseObject(propValue);
			String type = (String) tempMap.get("type");
			Object valueObj = tempMap.get("value");
			if (Constants.FILED_TYPE_NOMEAL.equals(type)) {
				return valueObj.toString();
			} else if (Constants.FILED_TYPE_JS.equals(type)) {
				String javaScript = valueObj.toString();
				return wfCommService.executeJavaScript(javaScript, varibaleMap);
			}
		}
		return null;
	}

	/**
	 * 通过简单配置或js脚本获取值的入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getNomalOrJavaScriptPropValue(List<WfModelPropExtends> modelPropList, String propName,
			Map<String, Object> varibaleMap) throws Exception {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		return getNomalOrJavaScriptPropValue(modelProp, varibaleMap);
	}

	/**
	 * 通过规则引擎或js脚本获取值的入口
	 * 
	 * @param modelPropExtends
	 * @return
	 */
	public String getRuleOrJavaScriptPropValue(WfModelPropExtends modelPropExtends, Map<String, Object> varibaleMap)
			throws Exception {
		String propValue = getNomalModelPropValue(modelPropExtends);
		if (StrUtil.isNotBlank(propValue)) {
			Map<String, Object> tempMap = JSONObject.parseObject(propValue);
			String type = (String) tempMap.get("type");
			Object valueObj = tempMap.get("value");
			
			log.info("| - ModelPropServiceImpl>>>>>条件类型：{}[{}]",modelPropExtends.getPropName(),modelPropExtends.getPropDesc());
			log.info("| - ModelPropServiceImpl>>>>>条件处理方式：{}",type);
			log.info("| - ModelPropServiceImpl>>>>>条件表达式：{}",valueObj);
			
			if (Constants.FILED_TYPE_JS.equals(type)) {
				String javaScript = valueObj.toString();
				return wfCommService.executeJavaScript(javaScript, varibaleMap);
			} else if (Constants.FILED_TYPE_URULE.equals(type)) {
				return uRule.getInstance().execUruleReusltList(varibaleMap, BpmStringUtil.objectToJsonObject(valueObj));
			}
		}
		return null;
	}

	/**
	 * 通过规则引擎或js脚本获取值的入口
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public String getRuleOrJavaScriptPropValue(List<WfModelPropExtends> modelPropList, String propName,
			Map<String, Object> varibaleMap) throws Exception {
		WfModelPropExtends modelProp = getSpcefiyModelProp(modelPropList, propName);
		if (modelProp == null) {
			return "";
		}
		
		return getRuleOrJavaScriptPropValue(modelProp, varibaleMap);
	}

	/**
	 * 获取属性列表中指定的属性
	 * 
	 * @param modelPropList
	 * @param propName
	 * @return
	 */
	public WfModelPropExtends getSpcefiyModelProp(List<WfModelPropExtends> modelPropList, String propName) {
		if (StrUtil.isNotBlank(propName) && modelPropList != null) {
			for (WfModelPropExtends tempModelProp : modelPropList) {
				if (tempModelProp.getPropName().equals(propName)) {
					return tempModelProp;
				}
			}
		}
		return null;
	}
}
