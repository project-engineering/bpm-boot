package com.bpm.workflow.client;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.client.urule.UruleServiceFactory;
import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.dto.transfer.UserOutputData;
import com.bpm.workflow.exception.ServiceException;
import com.bpm.workflow.mapper.WfPocUserModelMapper;
import com.bpm.workflow.timertask.StartWorkFlowTask;
import com.bpm.workflow.timertask.TimerUtil;
import com.bpm.workflow.util.BpmStringUtil;
import com.bpm.workflow.util.ConfigUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.*;
import java.util.Map.Entry;

@Log4j2
@Service(value = "queryRelationTeller")
abstract class QueryRelationTeller implements IQueryRelationTeller {

    @Resource
    private UruleServiceFactory uruleServiceFactory;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private TimerUtil timerUtil;

    @Resource
    private WfPocUserModelMapper wfPocUserModelMapper;

    /**
     * 根据策略查询符合条件的user列表
     * <pre>
     * 由于规则引擎查询不出具体的用户id，因此需要这里传入userId和userType的关系，替换规则平台返回的查询结果中的userType，然后以userId及规则引擎返回的规则去安全平台查询对应的用户信息
     * 因此该方法参数分为查询规则的参数userList，保存关系列表的参数jsonStr
     *
     * <b>选人策略有两种调用结果</b>
     * 1、正常调用，查询出符合条件的规则，根据规则调用安全平台的人员信息。
     * 2、调用失败，则返回需要调用的流程编码，并启动流程，整个审批流程结束后回调转办流程
     * </pre>
     *
     * @param jsonStr       <pre>json串，用来存储人员类型及人员id的关系，其中 target为0-客户经理，1-对方客户经理，2-流程发起人，3-上一节点处理人
     *                      					例如：{
     *                      							"id":"selectPersonRule",
     *                      							"list":
     *                      								[
     *                                                        {
     *                      										"source":"zhangsan",
     *                      										"target":"0"
     *                                                        },
     *                                                        {
     *                      										"source":"lisi",
     *                      										"target":"1"
     *                                                        } 	 * 			 					]
     *                                            }
     *                      				</pre>
     * @param userList      <pre>满足多个条件匹配，比如按照客户经理，对方客户经理，流程发起人，上一节点处理人等同时查询的规则,解析成json串如下所示：
     *                      				"userList":[ {
     *                        					"userType":"0",
     *                       					"userId":"111111",
     *                         					"userPost":"1",
     *                         					 ...
     *                                        },
     *                                    {
     *                        					"userType":"1",
     *                         					"userId":"2222",
     *                        					"userPost":"1",
     *                        					...
     *                                        }
     *                      				  ]
     *                      				</pre>
     * @param busMap        流程发起的业务参数
     * @param taskId        节点id
     * @param processDefId  流程定义id
     * @param processInstId 流程实例id
     * @return 符合条件的user列表
     * @return 符合条件的user列表
     */
    @Override
    public List<UserOutputData> queryTellerByPolicy(String taskId, String processDefId, String processInstId, String jsonStr, List<Map<String, Object>> userList, Map<String, Object> busMap, String branch) throws Exception {

        if (log.isDebugEnabled()) {
            log.info("| - queryTellerByPolicy>>>>>当前任务ID[{}]", taskId);
            log.info("| - queryTellerByPolicy>>>>>当前流程定义ID[{}]", processDefId);
            log.info("| - queryTellerByPolicy>>>>>当前流程实例ID[{}]", processInstId);
            log.info("| - queryTellerByPolicy>>>>>人员类型及人员id的关系，其中 target为0-客户经理，1-对方客户经理，2-流程发起人，3-上一节点处理人[{}]", jsonStr);
            log.info("| - queryTellerByPolicy>>>>>人员信息列表[{}]", userList.toString());
            log.info("| - queryTellerByPolicy>>>>>业务参数[{}]", busMap.toString());
            log.info("| - queryTellerByPolicy>>>>>当前流程分支[{}]", branch);
        }

        String result = null;
        Map<String, Object> param = new HashMap<>(5);
//		Map<String,Object> uruleMap=new HashMap<String,Object>();
        List<UserOutputData> resultList = new ArrayList<>();


        //组装请求规则引擎的参数
        JSONObject jsonObj = JSONObject.parseObject(jsonStr);
//		String ruleId = configUtil.getProperty("urule.req.selectPersonRule");
        String userMapList = getUserProp(jsonObj, "list");
        if (userList != null && !userList.isEmpty()) {
            param.put("userList", userList);
        }
        if (busMap != null && !busMap.isEmpty()) {
            // param.put("businessMap", busMap);
            param.putAll(busMap);
        }
        log.debug("| - queryTellerByPolicy>>>>>userList:{}", userMapList);

//		jsonobj.putIfAbsent("knowledgeId", ruleId);

        // 判断是否已经查询过规则
        if (busMap != null && !busMap.isEmpty() && StrUtil.isNotEmpty(branch)) {
            resultList = hasPreSelectPersion(busMap, branch);
            if (resultList != null && resultList.size() > 0) {
                return resultList;
            }
        }

        //调用规则前使用挡板
        if (configUtil.isNoUrule()) {
            return generateUserData();
        }

        //调用urule知识包获取规则
        result = uruleServiceFactory.getInstance().execUruleReusltList(param, jsonObj);

        //调用规则后使用模拟用户数据取自数据库
        if (configUtil.isNoUserModel()) {
            //组装访问安全平台的报文
            JSONArray userResult = (JSONArray) JSONArray.parse(result);
            if (userResult != null && userResult.size() > 0) {
                for (int i = 0; i < userResult.size(); i++) {
                    JSONObject object = (JSONObject) userResult.get(i);
                    if (object.get("orgId") != null && !StrUtil.isEmpty((String) object.get("orgId"))) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("orgId", (String) object.get("orgId"));
                        List<UserOutputData> userData = wfPocUserModelMapper.selectUser(map);
                        if (userData != null && userData.size() > 0) {
                            resultList.addAll(userData);
                        }
                    }
                }
            }
            return resultList;
        }
        String beforUserId = (String) busMap.get(Constants.BEFOR_USERID);
        //组装访问安全平台的报文
        Map<String, String> uMap = this.getUserMap(userMapList);
        log.debug("| - queryTellerByPolicy>>>>>uMap:{}", uMap);
        //当配置参数为不引用第三方服务时，直接返回测试数据，方便测试
        if (StrUtil.isEmpty(result)) {

            throw new ServiceException("规则调用异常：规则返回结果为空");
        }
        JSONArray jsonArray = formatReq(taskId, result, uMap, beforUserId, busMap);
        if (jsonArray == null) {
            return resultList;
        }

        //调用规则后使用模拟用户数据取自Java
        if (configUtil.isNotBusiness()) {
            return generateUserData();
        }
        //查询用户信息列表
        resultList = queryUserList(jsonArray, uMap, busMap);

        return resultList;
    }

    /**
     * 已经在上一个节点流转之前选人了
     *
     * @throws Exception
     */
    private List<UserOutputData> hasPreSelectPersion(Map<String, Object> variableMap, String branch) throws Exception {

        if (variableMap.containsKey(Constants.ROUTE_SELECT_KEY) && !StrUtil.isEmpty(branch)) {
            Object obj1 = variableMap.get(Constants.ROUTE_SELECT_KEY);
            if (obj1 instanceof JSONArray) {
                JSONArray array = (JSONArray) (obj1);
                for (int i = 0; i < array.size(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    log.info("| - queryTellerByPolicy>>>>>hasPreSelectPersion:{}", obj.toJSONString());
                    if (obj.getString("branch").equals(branch) && obj.containsKey("useAskList")) {
                        JSONArray userAskListArray = obj.getJSONArray("useAskList");
                        if (userAskListArray == null || userAskListArray.isEmpty()) {
                            return null;
                        }
                        array.remove(obj);
                        List<UserOutputData> userDataList = this.queryUserList(userAskListArray);
                        return userDataList;
                    }
                }
            } else if (obj1 instanceof ArrayList) {
                @SuppressWarnings("rawtypes")
                ArrayList array = (ArrayList) (obj1);
                log.info("| - queryTellerByPolicy>>>>>错误的格式：{}", BpmStringUtil.listToJson(array).toJSONString());
            }
        }
        return new ArrayList<UserOutputData>();
    }

    /**
     * 解析规则引擎计算出的规则结果，如果返回结果是流程编码，则启动流程，如果流程启动失败，则记入异常数据
     * 否则解析成安全平台需要的报文
     *
     * @param result
     * @param map
     * @return
     */
    private JSONArray formatReq(String taskId, String result, Map<String, String> map, String beforUserId, Map<String, Object> varmap) {

        if (result.contains(Constants.QUERY_RELATION_TELLER_FAIL_FLOW_CODE) && taskId != null) {
            String workflowcode = result.split("=")[1].replace("}", "");
            if (log.isInfoEnabled()) {
                log.info("| - queryTellerByPolicy>>>>>规则返回流程编码，90后启动流程[{}]", workflowcode);
            }
            timerUtil.addWorkflowTimerTask(new StartWorkFlowTask(workflowcode, taskId, beforUserId, varmap), 0, 0, 0, 0, 0, 90);
            return null;
        }
        //替换userId
        JSONArray arrayobj = JSONArray.parseArray(result);
        log.info("| - queryTellerByPolicy>>>>>arrayobj:{}", result);

        if (ObjectUtil.isNotEmpty(arrayobj)) {
            for (int i = 0; i < arrayobj.size(); i++) {
                JSONObject forJSONObj = arrayobj.getJSONObject(i);
                String userType = getUserProp(forJSONObj, "userType");
                log.debug("| - queryTellerByPolicy>>>>>map:{} key:{}", map.toString(), userType);
                if (map.containsKey(userType)) {
                    forJSONObj.putIfAbsent("userId", map.get(userType));
                }
            }
            log.debug("| - queryTellerByPolicy>>>>>arrayobj:{}", arrayobj.toString());
            return arrayobj;
        }
        return null;
    }

    /**
     * @return
     */
    private Map<String, String> getUserMap(String jsonstr) {
        Map<String, String> map = new HashMap<String, String>(5);
        JSONArray userarray = JSONArray.parseArray(jsonstr);
        for (int i = 0; i < userarray.size(); i++) {
            JSONObject forJSONObj = userarray.getJSONObject(i);
            if (!StrUtil.isEmpty(getUserProp(forJSONObj, "target"))) {
                map.put(getUserProp(forJSONObj, "target"), getUserProp(forJSONObj, "source"));
            }
        }
        return map;
    }

    /**
     * @param umap
     * @param userAdd 原来0-流程发起人，1-上一处理人，增加2-客户经理，3-对方客户经理，4-子流程上节发起人。
     * @return
     */
    public String getUserAddOp(Map<String, String> umap, String userAdd) {
        //umap中对应的，0-客户经理，1-对方客户经理，2-流程发起人，3-上一节点处理人,4-子流程上节发起人
        switch (userAdd) {
            case "0":
                return umap.get("0");
            case "1":
                return umap.get("1");
            case "2":
                return umap.get("2");
            case "3":
                return umap.get("3");
            case "4":
                return umap.get("4");
            default:
                return null;
        }
    }

    @SuppressWarnings("unused")
    private String Set2String(Set<String> set) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] strsTrue = set.toArray(new String[set.size()]);
        for (String str : strsTrue) {
            stringBuilder.append(str).append(Constants.COMMA);
        }
        String result = null;
        if (stringBuilder.toString().contains(Constants.COMMA)) {
            result = stringBuilder.substring(0, stringBuilder.length() - 1);
        }
        return result;

    }

    /**
     * userAdd  指定人员类型
     * userDel  去除人员类型
     * 你判定返回的策略中，若有userAdd标识，那么获取其值，
     * 若值=0，表明为流程发起人，若值=1，表明为上一节处理人
     * 若值=0，表明剔除流程发起人，=1，剔除上一节处理人。
     *
     * @param list
     * @param umap    0-客户经理，1-对方客户经理，2-流程发起人，3-上一节点处理人
     * @param userDel
     * @return
     */
    @SuppressWarnings("unused")
    private List<UserOutputData> operatUserList(List<UserOutputData> list, Map<String, String> umap, String userDel) {
        Map<String, UserOutputData> userMap = new HashMap<String, UserOutputData>();
        for (UserOutputData d : list) {
            userMap.put(d.getUserId(), d);
        }

        if (!StrUtil.isEmpty(userDel)) {
            String[] users = userDel.split(",");
            for (String userId : users) {
                if (userMap.containsKey(userId)) {
                    userMap.remove(userId);
                }
            }
        }
        List<UserOutputData> list2 = new ArrayList<UserOutputData>();
        Iterator<Entry<String, UserOutputData>> iter = userMap.entrySet().iterator();  //获得map的Iterator
        while (iter.hasNext()) {
            Entry<String, UserOutputData> entry = iter.next();
            list2.add(entry.getValue());
        }
        return list2;
    }

    /**
     * 获取jsonObject对象key的value
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public String getUserProp(JSONObject jsonObject, String key) {
        Object objValue = jsonObject.get(key);
        if (objValue != null) {
            return objValue.toString();
        }
        return "";
    }

    /**
     * 测试数据
     *
     * @return
     */
    public static List<UserOutputData> generateUserData() {
        List<UserOutputData> list = new ArrayList<UserOutputData>();
        UserOutputData out = new UserOutputData();
        out.setUserId("lisi");
        out.setUserName("lisi");
        out.setUserEmail("lisi@qqq.com");
        out.setUserState("1");
        out.setUserBank("0001");
        out.setUserBankName("北京支行");
        list.add(out);
        UserOutputData out1 = new UserOutputData();
        out1.setUserId("zhangsan");
        out1.setUserName("zhangsan");
        out1.setUserEmail("zhangsan@qqq.com");
        out1.setUserState("1");
        out1.setUserBank("0001");
        out1.setUserBankName("北京支行");
        list.add(out1);
        UserOutputData out2 = new UserOutputData();
        out2.setUserId("wangwu");
        out2.setUserName("wangwu");
        out2.setUserEmail("wangwu@qqq.com");
        out2.setUserState("1");
        out2.setUserBank("0002");
        out2.setUserBankName("中国人民银行");
        list.add(out2);
        UserOutputData out3 = new UserOutputData();
        out3.setUserId("0500");
        out3.setUserName("0500");
        out3.setUserEmail("0500@qqq.com");
        out3.setUserState("1");
        out3.setUserBank("0003");
        out3.setUserBankName("陕西分行不靠谱支行");
        list.add(out3);
        UserOutputData out4 = new UserOutputData();
        out4.setUserId("130101");
        out4.setUserName("130101");
        out4.setUserEmail("130101@qqq.com");
        out4.setUserState("1");
        out4.setUserBank("0003");
        out4.setUserBankName("陕西分行不靠谱支行");
        list.add(out4);
        return list;
    }

    /**
     * 测试数据
     *
     * @param reqList
     * @return
     */
    public static List<UserOutputData> generateUserData(JSONArray reqList) {
        List<UserOutputData> list = new ArrayList<UserOutputData>();
        for (int i = 0; i < reqList.size(); i++) {
            JSONObject json = reqList.getJSONObject(i);
            String orderUserNo = json.getString("orderUserNo");
            UserOutputData out3 = new UserOutputData();
            out3.setUserId(orderUserNo);
            out3.setUserName(orderUserNo);
            out3.setUserEmail(orderUserNo + "@qqq.com");
            out3.setUserState("1");
            out3.setUserBank("0003");
            out3.setUserBankName("陕西分行不靠谱支行");
            list.add(out3);
        }
        return list;
    }


}
