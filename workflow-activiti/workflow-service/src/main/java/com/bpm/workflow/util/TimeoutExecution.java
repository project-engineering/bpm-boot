package com.bpm.workflow.util;

import lombok.Getter;
import lombok.Setter;

/**
 * @author yezx
 * @version V1.0
 * @Description: 超时处理机制
 * @date 2018年7月25日
 */
public enum TimeoutExecution {


    // 节点建模的扩展属性
    doNextNormal("超时通过", "01", true, false, false, false, "1"),
    doNextAbnormal("超时不通过", "02", true, false, false, false, "2"),
    endProcessNormal("正常终止流程", "03", true, true, true, false, "4"),
    endProcessAbnormal("异常常终止流程", "04", true, true, false, true, "04"),
    // 流程建模的扩展属性
    process_endProcessNormal("流程超时正常终止流程", "05", true, true, true, false, "2"),
    process_endProcessAbnormal("流程超时异常常终止流程", "06", true, true, false, true, "1");

    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private String value;
    @Setter
    @Getter
    private boolean needClean;
    @Setter
    @Getter
    private boolean isEndProcess;
    @Setter
    @Getter
    private boolean isNormal;
    @Setter
    @Getter
    private boolean isAbnormal;
    @Setter
    @Getter
    private String events = null;
    @Setter
    @Getter
    private String urule = null;
    @Setter
    @Getter
    private String datatime = null;
    @Setter
    @Getter
    private String type;

    TimeoutExecution(String name, String value, boolean needClean, boolean isEndProcess, boolean isNormal, boolean isAbnormal, String type) {
        this.name = name;
        this.value = value;
        this.needClean = needClean;
        this.isEndProcess = isEndProcess;
        this.isNormal = isNormal;
        this.isAbnormal = isAbnormal;
        this.type = type;
    }

    public static TimeoutExecution getTimeoutExecutionByValue(String value) {
        if (value == null) {
            return null;
        }

        for (TimeoutExecution theInst : TimeoutExecution.values()) {
            if (value.equals(theInst.getValue())) {
                return theInst;
            }
        }
        return null;
    }
}
