package com.bpm.workflow.timertask;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.constant.NumberConstants;
import com.bpm.workflow.entity.WfTaskDateConf;
import com.bpm.workflow.service.*;
import com.bpm.workflow.service.common.ActivitiBaseService;
import com.bpm.workflow.service.common.ProcessCoreService;
import com.bpm.workflow.util.constant.BpmConstants;
import com.bpm.workflow.util.*;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.ProcessEngine;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;


@Log4j2
@Component
public class TimerUtil {
    @Resource
    private ProcessEngine processEngine;

    @Resource
    private WfModelPropExtendsService wfModelpropExtendsService;
    @Resource
    private CacheDaoSvc cacheDaoSvc;
    @Resource
    private ProcessCoreService processCoreService;
    @Resource
    private ActivitiBaseService activitiBaseServer;
    @Resource
    private ProcessUtil processUtil;
    @Resource
    private WfTemplateService wfTemplateService;
    @Resource
    private WfParamService wfParamService;
    @Resource
    private CacheUtil cacheUtil;
    @Resource
    private SpringUtil springUtil;
    @Resource
    private ThreadUtil threadUtil;
    @Resource
    private ConfigUtil configUtil;
    @Resource
    private WfTaskDateConfService wfTaskDateConfService;
    @Resource
    private WfEvtServiceDefService wfEvtServiceDefService;
    private TaskRunner taskRunner = null;
    private ReentrantLock lockTimerUtil = new ReentrantLock();
    public static boolean isStop = false;
    private ReentrantLock lockTaskScheduleMap = new ReentrantLock();
    private Map<WorkflowTimerTask, WfTaskDateConf> taskScheduleMap = null;

    public enum TaskConfType {

        specific("specific"),
        schedule("schedule");

        private String name;

        private TaskConfType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static TaskConfType getByName(String theName) {
            for (TaskConfType taskConfType : TaskConfType.values()) {
                if (taskConfType.getName().equals(theName)) {
                    return taskConfType;
                }
            }
            throw new RuntimeException("not supported type:" + theName);
        }
    }

    public synchronized void stop() {
        lockTimerUtil.lock();
        try {
            isStop = true;
            if (taskRunner == null) {
                return;
            }
            taskRunner.getSetTimerStatus(false, true);
            taskRunner = null;
            if (timeoutTaskDealt != null) {
                timeoutTaskDealt.setDelaySecond(-1);
            }
        } catch (Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e), e);
            throw new RuntimeException(e);
        } finally {
            lockTimerUtil.unlock();
        }

    }

    @SuppressWarnings("rawtypes")
    private void initFromDb() {
        List theTasks = wfTaskDateConfService.selectByNodeId(ConfigUtil.getNodeId());
        if (theTasks != null && theTasks.size() > 0) {
            if (taskScheduleMap == null) {
                taskScheduleMap = new HashMap<>();
            } else {
                taskScheduleMap.clear();
            }
            for (Object obj : theTasks) {

                try {
                    WfTaskDateConf wfTaskDateConf = (WfTaskDateConf) obj;
                    WorkflowTimerTask workflowTimerTask = (WorkflowTimerTask) StreamUtil.deSerializeObjByStr(wfTaskDateConf.getSerializedTask());
                    taskScheduleMap.put(workflowTimerTask, wfTaskDateConf);
                } catch (Exception e) {
                    log.error("| - TimerUtil>>>>>定时扫描获取执行任务信息异常", e);
                }
            }
        }
    }

    public synchronized void start() {
        stop();

        isStop = false;

        lockTimerUtil.lock();
        try {
            initFromDb();
            startTimeoutDaemon();
            if (CacheUtil.USE_CACHE) {
                startCacheRefreshDaemon();
            }

            taskRunner = new TaskRunner();
            taskRunner.getSetTimerStatus(true, true);
            taskRunner.setDaemon(true);
            taskRunner.start();

            log.info("| - TimerUtil>>>>>TimerUtil started {}", Calendar.getInstance().getTime());
        } catch (Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e), e);
            throw new RuntimeException(e);
        } finally {
            lockTimerUtil.unlock();
        }
    }

    private static long getDelaySecond(int yearsFromNow, int monthsFromNow, int daysFromNow, int hoursFromNow, int minutesFromNow, int secondFromNow) {
        long result = 0;
        if (yearsFromNow != -1) {
            result = result + (long) yearsFromNow * 365 * 24 * 3600;
        }
        if (monthsFromNow != -1) {
            result = result + (long) monthsFromNow * 12 * 24 * 3600;
        }
        if (daysFromNow != -1) {
            result = result + (long) daysFromNow * 24 * 3600;
        }
        if (hoursFromNow != -1) {
            result = result + hoursFromNow * 3600L;
        }
        if (minutesFromNow != -1) {
            result = result + minutesFromNow * 60L;
        }
        if (secondFromNow != -1) {
            result = result + secondFromNow;
        }
        return result;
    }

    private void startCacheRefreshDaemon() {
        CacheRefresher cacheRefresher = new CacheRefresher(cacheUtil);
        cacheRefresher.setDaemon(true);
        cacheRefresher.start();
    }

    private TimeoutTaskDealt timeoutTaskDealt;

    private void startTimeoutDaemon() {
        TaskTimoutUtil.init(processEngine, wfModelpropExtendsService, cacheDaoSvc, processCoreService
                , activitiBaseServer, processUtil, wfParamService, wfTemplateService
                , wfEvtServiceDefService);

        String timeoutConfString = configUtil.getTimeOutConf();
        String timeoutPeriodType = configUtil.getTimeOutType();

        if (StrUtil.isEmpty(timeoutPeriodType) || StrUtil.isEmpty(timeoutConfString)) {
            fail("no timeout conf found");
        }

        String[] timeoutConf = timeoutConfString.split(",");
        int[] theConf = new int[timeoutConf.length];
        try {
            for (int i = 0; i < timeoutConf.length; i++) {
                theConf[i] = Integer.parseInt(timeoutConf[i]);
            }
        } catch (Exception e) {
            fail("wrong format of timeoutConfString:" + timeoutConfString);
        }
        timeoutTaskDealt = new TimeoutTaskDealt(threadUtil, wfParamService);

        timeoutTaskDealt.setDaemon(true);

        log.info("| - TimerUtil>>>>>config timout task, tyep:{} timeoutConfString:{}", timeoutPeriodType, timeoutConfString);
        if (BpmConstants.RANGE.equals(timeoutPeriodType)) {
            if (theConf.length != NumberConstants.INT_SIX) {
                fail("wrong format of timeoutConfString:" + timeoutConfString);
            }
            timeoutTaskDealt.setDelaySecond(getDelaySecond(theConf[0], theConf[1], theConf[2], theConf[3], theConf[4], theConf[5]));
            try {
                timeoutTaskDealt.setStartTime(configUtil.getTimeOutStartTime());
            } catch (ParseException e) {
                fail("wrong format of startTime:" + timeoutConfString);
            }

            timeoutTaskDealt.start();
        } else {
            fail("not supported timeout conf type");
        }
    }

    private void fail(String msg) {
        log.error(msg);
        springUtil.shutdown();
    }

    public String addWorkflowTimerTask(WorkflowTimerTask workflowTimerTask, int yearsFromNow, int monthsFromNow, int daysFromNow, int hoursFromNow, int minutesFromNow, int secondFromNow) {
        return addWorkflowTimerTask(workflowTimerTask, DateUtils.getDateByIntevalFromNow(yearsFromNow, monthsFromNow, daysFromNow, hoursFromNow, minutesFromNow, secondFromNow));
    }

    public static final int RUN_ENDLESS_TIMES = -100;

    public String scheduleTask(WorkflowTimerTask workflowTimerTask, int runtimes, int runMonth, int runDayOfMonth, int runDayOfWeek, int runHourOfDay
            , int runMinuteOfHour, int runSecondOfMinute) {
        if (taskScheduleMap == null) {
            taskScheduleMap = new HashMap<>();
        }
        WfTaskDateConf taskDateConfig = new WfTaskDateConf();
        lockTaskScheduleMap.lock();

        try {
            taskDateConfig.setConfType(TaskConfType.schedule.getName());
            taskDateConfig.setConfId(BpmStringUtil.getUniqId());
            taskDateConfig.setRuntimes(Integer.valueOf(runtimes));
            taskDateConfig.setRunMonth(Integer.valueOf(runMonth));
            taskDateConfig.setRunDayOfMonth(Integer.valueOf(runDayOfMonth));
            taskDateConfig.setRunDayOfWeek(Integer.valueOf(runDayOfWeek));
            taskDateConfig.setRunHourOfDay(Integer.valueOf(runHourOfDay));
            taskDateConfig.setRunMinuteOfHour(Integer.valueOf(runMinuteOfHour));
            taskDateConfig.setRunSecondOfMinute(Integer.valueOf(runSecondOfMinute));
            taskDateConfig.setSerializedTask(StreamUtil.serializeObjInStr(workflowTimerTask));
            wfTaskDateConfService.insert(taskDateConfig);
            taskScheduleMap.put(workflowTimerTask, taskDateConfig);
        } catch (Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e), e);
            throw new RuntimeException(e);
        } finally {
            lockTaskScheduleMap.unlock();
        }
        return taskDateConfig.getConfId();
    }


    private String addWorkflowTimerTask(WorkflowTimerTask workflowTimerTask, Date date) {
        log.info("| - TimerUtil>>>>>set exec time:{} now:{}", date, Calendar.getInstance().getTime());
        if (taskScheduleMap == null) {
            taskScheduleMap = new HashMap<>(1);
        }
        WfTaskDateConf taskDateConfig = new WfTaskDateConf();
        lockTaskScheduleMap.lock();

        try {
            taskDateConfig.setConfDate(DateUtils.formatDate2Str(date));
            taskDateConfig.setConfType(TaskConfType.specific.getName());
            taskDateConfig.setConfId(BpmStringUtil.getUniqId());
            taskDateConfig.setRuntimes(Integer.valueOf(1));
            taskDateConfig.setSerializedTask(StreamUtil.serializeObjInStr(workflowTimerTask));
            taskScheduleMap.put(workflowTimerTask, taskDateConfig);
        } catch (Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e), e);
            throw new RuntimeException(e);
        } finally {
            lockTaskScheduleMap.unlock();
        }

        wfTaskDateConfService.insert(taskDateConfig);

        return taskDateConfig.getConfId();
    }

    private Map<WorkflowTimerTask, WfTaskDateConf> runTasks = null;

    private void doSpecifyTimeExecTask() {
        lockTaskScheduleMap.lock();
        try {

            if (taskScheduleMap != null && taskScheduleMap.size() > 0) {
                if (runTasks == null) {
                    runTasks = new HashMap<>();
                }
                for (Map.Entry<WorkflowTimerTask, WfTaskDateConf> anEntry : taskScheduleMap.entrySet()) {
                    if (log.isDebugEnabled()) {
                        log.debug("| - TimerUtil>>>>>check:{}", anEntry.getKey());
                    }


                    if (needToRun(anEntry.getValue())) {
                        runTasks.put(anEntry.getKey(), anEntry.getValue());
                    }
                }
            }
        } catch (Exception e) {
            log.info(BpmStringUtil.getExceptionStack(e), e);
            throw new RuntimeException(e);
        } finally {
            lockTaskScheduleMap.unlock();
        }

        if (runTasks != null) {
            for (Map.Entry<WorkflowTimerTask, WfTaskDateConf> anEntry : runTasks.entrySet()) {
                execTask(anEntry);
            }
            runTasks.clear();
        }

    }


    private void execTask(Map.Entry<WorkflowTimerTask, WfTaskDateConf> anEntry) {
        if (log.isDebugEnabled()) {
            log.debug("| - TimerUtil>>>>>exec:{}", anEntry.getKey());
        }

        WfTaskDateConf wfTaskDateConf = anEntry.getValue();
        wfTaskDateConf.setLastExecDate(DateUtils.formatDate2Str(DateUtils.getNowTime()));

        TaskConfType taskConfType = TaskConfType.getByName(wfTaskDateConf.getConfType());
        if (taskConfType == TaskConfType.specific) {
            wfTaskDateConfService.delete(wfTaskDateConf);
            lockTaskScheduleMap.lock();
            try {
                taskScheduleMap.remove(anEntry.getKey());
            } catch (Exception e) {
                log.info(BpmStringUtil.getExceptionStack(e), e);
                throw new RuntimeException(e);
            } finally {
                lockTaskScheduleMap.unlock();
            }
        } else if (taskConfType == TaskConfType.schedule) {
            if (wfTaskDateConf.getRuntimes().intValue() != RUN_ENDLESS_TIMES) {
                if (wfTaskDateConf.getRuntimes().intValue() > 1) {
                    if (log.isInfoEnabled()) {
                        log.info("| - TimerUtil>>>>>======================about to exec schedule task===================================");
                        log.info("| - TimerUtil>>>>>remain runtime:{}", wfTaskDateConf.getRuntimes());
                        log.info("| - TimerUtil>>>>>getLastExecDate:{}", wfTaskDateConf.getLastExecDate());
                        log.info("| - TimerUtil>>>>>getRunS:{}", wfTaskDateConf.getRunSecondOfMinute());
                        log.info("| - TimerUtil>>>>>getRunS:{}", wfTaskDateConf.getRunSecondOfMinute());
                    }
                    wfTaskDateConf.setRuntimes(Integer.valueOf(wfTaskDateConf.getRuntimes().intValue() - 1));

                    wfTaskDateConfService.update(wfTaskDateConf);
                } else {
                    wfTaskDateConfService.delete(wfTaskDateConf);
                    lockTaskScheduleMap.lock();
                    try {
                        taskScheduleMap.remove(anEntry.getKey());
                    } catch (Exception e) {
                        log.info(BpmStringUtil.getExceptionStack(e), e);
                        throw new RuntimeException(e);
                    } finally {
                        lockTaskScheduleMap.unlock();
                    }

                }
            }
        }

        threadUtil.execute(anEntry.getKey());
    }

    private boolean needToRun(WfTaskDateConf wfTaskDateConf) {
        TaskConfType taskConfType = TaskConfType.getByName(wfTaskDateConf.getConfType());

        if (taskConfType == TaskConfType.specific) {
            Date confDate = DateUtils.parseStr2Date(wfTaskDateConf.getConfDate());
            Date nowDate = Calendar.getInstance().getTime();

            if (log.isDebugEnabled()) {
                log.debug("| - TimerUtil>>>>>compire:{} with {} result {}", wfTaskDateConf.getConfDate(), nowDate, (confDate.compareTo(nowDate) <= 0));
            }
            return confDate.compareTo(nowDate) <= 0;
        } else {
            Date lastExecDt = null;
            if (wfTaskDateConf.getLastExecDate() != null) {
                lastExecDt = DateUtils.parseStr2Date(wfTaskDateConf.getLastExecDate());
            }
            boolean isOnSchedule = DateUtils.isOnSchedule(
                    DateUtils.getNowTime(),
                    lastExecDt,
                    wfTaskDateConf.getRunMonth().intValue(),
                    wfTaskDateConf.getRunDayOfMonth().intValue(),
                    wfTaskDateConf.getRunDayOfWeek().intValue(),
                    wfTaskDateConf.getRunHourOfDay().intValue(),
                    wfTaskDateConf.getRunMinuteOfHour().intValue(),
                    wfTaskDateConf.getRunSecondOfMinute().intValue(),
                    Integer.parseInt(configUtil.getTaskCheckIntval())
            );
            if (isOnSchedule) {
                if (log.isInfoEnabled()) {
                    log.info("| - TimerUtil>>>>>===============on schedule=====================");
                    log.info("| - TimerUtil>>>>>lastExecDt:{}", lastExecDt);
                    log.info("| - TimerUtil>>>>>getRunMonth:{}", wfTaskDateConf.getRunMonth().intValue());
                    log.info("| - TimerUtil>>>>>getRunDayOfMonth:{}", wfTaskDateConf.getRunDayOfMonth().intValue());
                    log.info("| - TimerUtil>>>>>getRunDayOfWeek:{}", wfTaskDateConf.getRunDayOfWeek().intValue());
                    log.info("| - TimerUtil>>>>>getRunHourOfDay:{}", wfTaskDateConf.getRunHourOfDay().intValue());
                    log.info("| - TimerUtil>>>>>getRunMinuteOfHour:{}", wfTaskDateConf.getRunMinuteOfHour().intValue());
                    log.info("| - TimerUtil>>>>>sec:{}", wfTaskDateConf.getRunSecondOfMinute().intValue());
                    log.info("| - TimerUtil>>>>>now:{}", DateUtils.getNowTime());
                }
            }
            return isOnSchedule;

        }
    }

    private class TaskRunner extends Thread {
        private boolean isTimerStart = false;

        public synchronized boolean getSetTimerStatus(boolean isStart, boolean isSet) {
            if (isSet) {
                this.isTimerStart = isStart;
            }

            return this.isTimerStart;
        }

        @Override
        public void run() {
            while (getSetTimerStatus(false, false)) {
                try {
                    doSpecifyTimeExecTask();
                } catch (Exception e) {
                    log.error(BpmStringUtil.getExceptionStack(e), e);
                }

                try {
                    TimeUnit.MILLISECONDS.sleep(Long.parseLong(configUtil.getTaskCheckIntval()));
                } catch (NumberFormatException | InterruptedException e) {
                    log.error("| - TimerUtil>>>>>timer.task.check.inteval设置不正确", e);
                }
            }
        }
    }

}
