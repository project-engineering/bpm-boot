package com.bpm.workflow.util;

import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.dto.transfer.PageData;

import java.util.HashMap;
import java.util.Map;

public class WorkFlowSelectUtils {

	public static final String PAGE_PAGEDATA = "pageData";
	public static final String PAGE_ROWS = "pageRows";
	public static final String PAGE_CURPAGE = "curPage";
	
	/**
	 * @Description: 获取分页信息
	 * @Param: [pageData, inputMap]
	 * @Return: void
	 */
	@SuppressWarnings("rawtypes")
	public static void initPageData(PageData pageData, Map<String, Object> inputMap) {
		Object pageRows = inputMap.get(PAGE_ROWS);
		Object curPage = inputMap.get(PAGE_CURPAGE);
		int icurPage = BpmStringUtil.getIntByString(curPage);
		int ipageRows = BpmStringUtil.getIntByString(pageRows);
		if(icurPage == 0) {
			icurPage = 1;
		}
		if(ipageRows == 0) {

			ipageRows = 100;
		}
		pageData.setCurPage(icurPage);
		pageData.setPageRows(ipageRows);
		pageData.setStartRows((icurPage - 1) * ipageRows);
	}
	
	/**
	 * 
	 * @param item
	 * @param dateKey
	 */
	public static void changeDateToString(Map<String, Object> item, String dateKey) {
		if (item == null) {
			return;
		}
		if (item.get(dateKey) == null) {
			return;
		}
		String strName = dateKey + "Str";
		item.put(dateKey, item.get(strName));
		item.remove(strName);
	}
	
	public static void changeJsonStrToJson(Map<String, Object> item, String key) {
		if (item == null) {
			return;
		}
		if (item.get(key) == null) {
			return;
		}
		String jsonStr = (String) item.get(key);
		Map<String, Object> jsonObject = JSONObject.parseObject(jsonStr);
		item.put(key, jsonObject);
	}
	
    
	public static Map<String, String> getAllResultDef() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("0", "同意");
		map.put("1", "否决");
		map.put("2", "打回上一节点");
		map.put("3", "追回");
		map.put("4", "终止");
		map.put("5", "打回拟稿人");
		map.put("6", "任务转办");
		map.put("7", "退回任意节点");
		map.put("n", "自动关闭");
		return map;
	}

}
