package com.bpm.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bpm.workflow.entity.WfRuIdentitylink;
import com.bpm.workflow.mapper.WfRuIdentitylinkMapper;
import com.bpm.workflow.service.WfRuIdentitylinkService;
import com.bpm.workflow.util.UtilValidate;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Service
public class WfRuIdentitylinkServiceImpl extends ServiceImpl<WfRuIdentitylinkMapper, WfRuIdentitylink> implements WfRuIdentitylinkService {

    @Resource
    private WfRuIdentitylinkMapper wfRuIdentitylinkMapper;

    @Override
    public int deleteNotActive() {
        String sql = "delete from wf_ru_identitylink   where not exists (select task_id from wf_taskflow  where  task_id = task_id_)";
        return wfRuIdentitylinkMapper.deleteNotActive(sql);
    }

    @Override
    public void insertWfRuIdentitylink(WfRuIdentitylink wfRuIdentitylink) {
        wfRuIdentitylinkMapper.insert(wfRuIdentitylink);
    }

    @Override
    public void deleteByTaskId(WfRuIdentitylink wfRuIdentitylink) {
        LambdaQueryWrapper<WfRuIdentitylink> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(wfRuIdentitylink.getTaskId())) {
            queryWrapper.eq(WfRuIdentitylink::getTaskId,wfRuIdentitylink.getTaskId());
        }
        wfRuIdentitylinkMapper.delete(queryWrapper);
    }

    @Override
    public void deleteByUserIdTaskId(WfRuIdentitylink wfRuIdentitylink) {
        LambdaQueryWrapper<WfRuIdentitylink> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(wfRuIdentitylink.getUserId())) {
            queryWrapper.eq(WfRuIdentitylink::getUserId,wfRuIdentitylink.getUserId());
        }
        if (UtilValidate.isNotEmpty(wfRuIdentitylink.getTaskId())) {
            queryWrapper.eq(WfRuIdentitylink::getTaskId,wfRuIdentitylink.getTaskId());
        }
        wfRuIdentitylinkMapper.delete(queryWrapper);
    }

    @Override
    public WfRuIdentitylink selectByUserIdTaskId(WfRuIdentitylink wfRuIdentitylink) {
        LambdaQueryWrapper<WfRuIdentitylink> queryWrapper = new LambdaQueryWrapper<>();
        if (UtilValidate.isNotEmpty(wfRuIdentitylink.getUserId())) {
            queryWrapper.eq(WfRuIdentitylink::getUserId,wfRuIdentitylink.getUserId());
        }
        if (UtilValidate.isNotEmpty(wfRuIdentitylink.getTaskId())) {
            queryWrapper.eq(WfRuIdentitylink::getTaskId,wfRuIdentitylink.getTaskId());
        }
        return  wfRuIdentitylinkMapper.selectOne(queryWrapper);
    }
}
