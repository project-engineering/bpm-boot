package com.bpm.workflow.util;

import cn.hutool.core.util.StrUtil;
import com.bpm.workflow.util.constant.BpmConstants;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class WorkFlowElementUtil {
	private static Document initDocument(String xmlSource) throws Exception {
		log.info("| - WorkFlowElementUtil>>>>>=====initDocument===== :{} " , xmlSource);
		if (StrUtil.isBlank(BpmConstants.XML_SOURCE)) {
			throw new Exception("传入processId参数为空，请确认！");
		}
		Document doc;
		try {
			doc = DocumentHelper.parseText(xmlSource);
		} catch (DocumentException e) {
			throw new Exception("Document转换异常 ，请确认！");
		}
		return doc;
	}

	@SuppressWarnings("unchecked")
	private static List<Element> getSequenceFlow(String xmlSource) throws Exception {
		Document doc = initDocument(xmlSource);
		Element process = (Element) doc.getRootElement().element("process");
		if (process == null) {
			throw new Exception("XML中没有process标签 ，请确认！");
		}
		List<Element> sequenceFlows = process.elements("sequenceFlow");
		if (sequenceFlows == null || sequenceFlows.size() == 0) {
			throw new Exception("XML中没有sequenceFlow标签 ，请确认！");
		}
		return sequenceFlows;
	}

	public static List<String> getBeforeFlow(String xmlSource, String defId) throws Exception {
		if (StrUtil.isBlank(defId)) {
			throw new Exception("defId 参数不能为空，请确认！");
		}
		List<Element> sequenceFlows = getSequenceFlow(xmlSource);
		List<String> defIds = new ArrayList<String>();
		for (Element sequenceFlow : sequenceFlows) {
			String sourceRef = sequenceFlow.attribute("sourceRef").getValue();
			String targetRef = sequenceFlow.attribute("targetRef").getValue();
			if (defId.equals(targetRef)) {
				defIds.add(sourceRef);
			}
		}
		return defIds;
	}
	
	public static List<String> getNextFlow(String xmlSource, String defId) throws Exception {
		if (StrUtil.isBlank(defId)) {
			throw new Exception("defId 参数不能为空，请确认！");
		}
		List<Element> sequenceFlows = getSequenceFlow(xmlSource);
		List<String> defIds = new ArrayList<String>();
		for (Element sequenceFlow : sequenceFlows) {
			String sourceRef = sequenceFlow.attribute("sourceRef").getValue();
			String targetRef = sequenceFlow.attribute("targetRef").getValue();
			if (defId.equals(sourceRef)) {
				defIds.add(targetRef);
			}
		}
		return defIds;
	}
}
