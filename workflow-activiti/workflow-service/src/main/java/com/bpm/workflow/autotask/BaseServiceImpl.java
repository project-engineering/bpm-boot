package com.bpm.workflow.autotask;

import com.bpm.workflow.util.TaskUtil;
import com.bpm.workflow.util.ThreadUtil;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import java.util.HashMap;
import java.util.Map;

public abstract  class BaseServiceImpl implements JavaDelegate, Runnable{
    
	public static final String SERVICE_TYPE_SYNC= "SERVICE_TYPE_SYNC";
	public static final String SERVICE_TYPE_ASYNC= "SERVICE_TYPE_ASYNC";
	public static final String SERVICE_TYPE_CALLBACK= "SERVICE_TYPE_CALLBACK";

	public static ThreadUtil threadUtil;
	public static TaskUtil taskUtil;
	

	
	private String serviceTaskDefId;
	private String executionId;
	private String processDefinitionId;
	private String processInstanceId;

	
	
	@Override
	public void execute(DelegateExecution execution) {
		this.serviceTaskDefId = execution.getCurrentActivityId();	
		this.executionId = execution.getId();
		this.processDefinitionId = execution.getProcessDefinitionId();
		this.processInstanceId = execution.getProcessInstanceId();
		
		
		addInfo = getAddtionalInfoServiceTypeByNodeConf(execution);
		
		if(SERVICE_TYPE_ASYNC.equals(addInfo.get(DelegateExecutionInfoKey.SERVICE_TYPE))
				|| SERVICE_TYPE_CALLBACK.equals(addInfo.get(DelegateExecutionInfoKey.SERVICE_TYPE))) {
			threadUtil.execute(this);
			
		}else {
			executeImpl(serviceTaskDefId,executionId,processInstanceId,processDefinitionId);
		}
		
		if(!SERVICE_TYPE_CALLBACK.equals(addInfo.get(DelegateExecutionInfoKey.SERVICE_TYPE))) {
			taskUtil.completeServiceTask(serviceTaskDefId, executionId, processInstanceId,processDefinitionId, null, false);
		}
	}
	
	private Map<DelegateExecutionInfoKey,String> getAddtionalInfoServiceTypeByNodeConf(DelegateExecution execution) {
		addInfo = new HashMap<DelegateExecutionInfoKey,String>();
		String documentation = execution.getCurrentFlowElement().getDocumentation();
		String[] infos = documentation.split(":");
		addInfo.put(DelegateExecutionInfoKey.SERVICE_TYPE, infos[0]);
		if(SERVICE_TYPE_CALLBACK.equals(infos[0])) {
			String[] callInfos = infos[1].split(",");
			addInfo.put(DelegateExecutionInfoKey.CALL_INFO_SERVICE_ID, callInfos[0]);
			addInfo.put(DelegateExecutionInfoKey.CALL_INFO_METHOD_ID, callInfos[1]);
		}
		return addInfo;
	}

	protected Map<DelegateExecutionInfoKey,String> addInfo = null;

	public enum DelegateExecutionInfoKey{
		/**
		 * 服务类型
		 */
		SERVICE_TYPE("SERVICE_TYPE"),
		/**
		 * 调用信息-服务编号
		 */
		CALL_INFO_SERVICE_ID("CALL_INFO_SERVICE_ID"),
		/**
		 * 调用信息-方法编号
		 */
		CALL_INFO_METHOD_ID("CALL_INFO_METHOD_ID");
		
		private String name;

		private DelegateExecutionInfoKey(String name) {
			this.name= name;
		}
		public String getName() {
			return name;
		}
	}
	
	@Override
	public void run() {
		executeImpl(serviceTaskDefId,executionId,processInstanceId,processDefinitionId);
	}


	protected abstract void executeImpl(String serviceTaskId,String executionId,String processInstanceId,String processDefinitionId);
}
