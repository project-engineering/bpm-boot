package com.bpm.workflow.engine.listener;

import com.bpm.workflow.constant.Constants;
import com.bpm.workflow.constant.ErrorCode;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.service.common.WfCommService;
import com.bpm.workflow.util.ModelProperties;
import com.bpm.workflow.util.ProcessUtil;
import com.bpm.workflow.util.SpringUtil;
import com.bpm.workflow.util.WfVariableUtil;
import lombok.extern.log4j.Log4j2;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 任务节点完成的监听器
 *
 */
@Log4j2
@Component("taskCompleteListener")
public class TaskCompleteListener extends SpringUtil implements TaskListener {

	@Resource
	private ProcessUtil processUtil;
	/**
	 * 
	 */
	private static final long serialVersionUID = 3813696337579080796L;

	@Resource
	private WfCommService wfCommService;
	@Resource
	private WfModelPropExtendsService wfModelpropExtendsService;
	
	@Override
	public void notify(DelegateTask delegateTask) {
		
		FlowElement element = delegateTask.getExecution().getCurrentFlowElement();
        String nodeType =processUtil.getTaskProp(delegateTask.getProcessDefinitionId(),element, Constants.NODETYPE);// element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODETYPE);
		String nodeId = processUtil.getTaskProp(delegateTask.getProcessDefinitionId(),element, Constants.NODEPROP_REFID);//element.getAttributeValue(Constants.XMLNAMESPACE, Constants.NODEPROP_REFID);
		List<WfModelPropExtends> extendsPropList = wfModelpropExtendsService.selectModelExtendsPropByOwnId(nodeId);
    	Map<String, Object> variableMap = WfVariableUtil.getVariableMap();
    	WfVariableUtil.removeVariableMap( Constants.ADDITION_MAPKEY);
		if(log.isInfoEnabled()) {
			log.info("| - TaskCompleteListener>>>>>离开任务节点[taskDefId:{},taskDefName:{}]",delegateTask.getTaskDefinitionKey(),delegateTask.getName());
			log.info("| - TaskCompleteListener>>>>>离开当前任务ID[{}]",delegateTask.getId());
			log.debug("| - TaskCompleteListener>>>>>离开当前流程定义ID[{}]",delegateTask.getProcessDefinitionId());
			log.info("| - TaskCompleteListener>>>>>离开当前流程实例ID[{}]",delegateTask.getProcessInstanceId());
			log.debug("| - TaskCompleteListener>>>>>离开业务参数[{}]",variableMap.toString());
		}
		
		//会签节点特殊处理--最后结果统计
		switch (nodeType) {
		case Constants.NODE_TYPE_SIGN:
		
			//下面两个实例数据判断必须从activiti的流程变量里获取
			int allInstCount = (int) delegateTask.getVariable("nrOfInstances");
			int complInstCount = (int) delegateTask.getVariable("nrOfCompletedInstances");
			if (allInstCount - complInstCount ==1) {
				wfCommService.isCompleteForSignComm(delegateTask.getExecution(),variableMap,true);
			}
			break;
		default:
			break;
		}
		//结束条件 ---应该在节点的最后执行
		wfCommService.conditionalVerification(ModelProperties.outputCondition.getName(), extendsPropList, variableMap, ErrorCode.WF000023);

	}
	
}
