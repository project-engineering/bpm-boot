package com.bpm.workflow.controller.workflow;

import com.alibaba.fastjson2.JSONObject;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfParam;
import com.bpm.workflow.service.WfParamService;
import com.bpm.workflow.service.common.ParamShowConfigService;
import com.bpm.workflow.util.ObjectUtil;
import com.bpm.workflow.vo.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.log4j.Log4j2;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@Validated
@Log4j2
@Tag(name = "工作流-参数管理")
@RestController
@RequestMapping("/workflowParam")
public class ParamController {
    @Resource
    private WfParamService wfParamService;

    @Resource
    private ParamShowConfigService paramShowConfigService;

    @ResponseBody
    @Operation(summary = "新增参数")
    @PostMapping("/addParam")
    public Result<Object> addParam(@RequestBody @Validated ParamAddRequest addRequest) {
        WfParam param = ObjectUtil.copyBeanProp(addRequest, WfParam.class);
        wfParamService.insertWfParam(param, addRequest.getItemChannelTypeList());
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "删除参数")
    @PostMapping("/deleteParam")
    public Result<Object> deleteParam(@RequestBody @Validated ParamDeleteRequest deleteRequest) {
        wfParamService.deleteById(deleteRequest.getId());
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "更新参数")
    @PostMapping("/updateParam")
    public Result<Void> updateParam(@RequestBody @Validated ParamUpdateRequest updateRequest) {
        WfParam param = ObjectUtil.copyBeanProp(updateRequest, WfParam.class);
        wfParamService.updateById(param, updateRequest.getItemChannelTypeList());
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "查询参数列表")
    @PostMapping("/listParam")
    public Result<ParamQueryResponse> listParam(@RequestBody @Validated ParamQueryRequest queryRequest) {
        JSONObject paramJson = JSONObject.parseObject(JSONObject.toJSONString(queryRequest));
        PageData<WfParam> pageData = ObjectUtil.getPageData(queryRequest, WfParam.class);
        List<WfParam> selectList = wfParamService.selectParamList(paramJson, pageData);
        return Result.success(pageData.getTotalRows(), ObjectUtil.copyBeanPropList(selectList, ParamQueryResponse.class));
    }


    @ResponseBody
    @Operation(summary = "根据事项小类查询展示配置")
    @GetMapping("/queryShowConfigByParam")
    public Result<ParamShowConfigQueryResponse> queryShowConfigByParam(@Valid @NotBlank @RequestParam String systemIdCode) {
        ParamShowConfigQueryResponse paramShowConfigQueryResponse = paramShowConfigService.queryShowConfigByParam(systemIdCode);
        return Result.success(paramShowConfigQueryResponse);
    }
}