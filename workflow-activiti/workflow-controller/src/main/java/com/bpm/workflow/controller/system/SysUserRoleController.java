package com.bpm.workflow.controller.system;

import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.constant.Constant;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.service.system.SysUserRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@RestController
@RequestMapping("/api/userRole")
@Tag(name = "用户角色管理")
public class SysUserRoleController {
    @Resource
    SysUserRoleService sysUserRoleService;
    @WebLog(module = "用户角色管理", type = Constant.SELECT, description = "查询用户角色")
    @Operation(summary = "查询用户角色")
    @GetMapping("/getRoleByUserId/{userId}")
    public Result getRoleByUserId(@PathVariable("userId") String userId){
        return Result.success(sysUserRoleService.getRoleByUserId(userId));
    }

}
