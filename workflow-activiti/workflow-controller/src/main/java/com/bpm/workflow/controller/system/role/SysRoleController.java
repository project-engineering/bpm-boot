package com.bpm.workflow.controller.system.role;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.constant.Constant;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.entity.system.role.SysRole;
import com.bpm.workflow.service.system.role.SysRoleService;
import com.bpm.workflow.vo.system.role.RoleParam;
import com.bpm.workflow.vo.system.role.RoleSelectType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "角色管理")
@RestController
@RequestMapping("/api/role")
public class SysRoleController {
    @Resource
    private SysRoleService sysRoleService;

    /**
     * 新增角色信息
     * @param sysRole
     * @return
     */
    @Operation(summary = "新增角色信息")
    @WebLog(module = "角色管理", type = Constant.ADD, description = "新增角色信息")
    @PostMapping
    public Result add(@RequestBody SysRole sysRole){
        if (sysRoleService.addRole(sysRole)) {
            return Result.success("新增角色成功!");
        }
        return Result.fail("新增角色失败!");
    }

    /**
     * 修改角色信息
     * @param sysRole
     * @return
     */
    @Operation(summary = "修改角色信息")
    @WebLog(module = "角色管理", type = Constant.UPDATE, description = "修改角色信息")
    @PutMapping
    public Result update(@RequestBody SysRole sysRole){
        if (sysRoleService.updateById(sysRole)) {
            return Result.success("修改角色成功!");
        }
        return Result.fail("修改角色失败!");
    }

    /**
     * 删除角色信息
     * @param roleId
     * @return
     */
    @Operation(summary = "删除角色信息")
    @WebLog(module = "角色管理", type = Constant.DELETE, description = "删除角色信息")
    @DeleteMapping("/{roleId}")
    public Result delete(@PathVariable("roleId") Long roleId){
        if (sysRoleService.removeById(roleId)) {
            return Result.success("删除角色成功!");
        }
        return Result.fail("删除角色失败!");
    }

    /**
     * 分页查询角色信息
     * @param param
     * @return
     */
    @Operation(summary = "分页查询角色信息")
    @WebLog(module = "角色管理", type = Constant.SELECT, description = "分页查询角色信息")
    @GetMapping("/list")
    public Result list(RoleParam param){
        IPage<SysRole> list = sysRoleService.getList(param);
        return Result.success(list);
    }

    /**
     * 查询角色下拉框信息
     * @return
     */

    @Operation(summary = "查询角色下拉框信息")
    @GetMapping("/getRoleSelectList")
    public Result getRoleSelectList(){
        List<RoleSelectType> list = sysRoleService.getRoleSelectList();
        return Result.success(list);
    }
}
