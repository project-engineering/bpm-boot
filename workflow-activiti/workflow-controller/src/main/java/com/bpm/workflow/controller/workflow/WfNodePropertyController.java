package com.bpm.workflow.controller.workflow;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.dto.transfer.WorkFlowNodePropData;
import com.bpm.workflow.entity.WfNodeProperty;
import com.bpm.workflow.service.WfNodePropertyService;
import com.bpm.workflow.util.ObjectUtil;
import com.bpm.workflow.vo.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * 节点建模相关操作的控制器
 */
@Tag(name = "工作流-节点定义管理")
@RestController
@RequestMapping("/nodeDefinitionManage")
@Log4j2
public class WfNodePropertyController {
    @Resource
    private WfNodePropertyService wfNodePropertyService;

    /**
     * 新增节点定义
     * @param addRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "新增节点定义")
    @PostMapping("/addNodeDefinition")
    @WebLog(description = "新增节点定义")
    public Result<Object> addNodeDefinition(@RequestBody @Validated NodeDefinitionAddRequest addRequest) {
        WorkFlowNodePropData nodeData = ObjectUtil.copyBeanProp(addRequest, WorkFlowNodePropData.class);
        nodeData.setParentId(Integer.valueOf(addRequest.getParentId()));
        wfNodePropertyService.insertNodeProperty(nodeData);
        return Result.success();
    }

    /**
     * 删除节点定义
     * @param deleteRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "删除节点定义")
    @PostMapping("/deleteNodeDefinition")
    @WebLog(description = "删除节点定义")
    public Result<Object> deleteNodeDefinition(@RequestBody @Validated NodeDefinitionDeleteRequest deleteRequest) {
        wfNodePropertyService.deleteNodeProperty(deleteRequest.getNodeId());
        return Result.success();
    }

    /**
     *
     * @param updateRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "修改节点定义")
    @PostMapping("/updateNodeDefinition")
    @WebLog(description = "修改节点定义")
    public Result<Object> updateNodeDefinition(@RequestBody @Validated NodeDefinitionUpdateRequest updateRequest) {
        WorkFlowNodePropData nodeData = ObjectUtil.copyBeanProp(updateRequest, WorkFlowNodePropData.class);
        nodeData.setId(Integer.valueOf(updateRequest.getId()));
        wfNodePropertyService.updateNodeProperty(nodeData);
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "查询节点定义列表")
    @PostMapping("/listNodeDefinition")
    @WebLog(description = "查询节点定义列表")
    public Result<NodeDefinitionQueryResponse> listNodeDefinition(@RequestBody @Validated NodeDefinitionQueryRequest nodeDefinitionQueryRequest) {
        IPage<WfNodeProperty> page = wfNodePropertyService.selectList(nodeDefinitionQueryRequest);
        return Result.success(page.getTotal(), ObjectUtil.copyBeanPropList(page.getRecords(), NodeDefinitionQueryResponse.class));
    }
}
