package com.bpm.workflow.aspect;

import cn.hutool.core.util.IdUtil;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.entity.system.log.SysLogErrorInfo;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import com.bpm.workflow.service.system.log.SysLogErrorInfoService;
import com.bpm.workflow.service.system.log.SysLogInfoService;
import com.bpm.workflow.util.IPUtils;
import com.bpm.workflow.util.JsonUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Aspect
@Component
public class LogAspect {

    private final static Logger log = LoggerFactory.getLogger(WebLogAspect.class);
    /**
     * 操作版本号
     * 项目启动时从命令行传入，例如：java -jar xxx.war --version=201902
     */
    @Value("${spring.application.version}")
    private String version;

    /**
     * 统计请求的处理时间
     */
    ThreadLocal<Long> startTime = new ThreadLocal<>();
    /** 换行符 */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    @Resource
    private SysLogInfoService sysLogInfoService;

    @Resource
    private SysLogErrorInfoService sysLogErrorInfoService;

    /**
     * 设置操作日志切入点 记录操作日志 在注解的位置切入代码
     * @author：liuc
     * @dateTime：2024/02/17 14:22
     * @Params： []
     * @Return： void
     */
    @Pointcut("@annotation(com.bpm.workflow.annotation.WebLog)")
    public void logPoinCut() {
    }

    @Before("logPoinCut()")
    public void doBefore() {
        // 接收到请求，记录请求开始时间
        startTime.set(System.currentTimeMillis());
    }

    /**
     * 设置操作异常切入点记录异常日志 扫描所有controller包下操作
     * @author：liuc
     * @dateTime：2024/02/17 14:22
     * @Params： []
     * @Return： void
     */
    @Pointcut("execution(* com.bpm.workflow.*.controller..*.*(..))")
    public void exceptionLogPoinCut() {
    }

    /**
     * 正常返回通知，拦截用户操作日志，连接点正常执行完成后执行， 如果连接点抛出异常，则不会执行
     * @author：liuc
     * @dateTime：2024/02/17 14:22
     * @Params： [joinPoint, keys]
     * @Return： void
     */
    @AfterReturning(value = "logPoinCut()", returning = "keys")
    public void doAfterReturning(JoinPoint joinPoint, Object keys) {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);

        SysLogInfo logInfo = SysLogInfo.builder().build();
        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();

            // 获取切入点所在的方法
            Method method = signature.getMethod();

            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();

            // 获取操作
            WebLog log = method.getAnnotation(WebLog.class);
            if (Objects.nonNull(log)) {
                logInfo.setModule(log.module());
                logInfo.setType(log.type());
                logInfo.setMessage(log.description());
            }

            logInfo.setId(IdUtil.simpleUUID());
            logInfo.setMethod(className + "." + method.getName()); // 请求的方法名
            logInfo.setReqParam(JsonUtil.toJson(converMap(request.getParameterMap()))); // 请求参数
            logInfo.setResParam(JsonUtil.toJson(keys)); // 返回结果
//            logInfo.setUserId(SecurityUserUtils.getUser().getId()); // 请求用户ID
//            logInfo.setUserName(SecurityUserUtils.getUser().getUsername()); // 请求用户名称
            logInfo.setIp(IPUtils.getIpAddr(request)); // 请求IP
            logInfo.setUri(request.getRequestURI()); // 请求URI
            logInfo.setCreateTime(LocalDateTime.now()); // 创建时间
            logInfo.setVersion(version); // 操作版本
            logInfo.setTakeUpTime((System.currentTimeMillis() - startTime.get())+ "ms"); // 耗时
            sysLogInfoService.save(logInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @methodName：doAfterThrowing
     * @description：异常返回通知，用于拦截异常日志信息 连接点抛出异常后执行
     * @author：liuc
     * @dateTime：2024/02/17 14:22
     * @Params： [joinPoint, e]
     * @Return： void
     */
    @AfterThrowing(pointcut = "exceptionLogPoinCut()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable e) {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);

        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();

            // 获取切入点所在的方法
            Method method = signature.getMethod();

            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();

            sysLogErrorInfoService.save(
                    SysLogErrorInfo.builder()
                            .id(UUID.randomUUID().toString())
                            .reqParam(JsonUtil.toJson(converMap(request.getParameterMap()))) // 请求参数
                            .method(className + "." + method.getName()) // 请求方法名
                            .name(e.getClass().getName()) // 异常名称
                            .message(stackTraceToString(e.getClass().getName(), e.getMessage(), e.getStackTrace())) // 异常信息
//                            .userId(SecurityUserUtils.getUser().getId()) // 操作员ID
//                            .userName(SecurityUserUtils.getUser().getUsername()) // 操作员名称
                            .uri(request.getRequestURI()) // 操作URI
                            .ip(IPUtils.getIpAddr(request)) // 操作员IP
                            .version(version) // 版本号
                            .createTime(LocalDateTime.now()) // 发生异常时间
                            .build()
            );
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }


    /**
     * 转换request 请求参数
     * @author：liuc
     * @dateTime：2024/02/17 14:12
     * @Params： [paramMap]
     * @Return： java.util.Map<java.lang.String, java.lang.String>
     */
    public Map<String, String> converMap(Map<String, String[]> paramMap) {
        Map<String, String> rtnMap = new HashMap<String, String>();
        for (String key : paramMap.keySet()) {
            rtnMap.put(key, paramMap.get(key)[0]);
        }
        return rtnMap;
    }

    /**
     * 转换异常信息为字符串
     * @author：liuc
     * @dateTime：2024/02/17 14:22
     * @Params： [exceptionName, exceptionMessage, elements]
     * @Return： java.lang.String
     */
    public String stackTraceToString(String exceptionName, String exceptionMessage, StackTraceElement[] elements) {
        StringBuffer strbuff = new StringBuffer();
        for (StackTraceElement stet : elements) {
            strbuff.append(stet + "<br/>");
        }
        String message = exceptionName + ":" + exceptionMessage + "<br/>" + strbuff.toString();
        return message;
    }

}
