package com.bpm.workflow.controller.workflow;

import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.dto.transfer.WorkFlowModelPropExtData;
import com.bpm.workflow.entity.WfModelPropExtends;
import com.bpm.workflow.entity.WfPropertiesConfig;
import com.bpm.workflow.service.WfModelPropExtendsService;
import com.bpm.workflow.util.ObjectUtil;
import com.bpm.workflow.vo.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 节点建模或流程建模的相关扩展属性操作的控制器
 */
@Validated
@Log4j2
@Tag(name = "工作流-扩展属性管理")
@RestController
@RequestMapping("/wfModelpropExtends")
public class WfModelpropExtendsController {
    @Resource
    private WfModelPropExtendsService wfModelPropExtendsService;

    @ResponseBody
    @Operation(summary = "新增扩展属性")
    @PostMapping("/addModelProperty")
    @WebLog(description = "新增扩展属性")
    public Result<Object> addModelProperty(@RequestBody @Validated ModelPropertyAddRequest addRequest) {
        WorkFlowModelPropExtData modelProp = ObjectUtil.copyBeanProp(addRequest, WorkFlowModelPropExtData.class);
        wfModelPropExtendsService.insertModelPropExtend(modelProp);
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "删除扩展属性")
    @PostMapping("/deleteModelProperty")
    @WebLog(description = "删除扩展属性")
    public Result<Object> deleteModelProperty(@RequestBody @Validated ModelPropertyDeleteRequest deleteRequest) {
        WorkFlowModelPropExtData modelProp = ObjectUtil.copyBeanProp(deleteRequest, WorkFlowModelPropExtData.class);
        wfModelPropExtendsService.deleteModelPropExtend(modelProp);
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "更新扩展属性")
    @PostMapping("/updateModelProperty")
    @WebLog(description = "更新扩展属性")
    public Result<Object> updateModelProperty(@RequestBody @Validated ModelPropertyUpdateRequest updateRequest) {
        WfModelPropExtends modelProp = ObjectUtil.copyBeanProp(updateRequest, WfModelPropExtends.class);
        wfModelPropExtendsService.updateModelPropExtend(modelProp);
        return Result.success();
    }

    @ResponseBody
    @Operation(summary = "查询扩展属性列表")
    @PostMapping("/listModelProperty")
    @WebLog(description = "查询扩展属性列表")
    public Result<Object> listModelProperty(@RequestBody @Validated ModelPropertyQueryRequest listRequest) {
        WorkFlowModelPropExtData modelProp = ObjectUtil.copyBeanProp(listRequest, WorkFlowModelPropExtData.class);
        List<WfModelPropExtends> resultList = wfModelPropExtendsService.selectModelPropExtendList(modelProp);
        return Result.success(ObjectUtil.copyBeanPropList(resultList, ModelPropertyQueryResponse.class));
    }

    /**
     * 查询可配置的扩展属性
     *
     * @param listRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "查询可配置的扩展属性")
    @PostMapping("/listModelConfigProperty")
    @WebLog(description = "查询可配置的扩展属性")
    public Result<Object> listModelConfigProperty(@RequestBody @Validated ModelPropertyConfigQueryRequest listRequest) {
        WfPropertiesConfig propConfig = ObjectUtil.copyBeanProp(listRequest, WfPropertiesConfig.class);
        List<WfPropertiesConfig> configList = wfModelPropExtendsService.selectEnableConfigProperis(propConfig);
        List<ModelPropertyConfigQueryResponse> reslut = configList.stream().map(t -> {
                    ModelPropertyConfigQueryResponse response = ObjectUtil.copyBeanProp(t, ModelPropertyConfigQueryResponse.class);
                    response.setChildList(ObjectUtil.copyBeanPropList(t.getChildList(), ModelPropertyConfigQueryResponse.class));
                    return response;
                }
        ).collect(Collectors.toList());
        return Result.success(reslut);
    }

    @ResponseBody
    @Operation(summary = "脚本测试")
    @PostMapping("/javaScriptTest")
    @WebLog(description = "脚本测试")
    public Result<Object> javaScriptTest(@RequestBody @Validated ModelPropertyJavaScriptTestRequest javaScriptRequest) {
        String js = javaScriptRequest.getJs();
        HashMap<String, Object> businessMap = (HashMap<String, Object>) javaScriptRequest.getBusinessMap();
        String result = wfModelPropExtendsService.executeJavaScript(js, businessMap);
        return Result.success(result);
    }
}
