package com.bpm.workflow.controller.workflow;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowDefinitionData;
import com.bpm.workflow.entity.WfDefinition;
import com.bpm.workflow.entity.system.user.SysUser;
import com.bpm.workflow.service.WfDefinitionService;
import com.bpm.workflow.util.ObjectUtil;
import com.bpm.workflow.vo.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  流程定义相关的前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Tag(name = "工作流-流程定义管理")
@RestController
@RequestMapping("/wfDefinitionManage")
@Log4j2
public class WfDefinitionController {
    @Resource
    private WfDefinitionService wfDefinitionService;

    @ResponseBody
    @Operation(summary = "新增流程定义")
    @PostMapping("/addWfDefinition")
    @WebLog(description = "新增流程定义")
    public Result<Object> addWfDefinition(@RequestBody @Validated WfDefinitionAddRequest addRequest) {
        WorkFlowDefinitionData definitionData = ObjectUtil.copyBeanProp(addRequest, WorkFlowDefinitionData.class);
        definitionData.setParentId(Integer.valueOf(addRequest.getParentId()));
        wfDefinitionService.addWorkFlow(definitionData);
        return Result.success();
    }

    /**
     * 删除流程定义
     * @param defId
     * @return
     */
    @ResponseBody
    @Operation(summary = "删除流程定义")
    @WebLog(description = "删除流程定义")
    @DeleteMapping("/deleteFlowDefinition/{defId}")
    public Result<Object> deleteFlowDefinition(@PathVariable("defId") String defId) {
        wfDefinitionService.deleteWorkFlow(0, defId);
        return Result.success();
    }

    /**
     * 流程定义更新
     * @param definitionUpdateRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "更新流程定义")
    @PostMapping("/updateFlowDefinition")
    @WebLog(description = "更新流程定义")
    public Result<Object> updateFlowDefinition(@RequestBody @Validated DefinitionUpdateRequest definitionUpdateRequest) {
        WorkFlowDefinitionData definitionData = ObjectUtil.copyBeanProp(definitionUpdateRequest, WorkFlowDefinitionData.class);
        definitionData.setId(Integer.valueOf(definitionUpdateRequest.getId()));
        wfDefinitionService.updateWorkFlow(definitionData);
        return Result.success();
    }

    /**
     * @Description: 流程定义列表查询
     * @Param: [queryRequest]
     */
    @ResponseBody
    @Operation(summary = "流程定义列表查询")
    @PostMapping("/listFlowDefinition")
    @WebLog(description = "流程定义列表查询")
    public Result listFlowDefinition(@RequestBody @Validated DefinitionQueryRequest definitionQueryRequest) {
        IPage<WfDefinition> list = wfDefinitionService.queryWorkFlowList(definitionQueryRequest);
        return Result.success(list);
    }

    /**
     * 流程定义查看
     *
     * @param viewRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "流程定义查看")
    @PostMapping("/viewFlowDefinition")
    @WebLog(description = "流程定义查看")
    public Map<String, Object> viewFlowDefinition(@RequestBody @Validated DefinitionViewRequest viewRequest) {
        //由于设计器暂时没法修改，所以这里就适配设计器了
        log.info(viewRequest.getId());
        WorkFlowDefinitionData data = new WorkFlowDefinitionData();
        data.setId(Integer.valueOf(viewRequest.getId()));
        WfDefinition wdDefinition = wfDefinitionService.viewWorkFlow(data);
        log.info(JSONObject.toJSONString(wdDefinition));

        Map<String,Object> resultMap = new HashMap<>();
        Map<String,Object> headMap = new HashMap<>();
        String code="000000",type="S",message="服务调用成功！";
        if(wdDefinition!=null) {
            resultMap.put("modelId", wdDefinition.getId());
            resultMap.put("key", wdDefinition.getDefId());
            resultMap.put("name", wdDefinition.getDefName());
            resultMap.put("jsonSource", wdDefinition.getJsonSource());
            resultMap.put("description", wdDefinition.getDefDesc());
            if(StringUtils.isEmpty(wdDefinition.getJsonSource())) {
                String initJson = "{\"id\":\"canvas\",\"resourceId\":\"canvas\",\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\"},\"properties\":{\"process_id\":\""+wdDefinition.getDefId()+"\",\"name\":\""+wdDefinition.getDefName()+"\"},"
                        + "\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"childShapes\":[],\"dockers\":[],\"outgoing\":[],\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"开始事件\",\"workflow_rule\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"}}]}";
                resultMap.put("jsonSource", initJson);
            }

        }else{
            code="999999";
            type="F";
            message="查询流程定义信息出错！";
        }

        headMap.put("code", code);
        headMap.put("type", type);
        headMap.put("message", message);
        resultMap.put("returnCode", headMap);
        log.info(JSONObject.toJSONString(resultMap));
        return resultMap;
    }

    /**
     * 保存流程图
     * @param saveRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "保存流程图")
    @PostMapping("/saveFlowDefinition")
    @WebLog(description = "保存流程图")
    public Result<Object> saveFlowDefinition(@RequestBody @Validated DefinitionSaveRequest saveRequest) {
        log.info(JSONObject.toJSONString(saveRequest));
        //此处也是适配设计器接口
        WorkFlowDefinitionData definitionData = new WorkFlowDefinitionData();
        definitionData.setDefId(saveRequest.getDefId());
        definitionData.setXmlSource(saveRequest.getXmlSource());
        definitionData.setJsonSource(saveRequest.getSourceJson());
        wfDefinitionService.saveWorkFlowDefinition(definitionData);
        return Result.success();
    }

    /**
     * 流程部署
     * @param deployRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "流程部署")
    @PostMapping("/deployFlowDefinition")
    @WebLog(description = "流程部署")
    public Result<Object> deployFlowDefinition(@RequestBody @Validated DefinitionDeployRequest deployRequest) {
        wfDefinitionService.deployWorkFlowByDefId(deployRequest.getDefId());
        return Result.success();
    }

    /**
     * 历史版本查询
     * @param listVersionRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "历史版本查询")
    @PostMapping("/listFlowDefinitionHistVersion")
    @WebLog(description = "历史版本查询")
    public Result listFlowDefinitionHistVersion(
            @RequestBody @Validated DefinitionListVersionRequest listVersionRequest) {
            List<WfDefinition> resultList = wfDefinitionService.selectVersionList(listVersionRequest.getDefId(),
                new PageData<>());
        return Result.success(resultList.size(),resultList);
    }

    /**
     * 版本回退
     * @param backVersionRequest
     * @return
     */
    @ResponseBody
    @Operation(summary = "版本回退")
    @PostMapping("/backFlowDefinitionByVersion")
    @WebLog(description = "版本回退")
    public Result<Object> backFlowDefinitionByVersion(
            @RequestBody @Validated DefinitionBackVersionRequest backVersionRequest) {
        WorkFlowDefinitionData definitionData = ObjectUtil.copyBeanProp(backVersionRequest,
                WorkFlowDefinitionData.class);
        definitionData.setDefVersion(backVersionRequest.getDefVersion());
        wfDefinitionService.backHistVersion(definitionData);
        return Result.success();
    }
}
