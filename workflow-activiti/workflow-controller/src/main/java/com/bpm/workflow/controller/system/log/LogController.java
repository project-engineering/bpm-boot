package com.bpm.workflow.controller.system.log;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.constant.Constant;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import com.bpm.workflow.service.system.log.SysLogInfoService;
import com.bpm.workflow.vo.system.log.LogParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@Tag(name = "日志管理")
@RestController
@RequestMapping("/api/log")
public class LogController {
    @Resource
    private SysLogInfoService sysLogInfoService;
    /**
     * 分页查询日志信息
     * @param param
     * @return
     */
    @Operation(summary = "分页查询日志信息")
    @WebLog(module = "日志管理", type = Constant.SELECT, description = "分页查询日志信息")
    @GetMapping("/list")
    public Result list(LogParam param){
        IPage<SysLogInfo> list = sysLogInfoService.getList(param);
        return Result.success(list);
    }
}
