package com.bpm.workflow.controller.system;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色菜单表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "角色菜单管理")
@RestController
@RequestMapping("/api/roleMenu")
public class SysRoleMenuController {

}
