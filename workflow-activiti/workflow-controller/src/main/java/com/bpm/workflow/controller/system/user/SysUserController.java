package com.bpm.workflow.controller.system.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bpm.workflow.annotation.WebLog;
import com.bpm.workflow.constant.Constant;
import com.bpm.workflow.data.Result;
import com.bpm.workflow.entity.system.user.SysUser;
import com.bpm.workflow.service.system.user.SysUserService;
import com.bpm.workflow.vo.system.user.UserParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Tag(name = "用户管理")
@RestController
@RequestMapping("/api/user")
public class SysUserController {
    @Resource
    private SysUserService sysUserService;

    /**
     * 新增用户信息
     * @param sysUser
     * @return
     */
    @Operation(summary = "新增用户信息")
    @WebLog(module = "用户管理", type = Constant.ADD, description = "新增用户信息")
    @PostMapping
    public Result add(@RequestBody SysUser sysUser){
        if (sysUserService.addUser(sysUser)) {
            return Result.success("新增用户成功!");
        }
        return Result.fail("新增用户失败!");
    }

    /**
     * 修改用户信息
     * @param sysUser
     * @return
     */
    @Operation(summary = "修改用户信息")
    @WebLog(module = "用户管理", type = Constant.UPDATE, description = "修改用户信息")
    @PutMapping
    public Result update(@RequestBody SysUser sysUser){
        if (sysUserService.updateUser(sysUser)) {
            return Result.success("修改用户成功!");
        }
        return Result.fail("修改用户失败!");
    }

    /**
     * 删除角色信息
     * @param userId
     * @return
     */
    @Operation(summary = "删除角色信息")
    @WebLog(module = "用户管理", type = Constant.DELETE, description = "删除角色信息")
    @DeleteMapping("/{userId}")
    public Result delete(@PathVariable("userId") String userId){
        if (sysUserService.deleteUser(userId)) {
            return Result.success("删除用户成功!");
        }
        return Result.fail("删除用户失败!");
    }

    /**
     * 分页查询用户信息
     * @param param
     * @return
     */
    @Operation(summary = "分页查询用户信息")
    @WebLog(module = "用户管理", type = Constant.SELECT, description = "分页查询用户信息")
    @GetMapping("/list")
    public Result list(UserParam param){
        IPage<SysUser> list = sysUserService.getList(param);
        return Result.success(list);
    }

    @Operation(summary = "重置密码")
    @WebLog(module = "用户管理", type = Constant.UPDATE, description = "重置密码")
    @PostMapping("/resetPwd/{userId}")
    public Result resetPwd(@PathVariable("userId") String userId){
        if (sysUserService.resetPwd(userId)>0) {
            return Result.success("重置密码成功!");
        }
        return Result.fail("重置密码失败!");
    }
}
