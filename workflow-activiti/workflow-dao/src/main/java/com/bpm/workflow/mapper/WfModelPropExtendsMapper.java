package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.dto.transfer.WorkFlowModelPropExtData;
import com.bpm.workflow.entity.WfModelPropExtends;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfModelPropExtendsMapper extends BaseMapper<WfModelPropExtends> {

    List<WfModelPropExtends> selectModelExtendsPropByOwnIdAndOwnType(WorkFlowModelPropExtData workFlowModelPropExtData);
}
