package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.WfAcceditLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfAcceditLogMapper extends BaseMapper<WfAcceditLog> {

}
