package com.bpm.workflow.mapper;

import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskData;
import com.bpm.workflow.dto.transfer.WorkFlowTaskFlowData;
import com.bpm.workflow.dto.transfer.WorkFlowUnDoTaskGroupData;
import com.bpm.workflow.entity.WfBusinessData;
import com.bpm.workflow.entity.WfInstance;
import com.bpm.workflow.entity.WfTaskflow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

/**
 * 流程查询 Mapper
 *
 */
@Mapper
public interface WorkFlowSelectMapper {

	/**
	 * 查询一个流程实例下子流程节点ID
	 * 
	 * @return
	 */
	List<String> selectTaskIdByProcessIdForSubProc(String processId);
	
	/**
	 * 查询子流程节点生成的流程实例ID
	 * 
	 * @return
	 */
	List<String> selectProcessIdByTaskIdForSubProc(String taskId);
	
	/**
	 * @Description: 查询待办任务（不含有userId）
	 * @Param: [theArg]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfTaskflow>
	 * @Author:
	 * @Date: 2021/9/23
	 */
	List<WfTaskflow> selectUndoTaskFlow(WorkFlowTaskFlowData theArg);
	
	/**
	 * 查询待办任务 (分页)（不含有userId）
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectUndoTaskFlow(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * @Description: 查询待办任务（含有userId）
	 * @Param: [theArg]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfTaskflow>
	 * @Author:
	 * @Date: 2021/9/23
	 */
	List<WfTaskflow> selectUndoTaskFlowHaveUserId(WorkFlowTaskFlowData theArg);
	
	/**
	 * 查询待办任务 (分页)（含有userId）
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectUndoTaskFlowHaveUserId(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * 统计待办任务数量
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WorkFlowUnDoTaskGroupData> selectUndoTaskFlowByGroup(WorkFlowTaskFlowData taskFlowData, PageData<WorkFlowUnDoTaskGroupData> pageData);
	
	/**
	 * 查询流程实例列表
	 * @param param
	 * @return
	 */
	List<WfInstance> selectInstanceList(Map<String, Object> param);
	
	/**
	 * 查询流程实例列表(分页)
	 * @param param
	 * @param pageData
	 * @return
	 */
	List<WfInstance> selectInstanceList(Map<String, Object> param, PageData<WfInstance> pageData);
	
	/**
	 * 查询流程实例列表(管理员)
	 * @param param
	 * @return
	 */
	List<WfInstance> selectInstanceListForManager(Map<String, Object> param);
	
	/**
	 * 查询流程实例列表(管理员) (分页)
	 * @param param
	 * @param pageData
	 * @return
	 */
	List<WfInstance> selectInstanceListForManager(Map<String, Object> param, PageData<WfInstance> pageData);
	
	/**
	 * 查询待办任务记录 (管理员)
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectManagerUndoTaskFlow(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询待办任务记录 (管理员) （分页）
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectManagerUndoTaskFlow(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * 查询已办任务记录 (管理员)
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectManagerDoTaskFlow(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询已办任务记录 (管理员) （分页）
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectManagerDoTaskFlow(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * @Description: 根据流水号信息查询任务记录
	 * @Param: [param]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfTaskflow>
	 * @Author:
	 * @Date: 2021/9/23
	 */
	List<WfTaskflow> selectTaskFlowByBussFlowNo(Map<String, Object> param);
	
	/**
	 * @Description: 根据流水号信息查询任务记录 (分页)
	 * @Param: [param, pageData]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfTaskflow>
	 * @Author:
	 * @Date: 2021/9/23
	 */
	List<WfTaskflow> selectTaskFlowByBussFlowNo(Map<String, Object> param, PageData<WfTaskflow> pageData);
	
	/**
	 * 根据parentTaskId取流程实例ID
	 * @param parentTaskId
	 * @return
	 */
	WfInstance getProcessIdByParentTaskId(String parentTaskId);
	
	/**
	 * 查询实例未结束的子流程
	 * @param param
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectSubTaskflowList(Map<String, Object> param, PageData<WfTaskflow> pageData);
	
	/**
	 * 查询实例已结束的子流程
	 * @param param
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectSubHistTaskflowList(Map<String, Object> param, PageData<WfTaskflow> pageData);
	
	/**
	 * 查询任务记录 （wf_taskflow 和 wf_hist_taskflow）
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowAllList(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询任务记录 （wf_taskflow 和 wf_hist_taskflow） (分页)
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowAllList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * 查询任务记录 （wf_hist_taskflow）
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowDoList(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询任务记录 （wf_hist_taskflow） (分页)
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowDoList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * 查询任务记录 （wf_taskflow）
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowUndoList(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询任务记录 （wf_taskflow） (分页)
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowUndoList(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	/**
	 * 统计 审批记录 数量 (取同一用户Max纪录)
	 * 
	 * @param taskFlowData
	 * @return
	 */
	List<WorkFlowUnDoTaskGroupData> selectOnlyMaxTaskFlowByGroup(WorkFlowTaskFlowData taskFlowData, PageData<WorkFlowUnDoTaskGroupData> pageData);
	
	/**
	 * @Description: 查询 审批记录(取同一用户Max纪录)
	 * @Param: [taskFlowData]
	 * @Return: java.util.List<com.yonyou.sml.wf.workflow.model.entity.WfTaskflow>
	 * @Author:
	 * @Date: 2021/9/23
	 */
	List<WfTaskflow> selectOnlyMaxTaskFlow(WorkFlowTaskFlowData taskFlowData);
	
	/**
	 * 查询 审批记录(取同一用户Max纪录) (分页)
	 * 
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectOnlyMaxTaskFlow(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
	
	
	/**
	 * 查询可撤回的任务列表：实例未结束，但任务已处理
	 * 
	 * @param param
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> queryEnableBackTaskList(Map<String, Object> param, PageData<WfTaskflow> pageData);

    // update by xuesw 20190529 待办任务节点类型统计查询接口
 	/**
 	 * 查询指定用户待处理的任务节点类型分类的任务数
 	 * 
 	 * @param param
 	 * @param pageData
 	 * @return
 	 * @throws Exception
 	 */
 	public List<Object> queryUnDoNodeTypeCountGroup(Map<String, Object> param,
 			PageData<WorkFlowTaskData> pageData);
 	// update by xuesw 20190529 待办任务节点类型统计查询接口
 	
 	/**
	 * 待办任务填充taskflow
	 * @param map
	 * @return
	 */
	List<WfBusinessData> selectUndoTaskFlowBusinessMap(Map<String, Object> map);
	
	/**
	 * 查询填充taskflow
	 * @param list
	 * @return
	 */
	List<WfBusinessData> selectTaskFlowBusinessMap(List<WfTaskflow> list);
	/**
     * 根据业务上送的流程实例ID列表，查询对应流程实例下对应的待办任务列表信息
     * select task_id taskId, process_id processId from  wf_taskflow where work_state=0 and process_id in(26340023,26340026)
     * @author anmingtao
     * @serialData 2019-05-29
     * @param processIdList 流程实例id列表
     * @return  通过流程实例任务列表查询对应的代办任务
     */
    List<WfTaskflow> selectByTaskIdList(@Param("processIdList") List<String> processIdList);

	List<WorkFlowUnDoTaskGroupData> selectUndoTaskFlowByGroup(WorkFlowTaskFlowData taskFlowData);

	List<WorkFlowUnDoTaskGroupData> selectOnlyMaxTaskFlowByGroup(WorkFlowTaskFlowData taskFlowData);

	List<WorkFlowTaskData> queryUnDoNodeTypeCountGroup(Map<String, Object> param);

	/**
	 * 查询任务记录 （wf_hist_taskflow） (分页)---动态表名
	 *
	 * @param taskFlowData
	 * @param pageData
	 * @return
	 */
	List<WfTaskflow> selectTaskFlowDoListByTable(WorkFlowTaskFlowData taskFlowData, PageData<WfTaskflow> pageData);
}
