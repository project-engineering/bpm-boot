package com.bpm.workflow.mapper;

import com.bpm.workflow.entity.WfPropertiesConfig;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface WfModelPropConfigMapper {

	List<WfPropertiesConfig> selectConfigPropList(WfPropertiesConfig propConfig);
}