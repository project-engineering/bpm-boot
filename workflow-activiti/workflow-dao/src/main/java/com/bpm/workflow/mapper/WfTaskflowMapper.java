package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.dto.transfer.WorkFlowTaskFlowData;
import com.bpm.workflow.entity.TaskCodeCount;
import com.bpm.workflow.entity.WfTaskflow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfTaskflowMapper extends BaseMapper<WfTaskflow> {

    List<TaskCodeCount> selectUnoTaskCountByUsers(List<String> list);

    WfTaskflow selectByIdForUpdate(Integer id);

    List<WfTaskflow> queryTaskDoResultList(WorkFlowTaskFlowData taskFlowData);

    WfTaskflow overTimeTaskList(WorkFlowTaskFlowData taskFlowData);

    List<WfTaskflow> selectUndoTaskFlow(Map<String, Object> param);

    List<TaskCodeCount> selectTaskCodeCount(Map<String, Object> param);

    /**
     * 检查流水号是否存在
     * 根据processCode,systemCode,globSeqNo进行查询
     *
     * @param paramMap
     * @return
     */
    int checkGlobSeqNoIsExsit(Map<String, Object> paramMap);
}
