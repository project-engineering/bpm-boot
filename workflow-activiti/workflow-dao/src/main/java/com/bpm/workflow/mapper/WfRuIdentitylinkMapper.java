package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.WfRuIdentitylink;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfRuIdentitylinkMapper extends BaseMapper<WfRuIdentitylink> {
    @Select("${sqlStr}")
    int deleteNotActive(@Param("sqlStr")String sql);
}
