package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.WfParam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfParamMapper extends BaseMapper<WfParam> {

    List<WfParam> selectParamList(WfParam wfParam);

    WfParam selectLastExecTimeout();
}
