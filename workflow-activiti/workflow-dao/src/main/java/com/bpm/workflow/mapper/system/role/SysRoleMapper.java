package com.bpm.workflow.mapper.system.role;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.system.role.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
