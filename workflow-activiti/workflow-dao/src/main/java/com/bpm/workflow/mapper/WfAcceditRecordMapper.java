package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.WfAcceditRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfAcceditRecordMapper extends BaseMapper<WfAcceditRecord> {

    List<WfAcceditRecord> selectList(Map<String, Object> param);
}
