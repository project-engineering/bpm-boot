package com.bpm.workflow.mapper.system.log;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.system.log.SysLogInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysLogInfoMapper extends BaseMapper<SysLogInfo> {

}

