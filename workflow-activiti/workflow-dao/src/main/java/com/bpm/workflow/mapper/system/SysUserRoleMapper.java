package com.bpm.workflow.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.system.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-11
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
