package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.entity.ParamShowConfig;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParamShowConfigMapper extends BaseMapper<ParamShowConfig> {
}
