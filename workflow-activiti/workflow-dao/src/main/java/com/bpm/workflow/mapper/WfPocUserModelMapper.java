package com.bpm.workflow.mapper;


import com.bpm.workflow.dto.transfer.UserOutputData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 模拟用户信息数据
 * 
 */
@Mapper
public interface WfPocUserModelMapper {
    List<UserOutputData> selectUser(Map<String, Object> map);
}
