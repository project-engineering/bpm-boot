package com.bpm.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bpm.workflow.dto.transfer.PageData;
import com.bpm.workflow.entity.WfDefinition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  流程定义表Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-04-26
 */
@Mapper
public interface WfDefinitionMapper extends BaseMapper<WfDefinition> {

    List<WfDefinition> selectByCondition(Map<String, Object> param, PageData<WfDefinition> pageData);

}
